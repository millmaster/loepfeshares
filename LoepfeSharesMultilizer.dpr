program LoepfeSharesMultilizer;

uses
  Forms,
  BaseForm in 'TEMPLATES\BASEFORM.pas' {mmForm},
  BASEDIALOG in 'TEMPLATES\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in 'TEMPLATES\BASEDIALOGBOTTOM.pas' {DialogBottom},
  SelectionDialog in 'TEMPLATES\SELECTIONDIALOG.pas' {frmSelectionDialog},
  BaseSetup in 'Common\BaseSetup.pas',
  DBVisualBox in 'Common\DBVisualBox.pas',
  FloorActiveX in 'Common\FloorActiveX.pas',
  FloorAxCtrls in 'Common\FloorAxCtrls.pas',
  FloorControlX in 'Common\FloorControlX.pas',
  GpProfH in 'Common\gpprofh.pas',
  IPCClass in 'Common\IPCClass.pas',
  IPCUnit in 'Common\IPCUnit.pas',
  LoepfeGlobal in 'Common\LoepfeGlobal.pas',
  LoepfePluginIMPL in 'Common\LoepfePluginIMPL.pas',
  Mailslot in 'Common\Mailslot.pas',
  mmColorButton in 'Common\mmColorButton.pas',
  MMEventLog in 'Common\mmEventLog.pas',
  mmLineLabel in 'Common\mmLineLabel.pas',
  mmLogin in 'Common\mmLogin.pas' {FLoginMain},
  mmMBCS in 'Common\mmMBCS.pas',
  MMMessages in 'Common\MMMessages.pas',
  mmRotateLabel in 'Common\mmRotateLabel.pas',
  MMSecurity in 'Common\MMSecurity.pas',
  MMSecurityConfig in 'Common\MMSecurityConfig.pas' {frmSelectComponents},
  NetAPI32MM in 'Common\NetAPI32MM.pas',
  NetErrors in 'Common\NetErrors.pas',
  NTApis in 'Common\NTApis.pas',
  NumCtrl in 'Common\NumCtrl.pas',
  TrayIcon in 'Common\TrayIcon.pas',
  WrapperForm in 'Common\WrapperForm.pas' {frmWrapper},
  ToolbarCustom in 'Templates\ToolbarCustom.pas' {BaseConfigForm};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TmmDialog, mmDialog);
  Application.Run;
end.
