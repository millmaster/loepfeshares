{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMSecurity.pas
| Projectpart...: Millmaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.11.1999  1.00  Wss | First release created: TMMSecurityDB, TMMSecurityControl
| 14.03.2000  1.01  Wss | TBaseMMSecurity.ReloadUserInfo readout changed
| 30.05.2001  1.02  Wss | - CurrentMMUser, CurrentNTUser properties added in TMMSecurityControl
                          - in TBaseMMSecurity GetCurrentUser() added and ReloadUserInfo adapted
| 26.07.2001  1.03  Wss | Bug in TMMSecurityControl: form name was token from components name
                          -> second form got FormName_1 so the name was not found in security list
                          -> solution: create the form name into mConfigName from owner's ClassName
| 03.08.2001  1.03  SDo | Bug in TMMSecurityControl.Create eliminated ( if aOwner <> NIL then begin )
| 10.04.2002  1.03  Wss | ReloadUserInfo changed to be public
| 19.09.2002  2.00  Lok | Umbau auf TAdoDBAccess.
|                       | TBaseMMSecurity und TMMSecurityDB zusammengefasst.
| 04.10.2002  2.01  SDo | Umbau von Aus- & Einlesen der Security Daten.
|                       | Die Security-Daten werden nun aus Tab. t_Security_Work
|                       | (mit neuer Struktur) gelesen & geschrieben und nicht mehr
|                       | aus Tab. t_Security.
|                       | Die Daten werden via einer SecurList (Record pSecurity)
|                       | in die vorhandene mSecurityList eingetragen. Siehe
|                       | -> procedure LoadSecurityInfo() & function SaveSecurityInfo()
|                       | Neue procedure TMMSecurityDB.AddFormCaption()
|                       | Neues property TMMSecurityControl.FormCaption
|                       | Neues property TMMSecurityControl.ControllsHasChanged()
|                       |       -> Zeigt, ob sich die Controllist geaendert hat
| 05.11.2002  2.01  SDo | Error in Proc. SaveSecurityInfo behoben (-> if FindFirst then xRec.SecurityGroups ...)
| 22.11.2002  2.01  Wss | Konfiguration nur f�r die MM Benutzer MMSupp und MMAdmin zulassen
| 28.11.2002  2.01  Wss | Diverse Korrekturen
| 22.12.2003  2.01  Wss | BugFix bei leerem SecurityName Property
|=========================================================================================*}
unit MMSecurity;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, mmList,
  Db, Inifiles, ActnList, AdoDBAccess, MMSecurityConfig;

type
  PSecurity = ^TSecurityRec;
  TSecurityRec = record
    Appl: string; // Programm
    ApplForm: string; // Form-Name
    ApplFunction: string; // Action-Name
    FormCaption: string; // Caption einer Form
    Info: string; // Beschreibung der Funktion
    SecurityGroups: string; // Gruppen mit Serurity Levels
  end;

  TMMSecurityDB = class(TComponent)
  private
    fCheckApplStart: Boolean;
    fUserGroups: string;
    fActive: Boolean;
    fApplName: string;
    mLogonUser: string;
    mDefaultUser: string;
    function GetControlledForms: string;
    function GetControlledGroups: string;
    procedure SetControlledForms(const Value: string);
    procedure SetControlledGroups(const Value: string);
  protected
    mSecurityList: TStringList;
    function GetCurrentUser(aIndex: Integer): string;
    procedure Loaded; override;
  public
    // properties
    property ApplName: string read fApplName write fApplName;
    property ControlledForms: string read GetControlledForms write SetControlledForms;
    property ControlledGroups: string read GetControlledGroups write SetControlledGroups;
    property UserGroups: string read fUserGroups write fUserGroups;

    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure AddForm(aFormName, aFormCaption: string; aControlList: TControlList);

    procedure AddGroup(aName: string);
    procedure DelForm(aFormName: string);

    function GetForm(aFormName: string; aControlList: TControlList): Boolean;
    procedure LoadAppl(aName: string);
    procedure ReloadUserInfo;
    procedure SaveAppl(aName: string);

    procedure LoadSecurityInfo; virtual;
    function SaveSecurityInfo: Boolean; virtual;
  published
    property Active: Boolean read fActive write fActive stored True;
    property CheckApplStart: Boolean read fCheckApplStart write fCheckApplStart;
  end;
  //----------------------------------------------------------------------------
  // this class is for the property editor only
  TMMSecurityNamesList = class(TmmList)
  private
  public
    function FindMMSecurity(const aName: string): TMMSecurityDB;
    procedure Remove(aItem: Pointer);
  end;
  //----------------------------------------------------------------------------
  TMMSecurityControl = class(TComponent)
  private
    fActive: Boolean;
    fMMSecurityName: string;
    fOnAfterSecurityCheck: TNotifyEvent;
    mConfigName: string;
    mControlList: TControlList;
    mMMSecurityInfo: TMMSecurityDB;
    mOwnerOnShow: TNotifyEvent;
    mPrepared: Boolean;
    mUserGroups: string;
    fFormCaption: string;
    mControllsHasChanged: Boolean;

    procedure CheckSecurityStates;
    function GetComment(aIndex: Integer): string;
    function GetCurrentUser(aIndex: Integer): string;
    function GetName(aIndex: Integer): string;
    function GetSecurityState(aCompName: string): TSecurityState;
    procedure SetMMSecurityName(const Value: string);
    procedure SetActive(const Value: Boolean);
    function Prepared: Boolean;
  protected
    procedure Notification(aComponent: TComponent; Operation: TOperation); override;
    procedure OwnerOnShow(Sender: TObject);
    function SecurityDB: TMMSecurityDB;
    procedure SetValidGroups(aGroups: string);
  public
    // Property
    property Comment[aIndex: Integer]: string read GetComment;
    property CurrentMMUser: string index 0 read GetCurrentUser;
    property CurrentNTUser: string index 1 read GetCurrentUser;
    property Name[aIndex: Integer]: string read GetName;

    function CanEnabled(aComponent: TComponent): Boolean;
    function CanVisible(aComponent: TComponent): Boolean;
    procedure Configure;
    function Count: Integer;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function GetGroupState(aName, aGroup: string): TSecurityState;

    procedure LoadForm(aFormName: string);
    procedure Refresh;
    procedure SaveForm(aFormName: string);
    procedure SetGroupState(aName, aGroup: string; aState: TSecurityState);
    property ControllsHasChanged: Boolean read mControllsHasChanged;

  published
    property FormCaption: string read fFormCaption write fFormCaption; //SDO
    property Active: Boolean read fActive write SetActive;
    property MMSecurityName: string read fMMSecurityName write SetMMSecurityName;
    property OnAfterSecurityCheck: TNotifyEvent read fOnAfterSecurityCheck write fOnAfterSecurityCheck;
  end;
  //----------------------------------------------------------------------------

//procedure Register;

var
  gMMSecurityNames: TMMSecurityNamesList;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  ComCtrls, TypInfo, LoepfeGlobal, IvDictio, mmRegistry;

const
  cSectionGroups = '[ControlledGroups]';
  cSectionForms = '[ControlledForms]';
  cSectionFormCaptions = '[ControlledFormCaptions]';

  // messages
  cMsgNoConfigureAvailable = '(*)Konfiguration ist in diesem Programm nicht moeglich.'; // ivlm
  cMsgApplDenied = '(*)Sie haben keine Erlaubnis dieses Programm zu starten. '#10'Wenden Sie sich an den Systemverantwortlichen.'; // ivlm
  // queries

  //Neu 03.10.02
  cSQLSelect = 'select * from t_security_work where c_appl = :ApplName order by c_form';
  cSQLDeleteAppl = 'delete from t_security_work where c_appl = :ApplName';

  cSQLInsert = 'insert into t_security_work (c_Appl, c_Form, c_Function, c_Caption, c_Info, c_Security) ' +
    'values (:ApplName, :Form, :Function, :Caption, :Info, :Security)';

  cSQLGetSecur = 'select c_Security from t_security_work where c_Appl = :ApplName and  c_Form= :Form and  c_Function= :Func ';
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TMMSecurityDB
//------------------------------------------------------------------------------
procedure TMMSecurityDB.AddForm(aFormName, aFormCaption: string; aControlList: TControlList);
var
  i: Integer;
begin
  DelForm(aFormName);
  // add entry in ControlledForms
  with TStringList.Create do try
    Sorted := True;
    Duplicates := dupIgnore;
    CommaText := ControlledForms;
    Add(aFormName);
    ControlledForms := CommaText;
  finally
    Free;
  end;
  // add section block for current form
  // [aForm]
  // 1
  // FormCaption
  // Control1 ## Control1 comment @@ Grouplist
  mSecurityList.Add(GetSectionString(aFormName));
  mSecurityList.Add(IntToStr(aControlList.Count));
  mSecurityList.Add(aFormCaption);
  for i := 0 to aControlList.Count - 1 do
    mSecurityList.Add(aControlList.GetControlGroupString(i));
end;
//------------------------------------------------------------------------------
procedure TMMSecurityDB.AddGroup(aName: string);
begin
  with TStringList.Create do try
    Sorted := True;
    Duplicates := dupIgnore;
    CommaText := ControlledGroups;
    Add(aName);
    ControlledGroups := CommaText;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
constructor TMMSecurityDB.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fApplName := '';
  fActive := False;
  fCheckApplStart := False;
  mSecurityList := TStringList.Create;

  // fill in global security list to provide names for TMMSecurityControl property page
  gMMSecurityNames.Add(Self);
  ReloadUserInfo;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityDB.DelForm(aFormName: string);
var
  xIndex: Integer;
  i: Integer;
  xNrOfControls: Integer;
  xStrList: TStringList;
begin
  // [aForm]      <-- xIndex
  // 1
  // FormCaption
  // Control1 ## Control1 comment @@ Grouplist
  xIndex := mSecurityList.IndexOf(GetSectionString(aFormName));
  // current form is in SecurityList
  if xIndex >= 0 then begin
    // remove entry in [ControlledForms]
    xStrList := TStringList.Create;
    try
      xStrList.Sorted := True;
      xStrList.CommaText := ControlledForms;
      if xStrList.Find(aFormName, i) then begin
        xStrList.Delete(i);
      end;
      ControlledForms := xStrList.CommaText;
    finally
      xStrList.Free;
    end;
    // remove section item on beginning of list [FormName]
    mSecurityList.Delete(xIndex);
    // delete number of controls
    xNrOfControls := StrToIntDef(mSecurityList.Strings[xIndex], 0);
    mSecurityList.Delete(xIndex);
    // remove FormCaption
    mSecurityList.Delete(xIndex);

    // delete section of current form
    for i := 1 to xNrOfControls do try
      mSecurityList.Delete(xIndex);
    except
    end;
  end;
end;
//------------------------------------------------------------------------------
destructor TMMSecurityDB.Destroy;
begin
  gMMSecurityNames.Remove(Self);
  mSecurityList.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMMSecurityDB.GetControlledForms: string;
var
  xIndex: Integer;
begin
  Result := '';
  xIndex := mSecurityList.IndexOf(cSectionForms);
  if xIndex >= 0 then
    Result := mSecurityList.Strings[xIndex + 1];
end;
//------------------------------------------------------------------------------
function TMMSecurityDB.GetControlledGroups: string;
var
  xIndex: Integer;
begin
  Result := '';
  xIndex := mSecurityList.IndexOf(cSectionGroups);
  if xIndex >= 0 then
    Result := mSecurityList.Strings[xIndex + 1];
end;
//------------------------------------------------------------------------------
function TMMSecurityDB.GetCurrentUser(aIndex: Integer): string;
begin
  if aIndex = 0 then
    Result := mLogonUser
  else
    Result := mDefaultUser;
end;
//------------------------------------------------------------------------------
function TMMSecurityDB.GetForm(aFormName: string; aControlList: TControlList): Boolean;
var
  i, xIndex: Integer;
  xNrOfComp: Integer;
begin
  Result := False;
  // first clear previous stored information in destination list
  aControlList.Clear;
  // are there some security info for this form? Check for block [aFormName].
  // [aForm]    <<--
  // 1
  // FormCaption
  // Control1 ## Control1 comment @@ Grouplist
  xIndex := mSecurityList.IndexOf(GetSectionString(aFormName));
  if xIndex >= 0 then begin
    // [aForm]      <-- xIndex
    // 1            <-- inc(xIndex)
    // FormCaption
    // Control1 ## Control1 comment @@ Grouplist
    inc(xIndex); // seek to NrOfComponents and read
    try
      try
        // get number of components and copy all lines
        xNrOfComp := StrToInt(mSecurityList.Strings[xIndex]);
      except
        Exit; // there is something wrong in the format of text block
      end;
      // [aForm]
      // 1
      // FormCaption
      // Control1 ## Control1 comment @@ Grouplist   <---- inc(xIndex, 2)
      inc(xIndex, 2); // seek to first component list
      for i := 1 to xNrOfComp do begin
        aControlList.AddControlGroupString(mSecurityList.Strings[xIndex]);
        inc(xIndex);
      end;

      // succeeded only on end of filling
      Result := True;
    except
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityDB.LoadAppl(aName: string);
var
  xUserGroup: TStringList;
  xApplGroup: TStringList;
  i: Integer;
  xOK: Boolean;
begin
  if aName <> '' then begin
    fApplName := aName;
    if not (csDesigning in ComponentState) then begin
      LoadSecurityInfo;
      if CheckApplStart then begin
        // this are the groups the user belongs to
        xUserGroup := TStringList.Create;
        xUserGroup.CommaText := UpperCase(UserGroups);
        // this are the controlled group
        xApplGroup := TStringList.Create;
        xApplGroup.CommaText := UpperCase(ControlledGroups);

        xOK := False;
        // if current user has at least one enabling group, appl doesn't close
        for i := 0 to xUserGroup.Count - 1 do begin
          xOK := (xApplGroup.IndexOf(xUserGroup.Strings[i]) >= 0);
          if xOK then
            break;
        end;

        if not xOK then begin
          MlMessageDlg(cMsgApplDenied, mtInformation, [mbOK], 0);
          Application.ShowMainForm := False;
          Application.Terminate;
        end;
      end; // if CheckApplStart
    end; // if designing
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityDB.Loaded;
begin
  inherited Loaded;

  if fActive then begin
    LoadAppl(gApplicationName);
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityDB.ReloadUserInfo;
begin
  fUserGroups := '';
  with TmmRegistry.Create do try
    Rootkey := cRegCU;
    if OpenKeyReadOnly(cRegMMSecurityPath) then begin
      mDefaultUser := ReadString(cRegDefUserName);
      if ValueExists(cRegLogonUserName) then begin
        mLogonUser := ReadString(cRegLogonUserName);
        fUserGroups := ReadString(cRegLogonUserGroups)
      end
      else begin
        mLogonUser := mDefaultUser;
        fUserGroups := ReadString(cRegDefUserGroups);
      end;
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityDB.SaveAppl(aName: string);
begin
  if CompareText(aName, fApplName) = 0 then begin
    SaveSecurityInfo;
  end
  else
    ShowMessageFmt('Profilname ist nicht gleich: %s <> %s', [fApplName, aName]);
end;
//------------------------------------------------------------------------------
procedure TMMSecurityDB.SetControlledForms(const Value: string);
var
  xIndex: Integer;
begin
  xIndex := mSecurityList.IndexOf(cSectionForms);
  if xIndex < 0 then begin
    mSecurityList.Add(cSectionForms);
    mSecurityList.Add(Value);
  end
  else begin
    inc(xIndex); // seek to items where the information is
    mSecurityList.Strings[xIndex] := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityDB.SetControlledGroups(const Value: string);
var
  xIndex: Integer;
  xFormIndex: Integer;
  xFormList: TStringList;
  xFormSC: TMMSecurityControl;
begin
  // write ControlledForms strings
  xIndex := mSecurityList.IndexOf(cSectionGroups);
  if xIndex >= 0 then begin
    inc(xIndex); // seek to items where the information is
    mSecurityList.Strings[xIndex] := Value;
  end
  else begin
    mSecurityList.Add(cSectionGroups);
    mSecurityList.Add(Value);
  end;

  // go through all forms and check for controlled groups
  xFormSC := TMMSecurityControl.Create(nil);
  xFormSC.MMSecurityName := Self.Name;

  xFormList := TStringList.Create;
  xFormList.CommaText := ControlledForms;
  for xFormIndex := 0 to xFormList.Count - 1 do begin
    xFormSC.LoadForm(xFormList.Strings[xFormIndex]);
    xFormSC.SetValidGroups(Value);
    xFormSC.SaveForm(xFormList.Strings[xFormIndex]);
  end;

end;
//------------------------------------------------------------------------------
procedure TMMSecurityDB.LoadSecurityInfo;
var
//  xForms: TStringList;
  xControlList: TControlList;
  xFormName: string;
  xFormCaption: string;
  xStr: string;
begin
  mSecurityList.Clear;

//  xForms := TStringList.Create;
//  xForms.Duplicates := dupIgnore;

  xFormName := '';
  xFormCaption := '';
  xControlList := TControlList.Create;
  xControlList.Sorted := False;
  with TAdoDBAccess.Create(1, False) do try
    if Init then begin
      with Query[cPrimaryQuery] do try
        SQL.Text := cSQLSelect;
        ParamByName('ApplName').AsString := ApplName;
        Open;
        while not EOF do begin
          xStr := FieldByName('c_Form').asString;
          // wenn neues Form festgestellt wird, dann vorhandenes erst speichern
          if xStr <> xFormName then begin
            // aber nur speichern, wenn aus etwas vorhanden ist
            if xControlList.Count > 0 then begin
              AddForm(xFormName, xFormCaption, xControlList);
              CodeSite.SendStringList('AddForm: ' + xFormName, xControlList);
            end;
            xControlList.Clear;
            // Titel des Form merken
            xFormCaption := FieldByName('c_Caption').AsString;
            // Merker
            xFormName := xStr;
          end;

          // Aktueller Record auslesen und in Liste abf�llen
          xStr := Format('%s ## %s @@ %s', [FieldByName('c_Function').AsString,
            FieldByName('c_Info').AsString,
              FieldByName('c_Security').asString]);
          xControlList.AddControlGroupString(xStr);
//          xControlList.Add(Format('%s ## %s @@ %s', [FieldByName('c_Function').AsString,
//                                                     FieldByName('c_Info').AsString,
//                                                     FieldByName('c_Security').asString]));

          Next;
        end;
        // am Schluss nochmals speichern
        if xControlList.Count > 0 then begin
          AddForm(xFormName, xFormCaption, xControlList);
          CodeSite.SendStringList('AddForm: ' + xFormName, xControlList);
        end;
      except
        on e: Exception do
          CodeSite.SendError('MMSecurityDB.LoadSecurityInfo: ' + e.Message);
      end;
    end; // if Init then begin
  finally
    Free;
    xControlList.Free;
  end;
end;
//------------------------------------------------------------------------------
(*procedure TMMSecurityDB.Notification(aComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) and (aComponent = fDatabase) then
    fDatabase := Nil;
end;*)
//------------------------------------------------------------------------------
function TMMSecurityDB.SaveSecurityInfo: Boolean;
var
  xForm, xCaption: string;
  x, n: Integer;
  xRec: PSecurity;
  xSecurList: TmmList;
  xForms: TStringList;
  xControlList: TControlList;
begin
  Result := False;

  xControlList := TControlList.Create;
  xSecurList := TmmList.Create;
  xForms := TStringList.Create;

  //Daten fuer zur Speicherung aufbereiten
  //-> Daten werden in eine Liste geschrieben
  //+++++++++++++++++++++++++++++++++++++++++

  //Alle kontrollierte Forms ermitteln
  xForms.CommaText := ControlledForms;
  for x := 0 to xForms.Count - 1 do begin
    xForm := xForms.Strings[x];

    // Form-Caption ermitteln
    //[ControlledForms]
    //Forms
    //[Form]       <-- Index n
    //DataCount
    //FormCaption  <-- inc(x, 2)
    //Data         <-- GetForm()
    n := mSecurityList.IndexOf(GetSectionString(xForm));
    inc(n, 2);
    xCaption := mSecurityList.Strings[n];
    if xCaption = '' then
      xCaption := xForm;

    //Form-Daten ermitteln
    GetForm(xForm, xControlList);
    // bei 1 starten, da hier FormCaption nicht ben�tigt wird
    for n := 0 to xControlList.Count - 1 do begin
      new(xRec);

      xRec.Appl         := gApplicationName;
      xRec.ApplForm     := xForm;
      xRec.FormCaption  := xCaption;
      xRec.ApplFunction := xControlList.Name[n];
      xRec.Info         := xControlList.Comment[n];

      xSecurList.Add(xRec);
    end;
  end;
  xForms.Free;
  xControlList.Free;

  // 1. DB-Daten der aktuellen Appl. loeschen
  // 2. xSecurList in DB neu einfuegen
  with TAdoDBAccess.Create(1, False) do
  try
    if Init then begin
      with Query[cPrimaryQuery] do begin

        // Falls dieses Security Item schon gibt, dann rette die Securityeinstellungen
        SQL.Text := cSQLGetSecur;
        for x := 0 to xSecurList.Count - 1 do begin
          xRec := xSecurList.Items[x];
          Close;
          ParamByName('ApplName').AsString := xRec.Appl;
          ParamByName('Form').AsString := xRec.ApplForm;
          ParamByName('Func').AsString := xRec.ApplFunction;
          Open;
          if FindFirst then
            xRec.SecurityGroups := FieldByName('c_Security').Value;
        end;

        Close;

        //Alle DB-Daten der aktuellen Appl. loeschen
        SQL.Text := cSQLDeleteAppl;
        ParamByName('ApplName').AsString := gApplicationName;
        try
          ExecSQL;
          Close;

          //xSecurList in DB neu einfuegen
          SQL.Text := cSQLInsert;
          for x := 0 to xSecurList.Count - 1 do begin
            xRec := xSecurList.Items[x];

            ParamByName('ApplName').AsString := xRec.Appl;
            ParamByName('Form').AsString     := xRec.ApplForm;
            ParamByName('Function').AsString := xRec.ApplFunction;
            ParamByName('Caption').AsString  := xRec.FormCaption;
            ParamByName('Info').AsString     := xRec.Info;
            ParamByName('Security').AsString := xRec.SecurityGroups;
            ExecSQL;
          end;
          Close;
        except
        end;
        Result := True;
      end; // with Query[cPrimaryQuery] do begin
    end; // if Init then begin
  finally
    Free;
    for x:=0 to xSecurList.Count-1 do begin
      Dispose(PSecurity(xSecurList.Items[x]));
    end;
    xSecurList.Free;
  end; // with TAdoDBAccess.Create(1,false) do
end; // function TMMSecurityDB.SaveSecurityInfo: Boolean;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TMMSecurityNamesList }
//------------------------------------------------------------------------------
function TMMSecurityNamesList.FindMMSecurity(const aName: string): TMMSecurityDB;
var
  i: Integer;
begin
  if Count > 0 then
    Result := Items[0] // set default object
  else
    Result := nil;

  for i := 0 to Count - 1 do begin
    if TControl(Items[i]).Name = aName then begin
      Result := TMMSecurityDB(Items[i]);
      Break;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityNamesList.Remove(aItem: Pointer);
var
  i: Integer;
begin
  if Assigned(aItem) then
    for i := 0 to Count - 1 do
      if Items[i] = aItem then begin
        Delete(i);
        Break;
      end;
end;

//------------------------------------------------------------------------------
// TMMSecurityControl }
//------------------------------------------------------------------------------
function TMMSecurityControl.CanEnabled(aComponent: TComponent): Boolean;
begin
  Result := (GetSecurityState(aComponent.Name) = ssEnabled);
end;
//------------------------------------------------------------------------------
function TMMSecurityControl.CanVisible(aComponent: TComponent): Boolean;
begin
  Result := (GetSecurityState(aComponent.Name) in [ssVisible, ssEnabled]);
end;
//------------------------------------------------------------------------------
procedure TMMSecurityControl.CheckSecurityStates;
var
  i: Integer;
  xState: TSecurityState;
  xComponent: TComponent;
  //--------------------------------------------------------------------------
  procedure SetComponentState(aComponent: TComponent; aVisible, aEnabled: string; aState: TSecurityState);
  var
    xVisible: Boolean;
    xEnabled: Boolean;
  begin
    xEnabled := (aState = ssEnabled);
    xVisible := (aState in [ssEnabled, ssVisible]);
    try
      if aEnabled <> '' then
        setOrdProp(aComponent, aEnabled, Integer(xEnabled));
      if aVisible <> '' then
        setOrdProp(aComponent, aVisible, Integer(xVisible));
    except
    end;
  end;
  //--------------------------------------------------------------------------
begin
  for i := 0 to mControlList.Count - 1 do begin
    // does the form contain such a component ?
    xComponent := Owner.FindComponent(mControlList.Name[i]);
    if Assigned(xComponent) then begin
      xState := GetSecurityState(xComponent.Name);
      if xComponent is TTabSheet then
        SetComponentState(xComponent, 'TabVisible', 'Enabled', xState)
      else if xComponent is TField then
        SetComponentState(xComponent, 'Visible', '', xState)
      else
        SetComponentState(xComponent, 'Visible', 'Enabled', xState)
    end; // if Assigned
  end; // for
  if Assigned(fOnAfterSecurityCheck) then
    fOnAfterSecurityCheck(Self);
end;
//------------------------------------------------------------------------------
procedure TMMSecurityControl.Configure;
begin
  EnterMethod('Configure');
  // Konfiguration nur f�r diese Beiden MM Benutzer zulassen
  if (CompareText(CurrentMMUser, 'MMSupp') = 0)  or
     (CompareText(CurrentMMUser, 'MMAdmin') = 0) or
     (DebugHook <> 0) then begin

    with TfrmSelectComponents.Create(Owner) do try
      ComponentList := mControlList;
      if ShowModal = mrOK then begin

        mControllsHasChanged := not mControlList.Equals(ComponentList);
        mControlList.Assign(ComponentList);
        CodeSite.SendStringList('mControlList', mControlList);
        // save new component list in SecurityInfoList
        SaveForm(mConfigName);
        //SaveForm(mConfigName, fFormCaption);
      end;
    finally
      Free;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMMSecurityControl.Count: Integer;
begin
  Result := mControlList.Count;
end;
//------------------------------------------------------------------------------
constructor TMMSecurityControl.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fFormCaption := '';

  mControllsHasChanged := False;
  mControlList := TControlList.Create;
  mMMSecurityInfo := nil;
  mOwnerOnShow := nil;
  mPrepared := False;
  mUserGroups := '';

  if aOwner <> nil then begin //SDo
    mConfigName := Owner.ClassName;
     // remove leading T from class name
    Delete(mConfigName, 1, 1);
  end;
end;
//------------------------------------------------------------------------------
destructor TMMSecurityControl.Destroy;
begin
  mControlList.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMMSecurityControl.GetComment(aIndex: Integer): string;
begin
  Result := mControlList.Comment[aIndex];
end;
//------------------------------------------------------------------------------
function TMMSecurityControl.GetCurrentUser(aIndex: Integer): string;
begin
  Result := SecurityDB.GetCurrentUser(aIndex);
//  Result := mMMSecurityInfo.GetCurrentUser(aIndex);
end;
//------------------------------------------------------------------------------
function TMMSecurityControl.GetGroupState(aName, aGroup: string): TSecurityState;
begin
  Result := mControlList.GetGroupState(aName, aGroup);
end;
//------------------------------------------------------------------------------
function TMMSecurityControl.GetName(aIndex: Integer): string;
begin
  Result := mControlList.Name[aIndex];
end;
//------------------------------------------------------------------------------
function TMMSecurityControl.GetSecurityState(aCompName: string): TSecurityState;
var
  i: Integer;
  xCompIndex: Integer;
  xTempState: TSecurityState;
  xCompGroupList: TGroupList;
  xUserGroupList: TStringList;
  xIndex: Integer;
begin
  Result := ssEnabled; // default state if component isn't found in controlled list
  if Prepared then begin
    if (aCompName <> '') then begin
      // do we have this component to control ?
      if mControlList.Find(aCompName, xCompIndex) then begin
        // ... Yes, we do
        Result := ssHide; // default state is lowest state
        xUserGroupList := TStringList.Create;
        try
          xUserGroupList.CommaText := mUserGroups;
          // get GroupList of current control
          xCompGroupList := TGroupList(mControlList.Objects[xCompIndex]);
          // count through all controlled groups of this component
          if Assigned(xCompGroupList) then
            for i := 0 to xUserGroupList.Count - 1 do begin
              if xCompGroupList.Find(xUserGroupList.Strings[i], xIndex) then begin
                xTempState := xCompGroupList.State[xIndex];
                // do we have a new higher state?
                if xTempState > Result then begin
                  Result := xTempState;
                  if Result = ssEnabled then
                    Break; // jump out of loop on highest state
                end;
              end;
            end; // for i
        finally
          xUserGroupList.Free;
        end;
      end; // if mControlList.Find
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecuritycontrol.LoadForm(aFormName: string);
begin
  SecurityDB.GetForm(aFormName, mControlList);
//  if Assigned(mMMSecurityInfo) then
//    mMMSecurityInfo.GetForm(aFormName, mControlList);
end;
//------------------------------------------------------------------------------
procedure TMMSecurityControl.Notification(aComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);

  if (Operation = opRemove) and (aComponent = mMMSecurityInfo) then begin
    fMMSecurityName := '';
    mMMSecurityInfo := nil;
    mUserGroups := '';
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityControl.OwnerOnShow(Sender: TObject);
begin
  // if this methode is called the Active property is true
  if Prepared then
    CheckSecurityStates;

  // after all call original OnShow methode
  if Assigned(mOwnerOnShow) then
    mOwnerOnShow(Sender);
end;
//------------------------------------------------------------------------------
function TMMSecurityControl.Prepared: Boolean;
begin
  if not mPrepared then begin
    if fActive then begin
      // get group information for current user
      mUserGroups := UpperCase(SecurityDB.UserGroups);
      // get security information for this form
      LoadForm(mConfigName);
      mPrepared := True;
    end;

//    if not Assigned(mMMSecurityInfo) then
//      mMMSecurityInfo := gMMSecurityNames.FindMMSecurity(fMMSecurityName);
//
//    if Assigned(mMMSecurityInfo) and fActive then begin
//      // get group information for current user
//      mUserGroups := UpperCase(mMMSecurityInfo.UserGroups);
//      // get security information for this form
//      LoadForm(mConfigName);
//      mPrepared := True;
//    end;
  end;
  Result := mPrepared;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityControl.Refresh;
begin
  if Prepared then begin
    // get group information for current user
    SecurityDB.ReloadUserInfo;
    // get group information for current user
    mUserGroups := UpperCase(SecurityDB.UserGroups);
    CheckSecurityStates;
  end;
//  if Prepared then begin
//    // get group information for current user
//    mMMSecurityInfo.ReloadUserInfo;
//    // get group information for current user
//    mUserGroups := UpperCase(mMMSecurityInfo.UserGroups);
//    CheckSecurityStates;
//  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecuritycontrol.SaveForm(aFormName: string);
begin
  // save new component list in SecurityInfoList
  SecurityDB.AddForm(aFormName, fFormCaption, mControlList);
  if fActive then
    SecurityDB.SaveSecurityInfo;
    
//  if Assigned(mMMSecurityInfo) then begin
//    mMMSecurityInfo.AddForm(aFormName, fFormCaption, mControlList);
//    if fActive then
//      mMMSecurityInfo.SaveSecurityInfo;
//  end;
end;
//------------------------------------------------------------------------------
function TMMSecurityControl.SecurityDB: TMMSecurityDB;
begin
  if not Assigned(mMMSecurityInfo) then
    mMMSecurityInfo := gMMSecurityNames.FindMMSecurity(fMMSecurityName);

  Result := mMMSecurityInfo;
  if not Assigned(Result) then
    Abort;
end;

procedure TMMSecurityControl.SetActive(const Value: Boolean);
begin
  if fActive <> Value then begin
    fActive := Value;
    // replace original OnShow event with new one to have possibility to check security
    if not (csDesigning in ComponentState) then
      if fActive then begin
        mOwnerOnShow := (Owner as TForm).OnShow;
        (Owner as TForm).OnShow := OwnerOnShow;
      end
      else begin
        if Assigned(mOwnerOnShow) then
          (Owner as TForm).OnShow := mOwnerOnShow;
        mOwnerOnShow := nil;
      end;
  end;

  if fActive and Assigned(Owner) then begin
    if Owner.InheritsFrom(TForm) and (fFormCaption = '') and (csDesigning in ComponentState) then begin
      fFormCaption := TForm(Owner).Caption;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityControl.SetGroupState(aName, aGroup: string; aState: TSecurityState);
begin
  mControlList.SetGroupState(aName, aGroup, aState);
end;
//------------------------------------------------------------------------------
procedure TMMSecurityControl.SetMMSecurityName(const Value: string);
begin
  if Value <> fMMSecurityName then begin
    fMMSecurityName := Value;
    mMMSecurityInfo := Nil;
    // get the MMSecurityInfo object by name from the global list
    // so we get informed if the component is removed in design time
    SecurityDB.FreeNotification(Self);
  end;
//  if Value <> fMMSecurityName then begin
//    fMMSecurityName := Value;
//    // get the MMSecurityInfo object by name from the global list
//    mMMSecurityInfo := gMMSecurityNames.FindMMSecurity(fMMSecurityName);
//    if Assigned(mMMSecurityInfo) then
//      // so we get informed if the component is removed in design time
//      mMMSecurityInfo.FreeNotification(Self);
//  end;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityControl.SetValidGroups(aGroups: string);
var
  i: Integer;
begin
  for i := 0 to mControlList.Count - 1 do begin
    mControlList.GroupList[i].SetValidGroups(aGroups);
  end;
end;
//------------------------------------------------------------------------------

initialization
  gMMSecurityNames := TMMSecurityNamesList.Create;
finalization
  gMMSecurityNames.Free;
end.

