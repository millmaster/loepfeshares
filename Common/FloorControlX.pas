(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: FloorControlX.pas
| Projectpart...: MillMaster NT
| Subpart.......: Floor
| Process(es)...: -
| Description...: Start component for building controls for Floor's Detail Report
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 19.11.1999  1.00  Wss | Initial release
| 07.03.2002  1.00  Wss | Anpassungen fuer QMatrix Autosize
| 14.03.2002  1.00  Wss | In Resize: Auskommentiert, da beim Print die Groesse nicht mehr stimmte
                          -> funktioniert aber weiterhin einwandfrei
| 10.05.2005  1.00  Wss | Diverse Anpassungen infolge �nderungen an den Komponenten QualityMatrix und XMLSettings
| 15.06.2005  1.00  Wss | - Namensverwechslung von OLE.UpdateControl mit FlooControlX verhindern -> UpdateVCLControl
                          - WarningMsg hinzugef�gt
| 26.10.2005  1.00  Wss | PrepareControlSize(): - mImage Gr�sse per SetBounds() statt Properties
                                                - Dormant aufrufen nach Gr�ssen�nderung, da oft auf Ausdruck nichts sichtbar war
|=========================================================================================*)
unit FloorControlX;

interface

uses
  Dialogs, Classes, ComObj, Controls, Graphics, Messages, Windows, mmImage, mmLabel;

type
  //---------------------------------------------------------------------------
  TPrepareControlSizeEvent = procedure(Sender: TObject; var aWidth, aHeight: Integer) of object;

  TFloorControlX = class(TCustomControl)
  private
    fDrawTest: Boolean;
    fLoepfeControl: TCustomControl;
    fOnPrepareControlSize: TPrepareControlSizeEvent;
    fWarningMsg: string;
    mImage: TmmImage;
    procedure PrepareControlSize;
    procedure SetLoepfeControl(const Value: TCustomControl);
    procedure SetWarningMsg(const Value: string);
  protected
    procedure ReadState(Reader: TReader); override;
    procedure Resize; override;
    procedure WriteState(Writer: TWriter); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
    procedure UpdateVCLControl;
    property DrawTest: Boolean read fDrawTest write fDrawTest default False;
    property LoepfeControl: TCustomControl read fLoepfeControl write SetLoepfeControl;
    property OnPrepareControlSize: TPrepareControlSizeEvent read fOnPrepareControlSize write fOnPrepareControlSize;
    property WarningMsg: string read fWarningMsg write SetWarningMsg;
  end;
  
  //---------------------------------------------------------------------------
  TTestControl = class(TCustomControl)
  private
    fPenColor: TColor;
  public
    constructor Create(aOwner: TComponent); override;
    procedure Paint; override;
  published
    property PenColor: TColor read fPenColor write fPenColor;
  end;

  //---------------------------------------------------------------------------

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  ActiveX, Forms, SysUtils, mmRegistry, mmPaintBox;

procedure Register;
begin
  RegisterComponents('LOEPFE', [TFloorControlX]);
end;

//******************************************************************************
// TFloorControlX
//******************************************************************************
//:---------------------------------------------------------------------------
//:--- Class: TFloorControlX
//:---------------------------------------------------------------------------
constructor TFloorControlX.Create(aOwner: TComponent);
begin
EnterMethod('TFloorControlX.Create');
  inherited Create(aOwner);

  fDrawTest      := False;
  fLoepfeControl := Nil;
  fWarningMsg    := '';

  mImage         := TmmImage.Create(Self);
  with mImage do begin
    AutoSize := False;
    Enabled  := False;
    Left     := 1;
    Top      := 1;
    Stretch  := True;
    Parent   := Self;
    Transparent := False;
  end;
end;

//:---------------------------------------------------------------------------
destructor TFloorControlX.Destroy;
begin
EnterMethod('TFloorControlX.Destroy');
  FreeAndNil(fLoepfeControl);
  FreeAndNil(mImage);
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TFloorControlX.Paint;
begin
  if fDrawTest then begin
    with Canvas do begin
      Pen.Color := clBlack;
      Rectangle(0, 0, Width, Height);
      MoveTo(0,0); LineTo(Width, Height);
      MoveTo(0,Height); LineTo(Width, 0);
      TextOut((Width - TextWidth('Center')) div 2, (Height - TextHeight('C')) div 2, 'Center');

      TextOut((Width - TextWidth(IntToStr(Width))) div 2, 2, IntToStr(Width));
      TextOut(2, (Height - TextHeight(IntToStr(Height))) div 2, IntToStr(Height));
      if Assigned(Owner) then begin
        TextOut((Width - TextWidth(IntToStr(Width))) div 2, 20, IntToStr(TControl(Owner).Width));
        TextOut(2, ((Height - TextHeight(IntToStr(Height))) div 2) + 20, IntToStr(TControl(Owner).Height));
      end else
        TextOut((Width - TextWidth(IntToStr(Width))) div 2, 20, 'Owner = Nil');
    end;
  end else begin
    if not Assigned(fLoepfeControl) then
      Canvas.DrawFocusRect(ClientRect);
  end;
end;

//:---------------------------------------------------------------------------
procedure TFloorControlX.PrepareControlSize;
var
  xWidth, xHeight: Integer;
begin
EnterMethod('TFloorControlX.PrepareControlSize');
  // die VCL-Komponente aus dem sichtbaren Bereich schieben
  if Assigned(fLoepfeControl) then
    fLoepfeControl.Left := -fLoepfeControl.Width;
  // Das X-Control hat die vorgegebene Gr�sse des ActiveX
  // Das Image dahinter jedoch kann eine andere Gr�sse haben, welche nur die
  // abgeleitete ActiveX-Komponente wissen (XMLSettings, QualityMatrix)
  xWidth  := Width;
  xHeight := Height;
  if Assigned(fOnPrepareControlSize) then
    fOnPrepareControlSize(Self, xWidth, xHeight);

  mImage.SetBounds(1, 1, xWidth, xHeight);
  mImage.Picture.Bitmap.Dormant;
end;

//:---------------------------------------------------------------------------
procedure TFloorControlX.ReadState(Reader: TReader);
begin
EnterMethod('TFloorControlX.ReadState');
  inherited ReadState(Reader);
  // read properties for LoepfeControl
  if Assigned(fLoepfeControl) then
  try
    Reader.ReadComponent(fLoepfeControl);
  except
    on e:Exception do
      CodeSite.SendError('TFloorControlX.RadState: ' + e.Message);
  end;
end;

//:---------------------------------------------------------------------------
procedure TFloorControlX.Resize;
var
  xWidth, xHeight: Integer;
begin
EnterMethod('TFloorControlX.Resize');
  inherited Resize;
  CodeSite.SendFmtMsg('TFloorControlX: %d,%d,%d,%d', [Left, Top, Width, Height]);
  // Das X-Control hat die vorgegebene Gr�sse des ActiveX
  // Das Image dahinter jedoch kann eine andere Gr�sse haben, welche nur die
  // abgeleitete ActiveX-Komponente wissen (XMLSettings, QualityMatrix)
  if Assigned(fLoepfeControl) then
    PrepareControlSize;
  // Hier im Resize darf die VCL-Komponente nicht nochmals ins Image gezeichnet werden
  // da Resize ebenfalls bei einem Print aufgerufen wird aber die Gr�sse der VCL-Komponente
  // daf�r nicht mehr ver�ndert werden darf
end;

//:---------------------------------------------------------------------------
procedure TFloorControlX.SetLoepfeControl(const Value: TCustomControl);
begin
EnterMethod('TFloorControlX.SetLoepfeControl');
  if Value <> fLoepfeControl then
    fLoepfeControl := Value;
end;

procedure TFloorControlX.SetWarningMsg(const Value: string);
begin
  fWarningMsg := Value;
end;

//:---------------------------------------------------------------------------
procedure TFloorControlX.UpdateVCLControl;
var
  i: Integer;
  xRect: TRect;
begin
  if Assigned(fLoepfeControl) then begin
    PrepareControlSize;

    // Das Bitmap bekommtn die gleiche Gr�sse wie das VCL. Das Bitmap wird dann
    // gestreckt (verkleinert oder vergr�ssert) auf dem TImage angezeigt
    with mImage.Picture.Bitmap do begin
      Width  := fLoepfeControl.Width;
      Height := fLoepfeControl.Height;

      if fWarningMsg <> '' then begin
        xRect := Rect(0, 0, Width, Height);
        Canvas.Font.Size := 10;
        i     := DrawText(Canvas.Handle, PChar(fWarningMsg), -1, xRect, DT_WORDBREAK) + 4;
        // nun noch die Bitmapgr�sse etwas anpassen damit das Control (z.B. Matrix) wieder Platz hat
        Height := Height + i;
      end else
        i := 0;

      fLoepfeControl.PaintTo(Canvas.Handle, 0, i);
    end;
    mImage.Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TFloorControlX.WriteState(Writer: TWriter);
begin
EnterMethod('TFloorControlX.WriteState');
  inherited WriteState(Writer);

  PrepareControlSize;
  if Assigned(fLoepfeControl) then
  try
    Writer.WriteComponent(fLoepfeControl);
  except
  end;
end;

//******************************************************************************
// TTestControl
//******************************************************************************
//:---------------------------------------------------------------------------
//:--- Class: TTestControl
//:---------------------------------------------------------------------------
constructor TTestControl.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Width  := 60;
  Height := 60;
end;

//:---------------------------------------------------------------------------
procedure TTestControl.Paint;
begin
  EnterMethod('TTestControl.Paint');
    with Canvas do begin
      Pen.Color := fPenColor;
      Rectangle(0, 0, Width, Height);
      MoveTo(0,0); LineTo(Width, Height);
      MoveTo(0,Height); LineTo(Width, 0);
      TextOut((Width - TextWidth('Center')) div 2, (Height - TextHeight('C')) div 2, 'Center');
  
      TextOut((Width - TextWidth(IntToStr(Width))) div 2, 2, IntToStr(Width));
      TextOut(2, (Height - TextHeight(IntToStr(Height))) div 2, IntToStr(Height));
  
      TextOut((Width - TextWidth(IntToStr(Width))) div 2, 20, IntToStr(TControl(Owner).Width));
      TextOut(2, ((Height - TextHeight(IntToStr(Height))) div 2) + 20, IntToStr(TControl(Owner).Height));
    end;
end;

//------------------------------------------------------------------------------
end.

