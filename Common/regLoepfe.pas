(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: regNEW.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components in different pages
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  1.00  Wss | Initial Release
| 11.03.1999  1.01  Wss | Component TmmVersionInfo added
| 28.09.1999  1.02  SDo | Component TMMTimeEdit & TTrayIcon added
|=========================================================================================*)
unit regLoepfe;

interface

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  Classes, DsgnIntf, Controls,
  MMLogin, MMSecurity, TimeEdit, TrayIcon;
//------------------------------------------------------------------------------
type
  TMMSecurityNameProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;
//------------------------------------------------------------------------------
procedure Register;
begin
  RegisterComponents('LOEPFE',[
                          TMMLogin,
                          TMMSecurityDB,
                          TMMSecurityControl,
                          TMMTimeEdit,
                          TTrayIcon
                                ]);

  // property editor for TMMSecurityControl
  RegisterPropertyEditor(TypeInfo(String), TMMSecurityControl, 'MMSecurityName', TMMSecurityNameProperty);
end;

//------------------------------------------------------------------------------
// TMMSecurityNameProperty
//------------------------------------------------------------------------------
function TMMSecurityNameProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList, paRevertable];
end;
//------------------------------------------------------------------------------
procedure TMMSecurityNameProperty.GetValues(Proc: TGetStrProc);
var
  i: Integer;
begin
  with gMMSecurityNames do
    for i:=0 to Count-1 do
      Proc(TControl(Items[i]).Name);
end;
//------------------------------------------------------------------------------
end.

