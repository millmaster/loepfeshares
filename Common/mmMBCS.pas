(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmMBCS.pas
| Projectpart...: common
| Subpart.......: -
| Process(es)...: -
| Description...: substitute string functions in system.pas and sysutils.pas
| Info..........: -
| Develop.system: Compaq Deskpro Pentium II 300MHz, Windows 2000 SP2
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.02.2002  0.00  Lok | File created
|=========================================================================================*)

// see additional informations in the file 'MBCS Funktionen.doc'

unit mmMBCS;


// If Symbol 'MBCS' defined, the ANSI Functions will be compiled.
// Otherwise the ASCII Functions will be compiled.
{$define MBCS}


// if Symbol 'MBCSFunctionCallCount' defined, a log file will be written.
// The file is located in the systems TMP-Path and includes the count of
// functioncalls to the belonging functions. The Filename is 'MBCSFunctionCallCount.log'
//{$define MBCSFunctionCallCount}

interface
(*
Sysutils
AnsiStrLower converts all characters in a null-terminated string to lower case.
Note:	This function supports multi-byte character sets (MBCS).*)
function StrLower(Str: PChar): PChar;
(*
Sysutils
AnsiStrPos returns a pointer to the first occurrence of SubStr in Str.
Unlike the StrPos function, AnsiStrPos works with multi-byte characters sets (MBCS).*)
function StrPos(Str, SubStr: PChar): PChar;
(*
Sysutils
AnsiStrRScan gibt einen Zeiger auf das letzte Vorkommen eines bestimmten Zeichens in einem String zurueck.
function AnsiStrRScan(Str: PChar; Chr: Char): PChar;
Im Gegensatz zu StrRScan kann AnsiStrRScan auch mit Multibyte-Zeichensaetzen (MBCS) verwendet werden.*)
function StrRScan(Str: PChar; Chr: Char): PChar;
(*
Sysutils
AnsiLowerCase returns a string that is a copy of the given string converted to lower case.
The conversion uses the current Windows locale. This function supports multi-byte character sets (MBCS).*)
function LowerCase(const S: string): string;
(*
AnsiStrUpper converts all characters in a null-terminated string to upper case.
Note:	This function supports multi-byte character sets (MBCS).*)
function StrUpper(Str: PChar): PChar;
(*
Sysutils
AnsiUpperCase converts a string to upper case.
Note:	This function supports multi-byte character sets (MBCS).*)
function UpperCase(const S: string): string;
(*
Sysutils
AnsiPos locates the position of a sub-string within a string.
Note:	This function supports multi-byte character sets (MBCS).*)
function Pos(const Substr, S: string): Integer;
(*
Sysutils
AnsiQuotedStr returns the quoted version of a string.
Note:	This function supports multi-byte character sets (MBCS).*)
//function AnsiQuotedStr(const S: string; Quote: Char): string;
function QuotedStr(const S: string): string;

(*
Sysutils
AnsiStrComp compares null-terminated character strings case sensitively.
Note:	This function supports multi-byte character sets (MBCS).*)
function StrComp(S1, S2: PChar): Integer;
(*
Sysutils
AnsiStrIComp compares null terminated character strings case insensitively.
Note:	This function supports multi-byte character sets (MBCS).*)
function StrIComp(S1, S2: PChar): Integer;
(*
Sysutils
AnsiStrLComp compares the first MaxLen bytes of two null-terminated strings, case-sensitively.
Note:	This function supports multi-byte character sets (MBCS). *)
function StrLComp(S1, S2: PChar; MaxLen: Cardinal): Integer;
(*
Sysutils
AnsiStrLIComp compares two strings, case-insensitively, up to the first MaxLen bytes.
Note:	This function supports multi-byte character sets (MBCS).*)
function StrLIComp(S1, S2: PChar; MaxLen: Cardinal): Integer;
(*
Sysutils
AnsiCompareStr compares strings based on the current Windows locale with case sensitivity.*)
function CompareStr(const S1, S2: string): Integer;
(*
Sysutils
AnsiCompareText compares strings based on the current Windows locale without case sensitivity.
AnsiCompareText compares S1 to S2, without case sensitivity.
The compare operation is controlled by the current Windows locale.
AnsiCompareText returns a value less than 0 if S1 < S2, a value greater
than 0 if S1 > S2, and returns 0 if S1 = S2. *)
function CompareText(const S1, S2: string): Integer;

implementation
uses
{$ifdef MBCSFunctionCallCount}
  windows,
{$endif}
  SysUtils;

{$ifdef MBCSFunctionCallCount}
var
  lcntCompareStr:cardinal = 0;    // Counts the functioncall to the function 'CompareStr'
  lcntCompareText:cardinal = 0;
  lcntLowerCase:cardinal = 0;
  lcntPos:cardinal = 0;
  lcntQuotedStr:cardinal = 0;
  lcntStrComp:cardinal = 0;
  lcntStrIComp:cardinal = 0;
  lcntStrLComp:cardinal = 0;
  lcntStrLIComp:cardinal = 0;
  lcntStrLower:cardinal = 0;
  lcntStrPos:cardinal = 0;
  lcntStrRScan:cardinal = 0;
  lcntStrUpper:cardinal = 0;
  lcntUpperCase:cardinal = 0;
{$endif}

function StrLower(Str: PChar): PChar;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntStrLower);
{$endif}

{$ifdef MBCS}
  result:=PChar(sysutils.AnsiLowerCase(StrPas(Str)));
{$else}
  result:=PChar(sysutils.LowerCase(StrPas(Str)));
{$endif}
end;// function StrLower(Str: PChar): PChar;

function StrPos(Str, SubStr: PChar): PChar;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntStrPos);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiStrPos(Str,SubStr);
{$else}
  result:=sysutils.StrPos(Str,SubStr);
{$endif}
end;// function StrPos(Str, SubStr: PChar): PChar;

function StrRScan(Str: PChar; Chr: Char): PChar;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntStrRScan);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiStrRScan(Str,Chr);
{$else}
  result:=sysutils.StrRScan(Str,Chr);
{$endif}
end;// function StrRScan(Str: PChar; Chr: Char): PChar;

function LowerCase(const S: string): string;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntLowerCase);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiLowerCase(s);
{$else}
  result:=sysutils.LowerCase(s);
{$endif}
end;// function LowerCase(const S: string): string;

function StrUpper(Str: PChar): PChar;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntStrUpper);
{$endif}

{$ifdef MBCS}
  result:=PChar(sysutils.AnsiUpperCase(StrPas(Str)));
{$else}
  result:=PChar(sysutils.UpperCase(StrPas(Str)));
{$endif}
end;// function StrUpper(Str: PChar): PChar;

function UpperCase(const S: string): string;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntUpperCase);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiUpperCase(s);
{$else}
  result:=sysutils.UpperCase(s);
{$endif}
end;// function UpperCase(const S: string): string;

function Pos(const Substr, S: string): Integer;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntPos);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiPos(Substr,S);
{$else}
  result:=system.Pos(Substr,S);
{$endif}
end;// function Pos(const Substr, S: string): Integer;

function QuotedStr(const S: string): string;
//function QuotedStr(const S: string; Quote: Char): string;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntQuotedStr);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiQuotedStr(S,'''');
{$else}
  result:=sysutils.QuotedStr(S);
{$endif}
end;// function QuotedStr(const S: string): string;

function StrComp(S1, S2: PChar): Integer;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntStrComp);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiStrComp(S1,S2);
{$else}
  result:=sysutils.StrComp(S1,S2);
{$endif}
end;// function StrComp(S1, S2: PChar): Integer;

function StrIComp(S1, S2: PChar): Integer;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntStrIComp);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiStrIComp(S1,S2);
{$else}
  result:=sysutils.StrIComp(S1,S2);
{$endif}
end;// function StrIComp(S1, S2: PChar): Integer;

function StrLComp(S1, S2: PChar; MaxLen: Cardinal): Integer;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntStrLComp);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiStrLComp(S1,S2,MaxLen);
{$else}
  result:=sysutils.StrLComp(S1,S2,MaxLen);
{$endif}
end;// function StrLComp(S1, S2: PChar; MaxLen: Cardinal): Integer;

function StrLIComp(S1, S2: PChar; MaxLen: Cardinal): Integer;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntStrLIComp);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiStrLIComp(S1,S2,MaxLen);
{$else}
  result:=sysutils.StrLIComp(S1,S2,MaxLen);
{$endif}
end;// function StrLIComp(S1, S2: PChar; MaxLen: Cardinal): Integer;

function CompareStr(const S1, S2: string): Integer;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntCompareStr);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiCompareStr(S1,S2);
{$else}
  result:=sysutils.CompareStr(S1,S2);
{$endif}
end;// function CompareStr(const S1, S2: string): Integer;

function CompareText(const S1, S2: string): Integer;
begin
{$ifdef MBCSFunctionCallCount}
  inc(lcntCompareText);
{$endif}

{$ifdef MBCS}
  result:=sysutils.AnsiCompareText(S1,S2);
{$else}
  result:=sysutils.CompareText(S1,S2);
{$endif}
end;// function CompareText(const S1, S2: string): Integer;



{$ifdef MBCSFunctionCallCount}

var
  lLogFileName:string;
  lBuffSize:cardinal;
  lMBCSFunctionCallCountLogFile:System.Text;
const
  cLogFileName ='MBCSFunctionCallCount.log';

//*********************************************************
//  Write one Line in the Log file
//*********************************************************
procedure WriteLineToLogFile(aDescription:string; aCount:cardinal);
begin
  writeLn(lMBCSFunctionCallCountLogFile,aCount,'   -   ',aDescription);
end;// procedure WriteLineToLogFile(aDescription:string; aCount:cardinal);

initialization

finalization
  // Get the length of systems TMP Path
  lBuffSize:=GetTempPath(lBuffSize,nil);
  if lBuffSize>0 then begin
    // Set Buffer length
    setLength (lLogFileName,lBuffSize+1);
    // Get syststems TMP Path
    lBuffSize:=GetTempPath(lBuffSize,PChar(lLogFileName));
    if lBuffSize>0 then begin
      // Correct length of the Path (cut the trailing #0)
      setLength (lLogFileName,lBuffSize);
      // Build Filename
      lLogFileName:=lLogFileName+cLogFileName;
      // Create the File new every time the programm starts
      Assign(lMBCSFunctionCallCountLogFile,lLogFileName);
      if FileExists(lLogFileName) then
        append(lMBCSFunctionCallCountLogFile)
      else
        rewrite(lMBCSFunctionCallCountLogFile);

      writeLn(lMBCSFunctionCallCountLogFile,'Date ',DateTimeToStr(now));

      // Writes the count of every function in the log file
      WriteLineToLogFile('StrLower',lcntStrLower);
      WriteLineToLogFile('StrPos',lcntStrPos);
      WriteLineToLogFile('StrRScan',lcntStrRScan);
      WriteLineToLogFile('StrRScan',lcntStrRScan);
      WriteLineToLogFile('LowerCase',lcntLowerCase);
      WriteLineToLogFile('StrUpper',lcntStrUpper);
      WriteLineToLogFile('UpperCase',lcntUpperCase);
      WriteLineToLogFile('Pos',lcntPos);
      WriteLineToLogFile('QuotedStr',lcntQuotedStr);
      WriteLineToLogFile('StrComp',lcntStrComp);
      WriteLineToLogFile('StrIComp',lcntStrIComp);
      WriteLineToLogFile('StrLComp',lcntStrLComp);
      WriteLineToLogFile('StrLIComp',lcntStrLIComp);
      WriteLineToLogFile('CompareStr',lcntCompareStr);
      WriteLineToLogFile('CompareText',lcntCompareText);
      writeln(lMBCSFunctionCallCountLogFile);
      
      closeFile(lMBCSFunctionCallCountLogFile);
    end;// if lBuffSize>0 then begin
  end;// if lBuffSize>0 then begin
{$endif}

end.



