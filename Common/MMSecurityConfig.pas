{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMSecurityConfig.pas
| Projectpart...: Millmaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.10.1999  1.00  Wss | Dialogvorlage erstellt
| 21.10.1999  1.00  Wss | Config dialog created
| 16.02.2000  1.01  Wss | Strings changed to Hint, Caption text, Native string now stored
| 10.03.2000  1.01  Wss | on OK check search native string changed: no compare of component's name
| 12.04.2000  1.01  Wss | Wenn Komponente nicht mehr existiert -> in FillListBox aus Liste entfernen
| 11.05.2000  1.01  Wss | TUpDown added for CHHU
| 11.01.2002  1.01  Wss | TControlList.Assign added to make a copy of worklist to work with it
|=========================================================================================*}
unit MMSecurityConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SelectionDialog, ActnList, mmActionList, Menus, mmPopupMenu, Buttons,
  mmSpeedButton, StdCtrls, mmListBox, mmLabel, mmGroupBox, mmButton,
  ExtCtrls, mmPanel, IvDictio, IvMulti, IvEMulti, mmTranslator;

type
  TSecurityState = (ssHide, ssVisible, ssEnabled);
  //............................................................................
  TGroupList = class;

  TControlList = class(TStringList)
  private
    function GetComment(aIndex: Integer): String;
    function GetName(aIndex: Integer): String;
    function GetGroupList(aIndex: Integer): TGroupList;
  public
    property Comment[aIndex:Integer]: String read GetComment;
    property GroupList[aIndex: Integer]: TGroupList read GetGroupList;
    property Name[aIndex: Integer]: String read GetName;

    procedure AddControlGroupString(const aStr: string);
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    constructor Create; virtual;
    destructor Destroy; override;
    function Find(const S: string; var Index: Integer): Boolean; reintroduce;
    function GetControlGroupString(aIndex: Integer): String;
    function GetGroupState(aName, aGroup: String): TSecurityState;
    procedure SetGroupState(aName, aGroup: String; aState: TSecurityState);
  end;
  //............................................................................
  TGroupList = class(TStringList)
  private
    function GetGroup(aIndex: Integer): String;
    function GetGroupString: String;
    function GetState(aIndex: Integer): TSecurityState;
    procedure SetGroup(aIndex: Integer; const Value: String);
    procedure SetGroupString(const Value: String);
    procedure SetState(aIndex: Integer; const Value: TSecurityState);
  public
    property Group[aIndex: Integer]: String read GetGroup write SetGroup;
    property GroupString: String read GetGroupString write SetGroupString;
    property State[aIndex: Integer]: TSecurityState read GetState write SetState;
    procedure AddGroup(aGroup: String; aState: Integer);
    constructor Create; virtual;
    procedure SetValidGroups(aGroups: String);
  end;
  //............................................................................
  TfrmSelectComponents = class(TfrmSelectionDialog)
    mmTranslator: TmmTranslator;
    procedure acOKExecute(Sender: TObject);
    procedure acMoveLeftExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    fCompList: TControlList;
    procedure FillListBox;
    procedure SetComponentList(const Value: TControlList);
//    function Translate(aString: String): String;
  public
    property ComponentList: TControlList read fCompList write SetComponentList;
  end;
//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
function GetSectionString(aName: String): String;
function StrLeft(aString: String; aSeparator: String): string;
function StrRight(aString: String; aSeparator: String): string;


var
  frmSelectComponents: TfrmSelectComponents;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}

uses
  mmMBCS, mmCS,

  ComCtrls, Db, TypInfo, mmDictionary, mmEventLog;
//------------------------------------------------------------------------------
function GetSectionString(aName: String): String;
begin
  Result := Format('[%s]', [aName]);
end;
//------------------------------------------------------------------------------
function StrLeft(aString: String; aSeparator: String): string;
var
  xPos: Integer;
begin
  xPos := Pos(aSeparator, aString);
  if xPos > 0 then
    Result := Trim(Copy(aString, 1, xPos-1))
  else
    Result := '';
end;
//------------------------------------------------------------------------------
function StrRight(aString: String; aSeparator: String): string;
var
  xPos: Integer;
begin
  xPos := Pos(aSeparator, aString);
  if xPos > 0 then begin
    xPos := xPos + Length(aSeparator);
    Result := Trim(Copy(aString, xPos, Length(aString) - xPos + 1))
  end else
    Result := '';
end;
//------------------------------------------------------------------------------

//******************************************************************************
// TControlList
//******************************************************************************
//------------------------------------------------------------------------------
procedure TControlList.AddControlGroupString(const aStr: string);
var
  xGroupList: TGroupList;
begin
  // string format:
  // Control1 ## Control1 comment @@ Grouplist
  xGroupList := TGroupList.Create;
  xGroupList.GroupString := StrRight(aStr, '@@');
  AddObject(StrLeft(aStr, '@@'), xGroupList);
end;
//------------------------------------------------------------------------------
procedure TControlList.Assign(Source: TPersistent);
var
  i: Integer;
begin
  if Source is TControlList then begin
    Clear;
    with Source as TControlList do begin
      for i:=0 to Count-1 do begin
        Self.AddControlGroupString(GetControlGroupString(i));
      end;
    end;
  end else
    inherited Assign(Source);
end;
//------------------------------------------------------------------------------
procedure TControlList.Clear;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    Objects[i].Free;

  inherited Clear;
end;
//------------------------------------------------------------------------------
constructor TControlList.Create;
begin
  inherited Create;
  Sorted := True;
end;
//------------------------------------------------------------------------------
destructor TControlList.Destroy;
begin
  Clear;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TControlList.Find(const S: string; var Index: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=0 to Count-1 do begin
    Result := (LowerCase(S) = LowerCase(Name[i]));
    if Result then begin
      Index := i;
      Break;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TControlList.GetComment(aIndex: Integer): String;
begin
  Result := StrRight(Strings[aIndex], '##');
end;
//------------------------------------------------------------------------------
function TControlList.GetName(aIndex: Integer): String;
begin
  Result := StrLeft(Strings[aIndex], '##');
end;
//------------------------------------------------------------------------------
function TControlList.GetControlGroupString(aIndex: Integer): String;
var
  xStr: String;
begin
  Result := Strings[aIndex] + ' @@ ';
  if Assigned(Objects[aIndex]) then begin
    xStr := TGroupList(Objects[aIndex]).GroupString;
    Result := Format('%s%s', [Result, xStr]);
  end;
end;
//------------------------------------------------------------------------------
function TControlList.GetGroupList(aIndex: Integer): TGroupList;
begin
  Result := TGroupList(Objects[aIndex]);
end;
//------------------------------------------------------------------------------
function TControlList.GetGroupState(aName, aGroup: String): TSecurityState;
var
  xIndex: Integer;
  xGroupList: TGroupList;
begin
  Result := ssEnabled;  // default if control isn't in list
  if Find(aName, xIndex) then begin
    Result := ssHide;  // control has to be controlled: default hide
    if Assigned(Objects[xIndex]) then begin
      xGroupList := TGroupList(Objects[xIndex]);
      if xGroupList.Find(aGroup, xIndex) then
        Result := xGroupList.State[xIndex];
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TControlList.SetGroupState(aName, aGroup: String; aState: TSecurityState);
var
  xIndex: Integer;
begin
  if Find(aName, xIndex) then begin
    if not Assigned(Objects[xIndex]) then
      Objects[xIndex] := TGroupList.Create;

    TGroupList(Objects[xIndex]).AddGroup(aGroup, Integer(aState));
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// TGroupList
//******************************************************************************
//------------------------------------------------------------------------------
procedure TGroupList.AddGroup(aGroup: String; aState: Integer);
var
  xIndex: Integer;
begin
  if Find(aGroup, xIndex) then
    Objects[xIndex] := TObject(aState)
  else
    AddObject(aGroup, TObject(aState));
end;
//------------------------------------------------------------------------------
constructor TGroupList.Create;
begin
  inherited Create;
  Duplicates := dupIgnore;
  Sorted     := True;
end;
//------------------------------------------------------------------------------
function TGroupList.GetGroup(aIndex: Integer): String;
begin
  Result := Strings[aIndex];
end;
//------------------------------------------------------------------------------
function TGroupList.GetGroupString: String;
var
  xStrList: TStringList;
  i: Integer;
begin
  Result := '';
  // create temporary string list
  xStrList := TStringList.Create;
  try
    BeginUpdate;
    for i:=0 to Count-1 do
      xStrList.Add(Format('%s=%d', [Strings[i], Integer(Objects[i])]));
    Result := xStrList.CommaText;
  finally
    EndUpdate;
    xStrList.Free;
  end;
end;
//------------------------------------------------------------------------------
function TGroupList.GetState(aIndex: Integer): TSecurityState;
begin
  Result := TSecurityState(Objects[aIndex]);
end;
//------------------------------------------------------------------------------
procedure TGroupList.SetGroup(aIndex: Integer; const Value: String);
begin
  Strings[aIndex] := Value
end;
//------------------------------------------------------------------------------
procedure TGroupList.SetValidGroups(aGroups: String);
var
  xStrList: TStringList;
  xStr: String;
  i: Integer;
  xIndex: Integer;
begin
  // create temporary string list
  xStrList := TStringList.Create;
  xStrList.CommaText := aGroups;
  try
    BeginUpdate;
    // check current group list for unnecessary groups
    i := 0;
    while i < Count do begin
      xStr := Group[i];
      if not xStrList.Find(xStr, xIndex) then
        Delete(i)
      else
        inc(i);
    end;
    // Now add new groups into GroupList
    for i:=0 to xStrList.Count-1 do
      Add(xStrList.Strings[i]);  // duplicates are ignored
  finally
    EndUpdate;
    xStrList.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TGroupList.SetGroupString(const Value: String);
var
  xStrList: TStringList;
  xStr: String;
  i: Integer;
begin
  // create temporary string list
  xStrList := TStringList.Create;
  xStrList.CommaText := Value;
  try
    BeginUpdate;
    Clear;
    for i:=0 to xStrList.Count-1 do begin
      xStr := xStrList.Strings[i];
      AddGroup(StrLeft(xStr, '='), StrToInt(StrRight(xStr, '=')));
    end;
  finally
    EndUpdate;
    xStrList.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TGroupList.SetState(aIndex: Integer; const Value: TSecurityState);
begin
  Objects[aIndex] := TObject(Value);
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TfrmSelectComponents
//------------------------------------------------------------------------------
procedure TfrmSelectComponents.acOKExecute(Sender: TObject);
var
  i, j: Integer;
  xIndex: Integer;
  xStr: String;
  xHelpStr: String;
begin
  EnterMethod('acOKExecute');
  CodeSite.SendStringList('fCompList', fCompList);
  // if there are components not already in complist -> add
  for i:=0 to lbDest.Items.Count-1 do begin
    xStr := TComponent(lbDest.Items.Objects[i]).Name;
    CodeSite.SendString('Component', xStr);
    if not fCompList.Find(xStr, xIndex) then begin
      // get native string from component for MMSetup else use string from listbox
      xHelpStr := lbDest.Items.Strings[i];
      with TmmDictionary(mmTranslator.Dictionary) do
      try
        for j:=0 to TranslationCount-1 do begin
          if Translations[j].Current = lbDest.Items.Strings[i] then begin
            xHelpStr := Translations[j].Str;
            break;
          end;
        end;
      except
        on e: Exception do
          WriteToEventLog('TfrmSelectComponents: Could not get native string. Error: ' + e.Message, Application.Name, etError);
      end;
      fCompList.Add(Format('%s ## %s', [xStr, xHelpStr]));
    end;
  end;
  CodeSite.SendStringList('fCompList', fCompList);

  inherited;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectComponents.acMoveLeftExecute(Sender: TObject);
var
  i: Integer;
  xIndex: Integer;
begin
  for i:=0 to lbDest.Items.Count-1 do begin
    // delete selected items in lbDest from fCompList if they exists
    if lbDest.Selected[i] then begin
      if fCompList.Find(TComponent(lbDest.Items.Objects[i]).Name, xIndex) then
        fCompList.Delete(xIndex);
    end;
  end;

  inherited;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectComponents.FillListBox;
var
  i: Integer;
  xIndex: Integer;
//  xCompName: String;
  xComponent: TComponent;
  xHelpStr: String;

  function GetHelpString(aComponent: TComponent; aProperty: String; var aHelpStr: String): Boolean;
  var
//    xTypeInfo: PTypeInfo;
//    xPropInfo: PPropInfo;
    xPropList: TPropList;
    xPropCount: Integer;
    i: Integer;
  begin
    aHelpStr := '';
    try
      // to avoid exception with GetStrProp if a property doesn't exist check
      // first if it is in property list.
      xPropCount := GetPropList(aComponent.ClassInfo, [tkString, tkLString, tkWString], @xPropList);
      for i:=0 to xPropCount-1 do begin
        if CompareText(xPropList[i].Name, aProperty) = 0 then begin
          // now we can access this property without an exception
          aHelpStr := GetStrProp(aComponent, aProperty);
          break;
        end;
      end;
    except
    end;
    Result := (aHelpStr <> '');
  end;

begin
  // FIRST: cross check if a component in list doesn't exists anymore
  i := 0;
  while i < fCompList.Count do begin
    xComponent := Owner.FindComponent(fCompList.Name[i]);
    if Assigned(xComponent) then
      inc(i)
    else
      fCompList.Delete(i);
  end;
  // SECOND: get all components which has to be controlled
  for i:=0 to Owner.ComponentCount-1 do begin
    xComponent := Owner.Components[i];
    if (xComponent is TCustomAction) or
       (xComponent is TCustomEdit) or
       (xComponent is TCustomComboBox) or
       (xComponent is TCustomAction) or
       (xComponent is TTabSheet) or
       (xComponent is TUpDown) or  // added for CHHU
       (xComponent is TField) then begin

      if not GetHelpString(xComponent, 'Hint', xHelpStr) then
        GetHelpString(xComponent, 'Caption', xHelpStr);

      // control components with valid Hint or Caption only
      if xHelpStr <> '' then begin
        // if this component is in the controlled list add to lbDest
        if fCompList.Find(xComponent.Name, xIndex) then begin
          if lbDest.Items.IndexOf( xHelpStr ) < 0 then
             lbDest.Items.AddObject(xHelpStr, xComponent);
        end else begin
          if lbSource.Items.IndexOf( xHelpStr ) < 0 then
             lbSource.Items.AddObject(xHelpStr, xComponent);
        end;
      end;
{
      if not GetHelpString(xComponent, 'Hint', xHelpStr) then
        if not GetHelpString(xComponent, 'Caption', xHelpStr) then
          xHelpStr := xComponent.Name;

      // if this component is in the controlled list add to lbDest
      if fCompList.Find(xComponent.Name, xIndex) then
        lbDest.Items.AddObject(xHelpStr, xComponent)
      else
        lbSource.Items.AddObject(xHelpStr, xComponent);
{}
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectComponents.FormCreate(Sender: TObject);
begin
  inherited;
  fCompList := TControlList.Create;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectComponents.FormDestroy(Sender: TObject);
begin
  fCompList.Free;
  inherited;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectComponents.SetComponentList(const Value: TControlList);
begin
  fCompList.Assign(Value);
  FillListBox;
end;
//------------------------------------------------------------------------------
{
function TfrmSelectComponents.Translate(aString: String): String;
begin
  Result := mmTranslator.Dictionary.Translate(aString);
end;
{}
//------------------------------------------------------------------------------


end.

