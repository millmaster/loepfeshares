unit WrapperForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM;

type
  TfrmWrapper = class(TmmForm)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  protected
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure RestoreFormLayout(aRegistryKey: String = ''); override;
    procedure SaveFormLayout(aRegistryKey: String = ''); override;
  end;

function CreateWrappedDataForm(aCaller: TObject; aFormClass: TFormClass; var aDataForm: TForm; var aHandle: Integer): Boolean;

implementation
{$R *.DFM}
uses
  mmMBCS,
  mmRegistry;

//------------------------------------------------------------------------------
function CreateWrappedDataForm(aCaller: TObject; aFormClass: TFormClass; var aDataForm: TForm; var aHandle: Integer): Boolean;
var
  xWrapForm: TfrmWrapper;
begin
  Result := False;
  try
    Application.CreateForm(TfrmWrapper, xWrapForm);
    // make the form invisible for the user
    with xWrapForm do begin
      Left := 0 - Width - 2;
      Top  := 0 - Height - 2;
      Visible := True;
      aHandle := Handle;
    end;

    // now create specified data form
    if GetCreateChildForm(aFormClass, aDataForm, xWrapForm, True) then begin
      xWrapForm.WrapperForm          := TmmForm(aDataForm);
      TmmForm(aDataForm).WrapperForm := xWrapForm;
    end;

    Result := True;
  except
  end;
end;
//------------------------------------------------------------------------------
constructor TfrmWrapper.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;
//------------------------------------------------------------------------------
destructor TfrmWrapper.Destroy;
begin
//  CodeSite.SendString('aWrapFrom destroy', Name);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TfrmWrapper.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//------------------------------------------------------------------------------
procedure TfrmWrapper.RestoreFormLayout(aRegistryKey: String);
begin
  // stay empty to prohibit loading from registry
end;
//------------------------------------------------------------------------------
procedure TfrmWrapper.SaveFormLayout(aRegistryKey: String);
begin
  // stay empty to prohibit saving in registry
end;
//------------------------------------------------------------------------------
end.

