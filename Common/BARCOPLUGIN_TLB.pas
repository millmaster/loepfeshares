unit BARCOPLUGIN_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 26.05.2000 13:56:35 from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\MillMaster\Floor\PLUGIN.TLB (1)
// IID\LCID: {050EBFD8-FB88-11D1-A243-006097C4C8E3}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\StdOle2.Tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// Errors:
//   Hint: Member 'type' of 'tagSTATSTG' changed to 'type_'
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  BARCOPLUGINMajorVersion = 1;
  BARCOPLUGINMinorVersion = 0;

  LIBID_BARCOPLUGIN: TGUID = '{050EBFD8-FB88-11D1-A243-006097C4C8E3}';

  IID_IBarcoPlugin: TGUID = '{050EBFE9-FB88-11D1-A243-006097C4C8E3}';
  IID_IBarcoPluginNotify: TGUID = '{050EBFEB-FB88-11D1-A243-006097C4C8E3}';
  IID_IMachineSelectionList: TGUID = '{6A22F030-E4B3-11D0-9AAD-0000C088D00B}';
  IID_IBarcoMachineSelectionList: TGUID = '{19AB9170-7580-11D3-901E-0060080C772F}';
  IID_IBarcoSectionList: TGUID = '{6EEBF830-7A67-11D3-9027-0060080C772F}';
  IID_ISequentialStream: TGUID = '{0C733A30-2A1C-11CE-ADE5-00AA0044773D}';
  IID_IStream: TGUID = '{0000000C-0000-0000-C000-000000000046}';
  IID_IReportBanner: TGUID = '{11EB82B0-7E0B-11D2-AA6C-00104B4B2529}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum __MIDL___MIDL_itf_plugin_0000_0006
type
  __MIDL___MIDL_itf_plugin_0000_0006 = TOleEnum;
const
  eMAINMENU = $00000000;
  eMENUFLOOR = $00000001;
  eMENUGROUP = $00000002;
  eMENUMACHINE = $00000003;

// Constants for enum __MIDL___MIDL_itf_plugin_0000_0001
type
  __MIDL___MIDL_itf_plugin_0000_0001 = TOleEnum;
const
  BPCCS_MACHINESELCHANGE = $00000001;
  BPCCS_DOCKEDNOTIFICATION = $00000002;
  BPCCS_STATUSCOLORCHANGE = $00000003;

// Constants for enum __MIDL___MIDL_itf_plugin_0000_0003
type
  __MIDL___MIDL_itf_plugin_0000_0003 = TOleEnum;
const
  BPGCS_HELPTEXT = $00000001;
  BPGCS_VERB = $00000002;
  BPGCS_TITLE = $00000003;
  BPGCS_STATUSBAR = $00000004;
  BPGCS_PLUGINNAME = $00000005;

// Constants for enum __MIDL___MIDL_itf_plugin_0000_0002
type
  __MIDL___MIDL_itf_plugin_0000_0002 = TOleEnum;
const
  BPNE_MACHINESCHANGED = $00000001;
  BPNE_STOPSCHANGED = $00000002;
  BPNE_MENUCHANGED = $00000003;
  BPNE_COMMANDCOMPLETE = $00000004;
  BPNE_DOCKREQUEST = $00000005;
  BPNE_UNDOCKREQUEST = $00000006;
  BPNE_STATUSCHANGE = $00000007;
  BPNE_REFRESHFLOOR = $00000008;

// Constants for enum __MIDL_IReportBanner_0001
type
  __MIDL_IReportBanner_0001 = TOleEnum;
const
  ReportHeader = $00000000;
  ReportFooter = $00000001;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IBarcoPlugin = interface;
  IBarcoPluginDisp = dispinterface;
  IBarcoPluginNotify = interface;
  IBarcoPluginNotifyDisp = dispinterface;
  IMachineSelectionList = interface;
  IMachineSelectionListDisp = dispinterface;
  IBarcoMachineSelectionList = interface;
//  IBarcoMachineSelectionListDisp = dispinterface;
  IBarcoSectionList = interface;
  IBarcoSectionListDisp = dispinterface;
  ISequentialStream = interface;
  IStream = interface;
  IReportBanner = interface;
  IReportBannerDisp = dispinterface;

// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  wireHDC = ^_RemotableHandle; 
  PPUserType1 = ^IStream; {*}
  PPUserType2 = ^IReportBanner; {*}
  PByte1 = ^Byte; {*}

  MENUTYPE = __MIDL___MIDL_itf_plugin_0000_0006; 
  CLIENTSTATUS = __MIDL___MIDL_itf_plugin_0000_0001; 
  COMMANDSTRING = __MIDL___MIDL_itf_plugin_0000_0003; 
  NOTIFYEVENTENUM = __MIDL___MIDL_itf_plugin_0000_0002; 

  _LARGE_INTEGER = packed record
    QuadPart: Int64;
  end;

  _ULARGE_INTEGER = packed record
    QuadPart: Largeuint;
  end;

  _FILETIME = packed record
    dwLowDateTime: LongWord;
    dwHighDateTime: LongWord;
  end;

  tagSTATSTG = packed record
    pwcsName: PWideChar;
    type_: LongWord;
    cbSize: _ULARGE_INTEGER;
    mtime: _FILETIME;
    ctime: _FILETIME;
    atime: _FILETIME;
    grfMode: LongWord;
    grfLocksSupported: LongWord;
    clsid: TGUID;
    grfStateBits: LongWord;
    reserved: LongWord;
  end;

  ENUM_REPORTBANNER = __MIDL_IReportBanner_0001; 

  __MIDL_IWinTypes_0009 = record
    case Integer of
      0: (hInproc: Integer);
      1: (hRemote: Integer);
  end;

  _RemotableHandle = packed record
    fContext: Integer;
    u: __MIDL_IWinTypes_0009;
  end;


// *********************************************************************//
// Interface: IBarcoPlugin
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {050EBFE9-FB88-11D1-A243-006097C4C8E3}
// *********************************************************************//
  IBarcoPlugin = interface(IDispatch)
    ['{050EBFE9-FB88-11D1-A243-006097C4C8E3}']
    procedure GetPluginMenu(iMenuType: MENUTYPE; var hMenu: Integer); safecall;
    procedure ChangeClientStatus(nStatus: CLIENTSTATUS); safecall;
    function  GetCommandString(nID: SYSINT; nFlag: COMMANDSTRING): WideString; safecall;
    function  GetCommandStatus(nID: SYSINT): Integer; safecall;
    function  GetToolbarBitmap(var pCommandIDs: OleVariant): IDispatch; safecall;
    function  InvokeCommand(nID: SYSINT; hWndParent: Integer; 
                            const pPluginNotify: IBarcoPluginNotify): SYSINT; safecall;
    procedure ContextHelp(nID: SYSINT; hWndParent: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoPluginDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {050EBFE9-FB88-11D1-A243-006097C4C8E3}
// *********************************************************************//
  IBarcoPluginDisp = dispinterface
    ['{050EBFE9-FB88-11D1-A243-006097C4C8E3}']
    procedure GetPluginMenu(iMenuType: MENUTYPE; var hMenu: Integer); dispid 1;
    procedure ChangeClientStatus(nStatus: CLIENTSTATUS); dispid 2;
    function  GetCommandString(nID: SYSINT; nFlag: COMMANDSTRING): WideString; dispid 3;
    function  GetCommandStatus(nID: SYSINT): Integer; dispid 4;
    function  GetToolbarBitmap(var pCommandIDs: OleVariant): IDispatch; dispid 5;
    function  InvokeCommand(nID: SYSINT; hWndParent: Integer; 
                            const pPluginNotify: IBarcoPluginNotify): SYSINT; dispid 6;
    procedure ContextHelp(nID: SYSINT; hWndParent: Integer); dispid 7;
  end;

// *********************************************************************//
// Interface: IBarcoPluginNotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {050EBFEB-FB88-11D1-A243-006097C4C8E3}
// *********************************************************************//
  IBarcoPluginNotify = interface(IDispatch)
    ['{050EBFEB-FB88-11D1-A243-006097C4C8E3}']
    procedure NotifyEvent(nEvent: NOTIFYEVENTENUM; v: OleVariant); safecall;
    function  Get_MachineSelection: IBarcoMachineSelectionList; safecall;
    procedure GetStatusColors(var ppStream: IStream); safecall;
    procedure GetIReportBanner(var pVal: IReportBanner); safecall;
    property MachineSelection: IBarcoMachineSelectionList read Get_MachineSelection;
  end;

// *********************************************************************//
// DispIntf:  IBarcoPluginNotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {050EBFEB-FB88-11D1-A243-006097C4C8E3}
// *********************************************************************//
  IBarcoPluginNotifyDisp = dispinterface
    ['{050EBFEB-FB88-11D1-A243-006097C4C8E3}']
    procedure NotifyEvent(nEvent: NOTIFYEVENTENUM; v: OleVariant); dispid 1;
    property MachineSelection: IBarcoMachineSelectionList readonly dispid 2;
    procedure GetStatusColors(var ppStream: IStream); dispid 3;
    procedure GetIReportBanner(var pVal: IReportBanner); dispid 4;
  end;

// *********************************************************************//
// Interface: IMachineSelectionList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6A22F030-E4B3-11D0-9AAD-0000C088D00B}
// *********************************************************************//
  IMachineSelectionList = interface(IDispatch)
    ['{6A22F030-E4B3-11D0-9AAD-0000C088D00B}']
    function  Get_FirstMachine: Smallint; safecall;
    procedure Set_FirstMachine(pVal: Smallint); safecall;
    function  Get_NextMachine(pos: SYSINT): Smallint; safecall;
    procedure Set_NextMachine(pos: SYSINT; pVal: Smallint); safecall;
    property FirstMachine: Smallint read Get_FirstMachine write Set_FirstMachine;
    property NextMachine[pos: SYSINT]: Smallint read Get_NextMachine write Set_NextMachine;
  end;

// *********************************************************************//
// DispIntf:  IMachineSelectionListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6A22F030-E4B3-11D0-9AAD-0000C088D00B}
// *********************************************************************//
  IMachineSelectionListDisp = dispinterface
    ['{6A22F030-E4B3-11D0-9AAD-0000C088D00B}']
    property FirstMachine: Smallint dispid 1;
    property NextMachine[pos: SYSINT]: Smallint dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoMachineSelectionList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {19AB9170-7580-11D3-901E-0060080C772F}
// *********************************************************************//
  IBarcoMachineSelectionList = interface(IMachineSelectionList)
    ['{19AB9170-7580-11D3-901E-0060080C772F}']
    function  Get_FirstMachine(var pos: SYSINT): Smallint; safecall;
    procedure Set_FirstMachine(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_NextMachine(var pos: SYSINT): Smallint; safecall;
    procedure Set_NextMachine(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_Section: IBarcoSectionList; safecall;
    procedure Set_Section(const pVal: IBarcoSectionList); safecall;
    property FirstMachine[var pos: SYSINT]: Smallint read Get_FirstMachine write Set_FirstMachine;
    property NextMachine[var pos: SYSINT]: Smallint read Get_NextMachine write Set_NextMachine;
    property Section: IBarcoSectionList read Get_Section write Set_Section;
  end;

// *********************************************************************//
// DispIntf:  IBarcoMachineSelectionListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {19AB9170-7580-11D3-901E-0060080C772F}
// *********************************************************************//
(**
  IBarcoMachineSelectionListDisp = dispinterface
    ['{19AB9170-7580-11D3-901E-0060080C772F}']
    property FirstMachine[var pos: SYSINT]: Smallint dispid 3;
    property NextMachine[var pos: SYSINT]: Smallint dispid 4;
    property Section: IBarcoSectionList dispid 5;
    property FirstMachine: Smallint dispid 1;
    property NextMachine[pos: SYSINT]: Smallint dispid 2;
  end;
(**)

// *********************************************************************//
// Interface: IBarcoSectionList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6EEBF830-7A67-11D3-9027-0060080C772F}
// *********************************************************************//
  IBarcoSectionList = interface(IDispatch)
    ['{6EEBF830-7A67-11D3-9027-0060080C772F}']
    function  Get_FirstSection(var pos: SYSINT): Smallint; safecall;
    procedure Set_FirstSection(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_NextSection(var pos: SYSINT): Smallint; safecall;
    procedure Set_NextSection(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_NumSections: SYSINT; safecall;
    property FirstSection[var pos: SYSINT]: Smallint read Get_FirstSection write Set_FirstSection;
    property NextSection[var pos: SYSINT]: Smallint read Get_NextSection write Set_NextSection;
    property NumSections: SYSINT read Get_NumSections;
  end;

// *********************************************************************//
// DispIntf:  IBarcoSectionListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6EEBF830-7A67-11D3-9027-0060080C772F}
// *********************************************************************//
  IBarcoSectionListDisp = dispinterface
    ['{6EEBF830-7A67-11D3-9027-0060080C772F}']
    property FirstSection[var pos: SYSINT]: Smallint dispid 1;
    property NextSection[var pos: SYSINT]: Smallint dispid 2;
    property NumSections: SYSINT readonly dispid 3;
  end;

// *********************************************************************//
// Interface: ISequentialStream
// Flags:     (0)
// GUID:      {0C733A30-2A1C-11CE-ADE5-00AA0044773D}
// *********************************************************************//
  ISequentialStream = interface(IUnknown)
    ['{0C733A30-2A1C-11CE-ADE5-00AA0044773D}']
    function  RemoteRead(out pv: Byte; cb: LongWord; out pcbRead: LongWord): HResult; stdcall;
    function  RemoteWrite(var pv: Byte; cb: LongWord; out pcbWritten: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IStream
// Flags:     (0)
// GUID:      {0000000C-0000-0000-C000-000000000046}
// *********************************************************************//
  IStream = interface(ISequentialStream)
    ['{0000000C-0000-0000-C000-000000000046}']
    function  RemoteSeek(dlibMove: _LARGE_INTEGER; dwOrigin: LongWord; 
                         out plibNewPosition: _ULARGE_INTEGER): HResult; stdcall;
    function  SetSize(libNewSize: _ULARGE_INTEGER): HResult; stdcall;
    function  RemoteCopyTo(const pstm: IStream; cb: _ULARGE_INTEGER; out pcbRead: _ULARGE_INTEGER; 
                           out pcbWritten: _ULARGE_INTEGER): HResult; stdcall;
    function  Commit(grfCommitFlags: LongWord): HResult; stdcall;
    function  Revert: HResult; stdcall;
    function  LockRegion(libOffset: _ULARGE_INTEGER; cb: _ULARGE_INTEGER; dwLockType: LongWord): HResult; stdcall;
    function  UnlockRegion(libOffset: _ULARGE_INTEGER; cb: _ULARGE_INTEGER; dwLockType: LongWord): HResult; stdcall;
    function  Stat(out pstatstg: tagSTATSTG; grfStatFlag: LongWord): HResult; stdcall;
    function  Clone(out ppstm: IStream): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IReportBanner
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {11EB82B0-7E0B-11D2-AA6C-00104B4B2529}
// *********************************************************************//
  IReportBanner = interface(IDispatch)
    ['{11EB82B0-7E0B-11D2-AA6C-00104B4B2529}']
    procedure Size(nBanner: ENUM_REPORTBANNER; var hic: _RemotableHandle; bPrinting: Integer; 
                   out pcx: Integer; out pcy: Integer); safecall;
    procedure Draw(nBanner: ENUM_REPORTBANNER; var hic: _RemotableHandle; 
                   var hdc: _RemotableHandle; bPrinting: Integer; out pcx: Integer; out pcy: Integer); safecall;
    procedure Initialize; safecall;
    function  Get_Margins: PSafeArray; safecall;
    property Margins: PSafeArray read Get_Margins;
  end;

// *********************************************************************//
// DispIntf:  IReportBannerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {11EB82B0-7E0B-11D2-AA6C-00104B4B2529}
// *********************************************************************//
  IReportBannerDisp = dispinterface
    ['{11EB82B0-7E0B-11D2-AA6C-00104B4B2529}']
    procedure Size(nBanner: ENUM_REPORTBANNER; var hic: {??_RemotableHandle} OleVariant; 
                   bPrinting: Integer; out pcx: Integer; out pcy: Integer); dispid 1;
    procedure Draw(nBanner: ENUM_REPORTBANNER; var hic: {??_RemotableHandle} OleVariant; 
                   var hdc: {??_RemotableHandle} OleVariant; bPrinting: Integer; out pcx: Integer; 
                   out pcy: Integer); dispid 2;
    procedure Initialize; dispid 3;
    property Margins: {??PSafeArray} OleVariant readonly dispid 4;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

end.
