(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: mmCS.pas
| Projectpart...: Parameter-Dialog fuer die Spulerei und Einzelspindelbericht
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 24.02.2005 1.00 Wss | leere Procedure Register entfernt
| 04.11.2005      Lok | SendMemoryToCodeSite() hinzugef�gt (2 �berladene Funktioenen)
| 11.04.2006      Lok | refactoring: SendMemoryToCodeSite() aufgeteilt mit MemoryToString()
|=============================================================================*)
unit mmCS;

interface

uses
  RzCSIntf, windows;

type
  ICodeSiteHelper = interface (IUnknown)
    ['{61737D8E-D2BD-4F69-B59E-2FBB6AB0BEF5}']
  end;
  
  TCodeSiteHelper = class (TInterfacedObject, ICodesiteHelper)
  private
    mMethodName: string;
  public
    constructor Create(const MethodName: String);
    destructor Destroy; override;
  end;

const
  // Konstanten aus rzcsintf
  csmInfo              = RzCSIntf.csmInfo;
  csmWarning           = RzCSIntf.csmWarning;
  csmError             = RzCSIntf.csmError;
  csmCheckPoint        = RzCSIntf.csmCheckPoint;
  csmObject            = RzCSIntf.csmObject;
  csmStream            = RzCSIntf.csmStream;
  csmStringList        = RzCSIntf.csmStringList;
  csmProperty          = RzCSIntf.csmProperty;
  csmNote              = RzCSIntf.csmNote;
  csmEnterMethod       = RzCSIntf.csmEnterMethod;
  csmExitMethod        = RzCSIntf.csmExitMethod;
  csmSeparator         = RzCSIntf.csmSeparator;
  csmInactiveSeparator = RzCSIntf.csmInactiveSeparator;
  csmClear             = RzCSIntf.csmClear;
  csmScratchPad        = RzCSIntf.csmScratchPad;
  csmSaveLogFile       = RzCSIntf.csmSaveLogFile;
  csmLevel1            = 24;
  csmLevel2            = 25;
  csmLevel3            = 26;
  csmLevel4            = 27;
  csmLevel5            = 28;
  csmLevel6            = 29;
  csmLevel7            = 30;
  csmReminder          = 23;
  csmRed               = csmLevel1;
  csmOrange            = csmLevel2;
  csmYellow            = csmLevel3;
  csmGreen             = csmLevel4;
  csmBlue              = csmLevel5;
  csmIndigo            = csmLevel6;
  csmViolet            = csmLevel7;
  csmUser              = RzCSIntf.csmUser;

function CodeSite: TRzCodeSite;

function EnterMethod(const MethodName: String): ICodeSiteHelper;

(*---------------------------------------------------------
  Sendet einen beliebigen Datenstream an Codesite. Die Applikation ist verantwortlich,
  dass die Daten auch gesendet werden k�nnen (Schutzverletzung wegen falscher L�nge)

  Beispielaufruf (xData ist ein String)
    SendMemoryToCodeSite('Daten empfangen', @xData[1], Length(xData));
----------------------------------------------------------*)
procedure SendMemoryToCodeSite(aMsgType: Integer; aMsg: string; const aBuffer: PByte; aCount: Integer); overload;
procedure SendMemoryToCodeSite(aMsg: string; const aBuffer: PByte; aCount: Integer); overload;

(*---------------------------------------------------------
  Stellt einen String mit Hex Werten zusammen
----------------------------------------------------------*)
function MemoryToString(const aBuffer: PByte; aCount: Integer): string;

implementation

uses
  sysutils, JclStrings;

function CodeSite: TRzCodeSite;
begin
  Result := RzCSIntf.CodeSite;
end;

function EnterMethod(const MethodName: String): ICodeSiteHelper;
begin
  Result := TCodeSiteHelper.Create(MethodName);
end;

(*---------------------------------------------------------
  Stellt einen String mit Hex Werten zusammen
----------------------------------------------------------*)
function MemoryToString(const aBuffer: PByte; aCount: Integer): string;
var
  i: Cardinal;
  xTempStr: string;
begin
  SetLength(result, aCount * 3);
  for i := 0 to aCount - 1 do begin
    // Byte decodieren
    xTempStr := ' ' + IntToHex(PByte(cardinal(aBuffer) + i)^, 2);
    // die drei Bytes in das Resultat kopieren (Leerzeichen und zwei Zeichen f�r die Hexdarstellung)
    StrMove(result, xTempStr, (i * 3) + 1, 1, 3);
  end;// for i := 0 to aCount - 1 do begin;
end;// function MemoryToString(const aBuffer: PByte; aCount: Integer): string;

(*---------------------------------------------------------
  Sendet einen beliebigen Datenstream an Codesite. Die Applikation ist verantwortlich,
  dass die Daten auch gesendet werden k�nnen (Schutzverletzung wegen falscher L�nge)

  Beispielaufruf (xData ist ein String)
    SendMemoryToCodeSite(csmRed, 'Daten empfangen', @xData[1], Length(xData));
----------------------------------------------------------*)
procedure SendMemoryToCodeSite(aMsgType: Integer; aMsg: string; const aBuffer: PByte; aCount: Integer);
begin
  try
    // Daten senden
    CodeSite.SendStringEx(aMsgType, aMsg, MemoryToString(aBuffer, aCount));
  except
    on e: Exception do
      CodeSite.SendFmtError('SendMemoryToCodeSite: Exception Occured (%s)', [e.Message]);
  end;// try except
end;// procedure SendMemoryToCodeSite(aMsg: string; const aBuffer: PByte; aCount: Integer);

(*---------------------------------------------------------
  Sendet einen beliebigen Datenstream an Codesite. Die Applikation ist verantwortlich,
  dass die Daten auch gesendet werden k�nnen (Schutzverletzung wegen falscher L�nge)

  Beispielaufruf (xData ist ein String)
    SendMemoryToCodeSite('Daten empfangen', @xData[1], Length(xData));
----------------------------------------------------------*)
procedure SendMemoryToCodeSite(aMsg: string; const aBuffer: PByte; aCount: Integer);
begin
  SendMemoryToCodeSite(csmInfo ,aMsg, aBuffer, aCount);
end;// procedure SendMemoryToCodeSite(aMsg: string; const aBuffer: PByte; aCount: Integer);

//------------------------------------------------------------------------------
//--- TCodeSiteHelper
//------------------------------------------------------------------------------
constructor TCodeSiteHelper.Create(const MethodName: String);
begin
  inherited Create;
  mMethodName := MethodName;
  CodeSite.EnterMethod(mMethodName);
end;

//------------------------------------------------------------------------------
destructor TCodeSiteHelper.Destroy;
begin
  CodeSite.ExitMethod(mMethodName);
  inherited Destroy;
end;


end.
