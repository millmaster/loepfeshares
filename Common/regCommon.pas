unit regCommon;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R DBVisualBox.dcr}
{$R mmColorButton.dcr}
{$R MMLogin.dcr}
{$R TrayIcon.dcr}
{$R mmLineLabel.dcr}
{$R mmRotateLabel.dcr}
{$R FolderBrowser.dcr}
{$R mmExtStringGrid.dcr}
{$R mmCheckListEdit.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf, TypInfo, Controls, Db, sysutils,
  DBVisualBox,
  mmColorButton,
  MMLogin,
  TrayIcon, mmTrayIcon,
  mmLineLabel,
  mmRotateLabel,
  FolderBrowser,
  mmExtStringGrid, asgde, // asgde for ComponentEditor
  mmCheckListEdit; 
//------------------------------------------------------------------------------
type
  TDBVisualBoxKeyFieldProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function GetPropertyValue(aInstance: TPersistent; const aPropName: string): TPersistent;
var
  xPropInfo: PPropInfo;
begin
  Result := nil;
  xPropInfo := TypInfo.GetPropInfo(aInstance.ClassInfo, aPropName);
  if (xPropInfo <> nil) and (xPropInfo^.PropType^.Kind = tkClass) then
    Result := TObject(GetOrdProp(aInstance, xPropInfo)) as TPersistent;
end;
//------------------------------------------------------------------------------
// TDBVisualBoxKeyFieldProperty
//------------------------------------------------------------------------------
function TDBVisualBoxKeyFieldProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList, paRevertable];
end;
//------------------------------------------------------------------------------
procedure TDBVisualBoxKeyFieldProperty.GetValues(Proc: TGetStrProc);
var
  i: Integer;
  xValues: TStringList;
  //.........................................................
  procedure GetValueList(aList: TStrings);
  var
    xDataSource: TDataSource;
    i: Integer;
    xDataType: TFieldType;
  begin
    xDataSource := GetPropertyValue(GetComponent(0), 'DataSource') as TDataSource;
    if (xDataSource <> nil) and (xDataSource.DataSet <> nil) then
      xDataSource.DataSet.GetFieldNames(aList);

(**
    i := 0;
    while i < aList.Count-1 do begin
      xDataType := xDataSource.DataSet.Fields.Fields[i].DataType;
{
      if not(xDataSource.DataSet.FieldByName(aList.Strings[i]).DataType in [ftSmallint, ftInteger, ftWord, ftAutoInc, ftLargeint]) then
        aList.Delete(i)
      else
{}
        inc(i);
    end;
(**)
  end;
  //.........................................................
begin
  xValues := TStringList.Create;
  try
    GetValueList(xValues);
    for i:=0 to xValues.Count-1 do
      Proc(xValues[i]);
  finally
    xValues.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure MMRegister;
begin
  // Unit DBVisualBox
  RegisterComponents('LOEPFE', [TDBVisualBox, TDBVisualListBox, TDBVisualStringList]);
  // property editors
  RegisterPropertyEditor(TypeInfo(String), TDBVisualBox, 'KeyField', TDBVisualBoxKeyFieldProperty);
  RegisterPropertyEditor(TypeInfo(String), TDBVisualListBox, 'KeyField', TDBVisualBoxKeyFieldProperty);
  RegisterPropertyEditor(TypeInfo(String), TDBVisualStringList, 'KeyField', TDBVisualBoxKeyFieldProperty);
  // Unit mmColorButton
  RegisterComponents('LOEPFE', [TmmColorButton]);
  // Unit MMLogin
  RegisterComponents('LOEPFE', [TMMLogin]);
  // Unit TrayIcon
  RegisterComponents('LOEPFE', [TTrayIcon]);
  // Unit TrayIcon
  RegisterComponents('LOEPFE', [TmmTrayIcon]);
  // Unit mmLineLabel
  RegisterComponents('Standard', [TmmLineLabel]);
  // Unit mmRotateLabel
  RegisterComponents('Standard', [TmmRotateLabel]);
  // Unit FolderBrowser
  RegisterComponents('Dialogs', [TFolderBrowser]);
  // Unit mmExStringGrid, asgde
  RegisterComponents('Additional', [TmmExtStringGrid]);
  RegisterComponentEditor(TmmExtStringGrid, TAdvStringGridEditor);

  RegisterComponents('LOEPFE', [TMMCheckListEdit]);

end;
//------------------------------------------------------------------------------
end.
