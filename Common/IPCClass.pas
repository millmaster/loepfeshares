(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: IPCClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: IP Kommunikation mit NamedPipes fuer 1 Leser mit mehreren Schreiber
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 25.06.1998 0.00  Wss | Datei erstellt, Klasse TIPCServer und TPipeThread implementiert
| 06.07.1998 1.00  Wss | Code fuer TIPCClient von Mg eingefuehgt
| 14.07.1998 1.00  Wss | IPCServer: nur noch 1 Mutex fuer lokale Pipe
| 23.07.1998 1.01  Wss | Klasse TBaseMutex implementiert. Wenn eine Klasse Zugriffe von
                         mehreren Seiten ermoeglicht, so braucht sie eine Zugriffskontrolle.
                         Diese Klasse kann als Grundklass dafuer verwendet werden.
| 18.08.1998 1.02  Mg  | IPCServer mit Security Option erweitert.
| 10.09.1998 1.02  Wss | TBaseMutex.Create mit Default Wert ergaenzt
| 27.10.1998 1.03  Wss | TProtectedList implementiert -> loest TBaseMutex ab
| 24.11.1998 1.04  Mg  | TIPCServer.Read : Groessenvergleich in Kommentar
|                        TIPCServer.Init : Interne Pipe nur noch mSize gross
| 15.03.1999 1.05  Mg  | TPipeThread.Init : Errorhandling bei SetKernelObjectSecurity geaendert
| 14.04.1999 1.06  khp | Add new Class TCriticalSection
| 15.04.1999 1.07  Wss | TIPCServer derives now from TCriticalSection
|                        The building of the connection between TIPCClient and TIPCServer
|                        contains a security feature: the client is only allowed to write
|                        if it gets an short message from server to notify the server is
|                        ready to read: problem was if client connnect, write and disconnect
|                        very quickly the server haven't received the message. As a result
|                        some Sleep(1) to allow task switching could removed
| 21.04.1999 1.08  Wss | MemoryLeak found in TPipeThread.Destroy()
| 23.04.1999 1.09  Wss | IPCClient.Connect tries 2 times
| 27.04.1999 1.10  Wss | Enter/Leave; //LeaveCriticSection set as public
| 19.07.1999 1.11  Mg  | Event implemented
| 11.08.1999 1.12  Mg  | TIPCServer.CreateNewPipeReader : CriticalSection also over Init
| 18.10.1999 1.13  Wss | TCriticalList implemented: combines TmmList with TCriticalSection
| 19.09.2000 1.13  Wss | IPCClient: timeouted write with overlapped structure implemented
| 26.02.2004 1.13  Wss | - Packet�bermitlung mit grossen Datenmengen implementiert
                           in IPCServer und IPCClient -> ReadDynamic
| 21.07.2004 1.13  Wss | - Eigene TCriticalSection entfernt
|=========================================================================================*)
unit IPCClass;

interface

uses
  mmList, mmThread, Classes, SysUtils, Windows, IPCUnit, NTApis, syncobjs;

type
 //...........................................................................

  PBaseMutex = ^TBaseMutex;
  TBaseMutex = class(TObject)
  private
    mMutex: THandle;
    mMutexName: String;
    fTimeOut: DWord;
  protected
    fError: DWord;
    function GetMutex: Boolean;
    function FreeMutex: Boolean;
  public
    constructor Create(aMutexName: String = ''); virtual;
    destructor Destroy; override;
    function Init: Boolean; virtual;
    property Error: DWord read fError;
    property TimeOut: DWord read fTimeOut write fTimeOut;
  end;

  //..........................................................................

  PEvent = ^TEvent;
  TEvent = class(TObject)
  private
    mEvent: THandle;
    mEventName: String;
    mManualReset : boolean;
    fTimeOut: DWord;
  protected
    fError: DWord;
  public
    constructor Create( aEventName: String = ''; aManualReset:boolean = true ); virtual;
    destructor Destroy; override;
    function Init ( aSetEvent : boolean = true ): Boolean; virtual; // aSetEvent is the initial state, true = SetEvent
    function WaitEvent: Boolean;  // wait until anybody calls SetEvent, if Autoreset is true NT will call ResetEvent
    function SetEvent: Boolean;   // kick on the waiting thread(s) and all threads follow them
    function ResetEvent: Boolean; // threads have to wait when calling WaitEvent
    function PulseEvent: Boolean; // kick on only the actually waiting threads

    property Error: DWord read fError;
    property TimeOut: DWord read fTimeOut write fTimeOut;
  end;

  //...........................................................................
  PIPCClient = ^TIPCClient;
  TIPCClient = class(TObject)
  private
    fConnectErr : DWord;
    fError : DWord;
    fFirstWriteErr : DWord;
    fSecondWriteErr : DWord;
    mOverlapped: TMMOverlapped;
    mPOverlapped: PMMOverlapped;
    mPipeName: String;
    mPipeHandle: THandle;
  protected
  public
    constructor Create(aCompName: String; aPipeName: String; aOverlTmo: Integer = -1); virtual;
    destructor Destroy; override;
    function Connect : Boolean;
    procedure DisConnect;
    function Write(aBuffer: PByte; aSize: DWord): Boolean;
    property ConnectErr: DWord read fConnectErr;
    property Error: DWord read fError;
    property FirstWriteErr: DWord read fFirstWriteErr;
    property SecondWriteErr: DWord read FSecondWriteErr;
  end;

  //...........................................................................
  // The inherited CriticalSection protects the thread list
  // The additional CriticalSection mPipeCS protects write into the local pipe
  PIPCServer = ^TIPCServer;
  TIPCServer = class(TCriticalSection)
  private
    mMaxPipe: Integer;
    mMaxPipeReached: Boolean;
    mPipeCS: TCriticalSection;
    mPipeName: String;
    mReadHandle: THandle;
    mSize: DWord;
    mTimeOut: Integer;
    mThreadList: TmmList;
    mWriteHandle: THandle;
    mUserOrGroup: String;

    fError: DWord;
    fPipeWait: Boolean;
    procedure SetPipeWait(aValue: Boolean);
  protected
    function CreateNewPipeReader: Boolean;
    function HandleValid(aHandle: THandle): Boolean;
    function RemovePipeReader(aHandle: THandle): Boolean;
    function WriteToLocalPipe(aBuffer: PByte; aSize: DWord): Boolean;
  public
    property Error: DWord read fError;
    property PipeWait: Boolean read fPipeWait write SetPipeWait default True;
    constructor Create(aPipeName: String; aUserOrGroup: String; aSize: DWord; aTimeOut: Integer; aMaxPipe: Integer); reintroduce;
    destructor Destroy; override;
    function Init: Boolean;
    function Read(aBuffer: PByte; aMaxSize: DWord; var aCount: DWord): Boolean;
    function ReadDynamic(var aBuffer: PByte; var aMaxSize: DWord; var aCount: DWord): Boolean;
  end;

  //...........................................................................
  TPipeThread = class(TmmThread)
  private
    mBuffer: PByte;
    mReceiveBuffer: TMemoryStream;
    mParent: TIPCServer;
    mPipeName: String;
    mReadHandle: THandle;
    mSize: DWord;
    mTimeOut: Integer;
    fError: DWord;
    function GetHandleValid: Boolean;
  protected
    procedure Execute; override;
    property HandleValid: Boolean read GetHandleValid;
  public
    property Error: DWord read fError;
    constructor Create(aParent: TIPCServer; aPipeName: String; aSize: DWord; aTimeOut: Integer); virtual;
    destructor Destroy; override;
    function Init(aUserOrGroup: String): Boolean;
  end;

  //...........................................................................
  TCriticalList = class(TmmList)
  private
    mCriticalSection: TCriticalSection;
  protected
    fListError: DWord;
  public
    property ListError: DWord read fListError;
    constructor Create; virtual;
    destructor Destroy; override;
    procedure LockList;
    procedure UnlockList;
  end;

  //...........................................................................
  TProtectedList = TCriticalList;


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  LoepfeGlobal;

const
  cConfirmPipeSize       = 2;
  cConfirmPipeChar       = 'X';
  cErrWriteWait          = 1000;          // one second
  cEventWaitTmo          = INFINITE;      // wait forever
  cMutexWaitTmo          = INFINITE;      // wait forever
  cServerPipeNameLocal   = 'LOCALPIPE';
  cMaxPipePacketSize     = 65530;         // byte
  cPipePacketTmo         = 1000;          // ms

const
  cPipeTransferHeaderSize = 2;
type
  PPipeTransferRec = ^TPipeTransferRec;
  TPipeTransferRec = packed record
    ID: Word;
    Data: Pointer;  // dient lediglich dazu, einen Anhaltspunkt f�r den Move() Befehl zu dienen
  end;

//******************************************************************************
// TBaseMutex
//******************************************************************************
constructor TBaseMutex.Create(aMutexName: String);
begin
  inherited Create;

  fError   := NO_ERROR;
  fTimeOut := cMutexWaitTmo;                   // default wait forever

  mMutex     := INVALID_HANDLE_VALUE;
  mMutexName := aMutexName;
end;
//-----------------------------------------------------------------------------
destructor TBaseMutex.Destroy;
begin
  CloseHnd(mMutex);
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TBaseMutex.FreeMutex: Boolean;
begin
  Result := FreeMux(mMutex);
  if not Result then
    fError := GetLastError;
end;
//-----------------------------------------------------------------------------
function TBaseMutex.GetMutex: Boolean;
begin
  Result := GetMux(mMutex, TimeOut);
  if not Result then
    fError := GetLastError;
end;
//-----------------------------------------------------------------------------
function TBaseMutex.Init: Boolean;
begin
  Result := CreateMux(mMutex, mMutexName);
  if Result then fError := NO_ERROR
  else           fError := GetLastError;
end;
//-----------------------------------------------------------------------------

//******************************************************************************
// TEvent
//******************************************************************************
constructor TEvent.Create( aEventName: String = ''; aManualReset:boolean = true );
begin
  inherited Create;
  mEventName := aEventName;
  mManualReset := aManualReset;
  fTimeOut := cEventWaitTmo;
end;
//------------------------------------------------------------------------------
destructor TEvent.Destroy;
begin
  CloseHandle ( mEvent );
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TEvent.Init ( aSetEvent : boolean = true ): Boolean;
begin
  Result := CreateEvent( mEvent,  mEventName, mManualReset, aSetEvent );
  if Result then fError := NO_ERROR
  else           fError := GetLastError;
end;
//------------------------------------------------------------------------------
function TEvent.WaitEvent: Boolean;
begin
  Result := IPCUnit.WaitEvent( mEvent, fTimeOut);
  if not Result then
    fError := GetLastError;
end;
//------------------------------------------------------------------------------
function TEvent.SetEvent: Boolean;
begin
  Result := IPCUnit.SetEvent( mEvent );
  if not Result then
    fError := GetLastError;
end;
//------------------------------------------------------------------------------
function TEvent.ResetEvent: Boolean;
begin
  Result := IPCUnit.ResetEvent( mEvent );
  if not Result then
    fError := GetLastError;
end;
//------------------------------------------------------------------------------
function TEvent.PulseEvent: Boolean;
begin
  Result := IPCUnit.PulseEvent( mEvent );
  if not Result then
    fError := GetLastError;
end;


//******************************************************************************
// TIPCClient
//******************************************************************************
constructor TIPCClient.Create(aCompName: String; aPipeName: String; aOverlTmo: Integer);
begin
  inherited Create;

  if aOverlTmo > 0 then begin
    mOverlapped.Timeout := aOverlTmo;
    CreateEvent(mOverlapped.Overl.hEvent, aPipeName + IntToStr(GetTickCount), False, False);
    mPOverlapped := @mOverlapped
  end else
    mPOverlapped := Nil;

  mPipeHandle := INVALID_HANDLE_VALUE;
  mPipeName := Format('\\%s\pipe\%s', [aCompName, aPipeName]);

  // initialize local error codes
  fConnectErr     := NO_ERROR;
  fError          := NO_ERROR;
  fFirstWriteErr  := NO_ERROR;
  fSecondWriteErr := NO_ERROR;
end;
//-----------------------------------------------------------------------------
destructor TIPCClient.Destroy;
begin
  if Assigned(mPOverlapped) then begin
    mPOverlapped := Nil;
    CloseHnd(mOverlapped.Overl.hEvent);
  end;

  CloseHnd(mPipeHandle);
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TIPCClient.Connect : Boolean;
var
  xBuffer: Array[1..cConfirmPipeSize] of Byte;
  xCount: DWord;
  i: Integer;
begin
  Result := False;
  // if a connection is open close it first
  if mPipeHandle <> INVALID_HANDLE_VALUE then
    CloseHnd(mPipeHandle);

  // make a connect do IPCServer
  for i:= 1 to 2 do begin
    if ClientConnectNamedPipe(mPipeHandle, mPipeName, omReadWrite, NMPWAIT_USE_DEFAULT_WAIT, Assigned(mPOverlapped)) then begin
      // now wait for an answer from server side to be sure the connection is established
      Result := ReadIPC(mPipeHandle, @xBuffer, cConfirmPipeSize, xCount);
      // NOW the server side is ready to read and
      // the transmitted information isn't interesting
      Break;
    end;
    // in case of not connected: wait
    Sleep(10);
  end;

  if not Result then begin
    fError      := GetLastError;
    fConnectErr := fError;
    CloseHnd(mPipeHandle);
  end;
end;
//-----------------------------------------------------------------------------
procedure TIPCClient.DisConnect;
begin
  CloseHnd(mPipeHandle);

  fConnectErr := NO_ERROR;
  fError := NO_ERROR;
  fFirstWriteErr := NO_ERROR;
  fSecondWriteErr := NO_ERROR;
end;

//-----------------------------------------------------------------------------
function TIPCClient.Write(aBuffer: PByte; aSize: DWord): Boolean;
var
  xSize: DWord;
  xBuffer: PByte;
  xPacket: PPipeTransferRec;
  xPacketID: Word;
  xAllSent: Boolean;
  //.................................................................
  function LocalWrite(aBuffer: PByte; aSize: DWord): Boolean;
  begin
    Result := WriteIPC(mPipeHandle, aBuffer, aSize, mPOverlapped);
    if not Result then begin
      if Assigned(mPOverlapped) then begin
        Sleep(1);  // allow task switching
        Result := WriteIPC(mPipeHandle, aBuffer, aSize, mPOverlapped);
        if not Result then
          fError := GetLastError;
      end else begin
        // write is faild
        fError := GetLastError;
        fFirstWriteErr := fError;
        case fError of
          ERROR_BROKEN_PIPE, ERROR_NO_DATA, ERROR_PIPE_NOT_CONNECTED : begin
              if Connect then begin       // make a connect and then try to write again
                Result := WriteIPC(mPipeHandle, aBuffer, aSize);
                if not Result then begin
                  // second write faild also notify error code and close pipe
                  fError          := GetLastError;
                  fSecondWriteErr := fError;
                  CloseHnd(mPipeHandle);
                end;
              end;
            end;
          ERROR_PIPE_BUSY : begin
              // pipe where busy: try again to write
              Sleep(cErrWriteWait);
              Result := WriteIPC(mPipeHandle, aBuffer, aSize);
              if not Result then begin
                // second write faild also notify error code and close pipe
                fError          := GetLastError;
                fSecondWriteErr := fError;
                CloseHnd(mPipeHandle);
              end;
            end;
        else
          // on every other error close connection
          CloseHnd(mPipeHandle);
        end;  // case
      end;  // if Assinged() else
    end; // if not Result
  end;
  //.................................................................
begin
  Result          := TRUE;
  fConnectErr     := NO_ERROR;
  fError          := NO_ERROR;
  fFirstWriteErr  := NO_ERROR;
  fSecondWriteErr := NO_ERROR;

  // if there is no connection -> create a connection to IPCServer
  if mPipeHandle = INVALID_HANDLE_VALUE then
    Result := Connect;

  if Result then
  try
    xAllSent  := False;
    xPacket   := Nil;
    xPacketID := (aSize div cMaxPipePacketSize);
    // Startadresse der Nutzdaten setzen
    xBuffer   := aBuffer;
    while Result and (not xAllSent) do begin
      if xPacketID=0 then xSize := (aSize mod cMaxPipePacketSize)
                     else xSize := cMaxPipePacketSize;

      // entsprechend Speicher reservieren und Daten hineinkopieren
      ReallocMem(xPacket, xSize + cPipeTransferHeaderSize);
      xPacket^.ID := xPacketID;
      Move(xBuffer^, xPacket^.Data, xSize);

      Result   := LocalWrite(PByte(xPacket), xSize + cPipeTransferHeaderSize);
      xAllSent := (xPacketID = 0);
      if not xAllSent then begin
        // Startadresse der Nutzdaten f�r das n�chste Packet "berechnen"
        xBuffer := Pointer(DWord(xBuffer) + xSize);
        dec(xPacketID);
      end;
    end; // while
  finally
    // Speicher wieder freigeben
    ReallocMem(xPacket, 0);
  end;  // if Result
end;
////-----------------------------------------------------------------------------
//function TIPCClient.Write(aBuffer: PByte; aSize: DWord): Boolean;
//begin
//  Result := TRUE;
//  fConnectErr := NO_ERROR;
//  fError := NO_ERROR;
//  fFirstWriteErr := NO_ERROR;
//  fSecondWriteErr := NO_ERROR;
//
//  // if there is no connection -> create a connection to IPCServer
//  if mPipeHandle = INVALID_HANDLE_VALUE then
//    Result := Connect;
//
//  if Result then begin
//    Result := WriteIPC(mPipeHandle, aBuffer, aSize, mPOverlapped);
//
//    // error handling only if not in overlapped mode
//    if not Result then begin
//      if Assigned(mPOverlapped) then begin
//        Sleep(1);  // allow task switching
//        Result := WriteIPC(mPipeHandle, aBuffer, aSize, mPOverlapped);
//        if not Result then
//          fError := GetLastError;
//      end else begin
//        // write is faild
//        fError := GetLastError;
//        fFirstWriteErr := fError;
//        case fError of
//          ERROR_BROKEN_PIPE, ERROR_NO_DATA, ERROR_PIPE_NOT_CONNECTED : begin
//              if Connect then begin       // make a connect and then try to write again
//                Result := WriteIPC(mPipeHandle, aBuffer, aSize);
//                if not Result then begin
//                  // second write faild also notify error code and close pipe
//                  fError          := GetLastError;
//                  fSecondWriteErr := fError;
//                  CloseHnd(mPipeHandle);
//                end;
//              end;
//            end;
//          ERROR_PIPE_BUSY : begin
//              // pipe where busy: try again to write
//              Sleep ( cErrWriteWait );
//              Result := WriteIPC(mPipeHandle, aBuffer, aSize);
//              if not Result then begin
//                // second write faild also notify error code and close pipe
//                fError          := GetLastError;
//                fSecondWriteErr := fError;
//                CloseHnd(mPipeHandle);
//              end;
//            end;
//        else
//          // on every other error close connection
//          CloseHnd(mPipeHandle);
//        end;  // case
//      end;  // if Assinged() else
//     end;  // if not Result
//  end;  // if Result
//end;
//-----------------------------------------------------------------------------

//******************************************************************************
// TIPCServer
//******************************************************************************
constructor TIPCServer.Create(aPipeName: String; aUserOrGroup : String;
                              aSize: DWord; aTimeOut: Integer; aMaxPipe: Integer);
begin
  inherited Create;

  mMaxPipe     := aMaxPipe;
  mMaxPipeReached := False;
  mPipeCS      := TCriticalSection.Create;
  mPipeName    := aPipeName;
  mUserOrGroup := aUserOrGroup;
  mSize        := aSize;
  mTimeOut     := aTimeOut;
  mThreadList  := TmmList.Create;
  fError       := NO_ERROR;
  fPipeWait    := True;
end;
//-----------------------------------------------------------------------------
destructor TIPCServer.Destroy;
begin
  Enter; //Enter;
  try
    // destroy all existing threads
    while mThreadList.Count > 0 do
      with TPipeThread(mThreadList.Items[0]) do begin
        // it is necessary to use the strong way because the PipeThread hangs in ReadIPC or ConnectNPipe
        Free;
      end;
  finally
    Leave; //LeaveCriticSection;
  end;

  // close handle of local pipe and mutex and destroy list
  mThreadList.Free;
  mPipeCS.Free;

  CloseHnd(mReadHandle);
  CloseHnd(mWriteHandle);

  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TIPCServer.CreateNewPipeReader: Boolean;
var
  xThread: TPipeThread;
begin
  Result := False;
  // check for MaxPipe
  mMaxPipeReached := (mMaxPipe > 0) and (mThreadList.Count >= mMaxPipe);
  // creates a new PipeThread only if the limit is not reached
  if not mMaxPipeReached then begin
    xThread := TPipeThread.Create(Self, mPipeName, mSize, mTimeOut);
    // PipeThread creates new NamedPipe
    Enter; // Mg V1.12
    try
      Result := xThread.Init ( mUserOrGroup );
      if Result then begin
        { Mg V1.12
        Enter;
        try
          mThreadList.Add(xThread);
        finally
          Leave; //LeaveCriticSection;
        end;
        }
        mThreadList.Add(xThread);
        xThread.Resume;
      end else begin
        fError := xThread.Error;
        xThread.Free;
      end;
    finally
      Leave; //LeaveCriticSection;
    end;
  end else
    fError := ERROR_TOO_MANY_OPEN_FILES;
end;
//-----------------------------------------------------------------------------
function TIPCServer.HandleValid(aHandle: THandle): Boolean;
begin
  Result := (aHandle <> INVALID_HANDLE_VALUE);
end;
//-----------------------------------------------------------------------------
function TIPCServer.Init: Boolean;
var
  xName: String;
begin
  Result := False;
  if (Length(mPipeName) > 0) and (mSize > 0) then begin
    xName := mPipeName + cServerPipeNameLocal;
    // create local pipe from which the read function works and the thread writes data
    if CreateNPipe(mReadHandle, xName, omRead, pmMessage, {10 * {}mSize, 0, mTimeOut ) then begin
      xName := '\\.\pipe\' + xName;
      // creates a handle to client side of local pipe
      if CreateIPCFile(mWriteHandle, xName, omWrite) then begin
        // creates first PipeThread
        if mThreadList.Count = 0 then begin
          Result := CreateNewPipeReader;
          if not Result then
            fError := GetLastError;
        end else
          fError := ERROR_TOO_MANY_OPEN_FILES;
      end else  // CreateIPCFile
        fError := GetLastError;
    end else // CreateNPipe
      fError := GetLastError;
  end else
    fError := ERROR_INVALID_PARAMETER;

  if not Result then begin
    CloseHnd(mReadHandle);
    CloseHnd(mWriteHandle);
  end;
end;
//-----------------------------------------------------------------------------
function TIPCServer.Read(aBuffer: PByte; aMaxSize: DWord; var aCount: DWord): Boolean;
begin
  Result := False;
  if HandleValid(mReadHandle) then begin
    Result := ReadIPC(mReadHandle, aBuffer, aMaxSize, aCount);
    if not Result then begin
      fError := GetLastError;
    end;
  end else
    fError := ERROR_INVALID_HANDLE;
end;
//-----------------------------------------------------------------------------
function TIPCServer.ReadDynamic(var aBuffer: PByte; var aMaxSize: DWord; var aCount: DWord): Boolean;
var
  xCount: DWord;
  xTempBuff: PByte;
  xOffsetPtr: PByte;
begin
  Result := False;
  if HandleValid(mReadHandle) then begin
    ZeroMemory(aBuffer, aMaxSize);
    Result := ReadIPC(mReadHandle, aBuffer, aMaxSize, aCount);
    if not Result then begin
      fError := GetLastError;
      // aktuelle Buffergr�sse ist zu klein -> vergr�ssern
      if fError = ERROR_MORE_DATA then begin
        fError := NO_ERROR;
        // wieviele Bytes warten noch in der Pipe?
        if PeekNamedPipe(mReadHandle, Nil, 0, Nil, Nil, @xCount) then begin
          // Speicherbereich mit neuer Gr�sse erstellen...
          xTempBuff := AllocMem(aMaxSize + xCount);
          // ...und bereits gelesener Inhalt umkopieren
          Move(aBuffer^, xTempBuff^, aMaxSize);
          // alter Buffer freigeben und gleich auf neuen zeigen lassen
          FreeMem(aBuffer, aMaxSize);
          aBuffer := xTempBuff;
          // den Rest aus der Pipe an diese Offsetstelle lesen
          xOffsetPtr := Pointer(DWord(aBuffer) + aMaxSize);
          // neue Gr�sse merken
          aMaxSize := aMaxSize + xCount;
          aCount   := aMaxSize;  // Buffergr�sse und gelesene Bytes sind nun ja gleichgross
          // 1. xCount = wieviel Platz noch verf�gbar ist, 2. xCount empfangene Bytes
          Result := ReadIPC(mReadHandle, xOffsetPtr, xCount, xCount);
          if not Result then
            fError := GetLastError;
        end // if PeeKNamedPipe()
        else
          fError := GetLastError;
      end; // if fError
    end;
  end else
    fError := ERROR_INVALID_HANDLE;
end;
//-----------------------------------------------------------------------------
function TIPCServer.RemovePipeReader(aHandle: THandle): Boolean;
var
  i: Integer;
begin
  Result := False;
  Enter;
  try
    for i:=0 to mThreadList.Count-1 do begin
      Result := (aHandle = TPipeThread(mThreadList.Items[i]).Handle);
      if Result then begin
        mThreadList.Delete(i);
        break;
      end else
        fError := ERROR_INVALID_DATA;
    end;
  finally
    Leave; //LeaveCriticSection;
  end;
  // if there where all pipe used before and one client has disconnected create the reserve pipe again
  if mMaxPipeReached then
    CreateNewPipeReader;
end;
//-----------------------------------------------------------------------------
procedure TIPCServer.SetPipeWait(aValue: Boolean);
begin
  if (aValue <> fPipeWait) and HandleValid(mReadHandle) then begin
    fPipeWait := aValue;
    if not SetNPipeWaitState(mReadHandle, fPipeWait) then
      fError := GetLastError;
  end;
end;
//-----------------------------------------------------------------------------
function TIPCServer.WriteToLocalPipe(aBuffer: PByte; aSize: DWord): Boolean;
begin
  Result := False;
  if (aSize > 0) and HandleValid(mWriteHandle) then begin
    mPipeCS.Enter;
    try
      Result := WriteIPC(mWriteHandle, aBuffer, aSize);
      if not Result then
        fError := GetLastError;
    finally
      mPipeCS.Leave; //LeaveCriticSection;
    end;
  end else
    fError := ERROR_INTERNAL_ERROR;
end;
//-----------------------------------------------------------------------------


//******************************************************************************
// TPipeThread
//******************************************************************************
constructor TPipeThread.Create(aParent: TIPCServer; aPipeName: String; aSize: DWord; aTimeOut: Integer);
begin
  inherited Create(True);

  FreeOnTerminate := True;

  mBuffer        := Nil;
  mReceiveBuffer := TMemoryStream.Create;
  mParent        := aParent;
  mPipeName      := aPipeName;
  mReadHandle    := INVALID_HANDLE_VALUE;
  mSize          := aSize;
  mTimeOut       := aTimeOut;

  fError     := NO_ERROR;
end;
//-----------------------------------------------------------------------------
destructor TPipeThread.Destroy;
var
  xRemoteTerminated: Boolean;
begin
  xRemoteTerminated := (ThreadID <> GetCurrentThreadID);

  // PipeThread is terminated from external
  if xRemoteTerminated then
    Suspend                 // prevent thread of using some removable variables
  else
    CloseHnd(mReadHandle);  // free ReadHandle only on SelfTermination (else program hangs)

  // cleanup
  FreeMem(mBuffer, mSize);
  mReceiveBuffer.Free;
  // Remove myself from parents thread list
  mParent.RemovePipeReader(Handle);


  // PipeThread is terminated from external
  if xRemoteTerminated then
    // use the hammer methode, because there is no way to terminate if thread
    // is in ReadIPC() methode
    TerminateThread(Handle, 0);

  inherited Destroy;
end;
//-----------------------------------------------------------------------------
procedure TPipeThread.Execute;
var
  xCount: DWord;
  xChar: Char;
  xPacketID: Word;
  xReadOK: Boolean;
begin
  // Wait until a Client has connected.
  if ConnectNPipe(mReadHandle) then begin
    // Somethings is happened to this pipe, create new one as reserve
    mParent.CreateNewPipeReader;
    // New PipeThread is created. Now send just one message to my client to confirm this connection
    // It doesn't matter what we send.
    xChar := cConfirmPipeChar;
    if WriteIPC(mReadHandle, @xChar, 1) then begin
      fError    := NO_ERROR;
      xPacketID := Word(-1);
      // loop until termination
      while not Terminated do begin
        xReadOK := ReadIPC(mReadHandle, mBuffer, mSize, xCount);
        if xReadOK or (GetLastError = ERROR_MORE_DATA) then begin
          // erstmal die erhaltene Meldung direkt im Buffer zwischenspeichern (ohne Header),
          // auch wenn sie nicht komplett ist (xReadOK = False)
          mReceiveBuffer.Write(PPipeTransferRec(mBuffer)^.Data, xCount - cPipeTransferHeaderSize);
          xPacketID := PPipeTransferRec(mBuffer)^.ID;
          if not xReadOK then begin
            // wenn der Fehler wegen zu kleinem Buffer war, dann einfach Gr�sse anfragen
            // und Buffer vergr�ssern
            if PeekNamedPipe(mReadHandle, Nil, 0, Nil, Nil, @xCount) then begin
              // Neue Gesamtgr�sse bestimmen
              mSize := mSize + xCount;
              // Empfangsbuffer vergr�ssern...
              ReallocMem(mBuffer, mSize);
              // ...noch den Rest aus der Pipe lesen...
              xReadOK := ReadIPC(mReadHandle, mBuffer, mSize, xCount);
              if xReadOK then
                // ...und schreibe diesen direkt in den Buffer -> hier hat es keinen Header mehr zu beachten
                mReceiveBuffer.Write(mBuffer^, xCount);
            end; // if PeekNamedPipe()
          end;
        end
        else begin
          // Client has finished its connection. Terminate myself
          Terminate;
        end; // if xReadOK or ()

        if xReadOK then begin
          // Letztes oder einzigstes Packet empfangen -> weiterleiten
          if xPacketID = 0 then
          try
            mReceiveBuffer.Position := 0;
            // write Msg to local pipe
            if not mParent.WriteToLocalPipe(mReceiveBuffer.Memory, mReceiveBuffer.Size) then
              fError := mParent.Error;
          finally
            mReceiveBuffer.Clear;
          end;
        end else
          fError := GetLastError;
      end; // while not Terminated
    end; // if WriteIPC()
  end else begin
    // there was an error
    fError := GetLastError;
  end;
end;
////-----------------------------------------------------------------------------
function TPipeThread.GetHandleValid: Boolean;
begin
  Result := (mReadHandle <> INVALID_HANDLE_VALUE);
end;
//-----------------------------------------------------------------------------
function TPipeThread.Init ( aUserOrGroup: String ): Boolean;
begin
  Result := False;
  if CreateNPipe(mReadHandle, mPipeName, omReadWrite, pmMessage, mSize,
                 cConfirmPipeSize, mTimeOut) then begin
    if aUserOrGroup <> '' then begin
      try
        Result := SetKernelObjectSecurity ( mReadHandle, PChar ( aUserOrGroup ), true );
      finally
      end;
      if not Result then begin
        fError := GetLastError;
        CloseHnd(mReadHandle);
        Exit;
      end;
    end;

    try
      // allocate buffer for receiving messages
      mBuffer := AllocMem(mSize);
      Result  := True;
    except // EOutOfMemory
      fError := ERROR_NOT_ENOUGH_MEMORY;
    end;
  end else
    fError := GetLastError;

  if not Result then begin
    CloseHnd(mReadHandle);
  end;
end;
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TCriticalList
//-----------------------------------------------------------------------------
constructor TCriticalList.Create;
begin
  inherited Create;
  mCriticalSection := TCriticalSection.Create;
end;
//-----------------------------------------------------------------------------
destructor TCriticalList.Destroy;
begin
  LockList;  // Make sure nobody else is inside the list.
  try
    inherited Destroy;
  finally
    UnlockList;
    mCriticalSection.Free;
  end;
end;
//-----------------------------------------------------------------------------
procedure TCriticalList.LockList;
begin
  mCriticalSection.Enter;
end;
//-----------------------------------------------------------------------------
procedure TCriticalList.UnlockList;
begin
  mCriticalSection.Leave;
end;



begin
end.

