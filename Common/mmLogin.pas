(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: LoginComp.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Hauptunit der MillMaster Login-Komponente
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 21.09.1998  0.00  Sdo | Projekt erstellt
| 01.12.1998  1.00  Wss | LoginForm und Komponente in gleicher Unit: mmLogin.pas
| 11.03.1999  1.01  Wss | Security information were written as wrong names in registry (cRegDefxxx)
|                       | Added some messages to inform user of success or failed login
| 27.05.1999  1.02  Wss | Improved
| 04.11.1999  1.03  Wss | Login changed to without database
| 11.02.2000  1.04  Wss | Nochmals ueberarbeitet
| 13.04.2000  1.04  Wss | CheckEingabe: Host parameter at LogonUser() set to nil. See WinAPI Help
| 30.08.2000  1.04  Wss | Get UserGroups from PDC too. If MM runs on additional server it doesn't
                          contain any user/group information -> read of group were "empty"
| 15.03.2001  1.04  Wss | protocol Login/Logout to event log
| 18.02.2004  1.04  Wss | - Login mit leerem Passwort nun m�glich
                          - Meldung beim Logout korrigiert mit aktiven MM User
| 17.10.2005  1.04  Wss | - CheckEingabe() mit try/finally etwas verbessert
                          - Aufrufende Funktionen zum Auslesen der MemberOf nun �ber
                            NetAPI32 gel�st um Fehler aus Floor zu l�sen
|=============================================================================*)
unit mmLogin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, IvDictio, IvMulti, IvEMulti,
  IvAMulti, IvBinDic, mmBitBtn, BaseForm, mmTranslator, mmDictionary,
  mmImage, mmPanel, mmEdit, mmLabel, mmBevel;

resourcestring
  rsMsgLoginWrong = '(*)Benutzername oder Passwort ist nicht korrekt.'; //ivlm
  rsMsgLoginOK = '(*)Sie sind nun als %s am MillMaster angemeldet.'; //ivlm
  rsMsgLogout = '(*)Benutzer %s ist nun vom MillMaster abgemeldet.'; //ivlm

type
  TComponentStyleEnum = (csInheritable, csCheckPropAvail);
  TComponentStyleSet = set of TComponentStyleEnum;
  //...........................................................................
  TMMLogin = class(TCommonDialog)
  private
    fShowLineToTrayIcon: Boolean;
    fDictionary: TmmDictionary;
    procedure SetDictionary(const Value: TmmDictionary);
  protected
  public
    constructor Create(aOwner: TComponent); override;
    function Execute: Boolean; override;
    procedure Logout;

  published
    property ShowLineToTrayIcon: Boolean read fShowLineToTrayIcon write fShowLineToTrayIcon;
    property Dictionary: TmmDictionary read fDictionary write SetDictionary;
  end;
  //...........................................................................
  TFLoginMain = class(TmmForm)
    laLoginname: TmmLabel;
    laPassword: TmmLabel;
    edLoginname: TmmEdit;
    edPassword: TmmEdit;
    Panel1: TmmPanel;
    bLogout: TmmBitBtn;
    bCancel: TmmBitBtn;
    bLogin: TmmBitBtn;
    mTranslator: TmmTranslator;
    Image1: TmmImage;
    mmPanel1: TmmPanel;
    mmLabel1: TmmLabel;
    lMMUser: TmmLabel;
    procedure bLogoutClick(Sender: TObject);
    procedure bLoginClick(Sender: TObject);
    procedure EditChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    mMessageApplID: WORD;
    fLoginSuccess: Boolean;
    function CheckEingabe(aLoginname, aPasswort: string): Boolean;
    function GetMMLoginUser: Boolean;
    procedure DeleteUserReg;
  public
    procedure Logout;
    property LoginSuccess: Boolean read fLoginSuccess;
  end;

  //...........................................................................

var
  FLoginMain: TFLoginMain;

implementation // 15.07.2002 added mmMBCS to imported units

{$R *.DFM}
uses
  mmMBCS,

  mmCS,
  LoepfeGlobal, MMEventLog, ShellAPI, mmRegistry, NetAPI32MM, MMMessages;

//******************************************************************************
//* TMMLogin
//******************************************************************************
constructor TMMLogin.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fShowLineToTrayIcon := False;
end;
//------------------------------------------------------------------------------
function TMMLogin.Execute: Boolean;
begin
  with TFLoginMain.Create(Self) do try
    mTranslator.Dictionary := Self.Dictionary;
    mTranslator.Translate;
    Result := (ShowModal = mrOK);
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
{
procedure TMMLogin.HideLogout;
begin
  with TFLoginMain.Create(Self) do
  try
    HideLogout;
  finally
    Free;
  end;
end;
{}
//------------------------------------------------------------------------------
procedure TMMLogin.Logout;
begin
  with TFLoginMain.Create(Self) do try
    Logout;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMLogin.SetDictionary(const Value: TmmDictionary);
begin
  if Assigned(Value) then
    fDictionary := Value
  else
    fDictionary := nil;
end;

//******************************************************************************
//* TFLoginMain
//******************************************************************************
//------------------------------------------------------------------------------
procedure TFLoginMain.bLogoutClick(Sender: TObject);
begin
  fLoginSuccess := True;
  Logout;
  MessageDlg(Format(rsMsgLogout, [edLoginname.Text]), mtInformation, [mbOK], 0);
end;
//------------------------------------------------------------------------------
procedure TFLoginMain.bLoginClick(Sender: TObject);
begin
  if edLoginname.Text = '' then
    edLoginname.SetFocus
  else begin
    fLoginSuccess := CheckEingabe(edLoginname.text, edPassword.text);
    // App. benachrichtigen
    if fLoginSuccess then begin
      WriteToEventLog(Format('Login MillMaster user: %s', [edLoginname.Text]), 'MMLogin: ', etSuccess, '', '', ssMMClient);
      MessageDlg(Format(rsMsgLoginOK, [edLoginname.Text]), mtInformation, [mbOK], 0);
      MMBroadCastMessage(mMessageApplID, cMsgLoginChanged);
    end
    else begin
      MessageDlg(rsMsgLoginWrong, mtError, [mbOK], 0);
      edLoginname.SetFocus;
    end;
  end; // if edLoginname.Text = ''
end;
//******************************************************************************
// Ueberprueft den Loginname & das Passwort.
// Bei Korrektheit wird das Benutzerecht in die Registry geschrieben und
// TRUE zurueckgegeben
//******************************************************************************
function TFLoginMain.CheckEingabe(aLoginname, aPasswort: string): Boolean;
var
  xUserGroups: TStringList;
  xLogonType: DWORD;
  xLogonProvider: DWORD;
  xToken: THANDLE;
begin
  Result := False;
  if aLoginname <> '' then begin
    Screen.Cursor          := crHourGlass;
    xUserGroups            := TStringList.Create;
    xUserGroups.Duplicates := dupIgnore;
    try
      // Passwort ueberpruefen
      xLogonType     := LOGON32_LOGON_NETWORK; //Orginal
      xLogonProvider := LOGON32_PROVIDER_DEFAULT;

      // check if Username and Password are correct
      if not LogonUser(PChar(aLoginname), nil, // PChar(gMMHost),  Wss: see WinApi Help
        PChar(aPasswort), xLogonType, xLogonProvider, xToken) then begin
        Beep;
        Sleep(100);
        Screen.Cursor := crDefault;
        exit;
      end;
      CloseHandle(xToken);

      // PWD & Loginname sind i.O.
      if CodeSite.Enabled then begin
        // wss: get user groups from local computer instead of local groups of server
        GetUserLocalGroupName(aLoginName, '', xUserGroups);
        CodeSite.SendStringList('Local User groups', xUserGroups);
        xUserGroups.Clear;

        GetUserGroupName(aLoginName, gMMHost, xUserGroups);
        CodeSite.SendStringList('gMMHost User groups', xUserGroups);
        xUserGroups.Clear;
        // Usergruppen vom DomainController  -> TStringList;
        GetUserGroupName(aLoginName, gDomainController, xUserGroups);
        CodeSite.SendStringList('gDomainController User groups', xUserGroups);
        CodeSite.AddSeparator;
        xUserGroups.Clear;
      end;

      // Usergruppen ermitteln
      // Usergruppen Local  -> TStringList;
      // wss: get user groups from local computer instead of local groups of server
      GetUserLocalGroupName(aLoginname, '', xUserGroups);
      // Usergruppen auf MM-Server  -> TStringList;
      GetUserGroupName(aLoginname, gMMHost, xUserGroups);
      // Usergruppen vom DomainController  -> TStringList;
      GetUserGroupName(aLoginname, gDomainController, xUserGroups);

      // LogonUserName, Rechte und LogonTime in Registry schreiben
      with TmmRegistry.Create do
      try
        RootKey := cRegCU;
        OpenKey(cRegMMSecurityPath, True);

        WriteString(cRegLogonUserName, aLoginname);
        WriteString(cRegLogonUserGroups, xUserGroups.CommaText);
        WriteString(cRegLogonTime, DateTimeToStr(Now));
        Result := True;
      finally
        Free;
      end;
    finally
      xUserGroups.Free;
      Screen.Cursor := crDefault;
    end;
  end
  else // if username and password
    Beep;
end; // END CheckEingabe
//------------------------------------------------------------------------------
procedure TFLoginMain.EditChange(Sender: TObject);
begin
  bLogin.Default := (edLoginName.Text <> '') and (edPassword.Text <> '');
end;
//------------------------------------------------------------------------------
procedure TFLoginMain.FormCreate(Sender: TObject);
begin
  fLoginSuccess := False;

  mMessageApplID := RegisterWindowMessage(cMsgAppl);

  // Abmelde-Button auf disable setzen wenn DefUser in Reg.
  bLogout.Enabled := GetMMLoginUser;

  lMMUser.Caption := GetCurrentMMUser;
end;
//******************************************************************************
// Ermittelt ModalResult aus den Buttons
//******************************************************************************
procedure TFLoginMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then
    CanClose := fLoginSuccess;
end;
//******************************************************************************
// Ermittelt ob ein DefUser in Reg. vorhanden ist
//******************************************************************************
function TFLoginMain.GetMMLoginUser: Boolean;
begin
  edLoginName.Text := GetRegString(cRegCU, cRegMMSecurityPath, cRegLogonUserName);
  Result := edLoginname.Text <> '';
end;
//******************************************************************************
// Logout: Loescht LogonUser Eintrag aus Reg.
//******************************************************************************
procedure TFLoginMain.Logout;
begin
  WriteToEventLog(Format('Logout MillMaster user: %s', [GetCurrentMMUser]), 'MMLogin: ', etSuccess, '', '', ssMMClient);
  DeleteUserReg;

  // App. benachrichtigen
  MMBroadCastMessage(mMessageApplID, cMsgLoginChanged);
end;
//------------------------------------------------------------------------------
procedure TFLoginMain.DeleteUserReg;
begin
  // MMUsername und MMRight aus Reg. loeschen
  with TmmRegistry.Create do try
    RootKey := cRegCU;
    if OpenKey(cRegMMSecurityPath, False) then begin
      DeleteValue(cRegLogonUserName);
      DeleteValue(cRegLogonUserGroups);
      DeleteValue(cRegLogonTime);
    end;
  finally
    Free;
  end;
end;

end.

