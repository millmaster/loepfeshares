(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: NTApis.pas
| Projectpart...: MillMaster NT Spulerei
| Description...: NT Apis which not available in Borland Dephi
| Info..........: -
| Develop.system: Compaq Deskpro Pentium Pro 200, Windows NT 4.0
| Target.system.: Windows NT
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 07.06.99 | 1.00 | Mg | Datei erstellt
|=============================================================================*)

unit NTApis;

interface
uses Windows;
//------------------------------------------------------------------------------
// Constants Types and Procedures for NT-Services
const
  SEMAPHORE_MODIFY_STATE      = $0002;
  SEMAPHORE_ALL_ACCESS        = STANDARD_RIGHTS_REQUIRED Or SYNCHRONIZE Or $3;

  SERVICE_KERNEL_DRIVER       = $00000001;
  SERVICE_FILE_SYSTEM_DRIVER  = $00000002;
  SERVICE_ADAPTER             = $00000004;
  SERVICE_RECOGNIZER_DRIVER   = $00000008;

  SERVICE_DRIVER              = SERVICE_KERNEL_DRIVER
                             Or SERVICE_FILE_SYSTEM_DRIVER
                             Or SERVICE_RECOGNIZER_DRIVER;

  SERVICE_WIN32_OWN_PROCESS   = $00000010;
  SERVICE_WIN32_SHARE_PROCESS = $00000020;
  SERVICE_WIN32               = SERVICE_WIN32_OWN_PROCESS
                             Or SERVICE_WIN32_SHARE_PROCESS;

  SERVICE_INTERACTIVE_PROCESS = $00000100;

  SERVICE_TYPE_ALL            = SERVICE_WIN32
                             Or SERVICE_ADAPTER
                             Or SERVICE_DRIVER
                             Or SERVICE_INTERACTIVE_PROCESS;

  { Start Type }
  SERVICE_BOOT_START        =     $00000000;
  SERVICE_SYSTEM_START      =     $00000001;
  SERVICE_AUTO_START        =     $00000002;
  SERVICE_DEMAND_START      =     $00000003;
  SERVICE_DISABLED          =     $00000004;

  { Error control type }
  SERVICE_ERROR_IGNORE      =     $00000000;
  SERVICE_ERROR_NORMAL      =     $00000001;
  SERVICE_ERROR_SEVERE      =     $00000002;
  SERVICE_ERROR_CRITICAL    =     $00000003;

  { The types of events that can be logged. }
  EVENTLOG_SUCCESS          =     $0000;
  EVENTLOG_ERROR_TYPE       =     $0001;
  EVENTLOG_WARNING_TYPE     =     $0002;
  EVENTLOG_INFORMATION_TYPE =     $0004;
  EVENTLOG_AUDIT_SUCCESS    =     $0008;
  EVENTLOG_AUDIT_FAILURE    =     $0010;


  SERVICES_ACTIVE_DATABASE = 'ServicesActive';
  SERVICES_FAILED_DATABASE = 'ServicesFailed';
  SC_GROUP_IDENTIFIER = '+';

  {- Value to indicate no change to an optional parameter}
  SERVICE_NO_CHANGE = $ffffffff;

  {- Service State -- for Enum Requests (Bit Mask)}
  SERVICE_ACTIVE    = $00000001;
  SERVICE_INACTIVE  = $00000002;
  SERVICE_STATE_ALL = SERVICE_ACTIVE Or SERVICE_INACTIVE;

  {- Controls}
  SERVICE_CONTROL_STOP             =  $00000001;
  SERVICE_CONTROL_PAUSE            =  $00000002;
  SERVICE_CONTROL_CONTINUE         =  $00000003;
  SERVICE_CONTROL_INTERROGATE      =  $00000004;
  SERVICE_CONTROL_SHUTDOWN         =  $00000005;

{ Service State -- for CurrentState }

  SERVICE_STOPPED                  = $00000001;
  SERVICE_START_PENDING            = $00000002;
  SERVICE_STOP_PENDING             = $00000003;
  SERVICE_RUNNING                  = $00000004;
  SERVICE_CONTINUE_PENDING         = $00000005;
  SERVICE_PAUSE_PENDING            = $00000006;
  SERVICE_PAUSED                   = $00000007;

{ Controls Accepted  (Bit Mask) }
  SERVICE_ACCEPT_STOP              = $00000001;
  SERVICE_ACCEPT_PAUSE_CONTINUE    = $00000002;
  SERVICE_ACCEPT_SHUTDOWN          = $00000004;

{ Service Control Manager object specific access types }
  SC_MANAGER_CONNECT               = $0001;
  SC_MANAGER_CREATE_SERVICE        = $0002;
  SC_MANAGER_ENUMERATE_SERVICE     = $0004;
  SC_MANAGER_LOCK                  = $0008;
  SC_MANAGER_QUERY_LOCK_STATUS     = $0010;
  SC_MANAGER_MODIFY_BOOT_CONFIG    = $0020;

  SC_MANAGER_ALL_ACCESS            = STANDARD_RIGHTS_REQUIRED
                                  Or SC_MANAGER_CONNECT
                                  Or SC_MANAGER_CREATE_SERVICE
                                  Or SC_MANAGER_ENUMERATE_SERVICE
                                  Or SC_MANAGER_LOCK
                                  Or SC_MANAGER_QUERY_LOCK_STATUS
                                  Or SC_MANAGER_MODIFY_BOOT_CONFIG;

{ Service object specific access type }
  SERVICE_QUERY_CONFIG             = $0001;
  SERVICE_CHANGE_CONFIG            = $0002;
  SERVICE_QUERY_STATUS             = $0004;
  SERVICE_ENUMERATE_DEPENDENTS     = $0008;
  SERVICE_START                    = $0010;
  SERVICE_STOP                     = $0020;
  SERVICE_PAUSE_CONTINUE           = $0040;
  SERVICE_INTERROGATE              = $0080;
  SERVICE_USER_DEFINED_CONTROL     = $0100;

  SERVICE_ALL_ACCESS               = STANDARD_RIGHTS_REQUIRED
                                  Or SERVICE_QUERY_CONFIG
                                  Or SERVICE_CHANGE_CONFIG
                                  Or SERVICE_QUERY_STATUS
                                  Or SERVICE_ENUMERATE_DEPENDENTS
                                  Or SERVICE_START
                                  Or SERVICE_STOP
                                  Or SERVICE_PAUSE_CONTINUE
                                  Or SERVICE_INTERROGATE
                                  Or SERVICE_USER_DEFINED_CONTROL;



type
  TSecurityInformation = Integer;
{ Handle Types }
  TSC_HANDLE = THandle;
  TSERVICE_STATUS_HANDLE = Integer;

{ Service Status Structure }

  TSERVICE_STATUS = Record
    dwServiceType,
    dwCurrentState,
    dwControlsAccepted,
    dwWin32ExitCode,
    dwServiceSpecificExitCode,
    dwCheckPoint,
    dwWaitHint: Integer
  End;

{ Service Status Enumeration Structure }

  TENUM_SERVICE_STATUSA = Record
    lpServiceName,
    lpDisplayName: PAnsiChar;
    ServiceStatus: TSERVICE_STATUS
  End;

  TENUM_SERVICE_STATUSW = Record
    lpServiceName,
    lpDisplayName: PWideChar;
    ServiceStatus: TSERVICE_STATUS
  End;

  TENUM_SERVICE_STATUS = TENUM_SERVICE_STATUSA;

{ Structures for the Lock API functions }

  TSC_LOCK = Pointer;

  TQUERY_SERVICE_LOCK_STATUSA = Record
    fIsLocked: Integer;
    lpLockOwner: PAnsiChar;
    dwLockDuration: Integer
  End;

  TQUERY_SERVICE_LOCK_STATUSW = Record
    fIsLocked: Integer;
    lpLockOwner: PWideChar;
    dwLockDuration: Integer
  End;

  TQUERY_SERVICE_LOCK_STATUS = TQUERY_SERVICE_LOCK_STATUSA;

{ Query Service Configuration Structure }

  TQUERY_SERVICE_CONFIGA = Record
    dwServiceType,
    dwStartType,
    dwErrorControl: Integer;
    lpBinaryPathName,
    lpLoadOrderGroup: PAnsiChar;
    dwTagId: Integer;
    lpDependencies,
    lpServiceStartName,
    lpDisplayName: PAnsiChar;
  End;

  TQUERY_SERVICE_CONFIGW = Record
    dwServiceType,
    dwStartType,
    dwErrorControl: Integer;
    lpBinaryPathName,
    lpLoadOrderGroup: PWideChar;
    dwTagId: Integer;
    lpDependencies,
    lpServiceStartName,
    lpDisplayName: PWideChar;
  End;

  TQUERY_SERVICE_CONFIG = TQUERY_SERVICE_CONFIGA;

{ Function Prototype for the Service Main Function }
  TService_Main_FunctionW = Procedure (dwNumServicesArgs: DWord; Var ArgVectors: PWideChar) StdCall;
  TService_Main_FunctionA = Procedure (dwNumServicesArgs: DWord; Var ArgVectors: PAnsiChar) StdCall;
  TService_Main_Function = Procedure (dwNumServicesArgs: DWord; Var ArgVectors: PChar) StdCall;

{ Service Start Table }
  TSERVICE_TABLE_ENTRYA = Record
    lpServiceName: PAnsiChar;
    lpServiceProc: TSERVICE_MAIN_FUNCTIONA
  End;

  TSERVICE_TABLE_ENTRYW = Record
    lpServiceName: PWideChar;
    lpServiceProc: TSERVICE_MAIN_FUNCTIONW
  End;

  TSERVICE_TABLE_ENTRY = TSERVICE_TABLE_ENTRYA;

{ Prototype for the Service Control Handler Function }
  THANDLER_FUNCTION = Procedure (dwControl: DWord) StdCall;

  Function ChangeServiceConfigA (hService: TSC_HANDLE; dwServiceType, dwStartType, dwErrorControl: Integer;
                                 BinaryPathName, LoadOrderGroup: PAnsiChar; Var lpdwTagId: Integer;
                                 Dependencies, ServiceStartName, Password, DisplayName: PAnsiChar): Bool; StdCall;
  Function ChangeServiceConfigW (hService: TSC_HANDLE; dwServiceType, dwStartType, dwErrorControl: Integer;
                                 BinaryPathName, LoadOrderGroup: PWideChar; Var lpdwTagId: Integer;
                                 Dependencies, ServiceStartName, Password, DisplayName: PWideChar): Bool; StdCall;
  Function ChangeServiceConfig  (hService: TSC_HANDLE; dwServiceType, dwStartType, dwErrorControl: Integer;
                                 BinaryPathName, LoadOrderGroup: PChar; Var lpdwTagId: Integer;
                                 Dependencies, ServiceStartName, Password, DisplayName: PChar): Bool; StdCall;
  Function CloseServiceHandle (hSCObject: TSC_HANDLE): Bool; StdCall;
  Function ControlService (hService: TSC_HANDLE; dwControl: Integer; Var ServiceStatus: TSERVICE_STATUS): Bool; StdCall;
  Function CreateServiceA (hSCManager: TSC_HANDLE; ServiceName, DisplayName: PAnsiChar;
                           dwDesiredAccess, dwServiceType,dwStartType,dwErrorControl: Integer;
                           BinaryPathName, lpLoadOrderGroup: PAnsiChar;
                           lpTagId: PInteger;
                           Dependencies, ServiceStartName, Password: PAnsiChar): TSC_Handle; StdCall;
  Function CreateServiceW (hSCManager: TSC_HANDLE; ServiceName, DisplayName: PWideChar;
                           dwDesiredAccess, dwServiceType,dwStartType,dwErrorControl: Integer;
                           BinaryPathName, lpLoadOrderGroup: PWideChar;
                           lpTagId: PInteger;
                           Dependencies, ServiceStartName, Password: PWideChar): TSC_Handle; StdCall;
  Function CreateService  (hSCManager: TSC_HANDLE; ServiceName, DisplayName: PChar;
                           dwDesiredAccess, dwServiceType,dwStartType,dwErrorControl: Integer;
                           BinaryPathName, lpLoadOrderGroup: PChar;
                           lpTagId: PInteger;
                           Dependencies, ServiceStartName, Password: PChar): TSC_Handle; StdCall;
  Function DeleteService (hService: TSC_HANDLE): Bool; StdCall;
  Function EnumDependentServicesA (hService: TSC_HANDLE; ServiceState: Integer;
                               Var Services: TENUM_SERVICE_STATUSA;
                                   cbBufSize: Integer;
                               Var cbBytesNeeded,ServicesReturned: Integer): Bool; StdCall;
  Function EnumDependentServicesW (hService: TSC_HANDLE; ServiceState: Integer;
                               Var Services: TENUM_SERVICE_STATUSW;
                                   cbBufSize: Integer;
                               Var cbBytesNeeded,ServicesReturned: Integer): Bool; StdCall;
  Function EnumDependentServices  (hService: TSC_HANDLE; ServiceState: Integer;
                               Var Services: TENUM_SERVICE_STATUS;
                                   cbBufSize: Integer;
                               Var cbBytesNeeded,ServicesReturned: Integer): Bool; StdCall;
  Function EnumServicesStatusA (hSCManager: TSC_HANDLE; ServiceType, ServiceState: Integer;
                            Var Services: TENUM_SERVICE_STATUSA;
                                cbBufSize: Integer;
                            Var cbBytesNeeded, ServicesReturned, ResumeHandle: Integer): Bool; StdCall;
  Function EnumServicesStatusW (hSCManager: TSC_HANDLE; ServiceType, ServiceState: Integer;
                            Var Services: TENUM_SERVICE_STATUSW;
                                cbBufSize: Integer;
                            Var cbBytesNeeded, ServicesReturned, ResumeHandle: Integer): Bool; StdCall;
  Function EnumServicesStatus  (hSCManager: TSC_HANDLE; ServiceType, ServiceState: Integer;
                            Var Services: TENUM_SERVICE_STATUS;
                                cbBufSize: Integer;
                            Var cbBytesNeeded, ServicesReturned, ResumeHandle: Integer): Bool; StdCall;
  Function GetServiceKeyNameA (hSCManager: TSC_HANDLE; DisplayName, ServiceName: PAnsiChar;
                           Var lpcchBuffer: Integer): Bool; StdCall;
  Function GetServiceKeyNameW (hSCManager: TSC_HANDLE; DisplayName, ServiceName: PWideChar;
                           Var lpcchBuffer: Integer): Bool; StdCall;
  Function GetServiceKeyName  (hSCManager: TSC_HANDLE; DisplayName, ServiceName: PChar;
                           Var lpcchBuffer: Integer): Bool; StdCall;
  Function GetServiceDisplayNameA (hSCManager: TSC_HANDLE; ServiceName, DisplayName: PAnsiChar;
                               Var cchBuffer: Integer): Bool; StdCall;
  Function GetServiceDisplayNameW (hSCManager: TSC_HANDLE; ServiceName, DisplayName: PWideChar;
                               Var cchBuffer: Integer): Bool; StdCall;
  Function GetServiceDisplayName  (hSCManager: TSC_HANDLE; ServiceName, DisplayName: PChar;
                               Var cchBuffer: Integer): Bool; StdCall;
  Function LockServiceDatabase (hSCManager: TSC_HANDLE): TSC_Handle; StdCall;
  Function NotifyBootConfigStatus (BootAcceptable: Bool): Bool; StdCall;

  Function OpenSCManagerA (MachineName, DatabaseName: PAnsiChar; DesiredAccess: Integer): TSC_Handle; StdCall;
  Function OpenSCManagerW (MachineName, DatabaseName: PWideChar; DesiredAccess: Integer): TSC_Handle; StdCall;
  Function OpenSCManager  (MachineName, DatabaseName: PChar; DesiredAccess: Integer): TSC_Handle; StdCall;
  Function OpenServiceA (hSCManager: TSC_HANDLE;lpServiceName: PAnsiChar; DesiredAccess: Integer): TSC_Handle; StdCall;
  Function OpenServiceW (hSCManager: TSC_HANDLE;lpServiceName: PWideChar; DesiredAccess: Integer): TSC_Handle; StdCall;
  Function OpenService  (hSCManager: TSC_HANDLE;lpServiceName: PChar; DesiredAccess: Integer): TSC_Handle; StdCall;
  Function QueryServiceConfigA (hService: TSC_HANDLE; Var ServiceConfig: TQUERY_SERVICE_CONFIGA;
                                cbBufSize: Integer; Var cbBytesNeeded: Integer): Bool; StdCall;
  Function QueryServiceConfigW (hService: TSC_HANDLE; Var ServiceConfig: TQUERY_SERVICE_CONFIGW;
                                cbBufSize: Integer; Var cbBytesNeeded: Integer): Bool; StdCall;
  Function QueryServiceConfig  (hService: TSC_HANDLE; Var ServiceConfig: TQUERY_SERVICE_CONFIG;
                                cbBufSize: Integer; Var cbBytesNeeded: Integer): Bool; StdCall;
  Function QueryServiceLockStatusA (hSCManager: TSC_HANDLE;
                                Var LockStatus: TQUERY_SERVICE_LOCK_STATUSA;
                                    cbBufSize: Integer;
                                Var cbBytesNeeded: Integer): Bool; StdCall;
  Function QueryServiceLockStatusW (hSCManager: TSC_HANDLE;
                                Var LockStatus: TQUERY_SERVICE_LOCK_STATUSW;
                                    cbBufSize: Integer;
                                Var cbBytesNeeded: Integer): Bool; StdCall;
  Function QueryServiceLockStatus  (hSCManager: TSC_HANDLE;
                                Var LockStatus: TQUERY_SERVICE_LOCK_STATUS;
                                    cbBufSize: Integer;
                                Var cbBytesNeeded: Integer): Bool; StdCall;
  Function QueryServiceObjectSecurity (hService: TSC_HANDLE; dwSecurityInformation: TSECURITYINFORMATION;
                                   Var SecurityDescriptor: TSECURITYDESCRIPTOR; cbBufSize: Integer;
                                   Var cbBytesNeeded: Integer): Bool; StdCall;
  Function QueryServiceStatus (hService: TSC_HANDLE; Var ServiceStatus: TSERVICE_STATUS): Bool; StdCall;
  Function RegisterServiceCtrlHandlerA (ServiceName: PAnsiChar; HandlerProc: THANDLER_FUNCTION): TSERVICE_STATUS_HANDLE; StdCall;
  Function RegisterServiceCtrlHandlerW (ServiceName: PWideChar; HandlerProc: THANDLER_FUNCTION): TSERVICE_STATUS_HANDLE; StdCall;
  Function RegisterServiceCtrlHandler (ServiceName: PChar; HandlerProc: THANDLER_FUNCTION): TSERVICE_STATUS_HANDLE; StdCall;
  Function SetServiceObjectSecurity (hService: TSC_HANDLE; SecurityInformation: TSECURITYINFORMATION;
                                 Var SecurityDescriptor: TSECURITYDESCRIPTOR): Bool; StdCall;
  Function SetServiceStatus (hServiceStatus: TSERVICE_STATUS_HANDLE; Var ServiceStatus: TSERVICE_STATUS): Bool; StdCall;
  Function StartServiceCtrlDispatcherA (Var ServiceStartTable{: TSERVICE_TABLE_ENTRYA}): Bool; StdCall;
  Function StartServiceCtrlDispatcherW (Var ServiceStartTable{: TSERVICE_TABLE_ENTRYW}): Bool; StdCall;
  Function StartServiceCtrlDispatcher  (Var ServiceStartTable{: TSERVICE_TABLE_ENTRY}): Bool; StdCall;
  Function StartServiceA (hService: TSC_HANDLE; NumServiceArgs: Integer; Var ServiceArgVectors: PAnsiChar): Bool; StdCall;
  Function StartServiceW (hService: TSC_HANDLE; NumServiceArgs: Integer; Var ServiceArgVectors: PWideChar): Bool; StdCall;
  Function StartService  (hService: TSC_HANDLE; NumServiceArgs: Integer; Var ServiceArgVectors: PChar): Bool; StdCall;
  Function UnlockServiceDatabase (ScLock: TSC_LOCK): Bool; StdCall;

//------------------------------------------------------------------------------
// Constants Types and Procedures for NT Security
const
  NO_INHERITANCE = 0;
  SUB_CONTAINERS_ONLY_INHERIT  = 2;
  SUB_OBJECTS_ONLY_INHERIT  = 1;
  SUB_CONTAINERS_AND_OBJECTS_INHERIT = 3;


type
 SE_OBJECT_TYPE = (
    SE_UNKNOWN_OBJECT_TYPE,
    SE_FILE_OBJECT,
    SE_SERVICE,
    SE_PRINTER,
    SE_REGISTRY_KEY,
    SE_LMSHARE,
    SE_KERNEL_OBJECT,
    SE_WINDOW_OBJECT );

 ACCESS_MODE = (
    NOT_USED_ACCESS,
    GRANT_ACCESS,
    SET_ACCESS,
    DENY_ACCESS,
    REVOKE_ACCESS,
    SET_AUDIT_SUCCESS,
    SET_AUDIT_FAILURE );

 TRUSTEE_TYPE = (
    TRUSTEE_IS_UNKNOWN,
    TRUSTEE_IS_USER,
    TRUSTEE_IS_GROUP );

 TRUSTEE_FORM = (
    TRUSTEE_IS_SID,
    TRUSTEE_IS_NAME );

 MULTIPLE_TRUSTEE_OPERATION = (
    NO_MULTIPLE_TRUSTEE,
    TRUSTEE_IS_IMPERSONATE );

 PTRUSTEE = ^TRUSTEE;
 TRUSTEE = record
    pMultipleTrustee            : PTRUSTEE;
    MultipleTrusteeOperation    : MULTIPLE_TRUSTEE_OPERATION;
    TrusteeForm                 : TRUSTEE_FORM;
    TrusteeType                 : TRUSTEE_TYPE;
    ptstrName                   : PChar;
 end;

 EXPLICIT_ACCESS = record
    grfAccessPermissions   : DWORD;
    grfAccessMode          : ACCESS_MODE;
    grfInheritance         : DWORD;
    Trustee                : TRUSTEE;
 end;

 Function GetSecurityInfo (aObject: THANDLE; aObjectTyp : SE_OBJECT_TYPE;
                           aSecurityInformation: TSECURITYINFORMATION;
                           var aOwnerSID : PSID; var aGroupSID : PSID;
                           var aDacl     : PACL;var aSacl     : PACL;
                           Var SecurityDescriptor: TSECURITYDESCRIPTOR ): DWORD; StdCall;

 Function SetSecurityInfo ( aObject : THANDLE; aObjectTyp : SE_OBJECT_TYPE;
                            aSecurityInformation: TSECURITYINFORMATION; aOwnerSID : PSID;
                            aGroupSID : PSID; aDacl     : PACL; aSacl     : PACL ): DWORD; StdCall;


 Procedure BuildExplicitAccessWithName ( var aExplicitAccess : EXPLICIT_ACCESS;
                                        aTrusteeName : PChar; aAccessMask : DWORD;
                                        aAccessMode : ACCESS_MODE; aInheritance : DWORD);  StdCall;

 Function SetEntriesInAcl ( aCountOfEntries : ULONG;
                             var aExplicitAccess : EXPLICIT_ACCESS;
                             aOldAcl  : PACL; var aNewAcl  : PACL ): DWORD; StdCall;

 Function SetKernelObjectSecurity ( aObject : THANDLE; aUser : PChar; aSet : boolean ) : boolean;

//------------------------------------------------------------------------------
// Types and Procedures for Process Informatin
type
  PHInst = ^HInst;
  TModuleInfo = record
    lpBaseOfDll : pointer;
    SizeOfImage : Integer;
    EntryPoint : pointer
  end;

  TPSAPIWsWatchInformation = record
    FaultingPc : pointer;
    FaultingVa : pointer
  end;

  TProcessMemoryCounters = record
    cb : Integer;
    PageFaultCount : Integer;
    PeakWorkingSetSize : Integer;
    WorkingSetSize : Integer;
    QuotaPeakPagedPoolUsage : Integer;
    QuotaPagedPoolUsage : Integer;
    QuotaPeakNonPagedPoolUsage : Integer;
    QuotaNonPagedPoolUsage : Integer;
    PagefileUsage : Integer;
    PeakPagefileUsage : Integer
  end;

  function EnumProcesses (pidList : PInteger; cb : Integer; var cbNeeded : Integer): boolean; stdcall;
  function EnumProcessModules (hProcess : THandle; moduleList : PHInst; cb : Integer; var cbNeeded : Integer) : boolean; stdcall;
  function GetModuleBaseName (hProcess : THandle; module : HInst; BaseName : Pchar; size : Integer) : Integer; stdcall;
  function GetModuleFileNameEx (hProcess : THandle; module : HInst; FileName : PChar; size : Integer) : Integer; stdcall;
  function GetModuleInformation(hProcess : THandle; module : HInst; var info : TModuleInfo; size : Integer) : boolean; stdcall;
  function EmptyWorkingSet (hProcess : THandle) : boolean; stdcall;
  function QueryWorkingSet (hProcess : THandle; var pv; size : Integer) : boolean; stdcall;
  function InitializeProcessForWsWatch (hProcess : THandle) : boolean; stdcall;
  function GetWsChanges (hProcess : THandle; var WatchInfo : TPSAPIWsWatchInformation; size : Integer) : boolean; stdcall;
  function GetMappedFileName (hProcess : THandle; pv : pointer; FileName : PChar; size : Integer) : Integer; stdcall;
  function EnumDeviceDrivers (var ImageBase : Integer; cb : Integer; var cbNeeded : Integer) : boolean; stdcall;
  function GetDeviceDriverBaseName (ImageBase : Integer; BaseName : PChar; size : Integer) : Integer; stdcall;
  function GetDeviceDriverFileName (ImageBase : Integer; FileName : PChar; size : Integer) : Integer; stdcall;
  function GetProcessMemoryInfo (hProcess : THandle; var ProcessMemoryCounters : TProcessMemoryCounters; size : Integer) : boolean; stdcall;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

const psapidll = 'psapi.dll';
      ADVAPI32 = 'ADVAPI32.DLL';

  function EnumProcesses;               external psapidll;
  function EnumProcessModules;          external psapidll;
  function GetModuleBaseName;           external psapidll name 'GetModuleBaseNameA';
  function GetModuleFileNameEx;         external psapidll name 'GetModuleFileNameExA';
  function GetModuleInformation;        external psapidll;
  function EmptyWorkingSet;             external psapidll;
  function QueryWorkingSet;             external psapidll;
  function InitializeProcessForWsWatch; external psapidll;
  function GetWsChanges;                external psapidll;
  function GetMappedFileName;           external psapidll name 'GetMappedFileNameA';
  function EnumDeviceDrivers;           external psapidll;
  function GetDeviceDriverBaseName;     external psapidll name 'GetDeviceDriverBaseNameA';
  function GetDeviceDriverFileName;     external psapidll name 'GetDeviceDriverFileNameA';
  function GetProcessMemoryInfo;        external psapidll;
  Function ChangeServiceConfigA;        External ADVAPI32 Name 'ChangeServiceConfigA';
  Function ChangeServiceConfigW;        External ADVAPI32 Name 'ChangeServiceConfigW';
  Function ChangeServiceConfig;         External ADVAPI32 Name 'ChangeServiceConfigA';
  Function CloseServiceHandle;          External ADVAPI32 Name 'CloseServiceHandle';
  Function ControlService;              External ADVAPI32 Name 'ControlService';
  Function CreateServiceA;              External ADVAPI32 Name 'CreateServiceA';
  Function CreateServiceW;              External ADVAPI32 Name 'CreateServiceW';
  Function CreateService;               External ADVAPI32 Name 'CreateServiceA';
  Function DeleteService;               External ADVAPI32 Name 'DeleteService';
  Function EnumDependentServicesA;      External ADVAPI32 Name 'EnumDependentServicesA';
  Function EnumDependentServicesW;      External ADVAPI32 Name 'EnumDependentServicesW';
  Function EnumDependentServices;       External ADVAPI32 Name 'EnumDependentServicesA';
  Function EnumServicesStatusA;         External ADVAPI32 Name 'EnumServicesStatusA';
  Function EnumServicesStatusW;         External ADVAPI32 Name 'EnumServicesStatusW';
  Function EnumServicesStatus;          External ADVAPI32 Name 'EnumServicesStatusA';
  Function GetServiceDisplayNameA;      External ADVAPI32 Name 'GetServiceDisplayNameA';
  Function GetServiceDisplayNameW;      External ADVAPI32 Name 'GetServiceDisplayNameW';
  Function GetServiceDisplayName;       External ADVAPI32 Name 'GetServiceDisplayNameA';
  Function GetServiceKeyNameA;          External ADVAPI32 Name 'GetServiceKeyNameA';
  Function GetServiceKeyNameW;          External ADVAPI32 Name 'GetServiceKeyNameW';
  Function GetServiceKeyName;           External ADVAPI32 Name 'GetServiceKeyNameA';
  Function LockServiceDatabase;         External ADVAPI32 Name 'LockServiceDatabase';
  Function NotifyBootConfigStatus;      External ADVAPI32 Name 'NotifyBootConfigStatus';
  Function OpenSCManagerA;              External ADVAPI32 Name 'OpenSCManagerA';
  Function OpenSCManagerW;              External ADVAPI32 Name 'OpenSCManagerW';
  Function OpenSCManager;               External ADVAPI32 Name 'OpenSCManagerA';
  Function OpenServiceA;                External ADVAPI32 Name 'OpenServiceA';
  Function OpenServiceW;                External ADVAPI32 Name 'OpenServiceW';
  Function OpenService;                 External ADVAPI32 Name 'OpenServiceA';
  Function QueryServiceConfigA;         External ADVAPI32 Name 'QueryServiceConfigA';
  Function QueryServiceConfigW;         External ADVAPI32 Name 'QueryServiceConfigW';
  Function QueryServiceConfig;          External ADVAPI32 Name 'QueryServiceConfigA';
  Function QueryServiceLockStatusA;     External ADVAPI32 Name 'QueryServiceLockStatusA';
  Function QueryServiceLockStatusW;     External ADVAPI32 Name 'QueryServiceLockStatusW';
  Function QueryServiceLockStatus;      External ADVAPI32 Name 'QueryServiceLockStatusA';
  Function QueryServiceObjectSecurity;  External ADVAPI32 Name 'QueryServiceObjectSecurity';
  Function QueryServiceStatus;          External ADVAPI32 Name 'QueryServiceStatus';
  Function RegisterServiceCtrlHandlerA; External ADVAPI32 Name 'RegisterServiceCtrlHandlerA';
  Function RegisterServiceCtrlHandlerW; External ADVAPI32 Name 'RegisterServiceCtrlHandlerW';
  Function RegisterServiceCtrlHandler;  External ADVAPI32 Name 'RegisterServiceCtrlHandlerA';
  Function SetServiceObjectSecurity;    External ADVAPI32 Name 'SetServiceObjectSecurity';
  Function SetServiceStatus;            External ADVAPI32 Name 'SetServiceStatus';
  Function StartServiceCtrlDispatcherA; External ADVAPI32 Name 'StartServiceCtrlDispatcherA';
  Function StartServiceCtrlDispatcherW; External ADVAPI32 Name 'StartServiceCtrlDispatcherW';
  Function StartServiceCtrlDispatcher;  External ADVAPI32 Name 'StartServiceCtrlDispatcherA';
  Function StartServiceA;               External ADVAPI32 Name 'StartServiceA';
  Function StartServiceW;               External ADVAPI32 Name 'StartServiceW';
  Function StartService;                External ADVAPI32 Name 'StartServiceA';
  Function UnlockServiceDatabase;       External ADVAPI32 Name 'UnlockServiceDatabase'
  Function  GetSecurityInfo;            External ADVAPI32 Name 'GetSecurityInfo';
  Function  SetSecurityInfo;            External ADVAPI32 Name 'SetSecurityInfo';
  Procedure BuildExplicitAccessWithName;External ADVAPI32 Name 'BuildExplicitAccessWithNameA';
  Function  SetEntriesInAcl;            External ADVAPI32 Name 'SetEntriesInAclA';

  function SetKernelObjectSecurity ( aObject : THANDLE; aUser : PChar; aSet : boolean ) : boolean;
  var  xRes    : integer;
       xExistingDacl : PACL;
       xNewDacl      : PACL;
       xSecDes       : TSECURITYDESCRIPTOR;
       xSid          : PSID;
       xSacl         : PAcl;
       xExAccess     : EXPLICIT_ACCESS;
       xAccess       : ACCESS_MODE;
  begin
    Result := true;
    xNewDacl := Nil;
    xRes := GetSecurityInfo ( aObject, SE_KERNEL_OBJECT, DACL_SECURITY_INFORMATION,
                             xSid, xSid, xExistingDacl, xSacl, xSecDes );
    if xRes <> 0 then
      Result := false;

    if Result then begin
      if aSet then
        xAccess := SET_ACCESS
      else
        xAccess := DENY_ACCESS;
      BuildExplicitAccessWithName ( xExAccess, aUser, GENERIC_ALL, xAccess,
                                    SUB_CONTAINERS_AND_OBJECTS_INHERIT );
      xRes := SetEntriesInAcl ( 1, xExAccess, xExistingDacl, xNewDacl );
    end;

    if xRes <> 0 then
      Result := false;

    if Result then
      xRes := SetSecurityInfo ( aObject, SE_KERNEL_OBJECT, DACL_SECURITY_INFORMATION,
                                Nil, Nil, xNewDAcl, Nil );

    if xRes <> 0 then
      Result := false;

    if xNewDacl <> Nil then
      LocalFree ( cardinal ( xNewDacl )  );
  end;
end.
