unit DEXSTD_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88  $
// File generated on 26.11.99 15:41:31 from Type Library described below.

// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
// ************************************************************************ //
// Type Lib: C:\Program Files\BARCO\FloorNew\dexstd.tlb (1)
// IID\LCID: {BED31DF0-5069-11D1-9173-0060080C772F}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\STDOLE2.TLB)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// Errors:
//   Hint: Member 'FirstMachine' of 'IBarcoMachineSelectionList' changed to 'FirstMachine_'
//   Hint: Member 'NextMachine' of 'IBarcoMachineSelectionList' changed to 'NextMachine_'
//   Hint: Member 'Filter' of 'IBarcoReportCommon' changed to 'Filter_'
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  DEXSTDMajorVersion = 1;
  DEXSTDMinorVersion = 0;

  LIBID_DEXSTD: TGUID = '{BED31DF0-5069-11D1-9173-0060080C772F}';

  IID_IDataNotify: TGUID = '{152F16C1-6549-11D0-99F6-0000C088D00B}';
  IID_IBarcoDataNotify: TGUID = '{E1017130-DAC5-11D2-8F7D-0060080C772F}';
  IID_IBarcoDataNotifyEx: TGUID = '{083ED670-817A-11D3-9031-0060080C772F}';
  IID_IBarcoKeyList: TGUID = '{18F14432-02EB-11D3-8F9F-0060080C772F}';
  IID_IMachineSelectionList: TGUID = '{6A22F030-E4B3-11D0-9AAD-0000C088D00B}';
  IID_IBarcoMachineSelectionList: TGUID = '{19AB9170-7580-11D3-901E-0060080C772F}';
  IID_IBarcoSectionList: TGUID = '{6EEBF830-7A67-11D3-9027-0060080C772F}';
  IID_IBarcoDataDictionary: TGUID = '{604412A0-D615-11D2-8F77-0060080C772F}';
  IID_IExtraSel: TGUID = '{112029D6-4EBC-11D1-9171-0060080C772F}';
  IID_IExtraSelEx: TGUID = '{EF19B620-D623-11D2-8F77-0060080C772F}';
  IID_IBarcoDBRequest: TGUID = '{14846230-D85B-11D2-8F7A-0060080C772F}';
  IID_IBarcoDBNotify: TGUID = '{77E68460-048B-11D3-8FA1-0060080C772F}';
  IID_IBarcoFilter: TGUID = '{130FA090-DAC4-11D2-8F7D-0060080C772F}';
  IID_IFilterTest: TGUID = '{24E4C030-E623-11D0-9AAE-0000C088D00B}';
  IID_IBarcoFilterTest: TGUID = '{7091AD00-9B65-11D3-B8F3-006008515DBF}';
  IID_IReportCommon: TGUID = '{002FC170-E7F4-11D0-9AB0-0000C088D00B}';
  IID_IReportTable: TGUID = '{46483440-EEFB-11D0-9ABC-0060977A5412}';
  IID_IColumnInfo: TGUID = '{396A9680-EF02-11D0-9ABC-0060977A5412}';
  IID_IReportNotify: TGUID = '{8CDA2AE0-EF03-11D0-9ABC-0060977A5412}';
  IID_IRpiList: TGUID = '{EA9DD953-C54C-11D1-8E42-0060080C772F}';
  IID_IReportItemCfg: TGUID = '{EA9DD952-C54C-11D1-8E42-0060080C772F}';
  IID_IFormat: TGUID = '{9CF18040-4EA8-11D1-9171-0060080C772F}';
  IID_IReportTableNotifyChange: TGUID = '{61602D1A-995D-11D1-91E8-0060080C772F}';
  IID_IFilters: TGUID = '{2AAD5360-E622-11D0-9AAE-0000C088D00B}';
  IID_IBarcoReportCommon: TGUID = '{0745E6A0-4416-11D3-B923-006008515DBF}';
  IID_IReportBanner: TGUID = '{11EB82B0-7E0B-11D2-AA6C-00104B4B2529}';
  IID_IFilterArray: TGUID = '{BF68C9E2-494D-11D2-8ED5-0060080C772F}';
  IID_IFilterCategory: TGUID = '{BF68C9E3-494D-11D2-8ED5-0060080C772F}';
  IID_IFrameCon: TGUID = '{85BF155A-600E-11D1-9186-0060080C772F}';
  IID_IFloorFrame: TGUID = '{C34F654E-6655-11D1-9196-0060080C772F}';
  IID_IBarcoFrameCon: TGUID = '{A92D4D00-913E-11D3-B8E5-006008515DBF}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum __MIDL___MIDL_itf_dexstd_0217_0001
type
  __MIDL___MIDL_itf_dexstd_0217_0001 = TOleEnum;
const
  BDD_TEXT_LONG = $00000000;
  BDD_TEXT_SHORT = $00000001;

// Constants for enum __MIDL_IBarcoFilterTest_0001
type
  __MIDL_IBarcoFilterTest_0001 = TOleEnum;
const
  filterTest_OtherType = $00000000;
  filterTest_DateType = $00000001;
  filterTest_TimeType = $00000002;
  filterTest_DateTimeType = $00000003;

// Constants for enum __MIDL_IReportNotify_0001
type
  __MIDL_IReportNotify_0001 = TOleEnum;
const
  dataNotify = $00000000;
  dataRefresh = $00000001;
  RequestData = $00000002;

// Constants for enum __MIDL_IReportTable_0001
type
  __MIDL_IReportTable_0001 = TOleEnum;
const
  commaDelimited = $00000000;

// Constants for enum __MIDL_IFormat_0001
type
  __MIDL_IFormat_0001 = TOleEnum;
const
  LEFT = $00000000;
  RIGHT = $00000001;

// Constants for enum __MIDL_IReportTable_0002
type
  __MIDL_IReportTable_0002 = TOleEnum;
const
  rpd_TotalLine = $00000000;
  rpd_DataLine = $000000FF;
  rpd_DataSummaryLine = $000000FE;
  rpd_Key1Line = $00000001;

// Constants for enum __MIDL_IReportBanner_0001
type
  __MIDL_IReportBanner_0001 = TOleEnum;
const
  ReportHeader = $00000000;
  ReportFooter = $00000001;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IDataNotify = interface;
  IDataNotifyDisp = dispinterface;
  IBarcoDataNotify = interface;
  IBarcoDataNotifyDisp = dispinterface;
  IBarcoDataNotifyEx = interface;
  IBarcoDataNotifyExDisp = dispinterface;
  IBarcoKeyList = interface;
  IBarcoKeyListDisp = dispinterface;
  IMachineSelectionList = interface;
  IMachineSelectionListDisp = dispinterface;
  IBarcoMachineSelectionList = interface;
  IBarcoSectionList = interface;
  IBarcoSectionListDisp = dispinterface;
  IBarcoDataDictionary = interface;
  IBarcoDataDictionaryDisp = dispinterface;
  IExtraSel = interface;
  IExtraSelDisp = dispinterface;
  IExtraSelEx = interface;
  IExtraSelExDisp = dispinterface;
  IBarcoDBRequest = interface;
  IBarcoDBRequestDisp = dispinterface;
  IBarcoDBNotify = interface;
  IBarcoDBNotifyDisp = dispinterface;
  IBarcoFilter = interface;
  IBarcoFilterDisp = dispinterface;
  IFilterTest = interface;
  IFilterTestDisp = dispinterface;
  IBarcoFilterTest = interface;
  IBarcoFilterTestDisp = dispinterface;
  IReportCommon = interface;
  IReportCommonDisp = dispinterface;
  IReportTable = interface;
  IReportTableDisp = dispinterface;
  IColumnInfo = interface;
  IColumnInfoDisp = dispinterface;
  IReportNotify = interface;
  IReportNotifyDisp = dispinterface;
  IRpiList = interface;
  IRpiListDisp = dispinterface;
  IReportItemCfg = interface;
  IReportItemCfgDisp = dispinterface;
  IFormat = interface;
  IFormatDisp = dispinterface;
  IReportTableNotifyChange = interface;
  IReportTableNotifyChangeDisp = dispinterface;
  IFilters = interface;
  IFiltersDisp = dispinterface;
  IBarcoReportCommon = interface;
  IBarcoReportCommonDisp = dispinterface;
  IReportBanner = interface;
  IReportBannerDisp = dispinterface;
  IFilterArray = interface;
  IFilterArrayDisp = dispinterface;
  IFilterCategory = interface;
  IFilterCategoryDisp = dispinterface;
  IFrameCon = interface;
  IFrameConDisp = dispinterface;
  IFloorFrame = interface;
  IFloorFrameDisp = dispinterface;
  IBarcoFrameCon = interface;
  IBarcoFrameConDisp = dispinterface;

// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  wireHDC = ^TGUID; 
  PUserType1 = ^TGUID; {*}
  PSYSINT1 = ^SYSINT; {*}

  BARCO_TEXTTYPE = __MIDL___MIDL_itf_dexstd_0217_0001; 
  FILTERTEST_ENUMTIME = __MIDL_IBarcoFilterTest_0001; 
  SETUPATTR_REASON = __MIDL_IReportNotify_0001; 

  __MIDL_IWinTypes_0009 = record
    case Integer of
      0: (hInproc: Integer);
      1: (hRemote: Integer);
  end;

  _RemotableHandle = packed record
    fContext: Integer;
    u: __MIDL_IWinTypes_0009;
  end;

  ENUM_EXPORTTYPE = __MIDL_IReportTable_0001; 
  JUSTIFY = __MIDL_IFormat_0001; 
  LINETYPE = __MIDL_IReportTable_0002; 
  ENUM_REPORTBANNER = __MIDL_IReportBanner_0001; 

// *********************************************************************//
// Interface: IDataNotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {152F16C1-6549-11D0-99F6-0000C088D00B}
// *********************************************************************//
  IDataNotify = interface(IDispatch)
    ['{152F16C1-6549-11D0-99F6-0000C088D00B}']
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; pvItems: PSafeArray); safecall;
  end;

// *********************************************************************//
// DispIntf:  IDataNotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {152F16C1-6549-11D0-99F6-0000C088D00B}
// *********************************************************************//
  IDataNotifyDisp = dispinterface
    ['{152F16C1-6549-11D0-99F6-0000C088D00B}']
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; pvItems: {??PSafeArray} OleVariant); dispid 1;
  end;

// *********************************************************************//
// Interface: IBarcoDataNotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E1017130-DAC5-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoDataNotify = interface(IDispatch)
    ['{E1017130-DAC5-11D2-8F7D-0060080C772F}']
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; var pvItems: PSafeArray); safecall;
    procedure NotifyEvent(nID: SYSINT; v: OleVariant); safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoDataNotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E1017130-DAC5-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoDataNotifyDisp = dispinterface
    ['{E1017130-DAC5-11D2-8F7D-0060080C772F}']
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; 
                           var pvItems: {??PSafeArray} OleVariant); dispid 1;
    procedure NotifyEvent(nID: SYSINT; v: OleVariant); dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoDataNotifyEx
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {083ED670-817A-11D3-9031-0060080C772F}
// *********************************************************************//
  IBarcoDataNotifyEx = interface(IBarcoDataNotify)
    ['{083ED670-817A-11D3-9031-0060080C772F}']
    procedure DbInfo(pParam: Integer; var riid: TGUID; const pUnk: IUnknown); safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoDataNotifyExDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {083ED670-817A-11D3-9031-0060080C772F}
// *********************************************************************//
  IBarcoDataNotifyExDisp = dispinterface
    ['{083ED670-817A-11D3-9031-0060080C772F}']
    procedure DbInfo(pParam: Integer; var riid: {??TGUID} OleVariant; const pUnk: IUnknown); dispid 3;
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; 
                           var pvItems: {??PSafeArray} OleVariant); dispid 1;
    procedure NotifyEvent(nID: SYSINT; v: OleVariant); dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoKeyList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {18F14432-02EB-11D3-8F9F-0060080C772F}
// *********************************************************************//
  IBarcoKeyList = interface(IDispatch)
    ['{18F14432-02EB-11D3-8F9F-0060080C772F}']
    function  Get_FirstKey(var ppos: SYSINT; var pnRowNo: SYSINT): OleVariant; safecall;
    procedure Set_FirstKey(var ppos: SYSINT; var pnRowNo: SYSINT; pVal: OleVariant); safecall;
    function  Get_NextKey(var ppos: SYSINT; var pnRowNo: SYSINT): OleVariant; safecall;
    procedure Set_NextKey(var ppos: SYSINT; var pnRowNo: SYSINT; pVal: OleVariant); safecall;
    procedure Set_MachineSelection(const pVal: IBarcoMachineSelectionList); safecall;
    function  Get_MachineSelection: IBarcoMachineSelectionList; safecall;
    procedure Set_KeyID(pVal: SYSINT); safecall;
    function  Get_KeyID: SYSINT; safecall;
    function  Get_NumKeys: SYSINT; safecall;
    property FirstKey[var ppos: SYSINT; var pnRowNo: SYSINT]: OleVariant read Get_FirstKey write Set_FirstKey;
    property NextKey[var ppos: SYSINT; var pnRowNo: SYSINT]: OleVariant read Get_NextKey write Set_NextKey;
    property MachineSelection: IBarcoMachineSelectionList read Get_MachineSelection write Set_MachineSelection;
    property KeyID: SYSINT read Get_KeyID write Set_KeyID;
    property NumKeys: SYSINT read Get_NumKeys;
  end;

// *********************************************************************//
// DispIntf:  IBarcoKeyListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {18F14432-02EB-11D3-8F9F-0060080C772F}
// *********************************************************************//
  IBarcoKeyListDisp = dispinterface
    ['{18F14432-02EB-11D3-8F9F-0060080C772F}']
    property FirstKey[var ppos: SYSINT; var pnRowNo: SYSINT]: OleVariant dispid 1;
    property NextKey[var ppos: SYSINT; var pnRowNo: SYSINT]: OleVariant dispid 2;
    property MachineSelection: IBarcoMachineSelectionList dispid 3;
    property KeyID: SYSINT dispid 4;
    property NumKeys: SYSINT readonly dispid 5;
  end;

// *********************************************************************//
// Interface: IMachineSelectionList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6A22F030-E4B3-11D0-9AAD-0000C088D00B}
// *********************************************************************//
  IMachineSelectionList = interface(IDispatch)
    ['{6A22F030-E4B3-11D0-9AAD-0000C088D00B}']
    function  Get_FirstMachine: Smallint; safecall;
    procedure Set_FirstMachine(pVal: Smallint); safecall;
    function  Get_NextMachine(pos: SYSINT): Smallint; safecall;
    procedure Set_NextMachine(pos: SYSINT; pVal: Smallint); safecall;
    property FirstMachine: Smallint read Get_FirstMachine write Set_FirstMachine;
    property NextMachine[pos: SYSINT]: Smallint read Get_NextMachine write Set_NextMachine;
  end;

// *********************************************************************//
// DispIntf:  IMachineSelectionListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6A22F030-E4B3-11D0-9AAD-0000C088D00B}
// *********************************************************************//
  IMachineSelectionListDisp = dispinterface
    ['{6A22F030-E4B3-11D0-9AAD-0000C088D00B}']
    property FirstMachine: Smallint dispid 1;
    property NextMachine[pos: SYSINT]: Smallint dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoMachineSelectionList
// Flags:     (4352) OleAutomation Dispatchable
// GUID:      {19AB9170-7580-11D3-901E-0060080C772F}
// *********************************************************************//
  IBarcoMachineSelectionList = interface(IMachineSelectionList)
    ['{19AB9170-7580-11D3-901E-0060080C772F}']
    function  Get_FirstMachine_(var pos: SYSINT): Smallint; safecall;
    procedure Set_FirstMachine_(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_NextMachine_(var pos: SYSINT): Smallint; safecall;
    procedure Set_NextMachine_(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_Section: IBarcoSectionList; safecall;
    procedure Set_Section(const pVal: IBarcoSectionList); safecall;
    property FirstMachine_[var pos: SYSINT]: Smallint read Get_FirstMachine_ write Set_FirstMachine_;
    property NextMachine_[var pos: SYSINT]: Smallint read Get_NextMachine_ write Set_NextMachine_;
    property Section: IBarcoSectionList read Get_Section write Set_Section;
  end;

// *********************************************************************//
// Interface: IBarcoSectionList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6EEBF830-7A67-11D3-9027-0060080C772F}
// *********************************************************************//
  IBarcoSectionList = interface(IDispatch)
    ['{6EEBF830-7A67-11D3-9027-0060080C772F}']
    function  Get_FirstSection(var pos: SYSINT): Smallint; safecall;
    procedure Set_FirstSection(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_NextSection(var pos: SYSINT): Smallint; safecall;
    procedure Set_NextSection(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_NumSections: SYSINT; safecall;
    property FirstSection[var pos: SYSINT]: Smallint read Get_FirstSection write Set_FirstSection;
    property NextSection[var pos: SYSINT]: Smallint read Get_NextSection write Set_NextSection;
    property NumSections: SYSINT read Get_NumSections;
  end;

// *********************************************************************//
// DispIntf:  IBarcoSectionListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6EEBF830-7A67-11D3-9027-0060080C772F}
// *********************************************************************//
  IBarcoSectionListDisp = dispinterface
    ['{6EEBF830-7A67-11D3-9027-0060080C772F}']
    property FirstSection[var pos: SYSINT]: Smallint dispid 1;
    property NextSection[var pos: SYSINT]: Smallint dispid 2;
    property NumSections: SYSINT readonly dispid 3;
  end;

// *********************************************************************//
// Interface: IBarcoDataDictionary
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {604412A0-D615-11D2-8F77-0060080C772F}
// *********************************************************************//
  IBarcoDataDictionary = interface(IDispatch)
    ['{604412A0-D615-11D2-8F77-0060080C772F}']
    function  GetFirstDataItemText(var pbstr: WideString; lCategory: Integer; lFormat: Integer; 
                                   eType: BARCO_TEXTTYPE): Integer; safecall;
    function  GetNextDataItemText(lDID: Integer; var pbstr: WideString; lCategory: Integer; 
                                  lFormat: Integer; eType: BARCO_TEXTTYPE): Integer; safecall;
    function  IsIndexedDataItem(lDID: Integer; var pExSel: IExtraSelEx): Integer; safecall;
    function  GetDataItemSize(lDID: Integer): Smallint; safecall;
    function  GetDbBinding(lDID: Integer; sSubDIDNo: Smallint): Byte; safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoDataDictionaryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {604412A0-D615-11D2-8F77-0060080C772F}
// *********************************************************************//
  IBarcoDataDictionaryDisp = dispinterface
    ['{604412A0-D615-11D2-8F77-0060080C772F}']
    function  GetFirstDataItemText(var pbstr: WideString; lCategory: Integer; lFormat: Integer; 
                                   eType: BARCO_TEXTTYPE): Integer; dispid 1;
    function  GetNextDataItemText(lDID: Integer; var pbstr: WideString; lCategory: Integer; 
                                  lFormat: Integer; eType: BARCO_TEXTTYPE): Integer; dispid 2;
    function  IsIndexedDataItem(lDID: Integer; var pExSel: IExtraSelEx): Integer; dispid 3;
    function  GetDataItemSize(lDID: Integer): Smallint; dispid 4;
    function  GetDbBinding(lDID: Integer; sSubDIDNo: Smallint): Byte; dispid 5;
  end;

// *********************************************************************//
// Interface: IExtraSel
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {112029D6-4EBC-11D1-9171-0060080C772F}
// *********************************************************************//
  IExtraSel = interface(IDispatch)
    ['{112029D6-4EBC-11D1-9171-0060080C772F}']
    function  Get_MaxSelection: Smallint; safecall;
    function  Get_Title: WideString; safecall;
    function  Get_Description(iPos: SYSINT): WideString; safecall;
    function  Get_Abbreviation(iPos: SYSINT): WideString; safecall;
    property MaxSelection: Smallint read Get_MaxSelection;
    property Title: WideString read Get_Title;
    property Description[iPos: SYSINT]: WideString read Get_Description;
    property Abbreviation[iPos: SYSINT]: WideString read Get_Abbreviation;
  end;

// *********************************************************************//
// DispIntf:  IExtraSelDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {112029D6-4EBC-11D1-9171-0060080C772F}
// *********************************************************************//
  IExtraSelDisp = dispinterface
    ['{112029D6-4EBC-11D1-9171-0060080C772F}']
    property MaxSelection: Smallint readonly dispid 1;
    property Title: WideString readonly dispid 2;
    property Description[iPos: SYSINT]: WideString readonly dispid 3;
    property Abbreviation[iPos: SYSINT]: WideString readonly dispid 4;
  end;

// *********************************************************************//
// Interface: IExtraSelEx
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EF19B620-D623-11D2-8F77-0060080C772F}
// *********************************************************************//
  IExtraSelEx = interface(IExtraSel)
    ['{EF19B620-D623-11D2-8F77-0060080C772F}']
    function  Get_DbBinding(iPos: SYSINT): Smallint; safecall;
    property DbBinding[iPos: SYSINT]: Smallint read Get_DbBinding;
  end;

// *********************************************************************//
// DispIntf:  IExtraSelExDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EF19B620-D623-11D2-8F77-0060080C772F}
// *********************************************************************//
  IExtraSelExDisp = dispinterface
    ['{EF19B620-D623-11D2-8F77-0060080C772F}']
    property DbBinding[iPos: SYSINT]: Smallint readonly dispid 5;
    property MaxSelection: Smallint readonly dispid 1;
    property Title: WideString readonly dispid 2;
    property Description[iPos: SYSINT]: WideString readonly dispid 3;
    property Abbreviation[iPos: SYSINT]: WideString readonly dispid 4;
  end;

// *********************************************************************//
// Interface: IBarcoDBRequest
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {14846230-D85B-11D2-8F7A-0060080C772F}
// *********************************************************************//
  IBarcoDBRequest = interface(IDispatch)
    ['{14846230-D85B-11D2-8F7A-0060080C772F}']
    procedure RequestData(pParam: Integer; pDidList: PSafeArray; lRecordNo: Integer; 
                          var pdidData: PSafeArray); safecall;
    procedure RequestDataByKeyList(const pKeyList: IBarcoKeyList; pDidList: PSafeArray; 
                                   pParam: Integer; const pdn: IBarcoDBNotify; 
                                   const pdbr: IBarcoDBRequest); safecall;
    procedure RequestComplete(pParam: Integer; hr: HResult); safecall;
    procedure Set_Filter(sSubItmNo: Smallint; const Param2: IBarcoFilter); safecall;
    property Filter[sSubItmNo: Smallint]: IBarcoFilter write Set_Filter;
  end;

// *********************************************************************//
// DispIntf:  IBarcoDBRequestDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {14846230-D85B-11D2-8F7A-0060080C772F}
// *********************************************************************//
  IBarcoDBRequestDisp = dispinterface
    ['{14846230-D85B-11D2-8F7A-0060080C772F}']
    procedure RequestData(pParam: Integer; pDidList: {??PSafeArray} OleVariant; lRecordNo: Integer; 
                          var pdidData: {??PSafeArray} OleVariant); dispid 1;
    procedure RequestDataByKeyList(const pKeyList: IBarcoKeyList; 
                                   pDidList: {??PSafeArray} OleVariant; pParam: Integer; 
                                   const pdn: IBarcoDBNotify; const pdbr: IBarcoDBRequest); dispid 2;
    procedure RequestComplete(pParam: Integer; hr: HResult); dispid 3;
    property Filter[sSubItmNo: Smallint]: IBarcoFilter writeonly dispid 4;
  end;

// *********************************************************************//
// Interface: IBarcoDBNotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {77E68460-048B-11D3-8FA1-0060080C772F}
// *********************************************************************//
  IBarcoDBNotify = interface(IDispatch)
    ['{77E68460-048B-11D3-8FA1-0060080C772F}']
    procedure ProcessDBItems(pParam: Integer; const bstrKey: WideString; var pvItems: PSafeArray); safecall;
    procedure RequestComplete(pParam: Integer); safecall;
    procedure DbInfo(pParam: Integer; var riid: TGUID; const pUnk: IUnknown); safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoDBNotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {77E68460-048B-11D3-8FA1-0060080C772F}
// *********************************************************************//
  IBarcoDBNotifyDisp = dispinterface
    ['{77E68460-048B-11D3-8FA1-0060080C772F}']
    procedure ProcessDBItems(pParam: Integer; const bstrKey: WideString; 
                             var pvItems: {??PSafeArray} OleVariant); dispid 1;
    procedure RequestComplete(pParam: Integer); dispid 2;
    procedure DbInfo(pParam: Integer; var riid: {??TGUID} OleVariant; const pUnk: IUnknown); dispid 3;
  end;

// *********************************************************************//
// Interface: IBarcoFilter
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {130FA090-DAC4-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoFilter = interface(IDispatch)
    ['{130FA090-DAC4-11D2-8F7D-0060080C772F}']
    function  Get_FirstFilterTest(var pos: SYSINT): IBarcoFilterTest; safecall;
    procedure Set_FirstFilterTest(var pos: SYSINT; const pVal: IBarcoFilterTest); safecall;
    function  Get_NextFilterTest(var pos: SYSINT): IBarcoFilterTest; safecall;
    procedure Set_NextFilterTest(var pos: SYSINT; const pVal: IBarcoFilterTest); safecall;
    function  Get_Name: WideString; safecall;
    function  Get_ID: Word; safecall;
    function  Get_AnyCondition: Integer; safecall;
    function  Get_HideMachines: Integer; safecall;
    property FirstFilterTest[var pos: SYSINT]: IBarcoFilterTest read Get_FirstFilterTest write Set_FirstFilterTest;
    property NextFilterTest[var pos: SYSINT]: IBarcoFilterTest read Get_NextFilterTest write Set_NextFilterTest;
    property Name: WideString read Get_Name;
    property ID: Word read Get_ID;
    property AnyCondition: Integer read Get_AnyCondition;
    property HideMachines: Integer read Get_HideMachines;
  end;

// *********************************************************************//
// DispIntf:  IBarcoFilterDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {130FA090-DAC4-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoFilterDisp = dispinterface
    ['{130FA090-DAC4-11D2-8F7D-0060080C772F}']
    property FirstFilterTest[var pos: SYSINT]: IBarcoFilterTest dispid 1;
    property NextFilterTest[var pos: SYSINT]: IBarcoFilterTest dispid 2;
    property Name: WideString readonly dispid 3;
    property ID: {??Word} OleVariant readonly dispid 4;
    property AnyCondition: Integer readonly dispid 5;
    property HideMachines: Integer readonly dispid 6;
  end;

// *********************************************************************//
// Interface: IFilterTest
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {24E4C030-E623-11D0-9AAE-0000C088D00B}
// *********************************************************************//
  IFilterTest = interface(IDispatch)
    ['{24E4C030-E623-11D0-9AAE-0000C088D00B}']
    function  Get_ID: Integer; safecall;
    procedure Set_ID(pVal: Integer); safecall;
    function  Get_CompareTest: Smallint; safecall;
    procedure Set_CompareTest(pVal: Smallint); safecall;
    function  Get_CompareValue: WideString; safecall;
    procedure Set_CompareValue(const pVal: WideString); safecall;
    function  Get_Name: WideString; safecall;
    function  Get_CompareID: Integer; safecall;
    procedure Set_CompareID(pVal: Integer); safecall;
    function  Get_UpperThreshold: Double; safecall;
    procedure Set_UpperThreshold(pVal: Double); safecall;
    function  Get_LowerThreshold: Double; safecall;
    procedure Set_LowerThreshold(pVal: Double); safecall;
    property ID: Integer read Get_ID write Set_ID;
    property CompareTest: Smallint read Get_CompareTest write Set_CompareTest;
    property CompareValue: WideString read Get_CompareValue write Set_CompareValue;
    property Name: WideString read Get_Name;
    property CompareID: Integer read Get_CompareID write Set_CompareID;
    property UpperThreshold: Double read Get_UpperThreshold write Set_UpperThreshold;
    property LowerThreshold: Double read Get_LowerThreshold write Set_LowerThreshold;
  end;

// *********************************************************************//
// DispIntf:  IFilterTestDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {24E4C030-E623-11D0-9AAE-0000C088D00B}
// *********************************************************************//
  IFilterTestDisp = dispinterface
    ['{24E4C030-E623-11D0-9AAE-0000C088D00B}']
    property ID: Integer dispid 1;
    property CompareTest: Smallint dispid 2;
    property CompareValue: WideString dispid 3;
    property Name: WideString readonly dispid 4;
    property CompareID: Integer dispid 5;
    property UpperThreshold: Double dispid 6;
    property LowerThreshold: Double dispid 7;
  end;

// *********************************************************************//
// Interface: IBarcoFilterTest
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7091AD00-9B65-11D3-B8F3-006008515DBF}
// *********************************************************************//
  IBarcoFilterTest = interface(IFilterTest)
    ['{7091AD00-9B65-11D3-B8F3-006008515DBF}']
    function  Get_TimeType: FILTERTEST_ENUMTIME; safecall;
    procedure Set_TimeType(pVal: FILTERTEST_ENUMTIME); safecall;
    property TimeType: FILTERTEST_ENUMTIME read Get_TimeType write Set_TimeType;
  end;

// *********************************************************************//
// DispIntf:  IBarcoFilterTestDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7091AD00-9B65-11D3-B8F3-006008515DBF}
// *********************************************************************//
  IBarcoFilterTestDisp = dispinterface
    ['{7091AD00-9B65-11D3-B8F3-006008515DBF}']
    property TimeType: FILTERTEST_ENUMTIME dispid 8;
    property ID: Integer dispid 1;
    property CompareTest: Smallint dispid 2;
    property CompareValue: WideString dispid 3;
    property Name: WideString readonly dispid 4;
    property CompareID: Integer dispid 5;
    property UpperThreshold: Double dispid 6;
    property LowerThreshold: Double dispid 7;
  end;

// *********************************************************************//
// Interface: IReportCommon
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {002FC170-E7F4-11D0-9AB0-0000C088D00B}
// *********************************************************************//
  IReportCommon = interface(IDispatch)
    ['{002FC170-E7F4-11D0-9AB0-0000C088D00B}']
    function  Get_WarningAboveColor: LongWord; safecall;
    procedure Set_WarningAboveColor(pVal: LongWord); safecall;
    function  Get_AlarmAboveColor: LongWord; safecall;
    procedure Set_AlarmAboveColor(pVal: LongWord); safecall;
    function  Get_WarningBelowColor: LongWord; safecall;
    procedure Set_WarningBelowColor(pVal: LongWord); safecall;
    function  Get_AlarmBelowColor: LongWord; safecall;
    procedure Set_AlarmBelowColor(pVal: LongWord); safecall;
    function  Get_ReportTable: IReportTable; safecall;
    procedure Set_ReportTable(const pVal: IReportTable); safecall;
    function  Get_NormalColor: LongWord; safecall;
    procedure Set_NormalColor(pVal: LongWord); safecall;
    function  Get_MachineSelection: IMachineSelectionList; safecall;
    procedure Set_MachineSelection(const pVal: IMachineSelectionList); safecall;
    function  Get_Filter: IFilters; safecall;
    procedure Set_Filter(const pVal: IFilters); safecall;
    function  Get_Selection: Smallint; safecall;
    procedure Set_Selection(pVal: Smallint); safecall;
    property WarningAboveColor: LongWord read Get_WarningAboveColor write Set_WarningAboveColor;
    property AlarmAboveColor: LongWord read Get_AlarmAboveColor write Set_AlarmAboveColor;
    property WarningBelowColor: LongWord read Get_WarningBelowColor write Set_WarningBelowColor;
    property AlarmBelowColor: LongWord read Get_AlarmBelowColor write Set_AlarmBelowColor;
    property ReportTable: IReportTable read Get_ReportTable write Set_ReportTable;
    property NormalColor: LongWord read Get_NormalColor write Set_NormalColor;
    property MachineSelection: IMachineSelectionList read Get_MachineSelection write Set_MachineSelection;
    property Filter: IFilters read Get_Filter write Set_Filter;
    property Selection: Smallint read Get_Selection write Set_Selection;
  end;

// *********************************************************************//
// DispIntf:  IReportCommonDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {002FC170-E7F4-11D0-9AB0-0000C088D00B}
// *********************************************************************//
  IReportCommonDisp = dispinterface
    ['{002FC170-E7F4-11D0-9AB0-0000C088D00B}']
    property WarningAboveColor: LongWord dispid 1;
    property AlarmAboveColor: LongWord dispid 2;
    property WarningBelowColor: LongWord dispid 3;
    property AlarmBelowColor: LongWord dispid 4;
    property ReportTable: IReportTable dispid 5;
    property NormalColor: LongWord dispid 6;
    property MachineSelection: IMachineSelectionList dispid 7;
    property Filter: IFilters dispid 8;
    property Selection: Smallint dispid 9;
  end;

// *********************************************************************//
// Interface: IReportTable
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {46483440-EEFB-11D0-9ABC-0060977A5412}
// *********************************************************************//
  IReportTable = interface(IDispatch)
    ['{46483440-EEFB-11D0-9ABC-0060977A5412}']
    function  Get_ColumnCount: Smallint; safecall;
    procedure SetupPagedData(nStart: Integer; nEnd: Integer; bLoadAll: Integer); safecall;
    procedure SortOnColumn(bAsync: Integer; nColumn: Smallint); safecall;
    procedure IsDataLoaded(out bIsLoaded: Integer); safecall;
    function  Get_ColumnInfo(nColumn: Smallint): IColumnInfo; safecall;
    procedure Set_ColumnInfo(nColumn: Smallint; const pVal: IColumnInfo); safecall;
    procedure GetMinMaxValues(bIgnoreNegative: Integer; out pdbMin: Double; out pdbMax: Double; 
                              out pdbMinPareto: Double; out pdbMaxPareto: Double; 
                              out pdbParetoTotal: Double; out pdbTotals: OleVariant); safecall;
    procedure GetSelection(out pdwRowsSelected: OleVariant; out pdwColsSelected: OleVariant); safecall;
    procedure AddReportNotify(const prepNotify: IReportNotify); safecall;
    procedure DeleteReportNotify(const prepNotify: IReportNotify); safecall;
    function  Get_CellValue(nRow: Smallint; nCol: Smallint): Double; safecall;
    function  Get_CellText(nRow: Smallint; nCol: Smallint): WideString; safecall;
    function  Get_SortColumn: Smallint; safecall;
    procedure ExportData(eExportType: ENUM_EXPORTTYPE; const bstrFileName: WideString; 
                         lLineTypes: Integer); safecall;
    procedure RefreshAttachedObjects; safecall;
    function  Get_RIList: IRpiList; safecall;
    procedure Set_RIList(const pVal: IRpiList); safecall;
    procedure AddReportTableNotifyChange(const pNotify: IReportTableNotifyChange); safecall;
    procedure RemoveReportTableNotifyChange(const pNotify: IReportTableNotifyChange); safecall;
    function  Get_SummaryGroupingItem: IReportItemCfg; safecall;
    procedure Set_SummaryGroupingItem(const pVal: IReportItemCfg); safecall;
    function  Get_RowCount: Integer; safecall;
    function  Get_CellColor(iRow: SYSINT; iCol: SYSINT): LongWord; safecall;
    function  Get_KeyColumn: Integer; safecall;
    procedure IsColSelected(iCol: SYSINT); safecall;
    function  Get_LINETYPE(iRow: SYSINT): LINETYPE; safecall;
    function  Get_Cell(iRow: SYSINT; iCol: SYSINT): OleVariant; safecall;
    procedure Set_RowSelection(iRow: SYSINT; Param2: Integer); safecall;
    procedure Set_ColumnSelection(iCol: SYSINT; Param2: Integer); safecall;
    procedure IsLineSelected(iRow: SYSINT); safecall;
    procedure LockData(bLock: Integer); safecall;
    property ColumnCount: Smallint read Get_ColumnCount;
    property ColumnInfo[nColumn: Smallint]: IColumnInfo read Get_ColumnInfo write Set_ColumnInfo;
    property CellValue[nRow: Smallint; nCol: Smallint]: Double read Get_CellValue;
    property CellText[nRow: Smallint; nCol: Smallint]: WideString read Get_CellText;
    property SortColumn: Smallint read Get_SortColumn;
    property RIList: IRpiList read Get_RIList write Set_RIList;
    property SummaryGroupingItem: IReportItemCfg read Get_SummaryGroupingItem write Set_SummaryGroupingItem;
    property RowCount: Integer read Get_RowCount;
    property CellColor[iRow: SYSINT; iCol: SYSINT]: LongWord read Get_CellColor;
    property KeyColumn: Integer read Get_KeyColumn;
    property LINETYPE[iRow: SYSINT]: LINETYPE read Get_LINETYPE;
    property Cell[iRow: SYSINT; iCol: SYSINT]: OleVariant read Get_Cell;
    property RowSelection[iRow: SYSINT]: Integer write Set_RowSelection;
    property ColumnSelection[iCol: SYSINT]: Integer write Set_ColumnSelection;
  end;

// *********************************************************************//
// DispIntf:  IReportTableDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {46483440-EEFB-11D0-9ABC-0060977A5412}
// *********************************************************************//
  IReportTableDisp = dispinterface
    ['{46483440-EEFB-11D0-9ABC-0060977A5412}']
    property ColumnCount: Smallint readonly dispid 1;
    procedure SetupPagedData(nStart: Integer; nEnd: Integer; bLoadAll: Integer); dispid 2;
    procedure SortOnColumn(bAsync: Integer; nColumn: Smallint); dispid 3;
    procedure IsDataLoaded(out bIsLoaded: Integer); dispid 4;
    property ColumnInfo[nColumn: Smallint]: IColumnInfo dispid 5;
    procedure GetMinMaxValues(bIgnoreNegative: Integer; out pdbMin: Double; out pdbMax: Double; 
                              out pdbMinPareto: Double; out pdbMaxPareto: Double; 
                              out pdbParetoTotal: Double; out pdbTotals: OleVariant); dispid 6;
    procedure GetSelection(out pdwRowsSelected: OleVariant; out pdwColsSelected: OleVariant); dispid 7;
    procedure AddReportNotify(const prepNotify: IReportNotify); dispid 8;
    procedure DeleteReportNotify(const prepNotify: IReportNotify); dispid 9;
    property CellValue[nRow: Smallint; nCol: Smallint]: Double readonly dispid 10;
    property CellText[nRow: Smallint; nCol: Smallint]: WideString readonly dispid 11;
    property SortColumn: Smallint readonly dispid 12;
    procedure ExportData(eExportType: ENUM_EXPORTTYPE; const bstrFileName: WideString; 
                         lLineTypes: Integer); dispid 13;
    procedure RefreshAttachedObjects; dispid 14;
    property RIList: IRpiList dispid 15;
    procedure AddReportTableNotifyChange(const pNotify: IReportTableNotifyChange); dispid 16;
    procedure RemoveReportTableNotifyChange(const pNotify: IReportTableNotifyChange); dispid 17;
    property SummaryGroupingItem: IReportItemCfg dispid 18;
    property RowCount: Integer readonly dispid 19;
    property CellColor[iRow: SYSINT; iCol: SYSINT]: LongWord readonly dispid 20;
    property KeyColumn: Integer readonly dispid 21;
    procedure IsColSelected(iCol: SYSINT); dispid 22;
    property LINETYPE[iRow: SYSINT]: LINETYPE readonly dispid 23;
    property Cell[iRow: SYSINT; iCol: SYSINT]: OleVariant readonly dispid 24;
    property RowSelection[iRow: SYSINT]: Integer writeonly dispid 25;
    property ColumnSelection[iCol: SYSINT]: Integer writeonly dispid 26;
    procedure IsLineSelected(iRow: SYSINT); dispid 27;
    procedure LockData(bLock: Integer); dispid 28;
  end;

// *********************************************************************//
// Interface: IColumnInfo
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {396A9680-EF02-11D0-9ABC-0060977A5412}
// *********************************************************************//
  IColumnInfo = interface(IDispatch)
    ['{396A9680-EF02-11D0-9ABC-0060977A5412}']
    function  Get_Name: WideString; safecall;
    procedure Set_Name(const pVal: WideString); safecall;
    function  Get_Width: Smallint; safecall;
    procedure Set_Width(pVal: Smallint); safecall;
    function  Get_LeftJustify: Integer; safecall;
    procedure Set_LeftJustify(pVal: Integer); safecall;
    property Name: WideString read Get_Name write Set_Name;
    property Width: Smallint read Get_Width write Set_Width;
    property LeftJustify: Integer read Get_LeftJustify write Set_LeftJustify;
  end;

// *********************************************************************//
// DispIntf:  IColumnInfoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {396A9680-EF02-11D0-9ABC-0060977A5412}
// *********************************************************************//
  IColumnInfoDisp = dispinterface
    ['{396A9680-EF02-11D0-9ABC-0060977A5412}']
    property Name: WideString dispid 1;
    property Width: Smallint dispid 2;
    property LeftJustify: Integer dispid 3;
  end;

// *********************************************************************//
// Interface: IReportNotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8CDA2AE0-EF03-11D0-9ABC-0060977A5412}
// *********************************************************************//
  IReportNotify = interface(IDispatch)
    ['{8CDA2AE0-EF03-11D0-9ABC-0060977A5412}']
    procedure SetupAttributes(eReason: SETUPATTR_REASON; bRedraw: Integer; var hic: TGUID); safecall;
  end;

// *********************************************************************//
// DispIntf:  IReportNotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8CDA2AE0-EF03-11D0-9ABC-0060977A5412}
// *********************************************************************//
  IReportNotifyDisp = dispinterface
    ['{8CDA2AE0-EF03-11D0-9ABC-0060977A5412}']
    procedure SetupAttributes(eReason: SETUPATTR_REASON; bRedraw: Integer; 
                              var hic: {??TGUID} OleVariant); dispid 1;
  end;

// *********************************************************************//
// Interface: IRpiList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EA9DD953-C54C-11D1-8E42-0060080C772F}
// *********************************************************************//
  IRpiList = interface(IDispatch)
    ['{EA9DD953-C54C-11D1-8E42-0060080C772F}']
    function  Get_RI(pos: SYSINT): IReportItemCfg; safecall;
    procedure Set_RI(pos: SYSINT; const pVal: IReportItemCfg); safecall;
    property RI[pos: SYSINT]: IReportItemCfg read Get_RI write Set_RI;
  end;

// *********************************************************************//
// DispIntf:  IRpiListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EA9DD953-C54C-11D1-8E42-0060080C772F}
// *********************************************************************//
  IRpiListDisp = dispinterface
    ['{EA9DD953-C54C-11D1-8E42-0060080C772F}']
    property RI[pos: SYSINT]: IReportItemCfg dispid 1;
  end;

// *********************************************************************//
// Interface: IReportItemCfg
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EA9DD952-C54C-11D1-8E42-0060080C772F}
// *********************************************************************//
  IReportItemCfg = interface(IDispatch)
    ['{EA9DD952-C54C-11D1-8E42-0060080C772F}']
    function  Get_ID: Integer; safecall;
    procedure Set_ID(pVal: Integer); safecall;
    function  Get_SubItmNo: Smallint; safecall;
    procedure Set_SubItmNo(pVal: Smallint); safecall;
    function  Get_Name: WideString; safecall;
    procedure Set_Name(const pVal: WideString); safecall;
    procedure GetRange(out pbEnabled: Integer; out pdbStart: Double; out pdbEnd: Double); safecall;
    procedure SetRange(dbStart: Double; dbEnd: Double); safecall;
    procedure ClearRange; safecall;
    procedure Set_AlarmAbove(dbAlarmAbove: Double); safecall;
    function  Get_AlarmAbove: Double; safecall;
    procedure Set_AlarmBelow(dbAlarmBelow: Double); safecall;
    function  Get_AlarmBelow: Double; safecall;
    procedure Set_WarningAbove(dbWarningAbove: Double); safecall;
    function  Get_WarningAbove: Double; safecall;
    procedure Set_WarningBelow(dbWarningBelow: Double); safecall;
    function  Get_WarningBelow: Double; safecall;
    procedure Set_Format(const ppFormat: IFormat); safecall;
    function  Get_Format: IFormat; safecall;
    procedure EnableException(iException: SYSINT; bEnable: Integer); safecall;
    function  IsExceptionEnabled(iException: SYSINT): Integer; safecall;
    procedure Set_ExceptionMask(wExceptionEnabled: Smallint); safecall;
    function  Get_ExceptionMask: Smallint; safecall;
    procedure Set_CompareID(nID: Integer); safecall;
    function  Get_CompareID: Integer; safecall;
    function  Get_Width: Smallint; safecall;
    procedure Set_Width(wWidth: Smallint); safecall;
    property ID: Integer read Get_ID write Set_ID;
    property SubItmNo: Smallint read Get_SubItmNo write Set_SubItmNo;
    property Name: WideString read Get_Name write Set_Name;
    property AlarmAbove: Double read Get_AlarmAbove write Set_AlarmAbove;
    property AlarmBelow: Double read Get_AlarmBelow write Set_AlarmBelow;
    property WarningAbove: Double read Get_WarningAbove write Set_WarningAbove;
    property WarningBelow: Double read Get_WarningBelow write Set_WarningBelow;
    property Format: IFormat read Get_Format write Set_Format;
    property ExceptionMask: Smallint read Get_ExceptionMask write Set_ExceptionMask;
    property CompareID: Integer read Get_CompareID write Set_CompareID;
    property Width: Smallint read Get_Width write Set_Width;
  end;

// *********************************************************************//
// DispIntf:  IReportItemCfgDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EA9DD952-C54C-11D1-8E42-0060080C772F}
// *********************************************************************//
  IReportItemCfgDisp = dispinterface
    ['{EA9DD952-C54C-11D1-8E42-0060080C772F}']
    property ID: Integer dispid 1;
    property SubItmNo: Smallint dispid 2;
    property Name: WideString dispid 3;
    procedure GetRange(out pbEnabled: Integer; out pdbStart: Double; out pdbEnd: Double); dispid 4;
    procedure SetRange(dbStart: Double; dbEnd: Double); dispid 5;
    procedure ClearRange; dispid 6;
    property AlarmAbove: Double dispid 7;
    property AlarmBelow: Double dispid 8;
    property WarningAbove: Double dispid 9;
    property WarningBelow: Double dispid 10;
    property Format: IFormat dispid 11;
    procedure EnableException(iException: SYSINT; bEnable: Integer); dispid 12;
    function  IsExceptionEnabled(iException: SYSINT): Integer; dispid 13;
    property ExceptionMask: Smallint dispid 14;
    property CompareID: Integer dispid 15;
    property Width: Smallint dispid 16;
  end;

// *********************************************************************//
// Interface: IFormat
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9CF18040-4EA8-11D1-9171-0060080C772F}
// *********************************************************************//
  IFormat = interface(IDispatch)
    ['{9CF18040-4EA8-11D1-9171-0060080C772F}']
    function  Get_DecimalPlaces: Smallint; safecall;
    procedure Set_DecimalPlaces(pVal: Smallint); safecall;
    function  Get_NumberAsTime: Integer; safecall;
    procedure Set_NumberAsTime(pVal: Integer); safecall;
    function  Get_DateAsLong: Integer; safecall;
    procedure Set_DateAsLong(pVal: Integer); safecall;
    function  Get_FieldWidth: Integer; safecall;
    procedure Set_FieldWidth(pVal: Integer); safecall;
    function  Get_JUSTIFY: JUSTIFY; safecall;
    procedure Set_JUSTIFY(pVal: JUSTIFY); safecall;
    property DecimalPlaces: Smallint read Get_DecimalPlaces write Set_DecimalPlaces;
    property NumberAsTime: Integer read Get_NumberAsTime write Set_NumberAsTime;
    property DateAsLong: Integer read Get_DateAsLong write Set_DateAsLong;
    property FieldWidth: Integer read Get_FieldWidth write Set_FieldWidth;
    property JUSTIFY: JUSTIFY read Get_JUSTIFY write Set_JUSTIFY;
  end;

// *********************************************************************//
// DispIntf:  IFormatDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9CF18040-4EA8-11D1-9171-0060080C772F}
// *********************************************************************//
  IFormatDisp = dispinterface
    ['{9CF18040-4EA8-11D1-9171-0060080C772F}']
    property DecimalPlaces: Smallint dispid 1;
    property NumberAsTime: Integer dispid 2;
    property DateAsLong: Integer dispid 3;
    property FieldWidth: Integer dispid 4;
    property JUSTIFY: JUSTIFY dispid 5;
  end;

// *********************************************************************//
// Interface: IReportTableNotifyChange
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {61602D1A-995D-11D1-91E8-0060080C772F}
// *********************************************************************//
  IReportTableNotifyChange = interface(IDispatch)
    ['{61602D1A-995D-11D1-91E8-0060080C772F}']
    procedure IDListChange; safecall;
  end;

// *********************************************************************//
// DispIntf:  IReportTableNotifyChangeDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {61602D1A-995D-11D1-91E8-0060080C772F}
// *********************************************************************//
  IReportTableNotifyChangeDisp = dispinterface
    ['{61602D1A-995D-11D1-91E8-0060080C772F}']
    procedure IDListChange; dispid 1;
  end;

// *********************************************************************//
// Interface: IFilters
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2AAD5360-E622-11D0-9AAE-0000C088D00B}
// *********************************************************************//
  IFilters = interface(IDispatch)
    ['{2AAD5360-E622-11D0-9AAE-0000C088D00B}']
    function  Get_FirstFilterTest: IFilterTest; safecall;
    procedure Set_FirstFilterTest(const pVal: IFilterTest); safecall;
    function  Get_NextFilterTest(pos: SYSINT): IFilterTest; safecall;
    procedure Set_NextFilterTest(pos: SYSINT; const pVal: IFilterTest); safecall;
    function  Get_Name: WideString; safecall;
    function  Get_ID: Word; safecall;
    function  Get_AnyCondition: Integer; safecall;
    function  Get_HideMachines: Integer; safecall;
    property FirstFilterTest: IFilterTest read Get_FirstFilterTest write Set_FirstFilterTest;
    property NextFilterTest[pos: SYSINT]: IFilterTest read Get_NextFilterTest write Set_NextFilterTest;
    property Name: WideString read Get_Name;
    property ID: Word read Get_ID;
    property AnyCondition: Integer read Get_AnyCondition;
    property HideMachines: Integer read Get_HideMachines;
  end;

// *********************************************************************//
// DispIntf:  IFiltersDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2AAD5360-E622-11D0-9AAE-0000C088D00B}
// *********************************************************************//
  IFiltersDisp = dispinterface
    ['{2AAD5360-E622-11D0-9AAE-0000C088D00B}']
    property FirstFilterTest: IFilterTest dispid 1;
    property NextFilterTest[pos: SYSINT]: IFilterTest dispid 2;
    property Name: WideString readonly dispid 3;
    property ID: {??Word} OleVariant readonly dispid 4;
    property AnyCondition: Integer readonly dispid 5;
    property HideMachines: Integer readonly dispid 6;
  end;

// *********************************************************************//
// Interface: IBarcoReportCommon
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0745E6A0-4416-11D3-B923-006008515DBF}
// *********************************************************************//
  IBarcoReportCommon = interface(IReportCommon)
    ['{0745E6A0-4416-11D3-B923-006008515DBF}']
    function  Get_Filter_: IBarcoFilter; safecall;
    procedure Set_Filter_(const pVal: IBarcoFilter); safecall;
    procedure Set_ReportBanner(const pVal: IReportBanner); safecall;
    function  Get_ReportBanner: IReportBanner; safecall;
    function  Get_StatusCategoryArray: IDispatch; safecall;
    procedure Set_StatusCategoryArray(const pVal: IDispatch); safecall;
    procedure Set_FilterArray(const Param1: IFilterArray); safecall;
    property Filter_: IBarcoFilter read Get_Filter_ write Set_Filter_;
    property ReportBanner: IReportBanner read Get_ReportBanner write Set_ReportBanner;
    property StatusCategoryArray: IDispatch read Get_StatusCategoryArray write Set_StatusCategoryArray;
    property FilterArray: IFilterArray write Set_FilterArray;
  end;

// *********************************************************************//
// DispIntf:  IBarcoReportCommonDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0745E6A0-4416-11D3-B923-006008515DBF}
// *********************************************************************//
  IBarcoReportCommonDisp = dispinterface
    ['{0745E6A0-4416-11D3-B923-006008515DBF}']
    property Filter_: IBarcoFilter dispid 10;
    property ReportBanner: IReportBanner dispid 11;
    property StatusCategoryArray: IDispatch dispid 12;
    property FilterArray: IFilterArray writeonly dispid 13;
    property WarningAboveColor: LongWord dispid 1;
    property AlarmAboveColor: LongWord dispid 2;
    property WarningBelowColor: LongWord dispid 3;
    property AlarmBelowColor: LongWord dispid 4;
    property ReportTable: IReportTable dispid 5;
    property NormalColor: LongWord dispid 6;
    property MachineSelection: IMachineSelectionList dispid 7;
    property Filter: IFilters dispid 8;
    property Selection: Smallint dispid 9;
  end;

// *********************************************************************//
// Interface: IReportBanner
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {11EB82B0-7E0B-11D2-AA6C-00104B4B2529}
// *********************************************************************//
  IReportBanner = interface(IDispatch)
    ['{11EB82B0-7E0B-11D2-AA6C-00104B4B2529}']
    procedure Size(nBanner: ENUM_REPORTBANNER; var hic: TGUID; bPrinting: Integer; 
                   out pcx: Integer; out pcy: Integer); safecall;
    procedure Draw(nBanner: ENUM_REPORTBANNER; var hic: TGUID; var hdc: TGUID; bPrinting: Integer; 
                   out pcx: Integer; out pcy: Integer); safecall;
    procedure Initialize; safecall;
    function  Get_Margins: PSafeArray; safecall;
    property Margins: PSafeArray read Get_Margins;
  end;

// *********************************************************************//
// DispIntf:  IReportBannerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {11EB82B0-7E0B-11D2-AA6C-00104B4B2529}
// *********************************************************************//
  IReportBannerDisp = dispinterface
    ['{11EB82B0-7E0B-11D2-AA6C-00104B4B2529}']
    procedure Size(nBanner: ENUM_REPORTBANNER; var hic: {??TGUID} OleVariant; bPrinting: Integer; 
                   out pcx: Integer; out pcy: Integer); dispid 1;
    procedure Draw(nBanner: ENUM_REPORTBANNER; var hic: {??TGUID} OleVariant; 
                   var hdc: {??TGUID} OleVariant; bPrinting: Integer; out pcx: Integer; 
                   out pcy: Integer); dispid 2;
    procedure Initialize; dispid 3;
    property Margins: {??PSafeArray} OleVariant readonly dispid 4;
  end;

// *********************************************************************//
// Interface: IFilterArray
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BF68C9E2-494D-11D2-8ED5-0060080C772F}
// *********************************************************************//
  IFilterArray = interface(IDispatch)
    ['{BF68C9E2-494D-11D2-8ED5-0060080C772F}']
    function  Get_FirstCategory(var pos: SYSINT): IFilterCategory; safecall;
    procedure Set_FirstCategory(var pos: SYSINT; const pVal: IFilterCategory); safecall;
    function  Get_NextCategory(var pos: SYSINT): IFilterCategory; safecall;
    procedure Set_NextCategory(var pos: SYSINT; const pVal: IFilterCategory); safecall;
    function  Get_NumberOfElements: SYSINT; safecall;
    property FirstCategory[var pos: SYSINT]: IFilterCategory read Get_FirstCategory write Set_FirstCategory;
    property NextCategory[var pos: SYSINT]: IFilterCategory read Get_NextCategory write Set_NextCategory;
    property NumberOfElements: SYSINT read Get_NumberOfElements;
  end;

// *********************************************************************//
// DispIntf:  IFilterArrayDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BF68C9E2-494D-11D2-8ED5-0060080C772F}
// *********************************************************************//
  IFilterArrayDisp = dispinterface
    ['{BF68C9E2-494D-11D2-8ED5-0060080C772F}']
    property FirstCategory[var pos: SYSINT]: IFilterCategory dispid 1;
    property NextCategory[var pos: SYSINT]: IFilterCategory dispid 2;
    property NumberOfElements: SYSINT readonly dispid 3;
  end;

// *********************************************************************//
// Interface: IFilterCategory
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BF68C9E3-494D-11D2-8ED5-0060080C772F}
// *********************************************************************//
  IFilterCategory = interface(IDispatch)
    ['{BF68C9E3-494D-11D2-8ED5-0060080C772F}']
    function  Get_FirstFilter(var pos: SYSINT): IBarcoFilter; safecall;
    procedure Set_FirstFilter(var pos: SYSINT; const pVal: IBarcoFilter); safecall;
    function  Get_NextFilter(var pos: SYSINT): IBarcoFilter; safecall;
    procedure Set_NextFilter(var pos: SYSINT; const pVal: IBarcoFilter); safecall;
    function  Get_Name: WideString; safecall;
    function  AddFilter(const pIF: IBarcoFilter): SYSINT; safecall;
    procedure Remove(const bstr: WideString); safecall;
    function  Get_NumberOfElements: SYSINT; safecall;
    property FirstFilter[var pos: SYSINT]: IBarcoFilter read Get_FirstFilter write Set_FirstFilter;
    property NextFilter[var pos: SYSINT]: IBarcoFilter read Get_NextFilter write Set_NextFilter;
    property Name: WideString read Get_Name;
    property NumberOfElements: SYSINT read Get_NumberOfElements;
  end;

// *********************************************************************//
// DispIntf:  IFilterCategoryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BF68C9E3-494D-11D2-8ED5-0060080C772F}
// *********************************************************************//
  IFilterCategoryDisp = dispinterface
    ['{BF68C9E3-494D-11D2-8ED5-0060080C772F}']
    property FirstFilter[var pos: SYSINT]: IBarcoFilter dispid 1;
    property NextFilter[var pos: SYSINT]: IBarcoFilter dispid 2;
    property Name: WideString readonly dispid 3;
    function  AddFilter(const pIF: IBarcoFilter): SYSINT; dispid 4;
    procedure Remove(const bstr: WideString); dispid 5;
    property NumberOfElements: SYSINT readonly dispid 6;
  end;

// *********************************************************************//
// Interface: IFrameCon
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {85BF155A-600E-11D1-9186-0060080C772F}
// *********************************************************************//
  IFrameCon = interface(IDispatch)
    ['{85BF155A-600E-11D1-9186-0060080C772F}']
    procedure Run(hWnd: Integer; const pmf: IFloorFrame; const bstrTitle: WideString; 
                  bMakeDocked: Integer); safecall;
    function  GetWindow: Integer; safecall;
    procedure Set_NoOfFilters(Param1: SYSINT); safecall;
    procedure Set_FilterNo(iPos: SYSINT; const Param2: IFilters); safecall;
    procedure OnUpdate(lHint: SYSINT; pHint: OleVariant); safecall;
    property NoOfFilters: SYSINT write Set_NoOfFilters;
    property FilterNo[iPos: SYSINT]: IFilters write Set_FilterNo;
  end;

// *********************************************************************//
// DispIntf:  IFrameConDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {85BF155A-600E-11D1-9186-0060080C772F}
// *********************************************************************//
  IFrameConDisp = dispinterface
    ['{85BF155A-600E-11D1-9186-0060080C772F}']
    procedure Run(hWnd: Integer; const pmf: IFloorFrame; const bstrTitle: WideString; 
                  bMakeDocked: Integer); dispid 1;
    function  GetWindow: Integer; dispid 2;
    property NoOfFilters: SYSINT writeonly dispid 3;
    property FilterNo[iPos: SYSINT]: IFilters writeonly dispid 4;
    procedure OnUpdate(lHint: SYSINT; pHint: OleVariant); dispid 5;
  end;

// *********************************************************************//
// Interface: IFloorFrame
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C34F654E-6655-11D1-9196-0060080C772F}
// *********************************************************************//
  IFloorFrame = interface(IDispatch)
    ['{C34F654E-6655-11D1-9196-0060080C772F}']
    procedure AddDockedView(hWnd: Integer); safecall;
    procedure FloatView(hWnd: Integer); safecall;
    function  CanDock: WordBool; safecall;
    procedure ShuttingDown(hWnd: Integer); safecall;
    function  Get_MachineSelection: IBarcoMachineSelectionList; safecall;
    function  Get_Margins: OleVariant; safecall;
    procedure SaveReport(hWnd: Integer); safecall;
    procedure OnUpdate(lHint: SYSINT; pHint: OleVariant); safecall;
    property MachineSelection: IBarcoMachineSelectionList read Get_MachineSelection;
    property Margins: OleVariant read Get_Margins;
  end;

// *********************************************************************//
// DispIntf:  IFloorFrameDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C34F654E-6655-11D1-9196-0060080C772F}
// *********************************************************************//
  IFloorFrameDisp = dispinterface
    ['{C34F654E-6655-11D1-9196-0060080C772F}']
    procedure AddDockedView(hWnd: Integer); dispid 1;
    procedure FloatView(hWnd: Integer); dispid 2;
    function  CanDock: WordBool; dispid 3;
    procedure ShuttingDown(hWnd: Integer); dispid 4;
    property MachineSelection: IBarcoMachineSelectionList readonly dispid 5;
    property Margins: OleVariant readonly dispid 6;
    procedure SaveReport(hWnd: Integer); dispid 7;
    procedure OnUpdate(lHint: SYSINT; pHint: OleVariant); dispid 8;
  end;

// *********************************************************************//
// Interface: IBarcoFrameCon
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A92D4D00-913E-11D3-B8E5-006008515DBF}
// *********************************************************************//
  IBarcoFrameCon = interface(IFrameCon)
    ['{A92D4D00-913E-11D3-B8E5-006008515DBF}']
    procedure Set_FilterArray(const Param1: IFilterArray); safecall;
    property FilterArray: IFilterArray write Set_FilterArray;
  end;

// *********************************************************************//
// DispIntf:  IBarcoFrameConDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A92D4D00-913E-11D3-B8E5-006008515DBF}
// *********************************************************************//
  IBarcoFrameConDisp = dispinterface
    ['{A92D4D00-913E-11D3-B8E5-006008515DBF}']
    property FilterArray: IFilterArray writeonly dispid 6;
    procedure Run(hWnd: Integer; const pmf: IFloorFrame; const bstrTitle: WideString; 
                  bMakeDocked: Integer); dispid 1;
    function  GetWindow: Integer; dispid 2;
    property NoOfFilters: SYSINT writeonly dispid 3;
    property FilterNo[iPos: SYSINT]: IFilters writeonly dispid 4;
    procedure OnUpdate(lHint: SYSINT; pHint: OleVariant); dispid 5;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

end.
