(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: NumCtrl.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: Developed from Peter Worbis
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.08.2000  1.00  Wss | used in Floor on first call with empty trend report an
                          'Invalid floating point operation' occurs in FormatText method
|=========================================================================================*)
unit NumCtrl;
interface
uses
 SysUtils,WinTypes,WinProcs,Messages,Classes,Graphics,Controls,Forms,Dialogs,
 StdCtrls,Menus,DsgnIntF;

type
  {:----------------------------------------------------------------------------
   }
  TCustomStrEdit = class (TCustomEdit)
  private
    fAlignment: TAlignment;
    FOldAlignment: TAlignment;
    FTextMargin: Integer;
    function CalcTextMargin: Integer;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure SetAlignment(Value: TAlignment);
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
  protected
    property Alignment: TAlignment read fAlignment write SetAlignment default taRightJustify;
  public
    constructor Create(AOwner: TComponent); override;
  end;
  
  {:----------------------------------------------------------------------------
   }
  TStrEdit = class (TCustomStrEdit)
  published
    property Alignment;
    property AutoSize;
    property BorderStyle;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property Visible;
  end;
  
type
 TNumericType = (ntGeneral,ntCurrency,ntPercentage);
 TMaskString  = string[25];

type
  {:----------------------------------------------------------------------------
   Masks object                                                           }
  TMasks = class (TPersistent)
  private
    fNegativeMask: TMaskString;
    fOnChange: TNotifyEvent;
    fPositiveMask: TMaskString;
    fZeroMask: TMaskString;
  protected
    procedure SetNegativeMask(Value: TMaskString);
    procedure SetPositiveMask(Value: TMaskString);
    procedure SetZeroMask(Value: TMaskString);
  public
    constructor Create;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  published
    property NegativeMask: TMaskString read fNegativeMask write SetNegativeMask;
    property PositiveMask: TMaskString read fPositiveMask write SetPositiveMask;
    property ZeroMask: TMaskString read fZeroMask write SetZeroMask;
  end;
  
type
  {:----------------------------------------------------------------------------
   }
  TCustomNumEdit = class (TCustomStrEdit)
  private
    fDecimals: Word;
    fDigits: Word;
    fMasks: TMasks;
    fMax: Extended;
    fMin: Extended;
    fNumericType: TNumericType;
    fUseRounding: Boolean;
    fValidate: Boolean;
    fValidateString: string;
    fValue: Extended;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure SetDecimals(Value: Word);
    procedure SetDigits(Value: Word);
    procedure SetMasks(Mask: TMasks);
    procedure SetMax(Value: Extended);
    procedure SetMin(Value: Extended);
    procedure SetNumericType(Value: TNumericType);
    procedure SetValidate(Value: Boolean);
    procedure SetValidateString(const Value: string);
    procedure SetValue(Value: Extended);
  protected
    procedure FormatText; dynamic;
    procedure KeyPress(var Key: Char); override;
    procedure UnFormatText; dynamic;
    property Decimals: Word read fDecimals write SetDecimals;
    property Digits: Word read fDigits write SetDigits;
    property Masks: TMasks read fMasks write SetMasks;
    property Max: Extended read fMax write SetMax;
    property Min: Extended read fMin write SetMin;
    property NumericType: TNumericType read fNumericType write SetNumericType default ntCurrency;
    property UseRounding: Boolean read fUseRounding write fUseRounding;
    property Validate: Boolean read fValidate write SetValidate;
    property ValidateString: string read fValidateString write SetValidateString;
    property Value: Extended read fValue write SetValue;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function AsDouble: Double; dynamic;
    function AsInteger: Integer; dynamic;
    function AsLongint: LongInt; dynamic;
    function AsReal: Real; dynamic;
    function AsString: string; dynamic;
    function IsValid: Boolean;
    procedure MaskChanged(Sender: TObject);
    procedure ShowErrorMessage;
    function Valid(Value: extended): Boolean; dynamic;
  end;
  
  {:----------------------------------------------------------------------------
   }
  TNumEdit = class (TCustomNumEdit)
  published
    property AutoSize;
    property BorderStyle;
    property Color;
    property Ctl3D;
    property Decimals;
    property Digits;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection;
    property Masks;
    property Max;
    property Min;
    property NumericType;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property UseRounding;
    property Validate;
    property ValidateString;
    property Value;
    property Visible;
  end;
  
procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
const
  DefValidateStr = '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bis %s';
//  DefValidateStr = 'Wert ist nicht im erlaubten Eingabebereich.' +#10#13 +'Bereich von %s bis %s';

type
 TSetOfChar = set of char;

var
 OldMaxLength: integer;

function Power(x,y: integer): real;
begin
 result := exp(ln(x) *y);
end; //function Power
//-----------------------------------------------------------------------------
function StripChars(const Text: string; ValidChars: TSetOfChar): string;
var
 S        : string;
 i        : integer;
 Negative : boolean;

begin
 Negative := false;
 try
  if (Text[1] = '-') or (Text[length(Text)] = '-') then Negative := true;
  S := '';
  for i := 1 to length(Text) do
   if Text[i] in ValidChars then S := S +Text[i];

  if Negative then result := '-' +S
              else result := S;
 except
  result := '';
 end; //try..except
end; //function StripChars
//-----------------------------------------------------------------------------

//========================================================================
// Custom String Edit                                                     
//========================================================================

{:------------------------------------------------------------------------------
 TStrEdit}
{:------------------------------------------------------------------------------
 TNumEdit}
{:------------------------------------------------------------------------------
 TCustomStrEdit}
{:-----------------------------------------------------------------------------}
function TCustomStrEdit.CalcTextMargin: Integer;
var
  DC: HDC;
  SaveFont: HFont;
  I: Integer;
  SysMetrics, Metrics: TTextMetric;
begin
  DC := GetDC(0);
  GetTextMetrics(DC,SysMetrics);
  SaveFont := SelectObject(DC,Font.Handle);
  GetTextMetrics(DC,Metrics);
  SelectObject(DC,SaveFont);
  ReleaseDC(0,DC);
  I := SysMetrics.tmHeight;
  if I > Metrics.tmHeight then I := Metrics.tmHeight;
  result := I div 4;
end;

{:-----------------------------------------------------------------------------}
procedure TCustomStrEdit.CMEnter(var Message: TCMEnter);
begin
  inherited;
  FOldAlignment := FAlignment;
  Alignment := taLeftJustify;
end;

{:-----------------------------------------------------------------------------}
procedure TCustomStrEdit.CMExit(var Message: TCMExit);
begin
  inherited;
  Alignment := FOldAlignment;
end;

{:-----------------------------------------------------------------------------}
constructor TCustomStrEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FAlignment  := taLeftJustify;
  FTextMargin := CalcTextMargin;
end;

{:-----------------------------------------------------------------------------}
procedure TCustomStrEdit.SetAlignment(Value: TAlignment);
begin
  if FAlignment <> Value then
  begin
   FAlignment := Value;
   Invalidate;
  end; //if FAlignment <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomStrEdit.WMPaint(var Message: TWMPaint);
var
  Width, Indent, Left: Integer;
  R: TRect;
  DC: HDC;
  PS: TPaintStruct;
  S: string;
  Canvas: TControlCanvas;
begin
  if FAlignment = taLeftJustify then
  begin
   inherited;
   Exit;
  end; //if FAlignment = taLeftJustify then
  
  Canvas := TControlCanvas.Create;
  try
   Canvas.Control := Self;
   DC := Message.DC;
   if DC = 0 then DC := BeginPaint(Handle,PS);
   Canvas.Handle := DC;
  
   Canvas.Font := Font;
   with Canvas do
   begin
    R := ClientRect;
    Brush.Color := Color;
    S := Text;
    Width := TextWidth(S);
    if BorderStyle = bsNone then Indent := 0
                            else Indent := FTextMargin;
    if FAlignment = taRightJustify then Left := R.Right - Width - Indent
                                   else Left := (R.Left + R.Right - Width) div 2;
    TextRect(R,Left,Indent,S);
   end; //with Canvas do
  finally
   Canvas.Handle := 0;
   if Message.DC = 0 then EndPaint(Handle,PS);
  end; //try..finally
end;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//========================================================================
// Masks object                                                           
//========================================================================

{:------------------------------------------------------------------------------
 TMasks}
{:-----------------------------------------------------------------------------}
constructor TMasks.Create;
begin
  inherited Create;
  FPositiveMask := '#,##0';
  FNegativeMask := '';
  FZeroMask     := '';
end;

{:-----------------------------------------------------------------------------}
procedure TMasks.SetNegativeMask(Value: TMaskString);
begin
  if FNegativeMask <> Value then
  begin
   FNegativeMask := Value;
   OnChange(Self);
  end; //if FNegativeMask <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TMasks.SetPositiveMask(Value: TMaskString);
begin
  if FPositiveMask <> Value then
  begin
   FPositiveMask := Value;
   OnChange(Self);
  end; //if FPositiveMask <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TMasks.SetZeroMask(Value: TMaskString);
begin
  if FZeroMask <> Value then
  begin
   FZeroMask := Value;
   OnChange(Self);
  end; //procedure TMasks.SetZeroMask
end;

//========================================================================
// Custom Numeric Edit                                                    
//========================================================================

{:------------------------------------------------------------------------------
 TCustomNumEdit}
{:-----------------------------------------------------------------------------}
function TCustomNumEdit.AsDouble: Double;
  const
   MaxDouble: double = 1.7E308;
   MinDouble: double = -1.7E308;
begin
  result := 0;
  if (FValue < MaxDouble) and  (FValue > MinDouble) then result := FValue;
end;

{:-----------------------------------------------------------------------------}
function TCustomNumEdit.AsInteger: Integer;
  const
   MaxInteger: integer = 32767;
   MinInteger: integer = -32768;
begin
  result := 0;
  if (FValue < MaxInteger) and (FValue > MinInteger) then
   if FUseRounding then result := round(FValue)
                   else result := trunc(FValue);
end;

{:-----------------------------------------------------------------------------}
function TCustomNumEdit.AsLongint: LongInt;
  const
   MaxLongint: longint = 2147483647;
   MinLongint: longint = -2147483647;
begin
  result := 0;
  if (FValue < MaxLongint) and  (FValue > MinLongint) then
   if FUseRounding then result := round(FValue)
                   else result := trunc(FValue);
end;

{:-----------------------------------------------------------------------------}
function TCustomNumEdit.AsReal: Real;
  const
   MaxReal: real = 1.7E38;
   MinReal: real = -1.7E38;
begin
  result := 0;
  if (FValue < MaxReal) and  (FValue > MinReal) then result := FValue;
end;

{:-----------------------------------------------------------------------------}
function TCustomNumEdit.AsString: string;
var
  ValidChars: TSetOfChar;
begin
  ValidChars := ['0'..'9',DecimalSeparator,ThousandSeparator];
  result := StripChars(Text,ValidChars);
  if Value < 0 then result := '-' + result;
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.CMEnter(var Message: TCMEnter);
begin
  UnFormatText;
  OldMaxLength := MaxLength;
  MaxLength    := FDigits;
  inherited;
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.CMExit(var Message: TCMExit);
var
  S: string [80];
  X: Extended;
begin
  MaxLength := OldMaxLength;
  x := 0;
  S := StripChars(Text,['0'..'9',DecimalSeparator]);
  if S <> '' then
  begin
   try
    X := StrToFloat(S);
   except
    MessageBeep(0);
    MessageDlg('Format ???',mtError,[mbOk],0);
    SetFocus;
    Abort;
   end; //try..except
  end //if S <> '' then
  else X := 0.0;
  
  if Valid(X) then
  begin
   if FNumericType = ntPercentage then FValue := X /100
                                  else FValue := X;
   FormatText;
   inherited;
  end //if Valid(X) then
  else
  begin
   SelectAll;
   SetFocus;
  end; //else if Valid(X) then
end;

{:-----------------------------------------------------------------------------}
constructor TCustomNumEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Width := 85;
  FAlignment := taRightJustify;
  FNumericType := ntCurrency;
  FDigits := 12;
  FDecimals := 2;
  AutoSelect := true;
  FMax := 0.0;
  FMin := 0.0;
  FValidate := false;
  FValidateString := DefValidateStr;
  FValue := 0.0;
  FormatText;
  FTextMargin := CalcTextMargin;
  FUseRounding := true;
  FMasks := TMasks.Create;
  FMasks.OnChange := MaskChanged;
end;

{:-----------------------------------------------------------------------------}
destructor TCustomNumEdit.Destroy;
begin
  FMasks.Free;
  inherited Destroy;
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.FormatText;
var
  X: Extended;
  Multiplier: Real;
begin
  try
   Multiplier := Power(10,Decimals);
   if FNumericType = ntPercentage then X := FValue *100
                                  else X := FValue;
   if UseRounding then X := round(X *Multiplier) /Multiplier
                  else X := trunc(X *Multiplier) /Multiplier;
  except
    // wss: used in Floor on first call with empty trend report an
    // 'Invalid floating point operation' occurs which isn't handled in the
    // ERangeError below
    on e:Exception do begin
      X := FValue;
    end;
 {
   on ERangeError do
    X := FValue; //will cause rounding in the FloatToStr function
{}
  end; //try..except
  
  case FNumericType of
   ntCurrency   : Text := FloatToStrF(X,ffCurrency,FDigits,FDecimals);
   ntPercentage : Text := FloatToStrF(X,ffFixed,FDigits,FDecimals) + '%';
   ntGeneral    : with Masks do
                   Text := FormatFloat(PositiveMask+';'+NegativeMask+';'+ZeroMask,X);
  end; //case FNumericType of
end;

{:-----------------------------------------------------------------------------}
function TCustomNumEdit.IsValid: Boolean;
begin
  result := (Value >= FMin) and (Value <= FMax);
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.KeyPress(var Key: Char);
begin
  if (Key = DecimalSeparator) and (pos(DecimalSeparator,Text) = 0) then
  begin
   inherited KeyPress(Key);
   MaxLength := MaxLength +1;
  
   if (pos(DecimalSeparator,Text) = 1) then text := '0' +text;
  end //if (Key = DecimalSeparator) and (pos(DecimalSeparator,Text) = 0) then
  else
  begin
   if (Key = '-') and (pos('-',Text) = 0) then
   begin
    inherited KeyPress(Key);
    MaxLength := MaxLength +1;
   end //if (Key = '-') and (pos('-',Text) = 0) then
   else
   begin
    if Key in ['0'..'9','-',DecimalSeparator,ThousandSeparator,#8]
     then inherited KeyPress(Key)
     else Key := #0;
   end; //else if (Key = '-') and (pos('-',Text) = 0) then
  end; //else if (Key = DecimalSeparator) and (pos(DecimalSeparator,Text) = 0) then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.MaskChanged(Sender: TObject);
begin
  FormatText;
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.SetDecimals(Value: Word);
begin
  if FDecimals <> Value then
  begin
   FDecimals := Value;
   FormatText;
  end; //if FDecimals <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.SetDigits(Value: Word);
begin
  if FDigits <> Value then
  begin
   FDigits := Value;
   FormatText;
  end; //if FDigits <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.SetMasks(Mask: TMasks);
begin
  if fMasks <> Mask then
  begin
   fMasks := Masks;
   Invalidate;
  end; //if fMasks <> Mask then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.SetMax(Value: Extended);
begin
  if FMax <> Value then
  begin
   FMax := Value;
   //if FValue > FMax then FValue := FMax;
  end; //if FMax <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.SetMin(Value: Extended);
begin
  if FMin <> Value then
  begin
   FMin := Value;
   //if FValue < FMin then FValue := FMin;
  end; //if FMin <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.SetNumericType(Value: TNumericType);
begin
  if FNumericType <> Value then
  begin
   FNumericType := Value;
   FormatText;
  end; //if FNumericType <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.SetValidate(Value: Boolean);
begin
  if FValidate <> Value then
  begin
   FValidate := Value;
   if FValidate and ((FValue < FMin) or (FValue > FMax)) then
   begin
    FValue := FMin;
    FormatText;
   end; //if FValidate and ((FValue < FMin) or (FValue > FMax)) then
  end; //if FValidate <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.SetValidateString(const Value: string);
  const
   EngStr = 'Value must be between %g and %g';
begin
  if FValidateString <> Value then
  begin
   if Value <> '' then FValidateString := Value
                  else FValidateString := DefValidateStr;
  end; //if FValidateString <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.SetValue(Value: Extended);
begin
  if (FValue <> Value) and (Valid(Value)) then
  begin
   FValue := Value;
   FormatText;
  end; //if (FValue <> Value) and (Valid(Value)) then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.ShowErrorMessage;
var
  sMin, sMax, s: string;
begin
  if not Isvalid then
  begin
   with Masks do
   begin
    sMin := FormatFloat(PositiveMask+';'+NegativeMask+';'+ZeroMask,FMin);
    sMax := FormatFloat(PositiveMask+';'+NegativeMask+';'+ZeroMask,FMax);
   end; //with Masks do
  
   s := format(FValidateString,[sMin,sMax]);
   MessageBeep(0);
   MessageDlg(s,mtError,[mbOk],0);
  
   SelectAll;
   SetFocus;
  end; //if not Isvalid then
end;

{:-----------------------------------------------------------------------------}
procedure TCustomNumEdit.UnFormatText;
begin
  Text := StripChars(Text,['0'..'9',DecimalSeparator,ThousandSeparator]);
  if Value < 0 then Text := '-' + Text;
end;

{:-----------------------------------------------------------------------------}
function TCustomNumEdit.Valid(Value: extended): Boolean;
begin
  result := true;
  if Validate and (not Isvalid) then
  begin
   try
    ShowErrorMessage;
   finally
    Alignment := FOldAlignment;
   end; //try..finally
  end; //if Validate and (not Isvalid) then
end;
//------------------------------------------------------------------------------
procedure Register;
begin
  RegisterComponents('Worbis', [TNumEdit]);
  RegisterComponents('Worbis', [TStrEdit]);
end; //procedure Register

end. //NumCtrl.pas
