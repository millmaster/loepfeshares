(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: EventLog.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Komponenten fuer die Verwendung des Windows NT EventLog's
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 07.07.1998  1.00  Wss | Datei erstellt, Klasse TEventLogWriter implementiert und kompletiert
|                       | xEv := TEventLogWriter.Create('MillMaster', 'pcwss', ssMsgHandler, 'MsgDispatcher: ', True);
|                       | xEv.Write(etInformation, 'Test message')
| 17.09.1998  1.01  Wss | Eventlog Konstanten sind schon in Windows Unit definiert:
|                       | hier entfernt
| 02.10.1998  1.02  Wss | CAT_CIHANDLER zugefuegt, MM-Konstanten auf MM_DEFAULTMESSAGE
|                       | reduziert
| 20.10.1998  1.03  Wss | Fehlerkonstanten bei Write() und WriteBin() gesetzt, wenn nicht OPEN.
|                       | CAT_MMCLIENT zugefuegt.
| 20.01.2000  1.04  Wss | Globale WriteToEventLog() Methode zugefuegt
| 28.08.2001  1.05  Wss | Renamed to MMEventLog because of problems with ThirdPart NTSet
| 30.11.2001  1.06  Wss | TEventLogWriter.Write: send to CodeSite with Error, Warning added
| 02.05.2002  1.06  Wss | WriteToEventLogDebug added
| 11.02.2005  1.06  Wss | In Create: wenn ssApplication dann ExtractFileName(ParamStr(0)) zu DefaultText hinzugefügt
|=========================================================================================*)
unit MMEventLog;

interface

uses
  Windows, Classes, Graphics, LoepfeGlobal;

const
  // Identifiers for NT event classification
  EVENTLOG_FORWARDS_READ   =  $4;
  EVENTLOG_BACKWARDS_READ  =  $8;
  EVENTLOG_SEQUENTIAL_READ =  $1;
  EVENTLOG_SEEK_READ       =  $2;

type
  TEventType    = (etSuccess, etInformation, etWarning, etError);
  TEventTypeSet = set of TEventType;

const
  cEventColors: Array[TEventType] of TColor = (clGreen, clNavy, clYellow, clRed);

type
  TEventLogWriter = class(TObject)
  private
    mCategory:  Integer;
    mClassName: String;
    mCompName: String;
    mDefaultText: String;
    mHandle: THandle;
    fOpen: Boolean;
    fError: DWord;
    procedure SetOpen(aValue: Boolean);
    function GetOpen: Boolean;
  protected
  public
    constructor Create(aClassName: String; aCompName: String; aSubSystem: TSubSystemTyp; aDefaultText: String; aOpen: Boolean);
    destructor Destroy; override;
    function Write(aEvent: TEventType; aText: String): Boolean;
    function WriteBin(aEvent: TEventType; aText: String; aBuffer: PByte; aSize: DWord): Boolean;

    property Open: Boolean read GetOpen write SetOpen default False;
    property Error: DWord read fError;
  end;


//------------------------------------------------------------------------------
procedure WriteToEventLogDebug(aMsg: String; aDefaultText: String = '';
                          aEvent: TEventType = etInformation;
                          aSystemTyp: TSubSystemTyp = ssApplication);


procedure WriteToEventLog(aMsg: String; aDefaultText: String;
                          aEvent: TEventType = etInformation;
                          aClassName: String = '';
                          aCompName: String  = '';
                          aSystemTyp: TSubSystemTyp = ssApplication);
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  mmCS, sysutils;
  
const
  // default message identifier
  MM_DEFAULTMESSAGE      = $40000001;
  // MillMaster Category classification
  CAT_MMGUARD            = $00000002;
  CAT_MMCLIENT           = $00000003;
  CAT_JOBHANDLER         = $00000004;
  CAT_MSGHANDLER         = $00000005;
  CAT_STORAGEHANDLER     = $00000006;
  CAT_TIMEHANDLER        = $00000007;
  CAT_TXNHANDLER         = $00000008;
  CAT_WSCHANDLER         = $00000009;
  CAT_OEHANDLER          = $0000000A;
  CAT_CIHANDLER          = $0000000B;
  CAT_APPLICATION        = $0000000C;
  CAT_WSCRPCHANDLER      = $0000000D;
  CAT_MMSETUP            = $0000000E;

  cMMCategory: Array[TSubSystemTyp] of Integer =
    (CAT_MMGUARD,
     CAT_MMCLIENT,
     CAT_JOBHANDLER,
     CAT_MSGHANDLER,
     CAT_STORAGEHANDLER,
     CAT_TIMEHANDLER,
     CAT_TXNHANDLER,
     CAT_WSCHANDLER,
     CAT_OEHANDLER,
     CAT_CIHANDLER,
     CAT_APPLICATION,
     CAT_WSCRPCHANDLER,
     CAT_MMSETUP);

  cNTEventLog: Array[TEventType] of Integer =
    (EVENTLOG_SUCCESS,
     EVENTLOG_INFORMATION_TYPE,
     EVENTLOG_WARNING_TYPE,
     EVENTLOG_ERROR_TYPE);

//-----------------------------------------------------------------------------
procedure WriteToEventLog(aMsg: String; aDefaultText: String; aEvent: TEventType; aClassName: String;
                          aCompName: String; aSystemTyp: TSubSystemTyp);
begin
  if aCompName = '' then
    aCompName := gMMHost;
  if aClassName = '' then
    aClassName := 'MillMaster';

  with TEventLogWriter.Create(aClassName, aCompName, aSystemTyp, aDefaultText, True) do
  try
    Write(aEvent, aMsg);
  finally
    Free;
  end;

  if (aEvent = etInformation) or (aEvent = etSuccess) then
    CodeSite.SendMsg(aDefaultText + aMsg);
end;
//-----------------------------------------------------------------------------
procedure WriteToEventLogDebug(aMsg: String; aDefaultText: String = '';
                               aEvent: TEventType = etInformation;
                               aSystemTyp: TSubSystemTyp = ssApplication);

begin
  if CodeSite.Enabled then begin
    WriteToEventLog(aMsg, aDefaultText, aEvent, 'MillMasterDebug', '', aSystemTyp);
  end;
end;
//******************************************************************************
// TEventLogWriter class
//******************************************************************************
constructor TEventLogWriter.Create(aClassName: String; aCompName: String; aSubSystem: TSubSystemTyp; aDefaultText: String; aOpen: Boolean);
begin
  inherited Create;

  mCategory  := cMMCategory[aSubSystem];
  mClassName := aClassName;
  mCompName  := aCompName;
  mDefaultText := aDefaultText;
  if aSubSystem = ssApplication then
    mDefaultText := ExtractFileName(ParamStr(0)) + ' ' + mDefaultText;
  mHandle    := 0;

  fOpen      := False;
  fError     := NO_ERROR;
  Open       := aOpen;
end;
//-----------------------------------------------------------------------------
destructor TEventLogWriter.Destroy;
begin
  Open := False;
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TEventLogWriter.GetOpen: Boolean;
begin
  // if it isn't open yet try to open connection to event log
  if not fOpen then begin
    Open := True;
  end;
  Result := fOpen;
end;
//------------------------------------------------------------------------------
procedure TEventLogWriter.SetOpen(aValue: Boolean);
begin
  if aValue <> fOpen then begin
    fOpen := aValue;
    if fOpen then begin
      if mHandle <> 0 then
        DeregisterEventSource(mHandle);
      mHandle := RegisterEventSource(PChar(mCompName), PChar(mClassName));
      if mHandle = 0 then begin
        fOpen := False;
        fError := GetLastError;
      end;
    end else begin
      if not DeregisterEventSource(mHandle) then
        fError := GetLastError;
      mHandle := 0;
    end;
  end;
end;
//-----------------------------------------------------------------------------
function TEventLogWriter.Write(aEvent: TEventType; aText: String): Boolean;
var
  xStringArr: Array[0..0] Of PChar;
  xStr: String;
begin
  Result := False;
  if Open then begin
    xStr := mDefaultText + aText;
    xStringArr[0] := PChar(xStr);
    Result := ReportEvent(mHandle, cNTEventLog[aEvent], mCategory, MM_DEFAULTMESSAGE, Nil, 1, 0, @xStringArr, Nil);

    case aEvent of
      etError:   CodeSite.SendError(aText);
      etWarning: CodeSite.SendWarning(aText);
    else
    end;

    if Result then fError := NO_ERROR
    else           fError := GetLastError;
  end else
    fError := ERROR_NOT_CONNECTED;
end;
//-----------------------------------------------------------------------------
function TEventLogWriter.WriteBin(aEvent: TEventType; aText: String; aBuffer: PByte; aSize: DWord): Boolean;
var
  xStringArr: Array[0..0] of PChar;
  xStr: String;
begin
  Result := False;
  if Open then begin
    xStr := mDefaultText + aText;
    xStringArr[0] := PChar(xStr);
    Result := ReportEvent(mHandle, cNTEventLog[aEvent], mCategory, MM_DEFAULTMESSAGE, Nil, 1, aSize, @xStringArr, aBuffer);
    if Result then fError := NO_ERROR
    else           fError := GetLastError;
  end else
    fError := ERROR_NOT_CONNECTED;
end;
//-----------------------------------------------------------------------------
end.

