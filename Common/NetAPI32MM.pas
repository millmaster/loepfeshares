(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: NetAPI32MM.pas
| Projectpart...: Allg.
| Subpart.......: -
| Process(es)...: -
| Description...:  Div. Netzfunktionen
                   GetUserAccountInfo -> Ermittelt alle AccountInfos eines Users
                   GetServerName      -> Emittlet die Servernamen
                                         eines bestimmten Typs
                   GetUserGroupName   -> Ermittelt die Gruppenzugehoerigkeit
                                         eines Users
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 25.09.1998  0.00  Sdo | Erstellt
| Nov   1999  1.00  Wss | Korrekturen: List nun als Parameter anstelle Result
| 12.04.2000  1.00  Wss | GetUserGroups(): wenn Servername = '' dann lokale Gruppen holen
| 17.10.2005  1.00  Wss | GetUserGroups(): Error bei Login in Floor -> Funktionen aus NetAPI32 verwenden
|=============================================================================*)
unit NetAPI32MM;

interface

uses Windows, classes, SysUtils, 
     NetErrors;

Const
  cUSER_PRIV_GUEST   =  0;
  cUSER_PRIV_USER    =  1;
  cUSER_PRIV_ADMIN   =  2;

  // Konstanten zur ermittlung der Server
  cSV_TYPE_WORKSTATION          = $00000001;
  cSV_TYPE_SERVER               = $00000002;
  cSV_TYPE_SQLSERVER            = $00000004;
  cSV_TYPE_DOMAIN_CTRL          = $00000008;
  cSV_TYPE_DOMAIN_BAKCTRL       = $00000010;
  cSV_TYPE_TIME_SOURCE          = $00000020;
  cSV_TYPE_AFP                  = $00000040;
  cSV_TYPE_NOVELL               = $00000080;
  cSV_TYPE_DOMAIN_MEMBER        = $00000100;
  cSV_TYPE_PRINTQ_SERVER        = $00000200;
  cSV_TYPE_DIALIN_SERVER        = $00000400;
  cSV_TYPE_XENIX_SERVER         = $00000800;
  cSV_TYPE_SERVER_UNIX          = cSV_TYPE_XENIX_SERVER;
  cSV_TYPE_NT                   = $00001000;
  cSV_TYPE_WFW                  = $00002000;
  cSV_TYPE_SERVER_MFPN          = $00004000;
  cSV_TYPE_SERVER_NT            = $00008000;
  cSV_TYPE_POTENTIAL_BROWSER    = $00010000;
  cSV_TYPE_BACKUP_BROWSER       = $00020000;
  cSV_TYPE_MASTER_BROWSER       = $00040000;
  cSV_TYPE_DOMAIN_MASTER        = $00080000;
  cSV_TYPE_SERVER_OSF           = $00100000;
  cSV_TYPE_SERVER_VMS           = $00200000;
  cSV_TYPE_WINDOWS              = $00400000;  //* Windows95 and above */
  cSV_TYPE_DFS                  = $00800000;  //* Root of a DFS tree */
  cSV_TYPE_ALTERNATE_XPORT      = $20000000;  //* return list for alternate transport */
  cSV_TYPE_LOCAL_LIST_ONLY      = $40000000;  //* Return local list only */
  cSV_TYPE_DOMAIN_ENUM          = $80000000;
  cSV_TYPE_ALL                  = $FFFFFFFF;  //* handy for NetServerEnum2 */

TYPE
 NET_API_STATUS = DWORD;
 PSomeChar   = Pointer;

 TNetApiBufferFree = function (P: Pointer): NET_API_STATUS; stdcall;

//*****************************************************************************
// Userinfo
//*****************************************************************************
 PUser_Info_3 = ^TUser_Info_3;
 TNetUserGetInfo = function(servername, username: PSomeChar;
                            level: DWORD;
                            var bufptr: Pointer
                            ): NET_API_STATUS;  stdcall;

 TUser_Info_3 = record
   usri3_name:          PSomeChar;
   usri3_password:      PSomeChar;
   usri3_password_age:  DWORD;
   usri3_priv:          DWORD;
   usri3_home_dir:      PSomeChar;
   usri3_comment:       PSomeChar;
   usri3_flags:         DWORD;
   usri3_script_path:   PSomeChar;
   usri3_auth_flags:    DWORD;
   usri3_full_name:     PSomeChar;
   usri3_usr_comment:   PSomeChar;
   usri3_parms:         PSomeChar;
   usri3_workstations:  PSomeChar;
   usri3_last_logon:    DWORD;
   usri3_last_logoff:   DWORD;
   usri3_acct_expires:  DWORD;
   usri3_max_storage:   DWORD;
   usri3_units_per_week:DWORD;
   usri3_logon_hours:   PBYTE;
   usri3_bad_pw_count:  DWORD;
   usri3_num_logons:    DWORD;
   usri3_logon_server:  PSomeChar;
   usri3_country_code:  DWORD;
   usri3_code_page:     DWORD;
   usri3_user_id:       DWORD;
   usri3_primary_group_id: DWORD;
   usri3_profile:       PSomeChar;
   usri3_home_dir_drive: PSomeChar;
   usri3_password_expired: DWORD;
 end;

 PUser_Info_3_Arr=^TUser_Info_3_Arr;
 TUser_Info_3_Arr=array[0..MaxInt div SizeOf(TUser_info_3) - 1] of TUser_info_3;
//******************************************************************************
// Serverinfo
//******************************************************************************
 PServer_Info_101 = ^TServer_Info_101;
 TNetServerEnum = function(servername: PSomeChar;
                           level: DWORD;
                           var bufptr: Pointer;
                           prefmaxlen: DWORD;
                           var entriesread: DWORD;
                           var totalentries: DWORD;
                           servertype: DWORD;
                           domain: PSomeChar;
                           var resume_handle: DWORD
                           ):NET_API_STATUS;  stdcall;

 TServer_Info_101 = record
   sv101_plattform_id : DWORD;
   sv101_name         : PSomeChar;
   sv101_version_major: DWORD;
   sv101_version_minor: DWORD;
   sv101_type         : DWORD;
   sv101_commet       : PSomeChar;
 end;

 PServer_Info_101_Arr = ^TServer_Info_101_Arr;
 TServer_Info_101_Arr = array[0..MaxInt div
                              SizeOf(TServer_Info_101) - 1] of TServer_Info_101;
//******************************************************************************
// User Gruppen
//******************************************************************************
 PGroup_Info_0 = ^TGroup_Info_0;
 PGroupUser_Info_1 = ^TGroupUser_Info_1;

 // Server
 TNetUserGetGroups = function(servername: PSomeChar;
                              username: PSomeChar;
                              level: DWORD;
                              var bufptr: Pointer;
                              prefmaxlen: DWORD;
                              var entriesread: DWORD;
                              var totalentries: DWORD
                              ): NET_API_STATUS;  stdcall;
 // Local
 TNetUserGetLocalGroups = function(servername: PSomeChar;
                                   username: PSomeChar;
                                   Flags: DWORD;
                                   level: DWORD;
                                   var bufptr: Pointer;
                                   prefmaxlen: DWORD;
                                   var entriesread: DWORD;
                                   var totalentries: DWORD
                                   ): NET_API_STATUS;  stdcall;

 TGroup_Info_0 = record
   grpi0_name: PSomeChar;
 end;

 TGroupUser_Info_1 = record
   grui1_name:          PSomeChar;
   grui1_attributes:    DWORD;
 end;

 PGroup_Info_0_Arr = ^TGroup_Info_0_Arr;
 TGroup_Info_0_Arr = array[0..MaxInt div SizeOf(TGroup_Info_0) - 1] of TGroup_Info_0;


 PGroupUser_Info_1_Arr = ^TGroupUser_Info_1_Arr;
 TGroupUser_Info_1_Arr = array[0..MaxInt div
                            SizeOf(TGroupUser_Info_1) - 1] of TGroupUser_Info_1;
//******************************************************************************

//------------------------------------------------------------------------------
// Public PROTOTYPES
//------------------------------------------------------------------------------
// Ermittelt alle AccountInfos eines Users
procedure GetUserAccountInfo(aUsername, aSvrname: String; var aList: TStringList);
// Emittlet die Servernamen eines bestimmten Typs
procedure GetServerName(aDomain: String; aServerTyp: Integer; var aList: TStringList);
// Ermittelt die Gruppenzugehoerigkeit eines Users auf einen Server
procedure GetUserGroupName(aUsername, aSvrname: String; var aList: TStringList);
// Ermittelt die Gruppenzugehoerigkeit eines localen Users
procedure GetUserLocalGroupName(aUsername, aSvrname: String; var aList: TStringList);
// Ermittelt alle Domaenen im Netz
// function  GetAllDomains(aUserName: string): TStringList;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, NetApi32;
//------------------------------------------------------------------------------
// Private Funktionen
{
 function GetOS:DWORD; // Ermittelt das Betriebssystem
 function GetPrivilegeName(ACod: integer): string;
 function GetPointer(var s: string): pchar;
{}


//------------------------------------------------------------------------------
{
var NetUserGetInfo: TNetUserGetInfo;
    NetServerEnum: TNetServerEnum;
//    NetUserGetGroups: TNetUserGetGroups;
//    NetUserGetLocalGroups : TNetUserGetLocalGroups;
    NetApiBufferFree: TNetApiBufferFree;
{}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Ermittelt das Betriebssystem
function GetOS:DWORD;
var os : TOSVERSIONINFO;
begin
  os.dwOSVersionInfoSize := sizeof(os);
  GetVersionEx(os);
  Result:=  os.dwPlatformId;
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Wandelt die Privilege-Konstante in einen String um
function GetPrivilegeName(ACod: integer): string;
begin
  case ACod of
    cUSER_PRIV_GUEST: Result := 'Guest';
    cUSER_PRIV_USER:  Result := 'User';
    cUSER_PRIV_ADMIN: Result := 'Admin';
    else Result := 'Unknown';
  end;
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Kovertiert einen String vom Typ String in PChar um oder gibt NIL retour
function GetPointer(var s: string): pchar;
begin
  if s = '' then result := nil else result := PChar(s);
end;
//------------------------------------------------------------------------------



//******************************************************************************
// Ermittelt anhand des Login-Namens und des Server-Names
// die Account-Einstellung des Users aus der NT-Userdatenbank
// Eingabe : Username und Server
// Ausgabe : Stringlist mit den Accountdaten -> API USER_INFO_3
//******************************************************************************
// Autor   : Sdo
// Datum   : 24.09.1998
//******************************************************************************
procedure GetUserAccountInfo(aUsername, aSvrname: String; var aList: TStringList);
var
  xRes :                    NET_API_STATUS;
  xPWChUser, xPWChMachine : array[0..50] of WideChar;

  xDllName :                String;
  xDll_HModule :            HModule;
  xBuf :                    PUser_Info_3_Arr;
  xNetUserGetInfo: TNetUserGetInfo;
  xNetApiBufferFree: TNetApiBufferFree;
begin
  if not Assigned(aList) or (trim(aUsername) = '' ) then
    Exit;

  if (trim(aSvrname) = '' )  then
     aSvrname := ''
  else
     aSvrname := '\\' + aSvrname;

  // Betriebssystem ermitteln
  case GetOS of
    VER_PLATFORM_WIN32s	       : exit;  // Win 3.1
    VER_PLATFORM_WIN32_WINDOWS : xDllName := 'radmin32.dll'; // Win95
    VER_PLATFORM_WIN32_NT      : xDllName := 'netapi32.dll'; // WinNT
    else exit;
  end;

  // DLL laden
  xDll_HModule := LoadLibrary(PChar(xDllName));

  StringToWideChar(trim(aUsername), xPWChUser, SizeOf(xPWChUser));
  StringToWideChar(trim(aSvrname), xPWChMachine, SizeOf(xPWChMachine));

  xNetUserGetInfo := GetProcAddress(xDll_HModule, PChar('NetUserGetInfo'));
  xNetApiBufferFree := GetProcAddress(xDll_HModule, 'NetApiBufferFree');

  xbuf := NIL;
  xRes := xNetUserGetInfo(@xPWChMachine, @xPWChUser, 3, Pointer(xbuf));  //mories, changed level 2 to 3
  if xRes = 0 then begin
    with aList do begin
      Clear;
      // siehe API USER_INFO_3 Struktur
      // API USER_INFO_3-Struktur in TStringliste schreiben
      Add( WideCharToString(xBuf^[0].usri3_name));
      if (xBuf^[0].usri3_password <> NIL) then
          Add( WideCharToString(xBuf^[0].usri3_password))
      else
          Add('');
      Add(inttostr(xBuf^[0].usri3_password_age));
      Add(inttostr(xBuf^[0].usri3_priv));
      Add(WideCharToString(xBuf^[0].usri3_home_dir));
      Add(WideCharToString(xBuf^[0].usri3_comment));
      Add(inttostr(xBuf^[0].usri3_flags));
      Add(WideCharToString(xBuf^[0].usri3_script_path));
      Add(inttostr(xBuf^[0].usri3_auth_flags));
      Add(WideCharToString(xBuf^[0].usri3_full_name));
      Add(WideCharToString(xBuf^[0].usri3_usr_comment));
      Add(WideCharToString(xBuf^[0].usri3_parms));
      Add(WideCharToString(xBuf^[0].usri3_workstations));
      Add(inttostr(xBuf^[0].usri3_last_logon));
      Add(inttostr(xBuf^[0].usri3_last_logoff));
      Add(inttostr(xBuf^[0].usri3_acct_expires));
      Add(inttostr(xBuf^[0].usri3_max_storage));
      Add(inttostr(xBuf^[0].usri3_units_per_week));
      Add(inttostr(Byte(xBuf^[0].usri3_logon_hours)));
      Add(inttostr(xBuf^[0].usri3_bad_pw_count));
      Add(inttostr(xBuf^[0].usri3_num_logons));
      Add(WideCharToString(xBuf^[0].usri3_logon_server));
      Add(inttostr(xBuf^[0].usri3_country_code));
      Add(inttostr(xBuf^[0].usri3_code_page));
      Add(inttostr(xBuf^[0].usri3_User_id));
      Add(inttostr(xBuf^[0].usri3_primary_group_id));
      Add(WideCharToString(xBuf^[0].usri3_profile));
      Add(WideCharToString(xBuf^[0].usri3_home_dir_drive));
      Add(inttostr(xBuf^[0].usri3_password_expired));
    end
  end else
    aList.Add('No infos found');
  xNetApiBufferFree(xbuf);  // Speicher freigeben
end; // END GetUserAccountInfo

//******************************************************************************
// Ermittelt den Server-Namen in einer bestimmten Domaene
// Eingabe  : Domain und ServerTyp
// Ausgabe  : Servernamen, welche zum ausgewaehlten Servertyp gehoeren
// Bemerkung: Servertype siehe Konstanten oben (z.B.  SV_TYPE_ALL)
//******************************************************************************
// Autor   : Sdo
// Datum   : 24.09.1998
//******************************************************************************
{}
procedure GetServerName(aDomain: String; aServerTyp: Integer; var aList: TStringList);
var  xDllName :             String;
     xDll_HModule :         HModule;
     xbuf :                 PServer_Info_101_Arr;
     xRes :                 NET_API_STATUS;
     xPWCh :                array[0..255] of WideChar;
     xDomain :              array[0..50] of WideChar;
     xEntriesRead :         DWORD;
     xTotalEntries :        DWORD;
     xresume_handle :       DWORD;
     xServerName :          String;
     xi :                   integer;
     xNetApiBufferFree: TNetApiBufferFree;
     xNetServerEnum: TNetServerEnum;
begin
  xRes :=DWORD(-1);
  if not Assigned(aList) then
    Exit;

  // Betriebssystem ermitteln
  case GetOS of
    VER_PLATFORM_WIN32s	       : exit;  // Win 3.1
    VER_PLATFORM_WIN32_WINDOWS : xDllName := 'radmin32.dll'; // Win95
    VER_PLATFORM_WIN32_NT      : xDllName := 'netapi32.dll'; // WinNT
    else exit;
  end;

  xDll_HModule := LoadLibrary(PChar(xDllName));
  xNetServerEnum := GetProcAddress(xDll_HModule, PChar('NetServerEnum'));
  xNetApiBufferFree := GetProcAddress(xDll_HModule, 'NetApiBufferFree');

  xresume_handle := 0;
  xServerName    := '';

  StringToWideChar(xServerName, @xPWCh, SizeOf(xPWCh));
  StringToWideChar(aDomain, @xDomain, SizeOf(xDomain));

  if ((aDomain <> '') and (xServerName <>'')) then
      xRes :=  xNetServerEnum(@xPWCh,
                             101,
                             Pointer(xbuf),
                             8192,
                             xEntriesRead,
                             xTotalEntries,
                             aServerTyp,   // SV_TYPE_SQLSERVER
                             @xDomain,
                             xresume_handle)

  else
    if ((aDomain = '') and (xServerName <>'')) then
       xRes :=  xNetServerEnum(@xPWCh,
                             101,
                             Pointer(xbuf),
                             8192,
                             xEntriesRead,
                             xTotalEntries,
                             aServerTyp,   // SV_TYPE_SQLSERVER
                             NIL,
                             xresume_handle)
     else
       if (xServerName ='') then
       xRes :=  xNetServerEnum(NIL,
                             101,
                             Pointer(xbuf),
                             8192,
                             xEntriesRead,
                             xTotalEntries,
                             aServerTyp,   // SV_TYPE_SQLSERVER
                             NIL,
                             xresume_handle);



  if (xRes = 0) then
     for xi:=0 to xEntriesRead - 1 do
         aList.Add('\\' + WideCharToString(xBuf^[xi].sv101_name));
  xNetApiBufferFree(xbuf);  // Speicher freigeben
end; // END GetServerName

//******************************************************************************
// Ermittelt die Gruppennamen eines Users auf einem bestimmten Server
// Eingabe : Username, Servername und Funktionsname
// Ausgabe : Stringlist mit den zugewiesenen Gruppennamen eines Users
//******************************************************************************
// Autor   : Wss
// Datum   : 04.11.1999
//******************************************************************************
procedure GetUserGroups(aUsername, aSvrname: String; aLocalGroup: Boolean; var aList: TStringList);
var
//  xDllName :             String;
//  xDll_HModule :         HModule;
  xBuf :                 PGroup_Info_0_Arr;
  xRes :                 NET_API_STATUS;
  xPWChServerName,
  xPWChUserName :        array[0..255] of WideChar;
  xEntriesRead :         DWORD;
  xTotalEntries :        DWORD;
  xi :                   integer;
  xServerName :          String;
//  xNetApiBufferFree: TNetApiBufferFree;
//  xNetUserGetGroups: TNetUserGetGroups;
//  xNetUserGetLocalGroups: TNetUserGetLocalGroups;
begin
  if not Assigned(aList) then
    Exit;

  // aSvrname = '' if local groups required
  if aSvrname = '' then
    xServerName := ''
  else
    xServerName :='\\' + aSvrname;

  StringToWideChar(xServerName, @xPWChServerName, SizeOf(xPWChServerName));
  StringToWideChar(aUsername, @xPWChUserName, SizeOf(xPWChUserName));

  if aLocalGroup then begin
    xRes := NetUserGetLocalGroups(@xPWChServerName, @xPWChUserName, 0, 0,
                                   Pointer(xBuf), 32768, xEntriesRead, xTotalEntries );
  end else begin
    xRes := NetUserGetGroups(@xPWChServerName, @xPWChUserName, 0, Pointer(xBuf),
                              32768, xEntriesRead, xTotalEntries);
  end;

  if (xRes = 0) and Assigned(xBuf) then begin
    for xi := 0 to xEntriesRead - 1 do
      aList.Add(WideCharToString(xBuf^[xi].grpi0_name));
    // Free memory allocated by API call
    NetApiBufferFree(xBuf);
  end;
end; // GetUserGroups
//******************************************************************************
// Ermittelt die Gruppennamen eines Users auf einem bestimmten Server
// Eingabe : Username und Servername
// Ausgabe : Stringlist mit den zugewiesenen Gruppennamen eines Users
//******************************************************************************
// Autor   : Sdo
// Datum   : 05.10.1998
//******************************************************************************
procedure GetUserLocalGroupName(aUsername, aSvrname: String; var aList: TStringList);
begin
  GetUserGroups(aUsername, aSvrName, True, aList);
end; // GetUserLocalGroupName


//******************************************************************************
// Ermittelt die Gruppennamen eines Users auf einem bestimmten Server
// Eingabe : Username und Servername
// Ausgabe : Stringlist mit den zugewiesenen Gruppennamen eines Users
//******************************************************************************
// Autor   : Sdo
// Datum   : 25.09.1998
//******************************************************************************
procedure GetUserGroupName(aUsername, aSvrname: String; var aList: TStringList);
begin
  GetUserGroups(aUsername, aSvrName, False, aList);
end; // GetUserGroupName

//******************************************************************************
// Ermittelt die Domaene eines Users
// Eingabe : Username
// Ausgabe : Stringlist mit den Domaenen
//******************************************************************************
// Autor   : Sdo
// Datum   : 29.09.1998
//******************************************************************************
(*
function  GetAllDomains(aUserName: string): TStringList;
var
  xDomainName :             array[0..255] of char;
  xDomainNameSize :         DWORD;
  xn :                      DWORD;
  xpeUse :                  SID_NAME_USE;
  xsid :                    PSID;
  xDomainControl :          TStringList;
  xi :                      Integer;
  aMachineName :            String;
begin

  if aUserName = '' then exit;

  Result := TStringList.create;
  Result.clear;
  Result.Sorted:=TRUE;
  Result.Duplicates:= dupIgnore;  // dopplete Eintraege ingnorieren

  xDomainControl:= TStringList.create;
  xDomainControl.clear;

  xDomainNameSize := SizeOf(xDomainName);
  xn   := 0;
  xsid := Nil;

  // Nur Domainserver auslesen
  xDomainControl:=GetServerName('', cSV_TYPE_DOMAIN_CTRL or
                                    cSV_TYPE_DOMAIN_BAKCTRL or
                                    cSV_TYPE_DOMAIN_MASTER);


  for xi:= 0 to  xDomainControl.count-1 do
  begin
     aMachineName:=Copy(xDomainControl.Strings[xi],3,
                                            length(xDomainControl.Strings[xi]));
     // Domain und SID lesen
     if not LookupAccountName(GetPointer(aMachineName),
                              PChar(aUserName),
                              xsid,             // var SID
                              xn,
                              xDomainName,      // var Domain
                              xDomainNameSize,
                              xpeUse) then begin
       ReallocMem(xsid, xn);
       // 2. Versuch
       if not LookupAccountName(GetPointer(aMachineName),
                                PChar(aUserName),
                                xsid,
                                xn,
                                xDomainName,
                                xDomainNameSize,
                                xpeUse) then
         xDomainName := '';
     end;
     Result.add(xDomainName);
  end;
  ReallocMem(xSid, 0);
{
  for xi:= 0 to  xDomainControl.count-1 do
  begin
     aMachineName:=Copy(xDomainControl.Strings[xi],3,
                                            length(xDomainControl.Strings[xi]));
     // Domain und SID lesen
     LookupAccountName(
                       GetPointer(aMachineName),
                       PChar(aUserName),
                       xsid,             // var SID
                       xn,
                       xDomainName,      // var Domain
                       xDomainNameSize,
                       xpeUse);

     GetMem(xsid, xn);

     // 2. Versuch
     if not LookupAccountName(
                               GetPointer(aMachineName),
                               PChar(aUserName),
                               xsid,
                               xn,
                               xDomainName,
                               xDomainNameSize,
                               xpeUse) then xDomainName:='';
     Result.add(xDomainName);
  end;
{}
  xDomainControl.free;
end; // END GetAllDomains
(**)


// Private Functionen
// ******************


end.
