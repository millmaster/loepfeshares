unit BARCOPICTURELib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 26.05.2000 13:46:14 from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\MILLMA~1\Floor\BARCOP~1.DLL (1)
// IID\LCID: {A1198485-BA91-11D2-8F56-0060080C772F}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\StdOle2.Tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  BARCOPICTURELibMajorVersion = 1;
  BARCOPICTURELibMinorVersion = 0;

  LIBID_BARCOPICTURELib: TGUID = '{A1198485-BA91-11D2-8F56-0060080C772F}';

  IID_ICopyPicture: TGUID = '{A1198493-BA91-11D2-8F56-0060080C772F}';
  CLASS_CopyPicture: TGUID = '{A1198494-BA91-11D2-8F56-0060080C772F}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ICopyPicture = interface;
  ICopyPictureDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CopyPicture = ICopyPicture;


// *********************************************************************//
// Interface: ICopyPicture
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A1198493-BA91-11D2-8F56-0060080C772F}
// *********************************************************************//
  ICopyPicture = interface(IDispatch)
    ['{A1198493-BA91-11D2-8F56-0060080C772F}']
    function  Get_Picture: IPictureDisp; safecall;
    procedure Set_Picture(const ppPictDisp: IPictureDisp); safecall;
    property Picture: IPictureDisp read Get_Picture write Set_Picture;
  end;

// *********************************************************************//
// DispIntf:  ICopyPictureDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A1198493-BA91-11D2-8F56-0060080C772F}
// *********************************************************************//
  ICopyPictureDisp = dispinterface
    ['{A1198493-BA91-11D2-8F56-0060080C772F}']
    property Picture: IPictureDisp dispid 0;
  end;

// *********************************************************************//
// The Class CoCopyPicture provides a Create and CreateRemote method to          
// create instances of the default interface ICopyPicture exposed by              
// the CoClass CopyPicture. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCopyPicture = class
    class function Create: ICopyPicture;
    class function CreateRemote(const MachineName: string): ICopyPicture;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

class function CoCopyPicture.Create: ICopyPicture;
begin
  Result := CreateComObject(CLASS_CopyPicture) as ICopyPicture;
end;

class function CoCopyPicture.CreateRemote(const MachineName: string): ICopyPicture;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CopyPicture) as ICopyPicture;
end;

end.
