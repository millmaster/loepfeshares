(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: FloorActiveX.pas
| Projectpart...: MillMaster NT
| Subpart.......: Floor
| Process(es)...: -
| Description...: Template class for building controls for Floor with IReportCommon interface
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 19.11.1999  1.00  Wss | Initial release
| 30.05.2000  1.00  Wss | Retrieve of MachID was wrong in Set_MachineSelection()
| 18.07.2001  1.00  Wss | supporting filters for Leopfe controls (restricted to a few data items yet)
| 04.04.2002  1.00  Wss | In ConvertFilterValue Text in ' statt in " einfassen
| 26.10.2005  1.00  Wss | CreateLoepfeControl(): Gr�sse von fLoepfeControl per SetBounds() statt Properties
|=========================================================================================*)
unit FloorActiveX;

interface

uses
  Classes, ComObj, Controls, DEXSTD_TLB, FloorAxCtrls, FloorControlX,
  HostlinkDef, HostlinkShare;

const
  cSupportedFilterItemsCount = 4;
  cSupportedFilterItems: Array[1..cSupportedFilterItemsCount] of TDidItems = (
    diMachine_Name, diStyle_Name, diProd_Name, diYM_set_name
  );

type
  //---------------------------------------------------------------------------
  // used as data type in IExtReportItem interface
//  TFloorCatType = (fctUnassigned, fctText, fctNumeric, fctTime, fctNumericCalc, fctUnavailable);
  // filter compare methode used in Floor
  TCompareTest = (ctBelow, ctBelowOrEqual, ctAbove, ctAboveOrEqueal, ctEqualTo,
                  ctNotEqualTo, ctWithin, ctOutside);
  // record to store filter informations and converting
  PFilterInfo = ^TFilterInfo;
  TFilterInfo = record
    DataItem: TDidItems;
    DataItemName: string;
    CompareTest: TCompareTest;
    CompareValue: string;
  end;
  
  // list class containing the filter information read from Floor
  TFloorFilterList = class(TList)
  private
    fCondition: Integer;
    fHideMachines: Boolean;
    fValid: Boolean;
    function GetFilterItems(aIndex: Integer): PFilterInfo;
    procedure SetCondition(const Value: Integer);
    procedure SetHideMachines(const Value: Boolean);
  protected
    function GetFilterInfo(aDataItem: TDidItems): PFilterInfo;
  public
    procedure Clear; override;
    procedure ParseFilter(aIFilter: DEXSTD_TLB.IBarcoFilter);
    property Condition: Integer read fCondition write SetCondition;
    property HideMachines: Boolean read fHideMachines write SetHideMachines;
    property Items[aIndex: Integer]: PFilterInfo read GetFilterItems;
    property Valid: Boolean read fValid write fValid;
  end;
  
  TFloorActiveXControl = class(TActiveXControl, IBarcoReportCommon, IReportCommon)
  private
    fFilterList: TFloorFilterList;
    fFloorControl: TFloorControlX;
    function GetLoepfeControl: TCustomControl;
    function Get_AlarmAboveColor: LongWord; safecall;
    function Get_AlarmBelowColor: LongWord; safecall;
    function Get_Filter: DEXSTD_TLB.IFilters; safecall;
    function Get_Filter_: DEXSTD_TLB.IBarcoFilter; safecall;
    function Get_MachineSelection: DEXSTD_TLB.IMachineSelectionList; safecall;
    function Get_NormalColor: LongWord; safecall;
    function Get_ReportBanner: IReportBanner; safecall;
    function Get_ReportTable: IReportTable; safecall;
    function Get_Selection: SmallInt; safecall;
    function Get_StatusCategoryArray: IDispatch; safecall;
    function Get_WarningAboveColor: LongWord; safecall;
    function Get_WarningBelowColor: LongWord; safecall;
    procedure Set_AlarmAboveColor(pVal: LongWord); safecall;
    procedure Set_AlarmBelowColor(pVal: LongWord); safecall;
    procedure Set_Filter(const pVal: DEXSTD_TLB.IFilters); safecall;
    procedure Set_FilterArray(const Param1: IFilterArray); safecall;
    procedure Set_Filter_(const pVal: DEXSTD_TLB.IBarcoFilter); safecall;
    procedure Set_MachineSelection(const pVal: DEXSTD_TLB.IMachineSelectionList); safecall;
    procedure Set_NormalColor(pVal: LongWord); safecall;
    procedure Set_ReportBanner(const pVal: IReportBanner); safecall;
    procedure Set_ReportTable(const pVal: IReportTable); safecall;
    procedure Set_Selection(pVal: Smallint); safecall;
    procedure Set_StatusCategoryArray(const pVal: IDispatch); safecall;
    procedure Set_WarningAboveColor(pVal: LongWord); safecall;
    procedure Set_WarningBelowColor(pVal: LongWord); safecall;
    property FilterArray: IFilterArray write Set_FilterArray;
    property Filter_: DEXSTD_TLB.IBarcoFilter read Get_Filter_ write Set_Filter_;
    property ReportBanner: IReportBanner read Get_ReportBanner write Set_ReportBanner;
    property StatusCategoryArray: IDispatch read Get_StatusCategoryArray write Set_StatusCategoryArray;
  protected
    mMachConfig: THostlinkShare;
    procedure InitializeControl; override;
  public
    destructor Destroy; override;
    procedure CreateLoepfeControl(aInstanceClass: TComponentClass); virtual;
    procedure SetAlarmAboveColor(Value: LongWord); virtual;
    procedure SetAlarmBelowColor(Value: LongWord); virtual;
    procedure SetFilterSelection; virtual;
    procedure SetMachineSelection(aMachineSelection: String; aProdID: Integer); virtual;
    procedure SetNormalColor(Value: LongWord); virtual;
    procedure SetTimeSelection(aDateFrom, aDateTo: TDateTime; aDataSelection: TDataSelection); virtual;
    procedure SetWarningAboveColor(Value: LongWord); virtual;
    procedure SetWarningBelowColor(Value: LongWord); virtual;
    property FilterList: TFloorFilterList read fFilterList;
    property FloorControl: TFloorControlX read fFloorControl;
    property LoepfeControl: TCustomControl read GetLoepfeControl;
  end;
  
  TFloorActiveXControlFactory = class(TActiveXControlFactory)
  public
    procedure UpdateRegistry(Register: Boolean); override;
  end;
  
  TFloorActiveFormFactory = class(TActiveFormFactory)
  public
    procedure UpdateRegistry(Register: Boolean); override;
  end;
  
//------------------------------------------------------------------------------
// PROTOTYPE
//------------------------------------------------------------------------------
// Has to be called in initialization part in the created ActiveXControl
procedure InitializeFloorObjectFactory(aControlClass: TActiveXControlClass; const aClassID: TGUID);
procedure InitializeFloorFormFactory(aFormClass: TWinControlClass; const aClassID: TGUID);
//------------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  ActiveX, ComServ, Dialogs, mmRegistry, SysUtils, Windows,
  Repitmob_TLB, LoepfeGlobal;

const
  cRegBarcoAddons = '\Software\Barco\AddOns';

//:---------------------------------------------------------------------------
//:--- Class: TFloorFilterList
//:---------------------------------------------------------------------------
procedure TFloorFilterList.Clear;
begin
  while Count > 0 do begin
    Dispose(Items[0]);
    Delete(0);
  end;
  inherited Clear;
end;

//:---------------------------------------------------------------------------
function TFloorFilterList.GetFilterInfo(aDataItem: TDidItems): PFilterInfo;
var
  i: Integer;
begin
  Result := Nil;
  for i:=0 to Count-1 do begin
    if Items[i]^.DataItem = aDataItem then begin
      Result := Items[i];
      Break;
    end;
  end;
  // if not found in list create new one
  if not Assigned(Result) then begin
    new(Result);
    with Result^ do begin
      DataItem     := aDataItem;
      DataItemName := GetDIDName(aDataItem);
      CompareTest  := ctEqualTo; // currently the only supported test criteria
      CompareValue := '';
    end;
    Add(Result);
  end;
end;

//:---------------------------------------------------------------------------
function TFloorFilterList.GetFilterItems(aIndex: Integer): PFilterInfo;
begin
  Result := PFilterInfo(inherited Items[aIndex]);
end;

//:---------------------------------------------------------------------------
procedure TFloorFilterList.ParseFilter(aIFilter: DEXSTD_TLB.IBarcoFilter);
var
  xRPISetup: IReportItemSetup;
  xReportItem: IExtReportItem;
  xIFilter: DEXSTD_TLB.IBarcoFilterTest;
  xPos: Integer;
  
    //.......................................................................
  procedure DebugFilter(aBFilter: DEXSTD_TLB.IBarcoFilterTest);
  begin
    EnterMethod('DebugFilter: ' + aBFilter.Name);
  
    CodeSite.SendInteger('ID', aBFilter.ID);
    CodeSite.SendInteger('CompareTest', aBFilter.CompareTest);
    CodeSite.SendString('CompareValue', aBFilter.CompareValue);
    CodeSite.SendInteger('CompareID', aBFilter.CompareID);
    CodeSite.SendFloat('UpperThreshold', aBFilter.UpperThreshold);
    CodeSite.SendFloat('LowerThreshold', aBFilter.LowerThreshold);
    CodeSite.SendInteger('TimeType', Integer(aBFilter.TimeType));
  end;
    //.......................................................................
  function CheckSupportedFilterItem(aLongText: String; var aDataItem: TDidItems): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if GetDIDItemFromName(aLongText, aDataItem) then begin
      for i:=1 to cSupportedFilterItemsCount do begin
        Result := (aDataItem = cSupportedFilterItems[i]);
        if Result then
          Break;
      end;
    end;
  end;
    //.......................................................................
  function ConvertFilterValue(aOldValue, aNewValue: String): String;
  var
    xStr: String;
  begin
      // add comma to handle on a common way
    aNewValue := StringReplace(aNewValue, '*', '%', [rfReplaceAll]) + ',';
    Result    := aOldValue;
  
    while CutStringSep(',', aNewValue, xStr) do begin
      Result := Result + Format(',''%s''', [xStr]);
  //      Result := Result + Format(',"%s"', [xStr]);
    end;
  
      // delete leading comma
    if Pos(',', Result) = 1 then
      System.Delete(Result, 1, 1);
  end;
    //.......................................................................
  procedure AddFilter(aIFilterTest: DEXSTD_TLB.IBarcoFilterTest);
  var
    xFilter: PFilterInfo;
    xDidItem: TDidItems;
  begin
    DebugFilter(aIFilterTest);
  
    if TCompareTest(aIFilterTest.CompareTest) = ctEqualTo then begin
        // get original report item definition
      xRPISetup.FindReportItemID(aIFilterTest.ID, xReportItem);
  {
        CodeSite.SendMsg('Formula:      ' + xReportItem.Formula);
        CodeSite.SendMsg('Name:         ' + xReportItem.Name);
        CodeSite.SendMsg('Category:     ' + IntToStr(xReportItem.Category));
        CodeSite.SendMsg('Item:         ' + IntToStr(xReportItem.Item));
        CodeSite.SendMsg('SubItem:      ' + IntToStr(xReportItem.SubItem));
        CodeSite.SendMsg('CompareID:    ' + IntToStr(xReportItem.CompareID));
        CodeSite.SendMsg('Type:         ' + IntToStr(xReportItem.Type_));
        CodeSite.SendMsg('WarningAbove: ' + FloatToStr(xReportItem.WarningAbove));
        CodeSite.SendMsg('WarningBelow: ' + FloatToStr(xReportItem.WarningBelow));
        CodeSite.SendMsg('AlarmAbove:   ' + FloatToStr(xReportItem.AlarmAbove));
        CodeSite.SendMsg('AlarmBelow:   ' + FloatToStr(xReportItem.AlarmBelow));
  {}
  
      if CheckSupportedFilterItem(xReportItem.Formula, xDidItem) then begin
        xFilter := GetFilterInfo(xDidItem);
        with xFilter^ do begin
          CompareValue := ConvertFilterValue(CompareValue, aIFilterTest.CompareValue);
        end;
      end else
        fValid := False;
    end else
      fValid := False;
  end;
    //.......................................................................
  
begin
  xRPISetup   := CreateOLEObject('BARCO.REPORTITEMSETUP.1') as IReportItemSetup;
  xReportItem := CreateOLEObject('BARCO.REPORTITEM.1') as IExtReportItem;
  xReportItem.AttachSetup(xRPISetup);
  
  {
    CodeSite.SendString('Name', aIFilter.Name);
    CodeSite.SendInteger('ID', aIFilter.ID);
    CodeSite.SendInteger('AnyCondition', aIFilter.AnyCondition);
    CodeSite.SendInteger('HideMachines', aIFilter.HideMachines);
    CodeSite.AddSeparator;
  {
      property ExceptionMask: Smallint read Get_ExceptionMask write Set_ExceptionMask;
      property Format: IFormat read Get_Format write Set_Format;
      property Type_: CATTYPE read Get_Type_ write Set_Type_;
  {}
  fCondition    :=  aIFilter.AnyCondition;
  fHideMachines := (aIFilter.HideMachines = 1);
  // default True because if no filter is selected means valid filtering
  fValid := True;
  
  xPos := 0;
  try
    xIFilter := aIFilter.FirstFilterTest[xPos];
    if Assigned(xIFilter) then begin
      AddFilter(xIFilter);
      while (xPos <> -1) and fValid do begin
        xIFilter := aIFilter.NextFilterTest[xPos];
        AddFilter(xIFilter);
      end;
    end;
  
    if fValid then begin
      CodeSite.SendInteger('FilterCount', Count);
      for xPos:=0 to Count-1 do begin
        EnterMethod(Items[xPos]^.DataItemName);
        CodeSite.SendString('CompareValue', Items[xPos]^.CompareValue);
      end;
    end else
        // if a unsupported data item appears in filter definition clear complete list
      Clear;
  finally
    xReportItem := Nil;
    xRPISetup := Nil;
  end;
  
end;

//:---------------------------------------------------------------------------
procedure TFloorFilterList.SetCondition(const Value: Integer);
begin
  fCondition := Value;
end;

//:---------------------------------------------------------------------------
procedure TFloorFilterList.SetHideMachines(const Value: Boolean);
begin
  fHideMachines := Value;
end;

//:---------------------------------------------------------------------------
//:--- Class: TFloorActiveXControl
//:---------------------------------------------------------------------------
destructor TFloorActiveXControl.Destroy;
begin
  FreeAndNil(mMachConfig);
  FreeAndNil(fFilterList);
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.CreateLoepfeControl(aInstanceClass: TComponentClass);
var
  xInstance: TCustomControl;
begin
  xInstance := TCustomControl(aInstanceClass.NewInstance);
  try
      // create the Loepfe component with owner as fFloorControl
    xInstance.Create(fFloorControl);
    xInstance.Parent := fFloorControl;
    fFloorControl.LoepfeControl := xInstance;
    fFloorControl.SetBounds(fFloorControl.Left, fFloorControl.Top, 100, 100);
  except
    on e:Exception do begin
      CodeSite.SendError('TFloorActiveXControl.CreateLoepfeControl: ' + e.Message);
      TCustomControl(xInstance) := nil;
      raise;
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.GetLoepfeControl: TCustomControl;
begin
  Result := fFloorControl.LoepfeControl;
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_AlarmAboveColor: LongWord;
begin
  
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_AlarmBelowColor: LongWord;
begin
  
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_Filter: DEXSTD_TLB.IFilters;
begin
  
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_Filter_: DEXSTD_TLB.IBarcoFilter;
begin
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_MachineSelection: DEXSTD_TLB.IMachineSelectionList;
begin
  
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_NormalColor: LongWord;
begin
  
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_ReportBanner: IReportBanner;
begin
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_ReportTable: IReportTable;
begin
  
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_Selection: SmallInt;
begin
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_StatusCategoryArray: IDispatch;
begin
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_WarningAboveColor: LongWord;
begin
  
end;

//:---------------------------------------------------------------------------
function TFloorActiveXControl.Get_WarningBelowColor: LongWord;
begin
  
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.InitializeControl;
begin
  fFloorControl := Control as TFloorControlX;
  mMachConfig := THostlinkShare.Connect;
  fFilterList := TFloorFilterList.Create;
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.SetAlarmAboveColor(Value: LongWord);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.SetAlarmBelowColor(Value: LongWord);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.SetFilterSelection;
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.SetMachineSelection(aMachineSelection: String; aProdID: Integer);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.SetNormalColor(Value: LongWord);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.SetTimeSelection(aDateFrom, aDateTo: TDateTime; aDataSelection: TDataSelection);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.SetWarningAboveColor(Value: LongWord);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.SetWarningBelowColor(Value: LongWord);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_AlarmAboveColor(pVal: LongWord);
begin
  SetAlarmAboveColor(pVal);
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_AlarmBelowColor(pVal: LongWord);
begin
  SetAlarmBelowColor(pVal);
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_Filter(const pVal: DEXSTD_TLB.IFilters);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_FilterArray(const Param1: IFilterArray);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_Filter_(const pVal: DEXSTD_TLB.IBarcoFilter);
begin
  EnterMethod('SetFilter_');
  fFilterList.Clear;
  if Assigned(pVal) then begin
    fFilterList.ParseFilter(pVal);
  end;
  SetFilterSelection;
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_MachineSelection(const pVal: DEXSTD_TLB.IMachineSelectionList);
var
  xPos: Integer;
  xMachines: string;
  xMachSel: IBarcoMachineSelectionList;
  xProdID: Integer;
  xMach: Integer;
begin
  xMachines := '';
  xProdID   := 0;
    // get the first selected machine number
  if pVal.QueryInterface(IID_IBarcoMachineSelectionList, xMachSel) = S_OK then
  try
  {$IFDEF FloorDummy}
    xMachines := '1';
  {$ELSE}
    xPos := 0;
    try
      xProdID := xMachSel.Section.FirstSection[xPos];
    except
    end;
  
    xPos := 0;
    try
      xMach := xMachSel.FirstMachine[xPos]-1; // Floor 1-basiert, MM 0-basiert
      xMachines := IntToStr(mMachConfig.MachRec[xMach].MachID);
      if xProdID = 0 then begin
          // readout next selected machines and build a string
        while xPos <> -1 do
        try
          xMach := xMachSel.NextMachine[xPos]-1; // Floor 1-basiert, MM 0-basiert
          xMachines := Format('%s, %d', [xMachines, mMachConfig.MachRec[xMach].MachID]);
        except
        end;
      end else begin
          // a section was selected -> get ProdID from shared memory
        xProdID := mMachConfig.MachRec[xMach].ProdGrp[xProdID].ProdID;
      end;
    except
    end;
  {$ENDIF}
  finally
    xMachSel := Nil;
  end;
  SetMachineSelection(xMachines, xProdID);
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_NormalColor(pVal: LongWord);
begin
  SetNormalColor(pVal);
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_ReportBanner(const pVal: IReportBanner);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_ReportTable(const pVal: IReportTable);
begin
  
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_Selection(pVal: Smallint);
var
  xDataSelection: TDataSelection;
begin
  CodeSite.SendInteger('TFloorActiveXControl.Set_Selection', pVal);
  xDataSelection := TDataSelection(pVal);
  if not(xDataSelection in [dsCurShift..dsIntervalData]) then
    xDataSelection := dsCurShift;
  
  {$IFDEF FloorDummy}
    SetTimeSelection(Now-1, Now, xDataSelection);
  {$ELSE}
  with mMachConfig.ShiftSelection[xDataSelection]^ do
    SetTimeSelection(TimeFrom, TimeTo, xDataSelection);
  {$ENDIF}
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_StatusCategoryArray(const pVal: IDispatch);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_WarningAboveColor(pVal: LongWord);
begin
end;

//:---------------------------------------------------------------------------
procedure TFloorActiveXControl.Set_WarningBelowColor(pVal: LongWord);
begin
end;

//:---------------------------------------------------------------------------
//:--- Class: TFloorActiveXControlFactory
//:---------------------------------------------------------------------------
procedure TFloorActiveXControlFactory.UpdateRegistry(Register: Boolean);
var
  xKey: string;
begin
  inherited UpdateRegistry(Register);
  
  if (Win32PlatForm = VER_PLATFORM_WIN32_NT) then begin
    xKey := Format('%s\%s.%s', [cRegBarcoAddons, ComServer.ServerName, Self.ClassName]);
    with TmmRegistry.Create do
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if Register then begin
        if OpenKey(xKey, True) then
          WriteString('Name', Self.ClassName);
      end else
        DeleteKey(xKey);
      CloseKey;
    finally
      Free;
    end;
  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TFloorActiveFormFactory
//:---------------------------------------------------------------------------
procedure TFloorActiveFormFactory.UpdateRegistry(Register: Boolean);
var
  xKey: string;
begin
  inherited UpdateRegistry(Register);
  
  if (Win32PlatForm = VER_PLATFORM_WIN32_NT) then begin
    xKey := Format('%s\%s.%s', [cRegBarcoAddons, ComServer.ServerName, Self.ClassName]);
    with TmmRegistry.Create do
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if Register then begin
        if OpenKey(xKey, True) then
          WriteString('Name', Self.ClassName);
      end else
        DeleteKey(xKey);
      CloseKey;
    finally
      Free;
    end;
  end;
end;


//******************************************************************************
// InitializeControlFactory
//******************************************************************************
procedure InitializeFloorObjectFactory(aControlClass: TActiveXControlClass; const aClassID: TGUID);
begin
  ComServ.ComServer.SetServerName('LOEPFE');

  TFloorActiveXControlFactory.Create(
    ComServer,
    aControlClass,
    TFloorControlX,
    aClassID,
    1,
    '',
    0,
    tmApartment);
end;
//******************************************************************************
// InitializeFloorFormFactory
//******************************************************************************
procedure InitializeFloorFormFactory(aFormClass: TWinControlClass; const aClassID: TGUID);
begin
  ComServ.ComServer.SetServerName('LOEPFE');

  TFloorActiveFormFactory.Create(
    ComServer,
    TActiveFormControl,
    aFormClass,
    aClassID,
    1,
    '',
    OLEMISC_SIMPLEFRAME or OLEMISC_ACTSLIKELABEL,
    tmApartment);
end;
//------------------------------------------------------------------------------
end.

