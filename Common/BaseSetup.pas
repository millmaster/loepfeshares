{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: BaseSetup.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: BasisKlasse der unsichtbaren Komponenten TBaseSettings und
|                 TBaseSetupModul.
| Info..........: TBaseSettings:   Zugriff auf das Konst. Array ohne Komponenten
|                 TBaseSetupModul: Zugriff auf das Konst. Array mittels Komponenten
|                 Wichtig: Wenn im Test zwischen Client oder Server unterschieden
                           werden muss, so ist in der Funktion GetNTProduct der
                           Wert Result := SERVER_NT oder WORKSTATION_NT zu
                           setzen. -> suche nach ' // Test NTProduct '
|
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 21.07.1999  1.00  SDo | File created
| 23.02.2000  1.01  SDo | NT-Produkt suche in Func. GetNTProduct erneuert
| 27.02.2001        Wss | Init und GetValue nun nicht mehr Abstract, da automatisch
                          initialisiert wird, wenn 1. mal mit GetValue Wert geholt wird.
| 14.11.2001        Wss | Diverse Modifikationen fuer optimierte Verwedung des TMMSettingsReader
| 31.08.2004        Wss | Native TCriticalSection von Borland verwenden statt eigene
===============================================================================}
unit BaseSetup;

interface

uses Windows, Classes, Sysutils, BaseGlobal, syncobjs;


const
  cDefaultText = ' (Default)';

type

  TComponentType = (ctNone, ctEdit, ctSpinEdit, ctLabel, ctComboBox,
    ctCheckBox, ctMemo, ctRadioGroup);
  TClientServerType = (csServer, csClient, csClientServer, csNone, csServerEdit);
  //csServerEdit = nur auf Server editierbar, auf Client sichtbar

  TLocation = (lReg, lDB, lIni);

  // aus MMDef.pas
  TDataType = (dtNone, dtArray, dtChar, dtByte, dtShort, dtWord, dtLong, dtDWord,
    dtFloat, dtDouble, dtString, dtDate, dtTime, dtDateTime,
    dtOLEDateTime, dtKey, dtInteger, dtBoolean, dtSaveComboItemID);

  // dtSaveComboItemID gehoert nicht gerade in TDataType,
  // aber es muss leider sein.

  TBaseSettings = class(TObject)
  private
    FApplName: string;
    fInitialized: Boolean;
  protected
    fErrorTxt: string;
    function GetClientServer(aId: Word): Variant; virtual; abstract;
    function GetComponentType(aId: Word): Variant; virtual; abstract;
    function GetData(aId: Word): Variant; virtual; abstract;
    function GetDefaultValue(aId: Word): Variant; virtual; abstract;
    function GetDescription(aId: Word): Variant; virtual; abstract;
    function GetFactor(aId: Word): Variant; virtual; abstract;
    function GetGroup(aId: Word): Variant; virtual; abstract;
    function GetHintText(aId: Word): Variant; virtual; abstract;
    function GetLocation(aId: Word): Variant; virtual; abstract;
    function GetLocationKey(aId: Word): Variant; virtual; abstract;
    function GetLocationName(aId: Word): Variant; virtual; abstract;
    function GetMask(aId: Word): Variant; virtual; abstract;
    function GetMaxValue(aId: Word): Variant; virtual; abstract;
    function GetMinValue(aId: Word): Variant; virtual; abstract;
    function GetNativeValue(aId: Word): Variant; virtual; abstract;
    function GetNumberOfValues: Word; virtual; abstract;
    function GetReadOnlyData(aId: Word): Variant; virtual; abstract; // ?
    function GetRestart(aId: Word): Variant; virtual; abstract;
    function GetUniqueValueName(aId: Word): Variant; virtual; abstract;
    function GetUserValue(aId: Word): Variant; virtual; abstract;
    function GetValueChange(aId: Word): Variant; virtual; abstract;
    function GetValueName(aId: Word): Variant; virtual; abstract;
    function GetValueText(aId: Word): Variant; virtual; abstract;
    function GetValueType(aId: Word): Variant; virtual; abstract;
    function GetUserValueByName(aUniqueName: string): Variant; virtual; abstract;
    procedure SetData(aId: Word; const Value: Variant); virtual; abstract;
    procedure SetHintText(aId: Word; const Value: Variant); virtual; abstract;
    procedure SetNativeValue(aId: Word; const Value: Variant); virtual; abstract;
    procedure SetUserValue(aId: Word; const Value: Variant); virtual; abstract;
    procedure SetValueChange(aId: Word; const Value: Variant); virtual; abstract;
    procedure SetValueText(aId: Word; const Value: Variant); virtual; abstract;
    function ViewToUserView(aId: Word): Variant; virtual; abstract; // ?

    function GetDBValue(aId: Word): Variant; virtual; abstract;
    procedure SetDBValue(aId: Word; const Value: Variant); virtual; abstract;

    function GetPrintable(aId: Word): Boolean; virtual; abstract;
    procedure SetPrintable(aId: Word; const Value: Boolean); virtual; abstract;

    procedure SetGroup(aId: Word; const Value: Variant); virtual; abstract;

  public
    procedure MakeEntries; virtual; abstract;
    procedure ReadFiltredlRegValueNames(aComponentType: TComponentType); virtual; abstract;
    procedure ReadAllRegValueNames(aComponentType: TComponentType); virtual; abstract;
    procedure SaveAllRegValues(aChangedValue: Boolean); virtual; abstract;
    function GetID(aRegUniqueName: string): Integer; virtual; abstract;
    procedure ReadDBValues; virtual; abstract;
    procedure ReadRegistryValues; virtual; abstract;
//    procedure RefreshDatapool; virtual; abstract;

    property ClientServer[aId: Word]: Variant read GetClientServer;
    property ComponentType[aId: Word]: Variant read GetComponentType;
    property Data[aId: Word]: Variant read GetData write SetData;
    property DefaultValue[aId: Word]: Variant read GetDefaultValue;
    property Description[aId: Word]: Variant read GetDescription;
    property Factor[aId: Word]: Variant read GetFactor;
    property Group[aId: Word]: Variant read GetGroup write SetGroup;
    property HintText[aId: Word]: Variant read GetHintText write SetHintText;
    property Initialized: Boolean read fInitialized;
    property Location[aId: Word]: Variant read GetLocation;
    property LocationKey[aId: Word]: Variant read GetLocationKey;
    property LocationName[aId: Word]: Variant read GetLocationName;
    property Mask[aId: Word]: Variant read GetMask;
    property MaxValue[aId: Word]: Variant read GetMaxValue;
    property MinValue[aId: Word]: Variant read GetMinValue;
    property NativeValue[aId: Word]: Variant read GetNativeValue write SetNativeValue;
    property NumberOfValues: Word read GetNumberOfValues;
    property ReadOnlyData[aId: Word]: Variant read GetReadOnlyData;
    property Restart[aId: Word]: Variant read GetRestart;
    property UniqueValueName[aId: Word]: Variant read GetUniqueValueName;
    property UserValue[aId: Word]: Variant read GetUserValue write SetUserValue;
    property UserValueByName[aUniqueName: string]: Variant read GetUserValueByName;
    property ValueChange[aId: Word]: Variant read GetValueChange write SetValueChange;
    property ValueName[aId: Word]: Variant read GetValueName;
    property ValueText[aId: Word]: Variant read GetValueText write SetValueText;
    property ValueType[aId: Word]: Variant read GetValueType;
    property DBValue[aId: Word]: Variant read GetDBValue write SetDBValue;
    property ApplicationName: string read FApplName write FApplName;
    property ErrorTxt: string read fErrorTxt;
    property Printable[aId: Word]: Boolean read GetPrintable write SetPrintable;

    constructor Create; virtual;
    destructor Destroy; override;

    function Init: Boolean; virtual;

  published
  end;


  TBaseSetupModul = class(TComponent)
  private
    { Private declarations }
    FNTProduct: Word;
  protected
    { Protected declarations }
    fDBConnected: Boolean;
    function GetClientServer(aId: Word): Variant; virtual;
    function GetComponentType(aId: Word): Variant; virtual;
    function GetData(aId: Word): Variant; virtual;
    function GetDefaultValue(aId: Word): Variant; virtual;
    function GetDescription(aId: Word): Variant; virtual;
    function GetReadOnlyData(aId: Word): Variant; virtual;
    function GetFactor(aId: Word): Variant; virtual;
    function GetGroup(aId: Word): Variant; virtual;
    function GetHintText(aId: Word): Variant; virtual;
    function GetLocation(aId: Word): Variant; virtual;
    function GetLocationKey(aId: Word): Variant; virtual;
    function GetLocationName(aId: Word): Variant; virtual;
    function GetMask(aId: Word): Variant; virtual;
    function GetMaxValue(aId: Word): Variant; virtual;
    function GetMinValue(aId: Word): Variant; virtual;
    function GetNativeValue(aId: Word): Variant; virtual;
    function GetNumberOfValues: Word; virtual;
    function GetRestart(aId: Word): Variant; virtual;
    function GetUniqueValueName(aId: Word): Variant; virtual;
    function GetUserValue(aId: Word): Variant; virtual;
    function GetValueChange(aId: Word): Variant; virtual;
    function GetValueName(aId: Word): Variant; virtual;
    function GetValueText(aId: Word): Variant; virtual;
    function GetValueType(aId: Word): Variant; virtual;
    function GetPrintable(aId: Word): Boolean; virtual; abstract;

    //    function GetDBValue(aId: Word): Variant; virtual; abstract;
    //    procedure SetDBValue(aId: Word; const Value: Variant); virtual; abstract;

    procedure SetPrintable(aId: Word; const Value: Boolean); virtual; abstract;
    procedure SetData(aId: Word; const Value: Variant); virtual;
    procedure SetHintText(aId: Word; const Value: Variant); virtual;
    procedure SetNativeValue(aId: Word; const Value: Variant); virtual;
    procedure SetUserValue(aId: Word; const Value: Variant); virtual;
    procedure SetValueChange(aId: Word; const Value: Variant); virtual;
    procedure SetValueText(aId: Word; const Value: Variant); virtual;
    procedure SetGroup(aId: Word; const Value: Variant);
    procedure Loaded; override;
  public
    { Public declarations }
    mSettings: TBaseSettings;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function CheckRegExists(aRootKey: HKEY; aPath, aValueName: string): Boolean;
    function RunTime: Boolean;
    function Init: Boolean; virtual; abstract;

    procedure MakeEntries(aPCType: TClientServerType); virtual; abstract;
    procedure ReadFiltredlRegValueNames(aComponentType: TComponentType); virtual; abstract;
    procedure ReadAllRegValueNames(aComponentType: TComponentType); virtual; abstract;
    procedure SaveAllRegValues(aChangedValue: Boolean; aPCType: TClientServerType); virtual; abstract;

    property ClientServer[aId: Word]: Variant read GetClientServer;
    property ComponentType[aId: Word]: Variant read GetComponentType;
    property Data[aId: Word]: Variant read GetData write SetData;
    property DefaultValue[aId: Word]: Variant read GetDefaultValue;
    property Description[aId: Word]: Variant read GetDescription;
    property Factor[aId: Word]: Variant read GetFactor;
    property Group[aId: Word]: Variant read GetGroup write SetGroup;
    property HintText[aId: Word]: Variant read GetHintText write SetHintText;
    property Location[aId: Word]: Variant read GetLocation;
    property LocationKey[aId: Word]: Variant read GetLocationKey;
    property LocationName[aId: Word]: Variant read GetLocationName;
    property Mask[aId: Word]: Variant read GetMask;
    property MaxValue[aId: Word]: Variant read GetMaxValue;
    property MinValue[aId: Word]: Variant read GetMinValue;
    property NativeValue[aId: Word]: Variant read GetNativeValue write SetNativeValue;
    property NumberOfValues: Word read GetNumberOfValues;
    property ReadOnlyData[aId: Word]: Variant read GetReadOnlyData;
    property Restart[aId: Word]: Variant read GetRestart;
    property UniqueValueName[aId: Word]: Variant read GetUniqueValueName;
    property UserValue[aId: Word]: Variant read GetUserValue write SetUserValue;
    property ValueChange[aId: Word]: Variant read GetValueChange write SetValueChange;
    property ValueName[aId: Word]: Variant read GetValueName;
    property ValueText[aId: Word]: Variant read GetValueText write SetValueText;
    property ValueType[aId: Word]: Variant read GetValueType;
    property DBConnected: Boolean read fDBConnected;
    property NTProduct: Word read FNTProduct write FNTProduct default 1;
    property Printable[aId: Word]: Boolean read GetPrintable write SetPrintable;

  published
  end;

  TBaseSettingsReader = class(TObject)
  private
    mCriticalSection: TCriticalSection;
    fError: TErrorRec;
    fLastRequestedValue: String;
    function GetUserValue(aUniqueName: string): Variant;
  protected
    function GetValue(Name: string): Variant; virtual;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function Init: Boolean; virtual; abstract;
    property LastRequestedValue: String read fLastRequestedValue;
    property Value[aUniqueName: string]: Variant read GetUserValue;
    property Error: TErrorRec read fError;
  published
  end;
  //------------------------------------------------------------------------------
function GetNTProduct: Word; overload;
function GetNTProduct(aNTProduct: Word): string; overload;
function GetCompanyName: string;
function CheckKeyExists(aRootKey: HKEY; aKey: string): Boolean;
function DeleteRegValue(aPath: HKEY; aKey, aValue: string): Boolean;
//------------------------------------------------------------------------------

const

  // NT-Product
  WORKSTATION_NT = 1;
  SERVER_NT = 2;
  DOMAIN_CONTROLLER_NT = 3;
  ENTERPRISE_EDITION_NT = 4;


implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 mmRegistry, LoepfeGlobal, registry;


//------------------------------------------------------------------------------
function DeleteRegValue(aPath: HKEY; aKey, aValue: string): Boolean;
begin
  Result := False;
  with TmmRegistry.Create do try
    RootKey := aPath;
    if KeyExists(aKey) then
      Result := DeleteValue(aValue);
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------



//******************************************************************************
// Ermittlet das NT-Produkt als WORD
//******************************************************************************
function GetNTProduct: Word;
var
  xKey: string;
  xProduct: string;
  xReg: TRegistry;
  xVal: TStringList;
//  x: Integer;
begin

  xReg := TRegistry.Create;
  xVal := TStringList.Create;
  try

    with xReg do begin
      RootKey := cRegLM;
      OpenKeyReadOnly('SYSTEM\CurrentControlSet\Control\ProductOptions');
      xKey := 'SYSTEM\CurrentControlSet\Control\ProductOptions';
      {
      GetKeyNames(xVal);
      for x:=0 to xVal.Count -1 do begin
        xKey:=  'SYSTEM\' + xVal.Strings[x]  + '\Control\ProductOptions';
        if CheckKeyExists(cRegLM, xKey) then break;
      end;
      }
      xProduct := Trim(UpperCase(GetRegString(cRegLM, xKey, 'ProductType')));
    end;
  finally
    xReg.Free;
    xVal.Free;
  end;

  Result := 0;
  // NT-Workstation
  if xProduct = 'WINNT' then Result := WORKSTATION_NT else
    // NT-Server
    if xProduct = 'SERVERNT' then Result := SERVER_NT else
    // NT-Domain Controller
    if xProduct = 'LANMANNT' then Result := DOMAIN_CONTROLLER_NT else
    // NT-Enterprise Edition
    if xProduct = 'PRODUCTSUITE' then Result := ENTERPRISE_EDITION_NT;

  if xProduct = '' then Result := WORKSTATION_NT;

  //Result := SERVER_NT; // Test NTProduct

end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittlet das NT-Produkt Text
//******************************************************************************
function GetNTProduct(aNTProduct: Word): string;
begin
  case aNTProduct of
    WORKSTATION_NT: Result := 'Workstation'; // ivlm
    SERVER_NT: Result := 'Server'; // ivlm
    DOMAIN_CONTROLLER_NT: Result := 'Domain Controller'; // ivlm
    ENTERPRISE_EDITION_NT: Result := 'Enterprise Edition'; // ivlm
  end;
end;
//------------------------------------------------------------------------------
function GetCompanyName: string;
var
  xKey: string;
begin
  // FirmenName ermitteln
  xKey := '\SOFTWARE\MICROSOFT\WINDOWS NT\CURRENTVERSION\';
  Result := GetRegString(cRegLM, xKey, 'RegisteredOrganization');
end;
//------------------------------------------------------------------------------
function CheckKeyExists(aRootKey: HKEY; aKey: string): Boolean;
begin
  //  Result:=FALSE;
  with TmmRegistry.Create do try
    RootKey := aRootKey;
    Access := KEY_READ;
    // Pfad existiert nicht
    Result := KeyExists(aKey);
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// TBaseSettings
//------------------------------------------------------------------------------
constructor TBaseSettings.Create;
begin
  inherited Create;
  fInitialized := False;
end;
//------------------------------------------------------------------------------
destructor TBaseSettings.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TBaseSettings.Init: Boolean;
begin
  fInitialized := True;
  Result := fInitialized;
end;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// TBaseSetupModulModul
//------------------------------------------------------------------------------
function TBaseSetupModul.CheckRegExists(aRootKey: HKEY;
  aPath, aValueName: string): Boolean;
var
  xResult: Boolean;
  xPath: string;
begin
  xResult := FALSE;
  with TmmRegistry.Create do try
    RootKey := aRootKey;
    // Pfad existiert nicht
    Access := KEY_CREATE_SUB_KEY; //KEY_ALL_ACCESS;
    //if not KeyExists(aPath) then CreateKey(aPath);

    if KeyExists(aPath) then begin
      xPath := aPath + '\' + aValueName;
      xResult := OpenKeyReadOnly(aPath);
      xResult := ValueExists(aValueName);
    end;
  finally
    Free;
  end;
  Result := xResult;
end;
//------------------------------------------------------------------------------
constructor TBaseSetupModul.Create(AOwner: TComponent);
begin
  inherited Create(aOwner);
  fDBConnected := False;
end;
//------------------------------------------------------------------------------
destructor TBaseSetupModul.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetClientServer(aId: Word): Variant;
begin
  //  Result:= ' ';
  //  if Owner.ComponentState <> [csDesigning] then
  Result := mSettings.ClientServer[aId];
  //if mSettings <> NIL then Result:= mSettings.ClientServer[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetComponentType(aId: Word): Variant;
begin
  Result := mSettings.ComponentType[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetData(aId: Word): Variant;
begin
  Result := mSettings.Data[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetDefaultValue(aId: Word): Variant;
begin
  Result := mSettings.DefaultValue[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetDescription(aId: Word): Variant;
begin
  Result := mSettings.Description[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetReadOnlyData(aId: Word): Variant;
begin
  Result := mSettings.ReadOnlyData[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetFactor(aId: Word): Variant;
begin
  Result := mSettings.Factor[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetGroup(aId: Word): Variant;
begin
  Result := mSettings.Group[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetHintText(aId: Word): Variant;
begin
  Result := mSettings.HintText[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetLocation(aId: Word): Variant;
begin
  Result := mSettings.Location[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetLocationKey(aId: Word): Variant;
begin
  Result := mSettings.LocationKey[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetLocationName(aId: Word): Variant;
begin
  Result := mSettings.LocationName[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetMask(aId: Word): Variant;
begin
  Result := mSettings.Mask[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetMaxValue(aId: Word): Variant;
begin
  Result := mSettings.MaxValue[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetMinValue(aId: Word): Variant;
begin
  Result := mSettings.MinValue[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetNativeValue(aId: Word): Variant;
begin
  Result := mSettings.NativeValue[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetRestart(aId: Word): Variant;
begin
  Result := mSettings.Restart[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetUniqueValueName(aId: Word): Variant;
begin
  Result := mSettings.UniqueValueName[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetUserValue(aId: Word): Variant;
begin
  Result := mSettings.UserValue[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetValueChange(aId: Word): Variant;
begin
  Result := mSettings.ValueChange[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetValueName(aId: Word): Variant;
begin
  Result := mSettings.ValueName[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetValueText(aId: Word): Variant;
begin
  Result := mSettings.ValueText[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetValueType(aId: Word): Variant;
begin
  Result := mSettings.ValueType[aId];
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.RunTime: Boolean;
begin
  Result := not (csDesigning in ComponentState);
end;
//------------------------------------------------------------------------------
procedure TBaseSetupModul.SetData(aId: Word; const Value: Variant);
begin
  mSettings.Data[aId] := Value;
end;
//------------------------------------------------------------------------------
procedure TBaseSetupModul.SetHintText(aId: Word; const Value: Variant);
begin
  mSettings.HintText[aId] := Value;
end;
//------------------------------------------------------------------------------
procedure TBaseSetupModul.SetUserValue(aId: Word; const Value: Variant);
begin
  mSettings.UserValue[aId] := Value;
end;
//------------------------------------------------------------------------------
procedure TBaseSetupModul.SetValueChange(aId: Word; const Value: Variant);
begin
  mSettings.ValueChange[aId] := Value;
end;
//------------------------------------------------------------------------------
procedure TBaseSetupModul.SetValueText(aId: Word; const Value: Variant);
begin
  mSettings.ValueText[aId] := Value;
end;
//------------------------------------------------------------------------------
procedure TBaseSetupModul.SetNativeValue(aId: Word; const Value: Variant);
begin
  mSettings.NativeValue[aId] := Value;
end;
//------------------------------------------------------------------------------
function TBaseSetupModul.GetNumberOfValues: Word;
begin
  Result := mSettings.GetNumberOfValues;
end;
//------------------------------------------------------------------------------
procedure TBaseSetupModul.Loaded;
begin
  inherited;
  NTProduct := GetNTProduct;
end;
//------------------------------------------------------------------------------
procedure TBaseSetupModul.SetGroup(aId: Word; const Value: Variant);
begin
  mSettings.Group[aId] := Value;
end;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// TBaseSettingsReader
//------------------------------------------------------------------------------
constructor TBaseSettingsReader.Create;
begin
  inherited Create;
  mCriticalSection := TCriticalSection.Create;
  fLastRequestedValue := '';
end;
//------------------------------------------------------------------------------
destructor TBaseSettingsReader.Destroy;
begin
  mCriticalSection.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TBaseSettingsReader.GetValue(Name: string): Variant;
begin
  Result := 0; // dummy return value to prevent compiler hint
  fLastRequestedValue := Name;
  Init;
end;
//------------------------------------------------------------------------------
function TBaseSettingsReader.GetUserValue(aUniqueName: string): Variant;
begin
  Result := '';
  mCriticalSection.Enter;
  try
    Result := GetValue(aUniqueName);
  finally
    mCriticalSection.Leave;
  end;
//  Result := '';
//  mCriticalSection.EnterCriticSection;
//  try
//    Result := GetValue(aUniqueName);
//  except
//    mCriticalSection.LeaveCriticSection;
//    raise;
//  end;
//  mCriticalSection.LeaveCriticSection;
end;
//------------------------------------------------------------------------------
end.

