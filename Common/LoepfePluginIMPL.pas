(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: TLoepfePluginIMPL.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Implements the ILoepfePlugin interface and its methodes.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
  EXAMPLE of starting implementation
  **********************************
  unit Appl1ServerIMPL;

  interface

  uses
    ActiveX, ComObj, LoepfePluginIMPL, LoepfeLibrary_TLB, TestAppl1_TLB;

  type
    TAppl1Server = class(TLoepfePlugin, IAppl1Server)
    protected
    public
      function ExecuteCommand: SYSINT; override;
      procedure Initialize; override;
    end;

  implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

  type
    TCommandID = (ciAnyCommand);

  const
    cFloorMenuDef: Array[0..0] of TFloorMenuRec = (
      // --- Main Menu File ---
      (CommandID: Ord(ciAnyCommand); SubMenu: 0; MenuBmpName: 'ResourceName';
       MenuText: 'Menu Text'; MenuHint: 'Hint Text'; MenuStBar: 'Statusbar Text';
       ToolbarText: 'Toolbar Text';
       MenuGroup: mgMain; MenuTyp: mtFile; MenuState: msEnabled; MenuAvailable: maAll)
    );

   function TAppl1Server.ExecuteCommand: SYSINT;
   begin
     // Implementation for each CommandID
     case TCommandID(CommandIndex) of
       ciAnyCommand:;
     else
     end;
   end;

   procedure TAppl1Server.Initialize;
   begin
     inherited Initialize;
     InitMenu(cFloorMenuDef);
   end;


  initialization
    InitializeObjectFactory(TAppl1Server, Class_Appl1Server);
  end.
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.02.1999  1.00  Wss | TLoepfePlugin class completed and ready for use
| 25.03.1999  1.01  Wss | upload of menu values changed, upload toolbar bitmap implemented
| 14.09.2000  1.02  Wss | InitializeObjectFactory: aShowMainForm parameter included: default = False
| 24.10.2001  1.03  Wss | In PrepareChildForm set WrapperForm property with data form for closing
| 11.01.2002  1.04  Wss | Method Close improved in case user closes Floor with many open OLEForms
| 02.10.2002        LOK | Umbau ADO
| 22.07.2003  1.04  Wss | InitializeObjectFactory Parameter aUseInFloor hinzugef�gt
| 16.09.2003        LOK | InitializeObjectFactory: TLoepfeAutoObjectFactory.Create(..., tmApartment) von
|                       |   tmFree auf tmAppartment umgestellt (AccessViolation in VirtualTreeView beim
|                       |   initialisieren der COM Umgebung).
| 16.10.2003        Wss | Fehler �ber ein schon existierendes Wrapper Fenster umgehen, nun
|                       |  wird jedem neuen Wrapper Fenster der Name unique gemacht mit GetTickCount
| 15.06.2005        Lok | in TLoepfeAutoObjectFactory.UpdateRegistry() alle Exceptions unterdr�ckt (Kann Probleme
|                       |   geben f�r Benuzer mit eingeschr�nkten Rechten).
|=========================================================================================*)
unit LoepfePluginIMPL;

interface

uses
  ActiveX, ComObj, Forms, SysUtils, Windows, LoepfeLibrary_TLB,
  Graphics, BaseApplMain, BaseForm, WrapperForm;

type
  //---------------------------------------------------------------------------
  PFloorMenuRec = ^TFloorMenuRec;
  //---------------------------------------------------------------------------
  TLoepfeAutoObjectFactory = class(TAutoObjectfactory)
  public
    procedure UpdateRegistry(Register: Boolean); override;
  end;
  //---------------------------------------------------------------------------
  TLoepfePlugin = class(TAutoObject, ILoepfePlugin)
  private
    fCommandIndex: Integer;
    fMainForm: TForm;
    fProdID: Integer;
    fSelectedMachines: String;
    fSpdFirst: Integer;
    fSpdLast:  Integer;
    mMenu: Array of TFloorMenuRec;
    mMenuCount: Integer;
    procedure Close; safecall;
    procedure ChildFormIsClosing; safecall;
    function  Execute(aCommandIndex: Integer; const aMachines: WideString; aProdID: Integer;
                      aSpdFirst, aSpdLast: Integer; const aNotify: ILoepfePluginNotify): HResult; safecall;
    function GetToolbarBitmap: OleVariant; safecall;
    function MenuValues: OleVariant; safecall;
    procedure MergeBitmap(var aDestBmp: TBitmap; aSrcBmp: TBitmap);
    function GetValidBaseMain: Boolean;
    function GetBaseMain: TBaseApplMainForm;
  protected
    procedure InitMenu(aMenu: Array of TFloorMenuRec);
  public
    property CommandIndex: Integer read fCommandIndex;
    property BaseMain: TBaseApplMainForm read GetBaseMain;
    property MainForm: TForm read fMainForm;
    property ProdID: Integer read fProdID;
    property SelectedMachines: String read fSelectedMachines;
    property SpdFirst: Integer read fSpdFirst;
    property SpdLast: Integer read fSpdLast;
    property ValidBaseMain: Boolean read GetValidBaseMain;
    destructor Destroy; override;
    function ExecuteCommand: SYSINT; virtual; abstract;
    function GetCreateChildForm(aFormClass: TFormClass; var aForm: TForm; aDoCreate: Boolean = True): Boolean;
    function GetWrappedDataForm(aFormClass: TFormClass; var aDataForm: TForm; var aWrapForm: TmmForm): Boolean;
    procedure Initialize; override;
    procedure PrepareChildForm(aForm: TForm; aWrapperForm: TmmForm = Nil);
    function Translate(const aString: String): String;
  end;

//------------------------------------------------------------------------------
// PROTOTYPE
//------------------------------------------------------------------------------
// Has to be called in initialization part in the created OLE Automation Server unit
procedure InitializeObjectFactory(aAutoClass: TAutoClass; const aClassID: TGUID; aShowMainForm: Boolean = False; aUseInFloor: Boolean = True);
//------------------------------------------------------------------------------
implementation

uses
  mmCS, Messages,
  LoepfeGlobal, MMEventLog, Classes, ComServ, mmRegistry;

//******************************************************************************
// InitializeObjectFactory
//******************************************************************************
procedure InitializeObjectFactory(aAutoClass: TAutoClass; const aClassID: TGUID; aShowMainForm: Boolean; aUseInFloor: Boolean);
begin
  // if application is started as OLE Automation hide main form
  if ComServer.StartMode = smAutomation then begin
    Application.ShowMainForm := aShowMainForm;
//    ShowWindow(Application.Handle, Sw_Hide);
  end;

  // if the ThreadingModel is tmApartment a modal form prevents Floor of refreshing. It's kept blanc
  // -> tip from Shaun: use the tmFree instead
  ComServ.ComServer.SetServerName('LOEPFE');
  if aUseInFloor then
    TLoepfeAutoObjectFactory.Create(ComServer, aAutoClass, aClassID, ciMultiInstance, tmApartment)
//    TLoepfeAutoObjectFactory.Create(ComServer, aAutoClass, aClassID, ciMultiInstance, tmFree)
  else
    TAutoObjectFactory.Create(ComServer, aAutoClass, aClassID, ciMultiInstance, tmApartment);
end;

//******************************************************************************
// TLoepfeAutoObjectFactory
//******************************************************************************
procedure TLoepfeAutoObjectFactory.UpdateRegistry(Register: Boolean);
const
  cRegLoepfeAddons = '\Software\Loepfe\MillMaster\Plugin\AddOns';
var
  xKey: String;
begin
  try
//    CodeSite.SendWarning('UpdateRegistry disabled!!');
    inherited UpdateRegistry(Register);
  except
    on e:Exception do begin
      CodeSite.SendError('inherited UpdateRegistry: ' + e.Message);
//      Exit;
    end;
  end;

  if (Win32PlatForm = VER_PLATFORM_WIN32_NT) then 
  try
    xKey := Format('%s\%s.%s', [cRegLoepfeAddons, ComServer.ServerName, Self.ClassName]);
    with TmmRegistry.Create do
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if Register then OpenKey(xKey, True)
      else             DeleteKey(xKey);
      CloseKey;
    finally
      Free;
    end;
  except
    on e:Exception do
      CodeSite.SendError('Loepfe UpdateRegistry: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
//******************************************************************************
// TLoepfePlugin
//******************************************************************************
procedure TLoepfePlugin.ChildFormIsClosing;
begin
  ObjRelease;
end;
//------------------------------------------------------------------------------
procedure TLoepfePlugin.Close;
  procedure LookingForAForm(aOwner: TComponent);
  var
    i: Integer;
    xComp: TComponent;
  begin
    with aOwner do begin
      i := 0;
      while i < ComponentCount do begin
        xComp := Components[i];
        if (xComp is TfrmWrapper) then begin
          with xComp as TfrmWrapper do
          try
            // remove circular reference to this wrapper form
            WrapperForm.WrapperForm := Nil;
            WrapperForm.Close;
            WrapperForm := Nil;
            Free;
          except
            on e:Exception do begin
              CodeSite.SendError('LookingForAForm: ' + e.message);
              inc(i);
            end;
          end;
        end else
          inc(i);
      end;
    end;
  end;
  //----------------------------------------------------------------------------
begin
  LookingForAForm(Application);
  LookingForAForm(fMainForm);
end;
//------------------------------------------------------------------------------
destructor TLoepfePlugin.Destroy;
begin
  Finalize(mMenu);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TLoepfePlugin.Execute(aCommandIndex: Integer; const aMachines: WideString; aProdID: Integer;
                               aSpdFirst, aSpdLast: Integer; const aNotify: ILoepfePluginNotify): HResult; safecall;
begin
  fCommandIndex     := mMenu[aCommandIndex].CommandID;
  fProdID           := aProdID;
  fSelectedMachines := aMachines;
  fSpdFirst         := aSpdFirst;
  fSpdLast          := aSpdLast;
  Result            := ExecuteCommand;
end;
//------------------------------------------------------------------------------
{}
function TLoepfePlugin.GetBaseMain: TBaseApplMainForm;
begin
  if ValidBaseMain then
    Result := (fMainForm as TBaseApplMainForm)
  else
    Result := Nil;
end;
//------------------------------------------------------------------------------
function TLoepfePlugin.GetCreateChildForm(aFormClass: TFormClass; var aForm: TForm; aDoCreate: Boolean): Boolean;
begin
  Result := BaseForm.GetCreateChildForm(aFormClass, aForm, Self, aDoCreate);
  if Result then
    PrepareChildForm(aForm);
end;
//------------------------------------------------------------------------------
function TLoepfePlugin.GetWrappedDataForm(aFormClass: TFormClass; var aDataForm: TForm; var aWrapForm: TmmForm): Boolean;
begin
  Result := False;
  Application.CreateForm(TfrmWrapper, aWrapForm);
  // make the form invisible for the user
  with aWrapForm do begin
    Name := 'frmWrapper' + IntToStr(GetTickCount);
    Left := 0 - Width - 2;
    Top  := 0 - Height - 2;
    Visible := True;
  end;
  PrepareChildForm(aWrapForm);

  // now create specified data form
  if aFormClass <> Nil then begin
    if BaseForm.GetCreateChildForm(aFormClass, aDataForm, aWrapForm, True) then begin
      // save data form in property of wrapper form too if Floor is closing first, otherwise app hangs
      aWrapForm.WrapperForm := TmmForm(aDataForm);
      TmmForm(aDataForm).WrapperForm := aWrapForm;
      Result := True;
    end;
  end else
    Result := True;
end;
//------------------------------------------------------------------------------
function TLoepfePlugin.GetToolbarBitmap: OleVariant;
var
  xBmp: TBitmap;
  xDestBmp: TBitmap;
  xMemStream: TMemoryStream;
  xPointer: Pointer;
  i: Integer;
  xString: String;
  xBmpName: String;
begin
  xString := 'LoepfePlugin: GetToolbarBitmap failed.';
  xBmp     := TBitmap.Create;
  xDestBmp := TBitmap.Create;
  xDestBmp.Height := cFloorToolbarBmpHeight;
  try
    for i:=0 to mMenuCount-1 do begin
      if mMenu[i].MenuBmpName <> '' then
      try
        xBmpName := Uppercase(mMenu[i].MenuBmpName);
        xBmp.LoadFromResourceName(HInstance, xBmpName);
        MergeBitmap(xDestBmp, xBmp);
      except
        xString := 'LoepfePlugin: Bitmap resource name not found.';
        Exit;
      end;
    end;

    xMemStream := TMemoryStream.Create;
    try
      // writes bitmap to memory stream ...
      xDestBmp.SaveToStream(xMemStream);
      xMemStream.Position := 0;

      // ... and copy to the variant array
      Result := VarArrayCreate([0, xMemStream.Size-1], varByte);
      xPointer := VarArrayLock(Result);

      xMemStream.ReadBuffer(xPointer^, xMemStream.Size);
      VarArrayUnlock(Result);
      xMemStream.Free;
      // til here we don't have an error
      xString := '';
    except
      xString := 'LoepfePlugin: Preparing bitmap into OleVariant failed.';
    end;
  finally
    xBmp.Free;
    xDestBmp.Free;
    // even Exit is called these lines are executed
    if (xString <> '') and ValidBaseMain then begin
      BaseMain.WriteLog(etWarning, xString);
      // perform a exception to notify the FloorPlugin
      raise EAbort.Create(xString);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TLoepfePlugin.GetValidBaseMain: Boolean;
begin
  Result := MainForm is TBaseApplMainForm;
end;
//------------------------------------------------------------------------------
procedure TLoepfePlugin.Initialize;
begin
  inherited Initialize;

  fCommandIndex     := -1;
  fMainForm         := Application.MainForm;
  fProdID           := 0;
  fSelectedMachines := '';
end;
//------------------------------------------------------------------------------
procedure TLoepfePlugin.InitMenu(aMenu: Array of TFloorMenuRec);
var
  i: Integer;
begin
  mMenuCount := Length(aMenu);

  // copy the menu items to member variable
  SetLength(mMenu, mMenuCount);
  for i:=0 to mMenuCount-1 do begin
    mMenu[i] := aMenu[i];
    if (mMenu[i].SubMenu <> 0) then
      mMenu[i].MenuBmpName := '';
  end;
end;
//------------------------------------------------------------------------------
function TLoepfePlugin.MenuValues: OleVariant;
var
  i: Integer;
begin
  Result := VarArrayCreate([0, mMenuCount-1, mvMenuGroup, mvMenuAvailable], varVariant);
  for i:=0 to mMenuCount-1 do begin
    Result[i, mvMenuGroup]   := mMenu[i].MenuGroup;
    Result[i, mvMenuTyp]     := mMenu[i].MenuTyp;
    Result[i, mvMenuState]   := mMenu[i].MenuState;
    Result[i, mvSubMenus]    := mMenu[i].SubMenu;
    Result[i, mvMenuText]    := Translate(mMenu[i].MenuText);
    Result[i, mvMenuHint]    := Translate(mMenu[i].MenuHint);
    Result[i, mvMenuStBar]   := Translate(mMenu[i].MenuStBar);
    Result[i, mvToolbarText] := mMenu[i].ToolbarText;
    Result[i, mvMenuBmpName] := mMenu[i].MenuBmpName;
    Result[i, mvMenuAvailable] := mMenu[i].MenuAvailable;
  end;
end;
//------------------------------------------------------------------------------
procedure TLoepfePlugin.MergeBitmap(var aDestBmp: TBitmap; aSrcBmp: TBitmap);
var
  xWidth: Integer;
begin
  xWidth := aDestBmp.Width;
  aDestBmp.Width := aDestBmp.Width + aSrcBmp.Width;
  aDestBmp.Canvas.Draw(xWidth, 0, aSrcBmp);
end;
//------------------------------------------------------------------------------
procedure TLoepfePlugin.PrepareChildForm(aForm: TForm; aWrapperForm: TmmForm);
begin
  if aForm is TmmForm then begin
    // if this is a new Form then set the ILoepfePlugin property
    if not TmmForm(aForm).OLEMode then
      TmmForm(aForm).LoepfePlugin := Self;
    if Assigned(aWrapperForm) then begin
      // if data form is closing first it has to close the wrapper form too
      TmmForm(aForm).WrapperForm := aWrapperForm;
      // if Floor is closing first the wrapper form has to close data form too
      aWrapperForm.WrapperForm := TmmForm(aForm);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TLoepfePlugin.Translate(const aString: String): String;
begin
  Result := aString;
  try
    if ValidBaseMain then
      Result := BaseMain.Dictionary.Translate(aString);
  except
  end;
end;
//------------------------------------------------------------------------------

end.

