(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: Mailslot.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Kapselt die Funktionen einer Mailslot in 2 Klassen
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.06.1998 0.00  Wss | Datei erstellt
| 10.06.1998 1.00  Wss | Klassen TBaseMailslot, TMailslotReader, TMailslotWriter erstellt
| 11.06.1998 1.01  Wss | Init-Methode initialisiert nun die Mailslots, nicht mehr im Create
| 24.01.2000 1.02  Wss | Write delayed with 100ms -> see Methode Write()
| 13.03.2000 1.03  Mg  | TBroadcaster inserted
| 30.04.2002 1.04  LOK | Changed deklaration of TBroadcaster.Init
|            VCS       |   From :
|            0.2       |     function TBroadcaster.Init:boolean;
|                      |    To :
|                      |     function TBroadcaster.Init(aMaxTrial:integer; aMsSleep:integer):boolean;
|                      | Changed the implementation to initialize several times the Mailslot
|                      | if the function TMailslot.Init failed.
| 03.05.2002 1.04  LOK | Extended Error Message if write or init to Mailslot failed
|            VCS       |
|            0.3       |
| 11.07.2003 1.02  Wss | In Write wird Fehler ERROR_BAD_NETPATH ignoriert (wss/lok)
| 04.09.2003 1.02  LOK | TBroadcaster erweitert.
|                      |   Wenn beim Schreiben ein Mailslot nicht ansprechbar ist, dann wird
|                      |   das Mailslotobjekt freigegeben. Beim n�chsten Mal, wenn geschrieben werden soll
|                      |   wird �berpr�ft ob das Mailslot-Objekt existiert. Wenn nein wird es erzeugt
|                      |   und das ganze Spiel beginnt wieder von vorne.
| 11.09.2003       LOK | TBroadcaster.Write erweitert, dass der Mailslot vor jedem Schreiben neu
|                      |   neu erzeugt und nach dem Schreiben wieder freigegeben wird.
|                      |   Beseitigt die Fehlermeldung 38, wenn Windows LogOff-LogOn.
| 22.10.2003       lok | TBroadcaster.Write: Wenn die gefilterten FEhler auftraten, dann trotzdem True zur�ckgeben
| 26.04.2004   Lok/Wss | Broadcast wurde in TBroadcastWriterThread ausgelagert
|=========================================================================================*)
unit Mailslot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmStringList, mmThread;

const
  cMailslotTimeStampSize = 4;
  cRegBroadcastDisabled = 'BroadcastDisabled';
type

  rPMailslotHeader = ^rMailslotHeader;
  rMailslotHeader = record
    TimeStamp: DWord;
    Buffer:    array of byte;// old Pointer;
  end;


  // Base class of Mailslot
  TBaseMailslot = class(TObject)
  private
    { Private declarations }
    mMSName: String;
    fError: DWord;
    fHandle: THandle;
    function GetHandleValid: Boolean;
  protected
    { Protected declarations }
    procedure RaiseException(aError: DWord);

    property Handle: THandle read fHandle write fHandle;
  public
    { Public declarations }
    constructor Create; virtual;
    destructor Destroy; override;

    property HandleValid: Boolean read GetHandleValid;
    property Error: DWord read fError;
  end;

  // Creates a Mailslot and read from it
  TMailslotReader = class(TBaseMailslot)
  private
    mLastTimeStamp: DWord;
    fTimeout: DWord;
    procedure SetTimeout(aValue: DWord);
  protected
    function GetNextSize(var aSize: DWord): Boolean;
  public
    constructor Create(aMSName: String; aTimeOut: Integer); reintroduce;
    function Init: Boolean;
    function Read(aBuffer: PByte; aMaxSize: DWord; var aCount: DWord): Boolean;
    property Timeout: DWord read fTimeout write SetTimeout;
  end;

  // Creates a handle to a mailslot and write to it
  TMailslotWriter = class(TBaseMailslot)
  public
    constructor Create(aComputerName, aMSName: String); reintroduce;
    function Init: Boolean;
    function Write(aBuffer: PByte; aSize: DWord; var aCount: DWord): Boolean;
  end;

  TBroadcaster = class(TObject)
   private
    mDomainNames: string;
    mMailslotName: string;
    fError: DWord;
    fErrorInformation: string;
   public
    constructor Create(aDomainNames: string; aMailslotName: string); // Supported Domanes i.e : *,Domane1,Domane2,
     destructor  Destroy; override;

    function Init: Boolean;
    function Write(aMsg: PByte; aSizeOfMsg: Integer): Boolean;

    property Error: DWord read fError;
     // 3.5.02 LOK
    property ErrorInformation: string read fErrorInformation;
   end;

  TBroadcastWriterThread = class(TmmThread)
  private
    fDomainNames: String;
    fMailslotName: String;
    mMsg: PByte;
    mSizeOfMsg: integer;
    procedure SetDomainNames(const Value: string);
  public
    constructor Create(CreateSuspended: Boolean);
    destructor Destroy; override;

    procedure Execute; override;
    procedure SetMessage(aMsg: PByte; aSizeOfMsg: integer);

    property DomainNames: string write SetDomainNames;
    property MailslotName: string read fMailslotName write fMailslotName;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
  LoepfeGlobal, MMEventLog, mmCS, IdIcmpClient;

//*****************************************************************************
// TBaseMailslot
//*****************************************************************************
constructor TBaseMailslot.Create;
begin
  inherited Create;
  fError  := NO_ERROR;
  fHandle := INVALID_HANDLE_VALUE;
  mMSName := '';
end;
//-----------------------------------------------------------------------------
destructor TBaseMailslot.Destroy;
begin
  CloseHandle(Handle);
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TBaseMailslot.GetHandleValid: Boolean;
begin
  Result := (Handle <> INVALID_HANDLE_VALUE);
{
  if not Result then
    fError := GetLastError;
}
end;
//-----------------------------------------------------------------------------
procedure TBaseMailslot.RaiseException(aError: DWord);
begin
  fError := aError;
  raise EComponentError.Create(Format('%s: %s', [Self.ClassName, FormatErrorText(Error)]));
end;
//-----------------------------------------------------------------------------

//*****************************************************************************
// TMailslotReader
//*****************************************************************************

//-----------------------------------------------------------------------------
constructor TMailslotReader.Create(aMSName: String; aTimeOut: Integer);
begin
  inherited Create;
  // Init local variables
  mMSName := '\\.\mailslot\' + aMSName;
  mLastTimeStamp := 0;

  if aTimeOut = -1 then
    fTimeout := MAILSLOT_WAIT_FOREVER
  else
    fTimeout := aTimeOut;
end;
//-----------------------------------------------------------------------------
function TMailslotReader.GetNextSize(var aSize: DWord): Boolean;
var
  xMaxSize: DWord;
  xCount: DWord;
  xTimeout: DWord;
begin
  if GetMailslotInfo(Handle, @xMaxSize, aSize, @xCount, @xTimeout) then
    Result := (xCount > 0)
  else
    Result := False;
end;
//-----------------------------------------------------------------------------
function TMailslotReader.Init: Boolean;
begin
  if HandleValid then
    CloseHandle(Handle);

  // Create new Mailslot
  Handle := CreateMailslot(PChar(mMSName), 0, Timeout, Nil);

  Result := HandleValid;
  if not Result then
    fError := GetLastError;
{
  if not Result then
    RaiseException(GetLastError);
}
end;
//-----------------------------------------------------------------------------
function TMailslotReader.Read(aBuffer: PByte; aMaxSize: DWord; var aCount: DWord): Boolean;
var
  xBuf: rPMailslotHeader;
  xSize: DWord;
  xDummyBuffer: Char;

  function ReadMailslot: Boolean;
  begin
    // allocate enough memory and read next message
    GetMem(xBuf, xSize );
    Result := ReadFile(Handle, xBuf^, xSize, aCount, Nil);
    if Result then begin
      dec(aCount, cMailslotTimeStampSize);
      // if read succeeded copy buffer memory to receivers memory
      Move(xBuf.Buffer, aBuffer^, aCount);
      // was this message received before (note: messages are sent several times -> protocol depencies)
      Result := (xBuf.TimeStamp <> mLastTimeStamp);
      // keep timestamp from this message
      mLastTimeStamp := xBuf.TimeStamp;
      // if True but aCount = 0 >> there was not enough space in aBuffer
      Result := Result and (aCount > 0);
    end;
    // let the memory gone
    FreeMem(xBuf, xSize );
  end;

begin
  Result := False;
  if HandleValid then begin
    // !!! is there really a message ???
    if GetNextSize(xSize) then begin
      // YES
      Result := ReadMailslot;
    // there is no message ready to be read: hang in ReadFile just for timeout
    end else if ReadFile(Handle, xDummyBuffer, 0, aCount, Nil) then
      // if a message is received: get it right now
      Result := ReadMailslot;

    if Result then fError := NO_ERROR
    else           fError := GetLastError;
  end;
end;
//-----------------------------------------------------------------------------
procedure TMailslotReader.SetTimeout(aValue: DWord);
begin
  if aValue <> fTimeout then
    if HandleValid then begin
      if aValue = DWord(-1) then fTimeout := MAILSLOT_WAIT_FOREVER
      else                fTimeout := aValue;

      if SetMailslotInfo(Handle, Timeout) then
        fError := NO_ERROR
      else
        RaiseException(GetLastError);
    end else
      RaiseException(Error);
end;
//-----------------------------------------------------------------------------

//*****************************************************************************
// TMailslotWriter
//*****************************************************************************
constructor TMailslotWriter.Create(aComputerName, aMSName: String);
begin
  inherited Create;

  Handle := INVALID_HANDLE_VALUE;
  // initialize local variable
  mMSName := Format('\\%s\mailslot\%s', [aComputerName, aMSName]);
end;
//-----------------------------------------------------------------------------
function TMailslotWriter.Init: Boolean;
begin
  if HandleValid then
    CloseHandle(Handle);

  // Create handle to existing mailslot
  Handle := CreateFile( PChar ( mMSName ),            // name
                        GENERIC_WRITE,            // Access mode
                        FILE_SHARE_READ{ or FILE_SHARE_WRITE},          // Share mode
                        Nil,                      // Security pointer
                        OPEN_EXISTING,            // how to create
                        FILE_ATTRIBUTE_NORMAL,    // file attributes
                        0);                       // template file

  Result := HandleValid;
  if not Result then
    fError := GetLastError;
end;
//-----------------------------------------------------------------------------
function TMailslotWriter.Write(aBuffer: PByte; aSize: DWord; var aCount: DWord): Boolean;
var
  xBuf: rPMailslotHeader;
  xSize: DWord;
begin
  Result := False;
  if HandleValid then begin
    // buffer size is sender size + cMailslotTimeStampSize
    xSize := aSize + cMailslotTimeStampSize;
    // catch memory from heap;
    GetMem(xBuf, xSize);
    try
      // set timestamp of this message
      xBuf.TimeStamp := GetTickCount;
      // copy senders buffer to local buffer
      Move(aBuffer^, xBuf.Buffer, aSize);
      // write message one time (note: Windows send mailslot messages for each intalled protocol !!!!)
      Result := WriteFile(Handle, xBuf^, xSize, aCount, Nil);
      Result := Result and (xSize = aCount);
    finally
      // let the memory gone
      FreeMem(xBuf);
    end;
    // Wait 100 ms to preventing of fast speed connection:
    // TimeStamp can be the same value as previous message on
    Sleep(100);
    if Result then begin
      // remove timestamp size of sent bytes
      dec(aCount, cMailslotTimeStampSize);
      fError := NO_ERROR;
    end
    else begin
      fError := GetLastError;
    end;
  end;

//  Result := False;
//  if HandleValid then begin
//    // buffer size is sender size + cMailslotTimeStampSize
//    xSize := aSize + cMailslotTimeStampSize;
//    // catch memory from heap;
//    GetMem(xBuf, xSize);
//    // set timestamp of this message
//    xBuf.TimeStamp := GetTickCount;
//    // copy senders buffer to local buffer
//    Move(aBuffer^, xBuf.Buffer, aSize);
//    // write message one time (note: Windows send mailslot messages for each intalled protocol !!!!)
//    try
//      Result := WriteFile(Handle, xBuf^, xSize, aCount, Nil);
//      Result := Result and (xSize = aCount);
//    finally
//      // let the memory gone
//      FreeMem(xBuf, xSize);
//    end;
//    // Wait 100 ms to preventing of fast speed connection:
//    // TimeStamp can be the same value as previous message on
//    Sleep(100);
//    if Result then begin
//      // remove timestamp size of sent bytes
//      dec(aCount, cMailslotTimeStampSize);
//      fError := NO_ERROR;
//    end
//    else begin
//      fError := GetLastError;
//    end;
//  end;
end;
//-----------------------------------------------------------------------------
//--TBroadcaster----------------------------------------------------------------
//------------------------------------------------------------------------------
constructor TBroadcaster.Create(aDomainNames: string; aMailslotName: string);
begin
    inherited Create;
    mDomainNames := aDomainNames;
    mMailslotName := aMailslotName;
end;
//:-----------------------------------------------------------------------------
destructor TBroadcaster.Destroy;
begin
  inherited Destroy;
end;
//:-----------------------------------------------------------------------------
function TBroadcaster.Init: Boolean;
begin
  Result := True;
end;
//:-----------------------------------------------------------------------------
function TBroadcaster.Write(aMsg: PByte; aSizeOfMsg: Integer): Boolean;
begin
  Result := True;
  with TBroadcastWriterThread.Create(True) do
  try
    DomainNames  := mDomainNames;
    MailslotName := mMailslotName;
    SetMessage(aMsg, aSizeOfMsg);
    Resume;
  except
//    Free;
  end; // try except
end;
//:-----------------------------------------------------------------------------
// TBroadcastWriterThread
//:-----------------------------------------------------------------------------
constructor TBroadcastWriterThread.Create(CreateSuspended: Boolean);
begin
  inherited Create(CreateSuspended);
  FreeOnTerminate := True;
  mMsg            := Nil;
end;
//:-----------------------------------------------------------------------------
destructor TBroadcastWriterThread.Destroy;
begin
  if Assigned(mMsg) then
    FreeMem(mMsg);

  inherited Destroy;
end;
//:-----------------------------------------------------------------------------
procedure TBroadcastWriterThread.Execute;
var
  xPing: TIdIcmpClient;
  i: integer;
  xCount: DWord;
  xOK: Boolean;
  xList: TStringList;
  xStart: Cardinal;
begin
  // Broadcast ausschalten wenn gew�nscht (Fehler mit 2 Netzwerk Karten)
  if not(GetRegBoolean(cRegLM, cRegMMDebug, cRegBroadcastDisabled, False)) then begin
    xPing := TIdIcmpClient.Create(Nil);
    xPing.ReceiveTimeout := 1000;
    xList := TStringList.Create;
    try
      xList.CommaText := fDomainNames;
      for i:=0 to xList.Count-1 do begin
        xPing.Host := xList[i];
        try
          xPing.Ping;
          if xPing.ReplyStatus.ReplyStatusType = rsEcho then begin
            xStart := GetTickCount;
            with TMailslotWriter.Create(xList[i], fMailslotName) do
            try
              xOK := Init;
              if xOK then begin
                if not Write(mMsg, mSizeOfMsg, xCount) then begin
                  // ung�ltiger Pfad oder kein Netzwerk -> Fehler wird nicht protokolliert
                  xOK := (Error = ERROR_BAD_NETPATH) or (Error = ERROR_NETWORK_UNREACHABLE);
                  if xOK then
                    WriteToEventLogDebug(FormatErrorText(Error) + ' Destination: ' + xList[i]  + ' - Mailslot:' + fMailslotName + ' - Mailslot Handle: ' + IntToStr(Handle), 'TBroadcastWriterThread:', etError);
                end; // if not Write()
              end; // if Init
              if not xOK then
      {mem}          WriteToEventLog(FormatErrorText(Error) + ' Destination: ' + xList[i]  + ' - Mailslot:' + fMailslotName + ' - Mailslot Handle: ' + IntToStr(Handle), 'TBroadcastWriterThread:', etError);
            finally
              Free;
              CodeSite.SendInteger(xList[i] + ':' + IntToStr(ThreadID), GetTickCount - xStart);
            end; // with
          end else
            CodeSite.SendMsg(xList[i] + ':'  + IntToStr(ThreadID) + ' - timeout');
        except
          CodeSite.SendMsg(xList[i] + ':'  + IntToStr(ThreadID) + ' - nicht erreichbar');
        end;
      end; // for i
    finally
      xList.Free;
      xPing.Free;
    end;
  end;// if not(GetRegBoolean(cRegLM, cRegMMDebug, cRegBroadcastDisabled, False)) then begin
//  // Broadcast ausschalten wenn gew�nscht (Fehler mit 2 Netzwerk Karten)
//  if not(GetRegBoolean(cRegLM, cRegMMDebug, cRegBroadcastDisabled, False)) then begin
//    xList := TStringList.Create;
//    try
//      xList.CommaText := fDomainNames;
//      for i:=0 to xList.Count-1 do begin
//        xStart := GetTickCount;
//        with TMailslotWriter.Create(xList[i], fMailslotName) do
//        try
//          xOK := Init;
//          if xOK then begin
//            if not Write(mMsg, mSizeOfMsg, xCount) then begin
//              // ung�ltiger Pfad oder kein Netzwerk -> Fehler wird nicht protokolliert
//              xOK := (Error = ERROR_BAD_NETPATH) or (Error = ERROR_NETWORK_UNREACHABLE);
//              if xOK then
//                WriteToEventLogDebug(FormatErrorText(Error) + ' Destination: ' + xList[i]  + ' - Mailslot:' + fMailslotName + ' - Mailslot Handle: ' + IntToStr(Handle), 'TBroadcastWriterThread:', etError);
//            end; // if not Write()
//          end; // if Init
//          if not xOK then
//  {mem}          WriteToEventLog(FormatErrorText(Error) + ' Destination: ' + xList[i]  + ' - Mailslot:' + fMailslotName + ' - Mailslot Handle: ' + IntToStr(Handle), 'TBroadcastWriterThread:', etError);
//        finally
//          Free;
//          CodeSite.SendInteger(xList[i] + ':' + IntToStr(ThreadID), GetTickCount - xStart);
//        end; // with
//      end; // for i
//    finally
//      xList.Free;
//    end;
//  end;// if not(GetRegBoolean(cRegLM, cRegMMDebug, cRegBroadcastDisabled, False)) then begin
end;
//:-----------------------------------------------------------------------------
procedure TBroadcastWriterThread.SetDomainNames(const Value: string);
begin
  fDomainNames := Value;
end;
//:-----------------------------------------------------------------------------
procedure TBroadcastWriterThread.SetMessage(aMsg: PByte; aSizeOfMsg: integer);
begin
  if Assigned(mMsg) then
    FreeMem(mMsg);

  mSizeOfMsg := aSizeOfMsg;
  GetMem(mMsg, mSizeOfMsg);
  System.Move(aMsg^, mMsg^, mSizeOfMsg);
end;
//:-----------------------------------------------------------------------------

end.

