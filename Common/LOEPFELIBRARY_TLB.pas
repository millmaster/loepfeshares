unit LoepfeLibrary_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 17.04.2000 09:20:32 from Type Library described below.

// ************************************************************************ //
// Type Lib: \\Wetsrvbde\BDE_Dev\Delphi\Act\GUI\Floor\MillMasterPlugin\MillMasterPlugin.tlb (1)
// IID\LCID: {27E07800-AD7E-11D3-8656-00105A55E052}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 Repitmob, (C:\Program Files\BARCO\Floor\REPITMOB.TLB)
//   (2) v2.0 stdole, (C:\WINNT\System32\StdOle2.Tlb)
//   (3) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  Repitmob_TLB;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  LoepfeLibraryMajorVersion = 1;
  LoepfeLibraryMinorVersion = 0;

  LIBID_LoepfeLibrary: TGUID = '{27E07800-AD7E-11D3-8656-00105A55E052}';

  IID_IMillMasterPlugin: TGUID = '{27E07801-AD7E-11D3-8656-00105A55E052}';
  CLASS_MillMasterPlugin: TGUID = '{A301068D-B20A-11D3-865E-00105A55E052}';
  IID_ILoepfePlugin: TGUID = '{A301065E-B20A-11D3-865E-00105A55E052}';
  CLASS_LoepfePlugin: TGUID = '{A3010660-B20A-11D3-865E-00105A55E052}';
  IID_ILoepfePluginNotify: TGUID = '{A3010662-B20A-11D3-865E-00105A55E052}';
  CLASS_LoepfePluginNotify: TGUID = '{A3010664-B20A-11D3-865E-00105A55E052}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum TMenuValues
type
  TMenuValues = TOleEnum;
const
  mvMenuGroup = $00000000;
  mvMenuTyp = $00000001;
  mvMenuState = $00000002;
  mvSubMenus = $00000003;
  mvMenuText = $00000004;
  mvMenuHint = $00000005;
  mvMenuStBar = $00000006;
  mvToolbarText = $00000007;
  mvMenuBmpName = $00000008;
  mvMenuAvailable = $00000009;
  mvBitmap = $0000000A;

// Constants for enum TMenuGroup
type
  TMenuGroup = TOleEnum;
const
  mgMain = $00000000;
  mgFloor = $00000001;
  mgGroup = $00000002;
  mgMachine = $00000003;
  mgLoepfe = $00000004;

// Constants for enum TMenuTyp
type
  TMenuTyp = TOleEnum;
const
  mtFile = $00000000;
  mtEdit = $00000001;
  mtNone = $00000002;
  mtTrend = $00000003;
  mtHelp = $00000004;
  mtFloor = $00000005;
  mtGroup = $00000006;
  mtMachine = $00000007;
  mtLoepfe = $00000008;

// Constants for enum TMenuState
type
  TMenuState = TOleEnum;
const
  msDisabled = $00000000;
  msEnabled = $00000001;
  msHide = $00000002;

// Constants for enum TMenuAvailable
type
  TMenuAvailable = TOleEnum;
const
  maAll = $00000000;
  maMachineOnly = $00000001;
  maSectionOnly = $00000002;

// Constants for enum TLoepfeNotifyEvent
type
  TLoepfeNotifyEvent = TOleEnum;
const
  lnMenuChanged = $00000000;
  lnCommandComplete = $00000001;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IMillMasterPlugin = interface;
  IMillMasterPluginDisp = dispinterface;
  ILoepfePlugin = interface;
  ILoepfePluginDisp = dispinterface;
  ILoepfePluginNotify = interface;
  ILoepfePluginNotifyDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  MillMasterPlugin = IMillMasterPlugin;
  LoepfePlugin = ILoepfePlugin;
  LoepfePluginNotify = ILoepfePluginNotify;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  TFloorMenuRec = packed record
    CommandID: Integer;
    SubMenu: Integer;
    MenuBmpName: WideString;
    MenuText: WideString;
    MenuHint: WideString;
    MenuStBar: WideString;
    ToolbarText: WideString;
    MenuGroup: TMenuGroup;
    MenuTyp: TMenuTyp;
    MenuState: TMenuState;
    MenuAvailable: TMenuAvailable;
  end;


// *********************************************************************//
// Interface: IMillMasterPlugin
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {27E07801-AD7E-11D3-8656-00105A55E052}
// *********************************************************************//
  IMillMasterPlugin = interface(INotify)
    ['{27E07801-AD7E-11D3-8656-00105A55E052}']
  end;

// *********************************************************************//
// DispIntf:  IMillMasterPluginDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {27E07801-AD7E-11D3-8656-00105A55E052}
// *********************************************************************//
  IMillMasterPluginDisp = dispinterface
    ['{27E07801-AD7E-11D3-8656-00105A55E052}']
    procedure Change(const bstrUserName: WideString); dispid 1;
    procedure IsAwake; dispid 2;
    function  IsIDUsed(vnID: OleVariant; var bstrUsedIn: WideString): WordBool; dispid 3;
  end;

// *********************************************************************//
// Interface: ILoepfePlugin
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A301065E-B20A-11D3-865E-00105A55E052}
// *********************************************************************//
  ILoepfePlugin = interface(IDispatch)
    ['{A301065E-B20A-11D3-865E-00105A55E052}']
    function  Execute(aCommandIndex: Integer; const aMachines: WideString; aProdID: Integer; 
                      aSpdFirst: Integer; aSpdLast: Integer; const aNotify: ILoepfePluginNotify): HResult; safecall;
    procedure Close; safecall;
    function  MenuValues: OleVariant; safecall;
    function  GetToolbarBitmap: OleVariant; safecall;
    procedure ChildFormIsClosing; safecall;
  end;

// *********************************************************************//
// DispIntf:  ILoepfePluginDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A301065E-B20A-11D3-865E-00105A55E052}
// *********************************************************************//
  ILoepfePluginDisp = dispinterface
    ['{A301065E-B20A-11D3-865E-00105A55E052}']
    function  Execute(aCommandIndex: Integer; const aMachines: WideString; aProdID: Integer; 
                      aSpdFirst: Integer; aSpdLast: Integer; const aNotify: ILoepfePluginNotify): HResult; dispid 1;
    procedure Close; dispid 2;
    function  MenuValues: OleVariant; dispid 3;
    function  GetToolbarBitmap: OleVariant; dispid 4;
    procedure ChildFormIsClosing; dispid 5;
  end;

// *********************************************************************//
// Interface: ILoepfePluginNotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A3010662-B20A-11D3-865E-00105A55E052}
// *********************************************************************//
  ILoepfePluginNotify = interface(IDispatch)
    ['{A3010662-B20A-11D3-865E-00105A55E052}']
    procedure LoepfeNotify(aEvent: TLoepfeNotifyEvent; aParameter: OleVariant); safecall;
  end;

// *********************************************************************//
// DispIntf:  ILoepfePluginNotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A3010662-B20A-11D3-865E-00105A55E052}
// *********************************************************************//
  ILoepfePluginNotifyDisp = dispinterface
    ['{A3010662-B20A-11D3-865E-00105A55E052}']
    procedure LoepfeNotify(aEvent: TLoepfeNotifyEvent; aParameter: OleVariant); dispid 1;
  end;

// *********************************************************************//
// The Class CoMillMasterPlugin provides a Create and CreateRemote method to          
// create instances of the default interface IMillMasterPlugin exposed by              
// the CoClass MillMasterPlugin. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMillMasterPlugin = class
    class function Create: IMillMasterPlugin;
    class function CreateRemote(const MachineName: string): IMillMasterPlugin;
  end;

// *********************************************************************//
// The Class CoLoepfePlugin provides a Create and CreateRemote method to          
// create instances of the default interface ILoepfePlugin exposed by              
// the CoClass LoepfePlugin. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoLoepfePlugin = class
    class function Create: ILoepfePlugin;
    class function CreateRemote(const MachineName: string): ILoepfePlugin;
  end;

// *********************************************************************//
// The Class CoLoepfePluginNotify provides a Create and CreateRemote method to          
// create instances of the default interface ILoepfePluginNotify exposed by              
// the CoClass LoepfePluginNotify. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoLoepfePluginNotify = class
    class function Create: ILoepfePluginNotify;
    class function CreateRemote(const MachineName: string): ILoepfePluginNotify;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

class function CoMillMasterPlugin.Create: IMillMasterPlugin;
begin
  Result := CreateComObject(CLASS_MillMasterPlugin) as IMillMasterPlugin;
end;

class function CoMillMasterPlugin.CreateRemote(const MachineName: string): IMillMasterPlugin;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MillMasterPlugin) as IMillMasterPlugin;
end;

class function CoLoepfePlugin.Create: ILoepfePlugin;
begin
  Result := CreateComObject(CLASS_LoepfePlugin) as ILoepfePlugin;
end;

class function CoLoepfePlugin.CreateRemote(const MachineName: string): ILoepfePlugin;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_LoepfePlugin) as ILoepfePlugin;
end;

class function CoLoepfePluginNotify.Create: ILoepfePluginNotify;
begin
  Result := CreateComObject(CLASS_LoepfePluginNotify) as ILoepfePluginNotify;
end;

class function CoLoepfePluginNotify.CreateRemote(const MachineName: string): ILoepfePluginNotify;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_LoepfePluginNotify) as ILoepfePluginNotify;
end;

end.
