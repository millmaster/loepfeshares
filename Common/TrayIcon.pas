(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: TrayIcon.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Komponente fuer das Verwalten eines TrayIcons in der Trayablage
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------------------
| Wird im IDMessage property ein String angebeben, so wird ueberprueft, ob diese Applikation
| schon einmal vorhanden ist. Wenn ja, wird die eigene Applikation sofort wieder beendet.
| (Ohne Benachrichtigung)
|
| Wenn ShowApp = False ist, so muss im entsprechenden MainForm folgender MessageHandler
| hinzugefuegt werden:
|
|   Im Interface-Teil:
|   private
|     procedure WMSysCommand(var Message: TMessage); message WM_SYSCOMMAND;
|
|   Im Implementation-Teil:
|   procedure TForm1.WMSysCommand(var Message: TMessage);
|   begin
|     if not TrayIcon1.ShowApp then begin
|       if Message.WParam = SC_MINIMIZE then Self.Hide
|                                       else Inherited;
|     end else
|       Inherited;
|   end;
|
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.10.1098  1.00  Wss | Datei erstellt
|=========================================================================================*)
Unit TrayIcon;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls, ShellAPI, Forms,
  Menus;

const
  WM_TOOLTRAYICON = WM_USER+1;
  WM_RESETTOOLTIP = WM_USER+2;

type
  TTrayIcon = class(TComponent)
  private
  // BDS
  { internal use }
    hMapping: THandle;
  { Field Variables }
    IconData: TNOTIFYICONDATA;
    fIcon : TIcon;
    fToolTip : String;
    mWindowHandle : HWND;
    fActive : boolean;
    fShowApp : boolean;                                     // A. Meeder
    fSendMsg : string;
    fShowDesigning : Boolean;
  { Events }
    fOnClick     : TNotifyEvent;
    fOnDblClick  : TNotifyEvent;
    fOnRightClick : TMouseEvent;
    fPopupMenu   : TPopupMenu;
    function AddIcon : boolean;
    function ModifyIcon : boolean;
    function DeleteIcon : boolean;
    procedure DoRightClick( Sender : TObject ); 
    procedure FillDataStructure;
    procedure SetActive(Value : boolean);
    procedure SetPopupMenu(Value: TPopupMenu);
    procedure SetShowApp(Value : boolean);                  // A. Meeder
    procedure SetShowDesigning(Value : boolean);
    procedure SetIcon(Value : TIcon);
    procedure SetToolTip(Value : String);
    procedure WndProc(var msg : TMessage);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation ); override;
  public
    FMessageID: DWORD;
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;                             // A. Meeder
    procedure GoToPreviousInstance;
    procedure GetPopupMenu;
  published
    property Active : boolean read fActive write SetActive;
    property Icon : TIcon read fIcon write SetIcon;
    property IDMessage : string read fSendMsg write fSendMsg;
    property OnClick     : TNotifyEvent read FOnClick write FOnClick;
    property OnDblClick  : TNotifyEvent read FOnDblClick write FOnDblClick;
    property OnRightClick : TMouseEvent  read FOnRightClick write FonRightClick;
    property PopupMenu : TPopupMenu read fPopupMenu write SetPopupMenu;
    property ShowApp : boolean read fShowApp write SetShowApp; // A. Meeder
    property ShowDesigning : boolean read fShowDesigning write SetShowDesigning;
    property ToolTip : string read fTooltip write SetToolTip;
  end;

//procedure Register;

type
   PHWND = ^HWND;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//{$R TrayIcon.res}
//------------------------------------------------------------------------------
{
procedure Register;
begin
  RegisterComponents('LOEPFE', [TTrayIcon]);
end;
{}
//------------------------------------------------------------------------------
constructor TTrayIcon.Create(aOwner : Tcomponent);
begin
  inherited create(aOwner);

  // create my own window handle to receive windows messages
  mWindowHandle := AllocateHWnd( WndProc );

  FIcon := TIcon.Create;
  SetShowApp(False);
end;
//------------------------------------------------------------------------------
destructor TTrayIcon.Destroy;
begin
  // BDS
  CloseHandle(hMapping);

  if (not (csDesigning in ComponentState) and fActive) or
     ((csDesigning in ComponentState) and fShowDesigning) then
    DeleteIcon;
  FIcon.Free;

  DeAllocateHWnd( mWindowHandle );
  inherited destroy;
end;
//------------------------------------------------------------------------------
function TTrayIcon.AddIcon : boolean;
begin
  FillDataStructure;
  result := Shell_NotifyIcon(NIM_ADD, @IconData);
  // For some reason, if there is no tool tip set up, then the icon
  // doesn't display.  This fixes that.
  if fToolTip = '' then
    PostMessage( mWindowHandle, WM_RESETTOOLTIP, 0, 0 );
end;
//------------------------------------------------------------------------------
function TTrayIcon.DeleteIcon : boolean;
begin
  Result := Shell_NotifyIcon(NIM_DELETE,@IconData);
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.DoRightClick( Sender : TObject );
var
  xMouseCo: Tpoint;
begin
  GetCursorPos(xMouseCo);
  if assigned( fPopupMenu ) then begin
    SetForegroundWindow( Application.Handle );
    Application.ProcessMessages;
    fPopupmenu.Popup( xMouseco.X, xMouseco.Y );
  end;
  if assigned( FOnRightClick ) then
    FOnRightClick(self, mbRight, [], xMouseCo.x, xMouseCo.y);
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.FillDataStructure;
begin
  with IconData do begin
    cbSize := sizeof(TNOTIFYICONDATA);
    wnd := mWindowHandle;
    uID := 0; // is not passed in with message so make it 0
    uFlags := NIF_MESSAGE + NIF_ICON + NIF_TIP;
    hIcon := fIcon.Handle;
    StrPCopy(szTip,fToolTip);
    uCallbackMessage := WM_TOOLTRAYICON;
  end;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.GoToPreviousInstance;
begin
  PostMessage(hwnd_Broadcast, fMessageID, 0, 0);
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.Loaded;
var
  // BDS
  // hMapping: HWND;
  xTmp, xTmpID: PChar;
begin
  inherited Loaded;

  if fSendMsg <> '' then begin
    GetMem(xTmp, Length(fSendMsg) + 1);
    StrPCopy(xTmp, fSendMsg);
    fMessageID := RegisterWindowMessage(xTmp);
    FreeMem(xTmp);

    GetMem(xTmpID, Length(fSendMsg) + 1);
    StrPCopy(xTmpID, fSendMsg);

    hMapping := CreateFileMapping(HWND($FFFFFFFF), nil, PAGE_READONLY, 0, 32, xTmpID);
    if (hMapping <> NULL) and (GetLastError = ERROR_ALREADY_EXISTS) then begin
      if not (csDesigning in ComponentState) then begin
        GotoPreviousInstance;
        FreeMem(xTmpID);
        Application.Terminate;
        Exit;
      end;
    end;
    FreeMem(xTmpID);
  end;

  SetShowApp(fShowApp);
end;
//------------------------------------------------------------------------------
function TTrayIcon.ModifyIcon : boolean;
begin
  FillDataStructure;
  if fActive then
    result := Shell_NotifyIcon(NIM_MODIFY, @IconData)
  else
    result := True;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  case Operation of
    opRemove: if AComponent = fPopupMenu then fPopupMenu := Nil;
  end;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.SetActive(Value : boolean);
begin
  if value <> fActive then begin
    fActive := Value;
    if not (csdesigning in ComponentState) then begin
      if Value then
        AddIcon
      else
        DeleteIcon;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.SetIcon(Value : TIcon);
begin
  if Value <> fIcon then begin
    fIcon.Assign(value);
    ModifyIcon;
  end;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.SetShowApp(Value : boolean);      // A. Meeder
var
  xExWindowStyle: DWord;
begin
  if value <> fShowApp then
    fShowApp := value;

  if not (csDesigning in ComponentState) then begin
    Application.ShowMainForm := Value;
    xExWindowStyle := GetWindowLong(Application.Handle, GWL_EXSTYLE);

    if Value then begin
      xExWindowStyle := xExWindowStyle and not WS_EX_TOOLWINDOW {or WS_EX_APPWINDOW};
      ShowWindow(Application.Handle, SW_SHOW);
    end else begin
      xExWindowStyle := xExWindowStyle or WS_EX_TOOLWINDOW {and not WS_EX_APPWINDOW};
      ShowWindow(Application.Handle, SW_HIDE);
    end;

    SetWindowLong(Application.Handle, GWL_EXSTYLE, xExWindowStyle);
  end;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.SetShowDesigning(Value : boolean);
begin
  if csdesigning in ComponentState then begin
    if value <> fShowDesigning then begin
      fShowDesigning := Value;
      if Value then
        AddIcon
      else
        DeleteIcon;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.SetToolTip(Value : string);
begin
   // This routine ALWAYS re-sets the field value and re-loads the
   // icon.  This is so the ToolTip can be set blank when the component
   // is first loaded.  If this is changed, the icon will be blank on
   // the tray when no ToolTip is specified.
   if length( Value ) > 62 then
     Value := copy(Value,1,62);
   fToolTip := value;
   ModifyIcon;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.WndProc(var msg : TMessage);
begin
  with msg do
    if (msg = WM_RESETTOOLTIP) then
      SetToolTip( fToolTip )
    else if (msg = WM_TOOLTRAYICON) then begin
      case lParam of
        WM_LBUTTONDBLCLK: if assigned (FOnDblClick) then FOnDblClick(self);
        WM_LBUTTONUP    : if assigned(FOnClick)then FOnClick(self);
        WM_RBUTTONUP    : DoRightClick(self);
      else
      end;
    end else begin // Handle all messages with the default handler
      Result := DefWindowProc(mWindowHandle, Msg, wParam, lParam);
    end;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.SetPopupMenu(Value: TPopupMenu);
begin
  if Value <> fPopupMenu then begin
    fPopupMenu := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TTrayIcon.GetPopupMenu;
begin
  DoRightClick(self);
end;

end.

