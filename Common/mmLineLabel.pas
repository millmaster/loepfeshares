(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmLabel.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 25.10.2000  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmLineLabel;

interface

uses
  Classes, Graphics, StdCtrls, Windows;

type
  TmmLineLabel = class(TLabel)
  private
    fDistance: Integer;
    procedure SetDistance(const Value: Integer);
  protected
  public
    constructor Create(aOwner: TComponent); override;
    procedure Paint; override;
  published
    property Align;
    property Alignment;
    property Anchors;
//    property AutoSize;
    property BiDiMode;
    property Caption;
    property Color;
    property Constraints;
    property Distance: Integer read fDistance write SetDistance;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property FocusControl;
    property Font;
    property ParentBiDiMode;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowAccelChar;
    property ShowHint;
    property Transparent;
    property Layout;
    property Visible;
//    property WordWrap;
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{
procedure Register;
begin
  registercomponents('Wss', [TmmLineLabel]);
end;
{}

//------------------------------------------------------------------------------
// TmmLineLabel
//------------------------------------------------------------------------------
constructor TmmLineLabel.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  AutoSize := False;

  fDistance := 2;
end;
//------------------------------------------------------------------------------
procedure TmmLineLabel.Paint;
const
  Alignments: array[TAlignment] of Word = (DT_LEFT, DT_RIGHT, DT_CENTER);
  WordWraps: array[Boolean] of Word = (0, DT_WORDBREAK);
var
  H: Integer;
  R: TRect;
  CalcRect: TRect;
  DrawStyle: Longint;
  //---------------------------------------------------------------------
  procedure DrawLine(aX1, aX2, aY: Integer);
  begin
    with Canvas do begin
      Pen.Color := clBtnShadow;
      MoveTo(aX1, aY);
      LineTo(aX2, aY);

      inc(aY);
      Pen.Color := clBtnHighlight;
      MoveTo(aX1, aY);
      LineTo(aX2, aY);
    end;
  end;
  //---------------------------------------------------------------------
begin
  with Canvas do begin
    if not Transparent then begin
      Brush.Color := Self.Color;
      Brush.Style := bsSolid;
      FillRect(ClientRect);
    end;

    Font := Self.Font;
    case Layout of
      tlCenter: H := Height div 2;
      tlBottom: H := Height - TextHeight('0') div 2;
    else  // tlTop
      H := TextHeight('0') div 2;
    end;

    case Alignment of
      taLeftJustify: begin
          DrawLine(TextWidth(Caption) + fDistance, Width, H);
        end;
      taCenter: begin
          DrawLine(0, (Width - TextWidth(Caption)) div 2 - fDistance, H);
          DrawLine((Width + TextWidth(Caption)) div 2 + fDistance, Width, H);
        end;
      taRightJustify: begin
          DrawLine(0, Width - TextWidth(Caption) - fDistance, H);
        end;
    end;

    Brush.Style := bsClear;
    R := ClientRect;
    // DoDrawText takes care of BiDi alignments
    DrawStyle := DT_EXPANDTABS or WordWraps[WordWrap] or Alignments[Alignment];
    // Calculate vertical layout
    if Layout <> tlTop then
    begin
      CalcRect := R;
      DoDrawText(CalcRect, DrawStyle or DT_CALCRECT);
      if Layout = tlBottom then OffsetRect(R, 0, Height - CalcRect.Bottom)
      else OffsetRect(R, 0, (Height - CalcRect.Bottom) div 2);
    end;
    DoDrawText(R, DrawStyle);
  end;
end;
//------------------------------------------------------------------------------
procedure TmmLineLabel.SetDistance(const Value: Integer);
begin
  if Value <> fDistance then begin
    fDistance := Value;
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------
end.

