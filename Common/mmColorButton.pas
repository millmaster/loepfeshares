{$A+,B-,C+,D+,E-,F-,G+,H+,I+,J+,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmButton.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 14.01.2000  1.00  Wss | First version, this is only until we have a better one
| 20.01.2000  1.01  Wss | AutoLabel Property inserted
| 27.10.2000  1.01  Wss | Nervous flickering on MouseEnter/Leave removed
| 10.04.2003  1.01  Wss | Konstantennamen f�r ColorArray waren verwirrend
|=========================================================================================*)
unit mmColorButton;

interface

uses
  AutoLabelClass, Windows, Messages, SysUtils, Classes, dialogs,Graphics, Controls, Menus,
  Forms, Buttons;

const
  cColorRows = 5;
  cColorCols  = 4;
type
  TColorArray = Array[1..cColorRows, 1..cColorCols] of TColor;

  TButtonState = (bsDown, bsUp);

  TmmColorButton = class(TCustomControl)
  private
    fAutoLabel: TAutoLabel;
    fColorArray: TColorArray;
    fFlat: Boolean;
    fMargin: Integer;
    function GetVisible: Boolean;
    procedure SetVisible(Value: Boolean); virtual;
    procedure SetMargin(const Value: Integer);
    procedure SetFlat(const Value: Boolean);
  protected
    FChangeColor: TNotifyEvent;
    FColor: TColor;
    FRaiseContens: boolean;
    FDropDownMenu: TPopUpMenu;
    mButDown: Boolean;
    mHasMouse: Boolean;
    mIsFocused: Boolean;
    mKeybDown: Boolean;
    procedure DrawFrame(aButtonState: TButtonState);
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    //when the color changes in the selector form
    procedure  SelectColor(Sender: TObject; NewColor: TColor);
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Click; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    Procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CMMouseEnter(var Msg: TMessage); message CM_MOUSEENTER; //Detect Mouse
    procedure CMMouseLeave(var Msg: TMessage); message CM_MOUSELEAVE; //Detect Mouse
    procedure SetColor(Value: TColor);
    procedure SetEnabled(Value: Boolean); override;
    procedure WMSETFOCUS (var Msg: TMessage); message  WM_SETFOCUS;
    procedure WMKILLFOCUS (var Msg: TMessage); message WM_KILLFOCUS;
    procedure WMERASEBKGND(var message: TWMERASEBKGND); message WM_ERASEBKGND;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Paint; Override;

    property ColorArray: TColorArray read fColorArray write fColorArray;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Caption;
    property Color: TColor read FColor write SetColor;
    property OnChangeColor: TNotifyEvent read FChangeColor write FChangeColor;
    property Cursor;
    property Enabled;
    property Flat: Boolean read fFlat write SetFlat stored True default False;
    property Height;
    property HelpContext;
    property Hint;
    property Left;
    property Margin: Integer read fMargin write SetMargin default 5;
    property ParentShowHint;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Tag;
    property Top;
    property Visible: Boolean read GetVisible write SetVisible;
    property Width;

    property OnEnter;
    property OnExit;

    property OnClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;

  end;

type
  TSelectColor = procedure (Sender: TObject; NewColor: TColor) of object;
  TColorForm = class(tform)
  private
    FSelectColor: TSelectColor;
    FCurentColor: TColor;
    fColorArray: TColorArray;
    MouseInSquare: TPoint;
    procedure WMKILLFOCUS(var message: TWMKILLFOCUS); message WM_KILLFOCUS;
    procedure WMERASEBKGND(var message: TWMERASEBKGND); message WM_ERASEBKGND;
  protected
    procedure Paint; override;
    procedure DoShow; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    property ColorArray: TColorArray read fColorArray write fColorArray;
    property CurentColor: TColor read FCurentColor write FCurentColor;
  published
    //i believe that this is the best way to pass the selected color to TColorSelector.
//if you have a better idea please let me know
    property OnSelectColor: TSelectColor read FSelectColor write FSelectColor;
  end;

procedure Register;
procedure DrawSunkenSquare(Canvas: TCanvas; aRect: TRect);
procedure Frame3D(Canvas: TCanvas; var Rect: TRect; TopColor, BottomColor: TColor; Width: Integer);

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{$R mmColorButton.dfm}
//{$R ColorSelector.RES}

const
  cGridColors: TColorArray = ((clWhite,   clBlack,   clSilver,  clGray),
                              (clRed,     clMaroon,  clYellow,  clOlive),
                              (clLime,    clGreen,   clAqua,    clTeal),
                              (clBlue,    clNavy,    clFuchsia, clPurple),
                              ($00C0DCC0, $00F0CAA6, $00F0FBFF, $00A4A0A0));
  cColorFormWidth  = 78;
  cColorFormHeight = 122;

procedure Register;
begin
  RegisterComponents('LOEPFE', [TmmColorButton]);
end;


procedure Frame3D(Canvas: TCanvas; var Rect: TRect; TopColor, BottomColor: TColor; Width: Integer);

  procedure DoRect;
  var
    TopRight, BottomLeft: TPoint;
  begin
    with Canvas, Rect do begin
      TopRight.X := Right;
      TopRight.Y := Top;
      BottomLeft.X := Left;
      BottomLeft.Y := Bottom;
      Pen.Color := TopColor;
      PolyLine([BottomLeft, TopLeft, TopRight]);
      Pen.Color := BottomColor;
      Dec(BottomLeft.X);
      PolyLine([TopRight, BottomRight, BottomLeft]);
    end;
  end;

begin
  Canvas.Pen.Width := 1;
  Dec(Rect.Bottom); Dec(Rect.Right);
  while Width > 0 do begin
    Dec(Width);
    DoRect;
    InflateRect(Rect, -1, -1);
  end;
  Inc(Rect.Bottom); Inc(Rect.Right);
end;

//i know that this is not the best way of doing this.
//but it works for now.
//if you want to improve it fell free to do so
procedure DrawSunkenSquare(Canvas: TCanvas; aRect: TRect);
begin
  //don't change this line.
  //before i wrote this function i used DrawFrameControl to draw the mFrame. now this is needed to
  //make the image correct
  aRect := Rect(aRect.Left, aRect.Top, aRect.Right-1, aRect.Bottom-1);

  Canvas.Pen.color := clgray;
  Canvas.MoveTo(aRect.Right, aRect.Top);
  Canvas.LineTo(aRect.Left,  aRect.Top);
  Canvas.LineTo(aRect.Left,  aRect.Bottom);

  Canvas.pen.color := clwhite;
  Canvas.MoveTo(aRect.Left,  aRect.Bottom);
  Canvas.LineTo(aRect.Right, aRect.Bottom);
  Canvas.LineTo(aRect.Right, aRect.Top-1);

  InflateRect(aRect,-1,-1);
  Canvas.Pen.color := clblack;
  Canvas.MoveTo(aRect.Right, aRect.Top);
  Canvas.LineTo(aRect.Left,  aRect.Top);
  Canvas.LineTo(aRect.Left,  aRect.Bottom);

  Canvas.pen.color := $00DFDFDF;
  Canvas.MoveTo(aRect.Left,  aRect.Bottom);
  Canvas.LineTo(aRect.Right, aRect.Bottom);
  Canvas.LineTo(aRect.Right, aRect.Top-1);
end;

//draw the selected color rect
procedure DrawSelectedColorRect(aCanvas: TCanvas; aRect: TRect);
begin
  with aCanvas do begin
    // outer frame: black
    InflateRect(aRect, 1, 1);
    Brush.Color := clBlack;
    FrameRect(aRect);
    // middle frame: white
    InflateRect(aRect, -1, -1);
    Brush.Color := clWhite;
    FrameRect(aRect);
    // inner frame: black
    InflateRect(aRect, -1, -1);
    Brush.Color := clBlack;
    FrameRect(aRect);
  end;
end;
//------------------------------------------------------------------------------
constructor TColorForm.Create(AOwner: TComponent);
begin
  inherited Create(aOwner);
  BorderIcons := [];
  BorderStyle := bsnone;
  FormStyle := FsStayOnTop;
end;
//------------------------------------------------------------------------------
//just before we show the window we check is color is selected an select it
procedure TColorForm.DoShow;
var
   i,j:integer;
begin
  for i := 1 to cColorRows do
    for j := 1 to cColorCols do begin
      if fColorArray[i,j] = CurentColor then begin
//      if cGridColors[i,j] = CurentColor then begin
        MouseInSquare.x := j;
        MouseInSquare.y := i;
        exit;
      end;
    end;
  //if we get this far then the color must be diferent from the list.
  //so its at the right side of the "other" button
  MouseInSquare.x := 4;
  MouseInSquare.y := 6;
end;
//------------------------------------------------------------------------------
procedure TColorForm.Paint;
var xFrame: trect;
    i,j: integer;
    xBmp: TBitmap;// the doblebuffering bitmap

  function IsColorInArray(Color: TColor): Boolean;
  var i,j: integer;
  begin
    for i := 1 to cColorRows do
      for j := 1 to cColorCols do begin
        Result := fColorArray[i,j] = Color;
        if Result then
          Exit;
      end;
  end;

begin
  //i use doblebuffering here because it prevents the flikering because it draw
  //the whole image in the window,not step by step
  xBmp := TBitmap.Create;
  xBmp.Width  := Width;
  xBmp.Height := Height;
  //the background 3d rect
  DrawFrameControl(xBmp.Canvas.Handle, clientrect, DFC_BUTTON, DFCS_BUTTONPUSH);
  //the square
  for i := 1 to cColorRows do
    for j := 1 to cColorCols do begin
      // draw frame
      xFrame := Rect(4+(18*(j-1)), 4+(18*(i-1)), (4+(18*(j-1)))+16, (4+(18*(i-1)))+16);
      DrawSunkenSquare(xBmp.Canvas, xFrame);
      // draw color
      xBmp.Canvas.Brush.Color := fColorArray[i,j];
      InflateRect(xFrame, -2, -2);
      xBmp.Canvas.FillRect(xFrame);
    end;
  //draw an extra square if the curent color is not in the cGridColors array
  if not IsColorInArray(FCurentColor) then begin
    xFrame := Rect(Width-4-16,Height-4-17,Width-4,Height-5);
    DrawSunkenSquare(xBmp.Canvas, xFrame);
    xBmp.Canvas.Brush.Color := CurentColor;
    InflateRect(xFrame, -2, -2);
    xBmp.Canvas.FillRect(xFrame);
  end;

  // the dividing line
  xFrame := Rect(4, Height-26, Width-4, Height-26);
  Frame3D(xBmp.Canvas, xFrame, clWhite, clGray, 1);
  //draw the "other colors" button
  if IsColorInArray(FCurentColor) then //if the curent color is not in the array draw a smaller button
    xFrame := Rect(4, Height-25+3, 62+4, 18+Height-25+3)
  else
    xFrame := Rect(4, Height-25+3, 56, 18+Height-25+3);

  DrawFrameControl(xBmp.Canvas.Handle, xFrame, DFC_BUTTON, DFCS_BUTTONPUSH);
  xBmp.Canvas.Brush.Color := clBtnFace;
  DrawText(xBmp.Canvas.Handle, PChar(Caption), Length(Caption), xFrame, dt_SingleLine or Dt_VCenter or DT_CENTER);

  if ((MouseInSquare.X <> 0) and (MouseInSquare.y <> 0)){ AND
      (not ((MouseInSquare.X = 4) and (MouseInSquare.y = 6))) {}then begin
    // Draw the color selector
    // is extra color
    if ((MouseInSquare.X = 4) and (MouseInSquare.y = 6)) then
      xFrame := Rect(Width-4-16,Height-4-17,Width-4,Height-5)
    else
      // no, standard color
      xFrame := Rect(4+(18*(MouseInSquare.x-1)),4+(18*(MouseInSquare.y-1)),(4+(18*(MouseInSquare.x-1)))+16,(4+(18*(MouseInSquare.y-1)))+16);

    DrawSelectedColorRect(xBmp.Canvas, xFrame);
  end;

  //After we fineshed all the drawing we copy the final image to the form's Canvas
  BitBlt(Canvas.Handle, 0, 0, Width, Height, xBmp.Canvas.Handle, 0, 0, SRCCOPY);
end;
//------------------------------------------------------------------------------
procedure TColorForm.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin

  //if we've clicked on a color then close
  if (MouseInSquare.x <> 0) and (MouseInSquare.y <> 0) then begin
    FSelectColor(Self, fColorArray[MouseInSquare.y, MouseInSquare.x]);
//    FSelectColor(Self,cGridColors[MouseInSquare.y][MouseInSquare.x]);
    Close;
  end else if ((X >=3) and (X <= 62+3) and (Y >= Height-24+3) and (Y <= 18+Height-24+3)) then
    //if in the buton rect
    with TColorDialog.Create(nil) do
    try
      Options := [cdFullOpen];
      Color   := CurentColor;
      if Execute then
        FSelectColor(Self, Color);
    finally
      Free;
    end;
end;
//------------------------------------------------------------------------------
procedure TColorForm.MouseMove(Shift: TShiftState; X, Y: Integer);
  function PointInRect(APoint: TPoint; ARect: TRect): Boolean;
  begin
    Result := ((Apoint.X > ARect.Left) and (APoint.X < ARect.Right)) and ((APoint.Y > ARect.Top) and (APoint.Y < ARect.Bottom));
  end;

  function GetMouseSquare(APoint: TPoint): TPoint;
  var i,j: integer;
      xFrame: TRect;
  begin
    for i := 1 to 5 do
      for j := 1 to 4 do begin
        xFrame := Rect(4+(18*(j-1)),4+(18*(i-1)),(4+(18*(j-1)))+16,(4+(18*(i-1)))+16);
        //inflaterect so the rects are beggier o cover the margins also
        InflateRect(xFrame,2,2);
        if PointInRect(APoint,xFrame) then begin
          Result.X := j;
          Result.Y := i;
          Exit;
        end;
        //if we get this far then the mouse is outside the color area, so return (0,0) so we don't repaint
        Result.X := 0;
        Result.Y := 0;
      end;
    //if we get this far the mouse might be in the diferent color rect. So see if it is
    xFrame := Rect(Width-4-16,Height-4-16,Width-4,Height-4);
    if PointInRect(APoint,xFrame) then begin
      Result.X := 4;
      Result.Y := 6;
      Exit;
    end;
  end;

var
   NewSquare: TPoint;
begin
  NewSquare := GetMouseSquare(Point(X,Y));
  //if new square is diferent from old then repaint
  //if we are outside the color area the square is always (0,0) so we don't repaint
  if (MouseInSquare.X <> NewSquare.X) or (MouseInSquare.Y <> NewSquare.Y) then begin
    MouseInSquare := NewSquare;
    Invalidate;  //repaint with the new sqaure
  end;
end;
//------------------------------------------------------------------------------
//when the window loses the focus kills it self
procedure TColorForm.WMKILLFOCUS(var message: TWMKILLFOCUS);
begin
  Self.Close;
end;
//------------------------------------------------------------------------------
//if we bsDown't erase the backGround we save lots of time and prevent flikering
//bisedes there is no need to erase the backgorund since we are going to paint over it
procedure TColorForm.WMERASEBKGND(var message: TWMERASEBKGND);
begin
  message.Result := 1;
end;
//------------------------------------------------------------------------------

(* Utility Functions *)
procedure CreatePopMark(Canvas: TCanvas; X, Y: Integer; aColor: TColor) ;
var
  Pts:array[1..3] of TPoint;
  OldBColor, OldPenColor: TColor;
begin
  OldBColor := Canvas.Brush.Color;
  OldPenColor := Canvas.Pen.Color;

  Canvas.brush.color := aColor;
  Canvas.pen.color   := aColor;

  pts[1]:=point(x,y);
  pts[2]:=point(x+4,y);
  pts[3]:=point(x+2,y+2);
  Canvas.Polygon(pts);

  Canvas.brush.Color := OldBColor;
  Canvas.pen.color := OldPenColor;
end;

//------------------------------------------------------------------------------
// TmmColorButton
//------------------------------------------------------------------------------
procedure TmmColorButton.Click;
var
  xPos: TPoint;
begin
  xPos := ClientToScreen(Point(0, Height));
  with TColorform.Create(Self) do
  try
    Caption := Self.Caption;
    SetBounds(xPos.X, xPos.Y, cColorFormWidth, cColorFormHeight);
    OnSelectColor := SelectColor;
    CurentColor := FColor;
    ColorArray  := Self.fColorArray;
    Show;
  except
  end;
  inherited Click;
end;
//------------------------------------------------------------------------------
Procedure TmmColorButton.CMEnabledChanged(var Message: TMessage);
Begin
  Inherited;
  Invalidate;  // Redraw the button.
end;
//------------------------------------------------------------------------------
//Mouse Events
procedure TmmColorButton.CMMouseEnter(var Msg: TMessage);
begin
  mHasMouse := True;
  if Flat then
    Invalidate;  // Redraw the button.
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.CMMouseLeave(var Msg: TMessage);
begin
  mHasMouse := False;
  if Flat then
    Invalidate;  // Redraw the button.
end;
//------------------------------------------------------------------------------
constructor TmmColorButton.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Caption    := '&Weitere...';
  fAutoLabel := TAutoLabel.Create(Self);
  fColorArray := cGridColors;
  fFlat      := False;
  fMargin    := 5;
  Width      := 42;
  Height     := 21;
  TabStop    := True;
  Color      := clBlack;
end;
//------------------------------------------------------------------------------
destructor TmmColorButton.Destroy;
begin
  fAutoLabel.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.DrawFrame(aButtonState: TButtonState);
var
  xColorRect: TRect;
  xLineRect: TRect;
  xMarkX, xMarkY: Integer;
  xQuarterX: Integer;
  xFocusRect: TRect;
  xEdge, xFlag: Integer;
  xRect: TRect;

  procedure DrawColorRect;
  var
    xOldBrushColor: TColor;
    xOldPenColor: TColor;
  begin
    with Canvas do begin
      xOldBrushColor := Brush.Color;
      xOldPenColor   := Pen.Color;
      if Enabled then begin
        // draw colored ColorRect
        Pen.Color   := clblack;
        Brush.color := Color;
        Rectangle(xColorRect);
        // black pop mark
        CreatePopMark(Canvas, xMarkX, xMarkY, clBlack);
      end else begin
        // draw ghost ColorRect
        Frame3D(Canvas, xColorRect, clGray, clWhite, 1);
        // gray popup mark
        CreatePopMark(Canvas, xMarkX, xMarkY, clGray);
      end;
      Brush.color := xOldBrushColor;
      Pen.Color   := xOldPenColor;
    end;
  end;

begin

  xQuarterX := Width div 4;
  // position of popup mark
  xMarkX := (Width - xQuarterX) + (xQuarterX div 2) - 4;
  xMarkY := (Height div 2) - 1;
  // all other position in TRect variables
  xColorRect := Rect(0, 0, Width - xQuarterX - (2*fMargin), Height - (2*fMargin));
  xLineRect  := Rect(0, 0, 0, Height - (2*fMargin));
  xFocusRect := GetClientRect;
  InflateRect(xFocusRect, -3, -3);

  case aButtonState of
    bsDown: begin
        if fFlat then xEdge := BDR_SUNKENOUTER
                 else xEdge := DFCS_BUTTONPUSH + DFCS_PUSHED;
        // position of line
        OffsetRect(xLineRect, Width - xQuarterX - (fMargin div 2) + 1, fMargin +1);
        // position of ColorRect
        OffsetRect(xColorRect, fMargin + 1, fMargin + 1);
        // position of popup mark
        inc(xMarkX);
        inc(xMarkY);
      end;
  else  // bsUp
    if fFlat then xEdge := BDR_RAISEDINNER
             else xEdge := DFCS_BUTTONPUSH;  // default state for DrawFrameControl
    // position of line
    OffsetRect(xLineRect, Width - xQuarterX - (fMargin div 2), fMargin);
    // position of ColorRect
    OffsetRect(xColorRect, fMargin, fMargin);
  end;

  // draw border
  if fFlat {} then begin
    xRect := ClientRect;
    if mHasMouse then begin
      xFlag := BF_MIDDLE + BF_RECT;
      DrawEdge(Canvas.Handle, xRect, xEdge, xFlag);
    end else begin
      Canvas.Brush.Color := clBtnFace;
      Canvas.FillRect(xRect);
    end;
  end else
    DrawFrameControl(Canvas.Handle, ClientRect, DFC_BUTTON, xEdge);

  // draw ColorRect
  DrawColorRect;
  // draw the vertical line
  Frame3D(Canvas, xLineRect, clWhite, clGray, 1);
  // draw FocusRect
  if mIsFocused then
    Canvas.DrawFocusRect(xFocusRect);
end;
//------------------------------------------------------------------------------
function TmmColorButton.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key,Shift);
  If (Key = VK_SPACE) and (not mKeybDown)  then begin
    //if the space is already pressed there is no need to repaint the button
    mButDown  := true;
    mKeybDown := true;
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.KeyUp(var Key: Word; Shift: TShiftState);
begin
  inherited KeyUp(Key,Shift);
  if Key = VK_SPACE then begin
    mButDown  := false;
    mKeybDown := false;
    Invalidate;
    //finally show the color menu
    Click;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.KeyPress(var Key: Char);
begin
  if (Key = #13){ or (Key = #32)} then
    Click;
  inherited;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseDown(Button,Shift,x,y);
  If Button = mbLeft then begin
    mButDown := true;
    Invalidate;
    SetFocus;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseUp(Button,Shift,x,y);
  If Button = mbLeft then begin
    mButDown:=false;
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
    fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.Paint;
begin
  inherited Paint;
  if (mButDown and mHasMouse) or (mButDown and mKeybDown) then
    DrawFrame(bsDown)
  else
    DrawFrame(bsUp);
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.SelectColor(Sender: TObject; NewColor: TColor);
begin
  if FColor <> NewColor then begin
    FColor :=  NewColor;
    Invalidate; //repaint with the new color
    if assigned(FChangeColor) then //if FChangeColor is not assgned and we call it we get a runtime error
      FChangeColor(Self);
  end;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.SetColor(Value: TColor);
begin
  if FColor <> Value then begin
    FColor := Value;
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.SetFlat(const Value: Boolean);
begin
  if fFlat <> Value then begin
    fFlat := Value;
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;
//------------------------------------------------------------------------------
//if we bsDown't erase the backGround we save lots of time and prevent flikering
//bisedes there is no need to erase the backgorund since we are going to paint over it
procedure TmmColorButton.WMERASEBKGND(var message: TWMERASEBKGND);
begin
   message.Result := 1;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.WMKILLFOCUS (var Msg: TMessage);
begin
  mIsFocused := false;
  Invalidate;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.WMSETFOCUS (var Msg: TMessage);
begin
  mIsFocused := true;
  Invalidate;
end;
//------------------------------------------------------------------------------
procedure TmmColorButton.SetMargin(const Value: Integer);
begin
  if Value <> fMargin then begin
    fMargin := Value;
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------
end.

