(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DBVisualBox.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Displays a column from a database in a combobox.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 09.09.1998  1.00  Wss | Initial Release
| 13.09.2000  1.01  Wss | OnAfterFill event implemented
| 27.09.2000  1.02  SDo | In func. FillItemList ItemIndex:=0 added
| 26.01.2001  1.03  Wss | VisualListBox: if value = '' and Key <> '' then add 'ID ' + IntToStr(xKey)
                          Unselect items in list fixed if not in MultiSelect mode
| 07.02.2001  1.04  Wss | Empty SetSelection for clear selection corrected
| 12.02.2001  1.05  Wss | After fill in values call First to move to top position
| 24.10.2001  1.06  Wss | Check of Destroying and Designing before call FillItemListProc
| 11.02.2002  1.07  Wss | - TDBVisualListBox.SetSelection prueft nun auf ID's statt auf Name
| 26.07.2002  1.08WssNue| TDBVisualStringList added.
| 26.08.2002  1.09  Nue | TDBVisualStringList.Refresh added.
| 03.09.2002  1.10  Nue | OnBeforeFill event implemented
| 09.09.2002  1.11  Nue | Additionals because of multithread handlig of TDBVisualStringList
| 10.10.2002  1.11  Wss | property StoppFilling added in TDBVisualStringList and some improvments
| 23.01.2003  1.11  Wss | property ItemsCount hinzugef�gt. Gibt Anzahl der Items von mDBValues zur�ck
                          property SelCount hinzugef�gt. Gibt die Anzahl der Selektion zur�ck
| 13.11.2003  1.11  Wss | Multiselect war gar nicht implementiert -> einfache Version in AddSelection realisiert
|=========================================================================================*)
unit DBVisualBox;                

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DsgnIntf, Grids, DB{, dbtables}, mmComboBox, mmListBox;

const
  WM_STOPPFILLING = WM_USER+10;

  cInvalidIDValue = -1;

type
//------------------------------------------------------------------------------
  IFillDBVisualBox = interface (IUnknown)
    procedure FillItemList(aActive: Boolean);
  end;

//------------------------------------------------------------------------------
  TVisualBoxDataLink = class (TDataLink)
  private
    mControl: IFillDBVisualBox;
  protected
    procedure ActiveChanged; override;
  public
    constructor Create(aControl: IFillDBVisualBox);
  end;
  
//------------------------------------------------------------------------------
  TDBVisualBox = class (TmmComboBox, IFillDBVisualBox)
  private
    fDataField: string;
    FDataLink: TVisualBoxDataLink;
    fDefaultItems: string;
    fKeyField: string;
    fOnAfterFill: TNotifyEvent;
    fSorted: Boolean;
    function GetDataSource: TDataSource;
    function GetKeyValue: Integer;
    procedure SetDataField(const Value: string);
    procedure SetDataSource(aValue: TDataSource);
    procedure SetKeyField(const Value: string);
    procedure SetSorted(const Value: Boolean);
  protected
    procedure FillItemList(aActive: Boolean);
    procedure KeyPress(var Key: Char); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property KeyValue: Integer read GetKeyValue;
  published
    property DataField: string read fDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property DefaultItems: string read fDefaultItems write fDefaultItems;
    property KeyField: string read fKeyField write SetKeyField;
    property OnAfterFill: TNotifyEvent read fOnAfterFill write fOnAfterFill;
    property Sorted: Boolean read fSorted write SetSorted default False;
  end;

//------------------------------------------------------------------------------
  TDBVisualListBox = class (TmmListBox, IFillDBVisualBox)
  private
    fDataField: string;
    FDataLink: TVisualBoxDataLink;
    fDefaultItems: string;
    fKeyField: string;
    fOnAfterFill: TNotifyEvent;
    fSorted: Boolean;
    mSelectedItems: string;
    function GetDataCommaText: string;
    function GetDataSource: TDataSource;
    function GetKeyCommaText: string;
    function GetKeyValue: Integer;
    procedure SetDataCommaText(const Value: string);
    procedure SetDataField(const Value: string);
    procedure SetDataSource(aValue: TDataSource);
    procedure SetKeyCommaText(const Value: string);
    procedure SetKeyField(const Value: string);
    procedure SetSorted(const Value: Boolean);
  protected
    procedure DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState); override;
    procedure FillItemList(aActive: Boolean);
    procedure KeyPress(var Key: Char); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure ResetSelection;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function DataText(aDefault: String): string;
    procedure SetSelection(aCommaText: String);
    property DataCommaText: string read GetDataCommaText write SetDataCommaText;
    property KeyCommaText: string read GetKeyCommaText write SetKeyCommaText;
    property KeyValue: Integer read GetKeyValue;
  published
    property DataField: string read fDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property DefaultItems: string read fDefaultItems write fDefaultItems;
    property KeyField: string read fKeyField write SetKeyField;
    property OnAfterFill: TNotifyEvent read fOnAfterFill write fOnAfterFill;
    property Sorted: Boolean read fSorted write SetSorted default False;
  end;

//------------------------------------------------------------------------------
  TDBVisualStringList = class (TStringGrid, IFillDBVisualBox)
  private
    fDataField: string;
    fDefaultItems: string;
    fKeyField: string;
    fOnAfterFill: TNotifyEvent;
    fOnBeforeFill: TNotifyEvent;
    fSorted: Boolean;
    fMultiselect: Boolean;
    fStoppFilling: Boolean;
    fOnSelect: TNotifyEvent;
    fFilter: String;
    mDataLink: TVisualBoxDataLink;
    mSelectedItems: string;
    mDBValues: TStringList;
    mStartRow: Integer;
    mSelectedList: TList;
    mSelecting: Boolean;
    mStoppFilling: Boolean;
    procedure AddSelection(aFrom: Integer; aTo: Integer = -1);
    procedure AssignSelectedList(aNewList: TList);
    procedure CleanupUnusedSelections;
    procedure ClearSelection(aRow: Integer = -1);
    function GetDataCommaText: string;
    function GetDataSource: TDataSource;
    function GetKeyCommaText: string;
    function GetKeyValue: Integer;
    procedure SetDataCommaText(const Value: string);
    procedure SetDataField(const Value: string);
    procedure SetDataSource(aValue: TDataSource);
    procedure SetKeyCommaText(const Value: string);
    procedure SetKeyField(const Value: string);
    procedure SetSorted(const Value: Boolean);
    function UsedKeyField: Boolean;
    procedure SetFilter(Value: String);
    procedure UpdateGridList(aWithSelection: Boolean);
    function GetItemsCount: Integer;
    function GetSelCount: Integer;
  protected
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    procedure FillItemList(aActive: Boolean);
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure Resize; override;
    function SelectCell(ACol, ARow: Longint): Boolean; override;
    procedure TimedScroll(Direction: TGridScrollDirection); override;
    procedure WMSTOPPFILLING(var aMsg: TMessage);message WM_STOPPFILLING;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function DataText(aDefault: String; aSorted: Boolean = True): string;
    procedure GetSelectionList(aList: TStrings);
    function IsRowSelected(aRow: Integer): Boolean;
    function IsIDSelected(aID: Integer): Boolean;
    procedure SetSelection(aCommaText: String);
    procedure Refresh(aActive: Boolean);
    property DataCommaText: string read GetDataCommaText write SetDataCommaText;
    property Filter: String read fFilter write SetFilter;
    property ItemsCount: Integer read GetItemsCount;
    property KeyCommaText: string read GetKeyCommaText write SetKeyCommaText;
    property KeyValue: Integer read GetKeyValue;
    property Multiselect: Boolean read fMultiselect write fMultiselect;
    property SelCount: Integer read GetSelCount;
    property StoppFilling: Boolean read fStoppFilling write fStoppFilling;
  published
    property DataField: string read fDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property DefaultItems: string read fDefaultItems write fDefaultItems;
    property KeyField: string read fKeyField write SetKeyField;
    property OnAfterFill: TNotifyEvent read fOnAfterFill write fOnAfterFill;
    property OnBeforeFill: TNotifyEvent read fOnBeforeFill write fOnBeforeFill;
    property OnSelect: TNotifyEvent read fOnSelect write fOnSelect;
    property Sorted: Boolean read fSorted write SetSorted default False;
  end;

//------------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  Commctrl, ADODb,
  mmCS, TypInfo, mmStringList;

//************************************************************************
// Tool procedures
//************************************************************************
procedure FillItemListProc(aDataSet: TDataSet; aDefaults, aDataField, aKeyField: String; aItemList: TStrings; aSorted: Boolean);
var
  xStrList: TmmStringList;
  xStr: String;
  xKey: Integer;
  xDataField, xKeyField: TField;
begin
//  EnterMethod('FillItemListProc');
  xKeyField := Nil;
  if aDataField <> '' then begin
    xKey := 0;
    xStrList := TmmStringList.Create;
    xStrList.BeginUpdate;
    with aDataSet do
    try
      DisableControls;
      if not(Active) then
        Open;

      xDataField := FieldByName(aDataField);
      if aKeyField <> '' then
        xKeyField := FieldByName(aKeyField);

      while not EOF do begin
        xStr := xDataField.AsString;
//        if aKeyField <> '' then begin
        if Assigned(xKeyField) then begin
          xKey := xKeyField.AsInteger;
          if Trim(xStr) = '' then
            xStr := 'ID ' + IntToStr(xKey);
        end;

        xStrList.AddObject(xStr, Pointer(xKey));
        Next;
      end;
    finally
      EnableControls;
      xStrList.Sorted := aSorted;
      // assign results into destination string list
      aItemList.BeginUpdate;
      aItemList.CommaText := aDefaults;
      aItemList.AddStrings(xStrList);
      aItemList.EndUpdate;
      // clean up
      xStrList.EndUpdate;
      xStrList.Free;
    end; // width
  end; // if aDataField
end;
//------------------------------------------------------------------------
procedure LockControl(c: TWinControl; bLock: Boolean);
begin
  if (c = nil) or (c.Handle = 0) then Exit;
  if bLock then
    SendMessage(c.Handle, WM_SETREDRAW, 0, 0)
  else
  begin
    SendMessage(c.Handle, WM_SETREDRAW, 1, 0);
    RedrawWindow(c.Handle, nil, 0,
      RDW_ERASE or RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN);
  end;
end;
//------------------------------------------------------------------------
function GetKeyValueProc(aItemList: TStrings; aString: String): Integer;
var
  xIndex: Integer;
begin
  Result := -1;
  xIndex := aItemList.IndexOf(aString);
  if xIndex >= 0 then
    Result := Integer(aItemList.Objects[xIndex]);
end;

//:-----------------------------------------------------------------------------
//:-- TVisualBoxDataLink
//:-----------------------------------------------------------------------------
constructor TVisualBoxDataLink.Create(aControl: IFillDBVisualBox);
begin
  inherited Create;
  mControl := aControl;
end;

//:-----------------------------------------------------------------------------
procedure TVisualBoxDataLink.ActiveChanged;
begin
  mControl.FillItemList(Self.Active);
end;


//:-----------------------------------------------------------------------------
//:-- TDBVisualBox
//:-----------------------------------------------------------------------------
constructor TDBVisualBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDataLink := TVisualBoxDataLink.Create(Self);
  fOnAfterFill := Nil;
end;

//:-----------------------------------------------------------------------------
destructor TDBVisualBox.Destroy;
begin
  FDataLink.Free;
  inherited Destroy;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualBox.FillItemList(aActive: Boolean);
begin
  Enabled := aActive;
  if (([csDesigning, csDestroying] * ComponentState) = []) then
    if aActive then begin
      FillItemListProc(fDataLink.DataSource.DataSet, fDefaultItems, fDataField, fKeyField, Items, fSorted);
      ItemIndex := 0;
      if Assigned(fOnAfterFill) then
        fOnAfterFill(Self);
    end;
end;

//:-----------------------------------------------------------------------------
function TDBVisualBox.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

//:-----------------------------------------------------------------------------
function TDBVisualBox.GetKeyValue: Integer;
begin
  Result := -1;
  if fKeyField <> '' then begin
    Result := GetKeyValueProc(Items, Text);
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualBox.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  // Prevent any editing of text field: it's a visual box
  // Delete key is allowed
  if Key <> #08 then
    Key := #00;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  // if associated datasource is deleted remove link in properties
  if (Operation = opRemove) and (aComponent = DataSource) then begin
    DataField := '';
    DataSource := Nil;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualBox.SetDataField(const Value: string);
begin
  if Value <> fDataField then
    fDataField := Trim(Value);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualBox.SetDataSource(aValue: TDataSource);
begin
  if Assigned(aValue) then
    FDataLink.DataSource := aValue
  else
    FDataLink.DataSource := Nil;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualBox.SetKeyField(const Value: string);
begin
  if Value <> fKeyField then
    fKeyField := Trim(Value);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualBox.SetSorted(const Value: Boolean);
begin
  fSorted := Value;
end;


//:-----------------------------------------------------------------------------
//:-- TDBVisualListBox
//:-----------------------------------------------------------------------------
constructor TDBVisualListBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fDataLink := TVisualBoxDataLink.Create(Self);
  fOnAfterFill := Nil;

  mSelectedItems := '';
end;

//:-----------------------------------------------------------------------------
destructor TDBVisualListBox.Destroy;
begin
    FDataLink.Free;
    inherited Destroy;
end;

//:-----------------------------------------------------------------------------
function TDBVisualListBox.DataText(aDefault: String): string;
var
  i: Integer;
  xStrList: TmmStringList;
begin
  Result := aDefault;
  xStrList := TmmStringList.Create;
  try
    for i:=0 to Items.Count-1 do begin
      if Selected[i] then
        xStrList.Add(Items.Strings[i]);
    end;
    if xStrList.Count > 0 then
      Result := xStrList.Text;
  finally
    xStrList.Free;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if (not Focused) and (Index >= 0) and (odSelected in State) then begin
    Canvas.Brush.Color := clInactiveCaptionText;
    Canvas.Font.Color := clWindowText;
  end;
  inherited DrawItem(Index, Rect, State)
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.FillItemList(aActive: Boolean);
var
  xSorted: Boolean;
begin
  if (([csDesigning, csDestroying] * ComponentState) = []) then
    if aActive then begin
  //    TListBoxStrings(Items).
      xSorted := Sorted;
      Sorted  := False;
      FillItemListProc(fDataLink.DataSource.DataSet, fDefaultItems, fDataField, fKeyField, Items, fSorted);
      Sorted  := xSorted;

      if Trim(mSelectedItems) <> '' then
        SetSelection(mSelectedItems);
  
      if Assigned(fOnAfterFill) then
        fOnAfterFill(Self);
    end else begin
      mSelectedItems := KeyCommaText;
    end;
end;

//:-----------------------------------------------------------------------------
function TDBVisualListBox.GetDataCommaText: string;
var
  i: Integer;
  xStrList: TmmStringList;
begin
  Result := '';
  xStrList := TmmStringList.Create;
  try
    for i:=0 to Items.Count-1 do begin
      if Selected[i] then
        xStrList.Add(Format('''%s''', [Items.Strings[i]]));
    end;
    Result := xStrList.CommaText;
  finally
    xStrList.Free;
  end;
end;

//:-----------------------------------------------------------------------------
function TDBVisualListBox.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

//:-----------------------------------------------------------------------------
function TDBVisualListBox.GetKeyCommaText: string;
var
  i: Integer;
  xStrList: TmmStringList;
begin
  Result := '';
  if KeyField <> '' then begin
    xStrList := TmmStringList.Create;
    try
      for i:=0 to Items.Count-1 do begin
        if Selected[i] then begin
          // check if a DefaultItems value is selected -> stopp loop
          if (Pos(Items.Strings[i], fDefaultItems) <> 0) and (xStrList.Count = 0) then
            Break
          else
            xStrList.Add(IntToStr(Integer(Items.Objects[i])));
        end;
      end;
      Result := xStrList.CommaText;
    finally
      xStrList.Free;
    end;
  end;
end;

//:-----------------------------------------------------------------------------
function TDBVisualListBox.GetKeyValue: Integer;
begin
  Result := -1;
  if fKeyField <> '' then begin
    Result := GetKeyValueProc(Items, Text);
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.KeyPress(var Key: Char);
begin
  // Prevent any editing of text field: it's a visual box
  // Delete key is allowed
  if Key <> #08 then
    Key := #00;
  inherited KeyPress(Key);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  // if associated datasource is deleted remove link in properties
  if (Operation = opRemove) and (aComponent = DataSource) then begin
    DataField := '';
    DataSource := Nil;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.ResetSelection;
var
  i: Integer;
begin
  Hint := '';
  for i:=0 to Items.Count-1 do
    Selected[i] := False;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.SetDataCommaText(const Value: string);
var
  i: Integer;
  xStrList: TmmStringList;
begin
  if KeyField <> '' then begin
    xStrList := TmmStringList.Create;
    try
      xStrList.CommaText := Value;
      for i:=0 to Items.Count-1 do begin
        if xStrList.IndexOf(Self.Items.Strings[i]) >= 0 then
          Selected[i] := True;
      end;
    finally
      xStrList.Free;
    end;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.SetDataField(const Value: string);
begin
  if Value <> fDataField then
    fDataField := Trim(Value);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.SetDataSource(aValue: TDataSource);
begin
  if Assigned(aValue) then
    FDataLink.DataSource := aValue
  else
    FDataLink.DataSource := Nil;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.SetKeyCommaText(const Value: string);
var
  i: Integer;
  xStrList: TmmStringList;
begin
  ResetSelection;
  if KeyField <> '' then begin
    xStrList := TmmStringList.Create;
    try
      xStrList.CommaText := Value;
      for i:=0 to Items.Count-1 do begin
        if xStrList.IndexOf(IntToStr(Integer(Items.Objects[i]))) >= 0 then
          Selected[i] := True;
      end;
    finally
      xStrList.Free;
    end;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.SetKeyField(const Value: string);
begin
  if Value <> fKeyField then
    fKeyField := Trim(Value);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.SetSelection(aCommaText: String);
var
  xStrList: TStringList;
  i, xIndex: Integer;
begin
  xStrList := TStringList.Create;
  try
    // First: clear all selections
    if MultiSelect then begin
      ResetSelection;
    end else begin
      ItemIndex := -1;
    end;

    // Second: check if some items match the selection
    xStrList.CommaText := StringReplace(aCommaText, '''', '', [rfReplaceAll]);
    for i:=0 to Items.Count-1 do begin
      xIndex := xStrList.IndexOf(IntToStr(Integer(Items.Objects[i])));
      if xIndex >= 0 then
        if MultiSelect then
          Selected[i] := True
        else begin
          ItemIndex := i;
          Break;
        end;
    end;
  finally
    xStrList.Free;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualListBox.SetSorted(const Value: Boolean);
begin
  fSorted := Value;
end;


//:-----------------------------------------------------------------------------
//:-- TDBVisualStringList
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.AddSelection(aFrom: Integer; aTo: Integer);
var
  i: Integer;
begin
//  if aTo = -1 then
//    aTo := aFrom;

  // Nur 1 Zeile selektieren?
  if aTo = -1 then
    aTo := aFrom
  else
  if not fMultiselect then
    aFrom := aTo;

  for i:=aFrom to aTo do begin
    if not IsRowSelected(i) then begin
      // Wert in Objects ist entweder ID-Wert von DB oder Zeilennummer (wenn kein KeyField)
      mSelectedList.Add(Objects[0, i])
    end;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.ClearSelection(aRow: Integer);
var
  i: Integer;
begin
  Hint := '';

  if aRow = -1 then
    mSelectedList.Clear
  else begin
    i := mSelectedList.IndexOf(Objects[0, aRow]);
    if i >= 0 then
      mSelectedList.Delete(i);
  end;
end;
//:-----------------------------------------------------------------------------
constructor TDBVisualStringList.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  ColCount := 1;
  DefaultRowHeight := 13;
//  DoubleBuffered   := True;  // Flackern vermeiden
  FixedCols := 0;
  FixedRows := 0;
  Options := [goRangeSelect, goThumbTracking];
  RowCount := 1;

  fFilter        := '';
  fMultiselect   := True;
  fOnAfterFill   := Nil;
  fOnBeforeFill  := Nil;
  fOnSelect      := Nil;
  fStoppFilling  := False;
  mDataLink      := TVisualBoxDataLink.Create(Self);
  mDBValues      := TStringList.Create;
  mDBValues.Duplicates := dupAccept;
  mDBValues.Sorted     := False;
  mSelectedItems := '';
  mStartRow      := 0;
  mSelectedList  := TList.Create;
  mSelecting     := False;
  mStoppFilling  := False;
end;

//:-----------------------------------------------------------------------------
destructor TDBVisualStringList.Destroy;
begin
  mSelectedList.Free;
  mDataLink.Free;
  mDBValues.Free;
  inherited Destroy;
end;

//:-----------------------------------------------------------------------------
function TDBVisualStringList.DataText(aDefault: String; aSorted: Boolean): string;
var
  i: Integer;
  xStrList: TmmStringList;
begin
  Result := aDefault;
  xStrList := TmmStringList.Create;
  xStrList.Sorted := aSorted;
  if aSorted then
    xStrList.Duplicates := dupIgnore;
  try
    for i:=0 to RowCount-1 do begin
      if IsRowSelected(i) then
        xStrList.Add(Cells[0, i]);
    end;
    if xStrList.Count > 0 then
      Result := xStrList.Text;
  finally
    xStrList.Free;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
begin
  if IsRowSelected(ARow) then begin
    Canvas.Brush.Color := clHighLight;
    Canvas.Font.Color  := clHighLightText;
  end else begin
    Canvas.Brush.Color := clWindow;
    Canvas.Font.Color  := clWindowText;
  end;
  inherited DrawCell(ACol, ARow, ARect, AState);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.FillItemList(aActive: Boolean);
  //................................................................
  procedure FillItemListProc(aDataSet: TDataSet; aDefaults, aDataField, aKeyField: String);
  var
    xStr: String;
    xKey: Integer;
    xCount: Integer;
    xDataField, xKeyField: TField;
  begin
    xKeyField := Nil;
    if aDataField <> '' then begin
      // tempor�re Liste f�r selektierte Elemente
      with aDataSet do
      try
        if not(Active) then
          Open;

        xDataField := FieldByName(aDataField);
        if aKeyField <> '' then
          xKeyField := FieldByName(aKeyField);

        xCount := mDBValues.Count;
        while (not EOF) and (not (mStoppFilling or fStoppFilling)) do
        try
          xStr := xDataField.AsString;
          // wenn KeyField verwendet wird, dann den ID Wert von DB verwenden
//          if UsedKeyField then begin
          if Assigned(xKeyField) then begin
            xKey := xKeyField.AsInteger;
            if Trim(xStr) = '' then
              xStr := 'ID ' + IntToStr(xKey);
          end else
            // ansonsten einfach den Count
            xKey := mDBValues.Count;

          mDBValues.AddObject(xStr, Pointer(xKey));
          inc(xCount);
          // Zwischendurch das Grid updaten
          if xCount = 1000 then begin
            UpdateGridList(False);
            Sleep(1);
          end;

          Next;
        except
          on e:Exception do
            CodeSite.SendError('Fill error: ' + e.Message);
        end;
      finally
//        CleanupUnusedSelections;
        UpdateGridList(True);
//        CodeSite.SendFmtMsg('RowCount: %d, mDBValues: %d', [RowCount, mDBValues.Count]);
      end; // with aDataSet
    end; // if aDataField
  end;
  //................................................................
begin
  Enabled := aActive;
  if (([csDesigning, csDestroying] * ComponentState) = []) then
    if aActive then begin
      Sleep(1);
      Cols[0].Clear; // alle Strings in der Spalte l�schen, aber die RowCount bleibt
//      Cells[0,0] := DefaultItems;
      mDBValues.Clear;
      if DefaultItems <> '' then
        mDBValues.AddObject(DefaultItems, Pointer(cInvalidIDValue));
//      Cols[0].Assign(mDBValues);

      if Assigned(fOnBeforeFill) then    //3.8.02 Nue
        fOnBeforeFill(Self);             //3.8.02 Nue

      FillItemListProc(mDataLink.DataSource.DataSet, fDefaultItems, fDataField, fKeyField);

      mStoppFilling := False;

      if Assigned(fOnAfterFill) then
        fOnAfterFill(Self);
    end;
end;

//:-----------------------------------------------------------------------------
function TDBVisualStringList.GetDataCommaText: string;
var
  i: Integer;
  xStrList: TmmStringList;
begin
  Result := '';
  xStrList := TmmStringList.Create;
  try
    for i:=0 to RowCount-1 do begin
      if IsRowSelected(i) then
        xStrList.Add(Format('''%s''', [Cells[0, i]]));
    end;
    Result := xStrList.CommaText;
  finally
    xStrList.Free;
  end;
end;

//:-----------------------------------------------------------------------------
function TDBVisualStringList.GetDataSource: TDataSource;
begin
  Result := mDataLink.DataSource;
end;

//:-----------------------------------------------------------------------------
function TDBVisualStringList.GetKeyCommaText: string;
var
  i: Integer;
  xStrList: TmmStringList;
  xID: Integer;
begin
  //  F�r KeyKommaText gibt es nun drei Varianten:
  //  1. Nichts wurde selektiert und auch keine Filtrierung (mSelectedList.Count = 0)
  //     -> Result ist leer
  //  2. Etwas wurde selektiert
  //     -> Result wird aus den ID's von mSelectedList erstellt
  //  3. Nichts wurde selektiert ABER es wurde Filtriert
  //     -> Result wird aus den verf�gbaren Eintr�gen vom Grid erstellt
  Result := '';
  if UsedKeyField then begin
    xStrList := TmmStringList.Create;
    try
//      if (fFilter = '') then begin
        // Varianten 1 und 2: nichts oder dann halt die Selektion
        for i:=0 to mSelectedList.Count-1 do begin
          xID := Integer(mSelectedList.Items[i]);
          // wenn das DefaultItem in der Auswahl ist, dann abbrechen
          if xID = cInvalidIDValue then begin
            xStrList.Clear;
            Break;
          end else begin
            xStrList.Add(Format('''%d''', [xID]));
          end;
        end;
{
      end else begin
        // Variante 3: alle m�glichen Eintr�ge welche der Filter anzeigt
        for i:=0 to RowCount-1 do begin
          xID := Integer(Objects[0, i]);
          // wenn das DefaultItem in der Auswahl ist, dann abbrechen
          if xID <> cInvalidIDValue then begin
            xStrList.Add(Format('''%d''', [xID]));
          end;
        end;
      end;
{}
      Result := xStrList.CommaText;
//      CodeSite.SendString('KeyCommaText', Result);
    finally
      xStrList.Free;
    end;
  end;
end;

//:-----------------------------------------------------------------------------
function TDBVisualStringList.GetKeyValue: Integer;
begin
  Result := -1;
//  if fKeyField <> '' then begin
//     Result := GetKeyValueProc(Items, Text);
//  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.GetSelectionList(aList: TStrings);
var
  i: Integer;
begin
  aList.Clear;
  for i:=0 to RowCount-1 do begin
    if IsRowSelected(i) then
      aList.Add(Cells[0, i]);
  end;
end;
//:-----------------------------------------------------------------------------
function TDBVisualStringList.IsRowSelected(aRow: Integer): Boolean;
begin
  // ist die ID von der selektierten Zeile in der Objektliste mSelectedList drin?
//  Result := (mSelectedList.IndexOf(Objects[0, aRow]) >= 0);
  Result := IsIDSelected(Integer(Objects[0, aRow]));
end;
//:-----------------------------------------------------------------------------
function TDBVisualStringList.IsIDSelected(aID: Integer): Boolean;
begin
  // ist diese ID in der Objektliste mSelectedList drin?
  Result := (mSelectedList.IndexOf(TObject(aID)) >= 0);
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);

  if Key in [VK_UP, VK_DOWN, VK_HOME, VK_END, VK_PRIOR, VK_NEXT] then begin
    if ssShift in Shift then begin
      ClearSelection;
      if Row > mStartRow then
        AddSelection(mStartRow, Row)
      else
        AddSelection(Row, mStartRow);
    end else begin
      if not(ssShift in Shift) then
        ClearSelection;
      mStartRow := Row;
      AddSelection(Row);
    end;
    Invalidate;
    if Assigned(fOnSelect) then
      fOnSelect(Self);
  end;
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xCell, xRow: Integer;
begin
  mSelecting := True;
  inherited MouseDown(Button, Shift, X, Y);

  MouseToCell(X, Y, xCell, xRow);
  if xRow >= 0 then begin
    if ssShift in Shift then begin
      ClearSelection;
      // nach oben oder unten z�hlen?
      if xRow > mStartRow then
        AddSelection(mStartRow, xRow)
      else
        AddSelection(xRow, mStartRow);
    end else begin
      if not(ssCtrl in Shift) then begin
        ClearSelection;
        mStartRow := xRow;
      end;
      if IsRowSelected(xRow) then
        ClearSelection(xRow)
      else
        AddSelection(xRow);
    end;
  end;
  Invalidate;
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  xCell, xRow: Integer;
begin
  inherited MouseMove(Shift, X, Y);

  if Shift = [ssLeft] then begin
    MouseToCell(X, Y, xCell, xRow);
    if xRow <> -1 then begin
      ClearSelection;
      if xRow >= mStartRow then
        AddSelection(mStartRow, xRow)
      else
        AddSelection(xRow, mStartRow);
      Row := xRow;
    end;
    Invalidate;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xCell, xRow: Integer;
begin
  mSelecting := False;
  inherited MouseUp(Button, Shift, X, Y);
  MouseToCell(X, Y, xCell, xRow);
  if (xCell <> -1) and (xRow <> -1) then begin
    Row := xRow;
  end;

  // Wenn das DefaultItem mitselektiert wurde, dann die ganze selektion l�schen
  // und nur das DefaultItem als Selektion �bernehmen
  if (DefaultItems <> '') and IsRowSelected(0) and (mSelectedList.Count > 1) then begin
    ClearSelection;
    AddSelection(0);
    Invalidate;
  end;

  if Assigned(fOnSelect) and (mSelectedList.Count > 0) then
    fOnSelect(Self);
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  // if associated datasource is deleted remove link in properties
  if (Operation = opRemove) and (aComponent = DataSource) then begin
    DataField := '';
    DataSource := Nil;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.Refresh(aActive: Boolean);
begin
  FillItemList(aActive);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.Resize;
begin
  inherited Resize;
  ColWidths[0] := ClientWidth;
end;

//:-----------------------------------------------------------------------------
function TDBVisualStringList.SelectCell(ACol, ARow: Longint): Boolean;
begin
  if mSelecting then
    Result := False
  else
    Result := inherited SelectCell(ACol, ARow);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.SetDataCommaText(const Value: string);
var
  i: Integer;
  xStrList: TmmStringList;
begin
  xStrList := TmmStringList.Create;
  try
    xStrList.CommaText := Value;
    for i:=0 to RowCount-1 do begin
      if xStrList.IndexOf(Cells[0, i]) >= 0 then
        AddSelection(i);
    end;
  finally
    xStrList.Free;
    Invalidate;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.SetDataField(const Value: string);
begin
  if Value <> fDataField then
    fDataField := Trim(Value);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.SetDataSource(aValue: TDataSource);
begin
  if Assigned(aValue) then
    mDataLink.DataSource := aValue
  else
    mDataLink.DataSource := Nil;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.SetKeyCommaText(const Value: string);
var
  i: Integer;
  xStrList: TmmStringList;
begin
  ClearSelection;
  if UsedKeyField then begin
    xStrList := TmmStringList.Create;
    try
      xStrList.CommaText := Value;
      for i:=0 to RowCount-1 do begin
        if xStrList.IndexOf(IntToStr(Integer(Objects[0, i]))) >= 0 then
          AddSelection(i);
      end;
    finally
      xStrList.Free;
      Invalidate;
    end;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.SetKeyField(const Value: string);
begin
  if Value <> fKeyField then
    fKeyField := Trim(Value);
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.SetSelection(aCommaText: String);
var
  xStrList: TStringList;
  i: Integer;
begin
  ClearSelection;
  xStrList := TStringList.Create;
  try
    // Second: check if some items match the selection
    xStrList.CommaText := StringReplace(aCommaText, '''', '', [rfReplaceAll]);
    for i:=0 to RowCount-1 do begin
      if xStrList.IndexOf(IntToStr(Integer(Objects[0, i]))) >= 0 then
        AddSelection(i);
    end;
  finally
    xStrList.Free;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.SetSorted(const Value: Boolean);
begin
  fSorted := Value;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.TimedScroll(Direction: TGridScrollDirection);
var
  xRow: Integer;
begin
  inherited TimedScroll(Direction);
  if sdDown in Direction then
    xRow := Selection.Bottom
  else if sdUp in Direction then
    xRow := Selection.Top
  else
    Exit;

  ClearSelection;
  if xRow > mStartRow then
    AddSelection(mStartRow, xRow)
  else
    AddSelection(xRow, mStartRow);
  Invalidate;
end;


//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.WMSTOPPFILLING(var aMsg: TMessage);
begin
  if (aMsg.wParam=Self.Tag) then
  begin
    mStoppFilling := True;
//    CodeSite.SendFmtMsg('WM_STOPPFILLING arrived! Tag:%d',[Self.Tag]);
  end;
end;
//:-----------------------------------------------------------------------------

function TDBVisualStringList.UsedKeyField: Boolean;
begin
  Result := (fKeyField <> '');
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.AssignSelectedList(aNewList: TList);
var
  i: Integer;
begin
  mSelectedList.Clear;
  with aNewList do begin
    for i:=0 to Count-1 do
      mSelectedList.Add(Items[i]);
  end;
end;

procedure TDBVisualStringList.CleanupUnusedSelections;
var
  xIndex: Integer;
  i: Integer;
  xObj: TObject;
begin
  i := 0;
  while i < mSelectedList.Count do begin
    xObj := mSelectedList.Items[i];
    if mDBValues.IndexOfObject(xObj) = -1 then
      mSelectedList.Delete(i)
    else
      inc(i);
  end;
end;

//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.SetFilter(Value: String);
var
  xBool: Boolean;
begin
  Value := Trim(LowerCase(Value));
  if Value <> fFilter then begin
    fFilter := Value;
    xBool := (mSelectedList.Count > 0);
    ClearSelection;
    UpdateGridList(True);

    if xBool then
      fOnSelect(Self);
  end;
end;
//:-----------------------------------------------------------------------------
procedure TDBVisualStringList.UpdateGridList(aWithSelection: Boolean);
var
  i: Integer;
  xTmpList: TStringList;
  xStr: String;
  xTmpSel: TList;
  xObj: TObject;
begin
  // tempor�re Liste f�r selektierte Elemente
  xTmpSel := TList.Create;
  xTmpList := TStringList.Create;
  try
    for i:=0 to mDBValues.Count-1 do begin
      xStr := mDBValues.Strings[i];
      xObj := mDBValues.Objects[i];
      if (fFilter = '') OR (Pos(fFilter, LowerCase(xStr)) > 0) OR (xStr = fDefaultItems) then begin
        xTmpList.AddObject(xStr, xObj);
        // war diese Item in der Selektionsliste?
        if aWithSelection and IsIDSelected(Integer(xObj)) then begin
          // wenn ja, dann �bernehmen
          xTmpSel.Add(xObj);
        end;
      end;
    end;
  finally
    if aWithSelection then
      AssignSelectedList(xTmpSel);

    RowCount := xTmpList.Count;
    Cols[0].Assign(xTmpList);
    xTmpList.Free;
    xTmpSel.Free;
  end;
  Update;
end;
//:-----------------------------------------------------------------------------
function TDBVisualStringList.GetItemsCount: Integer;
begin
  if DefaultItems <> '' then
    Result := mDBValues.Count - 1
  else
    Result := mDBValues.Count;
end;
//:-----------------------------------------------------------------------------
function TDBVisualStringList.GetSelCount: Integer;
begin
  Result := mSelectedList.Count;
  if (DefaultItems <> '') and IsRowSelected(0) then
    dec(Result);
end;
//:-----------------------------------------------------------------------------
end.

