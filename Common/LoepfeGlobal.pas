{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: LoepfeGlobal.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: This unit contains common data types and definitions of both
|                 system: MillMaster Loepfe and MillMaster OE. Do not implement any
|                 independence types or constants which are not for both system.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  1.00  Wss | File created
| 01.04.1999  1.01  Wss | cDatabaseUsername/Password replaced by gDBUsername, gDBPassword
| 09.04.1999  1.02  khp | Add function SwapDWord
| 06.01.2000  1.03  Wss | cRegDefUserRight, cRegLogonUserRight remarked
| 17.05.2000  1.03  Wss | ApplicationExists changed to use Mutex
| 23.05.2000  1.03  Wss | IsOLEMode function added to determine if OLE mode or not
| 18.07.2000  1.04  SDo | func. DeleteRegValue() added
| 30.08.2000  1.04  Wss | gDomainController added for login to read groups from PDC
| 04.09.2000  1.04  Wss | MMGetComputerName function implemented
| 05.09.2000  1.04  Wss | Access  := KEY_WRITE in each SetRegXXX function implemented for
                          write acces into registry with lower user rights.
                          -> if language has changed the new language id wasn't saved to registry
| 26.10.2000  1.04  Wss | TSubSystemTyp extended with MMSetup for EventLog
| 19.12.2000  1.05  SDo | Proc. SystemErrorMsg_, UserErrorMsg, InfoMsg, WarningMsg,
|                       | CheckIntChar, CheckFloatChar from AssignComp.pas added
| 13.02.2001  1.06  Nue | cRegMMServicePath and cRegMMTexnetPath added.
| 22.03.2001  1.07  Wss | PopupMessage function implemented to display messages over messgnr service
| 30.03.2001  1.08  SDo | New Func. GetFileVersion()
| 02.04.2001  1.09  Nue | Constants cRegCurrentVersion, cRegLastUpdateVersion and cRegLastUpdateDate added.
| 05.06.2001  1.10  SDo | cRegMsgQOfflimit for the MMClient config. msg added
| 11.06.2001  1.11  SDo | cRegMillMasterRootPath added
| 02.07.2001  1.12  Wss | MMSystemErrorMsg_ moved to BaseGlobal because of preventing use of RzCSIntf in base functionality
| 14.09.2001  1.13  Wss | UserErrorMsg: call to dialog with aOwner extended
| 01.10.2001  1.14  Nue | cRegLongTimeMMWinding added.
| 12.11.2001  1.15  Wss | CodeSiteEnabled function added
| 30.11.2001  1.16  Wss | MMWeekNumber added: in parameter define the dey start of week
                          ISO: 1 = Sunday, 2 = Monday (default), etc, 7 = Saturday
| 01.07.2002  1.16  Wss | Helper function EnterMethod(aStr: String) implemented
                          For easy use of CodeSite.Enter/ExitMethod with only one line of Code
                          Types: TCodeSiteHelper, ICodeSiteHelper
| 16.07.2002  1.16  Wss | Konstante cRegHelpFilePath von HTMLHelp Datei hierher verschoben
| 18.07.2002  1.16  LOK | Neue globale Variablen:
                            - gSQLServerName: string;
                            - gDBName: string;
                          Funktion GetRegString() mit neuem Defaultparameter f�r eine Registry Instanz (Default = nil).
| 19.09.2002  1.16  Wss | GetDefaultConnectionString schaut in Registry ...\Debug nach ConnectionString
| 03.10.2002  1.16  Wss | cUSADateMask von ClassDataReader �bernommen
| 25.02.2003  1.16  Wss | Funktion GetWindowsVersion hinzugef�gt
| 30.04.2003  1.16  Wss | cAverageCharWidth = 5.5 hinzugef�gt
| 19.01.2004        Lok | Funktion mmFloatToStr hinzugef�gt
| 18.02.2004        Lok | Funktion mmFloatToStr in mmFormatFloatToStr umbenannt
|                       |   Funktionen MMStrToFloat() und MMFloatToStr() hinzugef�gt um Fliesskommazahlen in
|                       |   einem L�pfe Format (Punkt als Dezimalseparator) abzuspeichern (zB: xml).
| 19.02.2004        Wss | cUSADateMask: Zeittrennzeichen in G�nsef�schen eingefasst
| 08.04.2004        Lok | Funktion 'IsInterface()' hinzugef�gt um herauszufinden ob ein Objekt ein bestimmtes Interface unterst�tzt
| 19.04.2004        Wss | cBackSpace, cCR, cLF Konstanten eingef�gt
| 22.04.2004        Lok | Funktionen 'CopyLeft' und 'CopyRight' eingef�gt um Trennzeichenseparierte Strings
|                       | zu vereinzeln
| 03.05.2004        Wss | GetWindowsUserName eingef�gt
| 19.07.2004        Wss | LX anstelle von OE umbenannt
| 30.07.2004        Wss | VersionToInt hinzugef�gt
| 29.11.2004        Wss | MMGetComputerName: Buffer auf MAX_PATH vergr�ssert
| 20.04.2005        Lok | MMStrToFloat Threadsafe gemacht
|                       |   MMFloatToStr Threadsafe gemacht
| 24.05.2005    Wss/Lok | try...except um MMStrToFloat 
|==========================================================================================}
unit LoepfeGlobal;
{$MINENUMSIZE 2}
interface

uses
  Classes, Controls, Forms, SysUtils, Windows, IPCUnit, Messages, mmRegistry, ADOInt;


//------------------------------------------------------------------------------
// misc constant expressions
//------------------------------------------------------------------------------
const
  cUSADateMask = 'mm"/"dd"/"yyyy hh":"nn":"ss';
  cDateTime1970 = 25569.0;   // = 1. Januar 1970 00:00:00
  cAverageCharWidth = 5.5;   // Used to calculate pixel width <> char width within db fields

{
//------------------------------------------------------------------------------
// const and types for the setup
//------------------------------------------------------------------------------
type
  TComponentType = (ctNone, ctEdit, ctSpinButton, ctLabel);
  // aus MMDef.pas
  TDataType = (dtArray, dtChar, dtByte, dtShort, dtWord, dtLong, dtDWord,
               dtFloat, dtDouble, dtString, dtDate, dtTime, dtDateTime,
               dtOLEDateTime, dtKey, dtInteger);

  TClientServerType = (csServer, csClient, csClientServer, csNone); //

  TDataRegType = (drtDWord, drtString, drtBinary); // Datatyp for Registry
}

//******************************************************************************
// GLOBAL VARIABLES
//******************************************************************************
type
  TMMRunWith = (rwBARCO, rwWSC);
  TMMRunWithSet = set of TMMRunWith;
var
  gApplicationName: string;
  gMinDBVersion: String;
  gDBAliasNameODBC: string;
  gDBAliasNameNative: string;
  gDBPassword: string;
  gDBUsername: string;
  // Name of the Instance of the SQL Server (normaly the name of the Computer witch owns the SQLServer)
  gSQLServerName: string;
  gDBName: string;
  gDomainController: string;
  gMMHost: string;
  gMMRunWith: TMMRunWithSet;

//******************************************************************************
// constants and type definitions for BARCO
//******************************************************************************
const
  cFloorToolbarBmpWidth = 16;           // width of bitmap configurable in Floor
  cFloorToolbarBmpHeight = 16;          // height of bitmap configurable in Floor

//------------------------------------------------------------------------------
// access the registry of BARCO
//------------------------------------------------------------------------------
const
  // registry path
  cRegBarcoPath = '\SOFTWARE\BARCO';
  cRegBarcoAddons = cRegBarcoPath + '\Addons'; // RegPath for Floor to register Plugins
  cRegBarcoLocalePath = cRegBarcoPath + '\Locale'; // RegPath of language information

  // value names in registry
  cRegBarcoLanguage = 'Language';       // String

//------------------------------------------------------------------------------
// miscellaneos constants without any group
//------------------------------------------------------------------------------
const
//  cRegKeyLM             = HKEY_LOCAL_MACHINE;
//  cRegKeyCU             = HKEY_CURRENT_USER;
  cRegLM = HKEY_LOCAL_MACHINE;
  cRegCU = HKEY_CURRENT_USER;

  cBackSpace = #8;
  cCR        = #13;
  cLF        = #10;
  cCRLF      = #13#10;
//------------------------------------------------------------------------------
// Default values for the timehandler administrators
//------------------------------------------------------------------------------
const
  cDefaultIntervalLen = 60;             // Number of minutes for the DataEvAdmmin
  cDefaultMaxInterval = 30;             // Max. number of interval for table t_interval (DelIntAdmin)
  cDefaultTimePeriode = 1800000;        // ms for CleanUpEventAdmin ( 30 Min. )
  cDefaultDelShiftTime = 300;           // Days for DelShiftByTime Admin
//------------------------------------------------------------------------------
// Registry key for the timehandler administrators
//------------------------------------------------------------------------------
const
  // DelShiftByTime
  cDelShiftByTime = 'DaysForDelShift';
  // DataAquEv
  cRegIntervalLen = 'IntervalLen';
  // DelInt
  cRegMaxInterval = 'MaxInterval';
  //CleanUpEv
  cRegTimePeriod = 'CUpTimePeriod';

//------------------------------------------------------------------------------
// type information's for every SubSystem
//------------------------------------------------------------------------------
const
  cNumOfSubSystems = 12;

//******************************************************************************
// this are all SubSystems and Applications
//******************************************************************************
type
  TSubSystemTyp = (ssMMGuard,
    ssMMClient,
    ssJobHandler,
    ssMsgHandler,
    ssStorageHandler,
    ssTimeHandler,
    ssTXNHandler,
    ssWSCHandler,
    ssLXHandler,
    ssCIHandler,
    ssApplication,
    ssWSCRPCHandler,
    ssMMSetup);
const
  cApplicationNames: array[TSubSystemTyp] of string = (
    'MMGUARD.EXE',
    'MMCLIENT.EXE',
    'JOBHANDLER.EXE',
    'MSGHANDLER.EXE',
    'STORAGEHANDLER.EXE',
    'TIMEHANDLER.EXE',
    'TXNHANDLER.EXE',
    'WSCHANDLER.EXE',
    'LXHANDLER.EXE',
    'CIHANDLER.EXE',
    'APPLICATION.EXE',
    'WSCRPCHANDLER.EXE',
    'MMSETUP.EXE'
    );
//******************************************************************************

//------------------------------------------------------------------------------
// miscellaneos types/constants without any group
//------------------------------------------------------------------------------
type
  TLenBase = (lbAbsolute, lb100KM, lb1000KM, lb100000Y, lb1000000Y);

const
  cSecurityLevelCount = 10;             // Level 0 to 10: 0 = lowest access, 10 = highest access

//------------------------------------------------------------------------------
// any constants to access the registry of LOEPFE
//------------------------------------------------------------------------------
const
  // registry path
  cRegMillMasterPath = '\SOFTWARE\LOEPFE\MILLMASTER'; // base string
  cRegMMCommonPath = cRegMillMasterPath + '\Common';
  cRegLoepfeAddons = cRegMillMasterPath + '\Plugin\AddOns';
  cRegMMApplicationPath = cRegMillMasterPath + '\Application';
  cRegMMSecurityPath = cRegMillMasterPath + '\Security';
  cRegMMServicePath = cRegMillMasterPath + '\Service';
  cRegMMToolsPath = cRegMillMasterPath + '\Tools';
  cRegMMFloor = cRegMillMasterPath + '\Floor';
  cRegMMDebug = cRegMillMasterPath + '\Debug';
  cRegMMTimeouts = cRegMillMasterPath + '\Timeouts';
  //cRegMMtest       = '\MMGuard';
  cRegMMGuardPath = cRegMillMasterPath + '\MMGuard';
  cRegMMGuardDebugPath = cRegMMGuardPath + '\MMGuard\Debug';
  cRegMMWindowPos = cRegMillMasterPath + '\WindowPos';

  // value names in registry
  cRegDBAliasNameODBC = 'DBAliasNameODBC'; // String
  cRegDBAliasNameNative = 'DBAliasNameNative'; // String
  cRegDBPassword = 'DBPassword';        // String
  cRegDBUserName = 'DBUsername';        // String
  cRegSQLServerName = 'SQLServerName';  // String
  cRegDBName = 'DBName';                // String
  cRegDictionaryName = 'DictionaryFile'; // String
  cRegDomainController = 'DomainController'; // String
  cRegFloorDLLLanguage = 'FloorDLL%s';  // String: DLL for FloorGerman = W0407
  cRegHelpFilePath = 'HelpFilePath';       // String for TMMHtmlHelp component
  cRegMMHost = 'MMHost';                // String
  cRegMMServer = 'MMServer';
  cRegMMServerName = 'ServerName';      // String
  cRegMMUserGroupName = 'UserGroup';    // String
  cRegPrimaryLanguage = 'PrimaryLanguage'; // Number
  cRegRunBARCO = 'RunBARCO';            // Boolean
  cRegRunWSC = 'RunWSC';                // Boolean
  cRegCurrentVersion = 'CurrentVersion'; // String
  cRegLastUpdateVersion = 'LastUpdateVersion'; // String
  cRegLastUpdateDate = 'LastUpdateDate'; // Datetime (float)
  cRegMillMasterRootPath = 'MillMasterRootPath'; //String
  cRegLongTimeMMWinding = 'LongMMWinding'; // Boolean Nue:1.10.01 True=Even if MMWinding keep Data for at least 1 year

  //========================================================
  // MM-Client
  cRegDefUserName = 'DefaultUserName';  // String
  cRegDefUserGroups = 'DefaultUserGroups'; // String
  cRegDefLogonTime = 'DefaultLogonTime'; // String
  cRegMsgQOfflimit = 'MsgQOfflimit';    // Integer;

  // MM-Login
  cRegLogonUserName = 'LogonUserName';  // String
  cRegLogonUserGroups = 'LogonUserGroups'; // String
  cRegLogonTime = 'LogonTime';          // String

  // Applikationen
  cRegToolButton = 'TB';                // Registry-Eintrag der abgespeicherten UserButtons

//******************************************************************************

//------------------------------------------------------------------------------
// some const and types for implementation part
//------------------------------------------------------------------------------
const
  cDefaultConnectionString = 'Provider=SQLOLEDB.1;Data Source=%s;Initial Catalog=%s;Password=%s;User ID=%s;Auto Translate=False';
  // used with the DebugUtil and local Write procedure
  cDebugUtilMailSlotName = 'Loc_DebugUtil';
  cDebugUtilMailSlotTmo = 500;
  cEventLogMillMasterClass = 'MillMaster';
  cEventLogMillMasterDebugClass = 'MillMasterDebug';

  // default color for DBxxx components if in Extended mode and disabled (=Read Only)
  clMMReadOnlyColor = Integer(24 or $80000000);

resourcestring
  cErrorMsg = '(*)Im Programm ist ein Fehler aufgetreten, bitte benachrichtigen Sie den Systemadministrator. Fehler:'; // ivlm

type
  // define the mode some TDBxxx components are: smExtended means ReadOnly with status color clMMReadOnlyColor
  TShowMode = (smNormal, smExtended);

  STRING100 = array[1..100] of Char;

//------------------------------------------------------------------------------
// Konstanten f�r die Funktion GetWindowsVersion
const
  cWinVersionInvalid = 0;
  cWinVersionNT      = 1;
  cWinVersionW2k     = 2;
  cWinVersionXP      = 3;
//------------------------------------------------------------------------------

const
  // Dezimalseparator f�r Flieekommazahlen die im Stringformat abgespeichert werden (zB: xml)
  cLoepfeDecimalSeparator = '.';

//******************************************************************************
// PROTOTYPES
//******************************************************************************
// Checks if current application alread runs: return True if it do
function ApplicationExist(aApplication: TApplication; aShowApp: Boolean = True): Boolean;
// Decrements aValue by 1 if it is lower than aMin it set aValue to aMax
procedure CircDec(var aValue; aMin, aMax: Integer);
// Increments aValue by 1 if it is higher than aMax it set aValue to aMin
procedure CircInc(var aValue; aMin, aMax: Integer);
// Closes the handle aHandle and set aHandle to INVALID_HANDLE_VALUE
function CloseHnd(var aHandle: THandle): Boolean;

function CutStringSep(aSeparator: string; var aString, aResult: string): Boolean;

function DeleteRegValue(aPath: HKEY; aKey, aValue: string): Boolean;
function DeleteSubStr(aString, aSub: string): string;
// Formatiert den String ohne Exponenten zu verwenden
function mmFormatFloatToStr(aValue: extended; aDigits: Integer = 3): String;
(* Versucht einen String in einen Float zu verwandlen (im Loepfe Format
  entspricht ein '.' auch in anderen Lokalisierungen dem DecimalSeparator) *)
function MMStrToFloat(aValue: string): Extended;
function MMStrToFloatDef(aValue: string; aDefault: extended): Extended;
// Wandelt einen Float in das Loepfe interne Stringformat (mit '.' als Dezimalseparator)
function MMFloatToStr(aValue: extended): String;
// Formats the aError code from Windows NT into a string
function FormatErrorText(aError: DWord): string;

function MMGetComputerName: string;
function MMWeekNumber(aDate: TDateTime; aStartDay: Integer = 2): Integer;
function GetCurrentMMUser: string;
function GetWindowsUserName: String;
function GetIndexStr(aIndex: Integer; aString, aSeparator: string): string;
// Formats the GetLastError from Windows NT into a string
function GetLastErrorText: string;

// easy usable functions to read from registry with default value
function GetRegBoolean(aPath: HKEY; aKey, aValue: string; aDefault: Boolean): Boolean;
function GetRegFloat(aPath: HKEY; aKey, aValue: string; aDefault: Double = 0.0): Double;
function GetRegInteger(aPath: HKEY; aKey, aValue: string; aDefault: Integer = 0): Integer;
function GetRegString(aPath: HKEY; aKey, aValue: string; aDefault: string = ''; aRegInstance: TmmRegistry = nil): string;

// Retuns the current OS platform
// VER_PLATFORM_WIN32s	        Win32s on Windows 3.1.
// VER_PLATFORM_WIN32_WINDOWS	Win32 on Windows 95.
// VER_PLATFORM_WIN32_NT	Win32 on Windows NT.
function GetSystemPlatform: Integer;
function GetWindowsVersion: Integer;
function CodeSiteConnectionString: String;
function CodeSiteEnabled(aAppName: string = ''): Boolean;
// if application is started in OLE mode MainForm is set to hide
function IsOLEMode: Boolean;
// Pr�ft ob ein Interface unterst�tzt ist (aInterface ist das zu untersuchende Interface)
function IsInterface(aInterface: IUnknown; aIID: TGUID): boolean;

// Extracts filename of aFileName without extension
function mmExtractFileName(aFileName: string): string;

// Shortform of OutputDebugString
procedure ODS(aString: string);

// shows a popup message over the messanger service \\.\mailslot\messgnr
// returns True if success; False if failed -> GetLastError
function PopupMessage(aComputer, aFrom, aTo, aMessage: string): Boolean;

// !!!!!!!!!! this function should not be used anymore
function ReplaceSubStr(aString, aSub, aReplace: string): string;

// easy usable functions to write values to registry
procedure SetRegBoolean(aPath: HKEY; aKey, aName: string; aValue: Boolean);
procedure SetRegFloat(aPath: HKEY; aKey, aName: string; aValue: Double);
procedure SetRegInteger(aPath: HKEY; aKey, aName: string; aValue: DWord);
procedure SetRegString(aPath: HKEY; aKey, aName, aValue: string);

// Swap a DWord: Bytes 1-4, 2-3, 3-2, 4-1
function SwapDWord(aInValue: DWord): DWord;

// Writes aString to a mailslot named cDebugUtilMailslotName which supports the
// DebugUtil application
// 2.7.01, wss, removed because CodeSite shouldn't be used in global loepfe units
//function WriteDebug(aString: String; aCondition: Boolean = True): Boolean;



//..............................................................................
//procedure SystemErrorMsg_(aMsg: String; aOwner: TWinControl = Nil);
//..............................................................................
procedure UserErrorMsg(aMsg: string; aOwner: TComponent = nil);
//..............................................................................
procedure InfoMsg(aMsg: string; aOwner: TWinControl = nil);
//..............................................................................
// Wandelt einen Versionenstring z.B. Version 6.10 in eine Integerzahl um 610
function VersionToInt(aVersion: String; aFactor: Integer = 100): Integer;
//..............................................................................
procedure WarningMsg(aMsg: string; aOwner: TWinControl = nil);
//..............................................................................
procedure CheckIntChar(var aChar: Char); // aChar will be zero if it is not a Number
//..............................................................................
procedure CheckFloatChar(var aChar: Char); // aChar will be zero if it is not a Number or a decimal separator
//..............................................................................

function GetFileVersion(aFileName: string): string;

// Connection String for ADO
function GetDefaultConnectionString: string;

// Send Error Collection from ADO Command
function GetConnectionErrorMessage(aErrors: ADOInt.Errors):string;

(*-----------------------------------------------------------
  Gibt den linken Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird
  der Original String zur�ckgegeben.
  Ist 'aCut' = true, dann wird der Originalstring gek�rzt.
-------------------------------------------------------------*)
function CopyLeft(var aString: string; aSeparator: string; aCut: boolean): String;  overload;

(*-----------------------------------------------------------
  Gibt den linken Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird
  der Original String zur�ckgegeben.
-------------------------------------------------------------*)
function CopyLeft(aString: string; aSeparator: string): String; overload;

(*-----------------------------------------------------------
  Gibt den rechten Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird
  ein Leerstring zur�ckgegeben.
-------------------------------------------------------------*)
function CopyRight(aString: string; aSeparator: string): String; overload;

// Fragt ab ob eine Klasse Codesite Meldungen ausgeben soll (Ber�cksichtigt ob CS enabled ist)
function GetClassDebug(aClassName: string): Boolean;

(*---------------------------------------------------------
  �berpr�ft, ob Source in einer Variable vom Typ Target verwendet werden kann.
----------------------------------------------------------*)
function ValuesNotCompatible(aSource, aTarget: variant): Boolean;

(*---------------------------------------------------------
  Kontrolliert ob ein Variant ein nummerischer Typ ist. Ist das der Fall, dann wird
  der Variant mit MMStrToFloat gewandelt und dieser dann als Result zur�ckgegeben.
  Damit wird sichergestellt, dass ein allf�lliger Float mit dem richtigen Dezimal Separator
  ausgestattet ist (nacher vartype = vtfloat, vtsingle, ...).
----------------------------------------------------------*)
function NumVariantToFloat(const aToConvert: Variant; aRefParam: Variant): Variant;

(*---------------------------------------------------------
  Konvertiert einen Float Variant in einen String. Diese Wandlung darf
  nicht Delphi vornehmen, da im "MM" String immer ein Punkt als
  Dezimalseparator sein muss
----------------------------------------------------------*)
function FloatVariantToString(aToConvert: Variant): string;

//******************************************************************************
// IMPLEMENTATION
//******************************************************************************
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  Dialogs, mmDialogs, Math,
  {EventLog, } IvDictio {, BaseGlobal}, mmcs;
//-----------------------------------------------------------------------------

function ApplicationExist(aApplication: TApplication; aShowApp: Boolean): Boolean;
var
  xTitle: string;
  xHWnd: HWnd;
  xMuxHnd: THandle;
begin
  Result := False;
  if not (csDesigning in aApplication.ComponentState) then begin
    xTitle := aApplication.Title;
    Result := OpenMux(xMuxHnd, xTitle);
    if Result then begin
      // aktuelles Programm kurz umbenennen damit is nicht mehr gefunden wird
      xHWnd := FindWindow(nil, PChar(xTitle));
      SetWindowText(xHWnd, PChar(xTitle + '_'));
      // laeuft ein Programm mit gleichem Namen doch noch?
      xHWnd := FindWindow(nil, PChar(xTitle));
      // Gefunden: aktive machen
      if (xHWnd <> 0) and aShowApp then begin
        if IsIconic(xHWnd) then
          ShowWindow(xHWnd, SW_SHOWNORMAL)
        else
          SetForeGroundWindow(xHWnd);
      end;
    end else
      CreateMux(xMuxHnd, xTitle);
  end;
end;
//-------------------------------------------------------------------------
procedure CircDec(var aValue; aMin, aMax: Integer);
begin
  if Integer(aValue) > aMin then dec(Integer(aValue))
  else Integer(aValue) := aMax;
end;
//-------------------------------------------------------------------------
procedure CircInc(var aValue; aMin, aMax: Integer);
begin
  if Integer(aValue) < aMax then inc(Integer(aValue))
  else Integer(aValue) := aMin;
end;
//-----------------------------------------------------------------------------
function CloseHnd(var aHandle: THandle): Boolean;
begin
  Result := CloseHandle(aHandle);
  aHandle := INVALID_HANDLE_VALUE;
end;
//------------------------------------------------------------------------------
function CutStringSep(aSeparator: string; var aString, aResult: string): Boolean;
var
  xPos: Integer;
begin
  aResult := aString;
  xPos := Pos(aSeparator, aString);
  if xPos > 0 then begin
    aResult := Copy(aString, 1, xPos - 1);
    Delete(aString, 1, xPos);
  end else
    aString := '';

  Result := (aResult <> '');
end;
//------------------------------------------------------------------------------
function DeleteRegValue(aPath: HKEY; aKey, aValue: string): Boolean;
begin
  Result := False;
  with TmmRegistry.Create do try
    RootKey := aPath;
    if OpenKey(aKey, FALSE) then
      Result := DeleteValue(aValue);
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
function DeleteSubStr(aString, aSub: string): string;
var
  xPos: Integer;
begin
  Result := aString;
  xPos := Pos(aSub, Result);
  if xPos > 0 then
    Delete(Result, xPos, Length(aSub));
end;
//-----------------------------------------------------------------------------
function FormatErrorText(aError: DWord): string;
var
  xMsg: STRING100;
begin
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nil, aError, 0, @xMsg, sizeof(xMsg), nil);
  Result := IntToStr(aError) + ': ' + StrPas(@xMsg);
end;
(*-----------------------------------------------------------
  Versucht einen String in einen Float zu verwandlen (im Loepfe Format
  entspricht ein '.' auch in anderen Lokalisierungen dem DecimalSeparator)
  20.5.05: Die alte Funktion mit dem tempor�ren ersetzen des DecimalSeparators war nicht Threadsafe
-------------------------------------------------------------*)
function MMStrToFloat(aValue: string): Extended;
begin
  (* Wegen L�ndereinstellungen immer ersetzen (auch, wenn vom System her unn�tig, da
     die Exception die Konvertierungszeit um den Faktor 400 erh�ht *)
  try
    result := StrToFloat(StringReplace(aValue, cLoepfeDecimalSeparator, DecimalSeparator, []));
  except
    on e:Exception do
      raise EConvertError.CreateFmt('MMStrToFloat: aValue=%s; %s', [aValue, e.Message]);
  end;
end;// function MMStrToFloat(aValue: string): Extended;

(*-----------------------------------------------------------
  Wandelt einen Float in das Loepfe interne Stringformat (mit '.' als Dezimalseparator)
  20.4.05: Die alte Funktion mit dem tempor�ren ersetzen des DecimalSeparators war nicht Threadsafe
-------------------------------------------------------------*)
function MMFloatToStr(aValue: extended): String;
begin
  result := FloatToStr(aValue);
  if DecimalSeparator <> cLoepfeDecimalSeparator then
    result := StringReplace(result, DecimalSeparator, cLoepfeDecimalSeparator, []);
end;// function MMFloatToStr(aValue: extended): String;

//-----------------------------------------------------------------------------
function mmFormatFloatToStr(aValue: extended; aDigits: Integer = 3): String;
var
  i: Integer;
begin
  i := 1;
  if aValue <> 0 then
    i := Round(Log10(aValue) + 0.5);
    
  if i > aDigits then
    aDigits := i;
  Result := Format('%.*g', [aDigits, aValue]);
end;
//-----------------------------------------------------------------------------
function MMGetComputerName: string;
var
  xComputerName: array[0..MAX_PATH] of char;
  xCompNameSize: DWord;
begin
  xCompNameSize := MAX_PATH;
  if GetComputerName(xComputerName, xCompNameSize) then
    Result := StrPas(xComputerName)
  else
    Result := '';
end;
//-----------------------------------------------------------------------------
function MMWeekNumber(aDate: TDateTime; aStartDay: Integer = 2): Integer;
const
  cISOFirstWeekMinDays = 4;
var
  xYear, xMonth, xDay: Word;
  xWeekDay: Integer;
begin
  // key day is Thursday
  xWeekDay := ((DayOfWeek(aDate) - aStartDay + 7) mod 7) + 1;
  aDate := aDate - xWeekDay + 8 - cISOFirstWeekMinDays;
  DecodeDate(aDate, xYear, xMonth, xDay);
  Result := (Trunc(aDate - EncodeDate(xYear, 1, 1)) div 7) + 1;
end;
//-----------------------------------------------------------------------------
function GetCurrentMMUser: string;
//var
//  xUserName: array[0..100] of Char;
//  xSize: DWord;
begin
  Result := '';
  with TmmRegistry.Create do try
    Rootkey := cRegCU;
    if OpenKeyReadOnly(cRegMMSecurityPath) then begin
      if ValueExists(cRegLogonUserName) then
        Result := ReadString(cRegLogonUserName)
      else if ValueExists(cRegDefUserName) then
        Result := ReadString(cRegDefUserName);
    end;
  finally
    Free;
  end;
  // if nothing is in registry (should NOT happen) get the NT Username
  if Result = '' then begin
    Result := GetWindowsUserName;
//    xSize := 100;
//    if GetUserName(@xUserName, xSize) then
//      Result := StrPas(xUserName);
  end;
{
  Result := GetRegString(cRegCU, cRegMMSecurityPath, cRegLogonUserName, '');
  if Result = '' then begin
    xSize := 100;
    if GetUserName(@xUserName, xSize) then
      Result := StrPas(xUserName);
//    Result := GetRegString(cRegCU, cRegMMSecurityPath, cRegDefaultUserName, Result);
  end;
{}
end;
//-----------------------------------------------------------------------------
function GetWindowsUserName: String;
var
  xUserName: array[0..100] of Char;
  xSize: DWord;
begin
  xSize := 100;
  if GetUserName(@xUserName, xSize) then
    Result := StrPas(xUserName)
  else
    Result := '';
end;
//-----------------------------------------------------------------------------
function GetIndexStr(aIndex: Integer; aString, aSeparator: string): string;
var
  xPos: Integer;
begin
  Result := aString;
  repeat
    dec(aIndex);
    xPos := Pos(aSeparator, Result);
    if aIndex > 0 then begin
      Delete(Result, 1, xPos + Length(aSeparator) - 1);
    end;
  until (aIndex < 1) or (xPos = 0);

  if xPos > 0 then
    Result := Copy(Result, 1, xPos - 1);
end;
//-----------------------------------------------------------------------------
function GetLastErrorText: string;
var
  xMsg: STRING100;
  xErr: DWORD;
begin
  xErr := GetLastError;
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nil, xErr, 0, @xMsg, sizeof(xMsg), nil);
  Result := IntToStr(xErr) + ': ' + StrPas(@xMsg);
  SetLastError(xErr);
end;
//------------------------------------------------------------------------------
function GetRegBoolean(aPath: HKEY; aKey, aValue: string; aDefault: Boolean): Boolean;
begin
  Result := Boolean(GetRegInteger(aPath, aKey, aValue, Integer(aDefault)));
end;
(*---------------------------------------------------------
  Fragt ab ob eine Klasse Codesite Meldungen ausgeben soll (Ber�cksichtigt ob CS enabled ist)
----------------------------------------------------------*)
function GetClassDebug(aClassName: string): Boolean;
begin
  Result := CodeSite.Enabled and GetRegBoolean(cRegLM, cRegMMDebug, aClassName, false);
end;// function GetClassDebug(aClassName: string): Boolean;
//------------------------------------------------------------------------------
function GetRegFloat(aPath: HKEY; aKey, aValue: string; aDefault: Double): Double;
begin
  Result := aDefault;
  with TmmRegistry.Create do try
    RootKey := aPath;
    if OpenKeyReadOnly(aKey) then begin
      if ValueExists(aValue) then begin
        Result := ReadFloat(aValue);
      end;
      CloseKey;
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
function GetRegInteger(aPath: HKEY; aKey, aValue: string; aDefault: Integer): Integer;
begin
  Result := aDefault;
  with TmmRegistry.Create do try
    RootKey := aPath;
    if OpenKeyReadOnly(aKey) then begin
      if ValueExists(aValue) then begin
        Result := ReadInteger(aValue);
      end;
      CloseKey;
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
(* 18.7.02 LOK ersetzt
function GetRegString(aPath: HKEY; aKey, aValue: string; aDefault: string; aRegInstance: TRegistry): string;
begin
  with TmmRegistry.Create do try
    RootKey := aPath;
    if OpenKeyReadOnly(aKey) then
      if ValueExists(aValue) then begin
        Result := ReadString(aValue);
      end;
  finally
    Free;
  end;
end;
*)
function GetRegString(aPath: HKEY; aKey, aValue: string; aDefault: string; aRegInstance: TmmRegistry): string;
var
  xRegInstance: TmmRegistry;

  function ReadString: string;
  begin
    Result := aDefault;
    with xRegInstance do begin
      RootKey := aPath;
      if (OpenKeyReadOnly(aKey))and(ValueExists(aValue)) then begin
        Result := ReadString(aValue);
      end;// if (OpenKeyReadOnly(aKey))and(...
    end;// with xRegInstance do begin
  end;// function ReadString: string;

begin
  Result := aDefault;
  if assigned(aRegInstance) then begin
    xRegInstance := aReginstance;
    Result := ReadString;
  end else begin
    xRegInstance := TmmRegistry.Create;
    try
      Result := ReadString;
    finally
      xRegInstance.Free;
    end;// try finally
  end;// if assigned(aRegInstance) then begin
end;// function GetRegString(...

//-----------------------------------------------------------------------------
function GetSystemPlatform: Integer;
var
  xOs: TOSVersionInfo;
begin
  xOs.dwOSVersionInfoSize := sizeof(xOs);
  GetVersionEx(xOs);
  Result := xOs.dwPlatformId;
end;
//------------------------------------------------------------------------------
function GetWindowsVersion: Integer;
var
  xOs: TOSVersionInfo;
begin
  Result := cWinVersionInvalid;

  ZeroMemory(@xOs, sizeof(TOSVersionInfo));
  xOs.dwOSVersionInfoSize := SizeOf(TOSVersionInfo);
  if GetVersionEx(xOs) then begin
    if xOs.dwPlatformId = VER_PLATFORM_WIN32_NT then begin
      if xOS.dwMajorVersion <= 4 then
        Result := cWinVersionNT
      else if (xOS.dwMajorVersion = 5) and (xOS.dwMinorVersion = 0) then
        Result := cWinVersionW2k
      else if (xOS.dwMajorVersion = 5) and (xOS.dwMinorVersion = 1) then
        Result := cWinVersionXP;
    end;
  end;
end;
//------------------------------------------------------------------------------
function CodeSiteConnectionString: String;
begin
  Result := GetRegString(cRegLM, cRegMMDebug, 'CodeSiteConnection', 'Viewer');
end;
//------------------------------------------------------------------------------
function CodeSiteEnabled(aAppName: string = ''): Boolean;
begin
  if aAppName = '' then
    aAppName := gApplicationName;
  Result := GetRegBoolean(cRegLM, cRegMMDebug, aAppName, False);
end;
//------------------------------------------------------------------------------
function IsOLEMode: Boolean;
begin
  Result := not Application.ShowMainForm;
end;
//------------------------------------------------------------------------------
// Pr�ft ob ein Interface unterst�tzt ist (aInterface ist das zu untersuchende Interface)
function IsInterface(aInterface: IUnknown; aIID: TGUID): boolean;
var
  xPointer: IUnknown;   
begin
  result := false;
  if assigned(aInterface) then
    result := aInterface.QueryInterface(aIID, xPointer) = S_OK
end;// function IsInterface(aInterface: IUnknown; aIID: TGUID): boolean;
//------------------------------------------------------------------------------
function mmExtractFileName(aFileName: string): string;
var
  xPos: Integer;
begin
  xPos := Pos('.', aFileName);
  // cut extension at the end of file name
  if xPos > 0 then
    aFileName := Copy(aFileName, 0, xPos - 1);
  // extract only the file name of aFileName
  Result := ExtractFileName(aFileName);
end;
//------------------------------------------------------------------------------
procedure ODS(aString: string);
begin
  OutputDebugString(PChar(aString));
end;
//------------------------------------------------------------------------------
function PopupMessage(aComputer, aFrom, aTo, aMessage: string): Boolean;
var
  xRemotePath: string;
  xMsg: string;
  xRemoteHandle: LongWord;
  xLen: DWord;
begin
  xMsg := aFrom + #0 + aTo + #0 + aMessage;
  xRemotePath := '\\' + aComputer + '\mailslot\messngr';
  xRemoteHandle := CreateFile(PChar(xRemotePath), GENERIC_WRITE,
    FILE_SHARE_READ, nil, OPEN_ALWAYS,
    FILE_ATTRIBUTE_NORMAL, 0);

  Result := (xRemoteHandle <> INVALID_HANDLE_VALUE);
  if Result then begin
    Result := WriteFile(xRemoteHandle, Pointer(xMsg)^, Length(xMsg), xLen, nil);
    CloseHandle(xRemoteHandle);
  end;
end;
//------------------------------------------------------------------------------
function ReplaceSubStr(aString, aSub, aReplace: string): string;
begin
  Result := StringReplace(aString, aSub, aReplace, [rfReplaceAll]);
{
  Result := aString;
  xPos := Pos(aSub, Result);
  if xPos > 0 then begin
    Delete(Result, xPos, Length(aSub));
    Insert(aReplace, Result, xPos);
  end;
{}
end;
//------------------------------------------------------------------------------
procedure SetRegBoolean(aPath: HKEY; aKey, aName: string; aValue: Boolean);
begin
  with TmmRegistry.Create do try
    RootKey := aPath;
    Access := KEY_WRITE;
    if OpenKey(aKey, True) then
      WriteBool(aName, aValue);
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure SetRegFloat(aPath: HKEY; aKey, aName: string; aValue: Double);
begin
  with TmmRegistry.Create do try
    RootKey := aPath;
    Access := KEY_WRITE;
    if OpenKey(aKey, True) then
      WriteFloat(aName, aValue);
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure SetRegInteger(aPath: HKEY; aKey, aName: string; aValue: DWord);
begin
  with TmmRegistry.Create do try
    RootKey := aPath;
    Access := KEY_WRITE;
    if OpenKey(aKey, True) then
      WriteInteger(aName, aValue);
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure SetRegString(aPath: HKEY; aKey, aName, aValue: string);
begin
  with TmmRegistry.Create do try
    RootKey := aPath;
    Access := KEY_WRITE;
    if OpenKey(aKey, True) then
      WriteString(aName, aValue);
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
function SwapDWord(aInValue: DWord): DWord;
type
  pTTDWord = ^TDWord;
  TDWord = array[1..4] of Byte;
begin
  pTTDWord(@result)^[4] := pTTDWord(@aInValue)^[1];
  pTTDWord(@result)^[3] := pTTDWord(@aInValue)^[2];
  pTTDWord(@result)^[2] := pTTDWord(@aInValue)^[3];
  pTTDWord(@result)^[1] := pTTDWord(@aInValue)^[4];
end;
//-----------------------------------------------------------------------------

{
function WriteDebug(aString: String; aCondition: Boolean = True): Boolean;
begin
  Result := True;
  if aCondition then
    CodeSite.SendMsg(aString);
end;
{}
//-----------------------------------------------------------------------------
{
procedure SystemErrorMsg_(aMsg: String; aOwner: TWinControl);
//var   xEventLog : TEventLogWriter;
begin
  SysUtils.Beep;
  MMMessageDlg( Translate ( cErrorMsg ) + aMsg , mtError , [mbOk], 0);
  try
    xEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems,
                 gMMHost,   // Temp.
                 ssApplication,
                 'Assignment',
                 True);
    xEventLog.Write ( etError, aMsg );
    xEventLog.Free;
  except end;
end;
{}
//------------------------------------------------------------------------------
procedure UserErrorMsg(aMsg: string; aOwner: TComponent);
begin
  if aOwner is TWinControl then
    MMMessageDlg(aMsg, mtError, [mbOk], 0, TWinControl(aOwner))
  else
    MMMessageDlg(aMsg, mtError, [mbOk], 0, nil);
end;
//------------------------------------------------------------------------------
procedure InfoMsg(aMsg: string; aOwner: TWinControl);
begin
  MMMessageDlg(aMsg, mtInformation, [mbOk], 0, aOwner);
end;
//------------------------------------------------------------------------------
function VersionToInt(aVersion: String; aFactor: Integer = 100): Integer;
begin
  aVersion := Trim(aVersion);
  // Prefix eliminieren (BSP: 'V 6.14b' --> ' 6.14b')
  while not(aVersion[1] in ['0'..'9']) do
    System.Delete(aVersion, 1, 1);

  // Postfix eliminieren (BSP: '6.14b' --> '6.14')
  while not(aVersion[Length(aVersion)] in ['0'..'9']) do
    System.Delete(aVersion, Length(aVersion), 1);


//  while IsCharAlpha(aVersion[1]) do
//    System.Delete(aVersion, 1, 1);
//
//  // Postfix eliminieren (BSP: '6.14b' --> '6.14')
//  while IsCharAlpha(aVersion[Length(aVersion)]) do
//    System.Delete(aVersion, Length(aVersion), 1);

  Result := Trunc(MMStrToFloat(aVersion) * aFactor);
end;

//------------------------------------------------------------------------------
procedure WarningMsg(aMsg: string; aOwner: TWinControl);
begin
  MMMessageDlg(aMsg, mtWarning, [mbOk], 0);
end;
//------------------------------------------------------------------------------
procedure CheckIntChar(var aChar: Char);
var S: set of Char;
begin
  S := ['0'..'9', Char(8)];
  if not (aChar in S) then begin
    aChar := #0;
    SysUtils.Beep;
  end;
end;
//------------------------------------------------------------------------------
procedure CheckFloatChar(var aChar: Char);
begin
  if aChar <> DecimalSeparator then
    CheckIntChar(aChar);
end;
//------------------------------------------------------------------------------
function GetFileVersion(aFileName: string): string;
type
  TLocaleInfo = packed record
    LanguageID: WORD;
    CharacterSet: WORD;
  end;

var
  xFileName: PChar;
  xBuffSize: DWORD;
  xHandle: DWord;
  xBuffer: PChar;
  xFixedPointer: Pointer;
  xFixedLength: UINT;
  xLocaleInfo: TLocaleInfo;
  xQueryString: string;
  xResult: string;
begin
  Result := '';
  xFileName := PChar(aFileName);
  xBuffSize := GetFileVersionInfoSize(xFileName, xHandle);
  if xBuffSize > 0 then begin
    xBuffer := StrAlloc(xBuffSize);
    // get size of file info structure
    GetFileVersionInfo(xFileName, xHandle, xBuffSize, xBuffer);

    // get locale id
    VerQueryValue(xBuffer, '\VarFileInfo\Translation', xFixedPointer, xFixedLength);
    xLocaleInfo := TLocaleInfo(xFixedPointer^);
    // generate request string to get version information with locale id
    xQueryString := Format('\StringFileInfo\%s%s\FileVersion',
      [IntToHex(xLocaleInfo.LanguageID, 4), IntToHex(xLocaleInfo.CharacterSet, 4)]);

    // get version information with locale id
    VerQueryValue(xBuffer, PChar(xQueryString), xFixedPointer, xFixedLength);
    StrDispose(xBuffer);
    xResult := StrPas(xFixedPointer);

    //No file version available
    if xResult[1] < Chr(32) then
      Result := ''
    else
      Result := xResult;
  end;
end;


//------------------------------------------------------------------------------
{ Connection String kann optional noch ein Parameter f�r "Network Library=xxx" enthalten:
  dbnmpntw - Named Pipe
  dbmssocn - Winsock TCP/IP
  dbmsspxn - SPX/IPX
  dbmsvinn - Banyan Vines
  dbmsrpcn - Multi-Protocol (Windows RPC)}
function GetDefaultConnectionString: string;
var
  xStr: String;
begin
  xStr := GetRegString(cRegLM, cRegMMDebug, 'ConnectionString', cDefaultConnectionString);
  result := Format(xStr,[gSQLServerName,gDBName,gDBPassword,gDBUserName]);
end;// function GetDefaultConnectionString: string;

//------------------------------------------------------------------------------
{  Gibt alle Fehler die gesammelt wurden, mit detaillierteren Informationen zur�ck.
   Im Zusammenhang mit INSERT, DELETE oder UPDATE Statements kann es vorkommen, dass
   mehrere Fehler auftreten. Als Errorstring wird von der VCL aber nur der letzte Fehler
   angezeigt. Ausserdem werden sonst keine weiteren Informationen geliefert.

   Aufruf:
      GetConnectionErrorMessage(conDefault.Errors);
}
function GetConnectionErrorMessage(aErrors: ADOInt.Errors):string;
var
  i: integer;
  xMsg: string;
begin
  with aErrors do begin
    for i := 0 to Count - 1 do begin
      if i = 0 then
        xMsg := xMsg + '(';
      xMsg := xMsg + '#' + IntToStr(Item[i].Number) + cCRLF;
      xMsg := xMsg + '    ' + 'Error: ' + Item[i].Description + cCRLF;
      xMsg := xMsg + '    ' + 'Source: ' + Item[i].Source + cCRLF;
      xMsg := xMsg + '    ' + 'SQLState: ' + Item[i].SQLState + cCRLF;
      xMsg := xMsg + '    ' + 'NativeError: ' + IntToStr(Item[i].NativeError);
      if i = Count - 1 then
        xMsg := xMsg + ')'
      else
        xMsg := xMsg + cCRLF + cCRLF;
    end;// for i := 0 to Count - 1 do begin
    result := xMsg;
  end;// with aErrors do begin
end;// procedure GetConnectionErrorMessage(aErrors: ADOInt.Errors);
//------------------------------------------------------------------------------

(*-----------------------------------------------------------
  Gibt den linken Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird 
  der Original String zur�ckgegeben. 
  Ist 'aCut' = true, dann wird der Originalstring gek�rzt.
-------------------------------------------------------------*)
function CopyLeft(var aString: string; aSeparator: string; aCut: boolean): String;
var
  xPos: integer;
begin
  result := aString;
  
  // Position des Separators feststellen
  xPos := AnsiPos(aSeparator, result);
  
  // Linke Seite kopieren
  if xPos > 0 then
    result := copy(result, 1, xPos - 1);
    
  // Wenn der String gek�rzt werden soll
  if aCut then begin
    // rechte Seite hinter dem Separator zur�ckgeben
    if xPos > 0 then
      aString := copy(aString, xPos + Length(aSeparator), MAXINT)
    else
      // Ist der Separator nicht im String enthalten, dann Leerstring zur�ckgeben
      aString := '';
  end;// if aCut then begin
end;// function CopyLeft(aString: string; aSeparator: string):string;

(*-----------------------------------------------------------
  Gibt den linken Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird 
  der Original String zur�ckgegeben. 
-------------------------------------------------------------*)
function CopyLeft(aString: string; aSeparator: string): String;
var
  xPos: integer;
begin
  result := aString;
  
  // Position des Separators feststellen
  xPos := AnsiPos(aSeparator, result);
  
  // Linke Seite kopieren
  if xPos > 0 then
    result := copy(result, 1, xPos - 1);
end;// function CopyLeft(aString: string; aSeparator: string):string;

(*-----------------------------------------------------------
  Gibt den rechten Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird
  ein Leerstring zur�ckgegeben.
-------------------------------------------------------------*)
function CopyRight(aString: string; aSeparator: string): String;
var
  xPos: integer;
begin
  result := '';

  // Position des Separators feststellen
  xPos := AnsiPos(aSeparator, aString);
  
  // Rechte Seite kopieren
  if xPos > 0 then
    result := copy(aString, xPos + Length(aSeparator), MAXINT);
end;// function CopyLeft(aString: string; aSeparator: string):string;

(*-----------------------------------------------------------
  Versucht einen String in einen Float zu verwandlen (im Loepfe Format
  entspricht ein '.' auch in anderen Lokalisierungen dem DecimalSeparator)
-------------------------------------------------------------*)
function MMStrToFloatDef(aValue: string; aDefault: extended): Extended;
begin
  result := aDefault;
  try
    result := MMStrToFloat(aValue);
  except
    on e: EConvertError do;
  end;// try finally
end;// function MMStrToFloat(aValue: string): Extended;

(*---------------------------------------------------------
  �berpr�ft, ob Source in einer Variable vom Typ Target verwendet werden kann.
  Bsp:
    in einem XML Setting ist das Element 'Dia' vorhanden, aber leer (<Dia></Dia>).
    Der Aufruf von ValueDef[cXPDia, 5] sollte in diesem Fall 5 liefern. Um aber bestimmen zu
    k�nnen, dass der Wert von Dia ung�ltig ist (liefert einen Leerstring), muss bestimmt werden ob
    der Typ des "Target Variants" mit dem Resultat (Leerstring) kompatibel ist. Ist der Default
    kein Stringtyp dann muss der Variant eingestetzt werden.

    aSource ist der zu Pr�fende Variant
    aTarget ist der Defaultwert der den Typ vorgibt
----------------------------------------------------------*)
function ValuesNotCompatible(aSource, aTarget: variant): Boolean;
begin
  (* Variable ist ung�ltig, wenn Resultat NULL oder, im "NICHT String Fall"
     ein leerer String (Element vorhanden, aber leer *)
  Result := VarIsNull(aSource);
  // String separat, da der Wert $100 f�r ein SET zu gross ist
  if not((VarType(aTarget) in [varUnknown, varOleStr]) or (VarType(aTarget) = varString)) then
    if ((VarType(aSource) = varOleStr) or (VarType(aSource) = varString)) then
      result := result or (aSource = '');
end;// function ValuesNotCompatible(aSource, aTarget: variant): Boolean;

(*---------------------------------------------------------
  Kontrolliert ob ein Variant ein nummerischer Typ ist. Ist das der Fall, dann wird
  der Variant mit MMStrToFloat gewandelt und dieser dann als Result zur�ckgegeben.
  Damit wird sichergestellt, dass ein allf�lliger Float mit dem richtigen Dezimal Separator
  ausgestattet ist (nacher vartype = vtfloat, vtsingle, ...).
----------------------------------------------------------*)
function NumVariantToFloat(const aToConvert: Variant; aRefParam: Variant): Variant;
begin
  Result := aToConvert;
  if VarType(aRefParam) in [varSmallint, varInteger, varSingle, varDouble, varCurrency] then
    if not(varIsNull(aToConvert)) then
      result := MMStrToFloat(aToConvert);
end;// function ConvertNumVariantToFloat(const aToConvert: Variant; aRefParam: Variant): Variant;

(*---------------------------------------------------------
  Konvertiert einen Float Variant in einen String. Diese Wandlung darf
  nicht Delphi vornehmen, da im "MM" String immer ein Punkt als
  Dezimalseparator sein muss
----------------------------------------------------------*)
function FloatVariantToString(aToConvert: Variant): string;
begin
  result := '';
  if not(VarISNull(aToConvert)) then
    Result := aToConvert;

  if VarType(aToConvert) in [varSingle, varDouble, varCurrency] then
    result := MMFloatToStr(aToConvert);
end;// function FloatVariantToString(aToConvert: Variant): string;

//******************************************************************************
// INITIALIZING PART OF THIS UNIT
//******************************************************************************
var
  lRegInstance: TmmRegistry;
begin
  lRegInstance := TmmRegistry.Create;
  try
    gApplicationName := 'Undefined';
    gMinDBVersion    := '0.00.00';
    gDBAliasNameODBC := GetRegString(cRegLM, cRegMMCommonPath, cRegDBAliasNameODBC, 'MM_WindingODBC',lRegInstance);
    gDBAliasNameNative := GetRegString(cRegLM, cRegMMCommonPath, cRegDBAliasNameNative, 'MM_WindingNative',lRegInstance);
    gDBPassword := GetRegString(cRegLM, cRegMMCommonPath, cRegDBPassword, 'netpmek32',lRegInstance);
    gDBUsername := GetRegString(cRegLM, cRegMMCommonPath, cRegDBUsername, 'MMSystemSQL',lRegInstance);
    gMMHost := GetRegString(cRegLM, cRegMMCommonPath, cRegMMHost, 'WETSRVBDE',lRegInstance);
    gDomainController := GetRegString(cRegLM, cRegMMCommonPath, cRegDomainController, gMMHost,lRegInstance);

    gSQLServerName := GetRegString(cRegLM, cRegMMCommonPath, cRegSQLServerName, gMMHost,lRegInstance);
    gDBName := GetRegString(cRegLM, cRegMMCommonPath, cRegDBName, 'MM_Winding',lRegInstance);

    // get with which other company MillMaster runs
    gMMRunWith := [];
  finally
    lRegInstance.Free;
  end;// try finally
{
  if GetRegBoolean(cRegLM, cRegMMFloor, cRegRunBARCO, False) then
    Include(gMMRunWith, rwBARCO);
  if GetRegBoolean(cRegLM, cRegMMFloor, cRegRunWSC, False) then
    Include(gMMRunWith, rwWSC);
{}
end.

