(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: IPCUnit.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: InterProcess Communication
| Process(es)...: -
| Description...: Beinhaltet diverse Funktionen fuer die Kommunikation mit NamedPipes
|
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 2.01
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 25.06.1998 1.00  Wss | File created
| 15.04.1999 1.01  Wss | CreateNPipe: parameter changed (aReadSize, aWriteSize)
|                        ConnectNPipe: ERROR_PIPE_CONNECTED isn't an error -> result = TRUE
| 19.09.2000 1.13  Wss | client side of NamedPipe the overlapped method is optionally available
                         for timeouted write in BaseSystem
|=========================================================================================*)
unit IPCUnit;
interface
uses
  SysUtils, Windows ;
//-----------------------------------------------------------------------------
//const
//  cPipeConnectWaitTmo = 1000;
//  cReadDelay          = 5;
//  cReadTmo            = 2000;
//  cWriteDelay         = 10;
//  cWriteTmo           = 1000;
//-----------------------------------------------------------------------------
type
//  TIPCType = (MAILSLOT, NAMEDPIPE);
  eOpenMode = (omRead, omWrite, omReadWrite);
  ePipeMode = (pmMessage, pmByte);

  PMMOverlapped = ^TMMOverlapped;
  TMMOverlapped = record
    Timeout: DWord;
    Overl: TOverlapped;
  end;

//-----------------------------------------------------------------------------
function ClientConnectNamedPipe(var aHnd: THandle; aName: String; aOpenMode: eOpenMode; aTimeOut: Integer; aOverlapped: Boolean = False): Boolean;
  (*----------------------------------------------------------------------------
  | Description: Erstellt einen Handle fuer den Zugriff auf die Clientseite einer NamedPipe
  | Input......: aName = Pipename mit vollstaendigem Pfad (z.B '\\.\pipe\pipename')
  |              aTmo  = Wartezeit, bis Versuch abgebrochen wird
  |              aNoWait = TRUE, wenn Pipe den State PIPE_NOWAIT erhalten soll
  | Output.....: aHnd = Handle von der erstellten NamedPipe
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError(), GetLastErrorText()
  |---------------------------------------------------------------------------*)

function ConnectNPipe(aHnd: THandle): Boolean;
  (*----------------------------------------------------------------------------
  | Description:
  | Input......:
  |
  |
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)

function CreateIPCFile(var aHnd: THandle; aName: String; aOpenMode: eOpenMode; aOverlapped: Boolean = False): Boolean;
  (*----------------------------------------------------------------------------
  | Description:
  |
  | Input......: siehe Parameterliste: aName = nur Pipename (ohne '\pipe\')
  | Output.....: Handle aHnd von der erstellten NamedPipe
  | Info.......: TRUE, wenn erfolgreich
  |              FALSE: ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)

function CreateNPipe(var aHnd: THandle; aName: String; aOpenMode: eOpenMode;
                     aPipeMode: ePipeMode; aReadSize, aWriteSize: DWord; aTimeout: DWord): Boolean;
  (*----------------------------------------------------------------------------
  | Description:
  | Input......:
  |
  |
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)

function ReadIPC(aHnd: THandle; aBuf: PByte; aMaxSize: DWord; var aCount: DWord): Boolean;
  (*----------------------------------------------------------------------------
  | Description: Versucht eine Anzahl Bytes von einem Handle zu lesen
  | Input......: aHnd = Handle der Pipe
  |              aBuf = Pointer auf einen Speicherbereich fuer die Meldung
  |              aMsgSz = Anzahl der Bytes, die zu erwarten sind
  | Output.....: aMsgSz = Anzahl der gelesenen Bytes
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)

function SetNPipeWaitState(aHnd: THandle; aWait: Boolean): Boolean;
  (*----------------------------------------------------------------------------
  | Description:
  | Input......:
  |
  |
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)

function WriteIPC(aHnd: THandle; aBuffer: PByte; aSize: DWord; aOverl: PMMOverlapped = Nil): Boolean;
  (*----------------------------------------------------------------------------
  | Description: Versucht eine Anzahl Bytes in einen Handle zu schreiben
  | Input......: aHnd = Handle der Pipe
  |              aBuf = Pointer auf einen Speicherbereich fuer die Meldung
  |              aMsgSz = Anzahl der Bytes, die zu erwarten sind
  |                       0 = es wird nur ein mal Versucht
  |              aTmo = Zeit, in der Versucht wird die Meldung zu schreiben
  |              aDelay = Wartezeit zwischen 2 Schreibversuche
  | Output.....: -
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)



function CreateMux(var aHandle: THandle; aName: String): Boolean;
  (*----------------------------------------------------------------------------
  | Description:
  | Input......:
  |
  |
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)
function FreeMux(aHandle: THandle): Boolean;
  (*----------------------------------------------------------------------------
  | Description:
  | Input......:
  |
  |
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)
function GetMux(aHandle: THandle; aTimeOut: DWord): Boolean;
  (*----------------------------------------------------------------------------
  | Description:
  | Input......:
  |
  |
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)
function OpenMux(var aHandle: THandle; aName: String): Boolean;
  (*----------------------------------------------------------------------------
  | Description:
  | Input......:
  |
  |
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)


function CreateEvent(var aHandle: THandle; aName: String; aManualReset, aInitialState : boolean ): Boolean;
  (*----------------------------------------------------------------------------
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)
function WaitEvent(aHandle: THandle; aTimeOut : DWord ): Boolean;
  (*----------------------------------------------------------------------------
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)
function SetEvent( aHandle: THandle ): Boolean;
  (*----------------------------------------------------------------------------
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)
function ResetEvent( aHandle: THandle ): Boolean;
  (*----------------------------------------------------------------------------
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)
function PulseEvent( aHandle: THandle ): Boolean;
  (*----------------------------------------------------------------------------
  | Output.....:
  | Info.......: TRUE = wenn erfolgreich
  |              FALSE = ErrorInfo ueber GetLastError, GetLastErrorText
  |---------------------------------------------------------------------------*)


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  LoepfeGlobal;
//-----------------------------------------------------------------------------
// Implementation for NamedPipes
//-----------------------------------------------------------------------------
function ClientConnectNamedPipe(var aHnd: THandle; aName: String; aOpenMode: eOpenMode; aTimeOut: Integer; aOverlapped: Boolean): Boolean;
begin
  Result := False;
  if WaitNamedPipe(PChar(aName), aTimeOut) then
    Result := CreateIPCFile(aHnd, aName, aOpenMode, aOverlapped);
end;
//-----------------------------------------------------------------------------
function ConnectNPipe(aHnd: THandle): Boolean;
var
  xError: DWord;
begin
  Result := ConnectNamedPipe(aHnd, Nil);
  if not Result then begin
    xError := GetLastError;
    Result := (xError = ERROR_PIPE_CONNECTED);
    if not Result then
      SetLastError(xError);
  end;
end;
//-----------------------------------------------------------------------------
function CreateIPCFile(var aHnd: THandle; aName: String; aOpenMode: eOpenMode; aOverlapped: Boolean): Boolean;
var
  xAccess, xShareMode, xAttribute: DWord;
begin
  case aOpenMode of
    omRead: begin
        xAccess := GENERIC_READ;
        xShareMode := FILE_SHARE_WRITE;  // allows to server side to write
      end;
    omWrite: begin
        xAccess := GENERIC_WRITE;
        xShareMode := FILE_SHARE_READ;    // allows to server side to read
      end;
  else // omReadWrite
    xAccess    := GENERIC_READ or GENERIC_WRITE;
    xShareMode := FILE_SHARE_READ or FILE_SHARE_WRITE; // allows to server side to read and write
  end;

  xAttribute := FILE_ATTRIBUTE_NORMAL;
  if aOverlapped then
    xAttribute := xAttribute or FILE_FLAG_OVERLAPPED;

  aHnd := CreateFile(PChar(aName),
                     xAccess,                     // AccessMode
                     xShareMode,                  // ShareMode
                     Nil,                         // SecurityAttributes
                     OPEN_EXISTING,               // CreationDistribution
                     xAttribute,                  // Flags and Attributes
                     0);                       // TemplateFile

  Result := (aHnd <> INVALID_HANDLE_VALUE);
end;
//-----------------------------------------------------------------------------
function CreateNPipe(var aHnd: THandle; aName: String; aOpenMode: eOpenMode;
                     aPipeMode: ePipeMode; aReadSize, aWriteSize: DWord; aTimeout: DWord): Boolean;
var
  xOpenMode,
  xPipeMode,
  xInSize, xOutSize: DWord;
  xName: String;
  xSecurity : TSecurityAttributes;
begin
  xName := '\\.\pipe\' + aName;

  xSecurity.lpSecurityDescriptor := Nil;
  xSecurity.bInheritHandle := true;
  xSecurity.nLength := sizeof ( xSecurity );

  // define OpenMode attributes and In/Out buffer size
  xInSize := 0; xOutSize := 0;
  case aOpenMode of
    omRead: begin
        xOpenMode := PIPE_ACCESS_INBOUND;
        xInSize   := aReadSize;
      end;
    omWrite: begin
        xOpenMode := PIPE_ACCESS_OUTBOUND;
        xOutSize  := aWriteSize;
      end;
  else // omReadWrite
    xOpenMode := PIPE_ACCESS_DUPLEX;
    xInSize   := aReadSize;
    xOutSize  := aWriteSize;
  end;
  xOpenMode := xOpenMode or WRITE_DAC;

  // define PipeMode attributes
  case aPipeMode of
    pmByte:
      xPipeMode := PIPE_TYPE_BYTE or PIPE_READMODE_BYTE;
  else
    // pmMessage
    xPipeMode := PIPE_TYPE_MESSAGE or PIPE_READMODE_MESSAGE;
  end;
  // should this pipe in the WAIT or NOWAIT mode
  if aTimeout = 0 then
    xPipeMode := (xPipeMode or PIPE_NOWAIT)
  else begin
    xPipeMode := xPipeMode or PIPE_WAIT;
  end;

  aHnd := CreateNamedPipe(PChar(xName),
                          xOpenMode,
                          xPipeMode,
                          PIPE_UNLIMITED_INSTANCES,
                          xOutSize,                   // OutBufferSize
                          xInSize,                    // InBufferSize
                          aTimeout,                   // Default Timeout
                          @xSecurity);                // Security Attributes

  Result := (aHnd <> INVALID_HANDLE_VALUE);
end;
//-----------------------------------------------------------------------------
function ReadIPC(aHnd: THandle; aBuf: PByte; aMaxSize: DWord; var aCount: DWord): Boolean;
begin
  Result := ReadFile(aHnd, aBuf^, aMaxSize, aCount, Nil);
end;
//-----------------------------------------------------------------------------
function SetNPipeWaitState(aHnd: THandle; aWait: Boolean): Boolean;
var
  xMode: DWord;
begin
  if aWait then xMode := PIPE_WAIT
           else xMode := PIPE_NOWAIT;
  Result := SetNamedPipeHandleState(aHnd, xMode, Nil, Nil);
end;
//-----------------------------------------------------------------------------
function WriteIPC(aHnd: THandle; aBuffer: PByte; aSize: DWord; aOverl: PMMOverlapped): Boolean;
var
  xCnt: DWord;
  xRes: Integer;
begin
  if Assigned(aOverl) then begin
    WriteFile(aHnd, aBuffer^, aSize, xCnt, @aOverl^.Overl);
    xRes := WaitForSingleObject(aOverl^.Overl.hEvent, aOverl^.Timeout);
    Result := (xRes = WAIT_OBJECT_0);
    if not Result then
      CancelIo(aHnd);
  end else
    Result := WriteFile(aHnd, aBuffer^, aSize, xCnt, Nil);
end;

//-----------------------------------------------------------------------------
// Implementation for Mutexes
//-----------------------------------------------------------------------------
function CreateMux(var aHandle: THandle; aName: String): Boolean;
begin
  aHandle := CreateMutex(Nil, False, PChar(aName));
  Result := (aHandle <> 0);
end;
//-----------------------------------------------------------------------------
function FreeMux(aHandle: THandle): Boolean;
begin
  Result := ReleaseMutex(aHandle);
end;
//-----------------------------------------------------------------------------
function GetMux(aHandle: THandle; aTimeOut: DWord): Boolean;
var
  xState: DWord;
begin
  xState := WaitForSingleObject(aHandle, aTimeOut);
  if xState = WAIT_TIMEOUT then
    SetLastError(ERROR_SEM_TIMEOUT);
  Result := (xState = 0);
end;
//-----------------------------------------------------------------------------
function OpenMux(var aHandle: THandle; aName: String): Boolean;
begin
  aHandle := OpenMutex(SYNCHRONIZE, False, PChar(aName));
  Result := (aHandle <> 0);
end;
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Implementation for Events
//-----------------------------------------------------------------------------
function CreateEvent(var aHandle: THandle; aName: String; aManualReset, aInitialState : boolean ): Boolean;
begin
  aHandle := Windows.CreateEvent ( Nil, aManualReset,aInitialState, PChar(aName));
  Result := (aHandle <> 0);
end;
//------------------------------------------------------------------------------
function WaitEvent(aHandle: THandle; aTimeOut : DWord ): Boolean;
var
  xState: DWord;
begin
  xState := WaitForSingleObject(aHandle, aTimeOut);
  if xState = WAIT_TIMEOUT then
    SetLastError(ERROR_SEM_TIMEOUT);
  Result := (xState = 0);
end;
//------------------------------------------------------------------------------
function SetEvent( aHandle: THandle ): Boolean;
begin
  Result := Windows.SetEvent ( aHandle );
end;
//------------------------------------------------------------------------------
function ResetEvent( aHandle: THandle ): Boolean;
begin
  Result := Windows.ResetEvent ( aHandle );
end;
//------------------------------------------------------------------------------
function PulseEvent( aHandle: THandle ): Boolean;
begin
  Result := Windows.PulseEvent ( aHandle );
end;
//------------------------------------------------------------------------------
end.

