unit regMMSecurity;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R MMSecurity.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf, Controls,
  MMSecurity;
//------------------------------------------------------------------------------
type
  TMMSecurityNameProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;
//------------------------------------------------------------------------------
procedure MMRegister;
begin
  // Unit MMSecurity
  RegisterComponents('LOEPFE', [TMMSecurityDB,
                                TMMSecurityControl]);

  // property editor for TMMSecurityControl
  RegisterPropertyEditor(TypeInfo(String), TMMSecurityControl, 'MMSecurityName', TMMSecurityNameProperty);
end;

//------------------------------------------------------------------------------
// TMMSecurityNameProperty
//------------------------------------------------------------------------------
function TMMSecurityNameProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList, paRevertable];
end;
//------------------------------------------------------------------------------
procedure TMMSecurityNameProperty.GetValues(Proc: TGetStrProc);
var
  i: Integer;
begin
  with gMMSecurityNames do
    for i:=0 to Count-1 do
      Proc(TControl(Items[i]).Name);
end;
//------------------------------------------------------------------------------
end.
