unit Repitmob_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 17.Jul.2001 08:46:46 from Type Library described below.

// ************************************************************************ //
// Type Lib: V:\mmLibrary5\Global\repitmob.tlb (1)
// IID\LCID: {A5E2A141-C84A-11CF-86D8-00608C71B320}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\StdOle2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// Errors:
//   Hint: Member 'Type' of 'IExtReportItem' changed to 'Type_'
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  RepitmobMajorVersion = 2;
  RepitmobMinorVersion = 0;

  LIBID_Repitmob: TGUID = '{A5E2A141-C84A-11CF-86D8-00608C71B320}';

  IID_IRequestInfo: TGUID = '{F7A1C3E0-80C9-11D0-8CA8-0000C088D00B}';
  IID_IDataNotify: TGUID = '{152F16C1-6549-11D0-99F6-0000C088D00B}';
  IID_IRequestInfoEx: TGUID = '{784F3632-C937-11D1-8E45-0060080C772F}';
  IID_IBarcoRequestInfo: TGUID = '{222CD8D0-DAC5-11D2-8F7D-0060080C772F}';
  IID_IBarcoDataNotify: TGUID = '{E1017130-DAC5-11D2-8F7D-0060080C772F}';
  IID_IBarcoDataNotifyEx: TGUID = '{083ED670-817A-11D3-9031-0060080C772F}';
  IID_IExtraSel: TGUID = '{112029D6-4EBC-11D1-9171-0060080C772F}';
  IID_IReportItemSetup: TGUID = '{10DD4CF0-C9D6-11CF-86DA-00608C71B320}';
  IID_IBarcoReportItemSetup: TGUID = '{D8741E90-8891-11D3-903A-0060080C772F}';
  IID_IExtReportItem: TGUID = '{E3BDADC6-5A48-11D0-99F2-0000C088D00B}';
  IID_IFormat: TGUID = '{9CF18040-4EA8-11D1-9171-0060080C772F}';
  IID_INotify: TGUID = '{53AD5A20-4E6D-11CF-86DA-00608C71B320}';
  IID_IDeleteInfo: TGUID = '{C8141752-E96F-11D1-8E6B-0060080C772F}';
  IID_IConvert: TGUID = '{BD60E355-6D24-11D0-99FF-0000C088D00B}';
  IID_INotifySrc: TGUID = '{A7B3F570-4E8D-11CF-86DA-00608C71B320}';
  IID_INotifySrc2: TGUID = '{36439AC0-12FC-11D1-9B05-0060080C772F}';
  IID_IBarcoDictionaryQuery: TGUID = '{34A79280-E5C9-11D2-8F86-0060080C772F}';
  IID_IUserStats: TGUID = '{BD69EF70-1479-11D1-9B07-0060080C772F}';
  IID_IBarcoRPIStream: TGUID = '{B8BEB670-37A5-11D4-B741-0050DA734520}';
  IID_IBarcoCalcFunc: TGUID = '{FCC4E79E-FCFA-11D4-8ABC-0050DA11E5DF}';
  CLASS_REPORTITEMSETUP: TGUID = '{18DC9CD1-8B00-11D0-9A2C-0000C088D00B}';
  IID_iCategory: TGUID = '{1AE57610-8BD0-11D0-9A2D-0000C088D00B}';
  IID_IBarcoCategory: TGUID = '{A371E020-B620-11D4-91C1-000629F9D283}';
  CLASS_Category: TGUID = '{4CA5F496-BAED-11D0-9A79-0000C088D00B}';
  IID_IBarcoExtReportItem: TGUID = '{A814CF10-5959-11D3-8FEA-0060080C772F}';
  IID_ICalc: TGUID = '{85275621-6939-11D0-99FA-0000C088D00B}';
  IID_ICalcEngine: TGUID = '{407A9EF8-29D9-11D1-9155-0060080C772F}';
  CLASS_REPORTITEM: TGUID = '{CD695352-5CA9-11D0-99F3-0000C088D00B}';
  IID_ICalcList: TGUID = '{963AD14E-29DB-11D1-9155-0060080C772F}';
  IID_IBarcoReportItemRequest: TGUID = '{4E567760-DAEF-11D2-8F7D-0060080C772F}';
  IID_IReportItemRequest: TGUID = '{E3BDADC8-5A48-11D0-99F2-0000C088D00B}';
  IID_IBarcoFilter: TGUID = '{130FA090-DAC4-11D2-8F7D-0060080C772F}';
  IID_IFilterTest: TGUID = '{24E4C030-E623-11D0-9AAE-0000C088D00B}';
  IID_IBarcoFilterTest: TGUID = '{7091AD00-9B65-11D3-B8F3-006008515DBF}';
  IID_IBarcoKeyList: TGUID = '{18F14432-02EB-11D3-8F9F-0060080C772F}';
  IID_IMachineSelectionList: TGUID = '{6A22F030-E4B3-11D0-9AAD-0000C088D00B}';
  IID_IBarcoMachineSelectionList: TGUID = '{19AB9170-7580-11D3-901E-0060080C772F}';
  IID_IBarcoSectionList: TGUID = '{6EEBF830-7A67-11D3-9027-0060080C772F}';
  IID_ISummary: TGUID = '{9C95D441-704B-11D0-9A04-0000C088D00B}';
  IID_IFilters: TGUID = '{2AAD5360-E622-11D0-9AAE-0000C088D00B}';
  CLASS_REPORTITEMREQUEST: TGUID = '{7B849440-7471-11D0-9A0B-0000C088D00B}';
  IID_IRPIComCreator: TGUID = '{BDFAA765-E2D0-11D2-8F84-0060080C772F}';
  CLASS_RPICOMCREATOR: TGUID = '{BDFAA766-E2D0-11D2-8F84-0060080C772F}';
  IID_IBarcoDataDictionary: TGUID = '{604412A0-D615-11D2-8F77-0060080C772F}';
  IID_IBarcoDDMaster: TGUID = '{255BF510-0AD1-11D4-910C-0050DA734520}';
  IID_IExtraSelEx: TGUID = '{EF19B620-D623-11D2-8F77-0060080C772F}';
  CLASS_BarcoDataDictionary: TGUID = '{96729639-ED87-11D2-8F8C-0060080C772F}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum __MIDL___MIDL_itf_repitmob_0283_0001
type
  __MIDL___MIDL_itf_repitmob_0283_0001 = TOleEnum;
const
  ID_MC = $00000001;
  ID_STOP_GROUP = $00000002;
  ID_MC_STATUS = $00000003;
  ID_STATUS_TEXTS = $00000004;
  ID_SPD_TREND = $00000005;
  ID_SHIFT_START = $0000001A;
  ID_SHIFT_END = $0000001B;
  NFIXED_IDS = $00000032;

// Constants for enum __MIDL___MIDL_itf_repitmob_0283_0002
type
  __MIDL___MIDL_itf_repitmob_0283_0002 = TOleEnum;
const
  RPI_ALL_RECORDS = $FFFFFFFF;

// Constants for enum __MIDL___MIDL_itf_repitmob_0283_0003
type
  __MIDL___MIDL_itf_repitmob_0283_0003 = TOleEnum;
const
  RPI_TEXT_ITEMS = $00000001;
  RPI_NUMERIC_ITEMS = $00000002;
  RPI_TIME_ITEMS = $00000004;
  RPI_CALC_ITEMS = $00000008;
  RPI_ALL_BASE_ITEMS = $00000007;
  RPI_ALL_ITEMS = $0000000F;
  RPI_KEY_ITEMS = $00008000;

// Constants for enum __MIDL_IFormat_0001
type
  __MIDL_IFormat_0001 = TOleEnum;
const
  LEFT = $00000000;
  RIGHT = $00000001;

// Constants for enum __MIDL_IExtReportItem_0001
type
  __MIDL_IExtReportItem_0001 = TOleEnum;
const
  unassignedType = $00000000;
  textType = $00000001;
  numericType = $00000002;
  timeType = $00000003;
  numericCalcType = $00000004;
  unavailableType = $00000005;

// Constants for enum __MIDL_IBarcoExtReportItem_0001
type
  __MIDL_IBarcoExtReportItem_0001 = TOleEnum;
const
  dbRealTime = $00000000;
  dbArchivedStore = $00000001;
  dbFromBoth = $00000002;

// Constants for enum __MIDL_IBarcoFilterTest_0001
type
  __MIDL_IBarcoFilterTest_0001 = TOleEnum;
const
  filterTest_OtherType = $00000000;
  filterTest_DateType = $00000001;
  filterTest_TimeType = $00000002;
  filterTest_DateTimeType = $00000003;

// Constants for enum __MIDL___MIDL_itf_repitmob_0220_0001
type
  __MIDL___MIDL_itf_repitmob_0220_0001 = TOleEnum;
const
  BDD_TEXT_LONG = $00000000;
  BDD_TEXT_SHORT = $00000001;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IRequestInfo = interface;
  IRequestInfoDisp = dispinterface;
  IDataNotify = interface;
  IDataNotifyDisp = dispinterface;
  IRequestInfoEx = interface;
  IRequestInfoExDisp = dispinterface;
  IBarcoRequestInfo = interface;
  IBarcoRequestInfoDisp = dispinterface;
  IBarcoDataNotify = interface;
  IBarcoDataNotifyDisp = dispinterface;
  IBarcoDataNotifyEx = interface;
  IBarcoDataNotifyExDisp = dispinterface;
  IExtraSel = interface;
  IExtraSelDisp = dispinterface;
  IReportItemSetup = interface;
  IReportItemSetupDisp = dispinterface;
  IBarcoReportItemSetup = interface;
  IBarcoReportItemSetupDisp = dispinterface;
  IExtReportItem = interface;
  IExtReportItemDisp = dispinterface;
  IFormat = interface;
  IFormatDisp = dispinterface;
  INotify = interface;
  INotifyDisp = dispinterface;
  IDeleteInfo = interface;
  IDeleteInfoDisp = dispinterface;
  IConvert = interface;
  IConvertDisp = dispinterface;
  INotifySrc = interface;
  INotifySrcDisp = dispinterface;
  INotifySrc2 = interface;
  INotifySrc2Disp = dispinterface;
  IBarcoDictionaryQuery = interface;
  IUserStats = interface;
  IUserStatsDisp = dispinterface;
  IBarcoRPIStream = interface;
  IBarcoRPIStreamDisp = dispinterface;
  IBarcoCalcFunc = interface;
  IBarcoCalcFuncDisp = dispinterface;
  iCategory = interface;
  iCategoryDisp = dispinterface;
  IBarcoCategory = interface;
  IBarcoCategoryDisp = dispinterface;
  IBarcoExtReportItem = interface;
  IBarcoExtReportItemDisp = dispinterface;
  ICalc = interface;
  ICalcDisp = dispinterface;
  ICalcEngine = interface;
  ICalcEngineDisp = dispinterface;
  ICalcList = interface;
  ICalcListDisp = dispinterface;
  IBarcoReportItemRequest = interface;
  IBarcoReportItemRequestDisp = dispinterface;
  IReportItemRequest = interface;
  IReportItemRequestDisp = dispinterface;
  IBarcoFilter = interface;
  IBarcoFilterDisp = dispinterface;
  IFilterTest = interface;
  IFilterTestDisp = dispinterface;
  IBarcoFilterTest = interface;
  IBarcoFilterTestDisp = dispinterface;
  IBarcoKeyList = interface;
  IBarcoKeyListDisp = dispinterface;
  IMachineSelectionList = interface;
  IMachineSelectionListDisp = dispinterface;
  IBarcoMachineSelectionList = interface;
  IBarcoMachineSelectionListDisp = dispinterface;
  IBarcoSectionList = interface;
  IBarcoSectionListDisp = dispinterface;
  ISummary = interface;
  ISummaryDisp = dispinterface;
  IFilters = interface;
  IFiltersDisp = dispinterface;
  IRPIComCreator = interface;
  IRPIComCreatorDisp = dispinterface;
  IBarcoDataDictionary = interface;
  IBarcoDataDictionaryDisp = dispinterface;
  IBarcoDDMaster = interface;
  IBarcoDDMasterDisp = dispinterface;
  IExtraSelEx = interface;
  IExtraSelExDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  REPORTITEMSETUP = IReportItemSetup;
  Category = iCategory;
  REPORTITEM = IExtReportItem;
  REPORTITEMREQUEST = IBarcoReportItemRequest;
  RPICOMCREATOR = IRPIComCreator;
  BarcoDataDictionary = IBarcoDataDictionary;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PUserType1 = ^TGUID; {*}
  PUserType2 = ^ID_N_SUBITM; {*}
  PUserType3 = ^DID_N_DBBINDING; {*}
  PUserType4 = ^ID2_DID_R_FORMULA; {*}
  PSYSINT1 = ^SYSINT; {*}

  REQUEST_TYPE = __MIDL___MIDL_itf_repitmob_0283_0002; 
  CATTYPE_QUERY_MASK = __MIDL___MIDL_itf_repitmob_0283_0003; 
  JUSTIFY = __MIDL_IFormat_0001; 
  CATTYPE = __MIDL_IExtReportItem_0001; 

  __MIDL___MIDL_itf_repitmob_0259_0003 = packed record
    nID: LongWord;
    wSubItmNo: Word;
  end;

  ID_N_SUBITM = __MIDL___MIDL_itf_repitmob_0259_0003; 

  _tagDID = packed record
    wDID: Word;
    wSubDIDNo: Word;
  end;

  __MIDL___MIDL_itf_repitmob_0259_0001 = packed record
    did: _tagDID;
    byDBBinding: Byte;
  end;

  DID_N_DBBINDING = __MIDL___MIDL_itf_repitmob_0259_0001; 

  __MIDL___MIDL_itf_repitmob_0259_0002 = packed record
    nID: LongWord;
    did: _tagDID;
    bstrFormula: WideString;
  end;

  ID2_DID_R_FORMULA = __MIDL___MIDL_itf_repitmob_0259_0002; 
  DBSOURCE = __MIDL_IBarcoExtReportItem_0001; 
  FILTERTEST_ENUMTIME = __MIDL_IBarcoFilterTest_0001; 
  BARCO_TEXTTYPE = __MIDL___MIDL_itf_repitmob_0220_0001; 

// *********************************************************************//
// Interface: IRequestInfo
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F7A1C3E0-80C9-11D0-8CA8-0000C088D00B}
// *********************************************************************//
  IRequestInfo = interface(IDispatch)
    ['{F7A1C3E0-80C9-11D0-8CA8-0000C088D00B}']
    function  GetNumberOfItems: Smallint; safecall;
    function  GetIDs(nItemNo: Smallint; out nCompareID: Integer; out wSubItmNo: Smallint; 
                     out pbstrName: WideString): Integer; safecall;
    function  GetDataNotify: IDataNotify; safecall;
  end;

// *********************************************************************//
// DispIntf:  IRequestInfoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F7A1C3E0-80C9-11D0-8CA8-0000C088D00B}
// *********************************************************************//
  IRequestInfoDisp = dispinterface
    ['{F7A1C3E0-80C9-11D0-8CA8-0000C088D00B}']
    function  GetNumberOfItems: Smallint; dispid 1;
    function  GetIDs(nItemNo: Smallint; out nCompareID: Integer; out wSubItmNo: Smallint; 
                     out pbstrName: WideString): Integer; dispid 2;
    function  GetDataNotify: IDataNotify; dispid 3;
  end;

// *********************************************************************//
// Interface: IDataNotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {152F16C1-6549-11D0-99F6-0000C088D00B}
// *********************************************************************//
  IDataNotify = interface(IDispatch)
    ['{152F16C1-6549-11D0-99F6-0000C088D00B}']
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; pvItems: PSafeArray); safecall;
  end;

// *********************************************************************//
// DispIntf:  IDataNotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {152F16C1-6549-11D0-99F6-0000C088D00B}
// *********************************************************************//
  IDataNotifyDisp = dispinterface
    ['{152F16C1-6549-11D0-99F6-0000C088D00B}']
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; pvItems: {??PSafeArray} OleVariant); dispid 1;
  end;

// *********************************************************************//
// Interface: IRequestInfoEx
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {784F3632-C937-11D1-8E45-0060080C772F}
// *********************************************************************//
  IRequestInfoEx = interface(IRequestInfo)
    ['{784F3632-C937-11D1-8E45-0060080C772F}']
    function  GetRange(nItemNo: Smallint; out pvRange: OleVariant): Integer; safecall;
  end;

// *********************************************************************//
// DispIntf:  IRequestInfoExDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {784F3632-C937-11D1-8E45-0060080C772F}
// *********************************************************************//
  IRequestInfoExDisp = dispinterface
    ['{784F3632-C937-11D1-8E45-0060080C772F}']
    function  GetRange(nItemNo: Smallint; out pvRange: OleVariant): Integer; dispid 4;
    function  GetNumberOfItems: Smallint; dispid 1;
    function  GetIDs(nItemNo: Smallint; out nCompareID: Integer; out wSubItmNo: Smallint; 
                     out pbstrName: WideString): Integer; dispid 2;
    function  GetDataNotify: IDataNotify; dispid 3;
  end;

// *********************************************************************//
// Interface: IBarcoRequestInfo
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {222CD8D0-DAC5-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoRequestInfo = interface(IDispatch)
    ['{222CD8D0-DAC5-11D2-8F7D-0060080C772F}']
    function  GetNumberOfItems: Smallint; safecall;
    function  GetIDs(nItemNo: Smallint; var nCompareID: Integer; var wSubItmNo: Smallint; 
                     var pbstrName: WideString): Integer; safecall;
    function  GetDataNotify: IBarcoDataNotify; safecall;
    function  GetRange(nItemNo: Smallint; var pvRange: OleVariant): Integer; safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoRequestInfoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {222CD8D0-DAC5-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoRequestInfoDisp = dispinterface
    ['{222CD8D0-DAC5-11D2-8F7D-0060080C772F}']
    function  GetNumberOfItems: Smallint; dispid 1;
    function  GetIDs(nItemNo: Smallint; var nCompareID: Integer; var wSubItmNo: Smallint; 
                     var pbstrName: WideString): Integer; dispid 2;
    function  GetDataNotify: IBarcoDataNotify; dispid 3;
    function  GetRange(nItemNo: Smallint; var pvRange: OleVariant): Integer; dispid 4;
  end;

// *********************************************************************//
// Interface: IBarcoDataNotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E1017130-DAC5-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoDataNotify = interface(IDispatch)
    ['{E1017130-DAC5-11D2-8F7D-0060080C772F}']
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; var pvItems: PSafeArray); safecall;
    procedure NotifyEvent(nID: SYSINT; v: OleVariant); safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoDataNotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E1017130-DAC5-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoDataNotifyDisp = dispinterface
    ['{E1017130-DAC5-11D2-8F7D-0060080C772F}']
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; 
                           var pvItems: {??PSafeArray} OleVariant); dispid 1;
    procedure NotifyEvent(nID: SYSINT; v: OleVariant); dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoDataNotifyEx
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {083ED670-817A-11D3-9031-0060080C772F}
// *********************************************************************//
  IBarcoDataNotifyEx = interface(IBarcoDataNotify)
    ['{083ED670-817A-11D3-9031-0060080C772F}']
    procedure DbInfo(pParam: Integer; var riid: TGUID; const pUnk: IUnknown); safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoDataNotifyExDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {083ED670-817A-11D3-9031-0060080C772F}
// *********************************************************************//
  IBarcoDataNotifyExDisp = dispinterface
    ['{083ED670-817A-11D3-9031-0060080C772F}']
    procedure DbInfo(pParam: Integer; var riid: {??TGUID} OleVariant; const pUnk: IUnknown); dispid 3;
    procedure ProcessItems(pParam: Integer; sReplyType: Smallint; 
                           var pvItems: {??PSafeArray} OleVariant); dispid 1;
    procedure NotifyEvent(nID: SYSINT; v: OleVariant); dispid 2;
  end;

// *********************************************************************//
// Interface: IExtraSel
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {112029D6-4EBC-11D1-9171-0060080C772F}
// *********************************************************************//
  IExtraSel = interface(IDispatch)
    ['{112029D6-4EBC-11D1-9171-0060080C772F}']
    function  Get_MaxSelection: Smallint; safecall;
    function  Get_Title: WideString; safecall;
    function  Get_Description(iPos: SYSINT): WideString; safecall;
    function  Get_Abbreviation(iPos: SYSINT): WideString; safecall;
    property MaxSelection: Smallint read Get_MaxSelection;
    property Title: WideString read Get_Title;
    property Description[iPos: SYSINT]: WideString read Get_Description;
    property Abbreviation[iPos: SYSINT]: WideString read Get_Abbreviation;
  end;

// *********************************************************************//
// DispIntf:  IExtraSelDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {112029D6-4EBC-11D1-9171-0060080C772F}
// *********************************************************************//
  IExtraSelDisp = dispinterface
    ['{112029D6-4EBC-11D1-9171-0060080C772F}']
    property MaxSelection: Smallint readonly dispid 1;
    property Title: WideString readonly dispid 2;
    property Description[iPos: SYSINT]: WideString readonly dispid 3;
    property Abbreviation[iPos: SYSINT]: WideString readonly dispid 4;
  end;

// *********************************************************************//
// Interface: IReportItemSetup
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {10DD4CF0-C9D6-11CF-86DA-00608C71B320}
// *********************************************************************//
  IReportItemSetup = interface(IDispatch)
    ['{10DD4CF0-C9D6-11CF-86DA-00608C71B320}']
    procedure FindReportItem(const bstrRepItem: WideString; const pdei: IExtReportItem); safecall;
    procedure FindReportItemID(nID: SYSINT; const pdei: IExtReportItem); safecall;
    function  IsIDNumeric(nID: SYSINT): WordBool; safecall;
    function  IsIDTime(nID: SYSINT): WordBool; safecall;
    procedure BeginTransaction(const pNotify: INotify); safecall;
    procedure EndTransaction(const pNotify: INotify); safecall;
    procedure Copy(const priSetup: IReportItemSetup); safecall;
    procedure GetFirstReportItem(dwFormat: SYSINT; const pdei: IExtReportItem); safecall;
    procedure GetNextReportItem(dwFormat: SYSINT; const pdei: IExtReportItem); safecall;
    function  GetSignature: TDateTime; safecall;
    function  IsIDDeleted(out pInfo: IDeleteInfo; nID: SYSINT): WordBool; safecall;
    function  IsIDUsed(vnID: OleVariant; out bstrUsedIn: WideString): WordBool; safecall;
    procedure Restore(vInfo: OleVariant); safecall;
    function  New: IReportItemSetup; safecall;
    procedure CopyFile(const bstrFilePath: WideString); safecall;
  end;

// *********************************************************************//
// DispIntf:  IReportItemSetupDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {10DD4CF0-C9D6-11CF-86DA-00608C71B320}
// *********************************************************************//
  IReportItemSetupDisp = dispinterface
    ['{10DD4CF0-C9D6-11CF-86DA-00608C71B320}']
    procedure FindReportItem(const bstrRepItem: WideString; const pdei: IExtReportItem); dispid 30;
    procedure FindReportItemID(nID: SYSINT; const pdei: IExtReportItem); dispid 31;
    function  IsIDNumeric(nID: SYSINT): WordBool; dispid 32;
    function  IsIDTime(nID: SYSINT): WordBool; dispid 33;
    procedure BeginTransaction(const pNotify: INotify); dispid 34;
    procedure EndTransaction(const pNotify: INotify); dispid 35;
    procedure Copy(const priSetup: IReportItemSetup); dispid 36;
    procedure GetFirstReportItem(dwFormat: SYSINT; const pdei: IExtReportItem); dispid 37;
    procedure GetNextReportItem(dwFormat: SYSINT; const pdei: IExtReportItem); dispid 38;
    function  GetSignature: TDateTime; dispid 39;
    function  IsIDDeleted(out pInfo: IDeleteInfo; nID: SYSINT): WordBool; dispid 40;
    function  IsIDUsed(vnID: OleVariant; out bstrUsedIn: WideString): WordBool; dispid 41;
    procedure Restore(vInfo: OleVariant); dispid 42;
    function  New: IReportItemSetup; dispid 43;
    procedure CopyFile(const bstrFilePath: WideString); dispid 44;
  end;

// *********************************************************************//
// Interface: IBarcoReportItemSetup
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D8741E90-8891-11D3-903A-0060080C772F}
// *********************************************************************//
  IBarcoReportItemSetup = interface(IReportItemSetup)
    ['{D8741E90-8891-11D3-903A-0060080C772F}']
    procedure EnableAllSources(bEnable: Integer); safecall;
    procedure FindReportItemByShortDesc(const bstrRepItem: WideString; const pdei: IExtReportItem); safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoReportItemSetupDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D8741E90-8891-11D3-903A-0060080C772F}
// *********************************************************************//
  IBarcoReportItemSetupDisp = dispinterface
    ['{D8741E90-8891-11D3-903A-0060080C772F}']
    procedure EnableAllSources(bEnable: Integer); dispid 45;
    procedure FindReportItemByShortDesc(const bstrRepItem: WideString; const pdei: IExtReportItem); dispid 46;
    procedure FindReportItem(const bstrRepItem: WideString; const pdei: IExtReportItem); dispid 30;
    procedure FindReportItemID(nID: SYSINT; const pdei: IExtReportItem); dispid 31;
    function  IsIDNumeric(nID: SYSINT): WordBool; dispid 32;
    function  IsIDTime(nID: SYSINT): WordBool; dispid 33;
    procedure BeginTransaction(const pNotify: INotify); dispid 34;
    procedure EndTransaction(const pNotify: INotify); dispid 35;
    procedure Copy(const priSetup: IReportItemSetup); dispid 36;
    procedure GetFirstReportItem(dwFormat: SYSINT; const pdei: IExtReportItem); dispid 37;
    procedure GetNextReportItem(dwFormat: SYSINT; const pdei: IExtReportItem); dispid 38;
    function  GetSignature: TDateTime; dispid 39;
    function  IsIDDeleted(out pInfo: IDeleteInfo; nID: SYSINT): WordBool; dispid 40;
    function  IsIDUsed(vnID: OleVariant; out bstrUsedIn: WideString): WordBool; dispid 41;
    procedure Restore(vInfo: OleVariant); dispid 42;
    function  New: IReportItemSetup; dispid 43;
    procedure CopyFile(const bstrFilePath: WideString); dispid 44;
  end;

// *********************************************************************//
// Interface: IExtReportItem
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E3BDADC6-5A48-11D0-99F2-0000C088D00B}
// *********************************************************************//
  IExtReportItem = interface(IDispatch)
    ['{E3BDADC6-5A48-11D0-99F2-0000C088D00B}']
    procedure AttachGlobalSetup; safecall;
    procedure AttachSetup(const prpiSetup: IReportItemSetup); safecall;
    procedure Set_Category(iCategory: SYSINT); safecall;
    function  Get_Category: SYSINT; safecall;
    procedure Set_Item(iReportItem: SYSINT); safecall;
    function  Get_Item: SYSINT; safecall;
    procedure Set_Formula(const bstrFormula: WideString); safecall;
    function  Get_Formula: WideString; safecall;
    procedure Set_Name(const bstrName: WideString); safecall;
    function  Get_Name: WideString; safecall;
    procedure Set_SubItem(wSubItmNo: Smallint); safecall;
    function  Get_SubItem: Smallint; safecall;
    procedure Set_CompareID(nID: SYSINT); safecall;
    function  Get_CompareID: SYSINT; safecall;
    function  get_ID: SYSINT; safecall;
    procedure Set_ExceptionMask(wExceptionEnabled: Smallint); safecall;
    function  Get_ExceptionMask: Smallint; safecall;
    procedure Set_AlarmAbove(dbAlarmAbove: Double); safecall;
    function  Get_AlarmAbove: Double; safecall;
    procedure Set_AlarmBelow(dbAlarmBelow: Double); safecall;
    function  Get_AlarmBelow: Double; safecall;
    procedure Set_WarningAbove(dbWarningAbove: Double); safecall;
    function  Get_WarningAbove: Double; safecall;
    procedure Set_WarningBelow(dbWarningBelow: Double); safecall;
    function  Get_WarningBelow: Double; safecall;
    procedure Set_Format(const ppFormat: IFormat); safecall;
    function  Get_Format: IFormat; safecall;
    procedure EnableException(iException: SYSINT; vbEnable: WordBool); safecall;
    function  IsExceptionEnabled(iException: SYSINT): WordBool; safecall;
    function  IsIndexedItem(out ppExtra: IExtraSel): WordBool; safecall;
    procedure Set_Type_(peType: CATTYPE); safecall;
    function  Get_Type_: CATTYPE; safecall;
    procedure Copy(const pri: IExtReportItem); safecall;
    procedure Add; safecall;
    procedure Delete; safecall;
    property Category: SYSINT read Get_Category write Set_Category;
    property Item: SYSINT read Get_Item write Set_Item;
    property Formula: WideString read Get_Formula write Set_Formula;
    property Name: WideString read Get_Name write Set_Name;
    property SubItem: Smallint read Get_SubItem write Set_SubItem;
    property CompareID: SYSINT read Get_CompareID write Set_CompareID;
    property ExceptionMask: Smallint read Get_ExceptionMask write Set_ExceptionMask;
    property AlarmAbove: Double read Get_AlarmAbove write Set_AlarmAbove;
    property AlarmBelow: Double read Get_AlarmBelow write Set_AlarmBelow;
    property WarningAbove: Double read Get_WarningAbove write Set_WarningAbove;
    property WarningBelow: Double read Get_WarningBelow write Set_WarningBelow;
    property Format: IFormat read Get_Format write Set_Format;
    property Type_: CATTYPE read Get_Type_ write Set_Type_;
  end;

// *********************************************************************//
// DispIntf:  IExtReportItemDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E3BDADC6-5A48-11D0-99F2-0000C088D00B}
// *********************************************************************//
  IExtReportItemDisp = dispinterface
    ['{E3BDADC6-5A48-11D0-99F2-0000C088D00B}']
    procedure AttachGlobalSetup; dispid 1;
    procedure AttachSetup(const prpiSetup: IReportItemSetup); dispid 2;
    property Category: SYSINT dispid 3;
    property Item: SYSINT dispid 4;
    property Formula: WideString dispid 5;
    property Name: WideString dispid 6;
    property SubItem: Smallint dispid 7;
    property CompareID: SYSINT dispid 8;
    function  get_ID: SYSINT; dispid 9;
    property ExceptionMask: Smallint dispid 10;
    property AlarmAbove: Double dispid 11;
    property AlarmBelow: Double dispid 12;
    property WarningAbove: Double dispid 13;
    property WarningBelow: Double dispid 14;
    property Format: IFormat dispid 15;
    procedure EnableException(iException: SYSINT; vbEnable: WordBool); dispid 16;
    function  IsExceptionEnabled(iException: SYSINT): WordBool; dispid 17;
    function  IsIndexedItem(out ppExtra: IExtraSel): WordBool; dispid 18;
    property Type_: CATTYPE dispid 19;
    procedure Copy(const pri: IExtReportItem); dispid 20;
    procedure Add; dispid 21;
    procedure Delete; dispid 22;
  end;

// *********************************************************************//
// Interface: IFormat
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9CF18040-4EA8-11D1-9171-0060080C772F}
// *********************************************************************//
  IFormat = interface(IDispatch)
    ['{9CF18040-4EA8-11D1-9171-0060080C772F}']
    function  Get_DecimalPlaces: Smallint; safecall;
    procedure Set_DecimalPlaces(pVal: Smallint); safecall;
    function  Get_NumberAsTime: Integer; safecall;
    procedure Set_NumberAsTime(pVal: Integer); safecall;
    function  Get_DateAsLong: Integer; safecall;
    procedure Set_DateAsLong(pVal: Integer); safecall;
    function  Get_FieldWidth: Integer; safecall;
    procedure Set_FieldWidth(pVal: Integer); safecall;
    function  Get_JUSTIFY: JUSTIFY; safecall;
    procedure Set_JUSTIFY(pVal: JUSTIFY); safecall;
    property DecimalPlaces: Smallint read Get_DecimalPlaces write Set_DecimalPlaces;
    property NumberAsTime: Integer read Get_NumberAsTime write Set_NumberAsTime;
    property DateAsLong: Integer read Get_DateAsLong write Set_DateAsLong;
    property FieldWidth: Integer read Get_FieldWidth write Set_FieldWidth;
    property JUSTIFY: JUSTIFY read Get_JUSTIFY write Set_JUSTIFY;
  end;

// *********************************************************************//
// DispIntf:  IFormatDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9CF18040-4EA8-11D1-9171-0060080C772F}
// *********************************************************************//
  IFormatDisp = dispinterface
    ['{9CF18040-4EA8-11D1-9171-0060080C772F}']
    property DecimalPlaces: Smallint dispid 1;
    property NumberAsTime: Integer dispid 2;
    property DateAsLong: Integer dispid 3;
    property FieldWidth: Integer dispid 4;
    property JUSTIFY: JUSTIFY dispid 5;
  end;

// *********************************************************************//
// Interface: INotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {53AD5A20-4E6D-11CF-86DA-00608C71B320}
// *********************************************************************//
  INotify = interface(IDispatch)
    ['{53AD5A20-4E6D-11CF-86DA-00608C71B320}']
    procedure Change(const bstrUserName: WideString); safecall;
    procedure IsAwake; safecall;
    function  IsIDUsed(vnID: OleVariant; var bstrUsedIn: WideString): WordBool; safecall;
  end;

// *********************************************************************//
// DispIntf:  INotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {53AD5A20-4E6D-11CF-86DA-00608C71B320}
// *********************************************************************//
  INotifyDisp = dispinterface
    ['{53AD5A20-4E6D-11CF-86DA-00608C71B320}']
    procedure Change(const bstrUserName: WideString); dispid 1;
    procedure IsAwake; dispid 2;
    function  IsIDUsed(vnID: OleVariant; var bstrUsedIn: WideString): WordBool; dispid 3;
  end;

// *********************************************************************//
// Interface: IDeleteInfo
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C8141752-E96F-11D1-8E6B-0060080C772F}
// *********************************************************************//
  IDeleteInfo = interface(IDispatch)
    ['{C8141752-E96F-11D1-8E6B-0060080C772F}']
    function  Get_UserName: WideString; safecall;
    function  Get_OnDate: TDateTime; safecall;
    function  Get_Category: WideString; safecall;
    function  Get_ReportItemName: WideString; safecall;
    property UserName: WideString read Get_UserName;
    property OnDate: TDateTime read Get_OnDate;
    property Category: WideString read Get_Category;
    property ReportItemName: WideString read Get_ReportItemName;
  end;

// *********************************************************************//
// DispIntf:  IDeleteInfoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C8141752-E96F-11D1-8E6B-0060080C772F}
// *********************************************************************//
  IDeleteInfoDisp = dispinterface
    ['{C8141752-E96F-11D1-8E6B-0060080C772F}']
    property UserName: WideString readonly dispid 1;
    property OnDate: TDateTime readonly dispid 2;
    property Category: WideString readonly dispid 3;
    property ReportItemName: WideString readonly dispid 4;
  end;

// *********************************************************************//
// Interface: IConvert
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BD60E355-6D24-11D0-99FF-0000C088D00B}
// *********************************************************************//
  IConvert = interface(IDispatch)
    ['{BD60E355-6D24-11D0-99FF-0000C088D00B}']
    procedure Convert(pDoc: SYSINT; pArchive: SYSINT); safecall;
  end;

// *********************************************************************//
// DispIntf:  IConvertDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BD60E355-6D24-11D0-99FF-0000C088D00B}
// *********************************************************************//
  IConvertDisp = dispinterface
    ['{BD60E355-6D24-11D0-99FF-0000C088D00B}']
    procedure Convert(pDoc: SYSINT; pArchive: SYSINT); dispid 1610743808;
  end;

// *********************************************************************//
// Interface: INotifySrc
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A7B3F570-4E8D-11CF-86DA-00608C71B320}
// *********************************************************************//
  INotifySrc = interface(IDispatch)
    ['{A7B3F570-4E8D-11CF-86DA-00608C71B320}']
    procedure AddUser(const pNotify: INotify; const bstrUserName: WideString); safecall;
    procedure RemoveUser(const pNotify: INotify); safecall;
  end;

// *********************************************************************//
// DispIntf:  INotifySrcDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A7B3F570-4E8D-11CF-86DA-00608C71B320}
// *********************************************************************//
  INotifySrcDisp = dispinterface
    ['{A7B3F570-4E8D-11CF-86DA-00608C71B320}']
    procedure AddUser(const pNotify: INotify; const bstrUserName: WideString); dispid 1;
    procedure RemoveUser(const pNotify: INotify); dispid 2;
  end;

// *********************************************************************//
// Interface: INotifySrc2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {36439AC0-12FC-11D1-9B05-0060080C772F}
// *********************************************************************//
  INotifySrc2 = interface(INotifySrc)
    ['{36439AC0-12FC-11D1-9B05-0060080C772F}']
    function  FirstUser(out plPos: Integer): IUserStats; safecall;
    function  NextUser(var plPos: Integer): IUserStats; safecall;
  end;

// *********************************************************************//
// DispIntf:  INotifySrc2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {36439AC0-12FC-11D1-9B05-0060080C772F}
// *********************************************************************//
  INotifySrc2Disp = dispinterface
    ['{36439AC0-12FC-11D1-9B05-0060080C772F}']
    function  FirstUser(out plPos: Integer): IUserStats; dispid 3;
    function  NextUser(var plPos: Integer): IUserStats; dispid 4;
    procedure AddUser(const pNotify: INotify; const bstrUserName: WideString); dispid 1;
    procedure RemoveUser(const pNotify: INotify); dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoDictionaryQuery
// Flags:     (0)
// GUID:      {34A79280-E5C9-11D2-8F86-0060080C772F}
// *********************************************************************//
  IBarcoDictionaryQuery = interface(IUnknown)
    ['{34A79280-E5C9-11D2-8F86-0060080C772F}']
    function  ObtainDIDList(nReportItems: SYSINT; var pdwIDs: ID_N_SUBITM; out nDIDs: Word; 
                            out ppDIDnDBbList: PUserType3; out ppID2DidRFormula: PUserType4; 
                            out pnID: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IUserStats
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BD69EF70-1479-11D1-9B07-0060080C772F}
// *********************************************************************//
  IUserStats = interface(IDispatch)
    ['{BD69EF70-1479-11D1-9B07-0060080C772F}']
    function  Get_UserName: WideString; safecall;
    function  Get_ObjectsCreated: Smallint; safecall;
    function  Get_RequestsPerSecond: Integer; safecall;
    function  Get_TimeInProcess: Integer; safecall;
    property UserName: WideString read Get_UserName;
    property ObjectsCreated: Smallint read Get_ObjectsCreated;
    property RequestsPerSecond: Integer read Get_RequestsPerSecond;
    property TimeInProcess: Integer read Get_TimeInProcess;
  end;

// *********************************************************************//
// DispIntf:  IUserStatsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BD69EF70-1479-11D1-9B07-0060080C772F}
// *********************************************************************//
  IUserStatsDisp = dispinterface
    ['{BD69EF70-1479-11D1-9B07-0060080C772F}']
    property UserName: WideString readonly dispid 1;
    property ObjectsCreated: Smallint readonly dispid 2;
    property RequestsPerSecond: Integer readonly dispid 3;
    property TimeInProcess: Integer readonly dispid 4;
  end;

// *********************************************************************//
// Interface: IBarcoRPIStream
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B8BEB670-37A5-11D4-B741-0050DA734520}
// *********************************************************************//
  IBarcoRPIStream = interface(IDispatch)
    ['{B8BEB670-37A5-11D4-B741-0050DA734520}']
    procedure Set_RPIStream(pvStream: OleVariant); safecall;
    function  Get_RPIStream: OleVariant; safecall;
    property RPIStream: OleVariant read Get_RPIStream write Set_RPIStream;
  end;

// *********************************************************************//
// DispIntf:  IBarcoRPIStreamDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B8BEB670-37A5-11D4-B741-0050DA734520}
// *********************************************************************//
  IBarcoRPIStreamDisp = dispinterface
    ['{B8BEB670-37A5-11D4-B741-0050DA734520}']
    property RPIStream: OleVariant dispid 47;
  end;

// *********************************************************************//
// Interface: IBarcoCalcFunc
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FCC4E79E-FCFA-11D4-8ABC-0050DA11E5DF}
// *********************************************************************//
  IBarcoCalcFunc = interface(IDispatch)
    ['{FCC4E79E-FCFA-11D4-8ABC-0050DA11E5DF}']
    function  GetFirstFunction(var lPos: Integer): WideString; safecall;
    function  GetNextFunction(var lPos: Integer): WideString; safecall;
    function  GetFunctionDescription(var bstrName: WideString): WideString; safecall;
    function  GetFunctionHelpStrings(var bstrName: WideString): PSafeArray; safecall;
    procedure GetFunctionDefinitionByName(var bstrName: WideString; var lRetType: Integer; 
                                          var nParam: Integer; var ppsaParams: PSafeArray); safecall;
    procedure GetFunctionDefinitionByCLSID(var bstrCLSID: WideString; var nID: Integer; 
                                           var lRetType: Integer; var nParam: Integer; 
                                           var ppsaParams: PSafeArray); safecall;
    function  GetFunctionCLSIDInfo(var bstrName: WideString; var bstrCLSID: WideString): Integer; safecall;
    function  GetFunctionName(var bstrCLSID: WideString; var nID: Integer): WideString; safecall;
    function  ProcessFunction(var bstrCLSID: WideString; var nID: Integer; var nParam: Integer; 
                              var ppsaParams: PSafeArray): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoCalcFuncDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FCC4E79E-FCFA-11D4-8ABC-0050DA11E5DF}
// *********************************************************************//
  IBarcoCalcFuncDisp = dispinterface
    ['{FCC4E79E-FCFA-11D4-8ABC-0050DA11E5DF}']
    function  GetFirstFunction(var lPos: Integer): WideString; dispid 1;
    function  GetNextFunction(var lPos: Integer): WideString; dispid 2;
    function  GetFunctionDescription(var bstrName: WideString): WideString; dispid 3;
    function  GetFunctionHelpStrings(var bstrName: WideString): {??PSafeArray} OleVariant; dispid 4;
    procedure GetFunctionDefinitionByName(var bstrName: WideString; var lRetType: Integer; 
                                          var nParam: Integer; 
                                          var ppsaParams: {??PSafeArray} OleVariant); dispid 5;
    procedure GetFunctionDefinitionByCLSID(var bstrCLSID: WideString; var nID: Integer; 
                                           var lRetType: Integer; var nParam: Integer; 
                                           var ppsaParams: {??PSafeArray} OleVariant); dispid 6;
    function  GetFunctionCLSIDInfo(var bstrName: WideString; var bstrCLSID: WideString): Integer; dispid 7;
    function  GetFunctionName(var bstrCLSID: WideString; var nID: Integer): WideString; dispid 8;
    function  ProcessFunction(var bstrCLSID: WideString; var nID: Integer; var nParam: Integer; 
                              var ppsaParams: {??PSafeArray} OleVariant): OleVariant; dispid 9;
  end;

// *********************************************************************//
// Interface: iCategory
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {1AE57610-8BD0-11D0-9A2D-0000C088D00B}
// *********************************************************************//
  iCategory = interface(IDispatch)
    ['{1AE57610-8BD0-11D0-9A2D-0000C088D00B}']
    procedure AttachGlobalSetup; safecall;
    procedure AttachSetup(const prpiSetup: IReportItemSetup); safecall;
    procedure Set_Category(iCategory: SYSINT); safecall;
    function  Get_Category: SYSINT; safecall;
    procedure Set_Name(const bstrName: WideString); safecall;
    function  Get_Name: WideString; safecall;
    procedure Set_NumberOfItems(nReportItems: SYSINT); safecall;
    function  Get_NumberOfItems: SYSINT; safecall;
    procedure Set_NumberOfCategories(nCategories: SYSINT); safecall;
    function  Get_NumberOfCategories: SYSINT; safecall;
    procedure Add; safecall;
    procedure Delete; safecall;
    procedure FindCategoryName(const bstrCatName: WideString); safecall;
    function  HasBaseItems(dwFormat: SYSINT; out bstrFirstItem: WideString): WordBool; safecall;
    property Category: SYSINT read Get_Category write Set_Category;
    property Name: WideString read Get_Name write Set_Name;
    property NumberOfItems: SYSINT read Get_NumberOfItems write Set_NumberOfItems;
    property NumberOfCategories: SYSINT read Get_NumberOfCategories write Set_NumberOfCategories;
  end;

// *********************************************************************//
// DispIntf:  iCategoryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {1AE57610-8BD0-11D0-9A2D-0000C088D00B}
// *********************************************************************//
  iCategoryDisp = dispinterface
    ['{1AE57610-8BD0-11D0-9A2D-0000C088D00B}']
    procedure AttachGlobalSetup; dispid 1;
    procedure AttachSetup(const prpiSetup: IReportItemSetup); dispid 2;
    property Category: SYSINT dispid 3;
    property Name: WideString dispid 4;
    property NumberOfItems: SYSINT dispid 5;
    property NumberOfCategories: SYSINT dispid 6;
    procedure Add; dispid 7;
    procedure Delete; dispid 8;
    procedure FindCategoryName(const bstrCatName: WideString); dispid 9;
    function  HasBaseItems(dwFormat: SYSINT; out bstrFirstItem: WideString): WordBool; dispid 10;
  end;

// *********************************************************************//
// Interface: IBarcoCategory
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A371E020-B620-11D4-91C1-000629F9D283}
// *********************************************************************//
  IBarcoCategory = interface(iCategory)
    ['{A371E020-B620-11D4-91C1-000629F9D283}']
    function  GetBrowseReportItem(dwFormat: SYSINT): IBarcoExtReportItem; safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoCategoryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A371E020-B620-11D4-91C1-000629F9D283}
// *********************************************************************//
  IBarcoCategoryDisp = dispinterface
    ['{A371E020-B620-11D4-91C1-000629F9D283}']
    function  GetBrowseReportItem(dwFormat: SYSINT): IBarcoExtReportItem; dispid 11;
    procedure AttachGlobalSetup; dispid 1;
    procedure AttachSetup(const prpiSetup: IReportItemSetup); dispid 2;
    property Category: SYSINT dispid 3;
    property Name: WideString dispid 4;
    property NumberOfItems: SYSINT dispid 5;
    property NumberOfCategories: SYSINT dispid 6;
    procedure Add; dispid 7;
    procedure Delete; dispid 8;
    procedure FindCategoryName(const bstrCatName: WideString); dispid 9;
    function  HasBaseItems(dwFormat: SYSINT; out bstrFirstItem: WideString): WordBool; dispid 10;
  end;

// *********************************************************************//
// Interface: IBarcoExtReportItem
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A814CF10-5959-11D3-8FEA-0060080C772F}
// *********************************************************************//
  IBarcoExtReportItem = interface(IExtReportItem)
    ['{A814CF10-5959-11D3-8FEA-0060080C772F}']
    procedure Set_Source(peSource: DBSOURCE); safecall;
    function  Get_Source: DBSOURCE; safecall;
    procedure EnableAllSources(bEnable: Integer); safecall;
    property Source: DBSOURCE read Get_Source write Set_Source;
  end;

// *********************************************************************//
// DispIntf:  IBarcoExtReportItemDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A814CF10-5959-11D3-8FEA-0060080C772F}
// *********************************************************************//
  IBarcoExtReportItemDisp = dispinterface
    ['{A814CF10-5959-11D3-8FEA-0060080C772F}']
    property Source: DBSOURCE dispid 23;
    procedure EnableAllSources(bEnable: Integer); dispid 24;
    procedure AttachGlobalSetup; dispid 1;
    procedure AttachSetup(const prpiSetup: IReportItemSetup); dispid 2;
    property Category: SYSINT dispid 3;
    property Item: SYSINT dispid 4;
    property Formula: WideString dispid 5;
    property Name: WideString dispid 6;
    property SubItem: Smallint dispid 7;
    property CompareID: SYSINT dispid 8;
    function  get_ID: SYSINT; dispid 9;
    property ExceptionMask: Smallint dispid 10;
    property AlarmAbove: Double dispid 11;
    property AlarmBelow: Double dispid 12;
    property WarningAbove: Double dispid 13;
    property WarningBelow: Double dispid 14;
    property Format: IFormat dispid 15;
    procedure EnableException(iException: SYSINT; vbEnable: WordBool); dispid 16;
    function  IsExceptionEnabled(iException: SYSINT): WordBool; dispid 17;
    function  IsIndexedItem(out ppExtra: IExtraSel): WordBool; dispid 18;
    property Type_: CATTYPE dispid 19;
    procedure Copy(const pri: IExtReportItem); dispid 20;
    procedure Add; dispid 21;
    procedure Delete; dispid 22;
  end;

// *********************************************************************//
// Interface: ICalc
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {85275621-6939-11D0-99FA-0000C088D00B}
// *********************************************************************//
  ICalc = interface(IDispatch)
    ['{85275621-6939-11D0-99FA-0000C088D00B}']
    function  GetValidMaskAt(const bstrFormula: WideString; iPos: SYSINT): Smallint; safecall;
    function  CheckCalc(const bstrFormula: WideString): SYSINT; safecall;
  end;

// *********************************************************************//
// DispIntf:  ICalcDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {85275621-6939-11D0-99FA-0000C088D00B}
// *********************************************************************//
  ICalcDisp = dispinterface
    ['{85275621-6939-11D0-99FA-0000C088D00B}']
    function  GetValidMaskAt(const bstrFormula: WideString; iPos: SYSINT): Smallint; dispid 23;
    function  CheckCalc(const bstrFormula: WideString): SYSINT; dispid 24;
  end;

// *********************************************************************//
// Interface: ICalcEngine
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {407A9EF8-29D9-11D1-9155-0060080C772F}
// *********************************************************************//
  ICalcEngine = interface(IDispatch)
    ['{407A9EF8-29D9-11D1-9155-0060080C772F}']
    function  Calculate(const pcl: ICalcList): Double; safecall;
  end;

// *********************************************************************//
// DispIntf:  ICalcEngineDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {407A9EF8-29D9-11D1-9155-0060080C772F}
// *********************************************************************//
  ICalcEngineDisp = dispinterface
    ['{407A9EF8-29D9-11D1-9155-0060080C772F}']
    function  Calculate(const pcl: ICalcList): Double; dispid 30;
  end;

// *********************************************************************//
// Interface: ICalcList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {963AD14E-29DB-11D1-9155-0060080C772F}
// *********************************************************************//
  ICalcList = interface(IDispatch)
    ['{963AD14E-29DB-11D1-9155-0060080C772F}']
    function  Get_Name: WideString; safecall;
    function  Get_Value: Double; safecall;
    property Name: WideString read Get_Name;
    property Value: Double read Get_Value;
  end;

// *********************************************************************//
// DispIntf:  ICalcListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {963AD14E-29DB-11D1-9155-0060080C772F}
// *********************************************************************//
  ICalcListDisp = dispinterface
    ['{963AD14E-29DB-11D1-9155-0060080C772F}']
    property Name: WideString readonly dispid 1;
    property Value: Double readonly dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoReportItemRequest
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4E567760-DAEF-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoReportItemRequest = interface(IDispatch)
    ['{4E567760-DAEF-11D2-8F7D-0060080C772F}']
    procedure SetRequestItems(const prqi: IBarcoRequestInfo; out pnID: SYSINT); safecall;
    procedure SetSubItemNo(wSubItmNo: Smallint; vbNewSelection: WordBool); safecall;
    procedure RequestData(wRecordNo: Smallint; pParam: Integer; var pvItems: PSafeArray); safecall;
    procedure RequestKeyList(nID: SYSINT; pParam: Integer; var pvItems: PSafeArray); safecall;
    procedure SetFilters(const pf: IBarcoFilter); safecall;
    procedure RequestDataByKeyList(const pKeyList: IBarcoKeyList; pParam: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoReportItemRequestDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4E567760-DAEF-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoReportItemRequestDisp = dispinterface
    ['{4E567760-DAEF-11D2-8F7D-0060080C772F}']
    procedure SetRequestItems(const prqi: IBarcoRequestInfo; out pnID: SYSINT); dispid 1;
    procedure SetSubItemNo(wSubItmNo: Smallint; vbNewSelection: WordBool); dispid 2;
    procedure RequestData(wRecordNo: Smallint; pParam: Integer; 
                          var pvItems: {??PSafeArray} OleVariant); dispid 3;
    procedure RequestKeyList(nID: SYSINT; pParam: Integer; var pvItems: {??PSafeArray} OleVariant); dispid 4;
    procedure SetFilters(const pf: IBarcoFilter); dispid 5;
    procedure RequestDataByKeyList(const pKeyList: IBarcoKeyList; pParam: Integer); dispid 6;
  end;

// *********************************************************************//
// Interface: IReportItemRequest
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E3BDADC8-5A48-11D0-99F2-0000C088D00B}
// *********************************************************************//
  IReportItemRequest = interface(IDispatch)
    ['{E3BDADC8-5A48-11D0-99F2-0000C088D00B}']
    procedure SetRequestItems(const prqi: IRequestInfo; out pnID: SYSINT); safecall;
    procedure SetSubItemNo(wSubItmNo: Smallint; vbNewSelection: WordBool); safecall;
    procedure RequestData(wRecordNo: Smallint; pParam: Integer; out pvItems: PSafeArray); safecall;
    procedure RequestKeyList(nID: SYSINT; pParam: Integer; out pvItems: PSafeArray); safecall;
    procedure SetFilters(const pf: IFilters); safecall;
  end;

// *********************************************************************//
// DispIntf:  IReportItemRequestDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E3BDADC8-5A48-11D0-99F2-0000C088D00B}
// *********************************************************************//
  IReportItemRequestDisp = dispinterface
    ['{E3BDADC8-5A48-11D0-99F2-0000C088D00B}']
    procedure SetRequestItems(const prqi: IRequestInfo; out pnID: SYSINT); dispid 1;
    procedure SetSubItemNo(wSubItmNo: Smallint; vbNewSelection: WordBool); dispid 2;
    procedure RequestData(wRecordNo: Smallint; pParam: Integer; 
                          out pvItems: {??PSafeArray} OleVariant); dispid 3;
    procedure RequestKeyList(nID: SYSINT; pParam: Integer; out pvItems: {??PSafeArray} OleVariant); dispid 4;
    procedure SetFilters(const pf: IFilters); dispid 5;
  end;

// *********************************************************************//
// Interface: IBarcoFilter
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {130FA090-DAC4-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoFilter = interface(IDispatch)
    ['{130FA090-DAC4-11D2-8F7D-0060080C772F}']
    function  Get_FirstFilterTest(var pos: SYSINT): IBarcoFilterTest; safecall;
    procedure Set_FirstFilterTest(var pos: SYSINT; const pVal: IBarcoFilterTest); safecall;
    function  Get_NextFilterTest(var pos: SYSINT): IBarcoFilterTest; safecall;
    procedure Set_NextFilterTest(var pos: SYSINT; const pVal: IBarcoFilterTest); safecall;
    function  Get_Name: WideString; safecall;
    function  Get_ID: Word; safecall;
    function  Get_AnyCondition: Integer; safecall;
    function  Get_HideMachines: Integer; safecall;
    property FirstFilterTest[var pos: SYSINT]: IBarcoFilterTest read Get_FirstFilterTest write Set_FirstFilterTest;
    property NextFilterTest[var pos: SYSINT]: IBarcoFilterTest read Get_NextFilterTest write Set_NextFilterTest;
    property Name: WideString read Get_Name;
    property ID: Word read Get_ID;
    property AnyCondition: Integer read Get_AnyCondition;
    property HideMachines: Integer read Get_HideMachines;
  end;

// *********************************************************************//
// DispIntf:  IBarcoFilterDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {130FA090-DAC4-11D2-8F7D-0060080C772F}
// *********************************************************************//
  IBarcoFilterDisp = dispinterface
    ['{130FA090-DAC4-11D2-8F7D-0060080C772F}']
    property FirstFilterTest[var pos: SYSINT]: IBarcoFilterTest dispid 1;
    property NextFilterTest[var pos: SYSINT]: IBarcoFilterTest dispid 2;
    property Name: WideString readonly dispid 3;
    property ID: {??Word} OleVariant readonly dispid 4;
    property AnyCondition: Integer readonly dispid 5;
    property HideMachines: Integer readonly dispid 6;
  end;

// *********************************************************************//
// Interface: IFilterTest
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {24E4C030-E623-11D0-9AAE-0000C088D00B}
// *********************************************************************//
  IFilterTest = interface(IDispatch)
    ['{24E4C030-E623-11D0-9AAE-0000C088D00B}']
    function  Get_ID: Integer; safecall;
    procedure Set_ID(pVal: Integer); safecall;
    function  Get_CompareTest: Smallint; safecall;
    procedure Set_CompareTest(pVal: Smallint); safecall;
    function  Get_CompareValue: WideString; safecall;
    procedure Set_CompareValue(const pVal: WideString); safecall;
    function  Get_Name: WideString; safecall;
    function  Get_CompareID: Integer; safecall;
    procedure Set_CompareID(pVal: Integer); safecall;
    function  Get_UpperThreshold: Double; safecall;
    procedure Set_UpperThreshold(pVal: Double); safecall;
    function  Get_LowerThreshold: Double; safecall;
    procedure Set_LowerThreshold(pVal: Double); safecall;
    property ID: Integer read Get_ID write Set_ID;
    property CompareTest: Smallint read Get_CompareTest write Set_CompareTest;
    property CompareValue: WideString read Get_CompareValue write Set_CompareValue;
    property Name: WideString read Get_Name;
    property CompareID: Integer read Get_CompareID write Set_CompareID;
    property UpperThreshold: Double read Get_UpperThreshold write Set_UpperThreshold;
    property LowerThreshold: Double read Get_LowerThreshold write Set_LowerThreshold;
  end;

// *********************************************************************//
// DispIntf:  IFilterTestDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {24E4C030-E623-11D0-9AAE-0000C088D00B}
// *********************************************************************//
  IFilterTestDisp = dispinterface
    ['{24E4C030-E623-11D0-9AAE-0000C088D00B}']
    property ID: Integer dispid 1;
    property CompareTest: Smallint dispid 2;
    property CompareValue: WideString dispid 3;
    property Name: WideString readonly dispid 4;
    property CompareID: Integer dispid 5;
    property UpperThreshold: Double dispid 6;
    property LowerThreshold: Double dispid 7;
  end;

// *********************************************************************//
// Interface: IBarcoFilterTest
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7091AD00-9B65-11D3-B8F3-006008515DBF}
// *********************************************************************//
  IBarcoFilterTest = interface(IFilterTest)
    ['{7091AD00-9B65-11D3-B8F3-006008515DBF}']
    function  Get_timeType: FILTERTEST_ENUMTIME; safecall;
    procedure Set_timeType(pVal: FILTERTEST_ENUMTIME); safecall;
    property timeType: FILTERTEST_ENUMTIME read Get_timeType write Set_timeType;
  end;

// *********************************************************************//
// DispIntf:  IBarcoFilterTestDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7091AD00-9B65-11D3-B8F3-006008515DBF}
// *********************************************************************//
  IBarcoFilterTestDisp = dispinterface
    ['{7091AD00-9B65-11D3-B8F3-006008515DBF}']
    property timeType: FILTERTEST_ENUMTIME dispid 8;
    property ID: Integer dispid 1;
    property CompareTest: Smallint dispid 2;
    property CompareValue: WideString dispid 3;
    property Name: WideString readonly dispid 4;
    property CompareID: Integer dispid 5;
    property UpperThreshold: Double dispid 6;
    property LowerThreshold: Double dispid 7;
  end;

// *********************************************************************//
// Interface: IBarcoKeyList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {18F14432-02EB-11D3-8F9F-0060080C772F}
// *********************************************************************//
  IBarcoKeyList = interface(IDispatch)
    ['{18F14432-02EB-11D3-8F9F-0060080C772F}']
    function  Get_FirstKey(var ppos: SYSINT; var pnRowNo: SYSINT): OleVariant; safecall;
    procedure Set_FirstKey(var ppos: SYSINT; var pnRowNo: SYSINT; pVal: OleVariant); safecall;
    function  Get_NextKey(var ppos: SYSINT; var pnRowNo: SYSINT): OleVariant; safecall;
    procedure Set_NextKey(var ppos: SYSINT; var pnRowNo: SYSINT; pVal: OleVariant); safecall;
    procedure Set_MachineSelection(const pVal: IBarcoMachineSelectionList); safecall;
    function  Get_MachineSelection: IBarcoMachineSelectionList; safecall;
    procedure Set_KeyID(pVal: SYSINT); safecall;
    function  Get_KeyID: SYSINT; safecall;
    function  Get_NumKeys: SYSINT; safecall;
    property FirstKey[var ppos: SYSINT; var pnRowNo: SYSINT]: OleVariant read Get_FirstKey write Set_FirstKey;
    property NextKey[var ppos: SYSINT; var pnRowNo: SYSINT]: OleVariant read Get_NextKey write Set_NextKey;
    property MachineSelection: IBarcoMachineSelectionList read Get_MachineSelection write Set_MachineSelection;
    property KeyID: SYSINT read Get_KeyID write Set_KeyID;
    property NumKeys: SYSINT read Get_NumKeys;
  end;

// *********************************************************************//
// DispIntf:  IBarcoKeyListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {18F14432-02EB-11D3-8F9F-0060080C772F}
// *********************************************************************//
  IBarcoKeyListDisp = dispinterface
    ['{18F14432-02EB-11D3-8F9F-0060080C772F}']
    property FirstKey[var ppos: SYSINT; var pnRowNo: SYSINT]: OleVariant dispid 1;
    property NextKey[var ppos: SYSINT; var pnRowNo: SYSINT]: OleVariant dispid 2;
    property MachineSelection: IBarcoMachineSelectionList dispid 3;
    property KeyID: SYSINT dispid 4;
    property NumKeys: SYSINT readonly dispid 5;
  end;

// *********************************************************************//
// Interface: IMachineSelectionList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6A22F030-E4B3-11D0-9AAD-0000C088D00B}
// *********************************************************************//
  IMachineSelectionList = interface(IDispatch)
    ['{6A22F030-E4B3-11D0-9AAD-0000C088D00B}']
    function  Get_FirstMachine: Smallint; safecall;
    procedure Set_FirstMachine(pVal: Smallint); safecall;
    function  Get_NextMachine(pos: SYSINT): Smallint; safecall;
    procedure Set_NextMachine(pos: SYSINT; pVal: Smallint); safecall;
    property FirstMachine: Smallint read Get_FirstMachine write Set_FirstMachine;
    property NextMachine[pos: SYSINT]: Smallint read Get_NextMachine write Set_NextMachine;
  end;

// *********************************************************************//
// DispIntf:  IMachineSelectionListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6A22F030-E4B3-11D0-9AAD-0000C088D00B}
// *********************************************************************//
  IMachineSelectionListDisp = dispinterface
    ['{6A22F030-E4B3-11D0-9AAD-0000C088D00B}']
    property FirstMachine: Smallint dispid 1;
    property NextMachine[pos: SYSINT]: Smallint dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoMachineSelectionList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {19AB9170-7580-11D3-901E-0060080C772F}
// *********************************************************************//
  IBarcoMachineSelectionList = interface(IMachineSelectionList)
    ['{19AB9170-7580-11D3-901E-0060080C772F}']
    function  Get_FirstMachine(var pos: SYSINT): Smallint; safecall;
    procedure Set_FirstMachine(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_NextMachine(var pos: SYSINT): Smallint; safecall;
    procedure Set_NextMachine(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_Section: IBarcoSectionList; safecall;
    procedure Set_Section(const pVal: IBarcoSectionList); safecall;
    property FirstMachine[var pos: SYSINT]: Smallint read Get_FirstMachine write Set_FirstMachine;
    property NextMachine[var pos: SYSINT]: Smallint read Get_NextMachine write Set_NextMachine;
    property Section: IBarcoSectionList read Get_Section write Set_Section;
  end;

// *********************************************************************//
// DispIntf:  IBarcoMachineSelectionListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {19AB9170-7580-11D3-901E-0060080C772F}
// *********************************************************************//
  IBarcoMachineSelectionListDisp = dispinterface
    ['{19AB9170-7580-11D3-901E-0060080C772F}']
    property FirstMachine[var pos: SYSINT]: Smallint dispid 3;
    property NextMachine[var pos: SYSINT]: Smallint dispid 4;
    property Section: IBarcoSectionList dispid 5;
    property FirstMachine_: Smallint dispid 1;
    property NextMachine_[pos: SYSINT]: Smallint dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoSectionList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6EEBF830-7A67-11D3-9027-0060080C772F}
// *********************************************************************//
  IBarcoSectionList = interface(IDispatch)
    ['{6EEBF830-7A67-11D3-9027-0060080C772F}']
    function  Get_FirstSection(var pos: SYSINT): Smallint; safecall;
    procedure Set_FirstSection(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_NextSection(var pos: SYSINT): Smallint; safecall;
    procedure Set_NextSection(var pos: SYSINT; pVal: Smallint); safecall;
    function  Get_NumSections: SYSINT; safecall;
    property FirstSection[var pos: SYSINT]: Smallint read Get_FirstSection write Set_FirstSection;
    property NextSection[var pos: SYSINT]: Smallint read Get_NextSection write Set_NextSection;
    property NumSections: SYSINT read Get_NumSections;
  end;

// *********************************************************************//
// DispIntf:  IBarcoSectionListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6EEBF830-7A67-11D3-9027-0060080C772F}
// *********************************************************************//
  IBarcoSectionListDisp = dispinterface
    ['{6EEBF830-7A67-11D3-9027-0060080C772F}']
    property FirstSection[var pos: SYSINT]: Smallint dispid 1;
    property NextSection[var pos: SYSINT]: Smallint dispid 2;
    property NumSections: SYSINT readonly dispid 3;
  end;

// *********************************************************************//
// Interface: ISummary
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9C95D441-704B-11D0-9A04-0000C088D00B}
// *********************************************************************//
  ISummary = interface(IDispatch)
    ['{9C95D441-704B-11D0-9A04-0000C088D00B}']
    procedure SetKey(iKey: SYSINT; nID: SYSINT); safecall;
    procedure ClearSummaryData; safecall;
    function  GetSummaryLine(vadiKeys: OleVariant): OleVariant; safecall;
    procedure RemoveKeys; safecall;
    procedure SetIntervals(const prqi: IRequestInfoEx; out pnID: SYSINT); safecall;
  end;

// *********************************************************************//
// DispIntf:  ISummaryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9C95D441-704B-11D0-9A04-0000C088D00B}
// *********************************************************************//
  ISummaryDisp = dispinterface
    ['{9C95D441-704B-11D0-9A04-0000C088D00B}']
    procedure SetKey(iKey: SYSINT; nID: SYSINT); dispid 5;
    procedure ClearSummaryData; dispid 6;
    function  GetSummaryLine(vadiKeys: OleVariant): OleVariant; dispid 7;
    procedure RemoveKeys; dispid 8;
    procedure SetIntervals(const prqi: IRequestInfoEx; out pnID: SYSINT); dispid 9;
  end;

// *********************************************************************//
// Interface: IFilters
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2AAD5360-E622-11D0-9AAE-0000C088D00B}
// *********************************************************************//
  IFilters = interface(IDispatch)
    ['{2AAD5360-E622-11D0-9AAE-0000C088D00B}']
    function  Get_FirstFilterTest: IFilterTest; safecall;
    procedure Set_FirstFilterTest(const pVal: IFilterTest); safecall;
    function  Get_NextFilterTest(pos: SYSINT): IFilterTest; safecall;
    procedure Set_NextFilterTest(pos: SYSINT; const pVal: IFilterTest); safecall;
    function  Get_Name: WideString; safecall;
    function  Get_ID: Word; safecall;
    function  Get_AnyCondition: Integer; safecall;
    function  Get_HideMachines: Integer; safecall;
    property FirstFilterTest: IFilterTest read Get_FirstFilterTest write Set_FirstFilterTest;
    property NextFilterTest[pos: SYSINT]: IFilterTest read Get_NextFilterTest write Set_NextFilterTest;
    property Name: WideString read Get_Name;
    property ID: Word read Get_ID;
    property AnyCondition: Integer read Get_AnyCondition;
    property HideMachines: Integer read Get_HideMachines;
  end;

// *********************************************************************//
// DispIntf:  IFiltersDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2AAD5360-E622-11D0-9AAE-0000C088D00B}
// *********************************************************************//
  IFiltersDisp = dispinterface
    ['{2AAD5360-E622-11D0-9AAE-0000C088D00B}']
    property FirstFilterTest: IFilterTest dispid 1;
    property NextFilterTest[pos: SYSINT]: IFilterTest dispid 2;
    property Name: WideString readonly dispid 3;
    property ID: {??Word} OleVariant readonly dispid 4;
    property AnyCondition: Integer readonly dispid 5;
    property HideMachines: Integer readonly dispid 6;
  end;

// *********************************************************************//
// Interface: IRPIComCreator
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BDFAA765-E2D0-11D2-8F84-0060080C772F}
// *********************************************************************//
  IRPIComCreator = interface(IDispatch)
    ['{BDFAA765-E2D0-11D2-8F84-0060080C772F}']
    procedure Set_ClientName(const bstrName: WideString); safecall;
    function  Get_ClientName: WideString; safecall;
    function  AttachRPISetup: IReportItemSetup; safecall;
    property ClientName: WideString read Get_ClientName write Set_ClientName;
  end;

// *********************************************************************//
// DispIntf:  IRPIComCreatorDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BDFAA765-E2D0-11D2-8F84-0060080C772F}
// *********************************************************************//
  IRPIComCreatorDisp = dispinterface
    ['{BDFAA765-E2D0-11D2-8F84-0060080C772F}']
    property ClientName: WideString dispid 1;
    function  AttachRPISetup: IReportItemSetup; dispid 2;
  end;

// *********************************************************************//
// Interface: IBarcoDataDictionary
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {604412A0-D615-11D2-8F77-0060080C772F}
// *********************************************************************//
  IBarcoDataDictionary = interface(IDispatch)
    ['{604412A0-D615-11D2-8F77-0060080C772F}']
    function  GetFirstDataItemText(var pbstr: WideString; lCategory: Integer; lFormat: Integer; 
                                   eType: BARCO_TEXTTYPE): Integer; safecall;
    function  GetNextDataItemText(lDID: Integer; var pbstr: WideString; lCategory: Integer; 
                                  lFormat: Integer; eType: BARCO_TEXTTYPE): Integer; safecall;
    function  IsIndexedDataItem(lDID: Integer; var ppExSel: IExtraSelEx): Integer; safecall;
    function  GetDataItemSize(lDID: Integer): Smallint; safecall;
    function  GetDbBinding(lDID: Integer; sSubDIDNo: Smallint): Byte; safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoDataDictionaryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {604412A0-D615-11D2-8F77-0060080C772F}
// *********************************************************************//
  IBarcoDataDictionaryDisp = dispinterface
    ['{604412A0-D615-11D2-8F77-0060080C772F}']
    function  GetFirstDataItemText(var pbstr: WideString; lCategory: Integer; lFormat: Integer; 
                                   eType: BARCO_TEXTTYPE): Integer; dispid 1;
    function  GetNextDataItemText(lDID: Integer; var pbstr: WideString; lCategory: Integer; 
                                  lFormat: Integer; eType: BARCO_TEXTTYPE): Integer; dispid 2;
    function  IsIndexedDataItem(lDID: Integer; var ppExSel: IExtraSelEx): Integer; dispid 3;
    function  GetDataItemSize(lDID: Integer): Smallint; dispid 4;
    function  GetDbBinding(lDID: Integer; sSubDIDNo: Smallint): Byte; dispid 5;
  end;

// *********************************************************************//
// Interface: IBarcoDDMaster
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {255BF510-0AD1-11D4-910C-0050DA734520}
// *********************************************************************//
  IBarcoDDMaster = interface(IDispatch)
    ['{255BF510-0AD1-11D4-910C-0050DA734520}']
    procedure GetDidInfo(var plDID: Integer; var pbstrShortDesc: WideString; 
                         var psSubDidNo: Smallint); safecall;
    procedure ItemDIDListChanged; safecall;
    procedure SubItemListChanged; safecall;
  end;

// *********************************************************************//
// DispIntf:  IBarcoDDMasterDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {255BF510-0AD1-11D4-910C-0050DA734520}
// *********************************************************************//
  IBarcoDDMasterDisp = dispinterface
    ['{255BF510-0AD1-11D4-910C-0050DA734520}']
    procedure GetDidInfo(var plDID: Integer; var pbstrShortDesc: WideString; 
                         var psSubDidNo: Smallint); dispid 1;
    procedure ItemDIDListChanged; dispid 2;
    procedure SubItemListChanged; dispid 3;
  end;

// *********************************************************************//
// Interface: IExtraSelEx
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EF19B620-D623-11D2-8F77-0060080C772F}
// *********************************************************************//
  IExtraSelEx = interface(IExtraSel)
    ['{EF19B620-D623-11D2-8F77-0060080C772F}']
    function  Get_DbBinding(iPos: SYSINT): Smallint; safecall;
    function  Get_Category: WideString; safecall;
    procedure AddSelection(const bstrSelection: WideString; const bstrAbb: WideString; 
                           sDbBinding: Smallint); safecall;
    property DbBinding[iPos: SYSINT]: Smallint read Get_DbBinding;
    property Category: WideString read Get_Category;
  end;

// *********************************************************************//
// DispIntf:  IExtraSelExDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EF19B620-D623-11D2-8F77-0060080C772F}
// *********************************************************************//
  IExtraSelExDisp = dispinterface
    ['{EF19B620-D623-11D2-8F77-0060080C772F}']
    property DbBinding[iPos: SYSINT]: Smallint readonly dispid 5;
    property Category: WideString readonly dispid 6;
    procedure AddSelection(const bstrSelection: WideString; const bstrAbb: WideString; 
                           sDbBinding: Smallint); dispid 7;
    property MaxSelection: Smallint readonly dispid 1;
    property Title: WideString readonly dispid 2;
    property Description[iPos: SYSINT]: WideString readonly dispid 3;
    property Abbreviation[iPos: SYSINT]: WideString readonly dispid 4;
  end;

// *********************************************************************//
// The Class CoREPORTITEMSETUP provides a Create and CreateRemote method to          
// create instances of the default interface IReportItemSetup exposed by              
// the CoClass REPORTITEMSETUP. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoREPORTITEMSETUP = class
    class function Create: IReportItemSetup;
    class function CreateRemote(const MachineName: string): IReportItemSetup;
  end;

// *********************************************************************//
// The Class CoCategory provides a Create and CreateRemote method to          
// create instances of the default interface iCategory exposed by              
// the CoClass Category. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCategory = class
    class function Create: iCategory;
    class function CreateRemote(const MachineName: string): iCategory;
  end;

// *********************************************************************//
// The Class CoREPORTITEM provides a Create and CreateRemote method to          
// create instances of the default interface IExtReportItem exposed by              
// the CoClass REPORTITEM. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoREPORTITEM = class
    class function Create: IExtReportItem;
    class function CreateRemote(const MachineName: string): IExtReportItem;
  end;

// *********************************************************************//
// The Class CoREPORTITEMREQUEST provides a Create and CreateRemote method to          
// create instances of the default interface IBarcoReportItemRequest exposed by              
// the CoClass REPORTITEMREQUEST. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoREPORTITEMREQUEST = class
    class function Create: IBarcoReportItemRequest;
    class function CreateRemote(const MachineName: string): IBarcoReportItemRequest;
  end;

// *********************************************************************//
// The Class CoRPICOMCREATOR provides a Create and CreateRemote method to          
// create instances of the default interface IRPIComCreator exposed by              
// the CoClass RPICOMCREATOR. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRPICOMCREATOR = class
    class function Create: IRPIComCreator;
    class function CreateRemote(const MachineName: string): IRPIComCreator;
  end;

// *********************************************************************//
// The Class CoBarcoDataDictionary provides a Create and CreateRemote method to          
// create instances of the default interface IBarcoDataDictionary exposed by              
// the CoClass BarcoDataDictionary. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoBarcoDataDictionary = class
    class function Create: IBarcoDataDictionary;
    class function CreateRemote(const MachineName: string): IBarcoDataDictionary;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

class function CoREPORTITEMSETUP.Create: IReportItemSetup;
begin
  Result := CreateComObject(CLASS_REPORTITEMSETUP) as IReportItemSetup;
end;

class function CoREPORTITEMSETUP.CreateRemote(const MachineName: string): IReportItemSetup;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_REPORTITEMSETUP) as IReportItemSetup;
end;

class function CoCategory.Create: iCategory;
begin
  Result := CreateComObject(CLASS_Category) as iCategory;
end;

class function CoCategory.CreateRemote(const MachineName: string): iCategory;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Category) as iCategory;
end;

class function CoREPORTITEM.Create: IExtReportItem;
begin
  Result := CreateComObject(CLASS_REPORTITEM) as IExtReportItem;
end;

class function CoREPORTITEM.CreateRemote(const MachineName: string): IExtReportItem;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_REPORTITEM) as IExtReportItem;
end;

class function CoREPORTITEMREQUEST.Create: IBarcoReportItemRequest;
begin
  Result := CreateComObject(CLASS_REPORTITEMREQUEST) as IBarcoReportItemRequest;
end;

class function CoREPORTITEMREQUEST.CreateRemote(const MachineName: string): IBarcoReportItemRequest;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_REPORTITEMREQUEST) as IBarcoReportItemRequest;
end;

class function CoRPICOMCREATOR.Create: IRPIComCreator;
begin
  Result := CreateComObject(CLASS_RPICOMCREATOR) as IRPIComCreator;
end;

class function CoRPICOMCREATOR.CreateRemote(const MachineName: string): IRPIComCreator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RPICOMCREATOR) as IRPIComCreator;
end;

class function CoBarcoDataDictionary.Create: IBarcoDataDictionary;
begin
  Result := CreateComObject(CLASS_BarcoDataDictionary) as IBarcoDataDictionary;
end;

class function CoBarcoDataDictionary.CreateRemote(const MachineName: string): IBarcoDataDictionary;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_BarcoDataDictionary) as IBarcoDataDictionary;
end;

end.
