inherited frmSelectComponents: TfrmSelectComponents
  Left = 356
  Top = 245
  Caption = '(*)Funktionen auswaehlen'
  ClientHeight = 409
  ClientWidth = 604
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 16
  inherited mmPanel4: TmmPanel
    Top = 369
    Width = 604
    inherited bOK: TmmButton
      Left = 317
    end
    inherited bCancel: TmmButton
      Left = 412
    end
    inherited bHelp: TmmButton
      Left = 507
    end
  end
  inherited gbSelectionList: TmmPanel
    Width = 604
    Height = 369
    inherited mmPanel1: TmmPanel
      Width = 282
      Height = 365
      inherited mmLabel2: TmmLabel
        Width = 159
        Caption = '(*)Verfuegbare Funktionen:'
      end
      inherited lbSource: TmmListBox
        Width = 282
        Height = 345
      end
    end
    inherited mmPanel2: TmmPanel
      Left = 284
      Width = 36
      Height = 365
      inherited bMoveRight: TmmSpeedButton
        Left = 7
      end
      inherited bMoveLeft: TmmSpeedButton
        Left = 7
      end
    end
    inherited mmPanel3: TmmPanel
      Left = 320
      Width = 282
      Height = 365
      inherited mmLabel3: TmmLabel
        Width = 169
        Caption = '(*)Ausgewaehlte Funktionen:'
      end
      inherited lbDest: TmmListBox
        Width = 282
        Height = 345
      end
    end
  end
  inherited mmActionList: TmmActionList
    Left = 205
    Top = 41
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 207
    Top = 11
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
