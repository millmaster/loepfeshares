{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: NetWare.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: -Div. Netzwerk-, OS- und SQL-Funktionen
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 05.09.2002  1.00  SDo | File created
| 18.07.2007  1.01  SDo | - Anpassung Vista; Erweiterung GetOSType und TOSType
                          - Neue Func. ab Vista GetProductInfo(), GetProcedureAddress()
                            GetProductSuiteText() und Const Produkt Suiten
| 27.07.2007  1.02  SDo | - Neu: ExistsSQLServerOnMachine()
===============================================================================}
unit NetWare;

interface


uses Windows, Classes, sysutils;

type

 TOSType = (osUnknown, osWin95, osWin98, osME, osNT31, osWinNTWKS,
            osWinNTSRV, osW2kProf, osW2kSrv, osXPPro, osXPHome, osW2003SRV, osVista, osServer2008);


 TMachineType = (mmUnknown, maWKS, maWKSWorkgroup, maSRVAdd, maSRVWorkgroup,
                 maSRVActiveDir );

 TSQLType = (sqlUnknown, sqlMSDE, sqlStandard, sqlDesktop, sqlEntreprise);


 TOSVersionInfoEx  =  record
    OSVersionInfo: TOSVersionInfo;
    wServicePackMajor: WORD;
    wServicePackMinor: WORD;
    wSuiteMask: WORD;
    wProductType: BYTE;
    wReserved: BYTE;
  end;

//Network
//+++++++

//Ermittelt alle Maschinen in der angegebenen Domain
function GetServers(aDoamin:String): TStringList; overload;

function GetServers(aDoamin:String; aServerType : Word): TStringList; overload;

//Ermittelt alle Domains im Netz
function GetAllDomains: TStringList;

//Ermittelt das OS-Produkt (WKS, SRV etc.) anhand der ServerTyp-ID
function GetNTProduct(aServerTypes: DWord): Word; overload;

//Ermittelt das OS-Produkt (WKS, SRV etc.) von einem Rechner
function GetNTProduct(aServerName:String): Word; overload;

//Ermittelt den Domain-Namen
function GetActualDomainName:String;

//Ermittelt den Domain-Namen von einem Server -> kein result bei Workgroup
function GetDomainName(aServer:String):String;

//Ermittelt den PDC-Namen
function GetDomainServerName:String; overload;
//Ermittelt den PDC-Namen von einer beliebigen Domain
function GetDomainServerName(aDoamin :String):String;  overload;

//Ermittelt de Hostnamen anhand einer IP-Adresse im Netz (Firmennetz)
function GetBroadcastIP(aIPAddr: string): string;

//Ermittelt den Hostnamen anhand einer IP-Adresse
function IPToHostName(aIPAddr: string): string;

//Ermittelt die IP-Adresse von einem Host
function HostNameToIP(aPCName: string): string;

//Ermittelt den Netzwerkadappter Typ   (GetNetworkAdapterName)
function GetAdaterTypeName(Type_: Uint): string;

//Ermittelt den Netzwerkkarten-Namen
function GetNetworkAdapterName:String;

//Verbindungs-Check zu PC
function CheckConnectionTo(aHost:String; var aMsg :String):Boolean;


//OS
//++

//Ermittelt den LogOnServer
function GetLogOnServer: string;

//Ermittelt den PC-Namen
function GetMachineName: string;

//Ermittelt, ob PC in Workgroup oder Domain eingebunden ist
function IsWorkGroup :Boolean;

//Ermittelt die Windows Version (4=NT, 5=Win2k, 6=WinXP)
function GetOSMajorVersion:Word;

//Ermittelt, ob das Active Direcory installiert wurde
function HasActiveDirectory:Boolean;

//Ermittelt, ob eine Netzwerkkarte installiert ist
function ExistsNetWorkAdapter:Boolean;

//Ermittelt, ob TCP/IP installiert ist
function ExistsTCPIP:Boolean;

//Ermittelt die lokale TCP/IP
function GetLocalTCPIP:String;

//Ermittelt die lokale Subnet Maske
function GetLocalSubnetMask:String;

//Ermittelt die MAC Adresse des Netzwerkadapers -> 00-00-00-00-00-00 = kein Adapter
function GetMAC_Address:String;

//Ermittelt, ob eine Texnet-Karte installiert ist
function ExistsTexNetAdapter: Boolean;

//Ueberprueft ob ein Service Texnet oder PCI930 in der Reg. existiert
function ExistsTexnetRegistryService: Boolean;

//Ermittelt, ob die Texnet-Karte aktiv ist
function IsTexNetDriverRunning: Boolean;

//Ermittelt, ob der Treiber der Texnet-Karte istalliert ist
function IsTexNetDriverInstalled: Boolean;

//Ermittelt die Anzahl installierten ISA-Texnetkarten
function ISATexnetAdapterCout:integer;

//Ermittelt, ob das Distinct fuer AC338 istalliert ist
function ExistsDistinct:Boolean;

function GetOSInfo: TOSVersionInfoEx;

//Ermittelt die OS Plattform
function GetOSType: TOSType;

//Ermittelt das OS-SP
function GetOSServicePack: Word;

//Stellt eine Verbindung zu einem RemotePC her -> AddUser/AddGroups
function MachineConnection(aMachine, aUserName, aPWD:String):Boolean;

//Trennt eine RemotePC Verbindung
function MachineDisconnection(aMachine:String):Boolean;

//Ermittelt den Machinentype (WKS, PDC, Server etc.)
function GetMachineType: TMachineType;

//Ermittelt, ob das ActiveDirecory installiert ist -> Domain Controller
function GetActiveDirecory: Boolean;

//Ermittelt, ob das Multilanguage installiert ist
function IsMultiLanguagePack: Boolean;

//Ermittelt die OS-Sprache als ID
function GetOSLanguageID: DWORD;

//Ermittelt eine Sprache als Text
function GetOSLanguageAsText(aLangID: LANGID):String; overload;
function GetOSLanguageAsText:String; overload;



//SQL-Server
//++++++++++

//Ermittelt ob ein SQL-Server installiert ist
function ExistsSQLServerOnMachine:Boolean;

//Ueberprueft eine Verbindung zum SQL-Server
function CheckSQLConnection(aServerName, aSQLUser, aPWD: String):Boolean;

//Ermittelt die SQL-Server Version (7 = NT-SQL, 8 = SQL2000 oder MSDE2000
function GetSQLServerVersion:Integer; overload;
function GetSQLServerVersion(aServerName, aSQLUser, aPWD:String):Integer; overload;

//Ermittelt die SQL-Server ServicePack
function GetSQLServerSP(aServerName, aSQLUser, aPWD:String):integer;
function GetSQLServerSPAsText(aServerName, aSQLUser, aPWD:String):String;

//Ermittelt die SQL-Server Type (MSDE2000, Standard, Desktop)
function GetSQLServerType(aServerName, aSQLUser, aPWD:String): TSQLType;

//Ermittelt, ob eine MM_WinidngDB vorhanden ist
function ExistsMMWindingDB(aServerName, aSQLUser, aPWD: String): Boolean;

//Ermittelt, ob der SQL-Server l�uft
function IsSQLServerRunning(aHostName, aSQLUser, aPWD: String):Boolean;


//Ab Vista: Ermittelt OS-Infos wie z.B. die Produkt Suiten -> Vista business etc.
function GetProductInfo(aOSMajorVersion: DWord; aOSMinorVersion: DWord;
                        aSpMajorVersion: DWord; aSpMinorVersion: DWord;
                        var aReturnedProductType: Dword): BOOL; stdcall;
function GetProductSuiteText(aProductType: DWORD):String;
procedure GetProcedureAddress(var P: Pointer; const ModuleName, ProcName: string);

//Ab Vista: Ermittelt OS-Edition
function GetOSSEdition : string;


const
    //siehe u_SupportFunc.pas
    cWorkstation                  = 1;
    cServer                       = 2;
    cBackupdomaincontroller       = 3;
    cPrimarydomaincontroller      = 4;
    cAddServer                    = 5;
    cStandaloneServer             = 6;
    cStandaloneWKS                = 7;


    VER_NT_WORKSTATION       = 1;
    VER_NT_DOMAIN_CONTROLLER = 2;
    VER_NT_SERVER            = 3;

    VER_SUITE_SMALLBUSINESS            = $0001;
    VER_SUITE_ENTERPRISE               = $0002;
    VER_SUITE_BACKOFFICE               = $0004;
    VER_SUITE_TERMINAL                 = $0010;
    VER_SUITE_SMALLBUSINESS_RESTRICTED = $0020;
    VER_SUITE_DATACENTER               = $0080;
    VER_SUITE_PERSONAL                 = $0200;

    cMMWinding                         = 'MM_Winding';

//******************************************************************************
// OS Product Suiten
//******************************************************************************
const
  PRODUCT_BUSINESS = $00000006;
  PRODUCT_BUSINESS_N = $00000010;
  PRODUCT_CLUSTER_SERVER = $00000012;
  PRODUCT_DATACENTER_SERVER = $00000008;
  PRODUCT_DATACENTER_SERVER_CORE = $0000000C;
  PRODUCT_ENTERPRISE = $00000004;
  PRODUCT_ENTERPRISE_SERVER = $0000000A;
  PRODUCT_ENTERPRISE_SERVER_CORE = $0000000E;
  PRODUCT_ENTERPRISE_SERVER_IA64 = $0000000F;
  PRODUCT_HOME_BASIC = $00000002;
  PRODUCT_HOME_BASIC_N = $00000005;
  PRODUCT_HOME_PREMIUM = $00000003;
  PRODUCT_HOME_SERVER = $00000013;
  PRODUCT_SERVER_FOR_SMALLBUSINESS = $00000018;
  PRODUCT_SMALLBUSINESS_SERVER = $00000009;
  PRODUCT_SMALLBUSINESS_SERVER_PREMIUM = $00000019;
  PRODUCT_STANDARD_SERVER = $00000007;
  PRODUCT_STANDARD_SERVER_CORE = $0000000D;
  PRODUCT_STARTER = $0000000B;
  PRODUCT_STORAGE_ENTERPRISE_SERVER = $00000017;
  PRODUCT_STORAGE_EXPRESS_SERVER = $00000014;
  PRODUCT_STORAGE_STANDARD_SERVER = $00000015;
  PRODUCT_STORAGE_WORKGROUP_SERVER = $00000016;
  PRODUCT_UNDEFINED = $00000000;
  PRODUCT_ULTIMATE = $00000001;
  PRODUCT_WEB_SERVER = $00000011;
  VER_SUITE_COMPUTE_SERVER = $00004000;
  VER_SUITE_STORAGE_SERVER = $00002000;
  SM_TABLETPC = 86;
  SM_MEDIACENTER = 87;
  SM_STARTER = 88;
  SM_SERVERR2 = 89;
//------------------------------------------------------------------------------


implementation

uses NetApi32, NTCommon, FileCtrl, Dialogs, Forms,
     JclSysInfo, JclStrings, Loepfeglobal, registry, AdvApi32, WinSvc,
     Winsock, IpHlpApi, IPTypes, IpIfConst, JclFileUtils,
     SQLDMO_TLB, NTException, lmErr,
     NetworkPWD_DLG, Controls, mmCS,
     IdBaseComponent, IdComponent, IdRawBase, IdRawClient, IdIcmpClient, ActiveX;

//------------------------------------------------------------------------------
var
  _GetProductInfo: Pointer;
//------------------------------------------------------------------------------

//******************************************************************************
// DLL laden
//******************************************************************************
procedure GetProcedureAddress(var P: Pointer; const ModuleName, ProcName: string);
var
  ModuleHandle: HMODULE;
begin
  if not Assigned(P) then
  begin
    ModuleHandle := GetModuleHandle(PChar(ModuleName));
    if ModuleHandle = 0 then
    begin
      ModuleHandle := LoadLibrary(PChar(ModuleName));
      if ModuleHandle = 0 then raise Exception.Create('Library not found: ' + ModuleName);
    end;
    P := GetProcAddress(ModuleHandle, PChar(ProcName));
    if not Assigned(P) then raise Exception.Create('Function not found: ' + ModuleName + '.' + ProcName);
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittelt OS-Produkt Infos ab Vista!!
// z.B. aReturnedProductType -> Vista Business, Vista HOME BASIC etc.
//******************************************************************************
function GetProductInfo(aOSMajorVersion: DWord; aOSMinorVersion: DWord;
                        aSpMajorVersion: DWord; aSpMinorVersion: DWord;
                        var aReturnedProductType: Dword): BOOL; stdcall;
begin
  GetProcedureAddress(_GetProductInfo, 'kernel32.dll', 'GetProductInfo');
  asm
    mov esp, ebp
    pop ebp
    jmp [_GetProductInfo]
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//OS Suiten als Text
//******************************************************************************
function GetProductSuiteText(aProductType: DWORD):String;
var xSuite : string;
begin
   case (aProductType) of
        PRODUCT_BUSINESS                 : xSuite := 'Business Edition';
        PRODUCT_BUSINESS_N               : xSuite := 'Business N Edition';

        PRODUCT_CLUSTER_SERVER           : xSuite := 'Cluster Server Edition';
        PRODUCT_DATACENTER_SERVER        : xSuite := 'Server Datacenter Edition';
        PRODUCT_DATACENTER_SERVER_CORE   : xSuite := 'Core Server Datacenter Edition';

        PRODUCT_ENTERPRISE               : xSuite := 'Enterprise Edition';
        PRODUCT_ENTERPRISE_SERVER        : xSuite := 'Server Enterprise Edition';
        PRODUCT_ENTERPRISE_SERVER_CORE   : xSuite := 'Core Server Enterprise Edition';
        PRODUCT_ENTERPRISE_SERVER_IA64   : xSuite := 'Itanium Server Enterprise Edition';

        PRODUCT_HOME_BASIC               : xSuite := 'Home Basic Edition';
        PRODUCT_HOME_BASIC_N             : xSuite := 'Home Basic N Edition';
        PRODUCT_HOME_PREMIUM             : xSuite := 'Home Premium Edition';
        PRODUCT_HOME_SERVER              : xSuite := 'Home Server Edition';

        PRODUCT_SERVER_FOR_SMALLBUSINESS : xSuite := 'Server for Small Business Edition';
        PRODUCT_SMALLBUSINESS_SERVER     : xSuite := 'Small Business Server';
        PRODUCT_SMALLBUSINESS_SERVER_PREMIUM: xSuite := 'Small Business Server Premium Edition';

        PRODUCT_STANDARD_SERVER          : xSuite := 'Server Standard Edition';
        PRODUCT_STANDARD_SERVER_CORE     : xSuite := 'Core Server Standard Edition';

        PRODUCT_STARTER                  : xSuite := 'Starter Edition';

        PRODUCT_STORAGE_ENTERPRISE_SERVER: xSuite := 'Storage Server Enterprise Edition';
        PRODUCT_STORAGE_EXPRESS_SERVER   : xSuite := 'Storage Server Express Edition';
        PRODUCT_STORAGE_STANDARD_SERVER  : xSuite := 'Storage Server Standard Edition';
        PRODUCT_STORAGE_WORKGROUP_SERVER : xSuite := 'Storage Server Workgroup Edition';

        PRODUCT_ULTIMATE                 : xSuite := 'Ultimate Edition';
        PRODUCT_WEB_SERVER               : xSuite := 'Web Server Edition';

        else
          xSuite := '';
   end;

   Result := xSuite;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittelt OS-Edition ab Vista!!
//******************************************************************************
function GetOSSEdition : string;
var xMajorVers,
    xOSMajorVersion, xOSMinorVersion,
    xSpMajorVersion, xSpMinorVersion,
    xReturnedProductType  : DWORD;
begin
  try
  result := '';
  xMajorVers := GetOSInfo.OSVersionInfo.dwMajorVersion ;

  if (xMajorVers > 5 ) then begin
    GetProductInfo(xOSMajorVersion, xOSMinorVersion,
                   xSpMajorVersion, xSpMinorVersion,
                   xReturnedProductType);

     result := GetProductSuiteText(xReturnedProductType);
   end;
   except
   end;
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function GetServers(aDoamin:String): TStringList;
var
  xPWCh: array[0..255] of WideChar;
  xRes:   NET_API_STATUS;
  xbufptr:  PServer_Info_101_Arr;
  xentriesread: DWORD;
  xtotalentries: DWORD;
  xresume_handle: DWORD;
  x: integer;
  xDomain  : array[0..255] of WideChar;
  xDomName : PChar;
  xMachineName, xErrorMSG: String;
begin
 try
  xDomName := PChar(aDoamin);
  Result := TStringList.Create;

  //FillChar(xDomain,Sizeof(xDomain),0);
  xresume_handle := 0;
  StringToWideChar('', @xPWCh, SizeOf(xPWCh));
  StringToWideChar(xDomName, @xDomain, SizeOf(xDomain));

  FillChar(xbufptr,Sizeof(xbufptr),0);

  if aDoamin <> '' then begin
     xRes :=  NetServerEnum(@xPWCh,
                            101,
                            Pointer(xbufptr),
                            $FFFFFFFF,
                            xentriesread,
                            xtotalentries,
                            SV_TYPE_WORKSTATION,
                            @xDomain,
                            xresume_handle)
  end else
     xRes :=  NetServerEnum(@xPWCh,
                            101,
                            Pointer(xbufptr),
                            $FFFFFFFF,
                            xentriesread,
                            xtotalentries,
                            SV_TYPE_WORKSTATION,
                            NIL,
                            xresume_handle);

  if xRes = 0 then
     for x := 0 to xentriesread - 1 do begin
         xtotalentries :=  xBufPtr^[x].sv101_type ;
         xMachineName  := WideCharToString(xBufPtr^[x].sv101_name);
         Result.AddObject(xMachineName, TObject(xtotalentries) );
         //Result.Add(aDoamin +'\' + xMachineName);
         Forms.Application.ProcessMessages;
     end;
 except
   on E: Exception do begin
         xErrorMSG := Format('Error in func. GetServers( %s). Msg: %s',[aDoamin, e.message]);
         MessageDlg(xErrorMSG, mtError, [mbOk], 0);
   end;
 end;
end;
//------------------------------------------------------------------------------
function GetServers(aDoamin:String; aServerType : Word): TStringList; overload;
var
  xPWCh: array[0..255] of WideChar;
  xRes:   NET_API_STATUS;
  xbufptr:  PServer_Info_101_Arr;
  xentriesread: DWORD;
  xtotalentries: DWORD;
  xresume_handle: DWORD;
  x: integer;
  xDomain  : array[0..255] of WideChar;
  xDomName : PChar;
  xMachineName, xErrorMSG: String;
begin

 try
  xDomName := PChar(aDoamin);
  Result := TStringList.Create;

  //FillChar(xDomain,Sizeof(xDomain),0);
  xresume_handle := 0;
  StringToWideChar('', @xPWCh, SizeOf(xPWCh));
  StringToWideChar(xDomName, @xDomain, SizeOf(xDomain));

  FillChar(xbufptr,Sizeof(xbufptr),0);

  if aDoamin <> '' then begin
     xRes :=  NetServerEnum(@xPWCh,
                            101,
                            Pointer(xbufptr),
                            $FFFFFFFF,
                            xentriesread,
                            xtotalentries,
                            aServerType,
                            @xDomain,
                            xresume_handle)
  end else
     xRes :=  NetServerEnum(@xPWCh,
                            101,
                            Pointer(xbufptr),
                            $FFFFFFFF,
                            xentriesread,
                            xtotalentries,
                            aServerType,
                            NIL,
                            xresume_handle);

  if xRes = 0 then
     for x := 0 to xentriesread - 1 do begin
         xtotalentries :=  xBufPtr^[x].sv101_type ;
         xMachineName  := WideCharToString(xBufPtr^[x].sv101_name);
         Result.AddObject(xMachineName, TObject(xtotalentries) );
         Forms.Application.ProcessMessages;
     end;
 except
   on E: Exception do begin
       xErrorMSG := Format('Error in func. GetServers( %s, %d). Msg: %s',[aDoamin, aServerType, e.message]);
       MessageDlg(xErrorMSG, mtError, [mbOk], 0);
   end;
 end;
end;
//------------------------------------------------------------------------------
function GetAllDomains: TStringList;
var
  xPWCh: array[0..255] of WideChar;
  xRes:   NET_API_STATUS;
  xbufptr:  PServer_Info_101_Arr;
  xentriesread: DWORD;
  xtotalentries: DWORD;
  xresume_handle: DWORD;
  x: integer;
begin

  try
      Result := TStringList.Create;
      StringToWideChar('', @xPWCh, SizeOf(xPWCh));

      xresume_handle := 0;
      FillChar(xbufptr,Sizeof(xbufptr),0);
      xRes :=  NetServerEnum(@xPWCh,
                             101,
                             Pointer(xbufptr),
                             $FFFFFFFF,
                             xentriesread,
                             xtotalentries,
                             SV_TYPE_DOMAIN_ENUM,
                             NIL,
                             xresume_handle);

      if xRes = 0 then
         for x := 0 to xentriesread - 1 do begin
             xtotalentries :=  xBufPtr^[x].sv101_type ;
             Result.Add(WideCharToString(xBufPtr^[x].sv101_name));
             forms.Application.ProcessMessages;
         end;
  except
  end;
end;
//------------------------------------------------------------------------------
function GetNTProduct(aServerTypes: DWord): Word; overload;
var xProdID : Word;
    var xHexStr: String;
    xHex : Word;
begin
  result:= 0;

  if aServerTypes and SV_TYPE_DOMAIN_CTRL  <>  0 then
     xProdID:= cPrimarydomaincontroller
  else
    if aServerTypes and SV_TYPE_DOMAIN_BAKCTRL <> 0 then
       xProdID:= cBackupdomaincontroller
    else
      if aServerTypes and SV_TYPE_SERVER_NT <>  0 then
         if aServerTypes and SV_TYPE_WFW <> 0 then
            xProdID:= cStandaloneServer
         else
            xProdID:= cAddServer
      else
         if aServerTypes and SV_TYPE_WORKSTATION <> 0 then
            xProdID:= cWorkstation;

  if IsWorkGroup then begin
     if xProdID = cAddServer then    xProdID:= cStandaloneServer;
     if xProdID = cWorkstation then  xProdID:= cStandaloneWKS;
  end;
  result:= xProdID;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt den Typ des Betriebsystems von einem PC
//******************************************************************************
function GetNTProduct(aServerName:String): Word; overload;
var xbuf: Pserver_info_101;
    xRes: NET_API_STATUS;
    xPointer: Pointer;
    xServerName: array[0..255] of WideChar;
    xType :DWord;
begin
  try
      FillChar(xServerName, sizeof(xServerName), 0);
      StringToWideChar(PChar(aServerName), @xServerName, SizeOf(xServerName) );

      xRes := NetServerGetInfo(@xServerName, 101, xPointer);
      xBuf := Pserver_info_101( xPointer );
      try
        if xBuf <> nil then xType := xBuf^.sv101_type;
      finally
        NetAPIBufferFree(xbuf);
      end;
      result := GetNTProduct(xType);
  except
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt den Domain- oder Workgroup-Namen
//******************************************************************************
function GetActualDomainName:String;
begin
  try
    result:= GetPrimaryDomainName;
  except
    try
       result:= GetDomainNameForServer(GetMachineName);
    except
       result:= '';
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt den Domain-Namen von einem Server -> kein result bei Workgroup
//******************************************************************************
function GetDomainName(aServer:String):String;
begin
  try
     result:= GetDomainNameForServer(aServer);
  except
     result:= '';
  end;
  Result:= StringReplace( Result,'\\','', [rfReplaceAll] );
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt den PDC-Namen
//******************************************************************************
function GetDomainServerName:String; overload
begin
  try
    result:= StringReplace( GetPrimaryDomainServerName,'\\','', [rfReplaceAll] );
  except
    result:='';
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt den PDC-Namen von einer beliebigen Domain
//******************************************************************************
function GetDomainServerName(aDoamin :String):String;  overload;
var
  xRes:   NET_API_STATUS;
  xBuf:   Pointer;
  PWCh, LocalPDC: array[0..255] of WideChar;
begin
 FillChar(PWCh, SizeOf(PWCh), 0);
 FillChar(LocalPDC, SizeOf(LocalPDC), 0);

 StringToWideChar(aDoamin, @PWCh, SizeOf(PWCh) );
 //StringToWideChar(GetPrimaryDomainServerName, @LocalPDC, SizeOf(LocalPDC) );
 StringToWideChar('', @LocalPDC, SizeOf(LocalPDC) );
 try
   xRes := NetGetDCName(@LocalPDC, @PWCh, xBuf);

   if (xRes <> 0) and (xRes <> lmErr.NERR_DCNotFound) then begin
      CreateNetAPIException(xRes);
      Result := '';
      //NetApiBufferFree(xBuf);
      exit;
   end;

   if (xBuf <> NIL) and (xRes = 0) then begin
      Result := WideCharToString(xBuf);
      //NetApiBufferFree(xBuf);
   end else
      Result := '';


 finally
    NetApiBufferFree(xBuf);
 end;

 Result:= StringReplace( Result,'\\','', [rfReplaceAll] );

end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt den PC-Namen
//******************************************************************************
function GetMachineName: string;
var
  Buf: array[0..MAX_COMPUTERNAME_LENGTH] of char;
  n: DWORD;
begin
  n := SizeOf(Buf) + 1;
  if not GetComputerName(Buf, n) then begin
     Result := '';
     exit;
  end;
  Result := StrPas(Buf);
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermiitelt den LogOn PC
//******************************************************************************
function GetLogOnServer: string;
var
  xRes:  NET_API_STATUS;
  xBuf: PWKSTA_USER_INFO_1;
begin
  Result := '';
  xRes := NetWkstaUserGetInfo(nil, 1, Pointer(xBuf));
  try
    if xRes = 0 then begin
       Result := WideCharToString(xBuf^.wkui1_logon_server);
    end;

  finally
    NetAPIBufferFree(xBuf);
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob der lokale PC sich in einer Workgroup befindet
//******************************************************************************
function IsWorkGroup :Boolean;
var xRet : Boolean;
begin
  xRet:= FALSE;
  try
    GetPrimaryDomainName;
  except
    xRet:= TRUE;
  end;
  result := xRet;
end;
//------------------------------------------------------------------------------
function GetOSMajorVersion:Word;
var xbuf: Pserver_info_101;
    xRes: NET_API_STATUS;
    xPointer: Pointer;
    xServerName: array[0..255] of WideChar;
    xVers :DWord;
    xMachineName : PChar;
begin

  xMachineName:= PChar(GetMachineName);

  FillChar(xServerName, sizeof(xServerName), 0);
  StringToWideChar(xMachineName, @xServerName, SizeOf(xServerName) );

  xRes := NetServerGetInfo(@xServerName, 101, xPointer);
  xBuf := Pserver_info_101( xPointer );
  try
    if xBuf <> nil then xVers := xBuf^.sv101_version_major AND MAJOR_VERSION_MASK;
  except
    try
     xVers := Win32MajorVersion; // Delphi Func.
    finally
    end;
  end;
  NetAPIBufferFree(xbuf);

  result := xVers;
end;
//------------------------------------------------------------------------------
function HasActiveDirectory:Boolean;
var xwindir : String;
    xbuffer : Array [0..255] of Char;
begin
  Result := FALSE;
  GetWindowsDirectory(xbuffer, SizeOf(xbuffer));
  xwindir := xbuffer;

  if DirectoryExists(xwindir + '\NTDS') then
     Result :=  DirectoryExists(xwindir + '\SysVol');
end;
//------------------------------------------------------------------------------
function ExistsNetWorkAdapter:Boolean;
var xAddres: TStrings;
    x,n,  xNr : Integer;
    xRet   : Boolean;
    xMAC   : String;
    xHex  : Byte;
    //-----

    xErr                  : DWORD;

    pAdapterInfo         : PIP_ADAPTER_INFO ;
    AdapterInfoSize      : DWORD;
    pAddrStr             : PIP_ADDR_STRING;

   // xMACID : Cardinal;

begin
   xRet := FALSE;

  AdapterInfoSize := 0;

  xErr := GetAdaptersInfo(NIL, AdapterInfoSize);
  if (xErr <> 0) then begin
    if (xErr <> ERROR_BUFFER_OVERFLOW) then begin
      raise Exception.CreateFmt('GetAdaptersInfo sizing failed with error %d', [xErr]
      );
    end;
  end;

  // Allocate memory from sizing information
  pAdapterInfo := PIP_ADAPTER_INFO(GlobalAlloc(GPTR, AdapterInfoSize));
  if not Assigned(pAdapterInfo) then begin
    raise Exception.Create('Memory allocation error' );
  end;

  // Get actual adapter information
  xErr := GetAdaptersInfo(pAdapterInfo, AdapterInfoSize);
  if (xErr <> 0) then begin
    raise Exception.CreateFmt('GetAdaptersInfo failed with error %d', [xErr]);
  end;


  for n:=0 to pAdapterInfo^.AddressLength-1 do
      if Integer(pAdapterInfo^.Address[n]) > 0 then begin
          xRet := TRUE;
          break;
      end;

  Result := xRet;
 {



  exit;
  xRet := FALSE;
  try
    xAddres:= TStringList.Create;
    GetMacAddresses(xMAC, xAddres);

    if xAddres.Count > 0 then begin
       xMAC := xAddres.Strings[0];
       showMessage('MAC : ' + xMAC);
    end else
       showMessage('No xAddres : ');

    for x:= 0 to xAddres.Count -1 do begin
        xMAC := xAddres.Strings[x];
        // '00-50-DA-4C-A8-8D' -> network dapter exists
        // '00-00-00-00-00-00' -> No network adapter
        xMAC := StringReplace(xMAC, '-' ,'',[rfReplaceAll]);
        for n:= 1 to  Length(xMAC)  do begin
           xHex := CharHex  (xMAC[n]) ;
           if xHex > 0 then begin
              xRet:= TRUE;
              break;
           end;
        end;
    end;
  finally
  xAddres.free;



  if xRet then showMessage('MAC : ' + xMAC);
    //NetAdapter existiert definitiv, wenn auch TCI/IP installiert ist
  if xRet then xRet:= ExistsTCPIP;

  if xRet then showMessage('TCPIP ok');
  end;
  Result := xRet;
  }
end;
//------------------------------------------------------------------------------
function ExistsTCPIP:Boolean;
var xRet  : Boolean;
    xKey, xKeyName : String;

    xWsaData: TWSAData;
begin
   xRet := FALSE;

//   case Winsock.WSAStartup($0101, xWsaData) of
   case Winsock.WSAStartup(2, xWsaData) of
        WSAEINVAL,
        WSASYSNOTREADY,
        WSAVERNOTSUPPORTED: xRet := false;
   else begin
          Winsock.WSACleanup;
          xRet := TRUE;
        end;
   end;

   {
   xKey := 'SYSTEM\CurrentControlSet\Services\Tcpip\ServiceProvider';
   xKeyName:='Name';
   try
       with TRegistry.Create do begin
            RootKey    := cRegLM;
            Access     := KEY_QUERY_VALUE or KEY_ENUMERATE_SUB_KEYS or KEY_EXECUTE ;
            OpenKey(xKey, FALSE);
            if ReadString(xKeyName) <> '' then xRet := TRUE;
            Free;
       end;
   finally
   end;
   }
   Result := xRet;
end;
//------------------------------------------------------------------------------
function GetLocalTCPIP:String;
begin
  result := HostNameToIP(GetMachineName);
end;
//------------------------------------------------------------------------------
function GetLocalSubnetMask:String;
var //xIP, xVal, xNetzID, xBroad : Integer;
    //xSubnetMask, xMask, xErr   : Integer;
    xErr                    : Integer;
    xSubMask                   : PChar;
    //xInAddr                    : TInAddr;  //Winsock
    xAdapterInfo               : PIP_ADAPTER_INFO ;   //IPTypes
    xAdapterInfoSize           : DWORD;
    xAddrStr                   : PIP_ADDR_STRING;    //IPTypes
begin

  try
      xErr := IpHlpApi.GetAdaptersInfo(NIL, xAdapterInfoSize);

      // Allocate memory from sizing information
      xAdapterInfo := PIP_ADAPTER_INFO( GlobalAlloc(GPTR, xAdapterInfoSize) );
      if not Assigned(xAdapterInfo) then begin
         result := '';
         exit;
      end;

      IpHlpApi.GetAdaptersInfo( xAdapterInfo, xAdapterInfoSize);
      xAddrStr := @xAdapterInfo^.IpAddressList;

      xSubMask := xAddrStr^.IpMask.S;
  except
     xSubMask:= '';
  end;

  result := xSubMask;
end;
//------------------------------------------------------------------------------
function GetMAC_Address:String;
var
  xErr               : DWORD;
  xAdapterInfo       : PIP_ADAPTER_INFO ;
  xAdapterInfoSize   : DWORD;
  xtext              : String;
  i                  : integer;
begin

  Result :=  '';
  xAdapterInfoSize := 0;
  xtext:= '';

  xErr := GetAdaptersInfo(NIL, xAdapterInfoSize);
  if (xErr <> 0) then begin
    if (xErr <> ERROR_BUFFER_OVERFLOW) then begin
      raise Exception.CreateFmt( 'GetAdaptersInfo sizing failed with error %s', [FormatErrorText(xErr)] );
    end;
  end;

  // Allocate memory from sizing information
  xAdapterInfo := PIP_ADAPTER_INFO(GlobalAlloc(GPTR, xAdapterInfoSize));
  if not Assigned(xAdapterInfo) then begin
    raise Exception.Create( 'Memory allocation error' );
  end;

  // Get actual adapter information
  xErr := GetAdaptersInfo(xAdapterInfo, xAdapterInfoSize);
  if (xErr <> 0) then begin
    raise Exception.CreateFmt('GetAdaptersInfo failed with error %s', [FormatErrorText(xErr)]);
  end;

  for i:=0 to xAdapterInfo^.AddressLength-1 do begin
    if (i = (xAdapterInfo^.AddressLength - 1)) then begin
       xtext := xtext + Format('%.2X', [Integer(xAdapterInfo^.Address[i])]);
    end else begin
       xtext := xtext + Format('%.2X-', [Integer(xAdapterInfo^.Address[i])]);
    end;
  end; // for
  Result :=  xtext;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob eine Texnet-Karte installiert ist
//Mit dem Treiber PCI9030.sys ab Vers. 4.x kann so die Texnetkarte nicht mehr
//ermittelt werden. (PCI9030 sthet nicht in Liste  xDeviceList; 27.07.2004)
//******************************************************************************
function ExistsTexNetAdapter: Boolean;
var
  xID: integer;
  xCountChars, xRet: integer;
  xBuffer: string;
  xBufferLines: string;
  xDeviceList: TStringList;
  xPos: integer;
  xError: DWORD;
  xBool : Boolean;

const cTexnet0 = 'PCI9030';
      cTexnet1 = 'Texnet';

begin

  xBool := FALSE;

  xDeviceList := TStringList.Create;

  try
    xRet:= 0;
    xCountChars:= 1;

    //Alle Devics auslesen
    repeat
      SetLastError(0);
      xError:= 0;

      SetLength(xBuffer, xCountChars);
      xRet:= QueryDosDevice( nil,
                             PChar(xBuffer),
                             xCountChars
                           );
      xError:= GetLastError;
      xCountChars:= xCountChars * 2;
    until (xError <> ERROR_MORE_DATA) and (xError <> ERROR_INSUFFICIENT_BUFFER);

    SetLength(xBuffer, xRet);

    xBufferLines:='';
    xPos:=Pos(#0, xBuffer);
    while xPos>0 do
      try
        xBufferLines:= xBufferLines + Copy( xBuffer, 1, xPos-1) + #13#10;
        Delete(xBuffer, 1, xPos);
      finally
        xPos:= Pos(#0, xBuffer);
      end; //
    xDeviceList.Text:= xBufferLines;

    with xDeviceList do
         {
         for xID:= Count-1 downto 0 do
              if ( Pos( cTexnet0, Uppercase( Strings[xID] ) ) <> 0 ) or
                 ( Pos( cTexnet1, Uppercase( Strings[xID] ) ) <> 0 ) then
                  Delete(xID);
           }
        for xID:= Count-1 downto 0 do
              if ( Pos( cTexnet0,  Strings[xID]  ) > 0 ) or
                 ( Pos( cTexnet1,  Strings[xID]  ) > 0 ) then begin
                 xBool := TRUE;
                 break;
              end;

//    xBool := xDeviceList.Count > 0;
  finally
    xDeviceList.Free;
  end;

  //ab PCI9030 Ver. 4.x
  if not xBool then begin
     xBool := IsTexNetDriverInstalled;  //Ermittlung mittels Service
  end;
  Result := xBool;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob eine Texnet-Karte installiert ist
//******************************************************************************
(*
function ExistsTexNetAdapter: Boolean;
var xKey : String;
    xDevices : string;
    xSize    : dword;
    x        : integer;
    xKeyNames  : TStringList;

const cTexnet0 = 'PCI9030';
      cTexnet1 = 'Texnet';
      cSize    = 5000;

   {
    function FindRegKey(aKey : String):Boolean;
      var n          : Integer ;
          xKeyNames  : TStrings;
          xRet       : Boolean;
    begin
       xRet := FALSE;
       with TRegistry.Create do begin
            RootKey    := cRegLM;
            Access     := KEY_QUERY_VALUE or KEY_ENUMERATE_SUB_KEYS or KEY_EXECUTE ;

            xKeyNames  := TStringList.Create;

            OpenKey(aKey, FALSE);

            if (AnsiStrIComp( PChar(ReadString('Service')),'PCI9030') = 0) or
               (AnsiStrIComp( PChar(ReadString('Service')),'TexNet') = 0) then begin
                xRet  := TRUE;
            end else begin
               GetKeyNames(xKeyNames);

               for n:= 0 to xKeyNames.Count-1 do begin
                   xRet := FindRegKey(aKey + '\' + xKeyNames.Strings[n]);
                   if xRet then break;
               end;
            end;
            xKeyNames.free;
            Result := xRet;
            free;
       end;
    end;
    }
begin

  Result := FALSE;
  setlength(xDevices, cSize);
  xSize := QueryDosDevice(nil, @xDevices[1], cSize);

  for x := 1 to xSize do
    if xDevices[x] = #0 then xDevices[x] := #10;

  xDevices := Uppercase(xDevices);

  x := AnsiPos(Uppercase(cTexnet0), xDevices);
    if x > 0  then Result := TRUE;

  x := AnsiPos(Uppercase(cTexnet1), xDevices);
       if x > 0  then Result := TRUE;


  //Nochmal ueberpruefen -> API QueryDosDevice nicht zuverlaessig !!!
  if not Result then begin
     xKeyNames  := TStringList.Create;
     xKeyNames.Sorted:= TRUE;

     with TRegistry.Create do
     try
          RootKey  := cRegLM;
          OpenKeyReadOnly('SYSTEM\CurrentControlSet\Services');
          GetKeyNames(xKeyNames);

          for x:= 0 to xKeyNames.Count -1 do begin

              if (AnsiCompareText(cTexnet0, xKeyNames.Strings[x] ) = 0) then
                  Result := TRUE
              else if (AnsiCompareText(cTexnet1, xKeyNames.Strings[x] ) = 0) then begin
                  if ISATexnetAdapterCout > 0 then
                     Result := TRUE;
              end;
              if Result then break;
          end;
     finally
          free;
          xKeyNames.Free;
     end;
  end;


   //xKey := 'SYSTEM\CurrentControlSet\Enum\PCI';
   //Result := FindRegKey(xKey);
end;
//------------------------------------------------------------------------------
*)



//******************************************************************************
//Ueberprueft ob ein Service Texnet oder PCI930 in der Reg. existiert
//******************************************************************************
function ExistsTexnetRegistryService: Boolean;
var x         : integer;
    xKeyNames : TStringList;

const cTexnet0 = 'PCI9030';
      cTexnet1 = 'Texnet';

begin

  Result := FALSE;

  xKeyNames  := TStringList.Create;
  xKeyNames.Sorted:= TRUE;

  with TRegistry.Create do
  try
     RootKey  := cRegLM;
     OpenKeyReadOnly('SYSTEM\CurrentControlSet\Services');
     GetKeyNames(xKeyNames);

     for x:= 0 to xKeyNames.Count -1 do begin

         if (AnsiCompareText(cTexnet0, xKeyNames.Strings[x] ) = 0) then
             Result := TRUE
         else if (AnsiCompareText(cTexnet1, xKeyNames.Strings[x] ) = 0) then begin
            if ISATexnetAdapterCout > 0 then
               Result := TRUE;
         end;
         if Result then break;
     end;
  finally
     free;
     xKeyNames.Free;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob die Texnet-Karte aktiv ist
//******************************************************************************
function IsTexNetDriverRunning: Boolean;
var  xRet : Boolean;
     xManager, xService : SC_Handle;
     xServiceStatus     : TServiceStatus;
     xServiceName : String;
begin
  xRet := FALSE;
  try
      xServiceName := 'PCI9030';
      xManager:=OpenSCManager( '' ,nil, SC_MANAGER_CONNECT);

      if xManager > 0 then begin
        xService := OpenService(xManager,PChar(xServiceName),
                            SERVICE_QUERY_STATUS);

        QueryServiceStatus(xService, xServiceStatus);
        if xServiceStatus.dwCurrentState = SERVICE_RUNNING	then  xRet := TRUE;
      end;

      xServiceName := 'Texnet';
      if xRet = FALSE then begin
         if xManager > 0 then begin
            xService := OpenService(xManager,PChar(xServiceName),
                                SERVICE_QUERY_STATUS);

            QueryServiceStatus(xService, xServiceStatus);
            if xServiceStatus.dwCurrentState = SERVICE_RUNNING	then  xRet := TRUE;
          end;
      end;


  except
     //on E: Exception do Raise
     raise;
  end;
  CloseServiceHandle(xManager);
  result := xRet;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob der Treiber der Texnet-Karte istalliert ist
//******************************************************************************
function IsTexNetDriverInstalled: Boolean;
var  xRet : Boolean;
     xManager, xService : SC_Handle;
     xServiceStatus     : TServiceStatus;
     xServiceName : String;
begin
  xRet := FALSE;
  xServiceName := 'PCI9030';
  xManager:=OpenSCManager( '' ,nil, SC_MANAGER_CONNECT);
  if xManager > 0 then begin
    if OpenService(xManager,PChar(xServiceName),
                        SERVICE_QUERY_STATUS) > 0 then xRet := TRUE
    else begin
      xServiceName := 'Texnet';
      if OpenService(xManager,PChar(xServiceName),
                     SERVICE_QUERY_STATUS) > 0 then xRet := TRUE
    end;
  end;
  CloseServiceHandle(xManager);
  result := xRet;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt die Anzahl installierten ISA-Texnetkarten
//******************************************************************************
function ISATexnetAdapterCout:integer;
var xRootKey, xRootPath : String;
    x, xIRQ, xAddr, xCount : Integer;
    xAdapterxList, xList : TStringList;
begin
  xAdapterxList := TStringList.Create;
  xList := TStringList.Create;
  xCount := 0;

  xRootKey := '\System\CurrentControlSet\Services\TexNet\Parameters';
  with TRegistry.Create do begin
     Access  :=  KEY_READ;
     RootKey := cRegLM;
     OpenKey(xRootKey, FALSE);
     GetKeyNames(xList);
     Free;
  end;

  for x:= 0 to xList.count -1 do begin
      if AnsiPos('DEVICE', UpperCase( xList.Strings[x] ) ) >0 then
      xAdapterxList.Add(xList.Strings[x]);
  end;

  for x:= 0 to xAdapterxList.Count - 1 do begin
      xRootPath := Format('%s\%s',[xRootKey,xAdapterxList.Strings[x] ]);
      xIRQ  := GetRegInteger(cRegLM, xRootPath, 'IRQ', 0);
      xAddr := GetRegInteger(cRegLM, xRootPath, 'BaseAddress', 0);
      if (xIRQ >0) and (xAddr >0) then inc(xCount);
  end;

  xAdapterxList.free;
  xList.free;

  Result:= xCount;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob das Distinct fuer AC338 istalliert ist
//******************************************************************************
function ExistsDistinct:Boolean;
var xKey,  xName, xValue, xVers : String;
    xRet : Boolean;
begin
   xRet := FALSE;
   with TRegistry.Create do begin
     RootKey := cRegLM;
     xKey := 'SOFTWARE\Distinct Corporation';
     if KeyExists( xKey ) = TRUE then begin
         xKey := 'SOFTWARE\Distinct\SerialNumber';
         if KeyExists( xKey ) = TRUE then begin
            if OpenKey(xKey,False) then begin
               //Version 3
               if (ReadString('ExtensionKey') <> '') and
                  (ReadString('ExtensionNumber') <> '') then  xRet := TRUE;

               //ab Version 4
               if (ReadString('CRPCServerKey') <> '') and
                  (ReadString('CRPCServerNumber') <> '') then  xRet := TRUE;  

            end;
         end;
      end;

     Free;
   end;
   result := xRet;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt die OS Informationen (ein Record wird abgef�llt)
//******************************************************************************
function GetOSInfo: TOSVersionInfoEx;
var xOSVersionInfoEx : TOSVersionInfoEx;
begin
  xOSVersionInfoEx.OSVersionInfo.dwOSVersionInfoSize := SizeOf(xOSVersionInfoEx);
  GetVersionEx(xOSVersionInfoEx.OSVersionInfo);
  result := xOSVersionInfoEx;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt die OS Plattform  (NT, W2K, XP etc.)
//******************************************************************************
function GetOSType: TOSType;
 var xProdID, xMajorVers, xMinorVers,
     xPlatformId, xProductType, xSuiteMask : Word;
     xOSType: TOSType;
     xOSVersionInfoEx : TOSVersionInfoEx;
begin

  try
    xOSVersionInfoEx:= GetOSInfo;

    xMajorVers   := xOSVersionInfoEx.OSVersionInfo.dwMajorVersion;
    xMinorVers   := xOSVersionInfoEx.OSVersionInfo.dwMinorVersion;
    xPlatformId  := xOSVersionInfoEx.OSVersionInfo.dwPlatformId;
    xProductType := xOSVersionInfoEx.wProductType;
    xSuiteMask   := xOSVersionInfoEx.wSuiteMask;


    case xMajorVers of
       3 : if xMinorVers = 51 then
              xOSType := osNT31
           else
              xOSType :=osUnknown;

       4  : begin
             if xPlatformId = VER_PLATFORM_WIN32_NT  then
                case xMinorVers of
                   0:xOSType :=osWin95;
                  10: xOSType :=osWin98;
                  90: xOSType :=osME;
                else
                    xOSType :=osUnknown;
                end;

             //NT 4
             if (xPlatformId = VER_PLATFORM_WIN32_NT) and ( xMinorVers = 0 )then
                if xProductType = VER_NT_WORKSTATION then
                   xOSType := osWinNTWKS
                 else
                   xOSType := osWinNTSRV;
            end;

       5 :   case xMinorVers of
                  0: begin
                        if xProductType = VER_NT_WORKSTATION then
                           xOSType := osW2kProf
                        else
                           xOSType := osW2kSrv;
                     end;

                  1: begin
                        if xProductType = VER_NT_WORKSTATION then begin
                           if (xSuiteMask and VER_SUITE_PERSONAL) = VER_SUITE_PERSONAL then
                               xOSType := osXPHome
                           else
                               xOSType := osXPPro;
                        end;
                     end;
                  2: xOSType := osW2003SRV;
             end;
        6 :  case xMinorVers of
                  0: begin
                        if xProductType = VER_NT_WORKSTATION then
                           xOSType := osVista
                        else
                           xOSType := osServer2008;
                     end;
                  else
                     xOSType :=osUnknown;
             end;

    else
       xOSType :=osUnknown;
    end;
  except
    xOSType :=osUnknown;
  end;

  Result := xOSType;
end;
//------------------------------------------------------------------------------
function GetOSServicePack: Word;
//var xOSVersionInfoEx : TOSVersionInfoEx;
begin
  {
  GetOSInfo.wServicePackMajor
  xOSVersionInfoEx.OSVersionInfo.dwOSVersionInfoSize := SizeOf(xOSVersionInfoEx);
  GetVersionEx(xOSVersionInfoEx.OSVersionInfo);

  Result := xOSVersionInfoEx.wServicePackMajor;
  }
  Result :=     GetOSInfo.wServicePackMajor;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Stellt eine Verbindung zu einem RemotePC her -> fuer AddUser/AddGroups
//******************************************************************************
function MachineConnection(aMachine, aUserName, aPWD:String):Boolean;
var
  NetResource: TNetResource;
  xret : DWord;
  xText : String;
  xPCName :String;
  xUserName, xPWD : String;
begin
  Result := FALSE;

  try
    xPCName :=StringReplace(aMachine, '\\' , '', [rfReplaceAll]);
    xPCName := '\\' + xPCName ;

    { fill TNetResource record structure }
    NetResource.dwType       := RESOURCETYPE_ANY;  //RESOURCETYPE_DISK;
    NetResource.lpLocalName  := ''; //  'x:'
    NetResource.lpRemoteName :=  PChar(xPCName);
    NetResource.lpProvider   := '';

  //  xPWD := 'kempten' ;
  //  xUserName := 'Administrator';
    //xPWD := '' ;
    //xUserName := '';

    xPWD := aPWD ;
    xUserName := aUserName;

    xret := WNetAddConnection2(NetResource,
                               PChar(xPWD),            //Password or empty
                               PChar(xUserName),       //User name o empty
                               CONNECT_UPDATE_PROFILE);

    if xret = NO_Error then Result := TRUE;
  except
    on E: Exception do
       xret := e.HelpContext;
  end;

  SetLastError(xret);
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Trennt eine RemotePC Verbindung
//******************************************************************************
function MachineDisconnection(aMachine:String):Boolean;
var xret    : DWord;
    xText   : String;
    xPCName : String;
begin
  Result := FALSE;

  xPCName :=StringReplace(aMachine, '\\' , '', [rfReplaceAll]);
  xPCName := '\\' + xPCName;

  xret := WNetCancelConnection2( PChar(xPCName),         // pointer to resource name to disconnect
                                 CONNECT_UPDATE_PROFILE, // connection type flags
                                 TRUE                    // flag for unconditional disconnect
                               );

  if xret = NO_Error then Result := TRUE;

  SetLastError(xret);
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt den Maschinentyp (WKS, PDC, Server etc.)
//******************************************************************************
function GetMachineType: TMachineType;
var xProdID: Word;
    xMachineType : TMachineType;
    xDomain, xDCName, xLogonPC :String;
begin
try
  xProdID := GetOSInfo.wProductType;

  case xProdID of
     VER_NT_WORKSTATION       :  if IsWorkGroup then
                                    xMachineType :=  maWKSWorkgroup
                                 else
                                    xMachineType :=  maWKS;
     VER_NT_DOMAIN_CONTROLLER :  xMachineType :=  maSRVActiveDir;
     VER_NT_SERVER            :  if IsWorkGroup then
                                    xMachineType := maSRVWorkgroup
                                 else begin
                                    if IsWorkGroup then
                                       //Nicht in Domaine eingeloggt
                                       xMachineType := maSRVWorkgroup
                                    else
                                       xMachineType :=  maSRVAdd;
                                 end;
     else
       xMachineType :=mmUnknown;
  end;
  Result := xMachineType;
except
  Result := mmUnknown;
end;

end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob das ActiveDirecory installiert ist -> Domain Controller
//******************************************************************************
function GetActiveDirecory: Boolean;
var xwindir : String;
    xbuffer : Array [0..255] of Char;
begin
  Result := FALSE;
  try
    GetWindowsDirectory(xbuffer, SizeOf(xbuffer));
    xwindir := xbuffer;

    if DirectoryExists(xwindir + '\NTDS') then
       Result :=  DirectoryExists(xwindir + '\SysVol')
  except
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob das Multilanguage installiert ist
//******************************************************************************
function IsMultiLanguagePack: Boolean;
var  xKey, xValue, xRet, xDir: String;
     xList : TStrings;
begin
  Result := FALSE;
  xList  := TStringList.Create;
  xKey:= 'SYSTEM\CurrentControlSet\Control\Nls\MUILanguages';
  try
    with TRegistry.Create do begin
        RootKey := HKEY_LOCAL_MACHINE;
        if OpenKeyReadOnly(xKey) then begin
           GetValueNames(xList);
           if xList.Count > 0 then  Result := TRUE;
        end;
    end;
  except
    Result := FALSE;
  end;
  xList.Free;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt die OS-Sprache als ID
//******************************************************************************
function GetOSLanguageID: DWORD;
var xValue : String;
    xID : Word;
    xlid : LANGID;
begin
  try
     xValue:= GetWindowsSystemFolder + '\Kernel32.dll';

     with JclFileUtils.TJclFileVersionInfo.Create(xValue) do begin
        xID:=Translations[0].LangId;
        Free;
     end;
    xlid:= xID and $3ff; // -> LANG_ENGLISH, LANG_GERMAN etc.
  except
     xlid := 0;
  end;

  Result := xlid;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt die OS-Sprache als Text;
//Eingabe: Hex-Wert  (Ger. CH = $807,   Ger. = $407 -> Microsoft language identifier
//******************************************************************************
function GetOSLanguageAsText(aLangID: LANGID):String; overload;
var
  R: DWORD;
begin
  try
    SetLength(Result, 255);
    R := VerLanguageName(WORD(aLangID), PChar(Result), 255);
    SetLength(Result, R);
  except
  end;
end;
//------------------------------------------------------------------------------
function GetOSLanguageAsText:String; overload;
var xID : Word;
    xText :String;
begin
 try
    xText:= GetWindowsSystemFolder + '\Kernel32.dll';

    with TJclFileVersionInfo.Create(xText) do begin
       xID:=Translations[0].LangId;
       xText:= VersionLanguageName(xID);
       Free;
    end;

  finally
    Result:= xText;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ueberprueft eine Verbindung zum SQL-Server
//******************************************************************************
function CheckSQLConnection(aServerName, aSQLUser, aPWD: String):Boolean;
var xSQLServer: SQLServer2;
    xRet : Boolean;
begin
  xRet:= FALSE;
  try
    xSQLServer := CoSQLServer2.Create;
    xSQLServer.Connect(aServerName, aSQLUser, aPWD);
    xRet:= TRUE;
    xSQLServer.DisConnect;
    xSQLServer:= NIL;
  except
    xSQLServer:= NIL;
  end;
  Result :=  xRet ;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt die SQL-Server Version (7 = NT-SQL, 8 = SQL2000 oder MSDE2000)
//******************************************************************************
function GetSQLServerVersion:Integer; overload;
var xPath        : HKEY;
    xKey, xValue, xMajorVers : String;
    x, xSerial   : Integer;
begin
  Result := -1;

  try
      xPath  := cRegLM;
      xKey   := 'SOFTWARE\Microsoft\MSSQLServer\MSSQLServer\CurrentVersion';
      xMajorVers := GetRegString(xPath,  xKey, 'CurrentVersion', '0');

      xSerial :=  GetRegInteger(xPath,  xKey, 'SerialNumber', 0); // MSDE2000 -> SerialNumber ='';
      xSerial:= abs(xSerial);

      x:= pos('.', xMajorVers);
      if x> 0 then xMajorVers:= Copy(xMajorVers, 1, x-1);
      try
         if xSerial > 0 then
            Result := StrToInt(xMajorVers);
      except
         Result := -1;
      end;
  finally
  end;
end;
//------------------------------------------------------------------------------
function GetSQLServerVersion(aServerName, aSQLUser, aPWD:String):Integer; overload;
var xSQLServer: SQLServer2;
    xVers  : Word;
begin
  try
   xSQLServer := CoSQLServer2.Create;
  except
      on e: Exception do begin
         Result := -1;
         xSQLServer := Nil;
         exit;
      end
  end;

  with xSQLServer do
    try
      xSQLServer.Connect(aServerName, aSQLUser, aPWD);

      xVers := xSQLServer.VersionMajor;
      Result := xVers;

      xSQLServer := Nil;
    except
      on e: Exception do begin
         Result := -1;
         xSQLServer := Nil;
         raise;
      end;
    end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt das SQL-Server ServicePack
//******************************************************************************
function GetSQLServerSP(aServerName, aSQLUser, aPWD:String):Integer;
var xSQLServer: SQLServer2;
    xSPTxt, xValue :String;
    xSP, x    : Word;
begin
  xValue := '0';

  try
     xSQLServer := CoSQLServer2.Create;
  except
      on e: Exception do begin
         Result := -1;
         xSQLServer := Nil;
         exit;
      end
  end;

  with xSQLServer do
    try

      xSQLServer.Connect(aServerName, aSQLUser, aPWD);
      xSPTxt := xSQLServer.ProductLevel;
      xSQLServer := Nil;

      for x:= 1 to length(  xSPTxt ) do
          if (xSPTxt[x] > '0') and (xSPTxt[x] < '9') then
              xValue := xValue + xSPTxt[x];

      Result := StrToInt(xValue);

    except
      on e: Exception do begin
         Result := -1;
         xSQLServer := Nil;
         raise;
      end;
    end;    
end;
//------------------------------------------------------------------------------
function GetSQLServerSPAsText(aServerName, aSQLUser, aPWD:String):String;
var xSQLServer: SQLServer2;
begin

  try
    xSQLServer := CoSQLServer2.Create;
  except
      on e: Exception do begin
         Result := '';
         xSQLServer := Nil;
         exit;
      end
  end;

  with xSQLServer do
    try
      xSQLServer.Connect(aServerName, aSQLUser, aPWD);
      Result := xSQLServer.ProductLevel;
      xSQLServer := Nil;
    except
      on e: Exception do begin
         Result := '';
         xSQLServer := Nil;
         raise;
      end;
    end;

end;
//------------------------------------------------------------------------------



//******************************************************************************
//Ermittelt die SQL-Server Type (MSDE2000, Standard, Desktop)
//******************************************************************************
function GetSQLServerType(aServerName, aSQLUser, aPWD:String): TSQLType;
var xSQLServer: SQLServer2;
begin
  try
    xSQLServer := CoSQLServer2.Create;
  except
      on e: Exception do begin
         Result := sqlUnknown;
         xSQLServer := Nil;
         exit;
      end
  end;

  with xSQLServer do
    try
      xSQLServer.Connect(aServerName, aSQLUser, aPWD);

      case xSQLServer.IsPackage of
         SQLDMO_Unknown    :  Result := sqlUnknown;
         SQLDMO_MSDE       :  Result := sqlMSDE;
         SQLDMO_STANDARD   :  Result := sqlStandard;
         SQLDMO_OFFICE     :  Result := sqlDesktop;
         SQLDMO_ENTERPRISE :  Result := sqlEntreprise;
         else
            Result := sqlUnknown;
      end;

      xSQLServer := Nil;
    except
      on e: Exception do begin
         Result := sqlUnknown;
         xSQLServer := Nil;
         raise;
      end;
    end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob eine MM_WinidngDB vorhanden ist
//******************************************************************************
function ExistsMMWindingDB(aServerName, aSQLUser, aPWD: String): Boolean;
var xSQLServer: SQLServer2;
    xText :String;
    x : Integer;
begin
  Result := FALSE;

  try
    xSQLServer := CoSQLServer2.Create;
  except
      on e: Exception do begin
         Result := FALSE;
         xSQLServer := Nil;
         exit;
      end
  end;

  with xSQLServer do
    try
      xSQLServer.LoginTimeout := 10;
      xSQLServer.AutoReconnect := True;
      xSQLServer.Connect(aServerName, aSQLUser, aPWD);

      for x:=  1 to  xSQLServer.Databases.Count do
          if CompareText(xSQLServer.Databases.ItemByID(x).Name, cMMWinding) = 0 then begin
             Result := TRUE;
             break;
           end;

      xSQLServer := Nil;
    except
      on e: Exception do begin
         Result := FALSE;
         xSQLServer := Nil;
         raise;
      end;
    end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt, ob der SQL-Server l�uft
//******************************************************************************
function IsSQLServerRunning(aHostName, aSQLUser, aPWD: String):Boolean;
var xSQLServer: SQLServer2;
    xText :String;
    x : Integer;
    xSQLDMO_SVCSTATUS_TYPE : SQLDMO_SVCSTATUS_TYPE;
begin
  Result := FALSE;

  try
    xSQLServer := CoSQLServer2.Create;
  except
    on e: Exception do begin
          xSQLServer := Nil;
          xText := Format('Error in Netware.IsSQLServerRunning(); CoSQLServer2.Create. Error msg: %s',[aHostName, aSQLUser, e.Message]);
          CodeSite.SendError(xText);
          Result := FALSE;
          exit;
    end;
  end;

  with xSQLServer do
    try
      xSQLServer.Name := aHostName;
      xSQLServer.Login := aSQLUser;
      xSQLServer.Password := aPWD;
      xSQLServer.LoginTimeout := 10;

      xSQLServer.Connect(aHostName, aSQLUser, aPWD);
      if xSQLServer.Status = SQLDMOSvc_Running then Result := TRUE;
      xSQLServer.DisConnect;
      xSQLServer := Nil;
    except
      on e: Exception do begin
         Result := FALSE;
         xSQLServer.DisConnect;
         xSQLServer := Nil;
         xText := Format('Error in IsSQLServerRunning(). SQL Host: %s, SQL User: %s. Error msg: %s',[aHostName, aSQLUser, e.Message]);
         CodeSite.SendError(xText);
         //raise;
      end;
    end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt die Broadcast Adresse anhand einer IP-Adresse im Netz (Firmennetz)
//******************************************************************************
function GetBroadcastIP(aIPAddr: string): string;
var xIP, xVal, xNetzID, xBroad : Integer;
    xSubnetMask, xMask, xErr   : Integer;
    xSubMask                   : PChar;
    xInAddr                    : TInAddr;  //Winsock
    xAdapterInfo               : PIP_ADAPTER_INFO ;   //IPTypes
    xAdapterInfoSize           : DWORD;
    xAddrStr                   : PIP_ADDR_STRING;    //IPTypes
begin

  xErr := IpHlpApi.GetAdaptersInfo(NIL, xAdapterInfoSize);

  // Allocate memory from sizing information
  xAdapterInfo := PIP_ADAPTER_INFO( GlobalAlloc(GPTR, xAdapterInfoSize) );
  if not Assigned(xAdapterInfo) then begin
     result := '';
     exit;
  end;

  IpHlpApi.GetAdaptersInfo( xAdapterInfo, xAdapterInfoSize);
  xAddrStr := @xAdapterInfo^.IpAddressList;

  xSubMask :=xAdapterInfo^.Description;

  xSubMask := xAddrStr^.IpMask.S;

  xIP         := inet_addr( PChar(aIPAddr) );
  xSubnetMask := inet_addr( xSubMask );
  xMask       := inet_addr('255.255.255.255');


  xNetzID :=  xIP and xSubnetMask;
  xVal    :=  xSubnetMask xor xMask;
  xBroad  :=  xNetzID or xVal;

  xInAddr.S_addr :=  xBroad;
  result := Winsock.inet_ntoa(xInAddr);

{
  Ben�tigt wird die IP und die Subnetmask.
1. Schritt: AND-Vergleich von IP mit Subnetzmask -> NetzID
2. Schritt: XOR-Vergleich von Subnetmask mit 255.255.255.255 ->  Zwischenergebnis
3. Schritt: OR-Vergleich von NetzID mit Zwischenergebnis von Schritt 2 -> Broadcast-Adresse

   ->
   NetzID : 150.158.148.132
   Mask :  255.255.255.0

   Zwischenergebnis = Mask xor 255.255.255.255 = 0.0.0.255
   NetzID or  Zwischenergebnis = 150.158.148.255
}

end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittelt den Hostnamen anhand einer IP-Adresse im Firmennetz
// (Im Internet, nur Domains)
//******************************************************************************
function IPToHostName(aIPAddr: string): string;
var
  xSockAddrIn       : TSockAddrIn;
  xHostEnt          : PHostEnt;
  xWSAData          : TWSAData;
  xVersionRequested : WORD;
begin
  //Nur mit Winsock.pas
  xVersionRequested := MAKEWORD( 2, 0 );
  WSAStartup(xVersionRequested, xWSAData);

  xSockAddrIn.sin_addr.s_addr:= inet_addr(PChar(aIPAddr));

  xHostEnt:= GetHostByAddr( @xSockAddrIn.sin_addr.S_addr, Length(aIPAddr), AF_INET);

  if xHostEnt <> NIL then
    Result:=StrPas(xHostent^.h_name)

  else
    Result:='';
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt die IP-Adresse von einem Host
//******************************************************************************
function HostNameToIP(aPCName: string): string;
var
  xHostName         : array[0..255] of Char;
  xAddr             : PChar;
  xHostEnt          : PHostEnt;
  xWSData           : TWSAData;
  xVersionRequested : Word;
begin

  xVersionRequested := MAKEWORD( 2, 0 );
  WSAStartup(xVersionRequested, xWSData);
  try
     GetHostName(xHostName, SizeOf(xHostName));

    StrPCopy(xHostName, aPCName);
    xhostEnt := GetHostByName(xHostName);
    if Assigned(xHostEnt) then
      if Assigned(xHostEnt^.H_Addr_List) then begin
        xAddr := xHostEnt^.H_Addr_List^;
        if Assigned(xAddr) then begin
           Result := Format('%d.%d.%d.%d', [Byte(xAddr[0]), Byte(xAddr[1]),
                            Byte(xAddr[2]), Byte(xAddr[3])]);

        end else
          Result := '';
      end else
        Result := ''
    else begin
      Result := '';
    end;
  finally
    WSACleanup;
  end
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt den Netzwerkkarten-Namen
//******************************************************************************
function GetNetworkAdapterName:String;
var
  xErr             : DWORD;
  xAdapterInfo     : PIP_ADAPTER_INFO ;
  xAdapterInfoSize : DWORD;
begin

  Result := '';
  xAdapterInfoSize := 0;
  try
      xErr := GetAdaptersInfo(NIL, xAdapterInfoSize);
      if (xErr <> 0) then begin
        if (xErr <> ERROR_BUFFER_OVERFLOW) then begin
          raise Exception.CreateFmt( 'GetAdaptersInfo sizing failed with error %d', [xErr] );
          exit;
        end;
      end;

      // Allocate memory from sizing information
      xAdapterInfo := PIP_ADAPTER_INFO(GlobalAlloc(GPTR, xAdapterInfoSize));
      if not Assigned(xAdapterInfo) then begin
        raise Exception.Create('Memory allocation error');
        exit;
      end;

      // Get actual adapter information
      xErr := GetAdaptersInfo(xAdapterInfo, xAdapterInfoSize);
      if (xErr <> 0) then begin
        raise Exception.CreateFmt('GetAdaptersInfo failed with error %d', [xErr]);
        exit;
      end;

      Result := Format('%s : %s',[GetAdaterTypeName(xAdapterInfo^.Type_),
                                                    xAdapterInfo^.Description] ) ;
  except
    Result := '';
  end;

end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt den Netzwerkadappter Typ   (GetNetworkAdapterName)
//******************************************************************************
function GetAdaterTypeName(Type_: Uint): string;
begin
  case Type_ of
    MIB_IF_TYPE_ETHERNET: begin
      result:='Ethernet adapter';
    end;
    MIB_IF_TYPE_TOKENRING: begin
      result:='Token Ring adapter';
    end;
    MIB_IF_TYPE_FDDI: begin
      result:='FDDI adapter';
    end;
    MIB_IF_TYPE_PPP: begin
      result:='PPP adapter';
    end;
    MIB_IF_TYPE_LOOPBACK: begin
      result:='Loopback adapter';
    end;
    MIB_IF_TYPE_SLIP: begin
      result:='Slip adapter';
    end;
    else begin
      result:='Unknow Adapter type';
    end;
  end; // END case
end;
//------------------------------------------------------------------------------
function CheckConnectionTo(aHost:String; var aMsg :String):Boolean;
begin
  try

    Result := FALSE;
    if aHost = '' then begin
       aMsg   := 'Host string is empty';
       exit;
    end;

    aHost := StringReplace(aHost, '.', GetMachineName, [rfReplaceAll]);

    with TIdIcmpClient.Create(NIL) do begin
           Host := aHost;
           Port := 80;
           Ping;
           case ReplyStatus.ReplyStatusType of
             rsEcho:           begin

                                  aMsg:= Format('Response from host %s (IP: %s) in %d millisec.',
                                                              [aHost,
                                                               ReplyStatus.FromIpAddress,
                                                               ReplyStatus.MsRoundTripTime] );

                                  Result := TRUE;
                                end;


             rsError:            aMsg:= 'Unknown error.';
             rsTimeOut:          aMsg:= 'Timed out.';
             rsErrorUnreachable: aMsg:= Format('network host %s (IP: %s) unreachable.',
                                               [aHost,
                                                ReplyStatus.FromIpAddress]);

             rsErrorTTLExceeded: aMsg:= Format('Hope %d %s: TTL expired.', [TTL, ReplyStatus.FromIpAddress]);
           end;
           Free;
    end;
  except
    on E: Exception do begin
       aMsg:= e.Message;
       Result := FALSE;
    end;
  end;
end;
//------------------------------------------------------------------------------
function ExistsSQLServerOnMachine:Boolean;
var xbuf: Pserver_info_101;
    xRes: NET_API_STATUS;
    xPointer: Pointer;
    xServerName: array[0..255] of WideChar;
    xVers :DWord;
    xMachineName : PChar;
    xRet : Boolean;
begin
  xRet := False;
  xMachineName:= PChar(GetMachineName);

  FillChar(xServerName, sizeof(xServerName), 0);
  StringToWideChar(xMachineName, @xServerName, SizeOf(xServerName) );

  xRes := NetServerGetInfo(@xServerName, 101, xPointer);
  xBuf := Pserver_info_101( xPointer );
  try
    if xBuf <> nil then
      if xBuf^.sv101_type AND SV_TYPE_SQLSERVER	= SV_TYPE_SQLSERVER then
        xRet := TRUE;
  except
  end;
  NetAPIBufferFree(xbuf);

  result := xRet;
end;
//------------------------------------------------------------------------------



initialization
  CoInitialize(nil);

finalization
  CoUninitialize;
  
end.

