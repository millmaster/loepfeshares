(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMMessages.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: MillMaster Meldungen fuer MMClient
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 05.10.1998  0.00  Sdo | Projekt erstellt
|=============================================================================*)
unit MMMessages;

interface

uses  Messages, Windows, SysUtils, StdCtrls;


const

  //Adresse
  cMsgAppl           = 'MMApplications';
  cMsgClient         = 'MMClient';         // Meldung zu MMClient  (ID)


  // Meldungstyp (SendMessages)
  WM_MMClientRead     = WM_USER + 1;
  WM_MMClientClose    = WM_USER + 2;
  WM_MMClientMinimize = WM_USER + 3;
  WM_MMClientStop     = WM_USER + 4;
  WM_MMClientStart    = WM_USER + 5;
  WM_ShowClientMsg    = WM_USER + 6;
  WM_UpdateFloor      = WM_USER + 7;


  // Meldung wParam   MMClient
  cMMStarted   = 0;
  cMMStarting  = 1;
  cMMStopped   = 2;
  cMMStopping  = 3;
  cMMAlert     = 4;
  cMMError     = 5;
  cMMGetState  = 6;
  cMMDebugg    = 7;
  cMMRefresh   = 8;
  cMMSystemInfo   = 9;
  cMMAquisation   = 10;

  // An Applikationen
  cMsgLoginChanged = 20;  // Die User-Privilegien haben gewechselt
  cMMClientStarted = 21;  // Appl. MMClient wurde gestartet
  cMMClientStopped = 22;  // Appl. MMClient wurde beendet
  cModalWindow     = 100; // Appl. schliesst ein modales Fenster
  cNewLanguage     = 101; // Sparche hat gewechselt

  // An MMClient
  cMMApplTerminate = 30;  // Appl. wurde beendet


// Ladet alle Messages ID's in den Speicher
procedure loadMessagesIDs;

// Sendet eine Meldung an MillMaster Programme
function MMBroadCastMessage(aMessageID: THandle; WParam: Integer; LParam: Integer = 0):Boolean;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------
// Registrierung der Messages ID's (Local RAM)
procedure loadMessagesIDs;
begin
  // Meldungen an Appl. (User change, Msg von MMClient)
  //gMessageApplID   := RegisterWindowMessage(cMsgAppl);
  //gMessageClientID := RegisterWindowMessage(cMsgClient);
end;
//------------------------------------------------------------------------------
// Sendet eine Meldung an alle Programme
function MMBroadCastMessage(aMessageID: THandle; WParam: Integer; LParam: Integer = 0):Boolean;
var xRecp: DWord;
begin
  xRecp := BSM_APPLICATIONS;
  Result := (BroadcastSystemMessage(BSF_POSTMESSAGE, @xRecp,
                                    aMessageID, WParam, LParam) > 0);
end;
//------------------------------------------------------------------------------


end.
