(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMSettingsGUI.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: Clearer Settings
| Process(es)...: -
| Description...: Graphical User Interface for YarnMaster Settings
| Info..........: -
| Develop.system: Compaq Deskpro Pentium Pro 200, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 29.11.99 | 1.00 | Kr | Initial release
| 07.01.00 | 1.00 | Kr | Window sizes defined:
|          |      |    | TGUIYMSettings: 595x1010, Inside tabs: 565x1002
| 28.11.00 | 1.01 | Kr | NEW properties FromSpindle, ToSpindle, MachineConfig
|          |      |    | defined (interface definition only)
| 18.09.01 | 1.02 |NueKr| Handling of YM_set_name added.
| 3.10.01  | 1.03 |Kr  | GetSettingsfromScreen with "Changed" result extended
| 13.12.01 | 1.04 |Nue | GetActSensingHeadClass added.
| 20.09.02         Nue | Umbau ADO
|=============================================================================*)
unit YMSettingsGUI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, EditBox, YMParaEditBox, ExtCtrls, QualityMatrixBase,
  QualityMatrix, ComCtrls, mmTabControl, mmPageControl, mmGroupBox,
  YMParaDBAccessForGUI, YMParaDef, YMParaUtils, mmCheckBox, mmPanel, QualityMatrixDef,
  MMUGlobal, YMBasicProdParaBox, mmQuery, mmListBox, mmButton, adoDBAccess;

const
  //...........................................................................

 //==== Index for production group specific settings ====
  cPGYarnCount = 0;
  cPGYarnUnit = 1;
  cPGPilotSpindle = 2;
  cPGSpeedRamp = 3;
  cPGSpeed = 4;

  //...........................................................................

 //==== Propery index for machine specific config code ====
  cMConfigA = 1;
  cMConfigB = 2;
  cMConfigC = 3;

 //==== FF-Cluster strings ====
resourcestring
  rsFFSettings = '(16)FF-Einstellung'; //ivlm
  rsFFCluster = '(16)FF-Cluster'; //ivlm

type
  //...........................................................................
  TOnChange = procedure(aOwner: TComponent) of object;

  TGUIYMSettings = class(TFrame)
    mPageControl: TmmPageControl;
    mQuality: TTabSheet;
    mSetup: TTabSheet;
    mStructureGroupBox: TmmGroupBox;
    mFFGroupBox: TmmGroupBox;
    mFFMatrix: TQualityMatrix;
    mClassMatrix: TQualityMatrix;
    mGBBasicProdPara: TGBBasicProdPara;
    mCurveSelection: TmmPanel;
    mcbChannelCurve: TmmCheckBox;
    mcbSpliceCurve: TmmCheckBox;
    mcbClusterCurve: TmmCheckBox;
    mYarnCountEditBox: TYarnCountEditBox;
    mChannelEditBox: TChannelEditBox;
    mSpliceEditBox: TSpliceEditBox;
    mSFIEditBox: TSFIEditBox;
    mFFClusterEditBox: TFFClusterEditBox;
    mFFClusterButton: TmmButton;
    mFaultClusterEditBox: TFaultClusterEditBox;
    mpnSensingHeadClass: TmmPanel;

    procedure mcbChannelCurveClick(Sender: TObject);
    procedure mcbClusterCurveClick(Sender: TObject);
    procedure mcbSpliceCurveClick(Sender: TObject);
    procedure mFFClusterButtonClick(Sender: TObject);
  private
    { Private declarations }

    fChannelSelection: Boolean;
    fOnChange: TOnChange;
    fReadOnly: Boolean;

    mYMSettings: TYMSettings;

    mSettings: TYMSettingsRec;
    mDoChangedEnabled: Boolean;

//Nue 13.12.01
    function GetActSensingHeadClass: TSensingHeadClass;
    function GetChannelSelection: Boolean;
    function GetBuildMaxMachineConfig: Boolean;
    function GetProdGrpYMPara(const aIndex: Integer): Integer;
    function GetYMSettinsGUIMode: TYMSettinsGUIMode;

    procedure BasicCreate;

    procedure DoValidate;
    procedure DoChanged(aTag: Integer);
    procedure DoConfirm(aTag: Integer);
    procedure DoParameterChange(aSource: TParameterSource);
    procedure PutChannelSelection(const aValue: Boolean);
//    procedure PutMachineConfig(const aIndex: Integer; const aValue: Word);
    procedure PutMachineConfig(const aValue: TMachineYMConfigRec);
    procedure PutProdGrpYMPara(const aIndex: Integer;
      const aValue: Integer);
    procedure PutSpindleRange(const aIndex: Integer; const aValue: Byte);
//    procedure SetActiveGroup(const Value: Integer);
    procedure SetBuildMaxMachineConfig(const aValue: Boolean);
    procedure SetCollectMaxMachineConfig(const aValue: TMachineYMConfigRec);
    procedure SetReadOnly(const aValue: Boolean);
    procedure SetYMSettinsGUIMode(const aValue: TYMSettinsGUIMode);
    procedure SynchFFBoxes;
    procedure SynchronizeMachineInopSettings;
    procedure UpdateBasicProdParaBoxDependency;
    procedure UpdateMachineInopSettings;
    procedure UpdateSensingHeadClass;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent); overload; override;
    constructor Create(aOwner: TComponent; aQuery: TNativeAdoQuery);reintroduce; overload;
    constructor CreateStandAlone(aOwner: TComponent);

    destructor Destroy; override;

    function EqualSensingHeadClassInRange(const aFirstSpindle,
      aLastSpindle: Byte): Boolean; overload;
    function EqualSensingHeadClassInRange(const aFirstSpindle,
      aLastSpindle: Byte; const aSensingHeadClass: TSensingHeadClass): Boolean; overload;

//    property MachineAssigned: Boolean read GetMachineAssigned write SetMachineAssigned;
    function NewOnDB: Integer; overload;
{Mg} function NewOnDB(aName: string): Integer; overload;
{Mg} function NewTemplate(aName: string): Integer;

    procedure Delete(aSetID: Integer);
    function GetFromScreen(var aSettings: TYMSettingsByteArr; var aSize: Word): Boolean;
    procedure LoadFromDB(aSetID: Integer; var aYMSetName: TText50);
    procedure PutDefaultToScreen;
    procedure PutToScreen(var aSettings: TYMSettingsByteArr);
    procedure SetMaxSensingHeadClass;
    // Insert Max SensingHeadClass to the already loaded YMSettings
    // Note: Sensinghead dependend ConfigBits will be set.
    // Use only for templates recomended
    procedure UpdateOnDB(aSetID: Integer);

//Nue 13.12.01
    property ActSensingHeadClass: TSensingHeadClass read GetActSensingHeadClass;

  published

    property BuildMaxMachineConfig: Boolean read GetBuildMaxMachineConfig write SetBuildMaxMachineConfig;
    property CurveSelection: Boolean read GetChannelSelection write PutChannelSelection;
    property CollectMaxMachineConfig: TMachineYMConfigRec write SetCollectMaxMachineConfig;

      // Notifies the user if settings has changed
(*
    property MachineConfigA: Word index cMConfigA write PutMachineConfig;
    property MachineConfigB: Word index cMConfigB write PutMachineConfig;
    property MachineConfigC: Word index cMConfigC write PutMachineConfig;
*)
    property FromSpindle: Byte index cSRFrom write PutSpindleRange;
    // First spindle of the group, [1..cMaxSpindeln]
    property MachineConfig: TMachineYMConfigRec write PutMachineConfig;
//    property ActiveGroup: Integer write SetActiveGroup;

    property Mode: TYMSettinsGUIMode read GetYMSettinsGUIMode write SetYMSettinsGUIMode;

    property OnChange: TOnChange read fOnChange write fOnChange;

    property ProdGrpPilot: Integer index cPGPilotSpindle write PutProdGrpYMPara;
      // ProdGrpPilot := number of pilot spindles, [1..MaxSpindleOfProfGrp, default: 4];
    property ProdGrpSpeed: Integer index cPGSpeed write PutProdGrpYMPara;
      // ProdGrpSpeed := Winding Speed (SS Winder only), [20m/Min..1600m/Min, default: 900m/Min];
    property ProdGrpSpeedRamp: Integer index cPGSpeedRamp write PutProdGrpYMPara;
      // ProdGrpSpeedRamp := Speed Ramp (SS Winder only), [1s..30s];

    property ProdGrpYarnCount: Integer index cPGYarnCount read GetProdGrpYMPara write PutProdGrpYMPara;
      // ProdGrpYarnCount := current Yarn Count * 10;

    property ProdGrpYarnUnit: Integer index cPGYarnUnit read GetProdGrpYMPara write PutProdGrpYMPara;
      // ProdGrpYarnUnit := enumator of TYarnUnits;

    property ReadOnly: Boolean read fReadOnly write SetReadOnly;

    property ToSpindle: Byte index cSRTo write PutSpindleRange;
    // Last spindle of the group, [1..cMaxSpindeln]

  end;
  //..............................................................................

// procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

//******************************************************************************
(*
procedure Register;
begin
  RegisterComponents('YMClearer', [TGUIYMSettings]);
end;
*)
//******************************************************************************
// TGUIYMSettings
//******************************************************************************
//------------------------------------------------------------------------------

procedure TGUIYMSettings.BasicCreate;
begin

  mDoChangedEnabled := False;
  fReadOnly := False;

  with mYMSettings do
  begin

//    OnValidate := DoValidate;

    ChannelEditBox := mChannelEditBox;
    mChannelEditBox.OnConfirm := DoConfirm;
    mChannelEditBox.Printable := False;

    ClassMatrixEditBox := mClassMatrix;
//    mClassMatrix.OnConfirm := DoChanged;
    mClassMatrix.OnConfirm := DoConfirm;
    mClassMatrix.OnParameterChange := DoParameterChange;

    FFMatrixEditBox := mFFMatrix;
    mFFMatrix.MatrixMode := mmSelectCutFields;
    mFFMatrix.OnConfirm := DoChanged;

    FFClusterEditBox := mFFClusterEditBox;
    mFFClusterEditBox.OnConfirm := DoChanged;
    mFFClusterEditBox.Printable := False;
    mFFClusterEditBox.SynchronizeFFMatrix := SynchFFBoxes;
    SynchFFBoxes;

    FaultClusterEditBox := mFaultClusterEditBox;
    mFaultClusterEditBox.OnConfirm := DoConfirm;
//    mFaultClusterEditBox.OnConfirm := DoConfirmFaultCluster;
    mFaultClusterEditBox.Printable := False;

    SpliceEditBox := mSpliceEditBox;
    mSpliceEditBox.OnConfirm := DoConfirm;
    mSpliceEditBox.Printable := False;
//    mSpliceEditBox.UpperYarn := False;

    YarnCountEditBox := mYarnCountEditBox;
//    mYarnCountEditBox.OnConfirm := DoConfirmYarnCount;
    mYarnCountEditBox.OnConfirm := DoConfirm;
    mYarnCountEditBox.Printable := False;

 //    YarnCountEditBox.Plus := False;

    SFIEditBox := mSFIEditBox;
    mSFIEditBox.OnConfirm := DoChanged;
    mSFIEditBox.Printable := False;

    GBBasicProdPara := mGBBasicProdPara;
//    mGBBasicProdPara.OnConfirm := DoConfirmGBBasicProdPara;
    mGBBasicProdPara.OnLoadYMSettings := UpdateBasicProdParaBoxDependency;
    mGBBasicProdPara.OnConfirm := DoConfirm;

    PutDefaultToScreen;
  end;

  fOnChange := nil;
  CurveSelection := False;

  mcbChannelCurve.Checked := True;
  mClassMatrix.ChannelColor := mcbChannelCurve.Font.Color;
  mClassMatrix.ChannelVisible := True;

  mcbClusterCurve.Checked := False;
  mClassMatrix.ClusterColor := mcbClusterCurve.Font.Color;
  mClassMatrix.ClusterVisible := False;

  mcbSpliceCurve.Checked := False;
  mClassMatrix.SpliceColor := mcbSpliceCurve.Font.Color;
  mClassMatrix.SpliceVisible := False;

  mpnSensingHeadClass.Caption := rsSensingHeadClass + ':';

  ProdGRpYarnUnit := Integer(yUNm);

  ReadOnly := fReadOnly;
//ReadOnly := True;

  mYMSettings.GetFromScreen(mSettings);

  mDoChangedEnabled := True;
end;
//------------------------------------------------------------------------------

constructor TGUIYMSettings.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  mYMSettings := TYMSettings.Create;

  BasicCreate;

end;
//------------------------------------------------------------------------------

constructor TGUIYMSettings.Create(aOwner: TComponent; aQuery: TNativeAdoQuery);
begin
  inherited Create(aOwner);

  mYMSettings := TYMSettings.Create(aQuery);

  BasicCreate;

end;
//------------------------------------------------------------------------------

constructor TGUIYMSettings.CreateStandAlone(aOwner: TComponent);
begin
  inherited Create(aOwner);
  mYMSettings := TYMSettings.Create;
  BasicCreate;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.Delete(aSetID: Integer);
begin
  mYMSettings.Delete(aSetID)
end;
//------------------------------------------------------------------------------

destructor TGUIYMSettings.Destroy;
begin

  mYMSettings.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.DoChanged(aTag: Integer);
begin
  if Assigned(fOnChange) then
    fOnChange(self.Owner);
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.DoConfirm(aTag: Integer);
begin
  SynchronizeMachineInopSettings;

  if mYMSettings.Changed and mDoChangedEnabled then
  begin

    mYMSettings.InsertScreenToSettings(mSettings);
    mYMSettings.PutToScreenPrivat(mSettings);

    DoChanged(aTag);

  end;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.DoParameterChange(aSource: TParameterSource);
var
  xCluster: TFaultClusterSettingsRec;
begin

  case aSource of

    psChannel:
      begin

        mSettings.channel := mClassMatrix.GetChannelPara;
        mChannelEditBox.PutPara(mSettings.channel);
        mClassMatrix.PutYMSettings(mSettings);
        DoChanged(0);
      end;

    psCluster:
      begin

        mFaultClusterEditBox.GetPara(xCluster);
        xCluster.clusterDia := mClassMatrix.GetClusterDiameter;
        mFaultClusterEditBox.PutPara(xCluster);
        mYMSettings.InsertScreenToSettings(mSettings);
        mClassMatrix.PutYMSettings(mSettings);
        DoChanged(0);
      end;

    psSplice:
      begin

        mSettings.splice := mClassMatrix.GetSplicePara;
        mSpliceEditBox.PutPara(mSettings.splice);
        mClassMatrix.PutYMSettings(mSettings);
        DoChanged(0);

      end;

  end;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.DoValidate;
begin

  mYMSettings.InsertScreenToSettings(mSettings);
  mYMSettings.PutToScreenPrivat(mSettings);
end;
//------------------------------------------------------------------------------

function TGUIYMSettings.EqualSensingHeadClassInRange(const aFirstSpindle,
  aLastSpindle: Byte): Boolean;
begin

  Result := mGBBasicProdPara.EqualSensingHeadClassInRange(aFirstSpindle,
    aLastSpindle);
end;
//------------------------------------------------------------------------------

function TGUIYMSettings.EqualSensingHeadClassInRange(const aFirstSpindle,
  aLastSpindle: Byte; const aSensingHeadClass: TSensingHeadClass): Boolean;
begin

  Result := mGBBasicProdPara.EqualSensingHeadClassInRange(aFirstSpindle,
    aLastSpindle, aSensingHeadClass);
end;
//------------------------------------------------------------------------------

//Nue 13.12.01

function TGUIYMSettings.GetActSensingHeadClass: TSensingHeadClass;
begin
  Result := TYMSettingsUtils.GetSensingHeadClass(mSettings);
end;
//------------------------------------------------------------------------------

function TGUIYMSettings.GetBuildMaxMachineConfig: Boolean;
begin

  Result := mGBBasicProdPara.BuildMaxMachineConfig;
end;
//------------------------------------------------------------------------------

function TGUIYMSettings.GetChannelSelection: Boolean;
begin
  Result := fChannelSelection;
end;
//------------------------------------------------------------------------------

function TGUIYMSettings.GetFromScreen(var aSettings: TYMSettingsByteArr;
  var aSize: Word): Boolean;
begin

  mYMSettings.GetFromScreen(aSettings, aSize);
  Result := mYMSettings.GetChanged(PYMSettings(@aSettings)^);
end;
//------------------------------------------------------------------------------

function TGUIYMSettings.GetProdGrpYMPara(const aIndex: Integer): Integer;
begin
  Result := 0;
  case aIndex of
    cPGYarnCount:
      begin
        Result := mYarnCountEditBox.YarnCount;
      end;

    cPGYarnUnit:
      begin
        Result := Integer(mYarnCountEditBox.YarnUnit);
      end;

    cPGPilotSpindle:
      begin
//      Result := mGBBasicProdPara.ProdGrpPilot;
      end;

    cPGSpeedRamp:
      begin
//      Result := mGBBasicProdPara.ProdGrpSpeedRamp;
      end;

    cPGSpeed:
      begin
//      Result := mGBBasicProdPara.ProdGrpSpeed;
      end;

  end;

end;
//------------------------------------------------------------------------------

function TGUIYMSettings.GetYMSettinsGUIMode: TYMSettinsGUIMode;
begin

  Result := mGBBasicProdPara.Mode;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.LoadFromDB(aSetID: Integer; var aYMSetName: TText50);
begin

  mYMSettings.LoadFromDB(aSetID, aYMSetName);
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.mcbChannelCurveClick(Sender: TObject);
begin

  mClassMatrix.ChannelVisible := mcbChannelCurve.Checked;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.mcbClusterCurveClick(Sender: TObject);
begin

  mClassMatrix.ClusterVisible := mcbClusterCurve.Checked;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.mcbSpliceCurveClick(Sender: TObject);
begin
  mClassMatrix.SpliceVisible := mcbSpliceCurve.Checked;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.mFFClusterButtonClick(Sender: TObject);
begin
  if mFFMatrix.MatrixMode = mmSelectFFClusterFields then
  begin
    mFFMatrix.MatrixMode := mmSelectCutFields;
    mFFClusterButton.Caption := rsFFSettings;
  end
  else
  begin
    mFFMatrix.MatrixMode := mmSelectFFClusterFields;
    mFFClusterButton.Caption := rsFFCluster;
  end;

end;
//------------------------------------------------------------------------------

function TGUIYMSettings.NewOnDB: Integer;
begin
  Result := mYMSettings.NewOnDB;
end;
//------------------------------------------------------------------------------

function TGUIYMSettings.NewOnDB(aName: string): integer;
begin
  Result := mYMSettings.NewOnDB(aName);
end;
//------------------------------------------------------------------------------

function TGUIYMSettings.NewTemplate(aName: string): integer;
begin
  Result := mYMSettings.NewTemplate(aName);
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.PutChannelSelection(const aValue: Boolean);
begin
  fChannelSelection := aValue;

  if not ReadOnly then
  begin

    if aValue then
      mClassMatrix.MatrixMode := mmSelectSettings
    else
      mClassMatrix.MatrixMode := mmSelectCutFields;

  end
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.PutDefaultToScreen;
begin
  mYMSettings.PutDefaultToScreen;
  mYMSettings.GetFromScreen(mSettings);
end;
//------------------------------------------------------------------------------

(*
procedure TGUIYMSettings.PutMachineConfig(const aIndex: Integer;
  const aValue: Word);
begin
  case aIndex of

    cMConfigA:
      mGBBasicProdPara.MachineConfigA := aValue;

    cMConfigB:
      mGBBasicProdPara.MachineConfigB := aValue;

    cMConfigC:
      mGBBasicProdPara.MachineConfigC := aValue
  end;

end;
*)
// %% MachConf

procedure TGUIYMSettings.PutMachineConfig(
  const aValue: TMachineYMConfigRec);
begin
  mDoChangedEnabled := False;

  mGBBasicProdPara.MachineConfig := aValue;

  UpdateMachineInopSettings;

  mDoChangedEnabled := True;

end;

//------------------------------------------------------------------------------

procedure TGUIYMSettings.PutProdGrpYMPara(const aIndex: Integer;
  const aValue: Integer);
begin

  case aIndex of
    cPGYarnCount:
      begin
        mYarnCountEditBox.YarnCount := aValue; ;
      end;

    cPGYarnUnit:
      begin
        mYarnCountEditBox.YarnUnit := TYarnUnits(aValue);
      end;

    cPGPilotSpindle:
      begin
        mGBBasicProdPara.ProdGrpPilot := aValue;
      end;

    cPGSpeedRamp:
      begin
        mGBBasicProdPara.ProdGrpSpeedRamp := aValue;
      end;

    cPGSpeed:
      begin
        mGBBasicProdPara.ProdGrpSpeed := aValue;
      end;

  end;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.PutSpindleRange(const aIndex: Integer;
  const aValue: Byte);
begin

  mDoChangedEnabled := False;

  case aIndex of

    cSRFrom:
      mGBBasicProdPara.FromSpindle := aValue;

    cSRTo:
      mGBBasicProdPara.ToSpindle := aValue;
  end;

  UpdateMachineInopSettings;

  mDoChangedEnabled := True;

end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.PutToScreen(var aSettings: TYMSettingsByteArr);
begin
  mDoChangedEnabled := False;

  mSettings := PYMSettings(@aSettings)^;

  mYMSettings.PutToScreenPrivat(PYMSettings(@aSettings)^);
  UpdateMachineInopSettings;
  mYMSettings.IntroduceOriginalSettings;

  mDoChangedEnabled := True;

end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.SetBuildMaxMachineConfig(const aValue: Boolean);
begin

  mGBBasicProdPara.BuildMaxMachineConfig := aValue;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.SetCollectMaxMachineConfig(
  const aValue: TMachineYMConfigRec);
begin

  mGBBasicProdPara.CollectMaxMachineConfig := aValue;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.SetMaxSensingHeadClass;
begin

  mGBBasicProdPara.SetMaxSensingHeadClass;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.SetReadOnly(const aValue: Boolean);
begin
  mDoChangedEnabled := False;

  fReadOnly := aValue;

  if aValue then
  begin
    mClassMatrix.MatrixMode := mmDisplayData;
    mFFMatrix.MatrixMode := mmDisplayData;

  end
  else
  begin
    if CurveSelection then
      mClassMatrix.MatrixMode := mmSelectSettings
    else
      mClassMatrix.MatrixMode := mmSelectCutFields;

    if mFFMatrix.MatrixMode = mmDisplayData then
      mFFMatrix.MatrixMode := mmSelectSettings;
  end;

  mChannelEditBox.ReadOnly := fReadOnly;
  mSpliceEditBox.ReadOnly := fReadOnly;
  mYarnCountEditBox.ReadOnly := fReadOnly;
  mFaultClusterEditBox.ReadOnly := fReadOnly;
  mSFIEditBox.ReadOnly := fReadOnly;
  mFFClusterEditBox.ReadOnly := fReadOnly;
  SynchFFBoxes;
  mGBBasicProdPara.ReadOnly := fReadOnly;

  mDoChangedEnabled := True;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.SetYMSettinsGUIMode(
  const aValue: TYMSettinsGUIMode);
begin
  mDoChangedEnabled := False;

  mGBBasicProdPara.Mode := aValue;
  UpdateMachineInopSettings;

  mDoChangedEnabled := True;

end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.SynchFFBoxes;
begin
  if mFFClusterEditBox.Enabled then
  begin

    if (mFFMatrix.MatrixMode = mmSelectCutFields) or
      (mFFMatrix.MatrixMode = mmSelectSettings) then
      mFFClusterButton.Caption := rsFFSettings
    else if (mFFMatrix.MatrixMode = mmSelectFFClusterFields) then
      mFFClusterButton.Caption := rsFFCluster;

    mFFClusterButton.Enabled := not mFFClusterEditBox.ReadOnly;
  end
  else
  begin
    if ReadOnly then
      mFFMatrix.MatrixMode := mmDisplayData
    else
      mFFMatrix.MatrixMode := mmSelectCutFields;
    mFFClusterButton.Caption := rsFFSettings;
    mFFClusterButton.Enabled := False;
  end;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.SynchronizeMachineInopSettings;
var
  xRangeSpecificConfig: TGroupSpecificMachConfigRec;
begin
  xRangeSpecificConfig := mGBBasicProdPara.RangeSpecificConfig;

//  if (xRangeSpecificConfig.spindle.contents <> cSpdlRngNotDefined) or not MachineAssigned then

  if Mode <> gmTransparent then
  begin

    xRangeSpecificConfig := mGBBasicProdPara.RangeSpecificConfig;
    // Inopsettings from MachineConfig
    with mSettings.available do
    begin
      inoperativePara[0] := xRangeSpecificConfig.inoperativePara[0];

      inoperativePara[1] := xRangeSpecificConfig.inoperativePara[1];
(*
        inoperativePara[1] :=
          (xRangeSpecificConfig.inoperativePara[1] and not cIP1MFFSensingHeads) or
          (inoperativePara[1] and cIP1MFFSensingHeads);
*)

    end;

    mSettings.additional.configA := xRangeSpecificConfig.configA;
    mSettings.additional.configB := xRangeSpecificConfig.configB;
    mSettings.additional.configC := xRangeSpecificConfig.configC;
  end
  else if Mode = gmTransparent then
  begin
    // do nothing Inopsettings transparent
  end;

  mYMSettings.InopSettings0 := mSettings.available.inoperativePara[0];
  mYMSettings.InopSettings1 := mSettings.available.inoperativePara[1];
  UpdateSensingHeadClass;
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.UpdateBasicProdParaBoxDependency;
begin
  SynchronizeMachineInopSettings;

  mYMSettings.InsertScreenToSettings(mSettings);

(*  mGBBasicProdPara.GetPara(xBasicProdPara);
      // ConfigCode
    with mSettings do
    begin
      additional.configA := xBasicProdPara.configA;
      additional.configB := xBasicProdPara.configB;
      additional.configC := xBasicProdPara.configC;
//      available.inoperativePara[0] :=
    end;
*)
  mFFMatrix.PutYMSettings(mSettings);
  mFFClusterEditBox.PutYMSettings(mSettings);
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.UpdateMachineInopSettings;
begin
  SynchronizeMachineInopSettings;

  mYMSettings.InsertScreenToSettings(mSettings);

  mYMSettings.PutToScreenPrivat(mSettings);

end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.UpdateOnDB(aSetID: Integer);
begin
  mYMSettings.UpdateOnDB(aSetID);
end;
//------------------------------------------------------------------------------

procedure TGUIYMSettings.UpdateSensingHeadClass;
begin

  mpnSensingHeadClass.Caption := rsSensingHeadClass + ': ' +
    cSensingHeadClassNames[TYMSettingsUtils.GetSensingHeadClass(
    mSettings)];
end;
//------------------------------------------------------------------------------



end.

