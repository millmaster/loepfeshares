(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: EditBox.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: EditBox Component supports to add edit fields with label.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.11.1999  0.00  Kr  | Initial Release
|=============================================================================*)
unit EditBox;

interface

uses StdCtrls, Classes, KeySpinEdit, SysUtils, YMParaDef, Windows, IvDictio,
    mmLabel, PresetValue, Controls, mmGroupBox;

const
  cMaxEditFields = 8;

type
  //...........................................................................

  //...........................................................................

  TPrintableValue = class(TmmLabel, IPresetValue)
  private
    mPresetValue: TPresetValue;

    function GetValueHint: string;
    function GetValueText: string;
    function GetTextType: TTextType;
    function GetValue: Double;

    procedure SetValueHint(aValue: string);
    procedure SetValueText(aValue: string);
    procedure SetTextType(const aValue: TTextType);
    procedure SetValue(const aValue: Double);

  protected

    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

    procedure LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec );

//    property MaxValue: Double read fMaxValue write fMaxValue;
//    property MinValue: Double read fMinValue write fMinValue;
//    property Text: string read GetText write SetText;
    property TextType: TTextType read GetTextType write SetTextType;
    property Value: Double read GetValue write SetValue;
  end;

  //...........................................................................

  TGroupEditBox = class(TmmGroupBox)
  private
    { Private declarations }
    fCaptionWidth:  Integer;  // Width of caption field
    fReadOnly: Boolean;
    fMeasureWidth:  Integer;  // Width of measure field
    fOnConfirm:     TOnConfirm;
    fValueWidth:    Integer;  // Width of edit field
    fVerticalSpaceTbl: array[0..8] of Integer;  // Vertical Space between the fields

    mCaptionTbl: array[0..cMaxEditFields-1] of TmmLabel;
    mValueTbl: array[0..cMaxEditFields-1] of TComponent;
    mMeasureTbl: array[0..cMaxEditFields-1] of TmmLabel;

    function GetCaption(aID: Integer): string;
    function GetEnabledField(aID: Integer): Boolean;
    function GetTextType(aID: Integer): TTextType;
    function GetValue(aID: Integer): Double;

    procedure SetEnabledFiled(aID: Integer; const aValue: Boolean);
    procedure SetReadOnly(const aValue: Boolean);
    procedure SetTextType(aID: Integer; const aValue: TTextType);
    procedure SetValue(aID: Integer; const aValue: Double);
    procedure SetCaption(aID: Integer; const aValue: string);

  protected
    procedure SetEnabled(aValue: Boolean); override;
    procedure DoConfirm(aTag: Integer); virtual;

  public
    { Public declarations }
    constructor Create (aOwner: TComponent); override;
    destructor Destroy; override;
    function GetVerticalSpace( aID: Integer): Integer;

    procedure AddField(aTag: Integer; aValueSpec: TValueSpecRec;
      aPresetTextTbl: array of TPresetTextRec; aPrintable: Boolean);

    procedure BuildBox; virtual;

    procedure PutYMSettings(var aSettings: TYMSettingsRec); virtual; abstract;

    procedure SetVerticalSpace( aID, aVerticalSpace: Integer);

    property OnConfirm: TOnConfirm read fOnConfirm write fOnConfirm;
    property Captions[aID: Integer]: string read GetCaption write SetCaption;
    property EnabledField[aID: Integer]: Boolean read GetEnabledField write SetEnabledFiled;
    property TextType[aID: Integer]: TTextType read GetTextType write SetTextType;
    property Values[aID: Integer]: Double read GetValue write SetValue;
    property VerticalSpace[aID: Integer]: Integer read GetVerticalSpace write SetVerticalSpace;

  published
    property CaptionWidth: Integer read fCaptionWidth write fCaptionWidth;
    property Enabled write SetEnabled;
    property MeasureWidth: Integer read fMeasureWidth write fMeasureWidth;
    property ReadOnly: Boolean read fReadOnly write SetReadOnly;
    property ValueWidth: Integer read fValueWidth write fValueWidth;
  end;
  //...........................................................................

  EGroupEditBox = class(Exception);

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  LoepfeGlobal;

//******************************************************************************
// Helper functions
//******************************************************************************
//------------------------------------------------------------------------------

{
procedure Register;
begin
  RegisterComponents('YMClearer', [TGroupEditBox]);
end;
}

//******************************************************************************
// TGroupEditBox
//******************************************************************************
//------------------------------------------------------------------------------
const
  cGEBBoarder = 7;
  cGEBTopBoarder = 12;

procedure TGroupEditBox.AddField(aTag: Integer;
  aValueSpec: TValueSpecRec;
  aPresetTextTbl: array of TPresetTextRec;
  aPrintable: Boolean);
var
  xTotSpace, xTotHeight, i: Integer;
begin
  xTotSpace := 0;
  xTotHeight := 0;

  if aTag <= High(mValueTbl) then begin

    for i:=0 to aTag do
      xTotSpace := xTotSpace + fVerticalSpaceTbl[i];

    for i:=0 to aTag-1 do
      if Assigned(mValueTbl[i]) then
        xTotHeight := xTotHeight + (mValueTbl[i] as TControl).Height;

    // create Edit field
    if Assigned(mValueTbl[aTag]) then FreeAndNil(mValueTbl[aTag]);

    if aPrintable then
      mValueTbl[aTag] := TPrintableValue.Create(Self)
    else begin
      mValueTbl[aTag] := TKeySpinEdit.Create(Self);
      (mValueTbl[aTag] as TKeySpinEdit).ShowMode := smExtended;
    end;

    with (mValueTbl[aTag] as TControl) do begin
      Parent := Self;
      Width := ValueWidth;
      Top := xTotHeight + xTotSpace + cGEBTopBoarder;
      Left := cGEBBoarder + CaptionWidth;
      Tag := aTag;
    end;

    if aPrintable then begin
      (mValueTbl[aTag] as TPrintableValue).Alignment := taRightJustify;
      (mValueTbl[aTag] as TPrintableValue).LoadValueSpec(aValueSpec, aPresetTextTbl);
    end
    else begin
      (mValueTbl[aTag] as TKeySpinEdit).Alignment := taRightJustify;
      (mValueTbl[aTag] as TKeySpinEdit).LoadValueSpec(aValueSpec, aPresetTextTbl);
      (mValueTbl[aTag] as TKeySpinEdit).OnConfirm := DoConfirm;
      (mValueTbl[aTag] as TKeySpinEdit).ReadOnly := False;
    end;
    // Size of GroupBox
    Height := xTotHeight + (mValueTbl[aTag] as TControl).Height + xTotSpace + cGEBTopBoarder + cGEBBoarder;
    Width := cGEBBoarder + CaptionWidth + ValueWidth + 2 + MeasureWidth + cGEBBoarder;

    // create Label
    if Assigned(mCaptionTbl[aTag]) then FreeAndNil(mCaptionTbl[aTag]);
    if aValueSpec.caption <> '' then begin
//      if Assigned(mCaptionTbl[aTag]) then FreeAndNil(mCaptionTbl[aTag]);
      mCaptionTbl[aTag] := TmmLabel.Create(Self);
      with mCaptionTbl[aTag] do begin
        Parent  := Self;
        Caption := aValueSpec.caption;
        Width := CaptionWidth;
        Left := cGEBBoarder;
        Top := xTotHeight + xTotSpace + cGEBTopBoarder + ((mValueTbl[aTag] as TControl).Height - Height) div 2;
      end;
    end;

    // create Unit
      if Assigned(mMeasureTbl[aTag]) then FreeAndNil(mMeasureTbl[aTag]);
    if aValueSpec.measure <> '' then begin
//      if Assigned(mMeasureTbl[aTag]) then FreeAndNil(mMeasureTbl[aTag]);
      mMeasureTbl[aTag] := TmmLabel.Create(Self);
      with mMeasureTbl[aTag] do begin
        Parent  := Self;
        Caption := aValueSpec.Measure;
        Width := MeasureWidth;
        Left := cGEBBoarder+CaptionWidth+ValueWidth+2;
        Top := mCaptionTbl[aTag].Top;
      end;
    end;

    EnabledField[aTag] := Enabled;
  end
  else raise EGroupEditBox.Create('aTag higher then size of edit fields table');

end;
//------------------------------------------------------------------------------

procedure TGroupEditBox.BuildBox;
var
  i: Integer;
begin

  for i := 0 to High(mCaptionTbl) do
    FreeAndNil(mCaptionTbl[i]);
  for i := 0 to High(mValueTbl) do
    FreeAndNil(mValueTbl[i]);
  for i := 0 to High(mMeasureTbl) do
    FreeAndNil(mMeasureTbl[i]);

  for i := 0 to High(fVerticalSpaceTbl) do
    VerticalSpace[i] := cGEBBoarder;
end;
//------------------------------------------------------------------------------

constructor TGroupEditBox.Create(aOwner: TComponent);
var
  i: Integer;
begin
  inherited Create(aOwner);
  CaptionSpace := True;
  for i := 0 to High(mCaptionTbl) do
    mCaptionTbl[i] := nil;
  for i := 0 to High(mValueTbl) do
    mValueTbl[i] := nil;
  for i := 0 to High(mMeasureTbl) do
    mMeasureTbl[i] := nil;

  ValueWidth := 30;
  CaptionWidth := 20;
  MeasureWidth := 20;
//  BuildBox;
  fOnConfirm := nil;
end;
//------------------------------------------------------------------------------

destructor TGroupEditBox.Destroy;
var
  i: Integer;
begin

  for i := 0 to High(mCaptionTbl) do
    FreeAndNil(mCaptionTbl[i]);
  for i := 0 to High(mValueTbl) do
    FreeAndNil(mValueTbl[i]);
  for i := 0 to High(mMeasureTbl) do
    FreeAndNil(mMeasureTbl[i]);

  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TGroupEditBox.DoConfirm(aTag: Integer);
begin
  if Assigned(fOnConfirm) then
    fOnConfirm(aTag);
end;
//------------------------------------------------------------------------------

function TGroupEditBox.GetCaption(aID: Integer): string;
begin
  if aID <= High(mCaptionTbl) then begin
      Result := mCaptionTbl[aID].Caption;
  end
  else raise EGroupEditBox.Create('Edit field not available');

end;
//------------------------------------------------------------------------------

function TGroupEditBox.GetEnabledField(aID: Integer): Boolean;
begin
  if (aID <= High(mValueTbl)) then
    if (mValueTbl[aID] is TKeySpinEdit) then
      Result := (mValueTbl[aID] as TKeySpinEdit).Enabled
    else if (mValueTbl[aID] is TPrintableValue) then
      Result := (mValueTbl[aID] as TPrintableValue).Enabled
    else
      Result := False
  else
    Result := False;
end;
//------------------------------------------------------------------------------

function TGroupEditBox.GetTextType(aID: Integer): TTextType;
begin
  if aID <= High(mValueTbl) then begin
    if mValueTbl[aID] is TKeySpinEdit then
      Result := (mValueTbl[aID] as TKeySpinEdit).TextType
    else if (mValueTbl[aID] is TPrintableValue) then
      Result := (mValueTbl[aID] as TPrintableValue).TextType
    else
      raise EGroupEditBox.Create('Edit field not available');
  end
  else raise EGroupEditBox.Create('Edit field not available');
end;
//------------------------------------------------------------------------------

function TGroupEditBox.GetValue(aID: Integer): Double;
begin
  if aID <= High(mValueTbl) then begin
    if mValueTbl[aID] is TKeySpinEdit then
      Result := (mValueTbl[aID] as TKeySpinEdit).Value
    else if (mValueTbl[aID] is TPrintableValue) then
      Result := (mValueTbl[aID] as TPrintableValue).Value
    else
      raise EGroupEditBox.Create('Edit field not available');
  end
  else raise EGroupEditBox.Create('Edit field not available');
end;
//------------------------------------------------------------------------------

function TGroupEditBox.GetVerticalSpace(aID: Integer): Integer;
begin
  if aID <= High(fVerticalSpaceTbl) then Result := fVerticalSpaceTbl[aID]
  else raise EGroupEditBox.Create('aID higher then size of edit fields table');
end;
//------------------------------------------------------------------------------

procedure TGroupEditBox.SetCaption(aID: Integer; const aValue: string);
begin

  if aID <= High(mCaptionTbl) then begin
  mCaptionTbl[aID].Caption := aValue
  end
  else raise EGroupEditBox.Create('Edit field not available');

end;
//------------------------------------------------------------------------------

procedure TGroupEditBox.SetEnabled(aValue: Boolean);
var
  i:  Integer;
begin
  inherited SetEnabled(aValue);
  for i := 0 to High(mValueTbl) do
    EnabledField[i] := aValue;
end;
//------------------------------------------------------------------------------

procedure TGroupEditBox.SetEnabledFiled(aID: Integer;
  const aValue: Boolean);
begin
  if (aID <= High(mCaptionTbl)) and (aID <= High(mValueTbl)) and
     (aID <= High(mMeasureTbl)) then begin

    if Assigned(mCaptionTbl[aID]) then
      mCaptionTbl[aID].Enabled := aValue;

    if (mValueTbl[aID] is TKeySpinEdit) then
      (mValueTbl[aID] as TKeySpinEdit).Enabled := aValue
    else if (mValueTbl[aID] is TPrintableValue) then
      (mValueTbl[aID] as TPrintableValue).Enabled := aValue;

    if Assigned(mMeasureTbl[aID]) then
      mMeasureTbl[aID].Enabled := aValue;
  end
  else raise EGroupEditBox.Create('aID higher then size of edit fields table');
end;
//------------------------------------------------------------------------------

procedure TGroupEditBox.SetReadOnly(const aValue: Boolean);
var
  i: Integer;
begin
  fReadOnly := aValue;

  for i := 0 to High(mValueTbl) do
    if (mValueTbl[i] is TKeySpinEdit) then
      (mValueTbl[i] as TKeySpinEdit).ReadOnly := fReadOnly;
end;
//------------------------------------------------------------------------------

procedure TGroupEditBox.SetTextType(aID: Integer; const aValue: TTextType);
begin
  if aID <= High(mValueTbl) then begin
    if mValueTbl[aID] is TKeySpinEdit then
      (mValueTbl[aID] as TKeySpinEdit).TextType := aValue
    else if (mValueTbl[aID] is TPrintableValue) then
      (mValueTbl[aID] as TPrintableValue).TextType := aValue
    else
      raise EGroupEditBox.Create('Edit field not available');
  end
  else raise EGroupEditBox.Create('Edit field not available');
end;
//------------------------------------------------------------------------------

procedure TGroupEditBox.SetValue(aID: Integer; const aValue: Double);
begin
  if aID <= High(mValueTbl) then begin
    if mValueTbl[aID] is TKeySpinEdit then
      (mValueTbl[aID] as TKeySpinEdit).Value := aValue
    else if (mValueTbl[aID] is TPrintableValue) then
      (mValueTbl[aID] as TPrintableValue).Value := aValue
    else
      raise EGroupEditBox.Create('Edit field not available');
  end
  else raise EGroupEditBox.Create('Edit field not available');
end;
//------------------------------------------------------------------------------

procedure TGroupEditBox.SetVerticalSpace(aID, aVerticalSpace: Integer);
begin
  if aID <= High(fVerticalSpaceTbl) then
    fVerticalSpaceTbl[aID] := aVerticalSpace
  else raise EGroupEditBox.Create('aID higher then size of edit fields table');
end;

//------------------------------------------------------------------------------

//******************************************************************************
// TPrintableValue
//******************************************************************************
//------------------------------------------------------------------------------

constructor TPrintableValue.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  mPresetValue := TPresetValue.Create;
  mPresetValue.AttachInterface(self);
end;
//------------------------------------------------------------------------------

destructor TPrintableValue.Destroy;
begin
  FreeAndNil(mPresetValue);
  inherited;
end;

function TPrintableValue.GetTextType: TTextType;
begin

  Result := mPresetValue.TextType;
end;
//------------------------------------------------------------------------------

function TPrintableValue.GetValue: Double;
begin

  Result := mPresetValue.Value;
end;
//------------------------------------------------------------------------------

function TPrintableValue.GetValueHint: string;
begin
  Result := Hint;
end;
//------------------------------------------------------------------------------

function TPrintableValue.GetValueText: string;
begin
  Result := Text;
end;
//------------------------------------------------------------------------------

procedure TPrintableValue.LoadValueSpec(aValueSpec: TValueSpecRec;
  aPresetTbl: array of TPresetTextRec);
begin

  mPresetValue.LoadValueSpec(aValueSpec, aPresetTbl);
end;
//------------------------------------------------------------------------------

procedure TPrintableValue.SetTextType(const aValue: TTextType);
begin

  mPresetValue.TextType := aValue;
end;
//------------------------------------------------------------------------------

procedure TPrintableValue.SetValue(const aValue: Double);
begin

  mPresetValue.Value := aValue;
//  Caption := mPresetValue.Text;
end;
//------------------------------------------------------------------------------

procedure TPrintableValue.SetValueHint(aValue: string);
begin
  Hint := aValue;
end;
//------------------------------------------------------------------------------

procedure TPrintableValue.SetValueText(aValue: string);
begin
  Text := aValue;
end;
//------------------------------------------------------------------------------

end.
