(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: KeySpinEdit.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Spin Edit Component without Spin Buttons, Up- Down- Keys only.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 29.10.1999  0.00  Kr  | Initial Release
|=============================================================================*)
unit KeySpinEdit;

interface

uses Mask, StdCtrls, Classes, SysUtils, Windows, IvDictio, mmEdit, PresetValue; // ExtCtrls, Controls, Messages, SysUtils,
//  Forms, Graphics, Menus, Buttons;

type
  //...........................................................................

  TOnConfirm = procedure(aTag: Integer) of object;

(*TTextType = (ttText, ttCmdDisabled, ttFloat);

  TPresetTextRec = record
		typ:      TTextType;
   	text:     string;
  end;
  //...........................................................................

  TValueSpecRec = record
    minValue:   Double;
    maxValue:   Double;
    format:     string; // format of the folat value '%4.2f' use float format strings only!
    caption:    string; // label of value, if null string no label
    measure:    string; // unit of mesurement, if null string no unit
    hint:       string; // hint text, if null string no hint
  end;
*)
  //...........................................................................

  TKeySpinEdit = class(TmmEdit, IPresetValue)
  private
    fOnConfirm:   TOnConfirm;

    mOnEnterText: string;
    mOnEnterTextType: TTextType;
    mPresetValue: TPresetValue;

    function GetMaxValue: Double;
    function GetMinValue: Double;
    function GetTextType: TTextType;
    function GetValue: Double;
    function GetValueHint: string;
    function GetValueText: string;
    function IsValidChar(var aKey: Char): Boolean; virtual;


    procedure SetMaxValue(const aValue: Double);
    procedure SetMinValue(const aValue: Double);
    procedure SetTextType(const aValue: TTextType);
    procedure SetValueHint(aValue: string);
    procedure SetValueText(aValue: string);

  protected
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure KeyDown(var aKey: Word; aShift: TShiftState); override;
    procedure KeyPress(var aKey: Char); override;

  public
    constructor Create (aOwner: TComponent); override;
    destructor Destroy; override;
    procedure LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec);
    procedure SetValue (aValue: Double);

  published
    property AutoSelect;
    property AutoSize;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
//    property EditorEnabled: Boolean read FEditorEnabled write FEditorEnabled default True;
    property Enabled;
    property Font;
//    property Increment: Double read FIncrement write FIncrement;
    property MaxLength;
    property MaxValue: Double read GetMaxValue write SetMaxValue;
    property MinValue: Double read GetMinValue write SetMinValue;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property TextType: TTextType read GetTextType write SetTextType;
    property Value: Double read GetValue write SetValue;
    property Visible;
    property OnChange;
    property OnClick;
    property OnConfirm: TOnConfirm  read fOnConfirm write fOnConfirm;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
  end;

  //...........................................................................

//------------------------------------------------------------------------------
//  Functions and procedures
//------------------------------------------------------------------------------
  //...........................................................................
// procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//******************************************************************************
// Functions and procedures
//******************************************************************************
//------------------------------------------------------------------------------

(*
procedure Register;
begin
  RegisterComponents('YMClearer', [TKeySpinEdit]);
end;
*)
//------------------------------------------------------------------------------

//******************************************************************************
// TKeySpinEdit
//******************************************************************************
//------------------------------------------------------------------------------

constructor TKeySpinEdit.Create (aOwner: TComponent);
begin
  inherited Create (aOwner);

  mPresetValue := TPresetValue.Create;
  mPresetValue.AttachInterface(self);

  fOnConfirm := nil;
  mOnEnterText := Text;
  mOnEnterTextType := TextType;
end;
//------------------------------------------------------------------------------

destructor TKeySpinEdit.Destroy;
begin
  FreeAndNil(mPresetValue);
  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.DoEnter;
begin
  mOnEnterText := Text;
  mOnEnterTextType := TextType;
  inherited DoEnter;
end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.DoExit;
begin
  if TextType = ttFloat then
    SetValue(GetValue);
  if Assigned(fOnConfirm) then
    fOnConfirm(Tag);
  inherited DoExit;
end;
//------------------------------------------------------------------------------

function TKeySpinEdit.GetMaxValue: Double;
begin
  Result := mPresetValue.MaxValue;
end;
//------------------------------------------------------------------------------

function TKeySpinEdit.GetMinValue: Double;
begin
  Result := mPresetValue.MinValue;
end;
//------------------------------------------------------------------------------

function TKeySpinEdit.GetTextType: TTextType;
begin
  Result := mPresetValue.TextType;
end;
//------------------------------------------------------------------------------

function TKeySpinEdit.GetValue: Double;
begin
  Result := mPresetValue.Value;
end;
//------------------------------------------------------------------------------

function TKeySpinEdit.GetValueHint: string;
begin
  Result := Hint;
end;
//------------------------------------------------------------------------------

function TKeySpinEdit.GetValueText: string;
begin
  Result := Text;
end;
//------------------------------------------------------------------------------

function TKeySpinEdit.IsValidChar(var aKey: Char): Boolean;
begin
  Result := True;

  // Visible characters
  if aKey in [' '..'~'] then begin

    if not ReadOnly then begin
      if TextType = ttFloat then begin

        if aKey in [DecimalSeparator, '+', '-', '0'..'9'] then begin
          // Only one decimal seperator
          if (aKey = DecimalSeparator) and (Pos(DecimalSeparator, Text) > 0) then
            Result := False
        end
        else begin
          Result := mPresetValue.FindTargetText(aKey);

          if Result then begin
            aKey := #0;
            if Assigned(fOnConfirm) then
              fOnConfirm(Tag);
          end;
        end;
      end

      else begin

        if aKey in [DecimalSeparator, '+', '-', '0'..'9'] then begin
          TextType := ttFloat;
          Text := '';
        end
        else
          Result := False
      end;
    end
    else
      Result := False;
  end
  else begin

    if ReadOnly and
      ((aKey = Char(VK_BACK)) or (aKey = Char(VK_DELETE)) or
       (aKey = Chr(VK_RETURN)) or (aKey = Char(VK_ESCAPE))) then
      Result := False
    else if aKey = Chr(VK_RETURN) then begin
      if TextType = ttFloat then
        SetValue(GetValue);
      aKey := #0;

      if Assigned(fOnConfirm) then
        fOnConfirm(Tag);
    end
    else if aKey = Char(VK_ESCAPE) then begin
      Text := mOnEnterText;
      TextType := mOnEnterTextType;
      aKey := #0;
    end
    else if not ((aKey = Char(VK_BACK)) or (aKey = Char(VK_DELETE))) then
      Result := False;

  end;

end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.KeyDown (var aKey: Word; aShift: TShiftState);
var xText: string;
begin

  if ReadOnly then begin
    MessageBeep (0);
    inherited KeyDown (aKey, aShift);
  end
  else begin
    if aKey = VK_UP then begin
      xText := mPresetValue.NextText;
      if xText <> '' then begin
        Text := xText;
        if Assigned(fOnConfirm) then
          fOnConfirm(Tag);
      end
      else
        MessageBeep(0);
    end

    else if aKey = VK_DOWN then begin
      xText := mPresetValue.PreviousText;
      if xText <> '' then begin
        Text := xText;
        if Assigned(fOnConfirm) then
          fOnConfirm(Tag);
      end
      else
        MessageBeep(0);
    end
    else inherited KeyDown (aKey, aShift);
  end;

end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.KeyPress(var aKey: Char);
begin
  if IsValidChar(aKey) then
    inherited KeyPress(aKey)
  else begin
    MessageBeep(0);
    aKey := #0;
  end;
end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec );
begin

  mPresetValue.LoadValueSpec(aValueSpec, aPresetTbl);

  if Hint <> '' then
    ShowHint := True;
  mOnEnterText := Text;
end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.SetMaxValue(const aValue: Double);
begin
  mPresetValue.MaxValue := aValue;
end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.SetMinValue(const aValue: Double);
begin
  mPresetValue.MinValue := aValue;
end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.SetTextType(const aValue: TTextType);
begin
  mPresetValue.TextType := aValue;
end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.SetValue (aValue: Double);
begin
  mPresetValue.Value := aValue;
end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.SetValueHint(aValue: string);
begin
  Hint := aValue;
end;
//------------------------------------------------------------------------------

procedure TKeySpinEdit.SetValueText(aValue: string);
begin
  Text := aValue;
end;
//------------------------------------------------------------------------------

end.
