(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DisplayLayer.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr | Initial Release
|=========================================================================================*)
unit DisplayLayer;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, ExtCtrls, QualityMatrixDef, QualityMatrixBase;

const
  _Version_ = '$Revision: 2 $';
  _Date_ = '$Modtime: 3/25/04 2:57p $';
  // $NoKeywords: $

const
  cCuts = 0;
  cDefects = 1;
  cDotsFieldFactor = 2;

type
  //...........................................................................
  TDisplayLayer = class(TObject)
  private
    { Private declarations }
    fCuts: array of Real;
    fDefects: array of Real;
    fDotsColor: TColor;
    fFieldColor: array of TColor;
    fFieldDots: array of Integer;

    fCutsColor: TColor;
    fDefectsColor: TColor;
    fMode: TDisplayMode;
    fZeroLimit: Double;

    mPixelAid: TPixelAid;
    mQualityMatrixBase: TQualityMatrixBase;

    function GetFieldColor(aFieldID: Integer): TColor;
    function GetTotClasses: Integer;
    function GetTotValues: Integer;
    function GetValueColor(const aIndex: Integer): TColor;

    procedure AllocateValueTable;
    procedure DisplayNumber(aFieldRect: TRect; aFormat: TFormatTextRec; aValue: Real);
    procedure DisplayDots(aFieldID: Integer; aFieldRect: TRect);
    procedure SetFieldColor(aFieldID: Integer; const aValue: TColor);
    procedure SetMode(const aValue: TDisplayMode);
    procedure SetValueColor(const aIndex: Integer; const aValue: TColor);
    function GetFieldDots(aFieldID: Integer): Integer;
    procedure SetFieldDots(aFieldID: Integer; const aValue: Integer);

  protected
    { Protected declarations }

  public
    { Public declarations }
    constructor Create(aQualityMatrixBase: TQualityMatrixBase); reintroduce;
    destructor Destroy; override;

    function GetValue(aFieldID, aIndex: Integer): Real;

    procedure PaintField(aFieldID: Integer);
    procedure SetDefaultColor;
    procedure SetValue(aFieldID, aIndex: Integer; aValue: Real);
    procedure SetType(aType: TMatrixType);

    property Cuts[aFieldID: Integer]: Real index cCuts read GetValue write SetValue;
    property CutsColor: TColor index cCuts read GetValueColor write SetValueColor;
    property Defects[aFieldID: Integer]: Real index cDefects read GetValue write SetValue;
    property DefectsColor: TColor index cDefects read GetValueColor write SetValueColor;
    property DotsColor: TColor read fDotsColor write fDotsColor;
    property FieldColor[aFieldID: Integer]: TColor read GetFieldColor write SetFieldColor;
    property FieldDots[aFieldID: Integer]: Integer read GetFieldDots write SetFieldDots;

    property MatrixType: TMatrixType write SetType;
    property Mode: TDisplayMode read fMode write SetMode;
    property TotClasses: Integer read GetTotClasses;
    property TotValues: Integer read GetTotValues;
    property ZeroLimit: Double read fZeroLimit write fZeroLimit;

  published
    { Published declarations }

  end;

  // procedure Register;

implementation
//------------------------------------------------------------------------------
(*procedure Register;
begin
  RegisterComponents('YMClearer', [TDisplayLayer]);
end;
*)
//******************************************************************************
// TDisplayLayer
//******************************************************************************
//------------------------------------------------------------------------------

procedure TDisplayLayer.AllocateValueTable;
var
  i: Integer;
begin

  if (Length(fCuts) <> TotValues) or (Length(fDefects) <> TotValues) or
    (Length(fFieldColor) <> TotValues) or (Length(fFieldDots) <> TotValues) then
  begin
    SetLength(fCuts, TotValues);
    SetLength(fDefects, TotValues);
    SetLength(fFieldColor, TotValues);
    SetLength(fFieldDots, TotValues);

    for i := 0 to High(fCuts) do
    begin
      fCuts[i] := 0;
      fDefects[i] := 0;
      fFieldColor[i] := clNone;
      fFieldDots[i] := 0;
      //      fFieldColor[i] := clGreen + (i * 2); //clGreen;
      //      fFieldDots[i] := i + 13;

      //    fCuts[i] := i*Pi*Pi;
      //    fDefects[i] := i*Pi;
      //    fDefects[i] := i*0.0001;
    end;
  end;

end;

constructor TDisplayLayer.Create(aQualityMatrixBase: TQualityMatrixBase);
begin
  inherited Create;

  fDotsColor := clBlack;
  mQualityMatrixBase := aQualityMatrixBase;
  mPixelAid := TPixelAid.Create;
  SetDefaultColor;
  Mode := dmValues;
  ZeroLimit := 0.01;

  Randomize;
end;
//------------------------------------------------------------------------------

destructor TDisplayLayer.Destroy;
begin
  mPixelAid.Free;

  Finalize(fCuts);
  Finalize(fDefects);

  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.DisplayDots(aFieldID: Integer; aFieldRect: TRect);
var
  i, xRate, xFieldDots: Integer;
begin

  if FieldDots[aFieldID] > 0 then
  begin
    //    Randomize:
    xFieldDots := Round(mQualityMatrixBase.GetFieldWeight(aFieldID) *
      FieldDots[aFieldID]);

    with aFieldRect do
    begin
      xRate := cDotsFieldFactor * ((Bottom - Top + 1) * (Right - Left + 1)) div xFieldDots;
      if xRate > 1 then
      begin
        (*
                j := xRate;
                for i := 0 to FieldDots[aFieldID] - 1 do
                begin

                  mQualityMatrixBase.Canvas.Pixels[Left + (j mod (Right - Left + 1)),
                    Top + (j div (Right - Left + 1))] := xDotColor;
                  Inc(j, xRate);

                end;
        *)

        for i := 0 to xFieldDots - 1 do
        begin

          //          mQualityMatrixBase.Canvas.Pixels[Left + Random(Right - Left + 1),
          //            Top + Random(Bottom - Top + 1)] := DotsColor;

        end;

      end
      else
      begin
        mQualityMatrixBase.Canvas.Brush.Style := bsClear;
        mQualityMatrixBase.Canvas.Brush.Color := DotsColor;
        mQualityMatrixBase.Canvas.FillRect(aFieldRect);
      end

    end;

  end;

end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.DisplayNumber(aFieldRect: TRect; aFormat: TFormatTextRec; aValue: Real);
var
  xValueText, xFormatText: string;
  xTextRect: TRect;
  xBounds: TRangeRec;
begin
  if aValue >= ZeroLimit then
    with aFieldRect, aFormat do
    begin
      mQualityMatrixBase.Canvas.Brush.Style := bsClear;

      //    aValue := 23999999;
      //    aValue := 99.99;
      //    aValue := 6.789;
      //    aValue := 1.10;
      //      aValue := 0.9788;
      //    xValueText := preText+Format(formatText, [aValue])+postText;

      if aValue > 99.9 then
        xFormatText := '%.0n'
      else
      begin
        if aValue > 0.999 then
          xFormatText := '%.3g'
        else
          xFormatText := '%.2f';
      end;

      xValueText := preText + Format(xFormatText, [aValue]) + postText;

      if pFont <> nil then
      begin
        mQualityMatrixBase.Canvas.Font.Color := pFont^.Color;
        mQualityMatrixBase.Canvas.Font.Style := pFont^.Style;
        mQualityMatrixBase.Canvas.Font.Size := pFont^.Size;
        mQualityMatrixBase.Canvas.Font.Name := 'Arial Unicode MS'; //%% get from ini-file!
      end;

      xBounds.start := Left;
      xBounds.stop := Right;
      xBounds := mPixelAid.PlaceItem(
        xBounds, mQualityMatrixBase.Canvas.TextWidth(xValueText), horAlign);
      xTextRect.Left := xBounds.start;
      xTextRect.Right := xBounds.stop;

      xBounds.start := Top;
      xBounds.stop := Bottom;
      xBounds := mPixelAid.PlaceItem(
        xBounds, mQualityMatrixBase.Canvas.TextHeight(xValueText), vertAlign);
      xTextRect.Top := xBounds.start;
      xTextRect.Bottom := xBounds.stop;

      //%% mQualityMatrixBase.Canvas.TextRect(xTextRect, xTextRect.Left, xTextRect.Top, xValueText);
      Windows.TextOut(mQualityMatrixBase.Canvas.Handle, xTextRect.Left, xTextRect.Top,
        PChar(xValueText), Length(xValueText)); //%%
    end;

end;
//------------------------------------------------------------------------------

function TDisplayLayer.GetFieldColor(aFieldID: Integer): TColor;
begin
  if aFieldID < Length(fFieldColor) then
    Result := fFieldColor[aFieldID]
  else
    Result := clBlack;
end;
//------------------------------------------------------------------------------

function TDisplayLayer.GetFieldDots(aFieldID: Integer): Integer;
begin
  if aFieldID < Length(fFieldDots) then
    Result := fFieldDots[aFieldID]
  else
    Result := 0;
end;
//------------------------------------------------------------------------------

function TDisplayLayer.GetTotClasses: Integer;
begin

  with mQualityMatrixBase do
  begin
    if (Mode = dmValues) or (Mode = dmColor) or (Mode = dmDots) then
      Result := TotFields
    else
      Result := TotSuperClasses;
  end;
end;
//------------------------------------------------------------------------------

function TDisplayLayer.GetTotValues: Integer;
begin

  with mQualityMatrixBase do
  begin
    if Mode = dmSCValues then
      Result := TotSuperClasses
    else
      Result := TotFields;
  end;
end;
//------------------------------------------------------------------------------

function TDisplayLayer.GetValue(aFieldID, aIndex: Integer): Real;
begin
  Result := 0;

  case aIndex of
    cCuts:
      if aFieldID < Length(fCuts) then Result := fCuts[aFieldID];

    cDefects:
      if aFieldID < Length(fDefects) then Result := fDefects[aFieldID];
  end;
end;
//------------------------------------------------------------------------------

function TDisplayLayer.GetValueColor(const aIndex: Integer): TColor;
var
  xTextFormat: TFormatTextRec;
begin
  Result := clNone;

  case aIndex of
    cCuts:
      begin

        if fCutsColor = clNone then
        begin
          xTextFormat := mQualityMatrixBase.CutsFormat[0];
          Result := xTextFormat.pFont^.Color;
        end
        else
          Result := fCutsColor;
      end;

    cDefects:
      begin

        if fDefectsColor = clNone then
        begin
          xTextFormat := mQualityMatrixBase.DefectsFormat[0];
          Result := xTextFormat.pFont^.Color;
        end
        else
          Result := fDefectsColor;
      end;
  end;
end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.PaintField(aFieldID: Integer);
var
  i: Integer;
  xRect: TRect;
  xTextFormat: TFormatTextRec;
  xFont: TFontRec;
  xTbl: TSCMemberArray;
  xCuts, xDefects: Real;
  xBrush: TBrushRec;
  xHide: Boolean;
begin

  if aFieldID < TotClasses then
  begin
    with mQualityMatrixBase do
    begin

      xCuts := 0;
      xDefects := 0;
      xHide := False;
      case self.Mode of
        dmValues,
          dmColor,
          dmDots:
          begin
            if not ((((MatrixType = mtSiro) and ((MatrixSubType = mstSiroF) or (MatrixSubType = mstSiroH))) and
              ((aFieldID = 0) or (aFieldID = 1) or (aFieldID = 8) or (aFieldID = 9))) or
              ((MatrixType = mtColor) and ((aFieldID = 0) or (aFieldID = 1) or (aFieldID = 8) or (aFieldID = 9) or
              (aFieldID = 70) or (aFieldID = 71) or (aFieldID = 78) or (aFieldID = 79)))) then
            begin

              CoordinateMode := cmInside;
              xRect := Field[aFieldID];
              xCuts := Cuts[aFieldID];
              xDefects := Defects[aFieldID];
            end
            else
              xHide := False;
          end;

        dmSCValues:
          begin
            if not ((((MatrixType = mtSiro) and ((MatrixSubType = mstSiroF) or (MatrixSubType = mstSiroH))) and
              (aFieldID = 0)) or
              ((MatrixType = mtColor) and ((aFieldID = 0) or (aFieldID = 19)))) then
            begin
              xRect := SCField[aFieldID];

              xCuts := Cuts[aFieldID];
              xDefects := Defects[aFieldID];
            end
            else
              xHide := False;
          end;

        dmCalculateSCValues:
          begin
            if not ((((MatrixType = mtSiro) and ((MatrixSubType = mstSiroF) or (MatrixSubType = mstSiroH))) and
              (aFieldID = 0)) or
              ((MatrixType = mtColor) and ((aFieldID = 0) or (aFieldID = 19)))) then
            begin

              xRect := SCField[aFieldID];
              xTbl := SCMembers[aFieldID];

              i := 0;
              repeat
                if xTbl[i] <> cSCDelimiter then
                begin
                  xCuts := xCuts + Cuts[xTbl[i]];
                  xDefects := xDefects + Defects[xTbl[i]];
                  Inc(i);
                end
                else
                  i := TotValues;
              until i >= TotValues;
            end
            else
              xHide := False;

          end;
      end;

      if not xHide then
      begin
        if (Mode = dmColor) or (Mode = dmDots) then
        begin

          if (Mode = dmColor) then
          begin
            xBrush.color := FieldColor[aFieldId];
            xBrush.style := bsSolid;

            FillField(xRect, xBrush);
          end
          else
            DisplayDots(aFieldId, xRect);

          xTextFormat := CutsFormat[aFieldID];
          xFont := xTextFormat.pFont^;
          xTextFormat.pFont := @xFont;
          if fCutsColor <> clNone then
            xTextFormat.pFont^.color := fCutsColor;
          DisplayNumber(xRect, xTextFormat, xCuts);
        end
        else
        begin

          xTextFormat := CutsFormat[aFieldID];
          xFont := xTextFormat.pFont^;
          xTextFormat.pFont := @xFont;

          if (LastCutMode = lcValue) and (LastCutField = aFieldId) then
            xTextFormat.pFont^.color := LastCutColor
          else
          begin
            if fCutsColor <> clNone then
              xTextFormat.pFont^.color := fCutsColor;
          end;
          DisplayNumber(xRect, xTextFormat, xCuts);

          xTextFormat := DefectsFormat[aFieldID];
          if fDefectsColor <> clNone then
            xTextFormat.pFont^.color := fDefectsColor;
          DisplayNumber(xRect, xTextFormat, xDefects);
        end;
      end;
    end;
  end;

end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.SetDefaultColor;
begin

  fCutsColor := clNone;
  fDefectsColor := clNone;
end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.SetFieldColor(aFieldID: Integer; const aValue: TColor);
begin

  if aFieldID < Length(fFieldColor) then
    fFieldColor[aFieldID] := aValue;
end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.SetFieldDots(aFieldID: Integer;
  const aValue: Integer);
begin
  if aFieldID < Length(fFieldDots) then
    fFieldDots[aFieldID] := aValue;
end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.SetMode(const aValue: TDisplayMode);
begin

  if aValue <> fMode then
  begin

    fMode := aValue;

    AllocateValueTable;
  end;
end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.SetType(aType: TMatrixType);
begin

  AllocateValueTable;
end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.SetValue(aFieldID, aIndex: Integer; aValue: Real);
begin
  case aIndex of
    cCuts:
      if aFieldID < Length(fCuts) then fCuts[aFieldID] := aValue;

    cDefects:
      if aFieldID < Length(fDefects) then fDefects[aFieldID] := aValue;
  end;
end;
//------------------------------------------------------------------------------

procedure TDisplayLayer.SetValueColor(const aIndex: Integer;
  const aValue: TColor);
begin
  case aIndex of
    cCuts:
      fCutsColor := aValue;

    cDefects:
      fDefectsColor := aValue;
  end;

end;
//------------------------------------------------------------------------------

end.

