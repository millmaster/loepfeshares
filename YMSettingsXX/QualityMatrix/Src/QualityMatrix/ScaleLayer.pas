(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: ScaleLayer.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr | Initial Release
|=========================================================================================*)
unit ScaleLayer;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, QualityMatrixDef, QualityMatrixBase;

const
  _Version_         = '$Revision: 3 $';
  _Date_            = '$Modtime: 2/26/04 1:42p $';
  // $NoKeywords: $

type
  //...........................................................................
  TScaleLayer = class(TObject)
  private
    { Private declarations }

    mPixelAid: TPixelAid;
    mQualityMatrixBase: TQualityMatrixBase;

    procedure DisplayLine(aLine: TRect; aPen: TPenRec);
    procedure DisplayText(aText: TTextRec);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(aQualityMatrixBase: TQualityMatrixBase); reintroduce;
    destructor Destroy; override;
    procedure CustomPaint;

  published
    { Published declarations }
  end;

implementation

//******************************************************************************
// TScaleLayer
//******************************************************************************
//------------------------------------------------------------------------------
constructor TScaleLayer.Create(aQualityMatrixBase: TQualityMatrixBase);
begin
  inherited Create;

  mQualityMatrixBase := aQualityMatrixBase;

  mPixelAid := TPixelAid.Create;
end;
//------------------------------------------------------------------------------
destructor TScaleLayer.Destroy;
begin
  mPixelAid.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TScaleLayer.DisplayLine(aLine: TRect; aPen: TPenRec);
begin
  with mQualityMatrixBase do
  begin

    Canvas.Pen.Width := aPen.Width;
    Canvas.Pen.Color := aPen.Color;
    // 	  aPen.mode := Canvas.Pen.mode;
    Canvas.Pen.mode := aPen.mode;
    Canvas.Pen.Style := aPen.Style;

    Canvas.MoveTo(aLine.TopLeft.x, aLine.TopLeft.y);
    Canvas.LineTo(aLine.BottomRight.x, aLine.BottomRight.y);
  end;
end;
//------------------------------------------------------------------------------

procedure TScaleLayer.DisplayText(aText: TTextRec);
var
  xTextRect         : TRect;
  xBounds           : TRangeRec;
  xEnds             : boolean;
begin
  with mQualityMatrixBase, aText do
  begin
    xEnds := false;
    if pFont <> nil then
    begin
      if pFont^.Color <> clNone then
      begin
        Canvas.Font.Color := pFont^.Color;
        Canvas.Font.Style := pFont^.Style;
        Canvas.Font.Size := pFont^.Size;
        Canvas.Font.Charset := FontCharSet;
        Canvas.Font.Name := FontName;
      end
      else
        xEnds := True;
    end;

    if not xEnds then
    begin
      xBounds := mPixelAid.AlignItem(xPos, Canvas.TextWidth(text), horAlign);
      xTextRect.Left := xBounds.start;
      xTextRect.Right := xBounds.stop;

      xBounds := mPixelAid.AlignItem(yPos, Canvas.TextHeight(text), vertAlign);
      xTextRect.Top := xBounds.start;
      xTextRect.Bottom := xBounds.stop;

      Canvas.Brush.Style := bsClear;
      //%% Canvas.TextRect(xTextRect, xTextRect.Left, xTextRect.Top, text);
      Windows.TextOutW(Canvas.Handle, xTextRect.Left, xTextRect.Top, PWideChar(text), Length(text));  //%%
    end;
  end;
end;
//------------------------------------------------------------------------------

//procedure TScaleLayer.Paint;
procedure TScaleLayer.CustomPaint;
var
  i                 : Integer;
  xRecT             : TRect;
begin
  //	inherited Paint;

  // Draw Horizontal Line
  with mQualityMatrixBase do
  begin
    for i := 0 to TotHorLines - 1 do
    begin
      DisplayLine(HorLine[i], HorLinePen[i]);
    end;
  end;

  // Draw Vertical Line
  with mQualityMatrixBase do
  begin
    for i := 0 to TotVertLines - 1 do
    begin
      DisplayLine(VertLine[i], VertLinePen[i]);
    end;
  end;

  // Display XAxis Scales
  with mQualityMatrixBase do
  begin
    for i := 0 to TotXScales - 1 do
    begin
      DisplayText(XScaleText[i]);
    end;
  end;

  // Display YAxis Scales
  with mQualityMatrixBase do
  begin
    for i := 0 to TotYScales - 1 do
    begin
      DisplayText(YScaleText[i]);
    end;
  end;

  // Display Title
  with mQualityMatrixBase do
    if TitleEnabled then
    begin
      TitlePos.pFont^.Size := FontSize;
      DisplayText(TitlePos);
    end;

  // Display Labels
  with mQualityMatrixBase do
  begin
    for i := 0 to TotCaptions - 1 do
    begin
      DisplayText(Caption[i]);
    end;
  end;

  // Fill Background
  with mQualityMatrixBase do
  begin
    CoordinateMode := cmInside;
    for i := 0 to TotFields - 1 do
    begin
      xRecT := Field[i];
      //      FillField(xRect, BackgroundColor[i]);
    end;
  end;

end;

{
procedure TScaleLayer.Paint;
begin
  inherited Paint;

end;
}
//------------------------------------------------------------------------------

end.

