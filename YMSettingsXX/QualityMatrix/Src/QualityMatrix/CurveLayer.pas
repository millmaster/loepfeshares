(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: CurveLayer.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.05.1999  0.00  Kr  | Initial Release
|=========================================================================================*)
unit CurveLayer;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, ExtCtrls, QualityMatrixBase, QualityMatrixDef,
  DBAccessTypes, QualityMatrixDesc, YarnParaUtils, YarnParadef, Syncobjs;

const
  _Version_ = '$Revision: 2 $';
  _Date_ = '$Modtime: 25.02.04 17:38 $';
  // $NoKeywords: $

const
  cMaxSamples = 1;

  cChannelCurve = 0;
  cSpliceCurve = 1;
  cClusterCurve = 2;
  cMouseTargetThreshold = 4;
type
  //...........................................................................

  TCurveTypes = (cvtNSL, cvtThin, cvtNSLSplice, cvtThinSplice,
    cvtNSLCluster, cvtThinCluster);
  TChannelTypes = (ctShort, ctLong, ctThin, ctNeps, ctUnknown);
  TChannels = set of TChannelTypes;

  TFactorsRec = record
    df: Integer; // long or short dummy factor
    ap: Integer; // long or short parameter area
  end;
  //...........................................................................

  TOnTouchChannel = procedure(aChannelType: TChannelTypes) of object;
  //...........................................................................

  TChannelFormula = class(TObject)
  private
    { Private declarations }
    fChannelSettings: TIntegerChannelSettingsRec;

    mFactors: array[ctShort..ctThin] of TFactorsRec;

    function ChannelParaChanged(aPara: TChannelSettingsRec): Boolean;
    function GetChannel(aIndex: TChannelTypes): Boolean;
    function GetChannelPara: TChannelSettingsRec;

    procedure ComputeFactors;
    procedure PutChannelPara(aSettings: TChannelSettingsRec);
    function NaturalToInteger(const aSettings: TChannelSettingsRec): TIntegerChannelSettingsRec;
    function IntegerToNatural(const aSettings: TIntegerChannelSettingsRec): TChannelSettingsRec;


  protected
    { Protected declarations }

    property LongChannel: Boolean index ctLong read GetChannel;
    property NepsChannel: Boolean index ctNeps read GetChannel;
    property ShortChannel: Boolean index ctShort read GetChannel;
    property ThinChannel: Boolean index ctThin read GetChannel;
  public
    { Public declarations }
    constructor Create(); // override;
//    destructor Destroy; override;
    function ComputeDiameter(aChannel: TChannelTypes; length: Integer): Double;

    property ChannelSettings: TChannelSettingsRec read GetChannelPara write PutChannelPara;
  published
    { Published declarations }
  end;
  //...........................................................................

  TCurveControlRec = record
    totNSLSamples: Integer;
    totThinSamples: Integer;
    nepRange: TRangeRec;
    shortRange: TRangeRec;
    longRange: TRangeRec;
    thinRange: TRangeRec;
    xSectI, ySectI: Integer;
    sectHome: TPoint;
    xSection: TScaleRec;
    ySection: TScaleRec;
    visibleRange: TRect;
    nSLSamples: array of TPoint;
    thinSamples: array of TPoint;
    color: TColor
  end;
  //...........................................................................

  TClassFieldCurveRec = record
    Field: Integer; // Classfield index [0..TotFields-1, (0..127)]
    SubFields: string; // SubFieldY values: [0..SubFieldY]
  end;

  PTClassFieldCurveRec = ^TClassFieldCurveRec;

  TCutCurveList = class(TList)
  private
    fSubFieldY: Integer;
    fSubFieldX: Integer;
    procedure SetItems(aIndex: Integer; aValue: TClassFieldCurveRec);
    function GetItems(aIndex: Integer): TClassFieldCurveRec;
  public
//    constructor Create;
    destructor Destroy; override;

    function Add(aClassFieldCurve: TClassFieldCurveRec): Integer;
    procedure Clear; override;

    property Items[Index: Integer]: TClassFieldCurveRec read GetItems write SetItems;

    property SubFieldX: Integer read fSubFieldX;
      // Number of horizontal subfields of a matrix field currently used: 0..
    property SubFieldY: Integer read fSubFieldY;
      // Number of vertical subfields of a matrix field  currently used: 0..

  end;
  //...........................................................................

  TChannelCurve = class(TChannelFormula)
  private
    { Private declarations }
    fCurveColor: TColor;
    fCurveStyle: TPenStyle;
    fCurveVisible: Boolean;
    fCurveWidth: Integer;
    fEnabledChannels: TChannels;
    fHitChannel: TChannelTypes;
    fOnReleaseChannel: TOnTouchChannel;
    fOnParameterChange: TOnParameterChange;
    fParameterSource: TParameterSource;
    fSelectableChannels: TChannels;
    fSelectedChannel: TChannelTypes;

    mControl: TCurveControlRec;
    mCutCurve: TCutCurveList;

    mDragRoot: TPoint;
    mDragRootSettings: TChannelSettingsRec;

    mCriticalSection: TCriticalSection;
    mQualityMatrixBase: TQualityMatrixBase;
    mCalculationActive: Boolean;
    function GetChannelPara: TChannelSettingsRec;
    function HitCurve(aX, aY: Integer): TChannelTypes;
    function CheckHit(aX, aY: Integer; aRange: TRangeRec;
      aSamples: array of TPoint): Boolean;

    procedure AdjustChannel(const aDragTarget: TPoint);
    procedure ComputeNSLCurve;
    procedure ComputeThinCurve;
    procedure GenerateCutCurve(const aSamples: array of TPoint;
      const aTotSamples: Integer; const aReverse: Boolean); // const aRange: TRangeRec); overload;

//    procedure PaintCurve(aSamples: array of TPoint; aTotSamples: Integer);
    procedure PaintCurve(aSamples: array of TPoint;
      aTotSamples: Integer; aRange: TRangeRec); overload;
    procedure PutChannelPara(const aSettings: TChannelSettingsRec);
    procedure PutSplicePara(aSettings: TSpliceSettingsRec);
    procedure PutHitChannel(const aValue: TChannelTypes);
    procedure PutSelectableChannels(const aValue: TChannels);
    procedure PutSelectedChannel(const aValue: TChannelTypes);
    procedure SetChannelPara(const aSettings: TChannelSettingsRec);

  protected
    { Protected declarations }

    function EnterCalculation: Boolean;
    procedure LeaveCalculation;

  public
    { Public declarations }
    constructor Create(aQualityMatrixBase: TQualityMatrixBase); reintroduce;
    destructor Destroy; override;

    function GetCutCurve(const aCurveType: TCurveTypes): TCutCurveList;

    procedure CustomMouseDown(aShift: TShiftState; aX, aY: Integer);
    procedure CustomMouseMove(aShift: TShiftState; aX, aY: Integer);
    procedure CustomMouseUp(aShift: TShiftState; aX, aY: Integer);

    procedure SetSampleBufferLength;
    procedure PaintCurve; overload;

    property HitChannel: TChannelTypes read fHitChannel write PutHitChannel;
    property OnReleaseChannel: TOnTouchChannel read fOnReleaseChannel write fOnReleaseChannel;
    property SelectedChannel: TChannelTypes read fSelectedChannel write PutSelectedChannel;
  published
    { Published declarations }
    property ChannelParameter: TChannelSettingsRec read GetChannelPara write PutChannelPara;

    property CurveColor: TColor read fCurveColor write fCurveColor;
    property CurveStyle: TPenStyle read fCurveStyle write fCurveStyle;
    property CurveVisible: Boolean read fCurveVisible write fCurveVisible;
    property CurveWidth: Integer read fCurveWidth write fCurveWidth;
    property EnabledChannels: TChannels read fEnabledChannels write fEnabledChannels;

    property OnParameterChange: TOnParameterChange read fOnParameterChange write fOnParameterChange;
    property ParameterSource: TParameterSource read fParameterSource write fParameterSource;
    property SelectableChannels: TChannels read fSelectableChannels write PutSelectableChannels;

  end;
  //...........................................................................

  TCurveLayer = class(TObject)
  private
    { Private declarations }

    fImageStored: Boolean;
    fOnParameterChange: TOnParameterChange;
    fSelectCurve: Boolean;

    mCluster: TChannelCurve;
    mChannel: TChannelCurve;
    mImage: TImage;
    mQualityMatrixBase: TQualityMatrixBase;
    mSplice: TChannelCurve;

    function GetCurveColor(const aIndex: Integer): TColor;
    function GetCurveStyle(const aIndex: Integer): TPenStyle;
    function GetCurveVisible(const aIndex: Integer): Boolean;

    procedure DoParameterChange(aSource: TParameterSource);
    procedure DoReleaseChannel(aChannelType: TChannelTypes);
    procedure PutCurveColor(const aIndex: Integer; const aValue: TColor);
    procedure PutCurveStyle(const aIndex: Integer; const aValue: TPenStyle);
    procedure PutCurveVisible(const aIndex: Integer; const aValue: Boolean);
    procedure SetSelectCurve(const aValue: Boolean);

  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(aQualityMatrixBase: TQualityMatrixBase); reintroduce;
    destructor Destroy; override;

    function GetChannelPara: TChannelSettingsRec;
    function GetClusterDiameter: Double;
    function GetCutCurve(const aCurveType: TCurveTypes): TCutCurveList;
    function GetSplicePara: TChannelSettingsRec;

    procedure CustomMouseDown(aShift: TShiftState; aX, aY: Integer);
    procedure CustomMouseMove(aShift: TShiftState; aX, aY: Integer);
    procedure CustomMouseUp(aShift: TShiftState; aX, aY: Integer);
    procedure CustomPaint;
    procedure CustomResize;

    procedure PaintImage;
    procedure PutYMSettings(const aSettings: TYarnSettingsRec);
    procedure SetDefaultColor;
    procedure SetSampleBufferLength;
    procedure StoreImage;

  published
    { Published declarations }
    property ChannelColor: TColor index cChannelCurve read GetCurveColor write PutCurveColor;
    property ChannelStyle: TPenStyle index cChannelCurve read GetCurveStyle write PutCurveStyle;
    property ChannelVisible: Boolean index cChannelCurve read GetCurveVisible write PutCurveVisible;

    property ClusterColor: TColor index cClusterCurve read GetCurveColor write PutCurveColor;
    property ClusterStyle: TPenStyle index cClusterCurve read GetCurveStyle write PutCurveStyle;
    property ClusterVisible: Boolean index cClusterCurve read GetCurveVisible write PutCurveVisible;

    property ImageStored: Boolean read fImageStored write fImageStored;

    property OnParameterChange: TOnParameterChange read fOnParameterChange write fOnParameterChange;

    property SelectCurve: Boolean read fSelectCurve write SetSelectCurve;

    property SpliceColor: TColor index cSpliceCurve read GetCurveColor write PutCurveColor;
    property SpliceStyle: TPenStyle index cSpliceCurve read GetCurveStyle write PutCurveStyle;
    property SpliceVisible: Boolean index cSpliceCurve read GetCurveVisible write PutCurveVisible;

  end;
  //...........................................................................  TChannelCurve = class(TObject)

//------------------------------------------------------------------------------
// Channel Formula factors and definitions
//------------------------------------------------------------------------------
  //...........................................................................

const
// Diameter offlimit
  cDiaOfflimit = 1000 * 100 + 1; // visibleNSLTRange.Top*100+1;
// Short factors
  cKS = 350;
  cFS = 92;
  cGS = 40;
// Long factors
  cKL = 300;
  cFL = 98;
  cGL = 30;
// Thin factors
  cKT = 170;
  cFT = 102;
  cGT = 100;
// Invisible Pixcel
  cInvisiblePixcel = High(Integer);

implementation // 15.07.2002 added mmMBCS to imported units

//******************************************************************************
// TChannelFormula
//******************************************************************************
//------------------------------------------------------------------------------

function TChannelFormula.ChannelParaChanged(
  aPara: TChannelSettingsRec): Boolean;
begin
  with fChannelSettings do
  begin
    if (NepChannel <> aPara.NepChannel) or (NepDiameter <> aPara.NepDiameter) or
      (ShortChannel <> aPara.ShortChannel) or (ShortDiameter <> aPara.ShortDiameter) or
      (ShortDiameter <> aPara.ShortDiameter) or
      (LongChannel <> aPara.LongChannel) or (LongDiameter <> aPara.LongDiameter) or
      (LongLength <> aPara.LongLength) or
      (ThinChannel <> aPara.ThinChannel) or (ThinDiameter <> aPara.ThinDiameter) or
      (ThinLength <> aPara.ThinLength) then
      Result := True
    else
      Result := False;
  end;
end;
//------------------------------------------------------------------------------

function TChannelFormula.ComputeDiameter(aChannel: TChannelTypes;
  length: Integer): Double;
var xDf: Double;
begin
  xDf := mFactors[ctLong].df;
  case aChannel of
    ctNeps:
      begin
        with fChannelSettings do
          if NepChannel = swOn then
            Result := NepDiameter * 100
          else
            Result := cDiaOfflimit;
      end;
    ctShort, ctLong, ctThin:
      begin
        with mFactors[aChannel] do
          if length <> 0 then
            Result := df + ap / length
          else if xDf <> 0 then
            Result := cDiaOfflimit
          else
            Result := cDiaOfflimit + 1;
      end;
  else
    Result := cDiaOfflimit;
  end;
end;
//------------------------------------------------------------------------------

procedure TChannelFormula.ComputeFactors;
begin
  with fChannelSettings do
  begin

    // Short curve factor set
    with mFactors[ctShort] do
      if ShortChannel = swOn then
      begin
        df := cFS * ShortDiameter;
        ap := ((((cKS * (df - 10000)) div 10) + 100000 - df * 10) div 100) * ShortLength * cGS;
      end
      else
      begin
        df := cDiaOfflimit;
        ap := 0;
      end;

    // Long curve factor set
    with mFactors[ctLong] do
      if LongChannel = swOn then
      begin
        df := cFL * LongDiameter;
        ap := ((((cKL * (df - 10000)) div 10) + 100000 - df * 10) div 100) * LongLength * cGL;
      end
      else
      begin
        df := cDiaOfflimit;
        ap := 0;
      end;

    // Thin curve factor set
    with mFactors[ctThin] do
      if ThinChannel = swOn then
      begin
        df := cFT * ThinDiameter;
        ap := -((df * 10 - ((cKT * (df - 10000)) div 10) - 100000)) * ThinLength;
      end
      else
      begin
        df := cDiaOfflimit;
        ap := 0;
      end;
  end;
end;
//------------------------------------------------------------------------------

constructor TChannelFormula.Create;
begin
  inherited create;
//%%
  fChannelSettings := cTopSixSettings;
//  fChannelSettings := cTestSettings;
end;
//------------------------------------------------------------------------------

function TChannelFormula.GetChannel(aIndex: TChannelTypes): Boolean;
begin
  with fChannelSettings do
    case aIndex of
      ctNeps:
        if NepChannel = swOn then
          Result := True
        else
          Result := False;

      ctShort:
        if ShortChannel = swOn then
          Result := True
        else
          Result := False;

      ctLong:
        if LongChannel = swOn then
          Result := True
        else
          Result := False;

      ctThin:
        if ThinChannel = swOn then
          Result := True
        else
          Result := False;

    else
      Result := False;
    end;
end;
//------------------------------------------------------------------------------

function TChannelFormula.GetChannelPara: TChannelSettingsRec;
begin
  Result := IntegerToNatural(fChannelSettings);
end;
//------------------------------------------------------------------------------

function TChannelFormula.IntegerToNatural(const aSettings: TIntegerChannelSettingsRec): TChannelSettingsRec;
begin
  with Result do
  begin
    NepChannel := aSettings.NepChannel;
    ShortChannel := aSettings.ShortChannel;
    LongChannel := aSettings.LongChannel;
    ThinChannel := aSettings.ThinChannel;
//%%% delete if verified
    NepDiameter := aSettings.NepDiameter / 100;
    ShortDiameter := aSettings.ShortDiameter / 100;
    ShortLength := aSettings.ShortLength / 10;
    LongDiameter := aSettings.LongDiameter / 100;
    LongLength := aSettings.LongLength / 10;
    ThinDiameter := aSettings.ThinDiameter - 100;
    ThinLength := aSettings.ThinLength / 10;
  end;
end;
//------------------------------------------------------------------------------

function TChannelFormula.NaturalToInteger(const aSettings: TChannelSettingsRec): TIntegerChannelSettingsRec;
begin
  with Result do
  begin
    NepChannel := aSettings.NepChannel;
    ShortChannel := aSettings.ShortChannel;
    LongChannel := aSettings.LongChannel;
    ThinChannel := aSettings.ThinChannel;

//%%% delete if verified
    NepDiameter := round(aSettings.NepDiameter * 100);
    ShortDiameter := round(aSettings.ShortDiameter * 100);
    ShortLength := round(aSettings.ShortLength * 10);
    LongDiameter := round(aSettings.LongDiameter * 100);
    LongLength := round(aSettings.LongLength * 10);
    ThinDiameter := round(100 + aSettings.ThinDiameter);
    ThinLength := round(aSettings.ThinLength * 10);
  end;
end;
//------------------------------------------------------------------------------

procedure TChannelFormula.PutChannelPara(aSettings: TChannelSettingsRec);
begin
  fChannelSettings := NaturalToInteger(aSettings);
  ComputeFactors;
end;

//******************************************************************************
// TCurveLayer
//******************************************************************************
//------------------------------------------------------------------------------

constructor TCurveLayer.Create(aQualityMatrixBase: TQualityMatrixBase);
begin
  inherited Create;

  fOnParameterChange := nil;

  mQualityMatrixBase := aQualityMatrixBase;

  mImage := TImage.Create(mQualityMatrixBase);
  ImageStored := False;

  SelectCurve := True;

  mCluster := TChannelCurve.Create(aQualityMatrixBase);
  mChannel := TChannelCurve.Create(aQualityMatrixBase);
  mSplice := TChannelCurve.Create(aQualityMatrixBase);

  SetDefaultColor;

  mChannel.ParameterSource := psChannel;
  mChannel.OnParameterChange := DoParameterChange;
  mChannel.OnReleaseChannel := DoReleaseChannel;
  mChannel.CurveWidth := cSpliceCurvePen.width;
  ChannelStyle := cChannelCurvePen.style;
  ChannelVisible := True;

  mCluster.ParameterSource := psCluster;
  mCluster.OnParameterChange := DoParameterChange;
  mCluster.OnReleaseChannel := DoReleaseChannel;
  mCluster.CurveWidth := cClusterCurvePen.width;
  ClusterStyle := cClusterCurvePen.style;
//  mCluster.EnabledChannels := [ctShort, ctLong, ctNeps];
  mCluster.SelectableChannels := [ctShort];
  ClusterVisible := True;

  mSplice.ParameterSource := psSplice;
  mSplice.OnParameterChange := DoParameterChange;
  mSplice.OnReleaseChannel := DoReleaseChannel;
  mSplice.CurveWidth := cSpliceCurvePen.width;
  SpliceStyle := cSpliceCurvePen.style;
  SpliceVisible := True;

end;
//------------------------------------------------------------------------------

procedure TCurveLayer.CustomMouseDown(aShift: TShiftState; aX,
  aY: Integer);
begin

  if SelectCurve then
  begin

    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
      mChannel.CustomMouseDown(aShift, aX, aY);

    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) then
      mCluster.CustomMouseDown(aShift, aX, aY);

    if (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
      mSplice.CustomMouseDown(aShift, aX, aY);
  end;

end;
//------------------------------------------------------------------------------

procedure TCurveLayer.CustomMouseMove(aShift: TShiftState; aX,
  aY: Integer);
begin

  if SelectCurve then
  begin

    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
    begin
      if mChannel.HitChannel = ctUnknown then
        mQualityMatrixBase.Hint := '(*)Kanal:';
      mChannel.CustomMouseMove(aShift, aX, aY);
    end;

    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) then
    begin
      if mCluster.HitChannel = ctUnknown then
        mQualityMatrixBase.Hint := '(*)Cluster:';
      mCluster.CustomMouseMove(aShift, aX, aY);
    end;

    if (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
    begin
      if mSplice.HitChannel = ctUnknown then
        mQualityMatrixBase.Hint := '(*)Spleiss:';
      mSplice.CustomMouseMove(aShift, aX, aY);
    end;
  end;

end;
//------------------------------------------------------------------------------

procedure TCurveLayer.CustomMouseUp(aShift: TShiftState; aX, aY: Integer);
begin

  if SelectCurve then
  begin

    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
      mChannel.CustomMouseUp(aShift, aX, aY);

    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) then
      mCluster.CustomMouseUp(aShift, aX, aY);

    if (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
      mSplice.CustomMouseUp(aShift, aX, aY);
  end;

end;
//------------------------------------------------------------------------------

procedure TCurveLayer.CustomPaint;
begin

  if mChannel.HitChannel <> ctUnknown then
  begin

    if not ImageStored then
    begin

      mSplice.PaintCurve;
      mCluster.PaintCurve;

      StoreImage;
      ImageStored := True;
    end;

    mChannel.PaintCurve;
  end
  else if mCluster.HitChannel <> ctUnknown then
  begin

    if not ImageStored then
    begin

      mSplice.PaintCurve;
      mChannel.PaintCurve;

      StoreImage;
      ImageStored := True;
    end;

    mCluster.PaintCurve;

  end
  else if mSplice.HitChannel <> ctUnknown then
  begin

    if not ImageStored then
    begin

      mCluster.PaintCurve;
      mChannel.PaintCurve;

      StoreImage;
      ImageStored := True;
    end;

    mSplice.PaintCurve;

  end
  else
  begin

    mSplice.PaintCurve;
    mCluster.PaintCurve;
    mChannel.PaintCurve;
  end;

end;
//------------------------------------------------------------------------------

procedure TCurveLayer.CustomResize;
begin
  mChannel.ComputeThinCurve;
  mChannel.ComputeNSLCurve;
  mCluster.ComputeThinCurve;
  mCluster.ComputeNSLCurve;
  mSplice.ComputeThinCurve;
  mSplice.ComputeNSLCurve;
end;
//------------------------------------------------------------------------------

destructor TCurveLayer.Destroy;
begin
  FreeAndNil(mCluster);
  FreeAndNil(mChannel);
  FreeAndNil(mSplice);
  FreeAndNil(mImage);

  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TCurveLayer.DoParameterChange(aSource: TParameterSource);
begin
  if Assigned(fOnParameterChange) then
    fOnParameterChange(aSource);
end;
//------------------------------------------------------------------------------

procedure TCurveLayer.DoReleaseChannel(aChannelType: TChannelTypes);
begin

  if ImageStored then
    ImageStored := False;
end;
//------------------------------------------------------------------------------

function TCurveLayer.GetChannelPara: TChannelSettingsRec;
begin
//%%  Result := mChannel.ChannelParameter;
end;
//------------------------------------------------------------------------------

function TCurveLayer.GetClusterDiameter: Double;
begin
  Result := mCluster.ChannelParameter.ShortDiameter;
end;
//------------------------------------------------------------------------------

function TCurveLayer.GetCurveColor(const aIndex: Integer): TColor;
begin
  Result := clSilver;
  case aIndex of

    cChannelCurve:
      Result := mChannel.CurveColor;

    cClusterCurve:
      Result := mCluster.CurveColor;

    cSpliceCurve:
      Result := mSplice.CurveColor;
  end;
end;
//------------------------------------------------------------------------------

function TCurveLayer.GetCurveStyle(const aIndex: Integer): TPenStyle;
begin
  Result := psSolid;
  case aIndex of

    cChannelCurve:
      Result := mChannel.CurveStyle;

    cClusterCurve:
      Result := mCluster.CurveStyle;

    cSpliceCurve:
      Result := mSplice.CurveStyle;
  end;

end;
//------------------------------------------------------------------------------

function TCurveLayer.GetCurveVisible(const aIndex: Integer): Boolean;
begin
  Result := False;
  case aIndex of

    cChannelCurve:
      Result := mChannel.CurveVisible;

    cClusterCurve:
      Result := mCluster.CurveVisible;

    cSpliceCurve:
      Result := mSplice.CurveVisible;
  end;

end;
//------------------------------------------------------------------------------

function TCurveLayer.GetCutCurve(
  const aCurveType: TCurveTypes): TCutCurveList;
begin
  Result := nil;

  case aCurveType of

    cvtNSL, cvtThin:
      Result := mChannel.GetCutCurve(aCurveType);

    cvtNSLSplice:
      Result := mSplice.GetCutCurve(cvtNSL);

    cvtThinSplice:
      Result := mSplice.GetCutCurve(cvtThin);

    cvtNSLCluster:
      Result := mCluster.GetCutCurve(cvtNSL);

    cvtThinCluster:
      Result := mCluster.GetCutCurve(cvtThin);
  end;
end;
//------------------------------------------------------------------------------

function TCurveLayer.GetSplicePara: TChannelSettingsRec;
begin
  //%% Result := mSplice.ChannelParameter;
end;
//------------------------------------------------------------------------------

procedure TCurveLayer.PaintImage;
var
  xClientRect: TRect;
begin
  with mQualityMatrixBase do
  begin
    xClientRect := Rect(0, 0, ClientWidth, ClientHeight);
// Temporary for Release because Curve draging is not finished yet
//    Canvas.CopyRect(xClientRect, mImage.Canvas, xClientRect);

    Canvas.CopyRect(xClientRect, mImage.Canvas, xClientRect);
  end;

end;
//------------------------------------------------------------------------------

procedure TCurveLayer.PutCurveColor(const aIndex: Integer;
  const aValue: TColor);
begin
  case aIndex of

    cChannelCurve:
      mChannel.CurveColor := aValue;

    cClusterCurve:
      mCluster.CurveColor := aValue;

    cSpliceCurve:
      mSplice.CurveColor := aValue;
  end;
end;
//------------------------------------------------------------------------------

procedure TCurveLayer.PutCurveStyle(const aIndex: Integer;
  const aValue: TPenStyle);
begin
  case aIndex of

    cChannelCurve:
      mChannel.CurveStyle := aValue;

    cClusterCurve:
      mCluster.CurveStyle := aValue;

    cSpliceCurve:
      mSplice.CurveStyle := aValue;
  end;
end;
//------------------------------------------------------------------------------

procedure TCurveLayer.PutCurveVisible(const aIndex: Integer;
  const aValue: Boolean);
begin
  case aIndex of

    cChannelCurve:
      mChannel.CurveVisible := aValue;

    cClusterCurve:
      mCluster.CurveVisible := aValue;

    cSpliceCurve:
      mSplice.CurveVisible := aValue;
  end;

end;
//------------------------------------------------------------------------------

procedure TCurveLayer.PutYMSettings(const aSettings: TYarnSettingsRec);
var
  xPara: TChannelSettingsRec;
begin

  with aSettings do
  begin

    mChannel.PutChannelPara(ChannelSettings);

(*
    xPara := ChannelSettings;
    if aSettings.Cluster.ShortClusterMonitor = swOff then
    begin
      xPara.NepChannel := swOff;
      xPara.ShortChannel := swOff;
      xPara.LongChannel := swOff;
      xPara.ThinChannel := swOff;
    end
    else
    begin
      if xPara.ShortChannel = swOn then
      begin
        if aSettings.Cluster.ShortClusterDiameter < xPara.ShortDiameter then
          xPara.ShortDiameter := aSettings.Cluster.ShortClusterDiameter;
      end;
    end;
*)
    with aSettings.Cluster do
    begin
      xPara.NepChannel := swOff;
      xPara.NepDiameter := ChannelSettings.NepDiameter;
      xPara.ShortChannel := ShortClusterChannel;
      xPara.ShortDiameter := ShortClusterDiameter;
      xPara.ShortLength := ShortClusterLength;
      xPara.LongChannel := LongClusterChannel;
      xPara.LongDiameter := LongClusterDiameter;
      xPara.LongLength := LongClusterLength;
      xPara.ThinChannel := ThinClusterChannel;
      xPara.ThinDiameter := ThinClusterDiameter;
      xPara.ThinLength := ThinClusterLength;
    end;
    mCluster.PutChannelPara(xPara);

    mSplice.PutSplicePara(aSettings.SpliceSettings);
  end;

end;
//------------------------------------------------------------------------------

procedure TCurveLayer.SetDefaultColor;
begin

  ChannelColor := cChannelCurvePen.color;
  ClusterColor := cClusterCurvePen.color;
  SpliceColor := cSpliceCurvePen.color;

end;
//------------------------------------------------------------------------------

procedure TCurveLayer.SetSampleBufferLength;
begin

  mChannel.SetSampleBufferLength;
  mCluster.SetSampleBufferLength;
  mSplice.SetSampleBufferLength;
end;
//------------------------------------------------------------------------------

procedure TCurveLayer.SetSelectCurve(const aValue: Boolean);
begin
  fSelectCurve := aValue;
end;
//------------------------------------------------------------------------------

procedure TCurveLayer.StoreImage;
var
  xClientRect: TRect;
begin
  with mQualityMatrixBase do
  begin
    xClientRect := Rect(0, 0, ClientWidth, ClientHeight);

    mImage.Width := Width;
    mImage.Height := Height;

    mImage.Width := ClientWidth;
    mImage.Height := ClientHeight;
    mImage.Canvas.CopyRect(xClientRect, Canvas, xClientRect);
  end;

end;
//------------------------------------------------------------------------------

//******************************************************************************
// TChannelCurve
//******************************************************************************
//------------------------------------------------------------------------------

procedure TChannelCurve.AdjustChannel(const aDragTarget: TPoint);
var
  xSettings: TChannelSettingsRec;
  xLen, xDia: Double;

  procedure CalculateChannelPara(aGraphField: TRect; var aDia: Double; var aLen: Double);
  begin
    with aGraphField do
    begin

      if aDragTarget.y < Top then
        aDia := cMaxDia
      else if aDragTarget.y > Bottom then
        aDia := cMinDia
      else if SelectedChannel = ctThin then
      begin

        if mDragRoot.y <= Top then mDragRoot.y := Top + 1;
        aDia := (((((mDragRoot.y - aDragTarget.y) * 1000 div (Top - mDragRoot.y)) + 1000) * (aDia
          )) / 1000);
      end
      else
      begin

        if mDragRoot.y >= Bottom then mDragRoot.y := Bottom - 1;
        aDia := 1 + (((((mDragRoot.y - aDragTarget.y) * 1000 / (Bottom - mDragRoot.y)) + 1000) * (aDia
          - 1)) / 1000);
      end;

      if aDragTarget.x < Left then
        aLen := cMinLen
      else if aDragTarget.x > Right then
        aLen := cMaxLen
      else
      begin

        if mDragRoot.x <= Left then mDragRoot.x := Left + 1;
        aLen := (((((aDragTarget.x - mDragRoot.x) * 1000 / (mDragRoot.x - Left)) + 1000) * aLen) /
          1000);
      end;
    end;
  end;
begin

  xSettings := mDragRootSettings;
  case SelectedChannel of

    ctNeps:
      begin

        xDia := mDragRootSettings.NepDiameter;
        CalculateChannelPara(mQualityMatrixBase.NSLGraphField, xDia, xLen);
        xSettings.NepDiameter := xDia;
      end;

    ctShort:
      begin

        xDia := mDragRootSettings.ShortDiameter;
        xLen := mDragRootSettings.ShortLength;
        CalculateChannelPara(mQualityMatrixBase.NSLGraphField, xDia, xLen);
        xSettings.ShortDiameter := xDia;
        xSettings.ShortLength := xLen;
      end;

    ctLong:
      begin

        xDia := mDragRootSettings.LongDiameter;
        xLen := mDragRootSettings.LongLength;
        CalculateChannelPara(mQualityMatrixBase.NSLGraphField, xDia, xLen);
        xSettings.LongDiameter := xDia;
        xSettings.LongLength := xLen;
      end;
    ctThin:
      begin
        xDia := mDragRootSettings.ThinDiameter;
        xLen := mDragRootSettings.ThinLength;
{
        xThinGraphField.Left := mQualityMatrixBase.ThinGraphField.Left;
        xThinGraphField.Top := mQualityMatrixBase.ThinGraphField.Bottom;
        xThinGraphField.Right := mQualityMatrixBase.ThinGraphField.Right;
        xThinGraphField.Bottom := mQualityMatrixBase.ThinGraphField.Top;
        CalculateChannelPara(xThinGraphField, xDia, xLen);
}
        CalculateChannelPara(mQualityMatrixBase.ThinGraphField, xDia, xLen);
        xSettings.ThinDiameter := xDia;
        xSettings.ThinLength := xLen;

      end;
  end;
  TYMSettingsUtils.ValidateChannelSettings(xSettings);

  if ChannelParaChanged(xSettings) then
  begin

    SetChannelPara(xSettings);

    if Assigned(fOnParameterChange) then
      fOnParameterChange(ParameterSource);
  end;
end;
//------------------------------------------------------------------------------

function TChannelCurve.CheckHit(aX, aY: Integer; aRange: TRangeRec;
  aSamples: array of TPoint): Boolean;
var
  i: Integer;
  xLow, xHigh: TPoint;

  procedure EvaluateLowHigh(aValue1, aValue2: Integer; var aLow, aHigh: Integer);
  begin
    if aValue1 < aValue2 then
    begin

      aLow := aValue1;
      aHigh := aValue2;
    end
    else
    begin

      aLow := aValue2;
      aHigh := aValue1;
    end;
  end;
begin

  Result := False;
  with mControl, aRange do
  begin

    if (start >= 0) and (start < mQualityMatrixBase.TotSamples)
      and (stop >= 0) and (start < mQualityMatrixBase.TotSamples) then
    begin

      EvaluateLowHigh(aSamples[start].x, aSamples[stop].x, xLow.x, xHigh.x);
      EvaluateLowHigh(aSamples[start].y, aSamples[stop].y, xLow.y, xHigh.y);

      if (aX >= xLow.x) and (aX <= xHigh.x) and
        (aY >= xLow.y - cMouseTargetThreshold) and (aY <= xHigh.y + cMouseTargetThreshold) then
      begin

        i := Start;
        while i < Stop do
        begin

          EvaluateLowHigh(aSamples[i].x, aSamples[i + 1].x, xLow.x, xHigh.x);
          EvaluateLowHigh(aSamples[i].y, aSamples[i + 1].y, xLow.y, xHigh.y);

          if (aX >= xLow.x) and (aX <= xHigh.x) and
            (aY >= xLow.y - cMouseTargetThreshold) and (aY <= xHigh.y + cMouseTargetThreshold) then
          begin
            Result := True;
            i := stop;
          end
          else
            Inc(i);
        end;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TChannelCurve.ComputeNSLCurve;
var
  xNextScale, xLX, xDeltaIncr: Double;
  l, i, xOI: Integer;
  xLowerDia, xDia: Double;
  xPixcelFactX, xPixcelFactY: Integer;
  xNextXSection, xNextYSection: TScaleRec;
  xActiveChannel: TChannelTypes;
  xBreakFlag: Boolean;
  xGraphField: TRect;
begin

  if not EnterCalculation then
    Exit; // <============= mControl in use

  with mControl, mQualityMatrixBase do
  begin

    totNSLSamples := 0;
    nepRange.start := TotSamples + 1;
    nepRange.stop := TotSamples + 1;
    shortRange.start := TotSamples + 1;
    shortRange.stop := TotSamples + 1;
    longRange.start := TotSamples + 1;
    longRange.stop := TotSamples + 1;
    xActiveChannel := ctUnknown;

    if ((ctNeps in EnabledChannels) and NepsChannel) or
      ((ctShort in EnabledChannels) and ShortChannel) or
      ((ctLong in EnabledChannels) and LongChannel) then
    begin

      xSectI := 0; ySectI := 0;
      xGraphField := GraphField;
      sectHome.x := xGraphField.Left;
      sectHome.y := xGraphField.Top;
      ySection := YScale[ySectI];
      xNextYSection := YScale[ySectI + 1];
      xSection := XScale[xSectI];
      xNextXSection := XScale[xSectI + 1];
      visibleRange := VisibleNSLRange;

      i := 0;
      xLowerDia := cDiaOfflimit;

      xBreakFlag := False;
      while (xSection.pixcelToNextScale <> 0) and not xBreakFlag do
      begin

        with mControl.xSection do
        begin

          xLX := value * 1000;
          xNextScale := xNextXSection.value * 1000;
          if xNextXSection.pixcelToNextScale <> 0 then
            xDeltaIncr := (xNextScale - xLX) / numberOfSamples
          else
          begin
            xDeltaIncr := (xNextScale - xLX) / (numberOfSamples - 1);
            xNextScale := numberOfSamples * xDeltaIncr + xLX;
          end;
          totNSLSamples := totNSLSamples + numberOfSamples;

        // precalculat x and y  pixcel factors
          if numberOfSamples <> 0 then
          begin
            if xNextXSection.pixcelToNextScale <> 0 then
              xPixcelFactX := (100 * pixcelToNextScale) div numberOfSamples
            else
              xPixcelFactX := (100 * pixcelToNextScale) div (numberOfSamples - 1);
          end
          else
            xPixcelFactX := 1;

          if ySection.pixcelToNextScale <> 0 then
            xPixcelFactY := 100 * (ySection.value - xNextYSection.value) div ySection.pixcelToNextScale
          else
            xPixcelFactY := 1;

          xOI := 0;
          while xLX <= xNextScale - xDeltaIncr do
          begin
            l := Round(xLX / 100);

            case xActiveChannel of

              ctUnknown:
                begin
            // Nep curve
                  if ctNeps in EnabledChannels then
                  begin

                    xDia := ComputeDiameter(ctNeps, l);
                    if xDia < xLowerDia then
                    begin
                      xLowerDia := xDia;
                      xActiveChannel := ctNeps;
                      mControl.nepRange.start := i;
                    end;
                  end;

            // Short curve
                  if ctShort in EnabledChannels then
                  begin

                    xDia := ComputeDiameter(ctShort, l);
                    if xDia < xLowerDia then
                    begin
                      xLowerDia := xDia;
                      xActiveChannel := ctShort;
                      nepRange.start := TotSamples + 1;
                      mControl.shortRange.start := i;
                    end;
                  end;

            // Long curve
                  if ctLong in EnabledChannels then
                  begin

                    xDia := ComputeDiameter(ctLong, l);
                    if xDia < xLowerDia then
                    begin
                      xLowerDia := xDia;
                      xActiveChannel := ctLong;
                      mControl.nepRange.start := TotSamples + 1;
                      mControl.shortRange.start := TotSamples + 1;
                      mControl.longRange.start := i;
                    end;
                  end;
                end;

              ctNeps:
                begin
            // Nep curve
                  xLowerDia := ComputeDiameter(ctNeps, l);

            // Short curve
                  if ctShort in EnabledChannels then
                  begin

                    xDia := ComputeDiameter(ctShort, l);
                    if xDia < xLowerDia then
                    begin
                      xLowerDia := xDia;
                      xActiveChannel := ctShort;
                      mControl.shortRange.start := i;
                      mControl.nepRange.stop := i;
                    end;
                  end;

            // Long curve
                  if ctLong in EnabledChannels then
                  begin

                    xDia := ComputeDiameter(ctLong, l);
                    if xDia < xLowerDia then
                    begin
                      xLowerDia := xDia;
                      xActiveChannel := ctLong;
                      mControl.shortRange.start := TotSamples + 1;
                      mControl.longRange.start := i;
                      mControl.nepRange.stop := i;
                    end;
                  end;
                end;

              ctShort:
                begin
            // Short curve
                  xLowerDia := ComputeDiameter(ctShort, l);

            // Long curve
                  if ctLong in EnabledChannels then
                  begin

                    xDia := ComputeDiameter(ctLong, l);
                    if xDia < xLowerDia then
                    begin
                      xLowerDia := xDia;
                      xActiveChannel := ctLong;
                      longRange.start := i;
                      shortRange.stop := i;
                    end;
                  end;
                end;

              ctLong:
            // Long curve
                xLowerDia := ComputeDiameter(ctLong, l);
            end;

         // Convert to x and y pixcel
            mControl.nSLSamples[i].x := sectHome.x + (xOI * xPixcelFactX) div 100;

            if (xLowerDia > ySection.value * 100) then
              nSLSamples[i].y := 0
            else if xLowerDia > xNextYSection.value * 100 then
              nSLSamples[i].y := round(sectHome.y + (100 * ySection.value - xLowerDia) / xPixcelFactY)
            else
              while (ySection.pixcelToNextScale <> 0) and
                (xLowerDia <= xNextYSection.value * 100) do
              begin
                sectHome.y := sectHome.y + ySection.pixcelToNextScale;
                Inc(ySectI);
                ySection := YScale[ySectI];

                if ySection.pixcelToNextScale <> 0 then
                begin
                  xNextYSection := YScale[ySectI + 1];
                  xPixcelFactY := 100 * (ySection.value - xNextYSection.value) div ySection.pixcelToNextScale;
                end;
              end;
            if (ySection.pixcelToNextScale = 0) or
              (xLowerDia <= visibleRange.Bottom * 100) or
              (l > visibleRange.Right * 10) then
            begin
              totNSLSamples := i;
              xBreakFlag := True; // Break main loop too!
  (* BREAK *) break;
            end
            else
            begin

              if ((xLowerDia <= ySection.value * 100) and (xLowerDia > visibleRange.Top * 100)) or
                (l < visibleRange.Left * 10) then
                nSLSamples[i].y := cInvisiblePixcel
              else
              begin
                nSLSamples[i].y := round(sectHome.y + (100 * ySection.value - xLowerDia) / xPixcelFactY);
                nSLSamples[i + 1].y := cInvisiblePixcel;
              end;
            end;

            xLX := xLX + xDeltaIncr; Inc(i); Inc(xOI);
          end;
          sectHome.x := sectHome.x + pixcelToNextScale;

        end;

        Inc(xSectI);
        xSection := XScale[xSectI];
        xNextXSection := XScale[xSectI + 1];

      end;
//      mControl.longRange.stop := totNSLSamples-1;

    end
    else
      totNSLSamples := 0;

    case xActiveChannel of
      ctNeps:
        mControl.nepRange.stop := totNSLSamples - 1;

      ctShort:
        begin
          if mControl.nepRange.stop >= totNSLSamples then
            mControl.nepRange.stop := totNSLSamples - 1;
          mControl.shortRange.stop := totNSLSamples - 1;
        end;

      ctLong:
        begin
          if mControl.nepRange.stop >= totNSLSamples then
            mControl.nepRange.stop := totNSLSamples - 1;
          if mControl.shortRange.stop >= totNSLSamples then
            mControl.shortRange.stop := totNSLSamples - 1;
          mControl.longRange.stop := totNSLSamples - 1;
        end;

      ctThin:
        mControl.thinRange.stop := totNSLSamples - 1;

    end;

  end;

  LeaveCalculation; // <================ mControl free

end;
//------------------------------------------------------------------------------

procedure TChannelCurve.ComputeThinCurve;
var
  xPreviousScale, xLX, xDeltaIncr, l, i, xOI: Integer;
  xLowerDia: Integer;
  xPixcelFactX, xPixcelFactY: Integer;
  previousXSection, xNextYSection: TScaleRec;
  xBreakFlag: Boolean;
  xGraphField: TRect;
  xActiveChannel: TChannelTypes;
begin

  if not EnterCalculation then
    Exit; // <============= mControl in use


  with mControl, mQualityMatrixBase do
  begin

    totThinSamples := 0;

    thinRange.start := TotSamples + 1;
    thinRange.stop := TotSamples + 1;
    xActiveChannel := ctUnknown;

    if (ctThin in EnabledChannels) and ThinChannel then
    begin

      xSectI := TotXScales - 1; ySectI := 0;
      xGraphField := GraphField;
      sectHome.x := xGraphField.Right;
      sectHome.y := xGraphField.Top;
      ySection := YScale[ySectI];
      xNextYSection := YScale[ySectI + 1];
      xSection := XScale[xSectI];
      previousXSection := XScale[xSectI - 1];
      visibleRange := VisibleTRange;

      i := 0;

      xBreakFlag := False;
      while (xSectI > 0) and not xBreakFlag do
      begin

        with mControl.xSection do
        begin

          xLX := value * 1000;
          xPreviousScale := previousXSection.value * 1000;
          if xSectI <= 1 then
            xDeltaIncr := (xLX - xPreviousScale) div previousXSection.numberOfSamples
          else
          begin
            xDeltaIncr := (xLX - xPreviousScale) div (previousXSection.numberOfSamples - 1);
            xPreviousScale := xLX - (previousXSection.numberOfSamples * xDeltaIncr);
          end;
          totThinSamples := totThinSamples + previousXSection.numberOfSamples;

        // precalculat x and y  pixcel factors
          if previousXSection.numberOfSamples <> 0 then
          begin
            if previousXSection.pixcelToNextScale <> 0 then
              xPixcelFactX := (100 * previousXSection.pixcelToNextScale) div
                previousXSection.numberOfSamples
            else
              xPixcelFactX := (100 * previousXSection.pixcelToNextScale) div
                (previousXSection.numberOfSamples - 1);
          end
          else
            xPixcelFactX := 1;

          if ySection.pixcelToNextScale <> 0 then
            xPixcelFactY := 100 * (ySection.value - xNextYSection.value) div ySection.pixcelToNextScale
          else
            xPixcelFactY := 1;

          xOI := 0;
          while xLX > xPreviousScale + xDeltaIncr do
          begin
            l := xLX div 100;

            case xActiveChannel of

              ctUnknown:
                begin
             // Thin curve
                  xLowerDia := Round(ComputeDiameter(ctThin, l));
                  if xLowerDia <= ySection.value * 100 then
                  begin
                    xActiveChannel := ctThin;
                    i := 0;
                    mControl.thinRange.start := i;
                  end;
                end;

            else
              begin
             // Thin curve
                xLowerDia := Round(ComputeDiameter(ctThin, l));
              end;
            end;

         // Convert to x and y pixcel
            mControl.thinSamples[i].x := sectHome.x - (xOI * xPixcelFactX) div 100;

            if (xLowerDia > ySection.value * 100) then
              thinSamples[i].y := 0
            else if xLowerDia > xNextYSection.value * 100 then
              thinSamples[i].y := sectHome.y + (100 * ySection.value - xLowerDia) div xPixcelFactY
            else
              while (ySection.pixcelToNextScale <> 0) and
                (xLowerDia <= xNextYSection.value * 100) do
              begin
                sectHome.y := sectHome.y + ySection.pixcelToNextScale;
                Inc(ySectI);
                ySection := YScale[ySectI];

                if ySection.pixcelToNextScale <> 0 then
                begin
                  xNextYSection := YScale[ySectI + 1];
                  xPixcelFactY := 100 * (ySection.value - xNextYSection.value) div
                    ySection.pixcelToNextScale;

//                xActiveChannel := ctUnknown;
                end;
              end;
            if (ySection.pixcelToNextScale = 0) or
              (xLowerDia <= visibleRange.Bottom * 100) or
              (l < visibleRange.Left * 10) then
            begin
              totThinSamples := i;
              xBreakFlag := True; // Break main loop also!
  (* BREAK *) break;
            end
            else
            begin

              if ((xLowerDia <= ySection.value * 100) and (xLowerDia > visibleRange.Top * 100)) or
                (l > visibleRange.Right * 10) then
                thinSamples[i].y := cInvisiblePixcel
              else
              begin
                thinSamples[i].y := sectHome.y + (100 * ySection.value - xLowerDia) div xPixcelFactY;
                thinSamples[i + 1].y := cInvisiblePixcel;
              end;
            end;

            xLX := xLX - xDeltaIncr; Inc(i); Inc(xOI);
            if thinSamples[0].y = cInvisiblePixcel then
            begin
              xActiveChannel := ctUnknown;
            end;

          end;
          sectHome.x := sectHome.x - previousXSection.pixcelToNextScale;

        end;

        Dec(xSectI);
        xSection := XScale[xSectI];
        if xSectI <> 0 then
          previousXSection := XScale[xSectI - 1];

      end;

//      mControl.thinRange.stop := totThinSamples-1;
    end
    else
      totThinSamples := 0;

    if xActiveChannel = ctThin then
      mControl.thinRange.stop := totThinSamples - 1;

  end;

  LeaveCalculation; // <================ mControl free


end;
//------------------------------------------------------------------------------

constructor TChannelCurve.Create(aQualityMatrixBase: TQualityMatrixBase);
begin

  inherited Create;

  fHitChannel := ctUnknown;
  fOnParameterChange := nil;
  OnReleaseChannel := nil;
  fParameterSource := psUnknown;
  fSelectedChannel := ctUnknown;

  mCalculationActive := False;
  mDragRoot.x := 0;
  mDragRoot.y := 0;

  mQualityMatrixBase := aQualityMatrixBase;

  mQualityMatrixBase.SubFieldX := 0;
  mQualityMatrixBase.SubFieldY := 0;
(*
  SetLength(mControl.nSLSamples, mQualityMatrixBase.TotSamples + 1);
  SetLength(mControl.thinSamples, mQualityMatrixBase.TotSamples + 1);
*)
  mControl.nepRange.start := mQualityMatrixBase.TotSamples + 1;
  mControl.nepRange.stop := mQualityMatrixBase.TotSamples + 1;
  mControl.shortRange.start := mQualityMatrixBase.TotSamples + 1;
  mControl.shortRange.stop := mQualityMatrixBase.TotSamples + 1;
  mControl.longRange.start := mQualityMatrixBase.TotSamples + 1;
  mControl.longRange.stop := mQualityMatrixBase.TotSamples + 1;
  mControl.thinRange.start := mQualityMatrixBase.TotSamples + 1;
  mControl.thinRange.stop := mQualityMatrixBase.TotSamples + 1;

  EnabledChannels := [ctShort, ctLong, ctThin, ctNeps];
  SelectableChannels := EnabledChannels;

//  SetSampleBufferLength;

  SetLength(mControl.nSLSamples, mQualityMatrixBase.TotSamples + 1);
  SetLength(mControl.thinSamples, mQualityMatrixBase.TotSamples + 1);

  mCutCurve := TCutCurveList.Create;

  mCriticalSection := TCriticalSection.Create;
end;
//------------------------------------------------------------------------------

procedure TChannelCurve.CustomMouseDown(aShift: TShiftState; aX,
  aY: Integer);
begin
  if CurveVisible then
  begin
    if ([ssRight] <= aShift) and (HitChannel <> ctUnknown) and
      (SelectedChannel = ctUnknown) then
    begin

      SelectedChannel := HitChannel;
      mDragRoot.x := aX;
      mDragRoot.y := aY;
      mDragRootSettings := ChannelSettings;
    end
  end

end;
//------------------------------------------------------------------------------

procedure TChannelCurve.CustomMouseMove(aShift: TShiftState; aX,
  aY: Integer);
var
  xHitChannel: TChannelTypes;
  xDragTarget: TPoint;

begin
  if CurveVisible then
  begin
    if SelectedChannel = ctUnknown then
    begin

      xHitChannel := HitCurve(aX, aY);
      if (xHitChannel <> ctUnknown) then
      begin

        if (HitChannel = ctUnknown) then
        begin

          HitChannel := xHitChannel;
          case xHitChannel of
            ctNeps:
              mQualityMatrixBase.Hint := mQualityMatrixBase.Hint + ' ' + 'Noppen';
            ctShort:
              mQualityMatrixBase.Hint := mQualityMatrixBase.Hint + ' ' + 'Kurzfehler';
            ctLong:
              mQualityMatrixBase.Hint := mQualityMatrixBase.Hint + ' ' + 'Langfehler';
            ctThin:
              mQualityMatrixBase.Hint := mQualityMatrixBase.Hint + ' ' + 'DŁnnstellen';
          end;
          mQualityMatrixBase.ShowHint := True;
          mQualityMatrixBase.Invalidate;
        end
      end
      else
      begin

        if (HitChannel <> ctUnknown) then
        begin

          if Assigned(fOnReleaseChannel) then
            fOnReleaseChannel(HitChannel);

          HitChannel := xHitChannel;
          mQualityMatrixBase.ShowHint := False;
          mQualityMatrixBase.Invalidate;

        end
      end;
      if (aX > mQualityMatrixBase.ClientWidth - 4) or (aY > mQualityMatrixBase.ClientHeight - 4) then
      begin

        if Assigned(fOnReleaseChannel) then
          fOnReleaseChannel(HitChannel);

        HitChannel := ctUnknown;
        mQualityMatrixBase.ShowHint := False;
        mQualityMatrixBase.Invalidate;

      end
    end
    else
    begin
      xDragTarget.x := aX;
      xDragTarget.y := aY;

      AdjustChannel(xDragTarget);

      mQualityMatrixBase.Invalidate;
    end;
  end;

end;
//------------------------------------------------------------------------------

procedure TChannelCurve.CustomMouseUp(aShift: TShiftState; aX,
  aY: Integer);
begin
//    if ([ssLeft] <= aShift) and
  if CurveVisible then
  begin
    if (SelectedChannel <> ctUnknown) then
    begin

      if Assigned(fOnReleaseChannel) then
        fOnReleaseChannel(SelectedChannel);

      SelectedChannel := ctUnknown;
      mDragRoot.x := 0;
      mDragRoot.y := 0;
      mQualityMatrixBase.Invalidate;
    end;
  end;
end;
//------------------------------------------------------------------------------

destructor TChannelCurve.Destroy;
begin

  Finalize(mControl.nSLSamples);
  Finalize(mControl.thinSamples);

  mCutCurve.Free;

  mCriticalSection.Free;

  inherited;
end;
//------------------------------------------------------------------------------

function TChannelCurve.EnterCalculation: Boolean;
begin

  mCriticalSection.Enter; // <======== Critical section
  try

    Result := not mCalculationActive;

    if not mCalculationActive then
      mCalculationActive := True;

  finally
    mCriticalSection.Leave; // <======== Critical section
  end;
end;
//------------------------------------------------------------------------------

procedure TChannelCurve.GenerateCutCurve(const aSamples: array of TPoint;
  const aTotSamples: Integer; const aReverse: Boolean); // const aRange: TRangeRec);
var
  i, xSubFieldX, xSubFieldY: Integer;
  xFieldCurve: TClassFieldCurveRec;
  xFieldRect: TRect;
  xCoordinateMode: TCoordinateMode;
  xStrings: TStrings;
  //----------------------------------------------------------------------------

  procedure CalculateSubField(const aX, aY: Integer; const aRect: TRect);
  begin

    with aRect, mCutCurve do
      if (mQualityMatrixBase.SubFieldX > 0) and
        (mQualityMatrixBase.SubFieldY > 0) then
      begin

        if mQualityMatrixBase.SubFieldX > (aRect.Right - aRect.Left) then
          fSubFieldX := (aRect.Right - aRect.Left);

        if (aRect.Right - aRect.Left) > 0 then
          xSubFieldX := Round((aX - aRect.Left) * fSubFieldX /
            (aRect.Right - aRect.Left)) + 1
        else
          xSubFieldX := 0;

        if mQualityMatrixBase.SubFieldY > (aRect.Bottom - aRect.Top) then
          fSubFieldY := (aRect.Bottom - aRect.Top);

        if (aRect.Bottom - aRect.Top) > 0 then
          xSubFieldY := Round((aRect.Bottom - aY) * fSubFieldY /
            (aRect.Bottom - aRect.Top)) + 1
        else
          xSubFieldY := 0;
      end
      else
      begin

        xSubFieldX := 0;
        xSubFieldY := 0;
      end;
  end;
  //----------------------------------------------------------------------------

  procedure InsertSubField;
  begin

    with mQualityMatrixBase do
    begin
      if (xSubFieldX > 0) and (xSubFieldY > 0) then
      begin
        if xSubFieldX > SubFieldX then xSubFieldX := SubFieldX;
        if xSubFieldY > SubFieldY then xSubFieldY := SubFieldY;
(*
        xStrings.Delete(xSubFieldX - 1);
        if aReverse then
          xStrings.Insert(xSubFieldX - 1, IntToStr(SubFieldY - xSubFieldY + 1))
        else
          xStrings.Insert(xSubFieldX - 1, IntToStr(xSubFieldY));
*)
        if aReverse then
          xStrings.Strings[xSubFieldX - 1] := IntToStr(SubFieldY - xSubFieldY + 1)
        else
          xStrings.Strings[xSubFieldX - 1] := IntToStr(xSubFieldY);
      end

    end;

  end;
  //----------------------------------------------------------------------------

  procedure NewField;
  var
    j: Integer;
  begin

    with mQualityMatrixBase do
    begin
      xStrings.Clear;
      for j := 0 to SubFieldX - 1 do
        xStrings.Add('-1');

      xFieldCurve.Field := GetFieldID(aSamples[i].x, aSamples[i].y);
      xFieldRect := Field[xFieldCurve.Field];

      CalculateSubField(aSamples[i].x, aSamples[i].y, xFieldRect);
      InsertSubField;
    end;
  end;

  //----------------------------------------------------------------------------

  function IsInRect(const aX, aY: Integer; const aRect: TRect): Boolean;
  begin
    with aRect do

      if (aX >= Left) and (aX < Right) and (aY >= Top) and (aY < Bottom) then
      begin
        Result := True;

      end
      else
        Result := False;
  end;
  //----------------------------------------------------------------------------

  procedure PadOutSubFieldString;
  var
    j: Integer;
  begin

    with mQualityMatrixBase do
    begin
      j := SubFieldX - 1;

      while (j >= 0) and (xStrings.Strings[j] = '-1') do
      begin
(*
        xStrings.Delete(j);
        xStrings.Insert(j, '0');
*)
        xStrings.Strings[j] := '0';
        Dec(j);
      end
    end;

  end;
  //----------------------------------------------------------------------------

begin

  with mQualityMatrixBase do
  begin

    xStrings := TStringList.Create;

    xFieldCurve.Field := TotFields; // %% !! TotFields might be a problem in Rough mode

    xCoordinateMode := CoordinateMode;
    CoordinateMode := cmActual;

    mCutCurve.fSubFieldX := mQualityMatrixBase.SubFieldX;
    mCutCurve.fSubFieldY := mQualityMatrixBase.SubFieldY;


    if aReverse then
    begin
      i := aTotSamples;

      while aSamples[i].y = cInvisiblePixcel do
        Dec(i);

      while (i >= 0) do
      begin
        if not ((aSamples[i].x = 0) and (aSamples[i].y = 0)) then
        begin
          if xFieldCurve.Field < TotFields then
          begin

            if IsInRect(aSamples[i].x, aSamples[i].y, xFieldRect) then
            begin

              CalculateSubField(aSamples[i].x, aSamples[i].y, xFieldRect);
              InsertSubField;
            end
            else
            begin
              PadOutSubFieldString;

              XFieldCurve.SubFields := xStrings.CommaText;
              mCutCurve.Add(xFieldCurve);

              NewField;

            end;

          end
          else
          begin
            NewField;

          end;

//          Canvas.LineTo(aSamples[i].x, aSamples[i].y);
        end;

        Dec(i, CutCurveSampleDivisor);
      end;
    end
    else
    begin
      i := 0;

      while aSamples[i + 1].y = 0 do
        Inc(i);
      while aSamples[i].y = cInvisiblePixcel do
        Inc(i);

      while (i < aTotSamples) do
      begin
        if not ((aSamples[i].x = 0) and (aSamples[i].y = 0)) then
        begin
          if xFieldCurve.Field < TotFields then
          begin

            if IsInRect(aSamples[i].x, aSamples[i].y, xFieldRect) then
            begin
              CalculateSubField(aSamples[i].x, aSamples[i].y, xFieldRect);
              InsertSubField;
            end
            else
            begin
              PadOutSubFieldString;

              XFieldCurve.SubFields := xStrings.CommaText;
              mCutCurve.Add(xFieldCurve);

              NewField;

            end;

          end
          else
          begin
            NewField;

          end;

//          Canvas.LineTo(aSamples[i].x, aSamples[i].y);
        end;

        Inc(i, CutCurveSampleDivisor);
      end;
    end;

    CoordinateMode := xCoordinateMode;

    if TotSamples > 0 then
    begin
      PadOutSubFieldString;

      XFieldCurve.SubFields := xStrings.CommaText;
      mCutCurve.Add(xFieldCurve);
    end;

    xStrings.Free;
  end;
end;

//------------------------------------------------------------------------------

function TChannelCurve.GetChannelPara: TChannelSettingsRec;
begin
  Result := inherited GetChannelPara;

end;
//------------------------------------------------------------------------------

function TChannelCurve.GetCutCurve(
  const aCurveType: TCurveTypes): TCutCurveList;
begin

  mCutCurve.Clear;

  if aCurveType = cvtNSL then
  begin
    with mControl, mQualityMatrixBase do

      if totNSLSamples > 0 then
      begin

        mCutCurve.Capacity := (totNSLSamples div CutCurveSampleDivisor) + 1;
        GenerateCutCurve(nSLSamples, totNSLSamples, False);

      end
  end

  else if aCurveType = cvtThin then
  begin
    with mControl, mQualityMatrixBase do

      if totThinSamples > 0 then
      begin

        mCutCurve.Capacity := (totThinSamples div CutCurveSampleDivisor) + 1;
        GenerateCutCurve(thinSamples, totThinSamples, True);

      end;
  end;

  Result := mCutCurve;
end;
//------------------------------------------------------------------------------

function TChannelCurve.HitCurve(aX, aY: Integer): TChannelTypes;
begin
  Result := ctUnknown;

  with mControl do
  begin
    if (ctLong in SelectableChannels) and CheckHit(aX, aY, longRange, nSLSamples) then
      Result := ctLong
(*
    if (ctNeps in SelectableChannels) and CheckHit(aX, aY, nepRange, nSLSamples) then
      Result := ctNeps
    else if (ctShort in SelectableChannels) and CheckHit(aX, aY, shortRange, nSLSamples) then
      Result := ctShort
    else if (ctLong in SelectableChannels) and CheckHit(aX, aY, longRange, nSLSamples) then
      Result := ctLong
    else if (ctThin in SelectableChannels) and CheckHit(aX, aY, thinRange, thinSamples) then
      Result := ctThin;
*)
  end;
end;
//------------------------------------------------------------------------------

(*procedure TChannelCurve.PaintCurve(aSamples: array of TPoint; aTotSamples: Integer);
var
  i: Integer;
begin
  with mControl, mQualityMatrixBase do begin

    Canvas.Brush.Style := bsClear;
    Canvas.Pen.Color := CurveColor;
    Canvas.Pen.mode := pmCopy;
    if CurveStyle <> psSolid then
      Canvas.Pen.Width:= 1
    else
      Canvas.Pen.Width:= CurveWidth;
    Canvas.Pen.Style := CurveStyle;

    i := 0;
    while aSamples[i+1].y = 0 do Inc(i);
    while (aSamples[i].y = cInvisiblePixcel) or (aSamples[i].y = cInvisiblePixcel) do Inc(i);

    Canvas.MoveTo(aSamples[i].x, aSamples[i].y);

    for i := i to aTotSamples - 1 do begin
      if not ((aSamples[i].x = 0) and (aSamples[i].y = 0)) then
        Canvas.LineTo(aSamples[i].x, aSamples[i].y);
    end;

  end;
end; *)
//------------------------------------------------------------------------------

procedure TChannelCurve.PaintCurve(aSamples: array of TPoint;
  aTotSamples: Integer; aRange: TRangeRec);
var
  i: Integer;
begin

  with aRange, mQualityMatrixBase do
  begin

    if (start < aTotSamples) and (stop < aTotSamples) then
    begin
      Canvas.MoveTo(aSamples[start].x, aSamples[start].y);

      for i := start to stop do
      begin
        if not ((aSamples[i].x = 0) and (aSamples[i].y = 0)) and
          not (aSamples[i].y = cInvisiblePixcel) then
          Canvas.LineTo(aSamples[i].x, aSamples[i].y);
      end;
    end;
  end;

end;

//------------------------------------------------------------------------------

procedure TChannelCurve.LeaveCalculation;
begin

  mCalculationActive := False;
end;
//------------------------------------------------------------------------------

procedure TChannelCurve.PaintCurve;
var
  xStart: Integer;

  procedure SetupCanvas;
  begin

    with mQualityMatrixBase do
    begin

      Canvas.Brush.Style := bsClear;
      Canvas.Pen.Mode := pmCopy;
      Canvas.Pen.Color := CurveColor;
      Canvas.Pen.Style := CurveStyle;

      if CurveStyle <> psSolid then
        Canvas.Pen.Width := 1
      else
        Canvas.Pen.Width := CurveWidth;
    end;
  end;

  procedure SetHitCurveCanvasStyle;
  begin
    with mQualityMatrixBase do
    begin

      Canvas.Pen.Mode := pmMergePenNot;
      Canvas.Pen.Style := psDot;
      Canvas.Pen.Width := 1;
    end
  end;

  procedure SetRegularCurveCanvasStyle;
  begin
    with mQualityMatrixBase do
    begin

      Canvas.Pen.Mode := pmCopy;
      Canvas.Pen.Style := CurveStyle;
      Canvas.Pen.Width := CurveWidth;
    end

  end;

begin

  if CurveVisible then
    with mControl, mQualityMatrixBase do
    begin

      SetupCanvas;

      xStart := 0;
      while (xStart < High(nSLSamples)) and (nSLSamples[xStart + 1].y = 0) do
        Inc(xStart);
      while (xStart <= High(nSLSamples)) and
        (nSLSamples[xStart].y = cInvisiblePixcel) do
        Inc(xStart);

      if nepRange.start < TotSamples then
      begin
        if (xStart + 1) = nepRange.start then
          nepRange.start := xStart;
      end
      else if shortRange.start < TotSamples then
      begin
        if (xStart + 1) = shortRange.start then
          shortRange.start := xStart;
      end
      else if longRange.start < TotSamples then
      begin
        if (xStart + 1) = longRange.start then
          longRange.start := xStart;
      end;

      if ctNeps in EnabledChannels then
      begin

        if HitChannel = ctNeps then
        begin
          SetHitCurveCanvasStyle;
          PaintCurve(nSLSamples, totNSLSamples, nepRange);
          SetRegularCurveCanvasStyle;
        end
        else
          PaintCurve(nSLSamples, totNSLSamples, nepRange);

      end;

      if ctShort in EnabledChannels then
      begin

        if HitChannel = ctShort then
        begin
          SetHitCurveCanvasStyle;
          PaintCurve(nSLSamples, totNSLSamples, shortRange);
          SetRegularCurveCanvasStyle;
        end
        else
          PaintCurve(nSLSamples, totNSLSamples, shortRange);
      end;

      if ctLong in EnabledChannels then
      begin

        if HitChannel = ctLong then
        begin
          SetHitCurveCanvasStyle;
          PaintCurve(nSLSamples, totNSLSamples, longRange);
          SetRegularCurveCanvasStyle;
        end
        else
          PaintCurve(nSLSamples, totNSLSamples, longRange);
      end;

      if ctThin in EnabledChannels then
      begin

        xStart := 0;
        while (xStart < High(mControl.thinSamples)) and
          (mControl.thinSamples[xStart + 1].y = 0)
          do
          Inc(xStart);
        while (xStart <= High(mControl.thinSamples)) and
          (mControl.thinSamples[xStart].y = cInvisiblePixcel)
          do
          Inc(xStart);

        if thinRange.start < TotSamples then
        begin
          if (xStart + 1) = thinRange.start then
            thinRange.start := xStart;
        end;

        if HitChannel = ctThin then
        begin
          SetHitCurveCanvasStyle;
          PaintCurve(thinSamples, totThinSamples, thinRange);
          SetRegularCurveCanvasStyle;
        end
        else
          PaintCurve(thinSamples, totThinSamples, thinRange);
      end;

    end;

end;
//------------------------------------------------------------------------------

procedure TChannelCurve.PutChannelPara(const aSettings: TChannelSettingsRec);
begin

  if ChannelParaChanged(aSettings) then
    SetChannelPara(aSettings);
end;
//------------------------------------------------------------------------------

procedure TChannelCurve.PutHitChannel(const aValue: TChannelTypes);
begin
  if aValue <> fHitChannel then
  begin
    fHitChannel := aValue;
  end;

end;
//------------------------------------------------------------------------------

procedure TChannelCurve.PutSelectableChannels(const aValue: TChannels);
begin
  fSelectableChannels := EnabledChannels * aValue;
end;
//------------------------------------------------------------------------------

procedure TChannelCurve.PutSelectedChannel(const aValue: TChannelTypes);
begin
  if aValue <> fSelectedChannel then
  begin
    fSelectedChannel := aValue;
    if aValue = ctUnknown then
    begin
      HitChannel := ctUnknown;
    end
    else
    begin

    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TChannelCurve.PutSplicePara(aSettings: TSpliceSettingsRec);
var
  xSettings: TChannelSettingsRec;
begin
  with xSettings do
  begin
    NepChannel := aSettings.NepChannel;
    ShortChannel := aSettings.ShortChannel;
    LongChannel := aSettings.LongChannel;
    ThinChannel := aSettings.ThinChannel;

    NepDiameter := aSettings.NepDiameter;
    ShortDiameter := aSettings.ShortDiameter;
    ShortLength := aSettings.ShortLength;
    LongDiameter := aSettings.LongDiameter;
    LongLength := aSettings.LongLength;
    ThinDiameter := aSettings.ThinDiameter;
    ThinLength := aSettings.ThinLength;
  end;
  if ChannelParaChanged(xSettings) then
    SetChannelPara(xSettings);
end;

//------------------------------------------------------------------------------

procedure TChannelCurve.SetChannelPara(const aSettings: TChannelSettingsRec);
begin
  inherited PutChannelPara(aSettings);
  ComputeNSLCurve;
  ComputeThinCurve;
end;
//------------------------------------------------------------------------------

procedure TChannelCurve.SetSampleBufferLength;
begin

  SetLength(mControl.nSLSamples, mQualityMatrixBase.TotSamples + 1);
  SetLength(mControl.thinSamples, mQualityMatrixBase.TotSamples + 1);

  ComputeNSLCurve;
  ComputeThinCurve;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// TCutCurveList
//******************************************************************************
//------------------------------------------------------------------------------

function TCutCurveList.Add(
  aClassFieldCurve: TClassFieldCurveRec): Integer;
var
  xPClassFieldCurve: PTClassFieldCurveRec;
begin
  New(xPClassFieldCurve);

  xPClassFieldCurve^ := aClassFieldCurve;

  Result := inherited Add(xPClassFieldCurve);
end;
//------------------------------------------------------------------------------

procedure TCutCurveList.Clear;
var
  i: Integer;
begin

  for i := 0 to Count - 1 do
    if Assigned(inherited Items[i]) then
      Dispose(inherited Items[i]);
  inherited Clear;
end;
//------------------------------------------------------------------------------
(*
constructor TCutCurveList.Create;
begin

  Capacity := cZESpdGroupLimit;
end;
*)
//------------------------------------------------------------------------------

destructor TCutCurveList.Destroy;
begin

  Clear;
  inherited Destroy;
end;
//------------------------------------------------------------------------------

function TCutCurveList.GetItems(aIndex: Integer): TClassFieldCurveRec;
var
  xPClassFieldCurve: PTClassFieldCurveRec;
begin

  xPClassFieldCurve := inherited Items[aIndex];

  Result := xPClassFieldCurve^;
end;
//------------------------------------------------------------------------------

procedure TCutCurveList.SetItems(aIndex: Integer;
  aValue: TClassFieldCurveRec);
var
  xPClassFieldCurve: PTClassFieldCurveRec;
begin

  if Assigned(inherited Items[aIndex]) then
  begin

    xPClassFieldCurve := inherited Items[aIndex];
    xPClassFieldCurve^ := aValue;
  end
  else
  begin

    New(xPClassFieldCurve);
    xPClassFieldCurve^ := aValue;
    inherited Items[aIndex] := xPClassFieldCurve;
  end;
end;

//------------------------------------------------------------------------------
end.

