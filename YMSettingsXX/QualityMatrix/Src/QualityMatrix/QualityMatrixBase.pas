(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QualityMatrixBase.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Descriptio...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr | Initial Release
|=========================================================================================*)
unit QualityMatrixBase;

interface

uses
  Windows,  Classes, Dialogs, Graphics, ExtCtrls, SysUtils, QualityMatrixDef,
  QualityMatrixDesc, Controls;

const
  _Version_ = '$Revision: 4 $';
  _Date_ = '$Modtime: 3/25/04 11:38a $';
  // $NoKeywords: $

const
  cHorizontal = 0;
  cVertical = 1;
  cField = 2;
  cXAxis = 3;
  cYAxis = 4;
  cCaption = 5;
  cSamples = 6;
  cGraphField = 7;
  cNSLGraphField = 8;
  cThinGraphField = 9;
  cSuperClass = 10;

  cCuts = 0;
  cDefects = 1;

  cCutActive = 0;
  cCutPassive = 1;
  cFFClusterActive = 2;
  cFFClusterPassive = 3;
  cSCMemberActive = 4;
  cSCMemberPassive = 5;

type
  //...........................................................................

  TPixelAid = class(TObject)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
//		constructor Create(); override;
//    destructor Destroy; override;
    function AlignItem(aAlignPos, aItemLength: Integer;
      aAlign: TItemAlignement): TRangeRec;
    function PlaceItem(aBounds: TRangeRec; aItemLength: Integer;
      aAlign: TItemAlignement): TRangeRec;
  published
    { Published declarations }
  end;
  //...........................................................................

//  TQualityMatrixBase = class(TCustomPanel )
  TQualityMatrixBase = class(TCustomControl)
  private
    { Private declarations }
    fCoordinateMode: TCoordinateMode;
    fCutCurveSampleDivisor: Integer;
    fLastCutMode: TLastCutMode;
    fLastCutColor: TColor;
    fLastCutField: Integer;
    fNeighbour: TItemAlignement;
    fNumberOfSamples: Integer;

    fSubFieldX: Integer;
    fSubFieldY: Integer;

    fSubType: TMatrixSubType;
    fTitle: WideString;
    fTitleEnabled: Boolean;
    fType: TMatrixType;

    mMatrix: TQualityMatrixDescRec;

    mTotCaptions: Integer;
    mTotFields: Integer;
    mTotHorLines: Integer;
    mTotSuperClasses: Integer;
    mTotVertLines: Integer;
    mTotXScales: Integer;
    mTotYScales: Integer;
    fFontSize: Integer;
    fFontName: TFontName;
    fFontCharSet: TFontCharSet;

    function AdjustBoundary(aToAdjustRect, aBoundaryRect: TRect;
      aCalcBase: TCalculationBase): TRect;
    function CalculatePixelToRaster(aPixelPos: TPoint): TPoint;
    function CalculateRasterToPixel(aLeft, aTop,
      aRight, aBottom: Word): TRect;
    function FindeLineIDs(aFieldID: Integer): TRect;

    function GetBackgroundColor(aID, aIndex: Integer): TBrushRec;
    function GetCoordinate(aID, aIndex: Integer): TRect;
    function GetGraphField(aIndex: Integer): TRect;
    function GetLastCutMode: TLastCutMode;
    function GetLastCutColor: TColor;
    function GetLastCutField: Integer;
    function GetLinePen(aID, aIndex: Integer): TPenRec;
    function GetNumberOfSamples(aID: Integer): Integer;
    function GetSCField(aID: Integer): TRect;
    function GetSCMembers(aID: Integer): TSCMemberArray;
    function GetScale(aID, aIndex: Integer): TScaleRec;
    function GetStateColor(aID, aIndex: Integer): TBrushRec;
    function GetText(aID, aIndex: Integer): TTextRec;
    function GetTextFormat(aID, aIndex: Integer): TFormatTextRec;
    function GetTotItems(aIndex: Integer): Integer;
    function GetVisibleNSLRange: TRect;
    function GetVisibleTRange: TRect;

    procedure CalculateNumberOfSamples;
    procedure SetLastCutMode(const aValue: TLastCutMode);
    procedure SetLastCutColor(const aValue: TColor);
    procedure SetLastCutField(const aValue: Integer);
    procedure SetType(aType: TMatrixType);
    procedure SetSubField(const aIndex, aValue: Integer);
    procedure SetSubType(const aValue: TMatrixSubType);

    property NumberOfSamples[aID: Integer]: Integer read GetNumberOfSamples;
    function GetTitlePos: TTextRec;
    procedure SetTitleEnabled(const aValue: Boolean);
    procedure SetTitle(const aValue: WideString);

  protected
    { Protected declarations }

  public
    { Public declarations }

    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function GetFieldID(aX, aY: Integer): Integer;
    function GetNeighbourFieldID(var aX, aY: Integer): Integer;
    function GetFieldWeight(aId: Integer): Real;
    procedure FillField(aFieldRect: TRect; aSelected: TBrushRec);

    property BackgroundColor[aID: Integer]: TBrushRec index cField read GetBackgroundColor;
    property Caption[aID: Integer]: TTextRec index cCaption read GetText;
    property CoordinateMode: TCoordinateMode read fCoordinateMode write fCoordinateMode;
    property CutColor[aID: Integer]: TBrushRec index cCutActive read GetStateColor;
    property CutCurveSampleDivisor: Integer read fCutCurveSampleDivisor;
    property CutsFormat[aID: Integer]: TFormatTextRec index cCuts read GetTextFormat;
    property DefectsFormat[aID: Integer]: TFormatTextRec index cDefects read GetTextFormat;
    property FFClusterColor[aID: Integer]: TBrushRec index cFFClusterActive read GetStateColor;
    property Field[aID: Integer]: TRect index cField read GetCoordinate;
    property GraphField: TRect index cGraphField read GetGraphField;
    property HorLine[aID: Integer]: TRect index cHorizontal read GetCoordinate;
    property HorLinePen[aID: Integer]: TPenRec index cHorizontal read GetLinePen;
    property NeighbourField: TItemAlignement read fNeighbour write fNeighbour;
    property NoCutColor[aID: Integer]: TBrushRec index cCutPassive read GetStateColor;
    property NoFFClusterColor[aID: Integer]: TBrushRec index cFFClusterPassive read GetStateColor;
    property NoSCMemberColor[aID: Integer]: TBrushRec index cSCMemberPassive read GetStateColor;
    property NSLGraphField: TRect index cNSLGraphField read GetGraphField;
    property SCField[aID: Integer]: TRect read GetSCField;
    property SCMemberColor[aID: Integer]: TBrushRec index cSCMemberActive read GetStateColor;
    property SCMembers[aID: Integer]: TSCMemberArray read GetSCMembers;
    property SubFieldX: Integer index cXAxis read fSubFieldX write SetSubField;
      // Number of horizontal subfields of a matrix field: 0..
    property SubFieldY: Integer index cYAxis read fSubFieldY write SetSubField;
      // Number of vertical subfields of a matrix field: 0..
    property ThinGraphField: TRect index cThinGraphField read GetGraphField;
    property TitlePos: TTextRec read GetTitlePos;
    property TotCaptions: Integer index cCaption read GetTotItems;
    property TotFields: Integer index cField read GetTotItems;
    property TotHorLines: Integer index cHorizontal read GetTotItems;
    property TotSamples: Integer index cSamples read GetTotItems;
    property TotSuperClasses: Integer index cSuperClass read GetTotItems;
    property TotVertLines: Integer index cVertical read GetTotItems;
    property TotXScales: Integer index cXAxis read GetTotItems;
    property TotYScales: Integer index cYAxis read GetTotItems;
    property VertLine[aID: Integer]: TRect index cVertical read GetCoordinate;
    property VertLinePen[aID: Integer]: TPenRec index cVertical read GetLinePen;
    property VisibleNSLRange: TRect read GetVisibleNSLRange;
    property VisibleTRange: TRect read GetVisibleTRange;
    property XScale[aID: Integer]: TScaleRec index cXAxis read GetScale;
    property XScaleText[aID: Integer]: TTextRec index cXAxis read GetText;
    property YScale[aID: Integer]: TScaleRec index cYAxis read GetScale;
    property YScaleText[aID: Integer]: TTextRec index cYAxis read GetText;

  published
    { Published declarations }
    property Align;
//    property BevelInner;
//    property BevelOuter;
//    property BevelWidth;
    property Color;
    property Canvas;
    property Hint;

    property LastCutMode: TLastCutMode read GetLastCutMode write SetLastCutMode;
    property LastCutColor: TColor read GetLastCutColor write SetLastCutColor;
    property LastCutField: Integer read GetLastCutField write SetLastCutField;

      // The color of active settings and super class fields is set
      //  if ActiveColor := clNone the default color is enabled

    property FontCharSet: TFontCharSet read fFontCharSet write fFontCharSet;
    property FontName: TFontName read fFontName write fFontName;
    property FontSize: Integer read fFontSize write fFontSize;
    property MatrixType: TMatrixType read fType write SetType;
    property MatrixSubType: TMatrixSubType read fSubType write SetSubType;
    property ShowHint;
    property TitleEnabled: Boolean read fTitleEnabled write SetTitleEnabled;
    property Title: WideString read fTitle write SetTitle;
  end;

// procedure Register;

implementation
//------------------------------------------------------------------------------
(* procedure Register;
begin
  RegisterComponents('YMClearer', [TQualityMatrixBase]);
end;
*)
//------------------------------------------------------------------------------

//******************************************************************************
// TQualityMatrixBase
//******************************************************************************
//------------------------------------------------------------------------------

function TQualityMatrixBase.AdjustBoundary(aToAdjustRect, aBoundaryRect: TRect;
  aCalcBase: TCalculationBase): TRect;
begin
  Result := aToAdjustRect;
  case aCalcBase of
    cbAbsolute:
      begin
        if aToAdjustRect.Left < aBoundaryRect.Left then
        begin
// Adjust Left Boundary
          Result.Left := aBoundaryRect.Left;
          if aToAdjustRect.Left = aToAdjustRect.Right then
// Vertical Line
            Result.Right := aBoundaryRect.Left;
        end;
        if aToAdjustRect.Top < aBoundaryRect.Top then
        begin
// Adjust Top Boundary
          Result.Top := aBoundaryRect.Top;
          if aToAdjustRect.Top = aToAdjustRect.Bottom then
// Horizontal Line
            Result.Bottom := aBoundaryRect.Top;
        end;
        if aToAdjustRect.Right > aBoundaryRect.Right then
        begin
// Adjust Right Boundary
          Result.Right := aBoundaryRect.Right;
          if aToAdjustRect.Right = aToAdjustRect.Left then
// Vertical Line
            Result.Left := aBoundaryRect.Right;
        end;
        if aToAdjustRect.Bottom > aBoundaryRect.Bottom then
        begin
// Adjust Bottom Boundary
          Result.Bottom := aBoundaryRect.Bottom;
          if aToAdjustRect.Bottom = aToAdjustRect.Top then
// Horizontal Line
            Result.Top := aBoundaryRect.Bottom;
        end;
      end;
    cbRelative:
      begin
        Result.Left := aToAdjustRect.Left + aBoundaryRect.Left;
        Result.Top := aToAdjustRect.Top + aBoundaryRect.Top;
        Result.Right := aToAdjustRect.Right - aBoundaryRect.Right;
        Result.Bottom := aToAdjustRect.Bottom - aBoundaryRect.Bottom;
      end;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.CalculateNumberOfSamples;
begin
  if Assigned(mMatrix.pCurveLayer) then
  begin
    with mMatrix.pCurveLayer^ do
    begin

      if fSubFieldX <> 0 then

        if SubFieldX < numberOfSamplePerScale then
        begin
          fCutCurveSampleDivisor := (numberOfSamplePerScale div SubFieldX) + 1;
          fNumberOfSamples := fCutCurveSampleDivisor * fSubFieldX;
        end
        else
        begin
          fCutCurveSampleDivisor := 1;
          fNumberOfSamples := fSubFieldX
        end

      else
      begin
        fCutCurveSampleDivisor := 0;
        fNumberOfSamples := numberOfSamplePerScale;
      end;
    end;
  end
  else
  begin
    fCutCurveSampleDivisor := 0;
    fNumberOfSamples := 0;
  end;

end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.CalculatePixelToRaster(
  aPixelPos: TPoint): TPoint;
var
  xRaster: Double;
  xTitleHight: Integer;
begin

  if TitleEnabled then
    xTitleHight := mMatrix.pScaleLayer^.title.barHight
  else
    xTitleHight := 0;

  with Result do
  begin
//    if (ClientWidth<>0) and (ClientHeight<>0) then begin
    if (ClientWidth <> 0) and (ClientHeight <> 0) then
    begin
//      x := Round(aPixelPos.x * (mMatrix.pRaster^.width -1) / ClientWidth);
//      y := Round(aPixelPos.y * (mMatrix.pRaster^.height -1) / ClientHeight);

      xRaster := aPixelPos.x * (mMatrix.pRaster^.width) / ClientWidth;
      x := aPixelPos.x * mMatrix.pRaster^.width div ClientWidth;
      if (xRaster - x) >= 0.95 then
        x := x + 1;
(* Offset does not work because of TChannelCurve.GenerateCutCurve.IsInRect
  needs some more brain work!!

      xOffset := 0.01 * ClientHeight;
      if aPixelPos.y > xOffset then
        aPixelPos.y := aPixelPos.y - Round(xOffset);
*)

      xRaster := aPixelPos.y * (mMatrix.pRaster^.height + xTitleHight) / ClientHeight;
      y := (aPixelPos.y * (mMatrix.pRaster^.height + xTitleHight) div ClientHeight);
      if (xRaster - y) >= 0.95 then
        y := y + 1;
    end
    else
    begin
      x := 0;
      y := 0;
    end;
  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.CalculateRasterToPixel(aLeft, aTop, aRight,
  aBottom: Word): TRect;
var
  xTitleHight: Integer;
begin

  if TitleEnabled then
    xTitleHight := mMatrix.pScaleLayer^.title.barHight
  else
    xTitleHight := 0;

  with Result do
  begin
    if (mMatrix.pRaster^.width <> 0) and (mMatrix.pRaster^.height <> 0) then
    begin
      Left := Round(aLeft * ClientWidth
        / mMatrix.pRaster^.width);
      Top := Round(aTop * ClientHeight
        / (mMatrix.pRaster^.height + xTitleHight));
      Right := Round(aRight * ClientWidth
        / mMatrix.pRaster^.width);
      Bottom := Round(aBottom * ClientHeight
        / (mMatrix.pRaster^.height + xTitleHight));

    end
    else
    begin
      Left := 0;
      Top := 0;
      Right := 0;
      Bottom := 0;
    end;
  end;
end;
//------------------------------------------------------------------------------

constructor TQualityMatrixBase.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fFontCharSet := DEFAULT_CHARSET;
  fFontSize := cDefaultFontSize;
  fFontName := cDefaultFontName;
  fTitleEnabled := False;
  fTitle := '';

  CoordinateMode := cmActual;
  NeighbourField := iaTop;

  // Set initial Properties
  MatrixType := mtNone;
  LastCutMode := lcNone;
  LastCutColor := clLime;
  LastCutField := 0;

  SubFieldX := 0;
  SubFieldY := 0;
end;
//------------------------------------------------------------------------------

destructor TQualityMatrixBase.Destroy;
begin
//  Finalize(fCutState);
//  Finalize(fCuts);
//  Finalize(fDefects);

  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.FillField(aFieldRect: TRect;
  aSelected: TBrushRec);
begin
(*
  if aSelected.style <> bsSolid then begin
//    Canvas.Brush.Style := bsSolid;
  Color := clSilver;
    Canvas.Brush.Bitmap := nil;
    Canvas.CopyMode := cmNotSrcErase;
    Canvas.Pen.Mode := pmCopy;
    Canvas.Pen.Color := aSelected.color;
    Canvas.Brush.Color := aSelected.color;//clSilver;
//    Canvas.Brush.Style := bsClear;
    Canvas.Brush.Style := aSelected.style;
   Canvas.FillRect(aFieldRect);
  end
  else begin
*)

  Canvas.Brush.Color := aSelected.color;
  Canvas.Brush.Style := aSelected.style;

  Canvas.FillRect(aFieldRect);
//  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.FindeLineIDs(aFieldID: Integer): TRect;
var
  i: Integer; fieldRaster: TRect;
begin
  Result.Left := TotVertLines;
  Result.Right := TotVertLines;
  Result.Top := TotHorLines;
  Result.Bottom := TotHorLines;

  if aFieldID < TotFields then
  begin
    with mMatrix.pField^.table[aFieldID] do
    begin
      fieldRaster.Left := left;
      fieldRaster.Top := top;
      if width = 0 then
        fieldRaster.Right := left + mMatrix.pField^.fieldWidth
      else
        fieldRaster.Right := left + width;
      if height = 0 then
        fieldRaster.bottom := top + mMatrix.pField^.fieldHeight
      else
        fieldRaster.bottom := top + height;
    end;
    fieldRaster.Left := mMatrix.pField^.table[aFieldID].left;
    for i := 0 to TotVertLines - 1 do
    begin
      with mMatrix.pScaleLayer^.vertLine.table[i], fieldRaster do
      begin
        if (pos = Left) and (Top >= start) and (Bottom <= stop) then Result.Left := i;
        if (pos = Right) and (Top >= start) and (Bottom <= stop) then Result.Right := i;
      end;
    end;

    for i := 0 to TotHorLines - 1 do
    begin
      with mMatrix.pScaleLayer^.horLine.table[i], fieldRaster do
      begin
        if (pos = Top) and (Left >= start) and (Right <= stop) then Result.Top := i;
        if (pos = Bottom) and (Left >= start) and (Right <= stop) then Result.Bottom := i;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetBackgroundColor(aID, aIndex: Integer): TBrushRec;
begin
  case aIndex of
    cField:
      begin
        if aID < TotFields then
          with mMatrix.pField^.table[aID] do
          begin
            if pBackground = nil then
              Result := mMatrix.pField^.background
            else
              Result := pBackground^;
          end;
      end;
  else
    Result := cBackgroundUnassigned;
  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetCoordinate(aID, aIndex: Integer): TRect;
var
  xLine, xBoundary: TRect; xPen: TPenRec;
  xWidth, xHeight, xRemainder: Integer;
  xCoordinateMode: TCoordinateMode;
  xTitleHight: Integer;
begin
//���
//���
//��� Randbedingungeng checken div by zero, Breite, Hoehe overflow usw.
//���
// Default init
  Result.Left := 0;
  Result.Top := 0;
  Result.Right := ClientWidth;
  Result.Bottom := ClientHeight;

  xPen := GetLinePen(aID, aIndex);
  xRemainder := xPen.width mod 2;

  if TitleEnabled then
    xTitleHight := mMatrix.pScaleLayer^.title.barHight
  else
    xTitleHight := 0;

  case aIndex of
    cHorizontal:
      begin
        if aID < TotHorLines then
          with mMatrix.pScaleLayer^.horLine.table[aID] do
            Result := CalculateRasterToPixel(start, xTitleHight + pos,
              stop, xTitleHight + pos);
        with xBoundary do
        begin
          Left := 0;
          Top := xPen.width div 2;
          Right := ClientWidth;
          Bottom := ClientHeight - Top - xRemainder;
        end;
        Result := AdjustBoundary(Result, xBoundary, cbAbsolute);

        with Result do
        begin
          case CoordinateMode of
            cmTop:
              begin
                Top := Top - xBoundary.Top;
                Bottom := Top;
              end;

            cmBottom:
              begin
                Top := Top + xBoundary.Top + xRemainder;
                Bottom := Top;
              end;
          end;
        end;
      end;

    cVertical:
      begin
        if aID < TotVertLines then
          with mMatrix.pScaleLayer^.vertLine.table[aID] do
            Result := CalculateRasterToPixel(pos, xTitleHight + start,
              pos, xTitleHight + stop);

        with xBoundary do
        begin
          Left := xPen.width div 2;
          Top := 0;
          Right := ClientWidth - Left - xRemainder;
          Bottom := ClientHeight;
        end;
        Result := AdjustBoundary(Result, xBoundary, cbAbsolute);

        with Result do
        begin
          case CoordinateMode of
            cmLeft:
              begin
                Left := Left - xBoundary.Left;
                Right := Left;
              end;

            cmRight:
              begin
                Left := Left + xBoundary.Left + xRemainder;
                Right := Left;
              end;
          end;
        end;
      end;

    cField:
      begin
        if aID < TotFields then
        begin
//���
//���
//��� Randbedingungeng checken div by zero, Breite, Hoehe overflow usw.
//���
          if mMatrix.pField^.table[aID].width = 0 then
            xWidth := mMatrix.pField^.fieldWidth
          else
            xWidth := mMatrix.pField^.table[aID].width;
          if mMatrix.pField^.table[aID].height = 0 then
            xHeight := mMatrix.pField^.fieldHeight
          else
            xHeight := mMatrix.pField^.table[aID].height;

          with mMatrix.pField^.table[aID] do
            Result := CalculateRasterToPixel(left, xTitleHight + top,
              left + xWidth, xTitleHight + top + xHeight);

          xCoordinateMode := CoordinateMode;
          if (xCoordinateMode = cmOutside) or (xCoordinateMode = cmInside) then
          begin
            xBoundary := FindeLineIDs(aID);

            with xBoundary do
            begin
              if Top < TotHorLines then
              begin
                case xCoordinateMode of
                  cmInside: CoordinateMode := cmBottom;
                  cmOutside: CoordinateMode := cmTop;
                end;
                xLine := HorLine[Top];
                Top := xLine.Top;
              end
              else
                Top := Result.Top;

              if Bottom < TotHorLines then
              begin
                case xCoordinateMode of
                  cmInside: CoordinateMode := cmTop;
                  cmOutside: CoordinateMode := cmBottom;
                end;
                xLine := HorLine[Bottom];
                Bottom := xLine.Top;
              end
              else
                Bottom := Result.Bottom;

              if Left < TotVertLines then
              begin
                case xCoordinateMode of
                  cmInside: CoordinateMode := cmRight;
                  cmOutside: CoordinateMode := cmLeft;
                end;
                xLine := VertLine[Left];
                Left := xLine.Left;
              end
              else
                Left := Result.Left;

              if Right < TotVertLines then
              begin
                case xCoordinateMode of
                  cmInside: CoordinateMode := cmLeft;
                  cmOutside: CoordinateMode := cmRight;
                end;
                xLine := VertLine[Right];
                Right := xLine.Left;
              end
              else
                Right := Result.Right;
            end;
            CoordinateMode := xCoordinateMode;

            Result := AdjustBoundary(Result, xBoundary, cbAbsolute);
          end;
        end;
      end;

    cGraphField: // Boundary of the Graph field (Channel Curve Graph)
      begin

        if (mMatrix.pScaleLayer <> nil) and (mMatrix.pScaleLayer^.xAxis <> nil) and
          (mMatrix.pScaleLayer^.yAxis <> nil) then
        begin
          with mMatrix.pScaleLayer^ do
          begin
            Result := CalculateRasterToPixel(xAxis[0].xPos, xTitleHight + yAxis[0].yPos,
              xAxis[TotXScales - 1].xPos, xTitleHight+ yAxis[TotYScales - 1].yPos);
          end;
        end;
      end;

    cNSLGraphField: // Boundary of the Graph field (Channel Curve Graph)
      begin

        if (mMatrix.pScaleLayer <> nil) and (mMatrix.pScaleLayer^.xAxis <> nil) and
          (mMatrix.pScaleLayer^.yAxis <> nil) then
        begin
          with mMatrix.pScaleLayer^ do
          begin
            Result := CalculateRasterToPixel(xAxis[cNSLXLeftScale].xPos, xTitleHight + yAxis[cNSLYTopScale].yPos,
              xAxis[cNSLXRightScale].xPos, xTitleHight + yAxis[cNSLYBottomScale].yPos);
          end;
        end;
      end;

    cThinGraphField: // Boundary of the Graph field (Channel Curve Graph)
      begin

        if (mMatrix.pScaleLayer <> nil) and (mMatrix.pScaleLayer^.xAxis <> nil) and
          (mMatrix.pScaleLayer^.yAxis <> nil) then
        begin
          with mMatrix.pScaleLayer^ do
          begin
            Result := CalculateRasterToPixel(xAxis[cThinXLeftScale].xPos, xTitleHight + yAxis[cThinYTopScale].yPos,
              xAxis[cThinXRightScale].xPos, xTitleHight + yAxis[cThinYBottomScale].yPos);
          end;
        end;
      end;

  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetFieldID(aX, aY: Integer): Integer;
var
  i, xWidth, xHeight: Integer; xPixel, xRaster: TPoint;
begin
  xPixel.x := aX;
  xPixel.y := aY;
  xRaster := CalculatePixelToRaster(xPixel);
  if TitleEnabled then
    xRaster.y := xRaster.y - mMatrix.pScaleLayer^.title.barHight;

  Result := TotFields;

  for i := 0 to TotFields - 1 do
  begin
    with mMatrix.pField^.table[i], xRaster do
    begin
      if width = 0 then
        xWidth := mMatrix.pField^.fieldWidth
      else
        xWidth := width;
      if height = 0 then
        xHeight := mMatrix.pField^.fieldHeight
      else
        xHeight := height;
      if (x >= left) and (x < (left + xWidth)) and (y >= top) and (y < (top + xHeight)) then
      begin
        Result := i;
        Break;
      end;
    end;
  end;

end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetFieldWeight(aId: Integer): Real;
begin
  if mMatrix.pField^.table[aID].width = 0 then
    Result := 1.0
  else
    Result := mMatrix.pField^.table[aID].width / mMatrix.pField^.Fieldwidth;

  if mMatrix.pField^.table[aID].height <> 0 then
    Result := Result * mMatrix.pField^.table[aID].height / mMatrix.pField^.fieldHeight;

end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetGraphField(aIndex: Integer): TRect;
begin
  if (aIndex >= cGraphField) and (aIndex <= cThinGraphField) then
    Result := GetCoordinate(0, aIndex)
  else
  begin
    Result.Left := 0;
    Result.Top := 0;
    Result.Right := ClientWidth;
    Result.Bottom := ClientHeight;
  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetLastCutMode: TLastCutMode;
begin
  Result := fLastCutMode;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetLastCutColor: TColor;
begin
  Result := fLastCutColor;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetLastCutField: Integer;
begin
  Result := fLastCutField;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetLinePen(aID, aIndex: Integer): TPenRec;
begin
  Result := cLinePenUnassigned;
  case aIndex of
    cHorizontal:
      begin
        if aID < TotHorLines then
          with mMatrix.pScaleLayer^.horLine do
          begin
            if table[aID].pPen <> nil then
            begin
              Result := table[aID].pPen^;
            end
            else
              Result := pen;
          end;
      end;

    cVertical:
      begin
        if aID < TotVertLines then
          with mMatrix.pScaleLayer^.vertLine do
          begin
            if table[aID].pPen <> nil then
              Result := table[aID].pPen^
            else
              Result := pen;
          end;
      end;
  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetNeighbourFieldID(var aX, aY: Integer): Integer;
var
  xRecT: TRect;
  xFieldID: Integer;
  xCoordinateMode: TCoordinateMode;
  xGap: Integer;
begin
  xFieldID := GetFieldID(aX, aY);
  if xFieldID < TotFields then
  begin
    xCoordinateMode := CoordinateMode;
    CoordinateMode := cmActual;
    xRect := Field[xFieldID];

    case fNeighbour of
      iaTop:
        begin
          xGap := (xRect.Bottom - xRect.Top) div 2;
          if (xRect.Top >= xGap) then
          begin

            aY := xRect.Top - xGap;
            Result := GetFieldID(aX, aY);
          end
          else
            Result := TotFields;
        end;
      iaLeft:
        begin
          xGap := (xRect.Right - xRect.Left) div 4;
          if xRect.Left >= xGap then
          begin
            aX := xRect.Left - xGap;
            Result := GetFieldID(aX, aY);
          end
          else
            Result := TotFields;
        end;
      iaBottom:
        begin
          aY := xRect.Bottom + ((xRect.Bottom - xRect.Top) div 2);
          Result := GetFieldID(aX, aY);
        end;
      iaRight:
        begin
          aX := xRect.Right + ((xRect.Right - xRect.Left) div 4);
          Result := GetFieldID(aX, aY);
        end;
    else
      Result := TotFields;
    end;
    CoordinateMode := xCoordinateMode;
  end
  else
    Result := TotFields;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetNumberOfSamples(aID: Integer): Integer;
begin
  if (mMatrix.pCurveLayer <> nil) and
//    (mMatrix.pCurveLayer^.numberOfSampleTbl <> nil) and
  (aID < TotXScales - 1) then

    Result := fNumberOfSamples

  else
    Result := 0;

end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetScale(aID, aIndex: Integer): TScaleRec;
var
  xRect: TRect;
  xTitleHight: Integer;
begin

  Result.value := 0;
  Result.pixcelToNextScale := 0;

  if TitleEnabled then
    xTitleHight := mMatrix.pScaleLayer^.title.barHight
  else
    xTitleHight := 0;


  if (mMatrix.pCurveLayer <> nil) and
//    (mMatrix.pCurveLayer^.numberOfSampleTbl <> nil) and
  (mMatrix.pScaleLayer <> nil) then
    case aIndex of

      cXAxis:
        begin
          if (mMatrix.pScaleLayer^.xAxis <> nil) and (aID < TotXScales) then
          begin
            with mMatrix.pCurveLayer^, mMatrix.pScaleLayer^, Result do
            begin
              if (aID + 1 < TotXScales) and (xAxis[aID].pFont <> nil) and (xAxis[aID + 1].pFont <> nil) then
              begin
                xRect := CalculateRasterToPixel(xAxis[aID].xPos, xTitleHight + xAxis[aID].yPos,
                  xAxis[aID + 1].xPos, xTitleHight + xAxis[aID + 1].yPos);
                pixcelToNextScale := xRect.Right - xRect.Left;
                numberOfSamples := self.NumberOfSamples[aID];
              end;

              try
                value := Round(StrToFloat(StringReplace(xAxis[aID].text, '.', DecimalSeparator, [rfReplaceAll])) * 10);
              except
                on e: Exception do
                begin
                  raise Exception.Create('TQualityMatrixBase.GetScale (xAxis) failed.' + e.Message);
//              ShowMessage('ScaleLayer_.xAxis[' + IntToStr(aID) + ']: ' + e.message);
                end;
              end;
            end;
          end;
        end;

      cYAxis:
        begin
          if (mMatrix.pScaleLayer^.YAxis <> nil) and (aID < TotYScales) then
          begin
            with mMatrix.pScaleLayer^, Result do
            begin
              if (aID + 1 < TotYScales) and (yAxis[aID].pFont <> nil) and (yAxis[aID + 1].pFont <> nil) then
              begin
                xRect := CalculateRasterToPixel(yAxis[aID + 1].xPos, xTitleHight + yAxis[aID + 1].yPos,
                  yAxis[aID].xPos, xTitleHight + yAxis[aID].yPos);
                if xRect.Bottom >= xRect.Top then
                  pixcelToNextScale := xRect.Bottom - xRect.Top
                else
                  pixcelToNextScale := xRect.Top - xRect.Bottom;

                numberOfSamples := 0;
              end;

              try
                value := Round(StrToFloat(StringReplace(yAxis[aID].text, '.', DecimalSeparator, [rfReplaceAll])) * 100);
              except
                on e: Exception do
                begin
                  raise Exception.Create('TQualityMatrixBase.GetScale (yAxis) failed.' + e.Message);
//              ShowMessage('ScaleLayer.yAxis[' + IntToStr(aID) + ']: ' + e.message);
                end;
              end;
            end;
          end;
        end;
    end;

end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetSCField(aID: Integer): TRect;
var
  xTitleHight: Integer;
begin
  Result := Rect(0, 0, 0, 0);

  if TitleEnabled then
    xTitleHight := mMatrix.pScaleLayer^.title.barHight
  else
    xTitleHight := 0;

  if (mMatrix.pDisplayLayer <> nil) and (mMatrix.pDisplayLayer^.roughClassTbl <> nil) then
    with mMatrix.pDisplayLayer^.roughClassTbl[aID].boarder do
    begin
      Result := CalculateRasterToPixel(left, xTitleHight + top,
        left + width, xTitleHight + top + height);
    end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetSCMembers(aID: Integer): TSCMemberArray;
begin
  Result := noMember;

  if (mMatrix.pDisplayLayer <> nil) and (mMatrix.pDisplayLayer^.roughClassTbl <> nil) then
    Result := mMatrix.pDisplayLayer^.roughClassTbl[aID].memberTbl;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetStateColor(aID, aIndex: Integer): TBrushRec;
begin
  Result := cBackgroundUnassigned;
  Result.style := bsClear;

  case aIndex of

    cCutActive:
      with mMatrix.pSelectionLayer^.cutState do
        if pActiveColor <> nil then Result := pActiveColor^;

    cCutPassive:
      with mMatrix.pSelectionLayer^.cutState do
        if pPassiveColor <> nil then
          Result := pPassiveColor^
        else
          Result := BackgroundColor[aID];

    cFFClusterActive:
      with mMatrix.pSelectionLayer^.fFClusterState do
        if pActiveColor <> nil then Result := pActiveColor^;

    cFFClusterPassive:
      with mMatrix.pSelectionLayer^.fFClusterState do
        if pPassiveColor <> nil then
          Result := pPassiveColor^
        else
          Result := BackgroundColor[aID];

    cSCMemberActive:
      with mMatrix.pSelectionLayer^.sCMemberState do
        if pActiveColor <> nil then Result := pActiveColor^;

    cSCMemberPassive:
      with mMatrix.pSelectionLayer^.sCMemberState do
        if pPassiveColor <> nil then
          Result := pPassiveColor^
        else
          Result := BackgroundColor[aID];
  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetText(aID, aIndex: Integer): TTextRec;
var
  xRect: TRect;
  xTitleHight: Integer;
begin
  Result := cTextUnassigned;

  if TitleEnabled then
    xTitleHight := mMatrix.pScaleLayer^.title.barHight
  else
    xTitleHight := 0;

  case aIndex of


    cCaption:
      begin
        if (mMatrix.pScaleLayer^.caption <> nil) and
          (aID < TotCaptions) then
        begin
          Result := mMatrix.pScaleLayer^.caption[aID];
          with Result do
          begin
            xRect := CalculateRasterToPixel(xPos, xTitleHight + yPos,
              xPos, xTitleHight + yPos);
            xPos := xRect.Left;
            yPos := xRect.Top;
          end;
        end;
      end;

    cXAxis:
      begin
        if (mMatrix.pScaleLayer^.xAxis <> nil) and
          (aID < TotXScales) then
        begin
          Result := mMatrix.pScaleLayer^.xAxis[aID];
          with Result do
          begin
            text := StringReplace(text, '.', DecimalSeparator, [rfReplaceAll]);
            xRect := CalculateRasterToPixel(xPos, xTitleHight + yPos,
              xPos, xTitleHight + yPos);
            xPos := xRect.Left;
            yPos := xRect.Top;
          end;
        end;
      end;

    cYAxis:
      begin
        if (mMatrix.pScaleLayer^.yAxis <> nil) and
          (aID < TotYScales) then
        begin
          Result := mMatrix.pScaleLayer^.yAxis[aID];
          with Result do
          begin
            text := StringReplace(text, '.', DecimalSeparator, [rfReplaceAll]);
            xRect := CalculateRasterToPixel(xPos, xTitleHight + yPos,
              xPos, xTitleHight + yPos);
            xPos := xRect.Left;
            yPos := xRect.Top;
          end;
        end;
      end;
  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetTextFormat(aID, aIndex: Integer): TFormatTextRec;
begin
  Result := cFormatTextUnassigned;
  case aIndex of
    cCuts: Result := mMatrix.pDisplayLayer^.number.cuts;

    cDefects: Result := mMatrix.pDisplayLayer^.number.defects;
  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetTitlePos: TTextRec;
var
  xRect: TRect;
begin

  Result.pFont := @(mMatrix.pScaleLayer^.title.pFont^);
  Result.horAlign := mMatrix.pScaleLayer^.title.horAlign;
  Result.vertAlign := mMatrix.pScaleLayer^.title.vertAlign;
  Result.xPos := mMatrix.pScaleLayer^.title.xTextPos;
  Result.yPos := mMatrix.pScaleLayer^.title.yTextPos;
  Result.text := Title; // %% title property
  with Result do
  begin
    xRect := CalculateRasterToPixel(xPos, yPos, xPos, yPos);
    xPos := xRect.Left;
    yPos := xRect.Top;
  end;

end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetTotItems(aIndex: Integer): Integer;
var
  i, xTest: Integer;
begin
  Result := 0;
  case aIndex of

    cField: Result := mTotFields;

    cSuperClass: Result := mTotSuperClasses;

    cHorizontal: Result := mTotHorLines;

    cVertical: Result := mTotVertLines;

    cXAxis: Result := mTotXScales;

    cYAxis: Result := mTotYScales;

    cCaption: Result := mTotCaptions;

    cSamples:
      begin
        if (mMatrix.pCurveLayer <> nil) then //and
//          (mMatrix.pCurveLayer^.numberOfSampleTbl <> nil) then
        begin
          for i := 0 to TotXScales - 1 do
          begin
//            xTest := NumberOfSamples[i];
            xTest := NumberOfSamples[i];
            Result := Result + xTest;
//          Result := Result + mMatrix.pCurveLayer^.numberOfSampleTbl[i];
          end;
        end;
      end;
  end;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetVisibleNSLRange: TRect;
begin
  if (mMatrix.pCurveLayer <> nil) then
    Result := mMatrix.pCurveLayer^.visibleNSLRange
  else
    Result := visibleNSLTRange;
end;
//------------------------------------------------------------------------------

function TQualityMatrixBase.GetVisibleTRange: TRect;
begin
  if (mMatrix.pCurveLayer <> nil) then
    Result := mMatrix.pCurveLayer^.visibleTRange
  else
    Result := visibleNSLTRange;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.SetLastCutMode(const aValue: TLastCutMode);
begin
  fLastCutMode := aValue;
  Invalidate;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.SetLastCutColor(const aValue: TColor);
begin
  fLastCutColor := aValue;
  Invalidate;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.SetLastCutField(const aValue: Integer);
begin
  fLastCutField := aValue;
  Invalidate;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.SetSubField(const aIndex, aValue: Integer);
begin
  case aIndex of
    cXAxis:
      begin
        fSubFieldX := aValue;
        CalculateNumberOfSamples;
      end;

    cYAxis:
      if fSubFieldY <> aValue then
      begin
        fSubFieldY := aValue;
(* %%Caculate TotSamples
*)
      end;
  end;

end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.SetSubType(const aValue: TMatrixSubType);
begin

  case MatrixType of

    mtSiro:
      begin

        case aValue of
          mstNone:
            if (fSubType <> mstSiroF) then
            begin
              fSubType := mstSiroF;
              MatrixType := mtSiro;
            end;
          mstSiroF,
            mstSiroH,
            mstSiroBD:
            if (fSubType <> aValue) then
            begin
              fSubType := aValue;
              MatrixType := mtSiro;
            end;
        end;
      end;
    mtColor:
      begin

        case aValue of
          mstNone:
            if (fSubType <> mstColorDark) then
            begin
              fSubType := mstColorDark;
              MatrixType := mtColor;
            end;
          mstColorDark,
            mstColorDarkBright:
            if (fSubType <> aValue) then
            begin
              fSubType := aValue;
              MatrixType := mtColor;
            end;
        end;
      end
  else
    fSubType := mstNone;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.SetTitleEnabled(const aValue: Boolean);
begin

  fTitleEnabled := aValue;
  Invalidate;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.SetTitle(const aValue: WideString);
begin

  fTitle := aValue;
  Invalidate;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrixBase.SetType(aType: TMatrixType);
begin

  if (aType <> fType) or (aType = mtSiro) or (aType = mtColor)then
  begin

    case aType of
      mtShortLongThin:
        begin
          mMatrix := cSLTMatrix;
          fTitle := mMatrix.pScaleLayer^.title.text;
          mTotCaptions := Length(sLTCaptions);
          mTotSuperClasses := Length(sLTRoughClasses);
          mTotFields := Length(sLTFields);
          mTotHorLines := Length(sLTHorLines);
          mTotVertLines := Length(sLTVertLines);
          mTotXScales := Length(sLTXScales);
          mTotYScales := Length(sLTYScales);
        end;
      mtSplice:
        begin
          mMatrix := cSpliceMatrix;
          fTitle := mMatrix.pScaleLayer^.title.text;
          mTotCaptions := Length(sLTCaptions);
          mTotSuperClasses := Length(spliceRoughClasses);
          mTotFields := Length(sLTFields);
          mTotHorLines := Length(spliceHorLines);
          mTotVertLines := Length(spliceVertLines);
          mTotXScales := Length(sLTXScales);
          mTotYScales := Length(sLTYScales);
        end;
      mtSiro:
        begin
          if fSubType = mstNone then
            fSubType := mstSiroF;

          case fSubType of

            mstSiroH:
              begin
                mMatrix := cSIROHMatrix;
                fTitle := mMatrix.pScaleLayer^.title.text;
                mTotCaptions := 0;
  //      mTotCaptions := Length(sIROCaptions);
                mTotSuperClasses := Length(sIRORoughClasses);
                mTotFields := Length(sIROFields);
                mTotHorLines := Length(sIROHorLines);
                mTotVertLines := Length(sIROVertLines);
                mTotXScales := Length(sIROXScales);
                mTotYScales := Length(sIROHYScales);
//                Invalidate;
              end;
            mstSiroBD:
              begin
                mMatrix := cSIROBDMatrix;
                fTitle := mMatrix.pScaleLayer^.title.text;
                mTotCaptions := 0;
                mTotSuperClasses := Length(sLTRoughClasses);
                mTotSuperClasses := Length(sIROBDRoughClasses);
                mTotFields := Length(sIROBDFields);
                mTotHorLines := Length(sIROBDHorLines);
                mTotVertLines := Length(sIROBDVertLines);
                mTotXScales := Length(sIROBDXScales);
                mTotYScales := Length(sIROBDYScales);
              end;
          else
            begin
              mMatrix := cSIROFMatrix;
              fTitle := mMatrix.pScaleLayer^.title.text;
              mTotCaptions := 0;
  //      mTotCaptions := Length(sIROCaptions);
              mTotSuperClasses := Length(sIRORoughClasses);
              mTotFields := Length(sIROFields);
              mTotHorLines := Length(sIROHorLines);
              mTotVertLines := Length(sIROVertLines);
              mTotXScales := Length(sIROXScales);
              mTotYScales := Length(sIROFYScales);
            end;
          end;
        end;
      mtColor:
        begin

          if fSubType = mstNone then
            fSubType := mstColorDark;

          case fSubType of

            mstColorDark:
              begin
                mMatrix := cColorDarkMatrix;
                fTitle := mMatrix.pScaleLayer^.title.text;
                mTotCaptions := Length(colorCaptions);
                mTotSuperClasses := Length(colorDarkRoughClasses);
                mTotFields := Length(colorDarkFields);
                mTotHorLines := Length(colorDarkHorLines);
                mTotVertLines := Length(colorDarkVertLines);
                mTotXScales := Length(colorDarkXScales);
                mTotYScales := Length(colorDarkYScales);
//                Invalidate;
              end;
          else
            begin
              mMatrix := cColorMatrix;
              fTitle := mMatrix.pScaleLayer^.title.text;
              mTotCaptions := Length(colorCaptions);
              mTotSuperClasses := Length(colorRoughClasses);
              mTotFields := Length(colorFields);
              mTotHorLines := Length(colorHorLines);
              mTotVertLines := Length(colorVertLines);
              mTotXScales := Length(colorXScales);
              mTotYScales := Length(colorYScales);
           end;
          end;
        end;
    else
      begin
        mMatrix := cSLTMatrix;
        fTitle := mMatrix.pScaleLayer^.title.text;
        mTotCaptions := Length(sLTCaptions); ;
        mTotSuperClasses := Length(sLTRoughClasses);
        mTotFields := Length(sLTFields);
        mTotHorLines := Length(sLTHorLines);
        mTotVertLines := Length(sLTVertLines);
        mTotXScales := Length(sLTXScales);
        mTotYScales := Length(sLTYScales);
        aType := mtShortLongThin;
      end;
    end;

    fType := aType;
    Invalidate;
  end;

end;
//------------------------------------------------------------------------------

//******************************************************************************
// TPixelAid
//******************************************************************************
//------------------------------------------------------------------------------

function TPixelAid.AlignItem(aAlignPos, aItemLength: Integer;
  aAlign: TItemAlignement): TRangeRec;
var
  xScratch: Integer;
begin

  with Result do
  begin
    case aAlign of

      iaCenter:
        begin
          xScratch := aItemLength div 2;
          if aAlignPos > xScratch then
            start := aAlignPos - xScratch
          else
            start := 0;
          stop := start + aItemLength;
        end;

      iaLeft, iaTop:
        begin
          if aAlignPos > aItemLength then
            start := aAlignPos - aItemLength
          else
            start := 0;
          stop := aAlignPos;
        end;

      iaRight, iaBottom:
        begin
          start := aAlignPos;
          stop := aAlignPos + aItemLength;
        end;
    end;
  end;
end;
//------------------------------------------------------------------------------

const
  cBorderWidthRel = 3; //Relative borderwidth

function TPixelAid.PlaceItem(aBounds: TRangeRec; aItemLength: Integer;
  aAlign: TItemAlignement): TRangeRec;
var
  xBorder, xDistance: Integer;
begin
  with aBounds do
  begin
    Result.start := start;
    Result.stop := stop;

    if start <= stop then
    begin
      xDistance := stop - start;
      if aAlign = iaCenter then
        xBorder := 0
      else
        xBorder := xDistance * cBorderWidthRel div 100;

      if (aItemLength + xBorder) <= xDistance then
        case aAlign of

          iaCenter:
            begin
              Result.start := start + (stop - start - aItemLength) div 2;
              Result.stop := Result.start + aItemLength;
            end;

          iaBottom, iaRight:
            begin
              Result.start := stop - aItemLength - xBorder;
              Result.stop := stop - xBorder;
            end;

          iaLeft, iaTop:
            begin
              Result.start := start + xBorder;
              Result.stop := start + aItemLength + xBorder;
            end;
        end;
    end;
  end;
end;

end.

