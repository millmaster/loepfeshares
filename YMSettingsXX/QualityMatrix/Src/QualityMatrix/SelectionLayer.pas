(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SelectionLayer.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr | Initial Release
|=========================================================================================*)
unit SelectionLayer;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, ExtCtrls, QualityMatrixDef,
  QualityMatrixBase, YarnParaDef, DbAccessTypes;

const
  _Version_         = '$Revision: 1 $';
  _Date_            = '$Modtime: 24.10.03 8:57 $';
  // $NoKeywords: $

type
  //...........................................................................
  TFieldStateTbl = array of AnsiChar;
  //...........................................................................

//  TSelectionLayer = class(TPaintBox)
  TSelectionLayer = class(TObject)
  private
    { Private declarations }

    fActiveVisible: Boolean;
    fCutActiveColor: TBrushRec;
    fCutState: TFieldStateTbl;
    fFFClusterActiveColor: TBrushRec;
    fFFClusterState: TFieldStateTbl;
    fInactiveColor: TBrushRec;
    fMode: TSelectionMode;
    fSCActiveColor: TBrushRec;
    fSCMemberState: TFieldStateTbl;
    fSubType: TMatrixSubType;
    mQualityMatrixBase: TQualityMatrixBase;

    function GetCertainFieldState(aFieldID: Integer; aMode: TSelectionMode): Boolean;
    function GetCertainFieldStateColor(aID, aIndex: Integer; aMode: TSelectionMode): TBrushRec;
    function GetFieldState(aFieldID: Integer): Boolean;
    function GetFieldStateColor(aID, aIndex: Integer): TBrushRec;
    function GetStateColor(const aIndex: Integer): TColor;
    function GetStateStyle(const aIndex: Integer): TBrushStyle;

    procedure SetCertainFieldState(aFieldID: Integer; aState: Boolean; aMode: TSelectionMode);
    procedure SetFieldState(aFieldID: Integer; aState: Boolean);
    procedure SetStateColor(const aIndex: Integer; const aValue: TColor);
    procedure SetStateStyle(const aIndex: Integer; const aValue: TBrushStyle);
    procedure SetSubType(const aValue: TMatrixSubType);
    procedure SetType(aType: TMatrixType);
    procedure SetMode(const aValue: TSelectionMode);

  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(aQualityMatrixBase: TQualityMatrixBase); reintroduce;
    destructor Destroy; override;

    procedure PaintField(aFieldID: Integer);

    procedure GetFieldStateTable(var aClassClear: T128ClassFields); overload;
    procedure GetFieldStateTable(var aFFClass, aFFCluster: T64ClassFields); overload;
    procedure GetFieldStateTable(var aDarkClass, aBrightClass,
      aDarkCluster, aBrightCluster: T64ClassFields); overload;

    procedure SetDefaultColor;

    procedure SetFieldStateTable(aClassClear: T128ClassFields); overload;
    procedure SetFieldStateTable(aFFClass, aFFCluster: T64ClassFields); overload;
    procedure SetFieldStateTable(const aDarkClass, aBrightClass,
      aDarkCluster, aBrightCluster: T64ClassFields); overload;

    property ActiveFieldColor[aID: Integer]: TBrushRec index cCutActive read GetFieldStateColor;
    property FieldState[aFieldID: Integer]: Boolean read GetFieldState write SetFieldState;
    property MatrixType: TMatrixType write SetType;
    property MatrixSubType: TMatrixSubType write SetSubType;
    property InactiveFieldColor[aID: Integer]: TBrushRec index cCutPassive read GetFieldStateColor;
    property SelectionMode: TSelectionMode read fMode write SetMode;

    property ActiveColor: TColor index cCutActive read GetStateColor write SetStateColor;
    property ActiveStyle: TBrushStyle index cCutActive read GetStateStyle write SetStateStyle;
    property ActiveVisible: Boolean read fActiveVisible write fActiveVisible;

    property InactiveColor: TColor index cCutPassive read GetStateColor write SetStateColor;

  published
    { Published declarations }

  end;

implementation

//------------------------------------------------------------------------------
//******************************************************************************
// TSelectionLayer
//******************************************************************************
//------------------------------------------------------------------------------

constructor TSelectionLayer.Create(aQualityMatrixBase: TQualityMatrixBase);
begin
  inherited Create;

  mQualityMatrixBase := aQualityMatrixBase;

  //  SelectionMode := smSCMemberField;
  SelectionMode := smCutField;

  SetDefaultColor;

end;
//------------------------------------------------------------------------------

destructor TSelectionLayer.Destroy;
begin
  Finalize(fCutState);
  Finalize(fFFClusterState);
  Finalize(fSCMemberState);

  inherited Destroy;
end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetCertainFieldState(aFieldID: Integer;
  aMode: TSelectionMode): Boolean;
var
  xByte             : Byte;
  xState            : TFieldStateTbl;
const
  Bit0              = $01;
begin

  case aMode of
    smCutField:
      xState := fCutState;

    smFFClusterField:
      xState := fFFClusterState;

    smSCMemberField:
      xState := fSCMemberState;
  else
    xState := fCutState;
  end;

  Result := False;
  if aFieldID < mQualityMatrixBase.TotFields then
    Result := xState[aFieldID] = cClassCut
  else
    Result := False;

end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetCertainFieldStateColor(aID, aIndex: Integer;
  aMode: TSelectionMode): TBrushRec;
begin
  Result := cBackgroundUnassigned;
  Result.style := bsClear;

  with mQualityMatrixBase do
    case aIndex of

      cCutActive:
        case aMode of

          smCutField:
            begin

              if fCutActiveColor.color <> clNone then
              begin
                if fCutActiveColor.style = bsClear then
                  Result := CutColor[aID]
                else
                  Result.style := fCutActiveColor.style;
                Result.color := fCutActiveColor.color;
              end
              else
              begin
                Result := CutColor[aID];
                if fCutActiveColor.style <> bsClear then
                  Result.style := fCutActiveColor.style;
              end;
            end;

          smFFClusterField:
            begin

              if fFFClusterActiveColor.color <> clNone then
              begin
                if fFFClusterActiveColor.style = bsClear then
                  Result := FFClusterColor[aID]
                else
                  Result.style := fFFClusterActiveColor.style;
                Result.color := fFFClusterActiveColor.color;
              end
              else
              begin
                Result := FFClusterColor[aID];
                if fFFClusterActiveColor.style <> bsClear then
                  Result.style := fFFClusterActiveColor.style;
              end;
            end;

          smSCMemberField:
            begin

              if fSCActiveColor.color <> clNone then
              begin
                if fSCActiveColor.style = bsClear then
                  Result := SCMemberColor[aID]
                else
                  Result.style := fSCActiveColor.style;
                Result.color := fSCActiveColor.color;
              end
              else
              begin
                Result := SCMemberColor[aID];
                if fSCActiveColor.style <> bsClear then
                  Result.style := fSCActiveColor.style;
              end;
            end;
        end;

      cCutPassive:
        begin
          if fInactiveColor.color = clNone then
          begin
            case SelectionMode of

              smCutField:
                Result := NoCutColor[aID];

              smFFClusterField:
                Result := NoFFClusterColor[aID];

              smSCMemberField:
                Result := NoSCMemberColor[aID];
            end;
            if fInactiveColor.style <> bsClear then
              Result.style := fInactiveColor.style;
          end
          else
          begin
            if fInactiveColor.style = bsClear then
              case SelectionMode of

                smCutField:
                  Result := NoCutColor[aID];

                smFFClusterField:
                  Result := NoFFClusterColor[aID];

                smSCMemberField:
                  Result := NoSCMemberColor[aID];
              end
            else
              Result.style := fInactiveColor.style;
            Result.color := fInactiveColor.color;
          end;
        end;
    end;
end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetFieldState(aFieldID: Integer): Boolean;
begin

  Result := GetCertainFieldState(aFieldID, SelectionMode);
end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetFieldStateColor(aID, aIndex: Integer): TBrushRec;
begin

  Result := GetCertainFieldStateColor(aID, aIndex, SelectionMode);
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.GetFieldStateTable(
  var aClassClear: T128ClassFields);
var
  i                 : Integer;
begin
  for i := 0 to High(aClassClear) do
    aClassClear[i] := fCutState[i];
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.GetFieldStateTable(
  var aFFClass, aFFCluster: T64ClassFields);
var
  i                 : Integer;
begin
  if SizeOf(aFFClass) = Length(fCutState) then
    for i := 0 to High(aFFClass) do
    begin

      aFFClass[i] := fCutState[i];

      if fSubType <> mstSiroBD then
      begin
        if fCutState[i] = cClassCut then
          fFFClusterState[i] := cClassCut
        else
          fFFClusterState[i] := fFFClusterState[i];
        aFFCluster[i] := fFFClusterState[i];
      end
      else
        aFFCluster[i] := cClassUncut;
    end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.GetFieldStateTable(var aDarkClass, aBrightClass,
  aDarkCluster, aBrightCluster: T64ClassFields);
var
  i                 : Integer;
begin

  if SizeOf(aDarkClass) <= Length(fCutState) then
  begin

    for i := 0 to High(aDarkClass) do
    begin

      aDarkClass[i] := fCutState[i];

      if fCutState[i] = cClassCut then
        fFFClusterState[i] := cClassCut
      else
        fFFClusterState[i] := fFFClusterState[i];

      aDarkCluster[i] := fFFClusterState[i];
    end;
  end
  else
    for i := 0 to High(aDarkClass) do
    begin

      aDarkClass[i] := cClassUncut;
      aDarkCluster[i] := cClassUncut;
    end;

  if (SizeOf(aDarkClass) + SizeOf(aBrightClass)) = Length(fCutState) then
  begin

    for i := 0 to High(aBrightClass) do
    begin

      aBrightClass[i] := fCutState[High(aDarkClass) + i + 1];

      if fCutState[High(aDarkClass) + i + 1] = cClassCut then
        fFFClusterState[High(aDarkClass) + i + 1] := cClassCut;

      aBrightCluster[i] := fFFClusterState[High(aDarkClass) + i + 1];
    end;
  end
  else
    for i := 0 to High(aBrightClass) do
    begin

      aBrightClass[i] := cClassUncut;
      aBrightCluster[i] := cClassUncut;
    end;
end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetStateColor(const aIndex: Integer): TColor;
var
  xColor            : TBrushRec;
begin
  Result := clNone;

  with mQualityMatrixBase do
    case aIndex of

      cCutActive:
        case SelectionMode of

          smCutField:
            if fCutActiveColor.color = clNone then
            begin
              xColor := CutColor[0];
              Result := xColor.color;
            end
            else
              Result := fCutActiveColor.color;

          smFFClusterField:
            if fFFClusterActiveColor.color = clNone then
            begin
              xColor := FFClusterColor[0];
              Result := xColor.color;
            end
            else
              Result := fFFClusterActiveColor.color;

          smSCMemberField:
            if fSCActiveColor.color = clNone then
            begin
              xColor := SCMemberColor[0];
              Result := xColor.color;
            end
            else
              Result := fSCActiveColor.color;
        end;

      cCutPassive:
        if fInactiveColor.color = clNone then
        begin

          case SelectionMode of
            smSCMemberField:
              xColor := NoSCMemberColor[0];

            smCutField:
              xColor := NoCutColor[0];

            smFFClusterField:
              xColor := NoFFClusterColor[0];
          end;

          Result := xColor.color;
        end
        else
          Result := fInactiveColor.color;
    end;
end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetStateStyle(const aIndex: Integer): TBrushStyle;
var
  xColor            : TBrushRec;
begin

  Result := cBackgroundUnassigned.style;

  with mQualityMatrixBase do
    case aIndex of

      cCutActive:
        case SelectionMode of

          smCutField:
            if fCutActiveColor.style = bsClear then
            begin
              xColor := CutColor[0];
              Result := xColor.style;
            end
            else
              Result := fCutActiveColor.style;

          smFFClusterField:
            if fFFClusterActiveColor.style = bsClear then
            begin
              xColor := FFClusterColor[0];
              Result := xColor.style;
            end
            else
              Result := fFFClusterActiveColor.style;

          smSCMemberField:
            if fSCActiveColor.style = bsClear then
            begin
              xColor := SCMemberColor[0];
              Result := xColor.style;
            end
            else
              Result := fSCActiveColor.style;
        end;

      //    cCutPassive:
    end;

end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.PaintField(aFieldID: Integer);
var
  xRect             : TRect;
  xBoarder          : Integer;
  xTBrush           : TBrushRec;
begin
  with mQualityMatrixBase do
  begin

    CoordinateMode := cmInside;
    xRect := Field[aFieldID];

    if ActiveVisible then
    begin

      case SelectionMode of

        smCutField, smFFClusterField:
          begin

            if (LastCutMode = lcField) and (LastCutField = aFieldID) then
            begin
              xTBrush := InactiveFieldColor[aFieldID];
              xTBrush.color := LastCutColor;
              FillField(xRect, xTBrush);
            end
            else
            begin

              if GetCertainFieldState(aFieldID, smCutField) then

                FillField(xRect, GetCertainFieldStateColor(aFieldID, cCutActive, smCutField))

              else if GetCertainFieldState(aFieldID, smFFClusterField) then
              begin
                FillField(xRect, InactiveFieldColor[aFieldID]);

                with xRect do
                begin
                  xBoarder := (Right - Left) div 4;
                  Left := Left + xBoarder;
                  Right := Right - xBoarder;
                  xBoarder := (Bottom - Top) div 4;
                  Top := Top + xBoarder;
                  Bottom := Bottom - xBoarder;
                end;

                FillField(xRect, GetCertainFieldStateColor(aFieldID, cCutActive, smFFClusterField));
              end
              else
                FillField(xRect, InactiveFieldColor[aFieldID]);

            end;
          end;

        smSCMemberField:
          begin
            if GetCertainFieldState(aFieldID, smSCMemberField) then
              FillField(xRect, GetCertainFieldStateColor(aFieldID, cCutActive, smSCMemberField))
            else
              FillField(xRect, InactiveFieldColor[aFieldID]);
          end;
      end;
    end
    else
      FillField(xRect, InactiveFieldColor[aFieldID]);

  end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetCertainFieldState(aFieldID: Integer;
  aState: Boolean; aMode: TSelectionMode);
var
  xBitPosition      : Integer;
  function FieldState(aState: Boolean): AnsiChar;
  begin
    if aState then
      Result := cClassCut
    else
      Result := cClassUncut;
  end;

begin
  if aFieldID < mQualityMatrixBase.TotFields then
  begin
    case aMode of
      smCutField:
        begin
          fFFClusterState[aFieldID] := FieldState(aState);
          fCutState[aFieldID] := FieldState(aState);
        end;

      smFFClusterField:
        begin
          if fCutState[aFieldID] = cClassCut then
            fFFClusterState[aFieldID] := cClassCut
          else
            fFFClusterState[aFieldID] := FieldState(aState);
        end;

      smSCMemberField:
        begin
          fSCMemberState[aFieldID] := FieldState(aState);
        end;
    end;
  end;
end;

//------------------------------------------------------------------------------

procedure TSelectionLayer.SetDefaultColor;
begin

  fCutActiveColor.color := clNone;
  fCutActiveColor.style := bsClear;

  fFFClusterActiveColor.color := clNone;
  fFFClusterActiveColor.style := bsClear;

  fInactiveColor.color := clNone;
  fInactiveColor.style := bsClear;

  fSCActiveColor.color := clNone;
  fSCActiveColor.style := bsClear;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetFieldState(aFieldID: Integer; aState: Boolean);
begin
  SetCertainFieldState(aFieldID, aState, SelectionMode);
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetFieldStateTable(
  aClassClear: T128ClassFields);
var
  i                 : Integer;
begin
  for i := 0 to High(aClassClear) do
    fCutState[i] := aClassClear[i];
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetFieldStateTable(
  aFFClass, aFFCluster: T64ClassFields);
var
  i                 : Integer;
begin
  for i := 0 to High(aFFClass) do
  begin
    fCutState[i] := aFFClass[i];

    if fSubType <> mstSiroBD then
    begin
      fFFClusterState[i] := aFFCluster[i];
      if fCutState[i] = cClassCut then
        fFFClusterState[i] := cClassCut;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetFieldStateTable(const aDarkClass, aBrightClass,
  aDarkCluster, aBrightCluster: T64ClassFields);
var
  i                 : Integer;
begin
  if SizeOf(aDarkClass) <= Length(fCutState) then
    for i := 0 to High(aDarkClass) do
    begin
      fCutState[i] := aDarkClass[i];

      fFFClusterState[i] := aDarkCluster[i];
      if fCutState[i] = cClassCut then
        fFFClusterState[i] := cClassCut;
    end;

  if (SizeOf(aDarkClass) + SizeOf(aBrightClass)) = Length(fCutState) then
    for i := 0 to High(aBrightClass) do
    begin
      fCutState[High(aBrightClass) + i + 1] := aBrightClass[i];

      fFFClusterState[High(aBrightClass) + i + 1] := aBrightCluster[i];
      if fCutState[High(aBrightClass) + i + 1] = cClassCut then
        fFFClusterState[High(aBrightClass) + i + 1] := cClassCut;
    end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetMode(const aValue: TSelectionMode);
begin
  fMode := aValue;
  ActiveVisible := True;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetStateColor(const aIndex: Integer;
  const aValue: TColor);
begin

  case aIndex of

    cCutActive:
      case SelectionMode of

        smCutField:
          fCutActiveColor.color := aValue;

        smFFClusterField:
          fFFClusterActiveColor.color := aValue;

        smSCMemberField:
          fSCActiveColor.color := aValue;
      end;

    cCutPassive:
      fInactiveColor.color := aValue;
  end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetStateStyle(const aIndex: Integer;
  const aValue: TBrushStyle);
begin

  case aIndex of

    cCutActive:
      case SelectionMode of

        smCutField:
          fCutActiveColor.style := aValue;

        smFFClusterField:
          fFFClusterActiveColor.style := aValue;

        smSCMemberField:
          fSCActiveColor.style := aValue;
      end;

    //    cCutPassive:
  end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetSubType(const aValue: TMatrixSubType);
var
  i                 : Integer;
begin
  if fSubType <> aValue then
  begin
    if (aValue = mstSiroBD) or (fSubType = mstSiroBD) then
    begin
      for i := 0 to High(fFFClusterState) do
        fFFClusterState[i] := cClassUncut;

      for i := 0 to High(fCutState) do
        fCutState[i] := cClassUncut;
    end;

    fSubType := aValue;
  end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetType(aType: TMatrixType);
var
  i, xRemainder     : Integer;
begin
  with mQualityMatrixBase do
  begin

    if TotFields <> Length(fCutState) then
    begin
      SetLength(fCutState, TotFields);
      for i := 0 to High(fCutState) do
        fCutState[i] := cClassUncut;
    end;

    if TotFields <> Length(fFFClusterState) then
    begin
      SetLength(fFFClusterState, TotFields);
      for i := 0 to High(fFFClusterState) do
        fFFClusterState[i] := cClassUncut;
    end;

    if TotFields <> Length(fSCMemberState) then
    begin
      SetLength(fSCMemberState, TotFields);
      for i := 0 to High(fSCMemberState) do
        fSCMemberState[i] := cClassUncut;
    end;
  end;
end;
//------------------------------------------------------------------------------
end.

