(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QualityMatrix.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr | Initial Release
|=========================================================================================*)
unit QualityMatrix;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, QualityMatrixBase, DisplayLayer,
  CurveLayer, ScaleLayer, SelectionLayer, QualityMatrixDef, YarnParaDef, YarnParaUtils, DbAccessTypes;

const
  _Version_ = '$Revision: 2 $';
  _Date_ = '$Modtime: 30.01.04 15:50 $';
  // $NoKeywords: $

type
  //...........................................................................
  TOnFieldClick = procedure(aFieldId: Integer; aButton: TMouseButton;
    aShift: TShiftState) of object;

  //...........................................................................

  TQualityMatrix = class(TQualityMatrixBase)
  private

    { Private declarations }

    fDisplayMode: TDisplayMode;
    fMode: TMatrixMode;
    fOnConfirm: TOnConfirm;
    fOnFieldClick: TOnFieldClick;
    fOnFieldDblClick: TOnFieldClick;
    fOnParameterChange: TOnParameterChange;
    fOnTouch: TMouseEvent;

    mDisplayLayer: TDisplayLayer;
    mCurveLayer: TCurveLayer;
    mScaleLayer: TScaleLayer;
    mSelectionLayer: TSelectionLayer;

    mPixelAid: TPixelAid;

    mTouchFieldID: Integer;
    mTouchFieldState: Boolean;
    fEnabled: Boolean;
    fDisableMessage: WideString;
    fValidateCuts: Boolean;
    fValidateDefects: Boolean;

    function GetActiveVisible: Boolean;
    function GetCurveColor(const aIndex: Integer): TColor;
    function GetCurveStyle(const aIndex: Integer): TPenStyle;
    function GetCurveVisible(const aIndex: Integer): Boolean;

    function GetDisplayMode: TDisplayMode;
    function GetDotsColor: TColor;
    function GetFieldColor(aFieldId: Integer): TColor;
    function GetFieldDots(aFieldId: Integer): Integer;
    function GetFieldState(aFieldId: Integer): Boolean;
    function GetStateColor(const aIndex: Integer): TColor;
    function GetSubField(const aIndex: Integer): Integer;
    function GetSubType: TMatrixSubType;
    function GetTotClasse: Integer;
    function GetTotValues: Integer;
    function GetValueColor(const aIndex: Integer): TColor;
    function GetZeroLimit: Double;

    procedure DoParameterChange(aSource: TParameterSource);
    procedure PutActiveVisible(const aValue: Boolean);
    procedure PutCurveColor(const aIndex: Integer; const aValue: TColor);
    procedure PutCurveStyle(const aIndex: Integer; const aValue: TPenStyle);
    procedure PutCurveVisible(const aIndex: Integer; const aValue: Boolean);
    procedure SetDisableMessage(const aValue: WideString); //%%
    procedure SetDisplayMode(const aValue: TDisplayMode);
    procedure SetDotsColor(const aValue: TColor);
    procedure SetFieldColor(aFieldId: Integer; const aValue: TColor);
    procedure SetFieldDots(aFieldId: Integer; const aValue: Integer);
    procedure SetFieldState(aFieldId: Integer; aState: Boolean);
    procedure SetMode(aMode: TMatrixMode);
    procedure SetStateColor(const aIndex: Integer; const aValue: TColor);
    procedure SetSubType(aType: TMatrixSubType);
    procedure SetType(aType: TMatrixType);
    procedure SetValueColor(const aIndex: Integer; const aValue: TColor);
    procedure SetZeroLimit(const aValue: Double);
    procedure UpdateSuperClassHint;
    procedure SetSubField(const aIndex, aValue: Integer);
  protected
    { Protected declarations }
    procedure SetEnabled(const aValue: Boolean); reintroduce;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

    function GetChannelPara: TChannelSettingsRec;
    function GetClusterDiameter: Double;

    function GetCutCurve(const aCurveType: TCurveTypes): TCutCurveList;
    function GetSplicePara: TChannelSettingsRec;

    function GetValue(aFieldId, aIndex: Integer): Real;

    procedure GetFieldStateTable(var aClassClear: T128ClassFields); overload;
    procedure GetFieldStateTable(var aFFClass, aFFCluster: T64ClassFields); overload;
    procedure GetFieldStateTable(var aDarkClass, aBrightClass,
      aDarkCluster, aBrightCluster: T64ClassFields); overload;

    procedure MouseDown(aButton: TMouseButton;
      aShift: TShiftState; aX, aY: Integer); override;
    procedure MouseMove(aShift: TShiftState; aX, aY: Integer); override;
    procedure MouseUp(aButton: TMouseButton;
      aShift: TShiftState; aX, aY: Integer); override;
    procedure Paint; override;
    procedure PaintClass(aClassID: Integer);
    procedure PaintDisableMessage;
    procedure PaintField(aFieldId: Integer);
    procedure PutYMSettings(var aSettings: TYarnSettingsRec);
    procedure Resize; override;
    procedure SetDefaultColor;
    procedure SetFieldStateTable(const aClassClear: T128ClassFields); overload;
    procedure SetFieldStateTable(const aFFClass, aFFCluster: T64ClassFields); overload;
    procedure SetFieldStateTable(const aDarkClass, aBrightClass,
      aDarkCluster, aBrightCluster: T64ClassFields); overload;
    procedure SetValue(aFieldId, aIndex: Integer; aValue: Real);

    property Cuts[aFieldId: Integer]: Real index cCuts read GetValue write SetValue;
    property Defects[aFieldId: Integer]: Real index cDefects read GetValue write SetValue;
    property FieldColor[aFieldId: Integer]: TColor read GetFieldColor write SetFieldColor;
    property FieldDots[aFieldId: Integer]: Integer read GetFieldDots write SetFieldDots;
    property FieldState[aFieldId: Integer]: Boolean read GetFieldState write SetFieldState;

  published
    { Published declarations }
    property ActiveColor: TColor index cCutActive read GetStateColor write SetStateColor;
    // if MatrixMode = mmSelectSettings the color of active settings fields is set
    // if  MatrixMode = mmSelectSCMembers the color of active super class fields is set
    //  if ActiveColor := clNone the default color is enabled
    property ActiveVisible: Boolean read GetActiveVisible write PutActiveVisible;
    // if True Active Fields are visible
    property Anchors;
    property ChannelColor: TColor index cChannelCurve read GetCurveColor write PutCurveColor;
    // Color of Channel Curve
    property ChannelStyle: TPenStyle index cChannelCurve read GetCurveStyle write PutCurveStyle;
    // If ChannelStyle = psSolid then Pen.Width := 2
    // else Pen.Width := 1
    property ChannelVisible: Boolean index cChannelCurve read GetCurveVisible write PutCurveVisible;
    // if False the curve is disabled

    property ClusterColor: TColor index cClusterCurve read GetCurveColor write PutCurveColor;
    // Color of Cluster Curve, if clNone the curve is disabled
    property ClusterStyle: TPenStyle index cClusterCurve read GetCurveStyle write PutCurveStyle;
    // If ClusterStyle = psSolid then Pen.Width := 2
    // else Pen.Width := 1
    property ClusterVisible: Boolean index cClusterCurve read GetCurveVisible write PutCurveVisible;
    // if False the curve is disabled

    property Color;
    // Background color
    property CutsColor: TColor index cCuts read GetValueColor write SetValueColor;
    // Color of Cuts value
    // if CutsColor := clNone the default color is enabled
    property DefectsColor: TColor index cDefects read GetValueColor write SetValueColor;
    // Color of Defects value
    // if DefectsColor := clNone the default color is enabled
    property DisableMessage: WideString read fDisableMessage write SetDisableMessage; //%%
    // Message appears if QualityMatrix disabled (visible: True, invisible: False)
    property DisplayMode: TDisplayMode read GetDisplayMode write SetDisplayMode;
    // Message appears if QualityMatrix disabled (visible: True, invisible: False)
    property DotsColor: TColor read GetDotsColor write SetDotsColor;
    property Enabled: Boolean read fEnabled write SetEnabled;
    // Enables the QualityMatrix (visible: True, invisible: False)
    property InactiveColor: TColor index cCutPassive read GetStateColor write SetStateColor;
    // The color of active settings and super class fields is set
    //  if ActiveColor := clNone the default color is enabled

    property MatrixMode: TMatrixMode read fMode write SetMode;
    property MatrixType write SetType;
    property MatrixSubType read GetSubType write SetSubType;
    property OnConfirm: TOnConfirm read fOnConfirm write fOnConfirm;
    property OnFieldClick: TOnFieldClick read fOnFieldClick write fOnFieldClick;
    property OnFieldDblClick: TOnFieldClick read fOnFieldDblClick write fOnFieldDblClick;
    property OnParameterChange: TOnParameterChange read fOnParameterChange write fOnParameterChange;
    property OnTouch: TMouseEvent read fOnTouch write fOnTouch;

    property SpliceColor: TColor index cSpliceCurve read GetCurveColor write PutCurveColor;
    // Color of Splice Curve, if clNone the curve is disabled
    property SpliceStyle: TPenStyle index cSpliceCurve read GetCurveStyle write PutCurveStyle;
    // If SpliceStyle = psSolid then Pen.Width := 2
    // else Pen.Width := 1
    property SpliceVisible: Boolean index cSpliceCurve read GetCurveVisible write PutCurveVisible;

    property SubFieldX: Integer index cXAxis read GetSubField write SetSubField;
    // Number of horizontal subfields of a matrix field: 0..
    property SubFieldY: Integer index cYAxis read GetSubField write SetSubField;
    // Number of vertical subfields of a matrix field: 0..

    // if False the curve is disabled
    property TotClasses: Integer read GetTotClasse;
    property TotValues: Integer read GetTotValues;
    // Total Cut or Defect values (dependet on Mode or Type respectivly)

    property ValidateCuts: Boolean read fValidateCuts write fValidateCuts;
    property ValidateDefects: Boolean read fValidateDefects write fValidateDefects;
    // if True: inhibits displaying cuts or defects depending on Fieldstate
    property ZeroLimit: Double read GetZeroLimit write SetZeroLimit;
  end;

  // procedure Register;
function InsertButtonToShiftState(aButton: TMouseButton;
  aShift: TShiftState): TShiftState;

implementation
//------------------------------------------------------------------------------

function InsertButtonToShiftState(aButton: TMouseButton;
  aShift: TShiftState): TShiftState;
begin
  case aButton of
    mbLeft:
      Result := aShift + [ssLeft];

    mbRight:
      Result := aShift + [ssRight];

    mbMiddle:
      Result := aShift + [ssMiddle];
  end;
end;
(*
procedure Register;
begin
  RegisterComponents('YMClearer', [TQualityMatrix]);
end;
*)
//******************************************************************************
// TQualityMatrix
//******************************************************************************
//------------------------------------------------------------------------------

constructor TQualityMatrix.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  mDisplayLayer := nil;
  mCurveLayer := nil;
  mScaleLayer := nil;
  mSelectionLayer := nil;

  // Set default Properties
  mPixelAid := TPixelAid.Create;
  fDisableMessage := 'Quality Matrix disabled';

  Color := clWhite; // Panel Background Color

  fEnabled := True;
  fOnConfirm := nil;
  fOnFieldClick := nil;
  fOnFieldDblClick := nil;
  fOnParameterChange := nil;
  fOnTouch := nil;
  fValidateCuts := False;
  fValidateDefects := True;

  MatrixType := mtShortLongThin;
  MatrixMode := mmDisplayData;
  DisplayMode := dmValues;
end;
//------------------------------------------------------------------------------

destructor TQualityMatrix.Destroy;
begin

  FreeAndNil(mDisplayLayer);
  FreeAndNil(mCurveLayer);
  FreeAndNil(mScaleLayer);
  FreeAndNil(mSelectionLayer);

  FreeAndNil(mPixelAid);

  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.DoParameterChange(aSource: TParameterSource);
begin

  if Assigned(fOnParameterChange) then
    fOnParameterChange(aSource);
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetActiveVisible: Boolean;
begin
  if Assigned(mSelectionLayer) then
    Result := mSelectionLayer.ActiveVisible
  else
    Result := False;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetChannelPara: TChannelSettingsRec;
begin

  if Assigned(mCurveLayer) then
    Result := mCurveLayer.GetChannelPara
  else
    Result := cChannelSettingsOff;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetClusterDiameter: Double;
begin

  if Assigned(mCurveLayer) then
    Result := mCurveLayer.GetClusterDiameter
  else
    Result := cMinShortDia;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetCurveColor(const aIndex: Integer): TColor;
begin
  Result := clSilver;

  if Assigned(mCurveLayer) then
  begin
    with mCurveLayer do
      case aIndex of

        cChannelCurve:
          Result := ChannelColor;

        cClusterCurve:
          Result := ClusterColor;

        cSpliceCurve:
          Result := SpliceColor;
      end;
  end;

end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetCurveStyle(const aIndex: Integer): TPenStyle;
begin
  Result := psSolid;

  if Assigned(mCurveLayer) then
  begin
    with mCurveLayer do
      case aIndex of

        cChannelCurve:
          Result := ChannelStyle;

        cClusterCurve:
          Result := ClusterStyle;

        cSpliceCurve:
          Result := SpliceStyle;
      end;
  end;

end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetCurveVisible(const aIndex: Integer): Boolean;
begin
  Result := False;

  if Assigned(mCurveLayer) then
  begin
    with mCurveLayer do
      case aIndex of

        cChannelCurve:
          Result := ChannelVisible;

        cClusterCurve:
          Result := ClusterVisible;

        cSpliceCurve:
          Result := SpliceVisible;
      end;
  end;

end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetCutCurve(
  const aCurveType: TCurveTypes): TCutCurveList;
begin

  if Assigned(mCurveLayer) then
    Result := mCurveLayer.GetCutCurve(aCurveType)
  else
    Result := nil;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetDisplayMode: TDisplayMode;
begin

  if Assigned(mDisplayLayer) then
    fDisplayMode := mDisplayLayer.Mode;

  Result := fDisplayMode;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetDotsColor: TColor;
begin

  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.DotsColor
  else
    Result := clBlack;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetFieldColor(aFieldId: Integer): TColor;
begin

  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.FieldColor[aFieldId]
  else
    Result := clBlack;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetFieldDots(aFieldId: Integer): Integer;
begin
  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.FieldDots[aFieldId]
  else
    Result := 0;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetFieldState(aFieldId: Integer): Boolean;
begin
  if Assigned(mSelectionLayer) then
    Result := mSelectionLayer.FieldState[aFieldId]
  else
    Result := False;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.GetFieldStateTable(var aClassClear: T128ClassFields);
begin
  if Assigned(mSelectionLayer) then
    mSelectionLayer.GetFieldStateTable(aClassClear);
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.GetFieldStateTable(var aFFClass, aFFCluster: T64ClassFields);
begin
  if Assigned(mSelectionLayer) then
    mSelectionLayer.GetFieldStateTable(aFFClass, aFFCluster);
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.GetFieldStateTable(var aDarkClass, aBrightClass,
  aDarkCluster, aBrightCluster: T64ClassFields);
begin
  if Assigned(mSelectionLayer) then
    mSelectionLayer.GetFieldStateTable(aDarkClass, aBrightClass,
      aDarkCluster, aBrightCluster);
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetSplicePara: TChannelSettingsRec;
begin

  if Assigned(mCurveLayer) then
    Result := mCurveLayer.GetSplicePara
  else
    Result := cChannelSettingsOff;
  Result := mCurveLayer.GetSplicePara
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetStateColor(const aIndex: Integer): TColor;
begin
  Result := clNone;

  if Assigned(mSelectionLayer) then
    case aIndex of

      cCutActive:
        Result := mSelectionLayer.ActiveColor;

      cCutPassive:
        Result := mSelectionLayer.InactiveColor;
    end;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetSubField(const aIndex: Integer): Integer;
begin

  case aIndex of
    cXAxis:
      Result := inherited SubFieldX;

    cYAxis:
      Result := inherited SubFieldY;

  else
    Result := 0;
  end;

end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetSubType: TMatrixSubType;
begin
  Result := inherited MatrixSubType;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetTotClasse: Integer;
begin

  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.TotClasses
  else
    Result := 0;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetTotValues: Integer;
begin

  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.TotValues
  else
    Result := 0;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetValue(aFieldId, aIndex: Integer): Real;
begin
  Result := 0;

  if Assigned(mDisplayLayer) then
    case aIndex of
      cCuts:
        Result := mDisplayLayer.Cuts[aFieldId];

      cDefects:
        Result := mDisplayLayer.Defects[aFieldId];
    end;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetValueColor(const aIndex: Integer): TColor;
begin
  Result := clNone;

  if Assigned(mDisplayLayer) then
    case aIndex of

      cCuts:
        Result := mDisplayLayer.CutsColor;

      cDefects:
        Result := mDisplayLayer.DefectsColor;
    end;
end;
//------------------------------------------------------------------------------

function TQualityMatrix.GetZeroLimit: Double;
begin
  Result := 0.01;
  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.ZeroLimit;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.MouseDown(aButton: TMouseButton;
  aShift: TShiftState; aX, aY: Integer);
var
  xFieldID, i: Integer;
begin
  aShift := InsertButtonToShiftState(aButton, aShift);

  inherited MouseDown(aButton, aShift, aX, aY);

  if Enabled then
  begin

    xFieldID := GetFieldID(aX, aY);

    if xFieldID < TotFields then
    begin

      if ssDouble in aShift then
      begin
        if Assigned(fOnFieldDblClick) then
          fOnFieldDblClick(xFieldID, aButton, aShift);
      end
        // wss
      else if Assigned(fOnFieldClick) then
        //      else if Assigned(fOnFieldDblClick) then
        fOnFieldClick(xFieldID, aButton, aShift);
    end;

    if Assigned(mCurveLayer) then
      mCurveLayer.CustomMouseDown(aShift, aX, aY);

    if not Assigned(mCurveLayer) or not mCurveLayer.ImageStored then
    begin
      case fMode of
        mmSelectSettings, mmSelectCutFields,
          mmSelectSCMembers, mmSelectFFClusterFields:
          begin
            xFieldID := GetFieldID(aX, aY);

            if xFieldID < TotFields then
            begin

              if ([ssShift, ssLeft] <= aShift) and ActiveVisible then
              begin

                mTouchFieldID := xFieldID;
                mTouchFieldState := not FieldState[xFieldID];
                if Assigned(fOnConfirm) and (xFieldID < TotFields) then
                  fOnConfirm(0);

                NeighbourField := iaTop;

                repeat

                  FieldState[xFieldID] := mTouchFieldState;

                  PaintField(xFieldID);

                  xFieldID := GetNeighbourFieldID(aX, aY);

                until xFieldID >= TotFields;

                UpdateSuperClassHint;

                if Assigned(mCurveLayer) then
                  mCurveLayer.CustomPaint;
              end
              else if ([ssShift, ssRight] <= aShift) and ActiveVisible then
              begin
                mTouchFieldID := xFieldID;
                mTouchFieldState := not FieldState[xFieldID];
                if Assigned(fOnConfirm) and (xFieldID < TotFields) then
                  fOnConfirm(0);

                repeat

                  NeighbourField := iaTop;
                  i := aY;
                  repeat

                    FieldState[xFieldID] := mTouchFieldState;

                    PaintField(xFieldID);

                    xFieldID := GetNeighbourFieldID(aX, i);

                  until xFieldID >= TotFields;

                  NeighbourField := iaRight;
                  xFieldID := GetNeighbourFieldID(aX, aY);

                until xFieldID >= TotFields;

                UpdateSuperClassHint;

                if Assigned(mCurveLayer) then
                  mCurveLayer.CustomPaint;
              end
              else if ([ssLeft] <= aShift) and ActiveVisible then
              begin

                mTouchFieldID := xFieldID;
                mTouchFieldState := not FieldState[xFieldID];

                FieldState[xFieldID] := not FieldState[xFieldID];
                if Assigned(fOnConfirm) and (xFieldID < TotFields) then
                  fOnConfirm(0);

                PaintField(xFieldID);

                UpdateSuperClassHint;
                if Assigned(mCurveLayer) then
                  mCurveLayer.CustomPaint;
              end

            end;
          end;
      end;
    end;
    if Assigned(fOnTouch) then
      fOnTouch(self, aButton, aShift, aX, aY);
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.MouseMove(aShift: TShiftState; aX, aY: Integer);
var
  xFieldID: Integer;
begin
  inherited MouseMove(aShift, aX, aY);

  if Enabled then
  begin

    if Assigned(mCurveLayer) then
      mCurveLayer.CustomMouseMove(aShift, aX, aY);

    if not Assigned(mCurveLayer) or not mCurveLayer.ImageStored then
    begin
      case fMode of
        mmSelectSettings, mmSelectCutFields,
          mmSelectSCMembers, mmSelectFFClusterFields:
          begin

            if [ssLeft] <= aShift then
            begin
              xFieldID := GetFieldID(aX, aY);
              if (xFieldID < TotFields) and (xFieldID <> mTouchFieldID) then
              begin
                mTouchFieldID := xFieldID;
                FieldState[xFieldID] := mTouchFieldState;
                if Assigned(fOnConfirm) then
                  fOnConfirm(0);

                PaintField(xFieldID);

                UpdateSuperClassHint;

                if Assigned(mCurveLayer) then
                  mCurveLayer.CustomPaint;
              end;
            end;

          end;
      end;
    end;

  end;
end;

//------------------------------------------------------------------------------

procedure TQualityMatrix.MouseUp(aButton: TMouseButton;
  aShift: TShiftState; aX, aY: Integer);
begin
  aShift := InsertButtonToShiftState(aButton, aShift);

  inherited;
  if Enabled then
  begin

    if Assigned(mCurveLayer) then
      mCurveLayer.CustomMouseUp(aShift, aX, aY);
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.Paint;
var
  i: Integer;
begin
  inherited Paint;

  if Enabled then
  begin

    if Assigned(mCurveLayer) and mCurveLayer.ImageStored then
      mCurveLayer.PaintImage
    else
    begin
      if Assigned(mScaleLayer) then
        mScaleLayer.CustomPaint;

      for i := 0 to TotFields - 1 do
      begin
        PaintField(i);
      end;

      for i := 0 to TotClasses - 1 do
      begin
        PaintClass(i);
      end;

    end;

    if Assigned(mCurveLayer) then
      mCurveLayer.CustomPaint;
  end
  else
    PaintDisableMessage;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.PaintClass(aClassID: Integer);
begin
  case fDisplayMode of

    dmSCValues, dmCalculateSCValues:

      begin
        mDisplayLayer.PaintField(aClassID);
      end;
  end;

end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.PaintDisableMessage;
var
  xTextRect: TRect;
  xScratch: Integer;
begin

  Canvas.Font.Color := clDisableText;
  Canvas.Font.Style := [];
  Canvas.Font.Size := fsDisableText;
  Canvas.Brush.Style := bsClear;
  Canvas.Font.Name := 'Arial Unicode MS'; //%% get from ini-file!

  xScratch := Canvas.TextWidth(DisableMessage);
  if Width > xScratch then
  begin
    xTextRect.Left := (Width - xScratch) div 2;
    xTextRect.Right := xTextRect.Left + xScratch;
  end
  else
  begin
    xTextRect.Left := 0;
    xTextRect.Right := Width;
  end;

  xScratch := Canvas.TextHeight(DisableMessage);
  if Height > xScratch then
  begin
    xTextRect.Top := (Height - xScratch) div 2;
    xTextRect.Bottom := xTextRect.Top + xScratch;
  end
  else
  begin
    xTextRect.Top := 0;
    xTextRect.Bottom := Height;
  end;

  //%% Canvas.TextRect(xTextRect, xTextRect.Left, xTextRect.Top, DisableMessage);
  Windows.TextOutW(Canvas.Handle, xTextRect.Left, xTextRect.Top, PWideChar(DisableMessage),
    Length(DisableMessage)); //%%
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.PaintField(aFieldId: Integer);
begin

  case fDisplayMode of
    dmNone, dmSCValues, dmCalculateSCValues:
      mSelectionLayer.PaintField(aFieldId);

    dmValues, dmColor, dmDots:
      begin
        mSelectionLayer.PaintField(aFieldId);
        if Assigned(mDisplayLayer) then
          mDisplayLayer.PaintField(aFieldId);
      end;
  end;
  (* %%
    case fMode of

      mmDisplayData, mmDisplayColor, mmDisplayDots, mmSelectSCMembers:
        begin
          mSelectionLayer.PaintField(aFieldID);
          mDisplayLayer.PaintField(aFieldID);
        end;

      mmSelectSettings, mmSelectCutFields, mmSelectFFClusterFields,

      mmDisplaySCData, mmDisplayCalculateSCData:
        begin
          mSelectionLayer.PaintField(aFieldID);
        end;

    end;
  *)
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.PutActiveVisible(const aValue: Boolean);
begin
  if Assigned(mSelectionLayer) then
    mSelectionLayer.ActiveVisible := aValue;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.PutCurveColor(const aIndex: Integer;
  const aValue: TColor);
begin
  if Assigned(mCurveLayer) then
  begin
    with mCurveLayer do
      case aIndex of

        cChannelCurve:
          ChannelColor := aValue;

        cClusterCurve:
          ClusterColor := aValue;

        cSpliceCurve:
          SpliceColor := aValue;
      end;

    Invalidate;

  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.PutCurveStyle(const aIndex: Integer;
  const aValue: TPenStyle);
begin

  if Assigned(mCurveLayer) then
  begin
    with mCurveLayer do
      case aIndex of

        cChannelCurve:
          ChannelStyle := aValue;

        cClusterCurve:
          ClusterStyle := aValue;

        cSpliceCurve:
          SpliceStyle := aValue;
      end;

    Invalidate;

  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.PutCurveVisible(const aIndex: Integer;
  const aValue: Boolean);
begin

  if Assigned(mCurveLayer) then
  begin
    with mCurveLayer do
      case aIndex of

        cChannelCurve:
          ChannelVisible := aValue;

        cClusterCurve:
          ClusterVisible := aValue;

        cSpliceCurve:
          SpliceVisible := aValue;
      end;

    Invalidate;

  end;

end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.PutYMSettings(var aSettings: TYarnSettingsRec);
begin

  if MatrixType = mtSiro then
  begin
    (*%%%

     if (aSettings.additional.configA and cCCAFFSensorActiveBit) =
       cCCAFFSensorActiveBit then
     begin

       Enabled := True;
       if (aSettings.additional.configB and cCCBFFBDSensorActiveBit) =
         cCCBFFBDSensorActiveBit then
         MatrixSubType := mstSiroBD
       else
       begin
         if xMachineAttributes.IsSensingHeadFType then
           MatrixSubType := mstSiroF
         else
           MatrixSubType := mstSiroH;
       end;

     end
     else
     begin
       if xMachineAttributes.IsFFSensingHead then
         DisableMessage := Translate('(40)Fremdfaser-Reinigung ausgeschaltet')
       else
         DisableMessage := Translate('(40)Fremdfaser-Reinigung nicht verfuegbar');
       Enabled := False;
     end;
     *)
    SetFieldStateTable(aSettings.FFDarkClear, aSettings.FFDarkCluster.ClusterFFClear);
  end
  else if MatrixType = mtColor then
  begin

    SetFieldStateTable(aSettings.FFDarkClear, aSettings.FFBrightClear,
      aSettings.FFDarkCluster.ClusterFFClear, aSettings.FFBrightCluster.ClusterFFClear);
  end
  else
  begin
    Enabled := True;
    if Assigned(mCurveLayer) then
      mCurveLayer.PutYMSettings(aSettings);

    SetFieldStateTable(aSettings.classClear);
  end;

  Invalidate;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.Resize;
begin
  inherited;
  if Assigned(mCurveLayer) then
    mCurveLayer.CustomResize;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetDefaultColor;
begin

  if Assigned(mCurveLayer) then
    mCurveLayer.SetDefaultColor;

  if Assigned(mSelectionLayer) then
    mSelectionLayer.SetDefaultColor;

  if Assigned(mDisplayLayer) then
    mDisplayLayer.SetDefaultColor;

end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetDisableMessage(const aValue: WideString);
begin
  fDisableMessage := aValue;
  Invalidate;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetDisplayMode(const aValue: TDisplayMode);
begin

  fDisplayMode := aValue;

  if Assigned(mDisplayLayer) then
  begin
    mDisplayLayer.Mode := fDisplayMode;
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetDotsColor(const aValue: TColor);
begin

  if Assigned(mDisplayLayer) then
    mDisplayLayer.DotsColor := aValue;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetEnabled(const aValue: Boolean);
begin
  if aValue <> fEnabled then
  begin
    fEnabled := aValue;

    Invalidate;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetFieldColor(aFieldId: Integer;
  const aValue: TColor);
begin

  if Assigned(mDisplayLayer) then
    mDisplayLayer.FieldColor[aFieldId] := aValue;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetFieldDots(aFieldId: Integer;
  const aValue: Integer);
begin

  if Assigned(mDisplayLayer) then
    mDisplayLayer.FieldDots[aFieldId] := aValue;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetFieldState(aFieldId: Integer; aState: Boolean);
begin
  if mSelectionLayer <> nil then
  begin
    mSelectionLayer.FieldState[aFieldId] := aState;
    PaintField(aFieldId);
    //    if Assigned(mCurveLayer) then
    //      mCurveLayer.CustomPaint;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetFieldStateTable(
  const aClassClear: T128ClassFields);
begin
  if Assigned(mSelectionLayer) then
  begin
    mSelectionLayer.SetFieldStateTable(aClassClear);
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetFieldStateTable(
  const aFFClass, aFFCluster: T64ClassFields);
begin
  if Assigned(mSelectionLayer) then
  begin
    mSelectionLayer.SetFieldStateTable(aFFClass, aFFCluster);
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetFieldStateTable(const aDarkClass, aBrightClass,
  aDarkCluster, aBrightCluster: T64ClassFields);
begin
  if Assigned(mSelectionLayer) then
  begin
    mSelectionLayer.SetFieldStateTable(aDarkClass, aBrightClass,
      aDarkCluster, aBrightCluster);
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetMode(aMode: TMatrixMode);
var
  xType: TMatrixType;
begin

  xType := MatrixType;
  fMode := aMode;

  if mScaleLayer = nil then
  begin
    mScaleLayer := TScaleLayer.Create(self);
    //    mScaleLayer.Canvas := Canvas;
  end;

  if mDisplayLayer = nil then
    mDisplayLayer := TDisplayLayer.Create(self);
  with mDisplayLayer do
  begin
    MatrixType := xType;
    Mode := fDisplayMode;
  end;

  case aMode of

    mmSelectSettings, mmSelectCutFields, mmSelectFFClusterFields:
      begin

        if mSelectionLayer = nil then
        begin
          mSelectionLayer := TSelectionLayer.Create(self);
          mSelectionLayer.MatrixType := xType;
        end;
        with mSelectionLayer do
        begin
          if aMode = mmSelectFFClusterFields then
          begin
            if ((xType = mtSiro) and (self.MatrixSubType <> mstSiroBD)) or
              (xType = mtColor) then
              SelectionMode := smFFClusterField
            else
            begin
              fMode := mmSelectCutFields;
              SelectionMode := smCutField;
            end;
          end
          else
            SelectionMode := smCutField;

        end;

        if Assigned(mCurveLayer) then
        begin
          // Temporary for Release because Curve draging is not finished yet
          if aMode = mmSelectSettings then
            mCurveLayer.SelectCurve := True
          else
            mCurveLayer.SelectCurve := False;
        end;

        ShowHint := False;
        self.MatrixType := xType;
      end;
    (* %%
        mmDisplayCalculateSCData,
          mmDisplaySCData,
          mmDisplayData,
          mmDisplayColor,
          mmDisplayDots:
    *)
    mmDisplayData:
      begin
        if mSelectionLayer = nil then
        begin
          mSelectionLayer := TSelectionLayer.Create(self);
          mSelectionLayer.MatrixType := xType;
        end;
        with mSelectionLayer do
        begin
          SelectionMode := smCutField;

        end;
        //      mSelectionLayer.Free;
        //      mSelectionLayer := nil;

        if Assigned(mCurveLayer) then
          mCurveLayer.SelectCurve := False;

        ShowHint := False;
        self.MatrixType := xType;
      end;

    mmSelectSCMembers:
      begin
        if mSelectionLayer = nil then
          mSelectionLayer := TSelectionLayer.Create(self);
        with mSelectionLayer do
        begin
          SelectionMode := smSCMemberField;
          MatrixType := xType;
        end;

        if mDisplayLayer = nil then
          mDisplayLayer := TDisplayLayer.Create(self);
        with mDisplayLayer do
        begin
          MatrixType := self.MatrixType;
        end;

        if Assigned(mCurveLayer) then
          mCurveLayer.SelectCurve := False;

        ShowHint := True;
        self.MatrixType := xType;
      end;

  else
    fMode := mmSelectSettings;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetStateColor(const aIndex: Integer;
  const aValue: TColor);
begin
  if Assigned(mSelectionLayer) then
    case aIndex of

      cCutActive:
        mSelectionLayer.ActiveColor := aValue;

      cCutPassive:
        mSelectionLayer.InactiveColor := aValue;
    end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetSubField(const aIndex, aValue: Integer);
begin
  case aIndex of

    cXAxis:
      begin
        inherited SubFieldX := aValue;
        if Assigned(mCurveLayer) then
          mCurveLayer.SetSampleBufferLength;
      end;

    cYAxis:
      inherited SubFieldY := aValue;
  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetSubType(aType: TMatrixSubType);
begin
  inherited MatrixSubType := aType;

  if (aType = mstSiroBD) and (MatrixMode = mmSelectFFClusterFields) then
    MatrixMode := mmSelectCutFields;
  if Assigned(mSelectionLayer) then
    mSelectionLayer.MatrixSubType := aType;

end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetType(aType: TMatrixType);
var
  xRepaint: Boolean;
begin

  if aType <> inherited MatrixType then
  begin
    inherited MatrixType := aType;
    xRepaint := False;

    if Assigned(mSelectionLayer) then
    begin
      mSelectionLayer.MatrixType := aType;
      xRepaint := True;
    end;

    if mDisplayLayer <> nil then
    begin
      mDisplayLayer.MatrixType := aType;
      xRepaint := True;
    end;

    if aType = mtShortLongThin then
    begin
      if mCurveLayer = nil then
      begin
        mCurveLayer := TCurveLayer.Create(self);
        mCurveLayer.OnParameterChange := DoParameterChange;
      end;

      ValidateCuts := False;
      ValidateDefects := True;

      ChannelVisible := True;
      ClusterVisible := False;
      SpliceVisible := False;
      ActiveVisible := True;
      xRepaint := True;
    end
    else
    begin
      if aType = mtSplice then
      begin
        if mCurveLayer = nil then
        begin
          mCurveLayer := TCurveLayer.Create(self);
          mCurveLayer.OnParameterChange := DoParameterChange;
        end;

        ValidateCuts := False;
        ValidateDefects := True;

        ChannelVisible := False;
        ClusterVisible := False;
        SpliceVisible := True;
        ActiveVisible := False;
        xRepaint := True;
      end
      else
      begin
        mCurveLayer.Free;
        mCurveLayer := nil;


        ValidateCuts := True;
        ValidateDefects := True;
        ActiveVisible := True;
      end;
    end;

    if xRepaint then Invalidate;

  end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetValue(aFieldId, aIndex: Integer; aValue: Real);
begin
  if mDisplayLayer <> nil then
    with mDisplayLayer do
      case aIndex of
        cCuts:
          begin
            if fValidateCuts and not FieldState[aFieldId] then
              Cuts[aFieldId] := 0
            else
              Cuts[aFieldId] := aValue;
          end;

        cDefects:
          begin
            if fValidateDefects and FieldState[aFieldId] then
              Defects[aFieldId] := 0
            else
              Defects[aFieldId] := aValue;
          end;
      end;

  Invalidate;
  //  PaintField(aFieldID);
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetValueColor(const aIndex: Integer;
  const aValue: TColor);
begin

  if Assigned(mDisplayLayer) then
    case aIndex of

      cCuts:
        mDisplayLayer.CutsColor := aValue;

      cDefects:
        mDisplayLayer.CutsColor := aValue;
    end;
end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.SetZeroLimit(const aValue: Double);
begin
  if Assigned(mDisplayLayer) then
    mDisplayLayer.ZeroLimit := aValue

end;
//------------------------------------------------------------------------------

procedure TQualityMatrix.UpdateSuperClassHint;
var
  i: Integer;
  xCuts, xDefects: Real;
  xFormat: TFormatTextRec;
begin
  if fMode = mmSelectSCMembers then
  begin
    xCuts := 0;
    xDefects := 0;
    for i := 0 to TotFields - 1 do
    begin
      if FieldState[i] = True then
      begin
        xCuts := xCuts + Cuts[i];
        xDefects := xDefects + Defects[i];
      end;
    end;

    xFormat := DefectsFormat[0];
    with xFormat do
      if xDefects <> 0 then
        Hint := preText + Format(formatText, [xDefects]) + postText + '   '
      else
        Hint := '';

    xFormat := CutsFormat[0];
    with xFormat do
      if xCuts <> 0 then Hint := Hint + preText + Format(formatText, [xCuts]) + postText;
  end;

end;
//------------------------------------------------------------------------------

end.

