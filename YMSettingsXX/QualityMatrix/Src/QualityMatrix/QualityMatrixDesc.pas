(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QualityMatrixDesc.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr | Initial Release
|=========================================================================================*)
unit QualityMatrixDesc;

interface

uses
  Windows, Graphics, QualityMatrixDef;

const
  _Version_         = '$Revision: 3 $';
  _Date_            = '$Modtime: 3/25/04 10:50a $';
  // $NoKeywords: $

//type
//------------------------------------------------------------------------------
// Definition of Class Fields
//------------------------------------------------------------------------------
  //...........................................................................
const

//------------------------------------------------------------------------------
// Short Long Thin Matrix definition
//------------------------------------------------------------------------------
  //...........................................................................

  cTotSLTFields = 128;
  cTotSLTRoughClasses = 23;
  cTotSLTHorLines = 23;
  cTotSLTVertLines = 27;
  cTotSLTXScales = 14;
  cTotSLTYScales = 18;
  cTotSLTCaptions = 1;

  cNSLXLeftScale = 0;
  cNSLXRightScale = 13;
  cNSLYTopScale = 0;
  cNSLYBottomScale = 12;

  cThinXLeftScale = 4;
  cThinXRightScale = 13;
  cThinYTopScale = 13;
  cThinYBottomScale = 17;

  cSLTYAxisAlign = TItemAlignement(iaCenter);
  cSLTXAxisAlign = TItemAlignement(iaCenter);

  cSLTXOrgL = 2;
  cSLTYOrgL = 0;

  cSLTXOrgA = 1;
  cSLTYOrgA = cSLTYOrgL + 34;
  //...........................................................................

//------------------------------------------------------------------------------
// SLT Field definition
//------------------------------------------------------------------------------
  //...........................................................................
{(*}

  sLTFields: array[0..cTotSLTFields-1] of TFieldRec = (
  ( // field 0
    top:					cSLTYOrgL + 14;
    left:					cSLTXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
 ( // field 1
   top:					cSLTYOrgL + 12;
     left:					cSLTXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 2
   top:					cSLTYOrgL + 10;
     left:					cSLTXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 3
   top:					cSLTYOrgL + 8;
     left:					cSLTXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 4
   top:					cSLTYOrgL + 6;
     left:					cSLTXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:  nil;),
   ( // field 5
   top:					cSLTYOrgL + 4;
     left:					cSLTXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 6
   top:					cSLTYOrgL + 2;
     left:					cSLTXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 7
   top:					cSLTYOrgL + 0;
     left:					cSLTXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 8
   top:					cSLTYOrgL + 14;
     left:					cSLTXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:  nil;),
   ( // field 9
   top:					cSLTYOrgL + 12;
     left:					cSLTXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 10
   top:					cSLTYOrgL + 10;
     left:					cSLTXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 11
   top:					cSLTYOrgL + 8;
     left:					cSLTXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 12
   top:					cSLTYOrgL + 6;
     left:					cSLTXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 13
   top:					cSLTYOrgL + 4;
     left:					cSLTXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 14
   top:					cSLTYOrgL + 2;
     left:					cSLTXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 15
   top:					cSLTYOrgL + 0;
     left:					cSLTXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 16
   top:					cSLTYOrgL + 14;
     left:					cSLTXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 17
   top:					cSLTYOrgL + 12;
     left:					cSLTXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 18
   top:					cSLTYOrgL + 10;
     left:					cSLTXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 19
   top:					cSLTYOrgL + 8;
     left:					cSLTXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 20
   top:					cSLTYOrgL + 6;
     left:					cSLTXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:  nil;),
    ( // field 21
   top:					cSLTYOrgL + 4;
     left:					cSLTXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 22
    top:					cSLTYOrgL + 2;
     left:					cSLTXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 23
   top:					cSLTYOrgL + 0;
     left:					cSLTXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 24
   top:					cSLTYOrgL + 14;
     left:					cSLTXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 25
   top:					cSLTYOrgL + 12;
     left:					cSLTXOrgL + 6;
     width:				0;
      height:				0;
      pBackground:   nil;),
    ( // field 26
   top:					cSLTYOrgL + 10;
     left:					cSLTXOrgL + 6;
     width:				0;
     height:				0;
       pBackground:   nil;),
  ( // field 27
   top:					cSLTYOrgL + 8;
     left:					cSLTXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 28
   top:					cSLTYOrgL + 6;
     left:					cSLTXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 29
   top:					cSLTYOrgL + 4;
     left:					cSLTXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 30
   top:					cSLTYOrgL + 2;
     left:					cSLTXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 31
   top:					cSLTYOrgL + 0;
     left:					cSLTXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 32
   top:					cSLTYOrgL + 14;
     left:					cSLTXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 33
   top:					cSLTYOrgL + 12;
     left:					cSLTXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 34
   top:					cSLTYOrgL + 10;
     left:					cSLTXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 35
   top:					cSLTYOrgL + 8;
     left:					cSLTXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 36
   top:					cSLTYOrgL + 6;
     left:					cSLTXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 37
   top:					cSLTYOrgL + 4;
     left:					cSLTXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 38
   top:					cSLTYOrgL + 2;
     left:					cSLTXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 39
   top:					cSLTYOrgL + 0;
     left:					cSLTXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 40
   top:					cSLTYOrgL + 14;
     left:					cSLTXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 41
   top:					cSLTYOrgL + 12;
     left:					cSLTXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 42
   top:					cSLTYOrgL + 10;
     left:					cSLTXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 43
   top:					cSLTYOrgL + 8;
     left:					cSLTXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 44
   top:					cSLTYOrgL + 6;
     left:					cSLTXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 45
   top:					cSLTYOrgL + 4;
     left:					cSLTXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 46
   top:					cSLTYOrgL + 2;
     left:					cSLTXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 47
   top:					cSLTYOrgL + 0;
     left:					cSLTXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 48
   top:					cSLTYOrgL + 14;
     left:					cSLTXOrgL + 12;
     width:				4;
     height:				0;
      pBackground:  nil;),
   ( // field 49
   top:					cSLTYOrgL + 12;
     left:					cSLTXOrgL + 12;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 50
   top:					cSLTYOrgL + 10;
     left:					cSLTXOrgL + 12;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 51
   top:					cSLTYOrgL + 8;
     left:					cSLTXOrgL + 12;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 52
   top:					cSLTYOrgL + 6;
     left:					cSLTXOrgL + 12;
     width:				4;
     height:				0;
      pBackground:  nil;),
   ( // field 53
   top:					cSLTYOrgL + 4;
     left:					cSLTXOrgL + 12;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 54
   top:					cSLTYOrgL + 2;
     left:					cSLTXOrgL + 12;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 55
   top:					cSLTYOrgL + 0;
     left:					cSLTXOrgL + 12;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 56
   top:					cSLTYOrgL + 14;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 57
   top:					cSLTYOrgL + 12;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 58
   top:					cSLTYOrgL + 10;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 59
   top:					cSLTYOrgL + 8;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 60
   top:					cSLTYOrgL + 6;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 61
   top:					cSLTYOrgL + 4;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 62
   top:					cSLTYOrgL + 2;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 63
   top:					cSLTYOrgL + 0;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 64
   top:					cSLTYOrgL + 31;
     left:					cSLTXOrgL + 8;
     width:				4;
     height:				0;
      pBackground:  nil;),
   ( // field 65
   top:					cSLTYOrgL + 29;
     left:					cSLTXOrgL + 8;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 66
   top:					cSLTYOrgL + 27;
     left:					cSLTXOrgL + 8;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 67
   top:					cSLTYOrgL + 25;
     left:					cSLTXOrgL + 8;
     width:				4;
     height:				0;
       pBackground:   nil;),
   ( // field 68
   top:					cSLTYOrgL + 22;
     left:					cSLTXOrgL + 8;
     width:				4;
     height:				0;
      pBackground:  nil;),
    ( // field 69
   top:					cSLTYOrgL + 20;
     left:					cSLTXOrgL + 8;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 70
   top:					cSLTYOrgL + 18;
     left:					cSLTXOrgL + 8;
     width:				4;
     height:				0;
      pBackground:   nil;),
   ( // field 71
   top:					cSLTYOrgL + 16;
     left:					cSLTXOrgL + 8;
     width:				4;
      height:				0;
      pBackground:   nil;),
   ( // field 72
   top:					cSLTYOrgL + 31;
     left:					cSLTXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 73
   top:					cSLTYOrgL + 29;
     left:					cSLTXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 74
   top:					cSLTYOrgL + 27;
     left:					cSLTXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 75
   top:					cSLTYOrgL + 25;
     left:					cSLTXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 76
   top:					cSLTYOrgL + 22;
     left:					cSLTXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 77
   top:					cSLTYOrgL + 20;
     left:					cSLTXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 78
   top:					cSLTYOrgL + 18;
     left:					cSLTXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 79
   top:					cSLTYOrgL + 16;
     left:					cSLTXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 80
   top:					cSLTYOrgL + 31;
     left:					cSLTXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 81
   top:					cSLTYOrgL + 29;
     left:					cSLTXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 82
    top:					cSLTYOrgL + 27;
     left:					cSLTXOrgL + 14;
     width:				0;
      height:				0;
      pBackground:   nil;),
   ( // field 83
   top:					cSLTYOrgL + 25;
     left:					cSLTXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 84
   top:					cSLTYOrgL + 22;
     left:					cSLTXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 85
   top:					cSLTYOrgL + 20;
     left:					cSLTXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 86
   top:					cSLTYOrgL + 18;
     left:					cSLTXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 87
   top:					cSLTYOrgL + 16;
     left:					cSLTXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 88
   top:					cSLTYOrgL + 31;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 89
   top:					cSLTYOrgL + 29;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 90
   top:					cSLTYOrgL + 27;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 91
   top:					cSLTYOrgL + 25;
    left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 92
   top:					cSLTYOrgL + 22;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 93
   top:					cSLTYOrgL + 20;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 94
   top:					cSLTYOrgL + 18;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 95
   top:					cSLTYOrgL + 16;
     left:					cSLTXOrgL + 16;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 96
   top:					cSLTYOrgL + 31;
     left:					cSLTXOrgL + 18;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 97
   top:					cSLTYOrgL + 29;
     left:					cSLTXOrgL + 18;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 98
   top:					cSLTYOrgL + 27;
     left:					cSLTXOrgL + 18;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 99
   top:					cSLTYOrgL + 25;
     left:					cSLTXOrgL + 18;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 100
   top:					cSLTYOrgL + 22;
     left:					cSLTXOrgL + 18;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 101
   top:					cSLTYOrgL + 20;
     left:					cSLTXOrgL + 18;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 102
   top:					cSLTYOrgL + 18;
     left:					cSLTXOrgL + 18;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 103
   top:					cSLTYOrgL + 16;
     left:					cSLTXOrgL + 18;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 104
   top:					cSLTYOrgL + 31;
     left:					cSLTXOrgL + 20;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 105
   top:					cSLTYOrgL + 29;
     left:					cSLTXOrgL + 20;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 106
   top:					cSLTYOrgL + 27;
     left:					cSLTXOrgL + 20;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 107
   top:					cSLTYOrgL + 25;
     left:					cSLTXOrgL + 20;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 108
   top:					cSLTYOrgL + 22;
     left:					cSLTXOrgL + 20;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 109
   top:					cSLTYOrgL + 20;
     left:					cSLTXOrgL + 20;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 110
   top:					cSLTYOrgL + 18;
     left:					cSLTXOrgL + 20;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 111
   top:					cSLTYOrgL + 16;
     left:					cSLTXOrgL + 20;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 112
   top:					cSLTYOrgL + 31;
     left:					cSLTXOrgL + 22;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 113
   top:					cSLTYOrgL + 29;
     left:					cSLTXOrgL + 22;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 114
   top:					cSLTYOrgL + 27;
     left:					cSLTXOrgL + 22;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 115
   top:					cSLTYOrgL + 25;
     left:					cSLTXOrgL + 22;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 116
   top:					cSLTYOrgL + 22;
     left:					cSLTXOrgL + 22;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 117
   top:					cSLTYOrgL + 20;
     left:					cSLTXOrgL + 22;
     width:				0;
     height:				0;
      pBackground:   nil;),
  ( // field 118
  top:					cSLTYOrgL + 18;
   left:					cSLTXOrgL + 22;
    width:				0;
    height:				0;
    pBackground:   nil;),
  ( // field 119
  top:					cSLTYOrgL + 16;
   left:					cSLTXOrgL + 22;
    width:				0;
    height:				0;
    pBackground:   nil;),
  ( // field 120
  top:					cSLTYOrgL + 31;
    left:					cSLTXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 121
  top:					cSLTYOrgL + 29;
    left:					cSLTXOrgL + 24;
    width:				0;
   height:				0;
    pBackground:   nil;),
  ( // field 122
  top:					cSLTYOrgL + 27;
   left:					cSLTXOrgL + 24;
    width:				0;
   height:				0;
    pBackground:   nil;),
 ( // field 123
   top:					cSLTYOrgL + 25;
  left:					cSLTXOrgL + 24;
   width:				0;
   height:				0;
    pBackground:   nil;),
  ( // field 124
  top:					cSLTYOrgL + 22;
    left:					cSLTXOrgL + 24;
    width:				0;
   height:				0;
    pBackground:  nil;),
  ( // field 125
  top:					cSLTYOrgL + 20;
   left:					cSLTXOrgL + 24;
    width:				0;
   height:				0;
    pBackground:   nil;),
  ( // field 126
  top:					cSLTYOrgL + 18;
   left:					cSLTXOrgL + 24;
   width:				0;
   height:				0;
    pBackground:   nil;),
  ( // field 127
  top:					cSLTYOrgL + 16;
   left:					cSLTXOrgL + 24;
   width:				0;
   height:				0;
    pBackground:   nil;)
  );

  //...........................................................................
  sLTRaster:  TMatrixRasterRec = (// raster
   width:				28;
   height:				35;);
  sLTField:   TFieldsRec = (
    fieldWidth:   2;
    fieldHeight:	2;
     background:(
       color:      clQualityBackground;
       style:      bsSolid;);
  table:          @sLTFields;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SLT Scale Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  sLTHorLines: array[0..cTotSLTHorLines-1] of TLineRec = (
    (	// horizontal line 0
      pos:					cSLTYOrgL + 2;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 1
      pos:					cSLTYOrgL + 4;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 1 rough
      pos:					cSLTYOrgL + 4;
      start:				cSLTXOrgL + 2;
      stop:					cSLTXOrgL + 16;
    pPen:					@cLinePenRough;),
   (	// horizontal line 2
      pos:					cSLTYOrgL + 6;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 3
      pos:					cSLTYOrgL + 8;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 3 rough
      pos:					cSLTYOrgL + 8;
      start:				cSLTXOrgL + 2;
      stop:					cSLTXOrgL + 16;
    pPen:					@cLinePenRough;),
   (	// horizontal line 4
      pos:					cSLTYOrgL + 10;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 5
      pos:					cSLTYOrgL + 12;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 5 rough
      pos:					cSLTYOrgL + 12;
      start:				cSLTXOrgL + 2;
      stop:					cSLTXOrgL + 16;
    pPen:					@cLinePenRough;),
     (	// horizontal line 6
      pos:					cSLTYOrgL + 14;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 7
      pos:					cSLTYOrgL + 16;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
   (	// horizontal line 7 rough
      pos:					cSLTYOrgL + 16;
      start:				cSLTXOrgL + 2;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 8
      pos:					cSLTYOrgL + 18;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
   (	// horizontal line 9
      pos:					cSLTYOrgL + 20;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
   (	// horizontal line 10
      pos:					cSLTYOrgL + 22;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
   (	// horizontal line 10 rough
      pos:					cSLTYOrgL + 22;
      start:				cSLTXOrgL + 16;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 11
      pos:					cSLTYOrgL + 24;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
   (	// horizontal line 12
      pos:					cSLTYOrgL + 25;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
   (	// horizontal line 13
      pos:					cSLTYOrgL + 27;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
   (	// horizontal line 13 rough
      pos:					cSLTYOrgL + 27;
      start:				cSLTXOrgL + 18;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 14
      pos:					cSLTYOrgL + 29;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
 (	// horizontal line 15
   pos:					cSLTYOrgL + 31;
   start:				cSLTXOrgL + 8;
   stop:					cSLTXOrgL + 26;
  pPen:					nil;),
 (	// horizontal line 15 rough
   pos:					cSLTYOrgL + 31;
   start:				cSLTXOrgL + 18;
   stop:					cSLTXOrgL + 26;
  pPen:					@cLinePenRough;)
  );
  //...........................................................................

  sLTVertLines: array[0..cTotSLTVertLines-1] of TLineRec =(
  (	// vertical line 0
     pos:					cSLTXOrgL + 0;
     start:				cSLTYOrgL + 0;
     stop:					cSLTYOrgL + 16;
   pPen:					nil;),
(*				(	// vertical line 1 fine
      pos:					cSLTXOrgL + 2;
       start:				cSLTYOrgL + 0;
       stop:					cSLTYOrgL + 16;
     pPen:					nil;), *)
    (// vertical line 1 rough
     pos:					cSLTXOrgL + 2;
    start:				cSLTYOrgL + 0;
     stop:					cSLTYOrgL + 16;
   pPen:					@cLinePenRough;),
(*				(	// vertical line 2 fine
       pos:					cSLTXOrgL + 4;
       start:				cSLTYOrgL + 0;
       stop:					cSLTYOrgL + 16;
     pPen:					nil;), *)
  (	// vertical line 2 rough
     pos:					cSLTXOrgL + 4;
    start:				cSLTYOrgL + 0;
     stop:					cSLTYOrgL + 16;
   pPen:					@cLinePenRough;),
  (	// vertical line 3
      pos:					cSLTXOrgL + 6;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 16;
    pPen:					nil;),
   (	// vertical line 4 fine
      pos:					cSLTXOrgL + 8;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 4 rough
      pos:					cSLTXOrgL + 8;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 16;
    pPen:					@cLinePenRough;),
   (	// vertical line 4 Thin
      pos:					cSLTXOrgL + 8;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					nil;),
   (	// vertical line 5
      pos:					cSLTXOrgL + 10;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 16;
    pPen:					nil;),
   (	// vertical line 6 fine
      pos:					cSLTXOrgL + 12;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 6 rough
      pos:					cSLTXOrgL + 12;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 16;
    pPen:					@cLinePenRough;),
   (	// vertical line 6 Thin
      pos:					cSLTXOrgL + 12;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					nil;),
   (	// vertical line 7
      pos:					cSLTXOrgL + 14;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 7 Thin
      pos:					cSLTXOrgL + 14;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					nil;),
   (	// vertical line 8 fine
      pos:					cSLTXOrgL + 16;
      start:				cSLTYOrgL + 22;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 8 rough
      pos:					cSLTXOrgL + 16;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 22;
    pPen:					@cLinePenRough;),
   (	// vertical line 8 Thin
      pos:					cSLTXOrgL + 16;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					nil;),
   (	// vertical line 9
      pos:					cSLTXOrgL + 18;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 9 Thin fine
      pos:					cSLTXOrgL + 18;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 27;
    pPen:					nil;),
   (	// vertical line 9 Thin rough
      pos:					cSLTXOrgL + 18;
      start:				cSLTYOrgL + 27;
      stop:					cSLTYOrgL + 33;
    pPen:					@cLinePenRough;),
   (	// vertical line 10
      pos:					cSLTXOrgL + 20;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 10 Thin
      pos:					cSLTXOrgL + 20;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					nil;),
   (	// vertical line 11 fine
      pos:					cSLTXOrgL + 22;
      start:				cSLTYOrgL + 22;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 11 rough
      pos:					cSLTXOrgL + 22;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 22;
    pPen:					@cLinePenRough;),
  (	// vertical line 11 Thin fine
     pos:					cSLTXOrgL + 22;
    start:				cSLTYOrgL + 25;
     stop:					cSLTYOrgL + 27;
      pPen:					nil;),
    (	// vertical line 11 Thin rough
     pos:					cSLTXOrgL + 22;
    start:				cSLTYOrgL + 27;
     stop:					cSLTYOrgL + 33;
   pPen:					@cLinePenRough;),
    (	// vertical line 12
     pos:					cSLTXOrgL + 24;
    start:				cSLTYOrgL + 16;
     stop:					cSLTYOrgL + 24;
      pPen:					nil;),
    (	// vertical line 12 Thin
     pos:					cSLTXOrgL + 24;
    start:				cSLTYOrgL + 25;
     stop:					cSLTYOrgL + 33;
   pPen:					nil;)
  );
  //...........................................................................

  slTXScales: array[0..cTotSLTXScales-1] of TTextRec = (
    ( // X Axis Scale 0
      pFont:        @cFontScaleEnds;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 0;
      yPos:         cSLTYOrgA;
      text:         '0.0';),
    ( // X Axis Scale 1
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 2;
      yPos:         cSLTYOrgA;
      text:         '0.5';),
    ( // X Axis Scale 2
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 4;
      yPos:         cSLTYOrgA;
      text:         '1.0';),
    ( // X Axis Scale 3
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 6;
      yPos:         cSLTYOrgA;
      text:         '1.5';),
    ( // X Axis Scale 4
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 8;
      yPos:         cSLTYOrgA;
      text:         '2.0';),
    ( // X Axis Scale 5
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 10;
      yPos:         cSLTYOrgA;
      text:         '3.0';),
    ( // X Axis Scale 6
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 12;
      yPos:         cSLTYOrgA;
      text:         '4.0';),
    ( // X Axis Scale 7
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 14;
      yPos:         cSLTYOrgA;
      text:         '6.0';),
    ( // X Axis Scale 8
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 16;
      yPos:         cSLTYOrgA;
      text:         '8.0';),
    ( // X Axis Scale 9
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 18;
      yPos:         cSLTYOrgA;
      text:         '12.0';),
    ( // X Axis Scale 10
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 20;
      yPos:         cSLTYOrgA;
      text:         '20.0';),
    ( // X Axis Scale 11
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 22;
      yPos:         cSLTYOrgA;
      text:         '32.0';),
    ( // X Axis Scale 12
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 24;
      yPos:         cSLTYOrgA;
      text:         '70.0';),
    ( // X Axis Scale 13
      pFont:        @cFontScaleEnds;
      horAlign:     iaCenter;
      vertAlign:    cSLTXAxisAlign;
      xPos:         cSLTXOrgL + 26;
      yPos:         cSLTYOrgA;
      text:         '200.0';)
  );
  //...........................................................................

  sLTYScales: array[0..cTotSLTYScales-1] of TTextRec = (
    ( // Y Axis Scale 0
      pFont:        @cFontScaleEnds;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 0;
      text:         '10.0';),
    ( // Y Axis Scale 1
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 2;
      text:         '7.00';),
    ( // Y Axis Scale 2
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 4;
      text:         '5.00';),
    ( // Y Axis Scale 3
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 6;
      text:         '3.90';),
    ( // Y Axis Scale 4
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 8;
      text:         '3.20';),
    ( // Y Axis Scale 5
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 10;
      text:         '2.70';),
    ( // Y Axis Scale 6
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 12;
      text:         '2.30';),
    ( // Y Axis Scale 7
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 14;
      text:         '2.10';),
    ( // Y Axis Scale 8
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 16;
      text:         '1.80';),
    ( // Y Axis Scale 9
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 18;
      text:         '1.60';),
    ( // Y Axis Scale 10
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 20;
      text:         '1.45';),
    ( // Y Axis Scale 11
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 22;
      text:         '1.30';),
    ( // Y Axis Scale 12
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
//      vertAlign:     iaTop;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 24;
      text:         '1.20';),
    ( // Y Axis Scale 13
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
//      vertAlign:     iaBottom;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 25;
      text:         '0.83';),
    ( // Y Axis Scale 14
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 27;
      text:         '0.80';),
    ( // Y Axis Scale 15
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 29;
      text:         '0.75';),
    ( // Y Axis Scale 16
      pFont:        @cFontAxis;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 31;
      text:         '0.65';),
    ( // Y Axis Scale 17
      pFont:        @cFontScaleEnds;
      horAlign:     cSLTYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cSLTXOrgA;
      yPos:         cSLTYOrgL + 33;
      text:         '0.50';)
  );
  //...........................................................................

  sLTCaptions: array[0..cTotSLTCaptions-1] of TTextRec = (
    (
      pFont:        @cFontCaption;
      horAlign:     iaCenter;
      vertAlign:    iaTop;
      xPos:         cSLTXOrgL + 22;
      yPos:         cSLTYOrgL + 6;
      text:         'Garnfehler';)
  );
  //...........................................................................

  sLTScaleLayer:  TScaleLayerRec = (
   horLine:(
     pen: (
    width:				1;
     color:        clLine;
     mode:         pmCopy;
     style:        psSolid;
   );
   table:          @sLTHorLines;
    );
   vertLine: (
    pen: (
     width:				1;
     color:        clLine;
     mode:         pmCopy;
    style:         psSolid;
      );
    table:          @sLTVertLines;
    );
    xAxis:          @sLTXScales;
    yAxis:          @sLTYScales;
    title: (
      pFont:        @cFontTitle;
      horAlign:     iaRight;
      vertAlign:    iaCenter;
      xTextPos:     cSLTXOrgL;
      yTextPos:     1;
      barHight:     3;
      text: 'SLT Quality Matrix'
    );
    caption:        nil;
//    caption:          @sLTCaptions;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SLT Curve Layer definition
//------------------------------------------------------------------------------
  //...........................................................................
  sLTSamples: array[0..cTotSLTXScales-1] of Integer = (
    15, 15, 15, 15, 10, 10, 10, 10, 10, 10, 10, 10, 10, 0
  );
const
  visibleNSLTRange: TRect = (
    Left:   0;
    Top:    1000;
    Right:  2000;
    Bottom: 0;
  );

  sLTCurveLayer:  TCurveLayerRec = (
    numberOfSamplePerScale: 15;
//    numberOfSampleTbl:    @sLTSamples;
    visibleNSLRange:(
      Left:   0;
//      Left:   10;
      Top:    1000;
//      Top:    210;
      Right:  2000;
//      Right:  80;
      Bottom: 120;
//      Bottom: 130;
    );
    visibleTRange:(
      Left:   0;
      Top:    83;
      Right:  2000;
      Bottom: 50;
//    Left:   70;
//    Top:    75;
//    Right:  200;
//    Bottom: 65;
    );
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SLT Display Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  noMember: TSCMemberArray = (cSCDelimiter,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0);
  sLTRoughClasses:  array[0..cTotSLTRoughClasses-1] of TSuperClassRec = (
  ( // Rough Class 0
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 12;
      left:					cSLTXOrgL + 2;
      width:				2;
      height:				4;
      pBackground: nil );
    memberTbl: (8, 9, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 1
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 8;
      left:					cSLTXOrgL + 2;
      width:				2;
      height:				4;
      pBackground: nil );
    memberTbl: (10, 11, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 2
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 4;
      left:					cSLTXOrgL + 2;
      width:				2;
      height:				4;
      pBackground: nil );
    memberTbl: (12, 13, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 3
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL;
      left:					cSLTXOrgL + 2;
      width:				2;
      height:				4;
      pBackground: nil );
    memberTbl: (14, 15, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 4
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 12;
      left:					cSLTXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (16, 17, 24, 25, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 5
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 8;
      left:					cSLTXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (18, 19, 26, 27, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 6
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 4;
      left:					cSLTXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (20, 21, 28, 29, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 7
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL;
      left:					cSLTXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (22, 23, 30, 31, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 8
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 12;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (32, 33, 40, 41, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 9
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 8;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (34, 35, 42, 43, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 10
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 4;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (36, 37, 44, 45, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 11
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (38, 39, 46, 47, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 12
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 12;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (48, 49, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 13
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 8;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (50, 51, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 14
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 4;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (52, 53, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 15
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (54, 55, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 16
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL;
//      top:          cSLTYOrgL + 6;
      left:					cSLTXOrgL + 16;
      width:				2;
      height:				16;
//      height:				4;
      pBackground: nil );
    memberTbl: (56, 57, 58, 59, 60, 61, 62, 63, cSCDelimiter,0,0,0,0,0,0,0); ),
  ( // Rough Class 17
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 31;
      left:					cSLTXOrgL + 18;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (96, 104, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 18
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 27;
      left:					cSLTXOrgL + 18;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (97, 98, 105, 106, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 19
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 16;
      left:					cSLTXOrgL + 16;
      width:				6;
      height:				6;
      pBackground: nil );
    memberTbl: (93, 94, 95, 101, 102, 103, 109, 110,  111, cSCDelimiter,0,0,0,0,0,0); ),
  ( // Rough Class 20
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 31;
      left:					cSLTXOrgL + 22;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (112, 120, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 21
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 27;
      left:					cSLTXOrgL + 22;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (113, 114, 121, 122, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 22
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 16;
      left:					cSLTXOrgL + 22;
      width:				4;
      height:				6;
      pBackground: nil );
    memberTbl: (117, 118, 119, 125, 126, 127,cSCDelimiter,0, 0,0,0,0,0,0,0,0); )
  );
  //...........................................................................

 sLTDisplayLayer:  TDisplayLayerRec = (
   number:(
      cuts:(
        pFont:         @cFontCuts;
//          horAlign:     iaRight;
//          vertAlign:    iaTop;
        horAlign:     iaRight;
        vertAlign:    iaTop;
        background:   clNone;
        preText:      '';
        formatText:   '%.3g';
        postText:      '';
      );
      defects:(
        pFont:        @cFontDefects;
        horAlign:     iaLeft;
        vertAlign:    iaBottom;
        background:   clNone;
        preText:      '';
        formatText:   '%.3g';
        postText:     '';
      );
    );

    roughClassTbl: @sLTRoughClasses;
    defaultSuperClassTbl: nil;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SLT Selection Layer definition
//------------------------------------------------------------------------------
  //...........................................................................
  sLTSelectionLayer:  TSelectionLayerRec = (
    cutState:(
      pActiveColor:           @cCutOn;
      pPassiveColor:          nil;
    );
    fFClusterState:(
      pActiveColor:           nil;
      pPassiveColor:          nil;
    );
    sCMemberState:(
      pActiveColor:           @cSCMemberOn;
      pPassiveColor:          nil;
    );
  );

  cSLTMatrix:   TQualityMatrixDescRec = (
    pRaster:          @sLTRaster;
    pField:           @sLTField;
    pScaleLayer:      @sLTScaleLayer;
    pDisplayLayer:    @sLTDisplayLayer;
    pSelectionLayer:  @sLTSelectionLayer;
    pCurveLayer:      @sLTCurveLayer;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// Splice Matrix definiton based one Short Long Thin Matrix
//------------------------------------------------------------------------------
  //...........................................................................
  //...........................................................................
  //...........................................................................

  cTotSpliceRoughClasses = 26;
  cTotSpliceHorLines = 18;
  cTotSpliceVertLines = 21;


//------------------------------------------------------------------------------
// Splice Scale Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  spliceHorLines: array[0..cTotSpliceHorLines-1] of TLineRec = (
    (	// horizontal line 0
      pos:					cSLTYOrgL + 2;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 1
      pos:					cSLTYOrgL + 4;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 2
      pos:					cSLTYOrgL + 6;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 3
      pos:					cSLTYOrgL + 8;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 4
      pos:					cSLTYOrgL + 10;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 5
      pos:					cSLTYOrgL + 12;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 6 rough
      pos:					cSLTYOrgL + 12;
      start:				cSLTXOrgL + 4;
      stop:					cSLTXOrgL + 18;
    pPen:					@cLinePenRough;),
     (	// horizontal line 6
      pos:					cSLTYOrgL + 14;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 18;
    pPen:					nil;),
   (	// horizontal line 7
      pos:					cSLTYOrgL + 16;
      start:				cSLTXOrgL + 0;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
   (	// horizontal line 7 rough
      pos:					cSLTYOrgL + 16;
      start:				cSLTXOrgL + 4;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 8 rough
      pos:					cSLTYOrgL + 18;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 9 rough
      pos:					cSLTYOrgL + 20;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 10 rough
      pos:					cSLTYOrgL + 22;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 11 rough
      pos:					cSLTYOrgL + 24;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 12 rough
      pos:					cSLTYOrgL + 25;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 13 rough
      pos:					cSLTYOrgL + 27;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					@cLinePenRough;),
   (	// horizontal line 14
      pos:					cSLTYOrgL + 29;
      start:				cSLTXOrgL + 8;
      stop:					cSLTXOrgL + 26;
    pPen:					nil;),
 (	// horizontal line 15
   pos:					cSLTYOrgL + 31;
   start:				cSLTXOrgL + 8;
   stop:					cSLTXOrgL + 26;
  pPen:					nil;)
  );
  //...........................................................................

  spliceVertLines: array[0..cTotSpliceVertLines-1] of TLineRec =(
  (	// vertical line 0
     pos:					cSLTXOrgL + 0;
     start:				cSLTYOrgL + 0;
     stop:					cSLTYOrgL + 16;
   pPen:					nil;),
	(	// vertical line 1
      pos:					cSLTXOrgL + 2;
       start:				cSLTYOrgL + 0;
       stop:					cSLTYOrgL + 16;
     pPen:					nil;),
  (	// vertical line 2 rough
     pos:					cSLTXOrgL + 4;
    start:				cSLTYOrgL + 0;
     stop:					cSLTYOrgL + 16;
   pPen:					@cLinePenRough;),
  (	// vertical line 3
      pos:					cSLTXOrgL + 6;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 16;
    pPen:					nil;),
   (	// vertical line 4 rough
      pos:					cSLTXOrgL + 8;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 24;
    pPen:					@cLinePenRough;),
   (	// vertical line 4 Thin rough
      pos:					cSLTXOrgL + 8;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					@cLinePenRough;),
   (	// vertical line 5
      pos:					cSLTXOrgL + 10;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 16;
    pPen:					nil;),
   (	// vertical line 6 rough
      pos:					cSLTXOrgL + 12;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 24;
    pPen:					@cLinePenRough;),
   (	// vertical line 6 Thin rough
      pos:					cSLTXOrgL + 12;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					@cLinePenRough;),
   (	// vertical line 7
      pos:					cSLTXOrgL + 14;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 7 Thin
      pos:					cSLTXOrgL + 14;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					nil;),
   (	// vertical line 8 rough
      pos:					cSLTXOrgL + 16;
      start:				cSLTYOrgL + 0;
      stop:					cSLTYOrgL + 24;
    pPen:					@cLinePenRough;),
   (	// vertical line 8 Thin rough
      pos:					cSLTXOrgL + 16;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					@cLinePenRough;),
   (	// vertical line 9
      pos:					cSLTXOrgL + 18;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 9 Thin fine
      pos:					cSLTXOrgL + 18;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					nil;),
   (	// vertical line 10
      pos:					cSLTXOrgL + 20;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
   (	// vertical line 10 Thin
      pos:					cSLTXOrgL + 20;
      start:				cSLTYOrgL + 25;
      stop:					cSLTYOrgL + 33;
    pPen:					nil;),
   (	// vertical line 11 fine
      pos:					cSLTXOrgL + 22;
      start:				cSLTYOrgL + 16;
      stop:					cSLTYOrgL + 24;
    pPen:					nil;),
  (	// vertical line 11 Thin fine
     pos:					cSLTXOrgL + 22;
    start:				cSLTYOrgL + 25;
     stop:					cSLTYOrgL + 33;
      pPen:					nil;),
    (	// vertical line 12
     pos:					cSLTXOrgL + 24;
    start:				cSLTYOrgL + 16;
     stop:					cSLTYOrgL + 24;
      pPen:					nil;),
    (	// vertical line 12 Thin
     pos:					cSLTXOrgL + 24;
    start:				cSLTYOrgL + 25;
     stop:					cSLTYOrgL + 33;
   pPen:					nil;)
  );
  //...........................................................................

  spliceScaleLayer:  TScaleLayerRec = (
   horLine:(
     pen: (
    width:				1;
     color:        clLine;
     mode:         pmCopy;
     style:        psSolid;
   );
   table:          @spliceHorLines;
    );
   vertLine:(
    pen: (
     width:				1;
     color:        clLine;
     mode:         pmCopy;
    style:        psSolid;
      );
    table:          @spliceVertLines;
    );
    xAxis:            @sLTXScales;
    yAxis:            @sLTYScales;
    title: (
      pFont:        @cFontTitle;
      horAlign:     iaRight;
      vertAlign:    iaCenter;
      xTextPos:     cSLTXOrgL;
      yTextPos:     1;
      barHight:     3;
      text: 'Splice Quality Matrix'
    );
    caption:          nil;
//    caption:          @sLTCaptions;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// Splice Display Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  spliceRoughClasses:  array[0..cTotSpliceRoughClasses-1] of TSuperClassRec = (
  ( // Rough Class 0
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 12;
      left:					cSLTXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (16, 17, 24, 25, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 1
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL;
      left:					cSLTXOrgL + 4;
      width:				4;
      height:				12;
      pBackground: nil );
    memberTbl: (18, 19, 20, 21, 22, 23, 26, 27, 28, 29, 30, 31, cSCDelimiter,0,0,0); ),
  ( // Rough Class 2
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 27;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				6;
      pBackground: nil );
    memberTbl: (64, 65, 66, cSCDelimiter,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 3
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 25;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (67, cSCDelimiter,0,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 4
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 22;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (68, cSCDelimiter,0,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 5
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 20;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (69, cSCDelimiter,0,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 6
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 18;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (70, cSCDelimiter,0,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 7
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 16;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (71, cSCDelimiter,0,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 8
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 12;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (32, 33, 40, 41, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 9
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL;
      left:					cSLTXOrgL + 8;
      width:				4;
      height:				12;
      pBackground: nil );
    memberTbl: (34, 35, 36, 37, 38, 39, 42, 43, 44, 45, 46, 47, cSCDelimiter,0,0,0); ),
  ( // Rough Class 10
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 27;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				6;
      pBackground: nil );
    memberTbl: (72, 73, 74, 80, 81, 82, cSCDelimiter,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 11
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 25;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (75, 83, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 12
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 22;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (76, 84, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 13
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 20;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (77, 85, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 14
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 18;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (78, 86, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 15
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 16;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				2;
      pBackground: nil );
    memberTbl: (79, 87, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 16
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 12;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (48, 49, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 17
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL;
      left:					cSLTXOrgL + 12;
      width:				4;
      height:				12;
      pBackground: nil );
    memberTbl: (50, 51, 52, 53, 54, 55, cSCDelimiter,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 18
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 27;
      left:					cSLTXOrgL + 16;
      width:				10;
      height:				6;
      pBackground: nil );
    memberTbl: (88, 89, 90, 96, 97, 98, 104, 105, 106, 112, 113, 114, 120, 121, 122, cSCDelimiter); ),
  ( // Rough Class 19
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 25;
      left:					cSLTXOrgL + 16;
      width:				10;
      height:				2;
      pBackground: nil );
    memberTbl: (91, 99, 107, 115, 123, cSCDelimiter,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 20
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 22;
      left:					cSLTXOrgL + 16;
      width:				10;
      height:				2;
      pBackground: nil );
    memberTbl: (92, 100, 108, 116, 124, cSCDelimiter,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 21
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 20;
      left:					cSLTXOrgL + 16;
      width:				10;
      height:				2;
      pBackground: nil );
    memberTbl: (93, 101, 109, 117, 125, cSCDelimiter,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 22
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 18;
      left:					cSLTXOrgL + 16;
      width:				10;
      height:				2;
      pBackground: nil );
    memberTbl: (94, 102, 110, 118, 126, cSCDelimiter,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 23
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL + 16;
      left:					cSLTXOrgL + 16;
      width:				10;
      height:				2;
      pBackground: nil );
    memberTbl: (95, 103, 111, 119, 127, cSCDelimiter,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 24
    PNumber: nil;
    boarder: (
      top:          cSLTYOrgL + 12;
      left:					cSLTXOrgL + 16;
      width:				2;
      height:				4;
      pBackground: nil );
    memberTbl: (56, 57, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 25
    PNumber: nil;
    boarder: (
      top:	        cSLTYOrgL;
      left:					cSLTXOrgL + 16;
      width:				2;
      height:				12;
      pBackground: nil );
    memberTbl: (58, 59, 60, 61, 62,63, cSCDelimiter,0, 0,0,0,0,0,0,0,0); )
  );
  //...........................................................................

 spliceDisplayLayer:  TDisplayLayerRec = (
   number:(
      cuts:(
        pFont:         @cFontCuts;
//          horAlign:     iaRight;
//          vertAlign:    iaTop;
        horAlign:     iaRight;
        vertAlign:    iaTop;
        background:   clNone;
        preText:      '';
        formatText:   '%.3g';
        postText:      '';
      );
      defects:(
        pFont:        @cFontDefects;
        horAlign:     iaLeft;
        vertAlign:    iaBottom;
        background:   clNone;
        preText:      '';
        formatText:   '%.3g';
        postText:     '';
      );
    );

    roughClassTbl: @spliceRoughClasses;
    defaultSuperClassTbl: nil;
  );
  //...........................................................................


  cSpliceMatrix:   TQualityMatrixDescRec = (
    pRaster:          @sLTRaster;
    pField:           @sLTField;
    pScaleLayer:      @spliceScaleLayer;
    pDisplayLayer:    @spliceDisplayLayer;
    pSelectionLayer:  @sLTSelectionLayer;
    pCurveLayer:      @sLTCurveLayer;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SIRO F/H Matrix definition
//------------------------------------------------------------------------------
  //...........................................................................
  //...........................................................................
  //...........................................................................

  cTotSIROFields = 64;
  cTotSIRORoughClasses = 16;
  cTotSIROHorLines = 8;
  cTotSIROVertLines = 8;
  cTotSIROXScales = 7;
  cTotSIROYScales = 8;
  cTotSIROCaptions = 0;
//  cTotSIROCaptions = 1; // Caption

  cSIROYAxisAlign = TItemAlignement(iaCenter);
  cSIROXAxisAlign = TItemAlignement(iaCenter);

  cSIRORasterWidth = 36;
  cSIRORasterHeight = 18;
//  cSIRORasterHeight = 22; // Caption

  cSIROXOrgL = 4;
  cSIROYOrgL = 0;
//  cSIROYOrgL = 4; //Caption

  cSIROXOrgA = 2;
  cSIROYOrgA = 17;
//  cSIROYOrgA = 21;  // Caption
  //...........................................................................

//------------------------------------------------------------------------------
// SIRO F/H Field definition
//------------------------------------------------------------------------------
  //...........................................................................

  sIROFields: array[0..cTotSIROFields-1] of TFieldRec = (
  ( // field 0
    top:					cSIROYOrgL + 14;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 1
    top:					cSIROYOrgL + 12;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 2
    top:					cSIROYOrgL + 10;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 3
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 4
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 5
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 6
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 7
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 8
    top:					cSIROYOrgL + 14;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 9
    top:					cSIROYOrgL + 12;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 10
    top:					cSIROYOrgL + 10;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 11
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 12
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 13
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 14
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 15
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 16
    top:					cSIROYOrgL + 14;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 17
    top:					cSIROYOrgL + 12;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 18
    top:					cSIROYOrgL + 10;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 19
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 20
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 21
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 22
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 23
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 24
    top:					cSIROYOrgL + 14;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 25
    top:					cSIROYOrgL + 12;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 26
    top:					cSIROYOrgL + 10;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 27
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 28
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 29
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 30
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 31
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 32
    top:					cSIROYOrgL + 14;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 33
    top:					cSIROYOrgL + 12;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 34
    top:					cSIROYOrgL + 10;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 35
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 36
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 37
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 38
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 39
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 40
    top:					cSIROYOrgL + 14;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 41
    top:					cSIROYOrgL + 12;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 42
    top:					cSIROYOrgL + 10;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 43
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 44
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 45
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 46
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 47
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 48
    top:					cSIROYOrgL + 14;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 49
    top:					cSIROYOrgL + 12;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 50
    top:					cSIROYOrgL + 10;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 51
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 52
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 53
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 54
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 55
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 56
    top:					cSIROYOrgL + 14;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 57
    top:					cSIROYOrgL + 12;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 58
    top:					cSIROYOrgL + 10;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 59
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 60
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 61
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 62
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 63
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;)
  );

  //...........................................................................
  sIRORaster:  TMatrixRasterRec = (// raster
   width:				cSIRORasterWidth;
   height:				cSIRORasterHeight;
  );

  sIROField:   TFieldsRec = (
    fieldWidth:   4;
    fieldHeight:	2;
     background:(
       color:      clQualityBackground;
       style:      bsSolid;);
  table:          @sIROFields;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SIRO F/H Scale Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  sIROHorLines: array[0..cTotSIROHorLines-1] of TLineRec = (
  (	// horizontal line 0
   pos:					cSIROYOrgL + 2;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
  pPen:					nil;),
  (	// horizontal line 1
   pos:					cSIROYOrgL + 4;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
  pPen:					@cLinePenRough;),
  (	// horizontal line 2
   pos:					cSIROYOrgL + 6;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
  pPen:					nil;),
  (	// horizontal line 3
   pos:					cSIROYOrgL + 8;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
  pPen:					@cLinePenRough;),
  (	// horizontal line 4
   pos:					cSIROYOrgL + 10;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
  pPen:					nil;),
  (	// horizontal line 5
   pos:					cSIROYOrgL + 12;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
  pPen:					@cLinePenRough;),
  (	// horizontal line 6
   pos:					cSIROYOrgL + 14;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
  pPen:					nil;),
  (	// horizontal line 7
   pos:					cSIROYOrgL + 16;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
  pPen:					@cLinePenRough;)
  );
  //...........................................................................

  sIROVertLines: array[0..cTotSIROVertLines-1] of TLineRec =(
 (	// vertical line 0
   pos:					cSIROXOrgL + 0;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 16;
  pPen:					@cLinePenRough;),
 (	// vertical line 1
   pos:					cSIROXOrgL + 4;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 16;
  pPen:					nil;),
 (	// vertical line 2
   pos:					cSIROXOrgL + 8;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 16;
  pPen:					@cLinePenRough;),
 (	// vertical line 3
   pos:					cSIROXOrgL + 12;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 16;
  pPen:					nil;),
 (	// vertical line 4
   pos:					cSIROXOrgL + 16;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 16;
  pPen:					@cLinePenRough;),
 (	// vertical line 5
   pos:					cSIROXOrgL + 20;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 16;
  pPen:					nil;),
 (	// vertical line 6
   pos:					cSIROXOrgL + 24;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 16;
  pPen:					@cLinePenRough;),
 (	// vertical line 7
   pos:					cSIROXOrgL + 28;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 16;
  pPen:					nil;)
  );
  //...........................................................................

  sIROXScales: array[0..cTotSIROXScales-1] of TTextRec = (
  ( // X Axis Scale 0
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 4;
    yPos:         cSIROYOrgA;
    text:         '0.5';),
  ( // X Axis Scale 1
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 8;
    yPos:         cSIROYOrgA;
    text:         '1.0';),
  ( // X Axis Scale 2
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 12;
    yPos:         cSIROYOrgA;
    text:         '1.5';),
  ( // X Axis Scale 3
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 16;
    yPos:         cSIROYOrgA;
    text:         '2.0';),
  ( // X Axis Scale 4
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 20;
    yPos:         cSIROYOrgA;
    text:         '3.0';),
  ( // X Axis Scale 5
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 24;
    yPos:         cSIROYOrgA;
    text:         '4.0';),
  ( // X Axis Scale 6
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 28;
    yPos:         cSIROYOrgA;
    text:         '8.0';)
  );
  //...........................................................................

  sIROFYScales: array[0..cTotSIROYScales-1] of TTextRec = (
  ( // Y Axis Scale 0
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 16;
    text:         '1.0';),
  ( // Y Axis Scale 1
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 14;
    text:         '1.5';),
  ( // Y Axis Scale 2
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 12;
    text:         '2.0';),
  ( // Y Axis Scale 3
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 10;
    text:         '3.0';),
  ( // Y Axis Scale 4
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 8;
    text:         '4.5';),
  ( // Y Axis Scale 5
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 6;
    text:         '6.5';),
  ( // Y Axis Scale 6
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 4;
    text:         '9.0';),
  ( // Y Axis Scale 7
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 2;
    text:         '13.0';)
  );
  //...........................................................................

  sIROHYScales: array[0..cTotSIROYScales-1] of TTextRec = (
  ( // Y Axis Scale 0
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 16;
    text:         '0.7';),
  ( // Y Axis Scale 1
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 14;
    text:         '1.0';),
  ( // Y Axis Scale 2
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 12;
    text:         '1.5';),
  ( // Y Axis Scale 3
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 10;
    text:         '2.0';),
  ( // Y Axis Scale 4
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 8;
    text:         '3.0';),
  ( // Y Axis Scale 5
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 6;
    text:         '4.0';),
  ( // Y Axis Scale 6
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 4;
    text:         '6.0';),
  ( // Y Axis Scale 7
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 2;
    text:         '9.0';)
  );
  //...........................................................................
(*
  sIROCaptions: array[0..cTotSIROCaptions-1] of TTextRec = (
    (
      pFont:        @cFontCaption;
      horAlign:     iaCenter;
      vertAlign:    iaTop;
      xPos:         17;
      yPos:         3;
      text:         'SIRO Fehler';)
  );
*)
  //...........................................................................

  sIROFScaleLayer:  TScaleLayerRec = (
   horLine:(
     pen: (
    width:				1;
     color:        clLine;
     mode:         pmCopy;
     style:        psSolid;
   );
   table:          @sIROHorLines;
    );
   vertLine:(
    pen: (
     width:				1;
     color:        clLine;
     mode:         pmCopy;
    style:        psSolid;
      );
    table:          @sIROVertLines;
    );
    xAxis:            @sIROXScales;
    yAxis:            @sIROFYScales;
//    caption:          @sIROCaptions;
    title: (
      pFont:        @cFontTitle;
      horAlign:     iaRight;
      vertAlign:    iaCenter;
      xTextPos:     cSiroXOrgL;
      yTextPos:     0;
      barHight:     2;
      text: 'FF Quality Matrix/F'
    );
    caption:          nil;
  );
  //...........................................................................

  sIROHScaleLayer:  TScaleLayerRec = (
   horLine:(
     pen: (
    width:				1;
     color:        clLine;
     mode:         pmCopy;
     style:        psSolid;
   );
   table:          @sIROHorLines;
    );
   vertLine:(
    pen: (
     width:				1;
     color:        clLine;
     mode:         pmCopy;
    style:        psSolid;
      );
    table:          @sIROVertLines;
    );
    xAxis:            @sIROXScales;
    yAxis:            @sIROHYScales;
//    caption:          @sIROCaptions;
    title: (
      pFont:        @cFontTitle;
      horAlign:     iaRight;
      vertAlign:    iaCenter;
      xTextPos:     cSiroXOrgL;
      yTextPos:     0;
      barHight:     2;
      text: 'FF Quality Matrix/H'
    );
    caption:          nil;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SIRO F/H Display Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  sIRORoughClasses:  array[0..cTotSIRORoughClasses-1] of TSuperClassRec = (
  ( // Rough Class 0
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 12;
      left:					cSIROXOrgL;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (0, 1, 8, 9, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 1
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL + 8;
      left:					cSIROXOrgL;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (2, 3, 10, 11, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 2
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 4;
      left:					cSIROXOrgL;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (4, 5, 12, 13, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 3
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL;
      left:					cSIROXOrgL;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (6, 7, 14, 15, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 4
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 12;
      left:					cSIROXOrgL + 8;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (16, 17, 24, 25, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 5
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL + 8;
      left:					cSIROXOrgL + 8;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (18, 19, 26, 27, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 6
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 4;
      left:					cSIROXOrgL + 8;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (20, 21, 28, 29, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 7
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL;
      left:					cSIROXOrgL + 8;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (22, 23, 30, 31, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 8
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 12;
      left:					cSIROXOrgL + 16;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (32, 33, 40, 41, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 9
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL + 8;
      left:					cSIROXOrgL + 16;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (34, 35, 42, 43, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 10
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 4;
      left:					cSIROXOrgL + 16;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (36, 37, 44, 45, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 11
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL;
      left:					cSIROXOrgL + 16;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (38, 39, 46, 47, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 12
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 12;
      left:					cSIROXOrgL + 24;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (48, 49, 56, 57, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 13
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL + 8;
      left:					cSIROXOrgL + 24;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (50, 51, 58, 59, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 14
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 4;
      left:					cSIROXOrgL + 24;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (52, 53, 60, 61, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 15
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL;
      left:					cSIROXOrgL + 24;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (54, 55, 62, 63, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); )
  );
  //...........................................................................

 sIRODisplayLayer:  TDisplayLayerRec = (
   number:(
      cuts:(
        pFont:         @cFontCuts;
//          horAlign:     iaRight;
//          vertAlign:    iaTop;
        horAlign:     iaRight;
        vertAlign:    iaTop;
        background:   clNone;
        preText:      '';
        formatText:   '%.2n';
        postText:      '';
      );
      defects:(
        pFont:        @cFontDefects;
        horAlign:     iaLeft;
        vertAlign:    iaBottom;
        background:   clNone;
        preText:      '';
        formatText:   '%.2n';
        postText:     '';
      );
    );

    roughClassTbl: @sIRORoughClasses;
    defaultSuperClassTbl: nil;
);
  //...........................................................................

//------------------------------------------------------------------------------
// SIRO F/H Selection Layer definition
//------------------------------------------------------------------------------
  //...........................................................................
  sIROSelectionLayer:  TSelectionLayerRec = (
    cutState:(
      pActiveColor:           @cCutOn;
      pPassiveColor:          nil;
    );
    fFClusterState:(
      pActiveColor:           @cFFClusterOn;
      pPassiveColor:          nil;
    );
    sCMemberState:(
      pActiveColor:           @cSCMemberOn;
      pPassiveColor:          nil;
    );
  );

  cSIROFMatrix:   TQualityMatrixDescRec = (
    pRaster:          @sIRORaster;
    pField:           @sIROField;
    pScaleLayer:      @sIROFScaleLayer;
    pDisplayLayer:    @sIRODisplayLayer;
    pSelectionLayer:  @sIROSelectionLayer;
    pCurveLayer:      nil;
  );

  cSIROHMatrix:   TQualityMatrixDescRec = (
    pRaster:          @sIRORaster;
    pField:           @sIROField;
    pScaleLayer:      @sIROHScaleLayer;
    pDisplayLayer:    @sIRODisplayLayer;
    pSelectionLayer:  @sIROSelectionLayer;
    pCurveLayer:      nil;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SIRO BD Matrix definition
//------------------------------------------------------------------------------
  //...........................................................................
  //...........................................................................
  //...........................................................................

  cTotSIROBDVertLines = 16;
  cTotSIROBDXScales = 8;

  cSIROBDRasterHeight = 19;

  cSIROBDYOrgA = 18;
  //...........................................................................

//------------------------------------------------------------------------------
// SIRO BD Field definition
//------------------------------------------------------------------------------
  //...........................................................................

  sIROBDFields: array[0..cTotSIROFields-1] of TFieldRec = (
  ( // field 0
    top:					cSIROYOrgL + 15;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 1
    top:					cSIROYOrgL + 13;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 2
    top:					cSIROYOrgL + 11;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 3
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 4
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 5
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 6
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 7
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 8
    top:					cSIROYOrgL + 15;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 9
    top:					cSIROYOrgL + 13;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 10
    top:					cSIROYOrgL + 11;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 11
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 12
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 13
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 14
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 15
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 4;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 16
    top:					cSIROYOrgL + 15;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 17
    top:					cSIROYOrgL + 13;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 18
    top:					cSIROYOrgL + 11;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 19
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 20
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 21
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 22
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 23
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 8;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 24
    top:					cSIROYOrgL + 15;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 25
    top:					cSIROYOrgL + 13;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 26
    top:					cSIROYOrgL + 11;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 27
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 28
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 29
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 30
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 31
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 32
    top:					cSIROYOrgL + 15;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 33
    top:					cSIROYOrgL + 13;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 34
    top:					cSIROYOrgL + 11;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 35
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 36
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 37
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 38
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 39
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 16;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 40
    top:					cSIROYOrgL + 15;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 41
    top:					cSIROYOrgL + 13;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 42
    top:					cSIROYOrgL + 11;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 43
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 44
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 45
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 46
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 47
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 20;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 48
    top:					cSIROYOrgL + 15;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 49
    top:					cSIROYOrgL + 13;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 50
    top:					cSIROYOrgL + 11;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 51
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 52
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 53
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 54
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 55
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 24;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 56
    top:					cSIROYOrgL + 15;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 57
    top:					cSIROYOrgL + 13;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 58
    top:					cSIROYOrgL + 11;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 59
    top:					cSIROYOrgL + 8;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 60
    top:					cSIROYOrgL + 6;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 61
    top:					cSIROYOrgL + 4;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 62
    top:					cSIROYOrgL + 2;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 63
    top:					cSIROYOrgL + 0;
    left:					cSIROXOrgL + 28;
    width:				0;
    height:				0;
    pBackground:  nil;)
  );

  //...........................................................................

  sIROBDRaster:  TMatrixRasterRec = (// raster
   width:				cSIRORasterWidth;
   height:				cSIROBDRasterHeight;
  );

  sIROBDField:   TFieldsRec = (
    fieldWidth:   4;
    fieldHeight:	2;
     background:(
       color:      clQualityBackground;
       style:      bsSolid;);
  table:          @sIROBDFields;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SIRO BD Scale Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  sIROBDHorLines: array[0..cTotSIROHorLines-1] of TLineRec = (
  (	// horizontal line 0
    pos:					cSIROYOrgL + 2;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
  pPen:					nil;),
  (	// horizontal line 1
    pos:					cSIROYOrgL + 4;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
    pPen:					nil;),
  (	// horizontal line 2
    pos:					cSIROYOrgL + 6;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
    pPen:					@cLinePenRough;),
  (	// horizontal line 3
    pos:					cSIROYOrgL + 8;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
    pPen:					nil;),
  (	// horizontal line 4
    pos:					cSIROYOrgL + 10;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
    pPen:					@cLinePenRough;),
  (	// horizontal line 5
    pos:					cSIROYOrgL + 11;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
    pPen:					@cLinePenRough;),
  (	// horizontal line 6
    pos:					cSIROYOrgL + 13;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
    pPen:					nil;),
  (	// horizontal line 7
   pos:					cSIROYOrgL + 15;
    start:				cSIROXOrgL + 0;
    stop:					cSIROXOrgL + 32;
    pPen:					@cLinePenRough;)
  );
  //...........................................................................

  sIROBDVertLines: array[0..cTotSIROBDVertLines-1] of TLineRec =(
 (	// vertical line 0
   pos:					cSIROXOrgL + 0;
   start:				cSIROYOrgL + 0;
   stop: 				cSIROYOrgL + 10;
   pPen:				@cLinePenRough;),
 (	// vertical line 1
   pos:					cSIROXOrgL + 4;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 10;
  pPen:					nil;),
 (	// vertical line 2
   pos:					cSIROXOrgL + 8;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 10;
  pPen:					@cLinePenRough;),
 (	// vertical line 3
   pos:					cSIROXOrgL + 12;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 10;
  pPen:					nil;),
 (	// vertical line 4
   pos:					cSIROXOrgL + 16;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 10;
  pPen:					@cLinePenRough;),
 (	// vertical line 5
   pos:					cSIROXOrgL + 20;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 10;
  pPen:					nil;),
 (	// vertical line 6
   pos:					cSIROXOrgL + 24;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 10;
  pPen:					@cLinePenRough;),
 (	// vertical line 7
   pos:					cSIROXOrgL + 28;
   start:				cSIROYOrgL + 0;
    stop:					cSIROYOrgL + 10;
  pPen:					nil;),
 (	// vertical line 8
   pos:					cSIROXOrgL + 0;
   start:				cSIROYOrgL + 11;
   stop: 				cSIROYOrgL + 17;
   pPen:				@cLinePenRough;),
 (	// vertical line 9
   pos:					cSIROXOrgL + 4;
   start:				cSIROYOrgL + 11;
   stop: 				cSIROYOrgL + 17;
   pPen:					nil;),
 (	// vertical line 10
    pos:					cSIROXOrgL + 8;
    start:				cSIROYOrgL + 11;
    stop: 				cSIROYOrgL + 17;
    pPen:					@cLinePenRough;),
 (	// vertical line 11
    pos:					cSIROXOrgL + 12;
   start:				cSIROYOrgL + 11;
   stop: 				cSIROYOrgL + 17;
   pPen:					nil;),
 (	// vertical line 12
   pos:					cSIROXOrgL + 16;
   start:				cSIROYOrgL + 11;
   stop: 				cSIROYOrgL + 17;
  pPen:					@cLinePenRough;),
 (	// vertical line 13
   pos:					cSIROXOrgL + 20;
   start:				cSIROYOrgL + 11;
   stop: 				cSIROYOrgL + 17;
  pPen:					nil;),
 (	// vertical line 14
   pos:					cSIROXOrgL + 24;
   start:				cSIROYOrgL + 11;
   stop: 				cSIROYOrgL + 17;
  pPen:					@cLinePenRough;),
 (	// vertical line 15
   pos:					cSIROXOrgL + 28;
   start:				cSIROYOrgL + 11;
   stop: 				cSIROYOrgL + 17;
  pPen:					nil;)
  );
  //...........................................................................

  sIROBDXScales: array[0..cTotSIROBDXScales-1] of TTextRec = (
  ( // X Axis Scale 0
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 2;
    yPos:         cSIROBDYOrgA;
    text:         '2';),
  ( // X Axis Scale 1
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 6;
    yPos:         cSIROBDYOrgA;
    text:         '4';),
  ( // X Axis Scale 2
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 10;
    yPos:         cSIROBDYOrgA;
    text:         '6';),
  ( // X Axis Scale 3
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 14;
    yPos:         cSIROBDYOrgA;
    text:         '8';),
  ( // X Axis Scale 4
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 18;
    yPos:         cSIROBDYOrgA;
    text:         '12';),
  ( // X Axis Scale 5
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 22;
    yPos:         cSIROBDYOrgA;
    text:         '20';),
  ( // X Axis Scale 6
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 26;
    yPos:         cSIROBDYOrgA;
    text:         '32';),
  ( // X Axis Scale 7
    pFont:        @cFontAxis;
    horAlign:     iaCenter;
    vertAlign:    cSIROXAxisAlign;
    xPos:         cSIROXOrgL + 30;
    yPos:         cSIROBDYOrgA;
    text:         '70';)
);
  //...........................................................................

  sIROBDYScales: array[0..cTotSIROYScales-1] of TTextRec = (
  ( // Y Axis Scale 0
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 15;
    text:         '-4.0';),
  ( // Y Axis Scale 1
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 13;
    text:         '-2.5';),
  ( // Y Axis Scale 2
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaBottom; //iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 11;
    text:         '-1.5';),
  ( // Y Axis Scale 3
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaTop; //iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 10;
    text:         '1.5';),
  ( // Y Axis Scale 4
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 8;
    text:         '2.5';),
  ( // Y Axis Scale 5
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 6;
    text:         '4.0';),
  ( // Y Axis Scale 6
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 4;
    text:         '6.0';),
  ( // Y Axis Scale 7
    pFont:        @cFontAxis;
    horAlign:     cSIROYAxisAlign;
    vertAlign:    iaCenter;
    xPos:         cSIROXOrgA;
    yPos:         cSIROYOrgL + 2;
    text:         '9.0';)
  );
  //...........................................................................

  //...........................................................................

  sIROBDScaleLayer:  TScaleLayerRec = (
   horLine:(
     pen: (
    width:				1;
     color:        clLine;
     mode:         pmCopy;
     style:        psSolid;
   );
   table:          @sIROBDHorLines;
    );
   vertLine:(
    pen: (
     width:				1;
     color:        clLine;
     mode:         pmCopy;
    style:        psSolid;
      );
    table:          @sIROBDVertLines;
    );
    xAxis:            @sIROBDXScales;
    yAxis:            @sIROBDYScales;
    title: (
      pFont:        @cFontTitle;
      horAlign:     iaRight;
      vertAlign:    iaCenter;
      xTextPos:     cSiroXOrgL;
      yTextPos:     0;
      barHight:     2;
      text: 'FF Quality Matrix/BD'
    );
    caption:          nil;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// SIRO BD Display Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  sIROBDRoughClasses:  array[0..cTotSIRORoughClasses-1] of TSuperClassRec = (
  ( // Rough Class 0
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 15;
      left:					cSIROXOrgL;
      width:				8;
      height:				2;
      pBackground: nil );
    memberTbl: (0, 8, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 1
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL + 11;
      left:					cSIROXOrgL;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (1, 2, 9, 10, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 2
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 6;
      left:					cSIROXOrgL;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (3, 4, 11, 12, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 3
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL;
      left:					cSIROXOrgL;
      width:				8;
      height:				6;
      pBackground: nil );
    memberTbl: (5, 6, 7, 13, 14, 15, cSCDelimiter,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 4
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 15;
      left:					cSIROXOrgL + 8;
      width:				8;
      height:				2;
      pBackground: nil );
    memberTbl: (16, 24, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 5
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL + 11;
      left:					cSIROXOrgL + 8;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (17, 18, 25, 26, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 6
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 6;
      left:					cSIROXOrgL + 8;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (19, 20, 27, 28, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 7
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL;
      left:					cSIROXOrgL + 8;
      width:				8;
      height:				6;
      pBackground: nil );
    memberTbl: (21, 22, 23, 29, 30, 31, cSCDelimiter,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 8
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 15;
      left:					cSIROXOrgL + 16;
      width:				8;
      height:				2;
      pBackground: nil );
    memberTbl: (32, 40, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 9
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL + 11;
      left:					cSIROXOrgL + 16;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (33, 34, 41, 42, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 10
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 6;
      left:					cSIROXOrgL + 16;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (35, 36, 43, 44, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 11
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL;
      left:					cSIROXOrgL + 16;
      width:				8;
      height:				6;
      pBackground: nil );
    memberTbl: (37, 38, 39, 45, 46, 47, cSCDelimiter,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 12
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 15;
      left:					cSIROXOrgL + 24;
      width:				8;
      height:				2;
      pBackground: nil );
    memberTbl: (48, 56, cSCDelimiter,0,0,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 13
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL + 11;
      left:					cSIROXOrgL + 24;
      width:			  8;
      height:				4;
      pBackground: nil );
    memberTbl: (49, 50, 57, 58, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 14
    PNumber: nil;
    boarder: (
      top:          cSIROYOrgL + 6;
      left:					cSIROXOrgL + 24;
      width:				8;
      height:				4;
      pBackground: nil );
    memberTbl: (51, 52, 59, 60, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 15
    PNumber: nil;
    boarder: (
      top:	        cSIROYOrgL;
      left:					cSIROXOrgL + 24;
      width:				8;
      height:				6;
      pBackground: nil );
    memberTbl: (53, 54, 55, 61, 62, 63, cSCDelimiter,0, 0,0,0,0,0,0,0,0); )
  );
  //...........................................................................

 sIROBDDisplayLayer:  TDisplayLayerRec = (
   number:(
      cuts:(
        pFont:         @cFontCuts;
//          horAlign:     iaRight;
//          vertAlign:    iaTop;
        horAlign:     iaRight;
        vertAlign:    iaTop;
        background:   clNone;
        preText:      '';
        formatText:   '%.2n';
        postText:      '';
      );
      defects:(
        pFont:        @cFontDefects;
        horAlign:     iaLeft;
        vertAlign:    iaBottom;
        background:   clNone;
        preText:      '';
        formatText:   '%.2n';
        postText:     '';
      );
    );

    roughClassTbl: @sIROBDRoughClasses;
    defaultSuperClassTbl: nil;
);
  //...........................................................................


  cSIROBDMatrix:   TQualityMatrixDescRec = (
    pRaster:          @sIROBDRaster;
    pField:           @sIROBDField;
    pScaleLayer:      @sIROBDScaleLayer;
    pDisplayLayer:    @sIROBDDisplayLayer;
    pSelectionLayer:  @sIROSelectionLayer;
    pCurveLayer:      nil;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// Color Matrix definition
//------------------------------------------------------------------------------
  //...........................................................................
  //...........................................................................
  //...........................................................................

  cTotColorFields = 128;
  cTotColorRoughClasses = 32;
  cTotColorHorLines = 16;
  cTotColorVertLines = 16;
  cTotColorXScales = 8;
  cTotColorYScales = 16;
  cTotColorCaptions = 1;
(*
  cDarkXLeftScale = 0;
  cDarkXRightScale = 13;
  cDarkYTopScale = 0;
  cDarkYBottomScale = 12;

  cBrightXLeftScale = 4;
  cBrightXRightScale = 13;
  cBrightYTopScale = 13;
  cBrightYBottomScale = 17;
*)
  cColorYAxisAlign = TItemAlignement(iaCenter);
  cColorXAxisAlign = TItemAlignement(iaCenter);

  cColorXOrgL = 2;
  cColorYOrgL = 0;

  cColorXOrgA = 1;
  cColorYOrgA = 34;
  //...........................................................................

//------------------------------------------------------------------------------
// Color Field definition
//------------------------------------------------------------------------------
  //...........................................................................
{(*}

  colorFields: array[0..cTotColorFields-1] of TFieldRec = (
  ( // field 0
    top:					cColorYOrgL + 14;
    left:					cColorXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
 ( // field 1
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 2
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 3
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 4
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:  nil;),
   ( // field 5
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 6
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 7
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 8
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:  nil;),
   ( // field 9
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 10
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 11
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 12
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 13
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 14
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 15
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 16
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 17
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 18
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 19
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 20
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:  nil;),
    ( // field 21
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 22
    top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 23
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 24
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 25
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 6;
     width:				0;
      height:				0;
      pBackground:   nil;),
    ( // field 26
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
       pBackground:   nil;),
  ( // field 27
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 28
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 29
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 30
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 31
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 32
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 33
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 34
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 35
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 36
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 37
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 38
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 39
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 40
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 41
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 42
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 43
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 44
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 45
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 46
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 47
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 48
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 49
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 50
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 51
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 52
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 53
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 54
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 55
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 56
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 57
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 58
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 59
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 60
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 61
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 62
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 63
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 64
   top:					cColorYOrgL + 31;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 65
   top:					cColorYOrgL + 29;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 66
   top:					cColorYOrgL + 27;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 67
   top:					cColorYOrgL + 25;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 68
   top:					cColorYOrgL + 23;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
      pBackground:  nil;),
    ( // field 69
   top:					cColorYOrgL + 21;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 70
   top:					cColorYOrgL + 19;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 71
   top:					cColorYOrgL + 17;
     left:					cColorXOrgL + 0;
     width:				0;
      height:				0;
      pBackground:   nil;),
   ( // field 72
   top:					cColorYOrgL + 31;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 73
   top:					cColorYOrgL + 29;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 74
   top:					cColorYOrgL + 27;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 75
   top:					cColorYOrgL + 25;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 76
   top:					cColorYOrgL + 23;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 77
   top:					cColorYOrgL + 21;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 78
   top:					cColorYOrgL + 19;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 79
   top:					cColorYOrgL + 17;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 80
   top:					cColorYOrgL + 31;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 81
   top:					cColorYOrgL + 29;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 82
    top:					cColorYOrgL + 27;
     left:					cColorXOrgL + 4;
     width:				0;
      height:				0;
      pBackground:   nil;),
   ( // field 83
   top:					cColorYOrgL + 25;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 84
   top:					cColorYOrgL + 23;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 85
   top:					cColorYOrgL + 21;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 86
   top:					cColorYOrgL + 19;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 87
   top:					cColorYOrgL + 17;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 88
   top:					cColorYOrgL + 31;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 89
   top:					cColorYOrgL + 29;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 90
   top:					cColorYOrgL + 27;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 91
   top:					cColorYOrgL + 25;
    left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 92
   top:					cColorYOrgL + 23;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 93
   top:					cColorYOrgL + 21;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 94
   top:					cColorYOrgL + 19;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 95
   top:					cColorYOrgL + 17;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 96
   top:					cColorYOrgL + 31;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 97
   top:					cColorYOrgL + 29;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 98
   top:					cColorYOrgL + 27;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 99
   top:					cColorYOrgL + 25;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 100
   top:					cColorYOrgL + 23;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 101
   top:					cColorYOrgL + 21;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 102
   top:					cColorYOrgL + 19;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 103
   top:					cColorYOrgL + 17;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 104
   top:					cColorYOrgL + 31;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 105
   top:					cColorYOrgL + 29;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 106
   top:					cColorYOrgL + 27;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 107
   top:					cColorYOrgL + 25;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 108
   top:					cColorYOrgL + 23;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 109
   top:					cColorYOrgL + 21;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 110
   top:					cColorYOrgL + 19;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 111
   top:					cColorYOrgL + 17;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 112
   top:					cColorYOrgL + 31;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 113
   top:					cColorYOrgL + 29;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 114
   top:					cColorYOrgL + 27;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 115
   top:					cColorYOrgL + 25;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 116
   top:					cColorYOrgL + 23;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 117
   top:					cColorYOrgL + 21;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
  ( // field 118
  top:					cColorYOrgL + 19;
   left:					cColorXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:   nil;),
  ( // field 119
  top:					cColorYOrgL + 17;
   left:					cColorXOrgL + 12;
    width:				0;
    height:				0;
    pBackground:   nil;),
  ( // field 120
  top:					cColorYOrgL + 31;
    left:					cColorXOrgL + 14;
    width:				0;
    height:				0;
    pBackground:  nil;),
  ( // field 121
  top:					cColorYOrgL + 29;
    left:					cColorXOrgL + 14;
    width:				0;
   height:				0;
    pBackground:   nil;),
  ( // field 122
  top:					cColorYOrgL + 27;
   left:					cColorXOrgL + 14;
    width:				0;
   height:				0;
    pBackground:   nil;),
 ( // field 123
   top:					cColorYOrgL + 25;
  left:					cColorXOrgL + 14;
   width:				0;
   height:				0;
    pBackground:   nil;),
  ( // field 124
  top:					cColorYOrgL + 23;
    left:					cColorXOrgL + 14;
    width:				0;
   height:				0;
    pBackground:  nil;),
  ( // field 125
  top:					cColorYOrgL + 21;
   left:					cColorXOrgL + 14;
    width:				0;
   height:				0;
    pBackground:   nil;),
  ( // field 126
  top:					cColorYOrgL + 19;
   left:					cColorXOrgL + 14;
   width:				0;
   height:				0;
    pBackground:   nil;),
  ( // field 127
  top:					cColorYOrgL + 17;
   left:					cColorXOrgL + 14;
   width:				0;
   height:				0;
    pBackground:   nil;)
  );

  //...........................................................................
  colorRaster:  TMatrixRasterRec = (// raster
   width:				18;
   height:				35;);
  colorField:   TFieldsRec = (
    fieldWidth:   2;
    fieldHeight:	2;
     background:(
       color:      clQualityBackground;
       style:      bsSolid;);
  table:          @ColorFields;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// Color Scale Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  colorHorLines: array[0..cTotColorHorLines-1] of TLineRec = (
    (	// horizontal line 0
      pos:					cColorYOrgL + 2;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
      pPen:					nil;),
   (	// horizontal line 1 rough
      pos:					cColorYOrgL + 4;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
      pPen:					@cLinePenRough;),
   (	// horizontal line 2
      pos:					cColorYOrgL + 6;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
      pPen:					nil;),
   (	// horizontal line 3 rough
      pos:					cColorYOrgL + 8;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
      pPen:					@cLinePenRough;),
   (	// horizontal line 4
      pos:					cColorYOrgL + 10;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					nil;),
   (	// horizontal line 5 rough
      pos:					cColorYOrgL + 12;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					@cLinePenRough;),
     (	// horizontal line 6
      pos:					cColorYOrgL + 14;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					nil;),
   (	// horizontal line 7 rough
      pos:					cColorYOrgL + 16;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					@cLinePenRough;),
   (	// horizontal line 8 rough
      pos:					cColorYOrgL + 17;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					@cLinePenRough;),
   (	// horizontal line 9
      pos:					cColorYOrgL + 19;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					nil;),
   (	// horizontal line 10 rough
      pos:					cColorYOrgL + 21;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					@cLinePenRough;),
   (	// horizontal line 11
      pos:					cColorYOrgL + 23;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					nil;),
   (	// horizontal line 12 rough
      pos:					cColorYOrgL + 25;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					@cLinePenRough;),
   (	// horizontal line 13
      pos:					cColorYOrgL + 27;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					nil;),
   (	// horizontal line 14 rough
      pos:					cColorYOrgL + 29;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					@cLinePenRough;),
 (	// horizontal line 15
   pos:					cColorYOrgL + 31;
   start:				cColorXOrgL + 0;
   stop:					cColorXOrgL + 16;
  pPen:					nil;)
  );
  //...........................................................................

  colorVertLines: array[0..cTotColorVertLines-1] of TLineRec =(
  (	// vertical line 0
     pos:					cColorXOrgL + 0;
     start:				cColorYOrgL + 0;
     stop:					cColorYOrgL + 16;
     pPen:					@cLinePenRough;),
  (	// vertical line 1 fine
      pos:					cColorXOrgL + 2;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					nil;),
  (	// vertical line 2 rough
     pos:					cColorXOrgL + 4;
     start:				cColorYOrgL + 0;
     stop:					cColorYOrgL + 16;
     pPen:					@cLinePenRough;),
  (	// vertical line 3
      pos:					cColorXOrgL + 6;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					nil;),
   (	// vertical line 4 rough
      pos:					cColorXOrgL + 8;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					@cLinePenRough;),
   (	// vertical line 5
      pos:					cColorXOrgL + 10;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					nil;),
   (	// vertical line 6 rough
      pos:					cColorXOrgL + 12;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					@cLinePenRough;),
   (	// vertical line 7
      pos:					cColorXOrgL + 14;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					nil;),
   (	// vertical line 8 rough
      pos:					cColorXOrgL + 0;
      start:				cColorYOrgL + 17;
      stop:					cColorYOrgL + 33;
      pPen:					@cLinePenRough;),
   (	// vertical line 9
      pos:					cColorXOrgL + 2;
      start:				cColorYOrgL + 17;
      stop:					cColorYOrgL + 33;
      pPen:					nil;),
   (	// vertical line 10 rough
      pos:					cColorXOrgL + 4;
      start:				cColorYOrgL + 17;
      stop:					cColorYOrgL + 33;
      pPen:					@cLinePenRough;),
   (	// vertical line 11 fine
      pos:					cColorXOrgL + 6;
      start:				cColorYOrgL + 17;
      stop:					cColorYOrgL + 33;
      pPen:					nil;),
   (	// vertical line 12 rough
      pos:					cColorXOrgL + 8;
      start:				cColorYOrgL + 17;
      stop:					cColorYOrgL + 33;
      pPen:					@cLinePenRough;),
   (	// vertical line 13
       pos:					cColorXOrgL + 10;
       start:				cColorYOrgL + 17;
       stop:			 	cColorYOrgL + 33;
       pPen:			 	nil;),
   (	// vertical line 14 rough
        pos:			 	cColorXOrgL + 12;
        start:		 	cColorYOrgL + 17;
        stop:			 	cColorYOrgL + 33;
        pPen:			 	@cLinePenRough;),
   (	// vertical line 15
       pos:					cColorXOrgL + 14;
       start:				cColorYOrgL + 17;
       stop:			 	cColorYOrgL + 33;
       pPen:			 	nil;)
  );
  //...........................................................................

  colorXScales: array[0..cTotColorXScales-1] of TTextRec = (
    ( // X Axis Scale 0
      pFont:        @cFontScaleEnds;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 0;
      yPos:         cColorYOrgA;
      text:         '0.0';),
    ( // X Axis Scale 1
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 2;
      yPos:         cColorYOrgA;
      text:         '0.5';),
    ( // X Axis Scale 2
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 4;
      yPos:         cColorYOrgA;
      text:         '1.0';),
    ( // X Axis Scale 3
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 6;
      yPos:         cColorYOrgA;
      text:         '1.5';),
    ( // X Axis Scale 4
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 8;
      yPos:         cColorYOrgA;
      text:         '2.0';),
    ( // X Axis Scale 5
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 10;
      yPos:         cColorYOrgA;
      text:         '3.0';),
    ( // X Axis Scale 6
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 12;
      yPos:         cColorYOrgA;
      text:         '4.0';),
    ( // X Axis Scale 7
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 14;
      yPos:         cColorYOrgA;
      text:         '8.0';)
  );
  //...........................................................................

  ColorYScales: array[0..cTotColorYScales-1] of TTextRec = (
    ( // Y Axis Scale 0
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 2;
      text:         '9.0';),
    ( // Y Axis Scale 1
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 4;
      text:         '6.0';),
    ( // Y Axis Scale 2
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 6;
      text:         '4.0';),
    ( // Y Axis Scale 3
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 8;
      text:         '3.0';),
    ( // Y Axis Scale 4
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 10;
      text:         '2.0';),
    ( // Y Axis Scale 5
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 12;
      text:         '1.5';),
    ( // Y Axis Scale 6
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 14;
      text:         '1.0';),
    ( // Y Axis Scale 7
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
//      vertAlign:    iaTop;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 16;
      text:         '0.7';),
    ( // Y Axis Scale 8
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
//      vertAlign:    iaBottom;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 17;
      text:         '-0.7';),
    ( // Y Axis Scale 9
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 19;
      text:         '-1.0';),
    ( // Y Axis Scale 10
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 21;
      text:         '-1.5';),
    ( // Y Axis Scale 11
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 23;
      text:         '-2.0';),
    ( // Y Axis Scale 12
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 25;
      text:         '-3.0';),
    ( // Y Axis Scale 13
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 27;
      text:         '-4.0';),
    ( // Y Axis Scale 14
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 29;
      text:         '-6.0';),
    ( // Y Axis Scale 15
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 31;
       text:         '-9.0';)
  );
  //...........................................................................

  ColorCaptions: array[0..cTotColorCaptions-1] of TTextRec = (
    (
      pFont:        @cFontCaption;
      horAlign:     iaCenter;
      vertAlign:    iaTop;
      xPos:         cColorXOrgL + 22;
      yPos:         cColorYOrgL + 6;
      text:         'Color';)
  );
  //...........................................................................

  colorScaleLayer:  TScaleLayerRec = (
   horLine:(
     pen: (
    width:				1;
     color:        clLine;
     mode:         pmCopy;
     style:        psSolid;
   );
   table:          @ColorHorLines;
    );
   vertLine:(
    pen: (
     width:				1;
     color:        clLine;
     mode:         pmCopy;
    style:        psSolid;
      );
    table:          @ColorVertLines;
    );
    xAxis:            @ColorXScales;
    yAxis:            @ColorYScales;
    title: (
      pFont:        @cFontTitle;
      horAlign:     iaRight;
      vertAlign:    iaCenter;
      xTextPos:     cColorXOrgL;
      yTextPos:     1;
      barHight:     3;
      text: 'Color Quality Matrix'
    );
    caption:          nil;
//    caption:          @ColorCaptions;
  );
  //...........................................................................


//------------------------------------------------------------------------------
// Color Display Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  colorRoughClasses:  array[0..cTotColorRoughClasses-1] of TSuperClassRec = (
  ( // Rough Class 0
    PNumber: nil;
    boarder: (
    top:          cColorYOrgL + 12;
    left:					cColorXOrgL + 0;
    width:				4;
    height:				4;
    pBackground: nil );
    memberTbl: (0, 1, 8, 9, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 1
    PNumber: nil;
    boarder: (
    top:	        cColorYOrgL + 8;
    left:					cColorXOrgL + 0;
    width:				4;
    height:				4;
    pBackground: nil );
    memberTbl: (2, 3, 10, 11, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 2
    PNumber: nil;
    boarder: (
    top:          cColorYOrgL + 4;
    left:					cColorXOrgL + 0;
    width:				4;
    height:			  4;
    pBackground: nil );
    memberTbl: (4, 5, 12, 13, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 3
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL;
      left:					cColorXOrgL + 0;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (6, 7, 14, 15, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 4
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 12;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (16, 17, 24, 25, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 5
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 8;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (18, 19, 26, 27, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 6
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 4;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (20, 21, 28, 29, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 7
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (22, 23, 30, 31, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 8
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 12;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (32, 33, 40, 41, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 9
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 8;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (34, 35, 42, 43, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 10
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 4;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (36, 37, 44, 45, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 11
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (38, 39, 46, 47, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 12
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 12;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (48,49,56,57, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 13
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 8;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (50,51,58,59, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 14
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 4;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (52,53,60,61, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 15
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (54,55,62,63, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 16
    PNumber: nil;
    boarder: (
    top:          cColorYOrgL + 29;
    left:					cColorXOrgL + 0;
    width:				4;
    height:				4;
    pBackground: nil );
    memberTbl: (64,65,72,73, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 17
    PNumber: nil;
    boarder: (
    top:	        cColorYOrgL + 25;
    left:					cColorXOrgL + 0;
    width:				4;
    height:				4;
    pBackground: nil );
    memberTbl: (66,67,74,75, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 18
    PNumber: nil;
    boarder: (
    top:          cColorYOrgL + 21;
    left:					cColorXOrgL + 0;
    width:				4;
    height:			  4;
    pBackground: nil );
    memberTbl: (68,69,76,77, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 19
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 17;
      left:					cColorXOrgL + 0;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (70,71,78,79, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 20
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 29;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (80,81,88,89, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 21
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 25;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (82,83,90,91, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 22
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 21;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (84,85,92,93, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 23
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 17;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (86,87,94,95, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 24
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 29;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (96,97,104,105, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 25
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 25;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (98,99,106,107, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 26
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 21;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (100,101,108,109, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 27
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 17;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (102,103,110,111, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 28
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 29;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (112,113,120,121, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 29
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 25;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (114,115,122,123, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 30
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 21;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (116,117,124,125, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 31
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 17;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (118,119,126,127, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); )
  );
  //...........................................................................

 colorDisplayLayer:  TDisplayLayerRec = (
   number:(
      cuts:(
        pFont:         @cFontCuts;
//          horAlign:     iaRight;
//          vertAlign:    iaTop;
        horAlign:     iaRight;
        vertAlign:    iaTop;
        background:   clNone;
        preText:      '';
        formatText:   '%.3g';
        postText:      '';
      );
      defects:(
        pFont:        @cFontDefects;
        horAlign:     iaLeft;
        vertAlign:    iaBottom;
        background:   clNone;
        preText:      '';
        formatText:   '%.3g';
        postText:     '';
      );
    );

    roughClassTbl: @ColorRoughClasses;
    defaultSuperClassTbl: nil;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// Color Selection Layer definition
//------------------------------------------------------------------------------
  //...........................................................................
  colorSelectionLayer:  TSelectionLayerRec = (
    cutState:(
      pActiveColor:           @cCutOn;
      pPassiveColor:          nil;
    );
    fFClusterState:(
      pActiveColor:           @cFFClusterOn;
      pPassiveColor:          nil;
    );
    sCMemberState:(
      pActiveColor:           @cSCMemberOn;
      pPassiveColor:          nil;
    );
  );

  cColorMatrix:   TQualityMatrixDescRec = (
    pRaster:          @colorRaster;
    pField:           @colorField;
    pScaleLayer:      @colorScaleLayer;
    pDisplayLayer:    @colorDisplayLayer;
    pSelectionLayer:  @colorSelectionLayer;
    pCurveLayer:      nil;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// Color Dark Matrix definition
//------------------------------------------------------------------------------
  //...........................................................................
  //...........................................................................
  //...........................................................................

  cTotColorDarkFields = 64;
  cTotColorDarkRoughClasses = 16;
  cTotColorDarkHorLines = 8;
  cTotColorDarkVertLines = 8;
//  cTotColorXScales = 8;
  cTotColorDarkYScales = 8;
//  cTotColorCaptions = 1;
(*
  cDarkXLeftScale = 0;
  cDarkXRightScale = 13;
  cDarkYTopScale = 0;
  cDarkYBottomScale = 12;

  cBrightXLeftScale = 4;
  cBrightXRightScale = 13;
  cBrightYTopScale = 13;
  cBrightYBottomScale = 17;
*)
//  cColorYAxisAlign = TItemAlignement(iaCenter);
//  cColorXAxisAlign = TItemAlignement(iaCenter);

//  cColorXOrgL = 2;
//  cColorYOrgL = 0;

//  cColorDarkXOrgA = 1;
  cColorDarkYOrgA = 17;
  //...........................................................................

//------------------------------------------------------------------------------
// Color Dark Field definition
//------------------------------------------------------------------------------
  //...........................................................................
{(*}

  colorDarkFields: array[0..cTotColorDarkFields-1] of TFieldRec = (
  ( // field 0
    top:					cColorYOrgL + 14;
    left:					cColorXOrgL + 0;
    width:				0;
    height:				0;
    pBackground:  nil;),
 ( // field 1
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 2
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 3
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 4
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:  nil;),
   ( // field 5
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 6
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 7
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 0;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 8
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:  nil;),
   ( // field 9
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 10
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 11
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 12
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 13
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 14
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 15
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 2;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 16
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 17
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 18
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 19
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 20
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:  nil;),
    ( // field 21
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 22
    top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 23
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 4;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 24
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 25
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 6;
     width:				0;
      height:				0;
      pBackground:   nil;),
    ( // field 26
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
       pBackground:   nil;),
  ( // field 27
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 28
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 29
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 30
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 31
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 6;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 32
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 33
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 34
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 35
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 36
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 37
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 38
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 39
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 8;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 40
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 41
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 42
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 43
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 44
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 45
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 46
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 47
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 10;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 48
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 49
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 50
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 51
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 52
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 53
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 54
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 55
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 12;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 56
   top:					cColorYOrgL + 14;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 57
   top:					cColorYOrgL + 12;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
    ( // field 58
   top:					cColorYOrgL + 10;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 59
   top:					cColorYOrgL + 8;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 60
   top:					cColorYOrgL + 6;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:  nil;),
   ( // field 61
   top:					cColorYOrgL + 4;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;),
   ( // field 62
   top:					cColorYOrgL + 2;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
       pBackground:   nil;),
   ( // field 63
   top:					cColorYOrgL + 0;
     left:					cColorXOrgL + 14;
     width:				0;
     height:				0;
      pBackground:   nil;)
  );

  //...........................................................................
  colorDarkRaster:  TMatrixRasterRec = (// raster
   width:				18;
   height:				18;);
  colorDarkField:   TFieldsRec = (
    fieldWidth:   2;
    fieldHeight:	2;
     background:(
       color:      clQualityBackground;
       style:      bsSolid;);
  table:          @ColorDarkFields;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// Color Dark Scale Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  colorDarkHorLines: array[0..cTotColorDarkHorLines-1] of TLineRec = (
    (	// horizontal line 0
      pos:					cColorYOrgL + 2;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
      pPen:					nil;),
   (	// horizontal line 1 rough
      pos:					cColorYOrgL + 4;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
      pPen:					@cLinePenRough;),
   (	// horizontal line 2
      pos:					cColorYOrgL + 6;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
      pPen:					nil;),
   (	// horizontal line 3 rough
      pos:					cColorYOrgL + 8;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
      pPen:					@cLinePenRough;),
   (	// horizontal line 4
      pos:					cColorYOrgL + 10;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					nil;),
   (	// horizontal line 5 rough
      pos:					cColorYOrgL + 12;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					@cLinePenRough;),
     (	// horizontal line 6
      pos:					cColorYOrgL + 14;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					nil;),
   (	// horizontal line 7 rough
      pos:					cColorYOrgL + 16;
      start:				cColorXOrgL + 0;
      stop:					cColorXOrgL + 16;
    pPen:					@cLinePenRough;)
  );
  //...........................................................................

  colorDarkVertLines: array[0..cTotColorDarkVertLines-1] of TLineRec =(
  (	// vertical line 0
     pos:					cColorXOrgL + 0;
     start:				cColorYOrgL + 0;
     stop:					cColorYOrgL + 16;
     pPen:					@cLinePenRough;),
  (	// vertical line 1 fine
      pos:					cColorXOrgL + 2;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					nil;),
  (	// vertical line 2 rough
     pos:					cColorXOrgL + 4;
     start:				cColorYOrgL + 0;
     stop:					cColorYOrgL + 16;
     pPen:					@cLinePenRough;),
  (	// vertical line 3
      pos:					cColorXOrgL + 6;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					nil;),
   (	// vertical line 4 rough
      pos:					cColorXOrgL + 8;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					@cLinePenRough;),
   (	// vertical line 5
      pos:					cColorXOrgL + 10;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					nil;),
   (	// vertical line 6 rough
      pos:					cColorXOrgL + 12;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					@cLinePenRough;),
   (	// vertical line 7
      pos:					cColorXOrgL + 14;
      start:				cColorYOrgL + 0;
      stop:					cColorYOrgL + 16;
      pPen:					nil;)
  );
  //...........................................................................

  colorDarkXScales: array[0..cTotColorXScales-1] of TTextRec = (
    ( // X Axis Scale 0
      pFont:        @cFontScaleEnds;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 0;
      yPos:         cColorDarkYOrgA;
      text:         '0.0';),
    ( // X Axis Scale 1
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 2;
      yPos:         cColorDarkYOrgA;
      text:         '0.5';),
    ( // X Axis Scale 2
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 4;
      yPos:         cColorDarkYOrgA;
      text:         '1.0';),
    ( // X Axis Scale 3
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 6;
      yPos:         cColorDarkYOrgA;
      text:         '1.5';),
    ( // X Axis Scale 4
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 8;
      yPos:         cColorDarkYOrgA;
      text:         '2.0';),
    ( // X Axis Scale 5
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 10;
      yPos:         cColorDarkYOrgA;
      text:         '3.0';),
    ( // X Axis Scale 6
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 12;
      yPos:         cColorDarkYOrgA;
      text:         '4.0';),
    ( // X Axis Scale 7
      pFont:        @cFontAxis;
      horAlign:     iaCenter;
      vertAlign:    cColorXAxisAlign;
      xPos:         cColorXOrgL + 14;
      yPos:         cColorDarkYOrgA;
      text:         '8.0';)
  );
  //...........................................................................

  ColorDarkYScales: array[0..cTotColorDarkYScales-1] of TTextRec = (
    ( // Y Axis Scale 0
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 2;
      text:         '9.0';),
    ( // Y Axis Scale 1
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 4;
      text:         '6.0';),
    ( // Y Axis Scale 2
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 6;
      text:         '4.0';),
    ( // Y Axis Scale 3
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 8;
      text:         '3.0';),
    ( // Y Axis Scale 4
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 10;
      text:         '2.0';),
    ( // Y Axis Scale 5
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 12;
      text:         '1.5';),
    ( // Y Axis Scale 6
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 14;
      text:         '1.0';),
    ( // Y Axis Scale 7
      pFont:        @cFontAxis;
      horAlign:     cColorYAxisAlign;
      vertAlign:    iaCenter;
//      vertAlign:    iaTop;
      xPos:         cColorXOrgA;
      yPos:         cColorYOrgL + 16;
      text:         '0.7';)
  );
  //...........................................................................

  colorDarkScaleLayer:  TScaleLayerRec = (
   horLine:(
     pen: (
    width:				1;
     color:        clLine;
     mode:         pmCopy;
     style:        psSolid;
   );
   table:          @ColorDarkHorLines;
    );
   vertLine:(
    pen: (
     width:				1;
     color:        clLine;
     mode:         pmCopy;
    style:        psSolid;
      );
    table:          @ColorDarkVertLines;
    );
    xAxis:            @ColorDarkXScales;
    yAxis:            @ColorDarkYScales;
    title: (
      pFont:        @cFontTitle;
      horAlign:     iaRight;
      vertAlign:    iaCenter;
      xTextPos:     cColorXOrgL;
      yTextPos:     1;
      barHight:     3;
      text: 'Color Dark Quality Matrix'
    );
    caption:          nil;
//    caption:          @ColorCaptions;
  );
  //...........................................................................


//------------------------------------------------------------------------------
// Color Dark Display Layer definition
//------------------------------------------------------------------------------
  //...........................................................................

  colorDarkRoughClasses:  array[0..cTotColorDarkRoughClasses-1] of TSuperClassRec = (
  ( // Rough Class 0
    PNumber: nil;
    boarder: (
    top:          cColorYOrgL + 12;
    left:					cColorXOrgL + 0;
    width:				4;
    height:				4;
    pBackground: nil );
    memberTbl: (0, 1, 8, 9, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 1
    PNumber: nil;
    boarder: (
    top:	        cColorYOrgL + 8;
    left:					cColorXOrgL + 0;
    width:				4;
    height:				4;
    pBackground: nil );
    memberTbl: (2, 3, 10, 11, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 2
    PNumber: nil;
    boarder: (
    top:          cColorYOrgL + 4;
    left:					cColorXOrgL + 0;
    width:				4;
    height:			  4;
    pBackground: nil );
    memberTbl: (4, 5, 12, 13, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 3
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL;
      left:					cColorXOrgL + 0;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (6, 7, 14, 15, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 4
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 12;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (16, 17, 24, 25, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 5
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 8;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (18, 19, 26, 27, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 6
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 4;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (20, 21, 28, 29, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 7
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL;
      left:					cColorXOrgL + 4;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (22, 23, 30, 31, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 8
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 12;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (32, 33, 40, 41, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 9
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 8;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (34, 35, 42, 43, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 10
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 4;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (36, 37, 44, 45, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 11
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL;
      left:					cColorXOrgL + 8;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (38, 39, 46, 47, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 12
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 12;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (48,49,56,57, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 13
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL + 8;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (50,51,58,59, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 14
    PNumber: nil;
    boarder: (
      top:          cColorYOrgL + 4;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (52,53,60,61, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); ),
  ( // Rough Class 15
    PNumber: nil;
    boarder: (
      top:	        cColorYOrgL;
      left:					cColorXOrgL + 12;
      width:				4;
      height:				4;
      pBackground: nil );
    memberTbl: (54,55,62,63, cSCDelimiter,0,0,0, 0,0,0,0,0,0,0,0); )
  );
  //...........................................................................

 colorDarkDisplayLayer:  TDisplayLayerRec = (
   number:(
      cuts:(
        pFont:         @cFontCuts;
//          horAlign:     iaRight;
//          vertAlign:    iaTop;
        horAlign:     iaRight;
        vertAlign:    iaTop;
        background:   clNone;
        preText:      '';
        formatText:   '%.3g';
        postText:      '';
      );
      defects:(
        pFont:        @cFontDefects;
        horAlign:     iaLeft;
        vertAlign:    iaBottom;
        background:   clNone;
        preText:      '';
        formatText:   '%.3g';
        postText:     '';
      );
    );

    roughClassTbl: @ColorDarkRoughClasses;
    defaultSuperClassTbl: nil;
  );
  //...........................................................................

//------------------------------------------------------------------------------
// Color Dark Selection Layer definition
//------------------------------------------------------------------------------
  //...........................................................................
  colorDarkSelectionLayer:  TSelectionLayerRec = (
    cutState:(
      pActiveColor:           @cCutOn;
      pPassiveColor:          nil;
    );
    fFClusterState:(
      pActiveColor:           @cFFClusterOn;
      pPassiveColor:          nil;
    );
    sCMemberState:(
      pActiveColor:           @cSCMemberOn;
      pPassiveColor:          nil;
    );
  );

  cColorDarkMatrix:   TQualityMatrixDescRec = (
    pRaster:          @colorDarkRaster;
    pField:           @colorDarkField;
    pScaleLayer:      @colorDarkScaleLayer;
    pDisplayLayer:    @colorDarkDisplayLayer;
    pSelectionLayer:  @colorSelectionLayer;
    pCurveLayer:      nil;
  );
  //...........................................................................

{*)}

implementation

end.

