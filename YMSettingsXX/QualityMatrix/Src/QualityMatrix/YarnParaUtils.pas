(*------ VSS -------------------------------------------------------------------------------
* $Archive: /LZE/PublicVcl/Code/Kr/Src/QualityMatrix/YarnParaUtils.pas $
* $Date: 20.11.03 14:44 $
------------------------------------------------------------------------------------------*)
(*==========================================================================================
| Project.......: Lynx Zentrale
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YarnParaUtils.pas
| Projectpart...: -
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT-E
| Compiler/Tools: Delphi 6
|------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.10.1999  1.00  Kr  | Initial Release (for MillMaster)
| 13.08.2002  2.00  Fel | Modified for LZE
|=========================================================================================*)
unit YarnParaUtils;

interface

uses
  Windows, YarnParadef, DbAccesstypes, SysUtils, Classes;

const
  _Version_         = '$Revision: 2 $';
  _Date_            = '$Modtime: 24.10.03 8:57 $';
  // $NoKeywords: $

const
  //------------------------------------------------------------------------------
  // Test initialisation (more or less Memory C)
  //------------------------------------------------------------------------------

  cYarnSettingsDefault: TYarnSettingsRec =
    (
    //%% Id: 0;                              //%%
    Name: ('D', 'e', 'f', 'a', 'u', 'l', 't', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');

    ChannelSettings:
    (
    NepChannel: swOn;
    NepDiameter: 1.5;
    ShortChannel: swOn;
    ShortDiameter: 1.1;
    ShortLength: 1.0;
    LongChannel: swOn;
    LongDiameter: 1.04;
    LongLength: 6;
    ThinChannel: swOn;
    ThinDiameter: - 6;
    ThinLength: 5;
    );

    ClassClear:
    (
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U'
    );

    SpliceSettings:
    (
    NepChannel: swOn;
    NepDiameter: 1.5;
    ShortChannel: swOn;
    ShortDiameter: 1.1;
    ShortLength: 1.0;
    LongChannel: swOn;
    LongDiameter: 1.04;
    LongLength: 6;
    ThinChannel: swOn;
    ThinDiameter: - 6;
    ThinLength: 5;
    UpperYarnChannel: swOn;
    UpperYarnDiameter: 1.04;
    );

    OffCount:
    (
    OffCountMonitor: swOff;
    PositiveDiameterDifference: 3.0;
    NegativeDiameterDifference: 3.0;
    CountLength: 10;
    );

    Cluster:            //%%
    (
    ShortClusterMonitor: swOff;
    ShortClusterDiameter: 1.05;
    ShortClusterLength: 0.1;
    ShortObservationLength: 0.1;
    ShortFaults: 0;
    LongClusterMonitor: swOff;
    LongClusterDiameter: 1.05;
    LongClusterLength: 0.1;
    LongObservationLength: 1;
    LongFaults: 0;
    ThinClusterMonitor: swOff;
    ThinClusterDiameter: 1.05;
    ThinClusterLength: 0.1;
    ThinObservationLength: 0.1;
    ThinFaults: 0;
    );

    SFI:
    (
    SFIMonitor: swOff;
    ReferenceType: rtFloat;
    Reference: 5;
    PositiveLimit: 5;
    NegativeLimit: 5;
    );

    FFDarkClear:
    (
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U'
    );

    FFDarkCluster:
    (
    ClusterMonitor: swOff;
    ClusterFFClear:
    (
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U',
    'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U'
    );
    ObservationLength: 0.1;
    Faults: 0;
    );
    );

  // TMachineYMConfig: Propery index for Config Code
  cCfgA             = 0;
  cCfgB             = 1;
  cCfgC             = 2;
  cCfgMachA         = 3;
  cCfgMachB         = 4;
  cCfgMachC         = 5;
  cCfgProdA         = 6;
  cCfgProdB         = 7;
  cCfgProdC         = 8;

type
  // TConfigurationCode
  TCfgFilter = (cfNone, cfProduction, cfMachine, cfDiagnosis, cfUnused);

  // Listed group numbers
  TGroupListArr = array[1..cZESpdGroupLimit] of Integer;

  //------------------------------------------------------------------------------
  // Class Definition
  //------------------------------------------------------------------------------
  TYMSettingsUtils = class(TObject)
  private
  public
    class procedure ValidateChannelSettings(var aPara: TChannelSettingsRec);
    (* class function InitMachineYMConfigRec: TMachineYMConfigRec;
    class function InitGroupSpecificMachConfigRec: TGroupSpecificMachConfigRec; *)
  end;

  //------------------------------------------------------------------------------
  // Free functions
  //------------------------------------------------------------------------------
function CalculateDiaDiffByLength(aCorseFine, aCount: Cardinal): Cardinal;
function CalculateDiaDiffByWeight(aCorseFine, aCount: Cardinal): Cardinal;

implementation

//******************************************************************************
// TYMSettingsUtils
//******************************************************************************
class procedure TYMSettingsUtils.ValidateChannelSettings(var aPara: TChannelSettingsRec);

  function ValidateSwitch(aSwitch: TSwitch): TSwitch;
  begin
    if aSwitch <> swOn then
      Result := swOff
    else
      Result := swOn;
  end;

begin
  (*%%
    with aPara do
    begin
      // Neps
      NepDiameter := FitValue(NepDiameter, cMinNepsDia, cMaxNepsDia);
      NepChannel := ValidateSwitch(NepChannel);
      // Short
      ShortDiameter := FitValue(ShortDiameter, cMinShortDia, cMaxShortDia);
      ShortChannel := ValidateSwitch(ShortChannel);
      ShortLength := FitValue(ShortLength, cMinShortLen, cMaxShortLen);
      // Long
      LongDiameter := FitValue(LongDiameter, cMinLongDia, cMaxLongDia);
      LongChannel := ValidateSwitch(LongChannel);
      LongLength := FitValue(LongLength, cMinLongLen, cMaxLongLen);
      // Thin
      thinLen := FitValue(thinLen, cMinThinLen, cMaxThinLen);
      if thinLen >= cThinLenThreshold then
        thin.dia := FitValue(thin.dia, cMinThinDia, cMaxThinDiaAbove)
      else
        thin.dia := FitValue(thin.dia, cMinThinDia, cMaxThinDiaBelow);
      thin.sw := ValidateSwitch(thin.sw);
    end;
    *)
end;

function CalculateDiaDiffByLength(aCorseFine, aCount: Cardinal): Cardinal;
var
  xScratch          : Cardinal;
begin
  try
    xScratch := Round(Sqrt(aCorseFine / aCount) * 1000);
  except
    on e: Exception do
    begin
      raise Exception.Create('CalculateDiaDiffByLength failed.' + e.Message);
    end;
  end;

  if xScratch < 1000 then
    Result := 1000 - xScratch
  else
    Result := xScratch - 1000;
end;
//------------------------------------------------------------------------------

function CalculateDiaDiffByWeight(aCorseFine, aCount: Cardinal): Cardinal;
var
  xScratch          : Cardinal;
begin

  try
    xScratch := Round(Sqrt(aCount / aCorseFine) * 1000);
  except
    on e: Exception do
    begin
      raise Exception.Create('CalculateDiaDiffByWeight failed.' + e.Message);
    end;
  end;

  if xScratch < 1000 then
    Result := 1000 - xScratch
  else
    Result := xScratch - 1000;
end;
//------------------------------------------------------------------------------

end.

