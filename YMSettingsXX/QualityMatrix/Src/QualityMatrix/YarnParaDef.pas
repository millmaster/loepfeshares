(*------ VSS -------------------------------------------------------------------------------
* $Archive: /LZE/PublicVcl/Code/Kr/Src/QualityMatrix/YarnParaDef.pas $
* $Date: 20.11.03 14:44 $
------------------------------------------------------------------------------------------*)
(*
| Project.......: Lynx Zentrale
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YarnParaDef.pas
| Projectpart...: -
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT-E
| Compiler/Tools: Delphi 6
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 31.10.1999  1.00  Kr  | Initial Release
| 13.08.2002  2.00  Fel | Modified for LZE
|=========================================================================================*)

unit YarnParaDef;

interface
uses
  DbAccessTypes;

const
  _Version_         = '$Revision: 2 $';
  _Date_            = '$Modtime: 24.10.03 8:57 $';
  // $NoKeywords: $
const
  cZESpdGroupLimit  = 12;               //Max.12 ProdGrp. per Machine %%?
type
  TIntegerChannelSettingsRec = record
    NepChannel: TSwitch;
    NepDiameter: integer;
    ShortChannel: TSwitch;
    ShortDiameter: integer;
    ShortLength: integer;
    LongChannel: TSwitch;
    LongDiameter: integer;
    LongLength: integer;
    ThinChannel: TSwitch;
    ThinDiameter: integer;
    ThinLength: integer;
  end;

  TOnConfirm = procedure(aTag: integer) of object;

  TParameterSource =
    (
    psUnknown,
    psChannel,                          // Channel Parameter Box Identifier
    psSplice,                           // Splice Parameter Box Identifier
    psCluster,                          // Cluster Parameter Box Identifier
    psYarnCount,                        // YarnCount Parameter Box Identifier
    psSFI,                              // SFI Parameter Box Identifier
    psNSLTClass,                        // NSLT Class Parameter Box Identifier
    psFFClass                           // FF Class Parameter Box Identifier
    );

  TOnParameterChange = procedure(aParameterSource: TParameterSource) of object;

const
  //------------------------------------------------------------------------------
  // Constants for Channel Settings
  //------------------------------------------------------------------------------
  cMinNepsDia       = 1.50;             // min Nep Diameter
  cMaxNepsDia       = 7.00;             // max Nep Diameter

  cMinShortDia      = 1.10;             // min Short Diameter
  cMaxShortDia      = 4.00;             // max Short Diameter

  cMinShortLen      = 1.0;              // min Short Length
  cMaxShortLen      = 10.0;             // max Short Length

  cMinLongDia       = 1.04;             // min Long Diameter
  cMaxLongDia       = 200;              // max Long Diameter

  cMinLongLen       = 6.0;              // min Long Length
  cMaxLongLen       = 200.0;            // max Long Length

  cMinThinDia       = -6.0;             // min Thin Diameter
  cMaxThinDia       = -60.0;            // max Thin Diameter

  cMaxThinDiaAbove  = 94;               // max Thin Diameter above thin length treshold, [%]
  cMaxThinDiaBelow  = 80;               // max Thin Diameter below thin length treshold, [%]

  cMinThinLen       = 2.0;              // min Thin Length, [mm]
  cMaxThinLen       = 200.0;            // max Thin Length, [mm]

  cMinDia           = cMinThinDia;
  cMaxDia           = cMaxNepsDia;
  cMinLen           = cMinShortLen;
  cMaxLen           = cMaxLongLen;

  //==== Winding Speed ====
  cMinSpeed         = 20;               // min speed   20m/Min
  cMaxSpeed         = 1600;             // max speed 1600m/Min
  cDefaultSpeed     = 900;              // [m/Min]

  //------------------------------------------------------------------------------
  // Constants for Splice Monitor
  //------------------------------------------------------------------------------
  cMinUpperYarn     = cMinLongDia;      // min UpperYarn Diameter
  cMaxUpperYarn     = cMaxLongDia;      // max UpperYarn Diameter

  cMinSpliceLen     = 6.0;              // min Splice Length
  cMaxSpliceLen     = 200.0;            // max Splice Length
  cMaxMurSpliceLen  = 99.0;             // max Splice Length for Murata Inside only

  cMinSpliceCheck   = 150;              // min Splice Check length [cm]
  cMaxSpliceCheck   = 1200;             // max Splice Check length [cm]
  cSpliceCheckOff   = 0;                // Seperate Splice Check Off
  cSpliceCheckNotInit = $FF;
  cDefaultSpliceCheck = 25;             // Default Splice Check length 25 [cm]

  cTopSixSettings   : TIntegerChannelSettingsRec =
    (
    NepChannel: swOn;
    NepDiameter: 400;
    ShortChannel: swOn;
    ShortDiameter: 220;
    ShortLength: 13;
    LongChannel: swOn;
    LongDiameter: 120;
    LongLength: 100;
    ThinChannel: swOn;
    ThinDiameter: 90;
    ThinLength: 450;
    );

  cChannelSettingsOff: TChannelSettingsRec =
    (
    NepChannel: swOff;
    NepDiameter: 4.0;
    ShortChannel: swOff;
    ShortDiameter: 2.20;
    ShortLength: 1.3;
    LongChannel: swOff;
    LongDiameter: 1.20;
    LongLength: 100;
    ThinChannel: swOff;
    ThinDiameter: - 9;
    ThinLength: 4.5;
    );
implementation

end.

