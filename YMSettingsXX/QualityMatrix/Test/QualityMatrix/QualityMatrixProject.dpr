program QualityMatrixProject;

uses
  Forms,
  QualityMatrixTest in 'QualityMatrixTest.pas' {Form1},
  YarnParaUtils in '..\..\Src\QualityMatrix\YarnParaUtils.pas',
  DisplayLayer in '..\..\Src\QualityMatrix\DisplayLayer.pas',
  QualityMatrix in '..\..\Src\QualityMatrix\QualityMatrix.pas',
  QualityMatrixBase in '..\..\Src\QualityMatrix\QualityMatrixBase.pas',
  QualityMatrixDef in '..\..\Src\QualityMatrix\QualityMatrixDef.pas',
  QualityMatrixDesc in '..\..\Src\QualityMatrix\QualityMatrixDesc.pas',
  ScaleLayer in '..\..\Src\QualityMatrix\ScaleLayer.pas',
  SelectionLayer in '..\..\Src\QualityMatrix\SelectionLayer.pas',
  YarnParaDef in '..\..\Src\QualityMatrix\YarnParaDef.pas',
  CurveLayer in '..\..\Src\QualityMatrix\CurveLayer.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
