object Form1: TForm1
  Left = 138
  Top = 521
  Width = 885
  Height = 701
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object rgMatrix: TRadioGroup
    Left = 648
    Top = 8
    Width = 105
    Height = 105
    Caption = 'Matrix'
    Items.Strings = (
      'ClassClear'
      'FFClass'
      'SpliceClass'
      'ColorClass')
    TabOrder = 0
    OnClick = rgMatrixClick
  end
  object rgSubType: TRadioGroup
    Left = 648
    Top = 120
    Width = 105
    Height = 113
    Caption = 'Sub Type'
    Items.Strings = (
      'None'
      'SiroF'
      'SiroH'
      'SiroBD'
      'ColorDark'
      'ColorDarkBright')
    TabOrder = 1
    OnClick = rgSubTypeClick
  end
  object rgMode: TRadioGroup
    Left = 648
    Top = 232
    Width = 145
    Height = 129
    Caption = 'Mode'
    Items.Strings = (
      'Display'
      'SelectSettings'
      'SelectClass'
      'SelectFFCluster'
      'SelectSCMember')
    TabOrder = 2
    OnClick = rgModeClick
  end
  object rgSettings: TRadioGroup
    Left = 648
    Top = 376
    Width = 145
    Height = 169
    Caption = 'Settings'
    Items.Strings = (
      'Test'
      'Default'
      'Diagonal'
      'All Class'
      'All FF Cluster'
      'All Both'
      'None')
    TabOrder = 3
    OnClick = rgSettingsClick
  end
  object rgLastCut: TRadioGroup
    Left = 656
    Top = 556
    Width = 113
    Height = 81
    Caption = 'LastCut'
    Items.Strings = (
      'None'
      'Field'
      'Value')
    TabOrder = 4
    OnClick = rgLastCutClick
  end
  object clFlags: TCheckListBox
    Left = 760
    Top = 8
    Width = 113
    Height = 113
    ItemHeight = 13
    Items.Strings = (
      'Channel'
      'Splice'
      'Cluster'
      'ActiveFields'
      'Enabled'
      'TitleEnabled'
      'ValidateCuts'
      'ValidateDefects')
    TabOrder = 5
    OnClick = clFlagsClick
  end
  object seLastCut: TSpinEdit
    Left = 720
    Top = 584
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 6
    Value = 0
    OnChange = seLastCutChange
  end
  object rgDisplayMode: TRadioGroup
    Left = 496
    Top = 520
    Width = 121
    Height = 105
    Caption = 'DisplayMode'
    Items.Strings = (
      'None'
      'Values'
      'Color'
      'Dots'
      'SCValues'
      'CalculateSCValues')
    TabOrder = 7
    OnClick = rgDisplayModeClick
  end
end
