unit QualityMatrixTest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ExtCtrls, Spin, QualityMatrix, QualityMatrixDef,
  DbAccessTypes, PresetedYarnSettings;

type
  TForm1 = class(TForm)
    rgMatrix: TRadioGroup;
    rgSubType: TRadioGroup;
    rgMode: TRadioGroup;
    rgSettings: TRadioGroup;
    rgLastCut: TRadioGroup;
    clFlags: TCheckListBox;
    seLastCut: TSpinEdit;
    rgDisplayMode: TRadioGroup;
    procedure rgMatrixClick(Sender: TObject);
    procedure rgSubTypeClick(Sender: TObject);
    procedure rgModeClick(Sender: TObject);
    procedure rgSettingsClick(Sender: TObject);
    procedure rgLastCutClick(Sender: TObject);
    procedure clFlagsClick(Sender: TObject);
    procedure seLastCutChange(Sender: TObject);
    procedure rgDisplayModeClick(Sender: TObject);
  private
    { Private declarations }
    mQualityMatrix: TQualityMatrix;
    mPresetedYarnSettings: TPresetedYarnSettings;
    mDarkClass, mBrightClass, mDarkCluster, mBrightCluster: T64ClassFields;
    procedure UpdateClFlags;

  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;


  end;

const
  cMemoryTemplate: TYarnSettingsRec =
  (
    Name: 'Memory TPL';

    // "Edit Settings"Channelsettings
    ChannelSettings:
    (
    NepChannel: swOn; // SET default 1=On
    NepDiameter: 4.9; // default 1.5   neps Diameter, 1.5..7.0 / OFF * /
    ShortChannel: swOn; // SET default 1=On
    ShortDiameter: 1.3; // default 1.10  short Diameter, 1.1..4.0 / OFF * /
    ShortLength: 2.2; // default 1.0   1.0..10.0 cm * /
    LongChannel: swOn; // SET default 1=On
    LongDiameter: 1.2; // default 1.04  long Diameter, 1.04..2.0 / OFF * /
    LongLength: 21; // default 6     long Length, 6..200 cm * /
    ThinChannel: swOn; // SET default 1=On
    ThinDiameter: - 25; // default -6    thin Diameter, -6.. - 60 % / OFF * /
    ThinLength: 90; // default 5     thin Length, 2..200 cm * /
(*
      NepChannel: swOn;                   // SET default 1=On
      NepDiameter: 6.0;                   // default 1.5   neps Diameter, 1.5..7.0 / OFF * /
      ShortChannel: swOn;                 // SET default 1=On
      ShortDiameter: 2.6;                 // default 1.10  short Diameter, 1.1..4.0 / OFF * /
      ShortLength: 1.3;                   // default 1.0   1.0..10.0 cm * /
      LongChannel: swOn;                  // SET default 1=On
      LongDiameter: 1.3;                  // default 1.04  long Diameter, 1.04..2.0 / OFF * /
      LongLength: 40;                     // default 6     long Length, 6..200 cm * /
      ThinChannel: swOn;                  // SET default 1=On
      ThinDiameter: -25;                 // default -6    thin Diameter, -6.. - 60 % / OFF * /
      ThinLength: 90;                     // default 5     thin Length, 2..200 cm * /
*)
    );

    // Short Class and Long/Thin Class
    // booklet: - ==> here: U
    //          C ==> here: C
    ClassClear: // array[ 0..127 ] of AnsiChar;
      // "Short Class" = lower half of AnsiString
      // 12345678
    'UUUUUUUU' + // 1
    'UUUUUUUC' + // 2
    'UUUUUUCC' + // 3
    'UUUUUCCC' + // 4
    'UUUUCCCC' + // 5
    'UUUCCCCC' + // 6
    'UUUCCCCC' + // 7
    'UCCCCCCC' + // 8

      // "Long / Thin Class" = upper half of AnsiString
      // 12345678
    'UUUUUUUU' + // 1
    'UUUUUUUU' + // 2
    'UUUUUUUU' + // 3
    'UUUUUUUU' + // 4
    'UUUUUUUC' + // 5
    'CUUUUUUC' + // 6
    'CUUUUUCC' + // 7
    'CCUUUCCC'; // 8

    // "Optional Settings 2"
    SpliceSettings: // TSpliceSettingsRec
    (
    NepChannel: swOn; // default 1=On
    NepDiameter: 1.5; // default 1.5   neps Diameter, 1.5..7.0 / OFF * /
    ShortChannel: swOn; // default 1=On
    ShortDiameter: 2.6; // default 1.10  short Diameter, 1.1..4.0/ OFF */
    ShortLength: 4.0; // default 1.0   1.0..10.0 cm * /
    LongChannel: swOn; // default 1=On
    LongDiameter: 1.3; // default 1.04  long Diameter,1.04..2.0 / OFF * /
    LongLength: 20; // default 6, / * long Length, 6..200 cm * /
    ThinChannel: swOn; // default 1=On,
    ThinDiameter: - 6; // default - 6, / * thin Diameter, -6.. - 60 % / OFF * /
    ThinLength: 2; // default 5, / * thin Length, 2..200 cm * /

      // not for Spectra: Upper Yarn Channel / Diameter

    UpperYarnChannel: swOn; // default 1=On, AVAILABILITY OF SETTINGS! specsMMI
    UpperYarnDiameter: 1.50; // default 1.04, / * upper yarn Diameter, 1.04..2.0 * /
    );


    // "Optional settings -> Yarn Count" (on extra sheet)
    OffCount:
    (
    OffCountMonitor: swOn; // default 0=Off,
    PositiveDiameterDifference: 25.0; // default 3.0, / * Positive Diameter difference of Yarn Count, 3.0..44.0 * /
    NegativeDiameterDifference: - 25.0; // default 3.0, / * Negative Diameter difference of Yarn Count, 3.0..44.0 * /
    CountLength: 10; // default 10, / * Yarn Count Count length, 10, 20, 30, 40, 50 m * /
    );


    // New "Optional settings -> Yarn Count SHORT"  (on extra sheet)
    ShortOffCount:
    (
    OffCountMonitor: swOn; // default 0=Off,
    PositiveDiameterDifference: 25.0; // default 3.0,  Positive Diameter difference of Yarn Count, 3.0..44.0
    NegativeDiameterDifference: - 25.0; // default 3.0,  Negative Diameter difference of Yarn Count, 3.0..44.0
    CountLength: 12; // default 10,  length 1..32 m
    );

    // NEW: "Cluster Settings"
    Cluster:
    (
    ShortClusterMonitor: swOff; // default 0=Off,
    ShortClusterChannel: swOn; // default 0=Off,
    ShortClusterDiameter: 2.44; // default 1.05, Short Cluster Diameter, 1.05..4.0
    ShortClusterLength: 1.3; // default 1.0, Short Cluster length, 1.0..10.0 cm
    ShortObservationLength: 80; // default 0.1, Short Observation length, 0.1..80. m
    ShortFaults: 1; // default 0,  Short Faults per observationlength 1 - 9999
    LongClusterMonitor: swOff; // default 0=Off,
    LongClusterChannel: swOn; // default 0=Off,
    LongClusterDiameter: 1.3; // default 1.05, Long Cluster Diameter, 1.04..2.0
    LongClusterLength: 40.0; // default 6, Long Cluster length, 6..200 cm
    LongObservationLength: 80; // default 0.1, Long Observation length, 0.1..80.0 m
    LongFaults: 1; // default 0, Long Faults observationlength 1 - 9999
    ThinClusterMonitor: swOff; // default 0=Off,
    ThinClusterChannel: swOn; // default 0=Off,
    ThinClusterDiameter: - 25.0; // default -6, Thin Cluster Diameter, -6..-60 %
    ThinClusterLength: 90.0; // default 6, Thin Cluster length, 6..200 cm
    ThinObservationLength: 80; // default 0.1, Thin Observation length, 0.1..80. m
    ThinFaults: 1; // default 0, Thin Faults observationlength 1 - 9999
    );

    // "Optional Settings 2" 2nd Column
    SFI:
    (
    SFIMonitor: swOn; // default 0=Off,
    ReferenceType: rtConstant; // default 1=float //rtFloat;
    Reference: 13.5; // default 5.0, / * SFI constant reference 5..25 * /
                              //* SFI constant reference 5..25 * /
    PositiveLimit: 35.0; // default 5,   /* SFI upper value in percent 5..40 % * /
    NegativeLimit: - 35.0; // default 5,   /* SFI lower value in percent 5..40 % * /
    );

    // "FF CLEAR Class"   Foreign Fiber
    FFDarkClear:
      // 01234567
    'UUUUUUUU' + // 1
    'UUUUUUUU' + // 2
    'UUUUUUCC' + // 3
    'UUUUCCCC' + // 4
    'UUUCCCCC' + // 5
    'UUCCCCCC' + // 6
    'UUCCCCCC' + // 7
    'UUCCCCCC'; // 8

    // New, out of use -> Uncut
    FFBrightClear:
      // 01234567
    'UUUUUUUU' + // 1
    'UUUUUUUU' + // 2
    'UUUUUUUU' + // 3
    'UUUUUUUU' + // 4
    'UUUUUUUU' + // 5
    'UUUUUUUU' + // 6
    'UUUUUUUU' + // 7
    'UUUUUUUU'; // 8

    FFDarkCluster: // Like FFDarkClear plus stars
    (
    ClusterMonitor: swOn; // default 0=Off,
      // FF Clear Class  --> Stars "*" AND "C"s,              (On extra sheet!)
    ClusterFFClear: //T64ClassFields  char(64) default 'U',
                     //(filled with C or U)
      // 01234567
    'UUUUUUUU' + // 1
    'UUUUUUUU' + // 2
    'UUUUUUCC' + // 3
    'UUCCCCCC' + // 4
    'UUCCCCCC' + // 5
    'UUCCCCCC' + // 6
    'UUCCCCCC' + // 7
    'UUCCCCCC'; // 8
    ObservationLength: 80.0; // SET default 0.1, / * Observation length, 0.1..300. m * /
    Faults: 16; // SET default 0, / * Defects per observationlength 1 - 9999 * /
    );


    FFBrightCluster: // New, not used yet --> Uncut
    (
    ClusterMonitor: swOff;
    ClusterFFClear:
      // 01234567
    'UUUUUUUU' + // 1
    'UUUUUUUU' + // 2
    'UUUUUUUU' + // 3
    'UUUUUUUU' + // 4
    'UUUUUUUU' + // 5
    'UUUUUUUU' + // 6
    'UUUUUUUU' + // 7
    'UUUUUUUU'; // 8
    ObservationLength: 0.1;
    Faults: 0;
    );

    SynFF:
    (
      SynFFMonitor: swOn; //       default 0=Off,
      Limit: 4; // 0.5..9.9
      RefLength: 2.5; //  0.5..4.0; 2.5
    )

    ); // end Memory Template



var
  Form1: TForm1;

implementation

{$R *.dfm}

constructor TForm1.Create(aOwner: TComponent);
begin
  inherited;

  mQualityMatrix := TQualityMatrix.Create(aOwner);

  with mQualityMatrix do
  begin
    Parent := Form1;
//    Color := clSilver;
//    Parent := self;
//	  Align := alClient;
//    Align := alNone;
    Width := 500;
    Height := 250;
    MatrixType := mtShortLongThin;

    UpdateClFlags;
//    mPresetedYarnSettings := TPresetedYarnSettings.Instance;
  end;

end;

procedure TForm1.rgMatrixClick(Sender: TObject);
begin

  case rgMatrix.ItemIndex of
    0:
      begin
        mQualityMatrix.MatrixType := mtShortLongThin;
        mQualityMatrix.MatrixSubType := mstNone;
        mQualityMatrix.SubFieldX := 6;
        mQualityMatrix.Title := 'SortLongThin';
      end;
    1:
      begin
        mQualityMatrix.MatrixType := mtSiro;
//        mQualityMatrix.MatrixSubType := mstSiroF;
        mQualityMatrix.Title := 'Siro';
      end;
    2:
      begin
        mQualityMatrix.MatrixType := mtSplice;
        mQualityMatrix.MatrixSubType := mstNone;
        mQualityMatrix.Title := 'Splice';
      end;
    3:
      begin
        mQualityMatrix.MatrixType := mtColor;
        mQualityMatrix.MatrixSubType := mstNone;
        mQualityMatrix.Title := 'Color';
      end;
  end;


  UpdateClFlags;
end;

procedure TForm1.rgSubTypeClick(Sender: TObject);
begin
  case rgSubType.ItemIndex of
    0:
      mQualityMatrix.MatrixSubType := mstNone;
    1:
      begin
//        mQualityMatrix.MatrixType := mtSiro;
        mQualityMatrix.MatrixSubType := mstSiroF;
      end;
    2:
      begin
//        mQualityMatrix.MatrixType := mtSiro;
        mQualityMatrix.MatrixSubType := mstSiroH;
      end;
    3:
      mQualityMatrix.MatrixSubType := mstSiroBD;
    4:
      mQualityMatrix.MatrixSubType := mstColorDark;
    5:
      mQualityMatrix.MatrixSubType := mstColorDarkBright;
  end;

end;

procedure TForm1.rgModeClick(Sender: TObject);
var
  i: Integer;
begin

  with mQualityMatrix do
  begin
    case rgMode.ItemIndex of
      0:
        MatrixMode := mmDisplayData;
      1:
        MatrixMode := mmSelectSettings;
      2:
        MatrixMode := mmSelectCutFields;
      3:
        MatrixMode := mmSelectFFClusterFields;
      4:
        MatrixMode := mmSelectSCMembers;
    end;

    for i := 0 to (TotValues - 1) do
    begin
//      Cuts[i] := i * 10;
      Cuts[i] := i;
//      Defects[i] := (TotValues - i) * 100;
      Defects[i] := (TotValues - i - 1);
    end;

  end;

end;

procedure TForm1.rgSettingsClick(Sender: TObject);
var
  i, j, h, sz: Integer;
  xSettings: TYarnSettingsRec;
begin

  with mQualityMatrix do
  begin
    case rgSettings.ItemIndex of
      0:
        begin
(*
          xSettings := mPresetedYarnSettings.MemoryC;
*)
          xSettings := cMemoryTemplate;
          PutYMSettings(xSettings);

//          GetFieldStateTable(mDarkClass, mBrightClass, mDarkCluster, mBrightCluster);
        end;
      1:
        begin
//        PutYMSettings(cYMSettingsTest);
          xSettings := mPresetedYarnSettings.MemoryD;
          PutYMSettings(xSettings);
//         SetFieldStateTable(mDarkClass, mBrightClass, mDarkCluster, mBrightCluster);
        end;
      2:
        begin
          j := 0;
          h := 0;
          sz := 8;
          for i := 0 to (TotFields - 1) do
          begin
            if i = (j * 8 + h) then
            begin
              FieldState[i] := True;
              Inc(j);
              if Boolean(j mod sz) then
                Inc(h)
              else
                h := 0;
            end
            else
              FieldState[i] := False;
          end;

          mQualityMatrix.SubFieldX := 8;

          mQualityMatrix.SubFieldX := 25;
        end;

      3:
        begin
          xSettings := mPresetedYarnSettings.MemoryE;

          for i := 0 to High(xSettings.classClear) do
            xSettings.classClear[i] := cClassCut;

          for i := 0 to High(xSettings.FFDarkClear) do
          begin

            xSettings.FFDarkClear[i] := cClassCut;
            xSettings.FFBrightClear[i] := cClassCut;
            xSettings.FFDarkCluster.ClusterFFClear[i] := cClassCut;
            xSettings.FFBrightCluster.ClusterFFClear[i] := cClassCut;
          end;

          PutYMSettings(xSettings);
        end;

      4:
        begin
          xSettings := mPresetedYarnSettings.MemoryA;

//          for i := 0 to High(xSettings.classClear) do
//            xSettings.classClear[i] := cClassUnCut;

          for i := 0 to High(xSettings.FFDarkClear) do
          begin

            xSettings.FFDarkClear[i] := cClassUnCut;
            xSettings.FFBrightClear[i] := cClassUnCut;
            xSettings.FFDarkCluster.ClusterFFClear[i] := cClassCut;
            xSettings.FFBrightCluster.ClusterFFClear[i] := cClassCut;
          end;
          PutYMSettings(xSettings);
        end;

      5:
        begin
          xSettings := mPresetedYarnSettings.MemoryA;

          for i := 0 to High(xSettings.classClear) do
            xSettings.classClear[i] := cClassCut;


          for i := 0 to High(xSettings.FFDarkClear) do
          begin

            xSettings.FFDarkClear[i] := cClassCut;
            xSettings.FFBrightClear[i] := cClassCut;
            xSettings.FFDarkCluster.ClusterFFClear[i] := cClassUnCut;
            xSettings.FFBrightCluster.ClusterFFClear[i] := cClassUnCut;
          end;
          PutYMSettings(xSettings);
        end;

      6:
        begin
          xSettings := mPresetedYarnSettings.MemoryA;

          for i := 0 to High(xSettings.classClear) do
            xSettings.classClear[i] := cClassUnCut;

          for i := 0 to High(xSettings.FFDarkClear) do
          begin

            xSettings.FFDarkClear[i] := cClassUnCut;
            xSettings.FFBrightClear[i] := cClassUnCut;
            xSettings.FFDarkCluster.ClusterFFClear[i] := cClassUnCut;
            xSettings.FFBrightCluster.ClusterFFClear[i] := cClassUnCut;
          end;
          PutYMSettings(xSettings);
        end;

    end;
  end;

end;

procedure TForm1.rgLastCutClick(Sender: TObject);
begin

  with mQualityMatrix do
  begin
    case rgLastCut.ItemIndex of
      0:
        LastCutMode := lcNone;
      1:
        LastCutMode := lcfield;
      2:
        LastCutMode := lcValue;
    end;
  end;

end;

procedure TForm1.clFlagsClick(Sender: TObject);
begin
  with mQualityMatrix do
  begin

    ChannelVisible := clFlags.Checked[0];
    SpliceVisible := clFlags.Checked[1];
    ClusterVisible := clFlags.Checked[2];
    ActiveVisible := clFlags.Checked[3];
    Enabled := clFlags.Checked[4];
    TitleEnabled := clFlags.Checked[5];
    if clFlags.Checked[5] then
    begin
      mQualityMatrix.FontSize := 12;
      mQualityMatrix.FontCharSet := 4;
      mQualityMatrix.Title := 'Title enabled';
    end;
    ValidateCuts := clFlags.Checked[6];
    ValidateDefects := clFlags.Checked[7];


  end;

end;

procedure TForm1.seLastCutChange(Sender: TObject);
begin

  mQualityMatrix.LastCutField := seLastCut.Value;

end;

destructor TForm1.Destroy;
begin

//  mQualityMatrix.Free;

  inherited;
end;

procedure TForm1.UpdateClFlags;
begin

  with mQualityMatrix do
  begin
    clFlags.Checked[0] := ChannelVisible;
    clFlags.Checked[1] := SpliceVisible;
    clFlags.Checked[2] := ClusterVisible;
    clFlags.Checked[3] := ActiveVisible;
    clFlags.Checked[4] := Enabled;
    clFlags.Checked[5] := TitleEnabled;
    clFlags.Checked[6] := ValidateCuts;
    clFlags.Checked[7] := ValidateDefects;
  end;
end;

procedure TForm1.rgDisplayModeClick(Sender: TObject);
begin

  case rgDisplayMode.ItemIndex of
    0:
      mQualityMatrix.DisplayMode := dmNone;
    1:
      mQualityMatrix.DisplayMode := dmValues;
    2:
      mQualityMatrix.DisplayMode := dmColor;
    3:
      mQualityMatrix.DisplayMode := dmDots;
    4:
      mQualityMatrix.DisplayMode := dmSCValues;
    5:
      mQualityMatrix.DisplayMode := dmCalculateSCValues
  end;

end;

end.

