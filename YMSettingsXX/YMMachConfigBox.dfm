object GBMachConfigBox: TGBMachConfigBox
  Left = 0
  Top = 0
  Width = 969
  Height = 378
  TabOrder = 0
  object mllGarnreiniger: TmmLineLabel
    Left = 0
    Top = 0
    Width = 969
    Height = 89
    Align = alTop
    AutoSize = False
    Caption = '(*)Garnreiniger'
    Distance = 2
  end
  object mllFacility: TmmLineLabel
    Left = 0
    Top = 89
    Width = 969
    Height = 224
    Align = alTop
    AutoSize = False
    Caption = '(*)Anlage'
    Distance = 2
  end
  object mlaSensingHead: TmmLabel
    Left = 11
    Top = 36
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(17)Tastkopf:'
    FocusControl = mcobSensingHead
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbCutRetries: TmmLabel
    Left = 787
    Top = 60
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Schnittwiederholungen:'
    FocusControl = medCutRetries
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mllConfigCode: TmmLineLabel
    Left = 0
    Top = 313
    Width = 969
    Height = 65
    Align = alClient
    AutoSize = False
    Caption = '(*)Konfigurations Code'
    OnClick = DoChange
    Distance = 2
  end
  object mlbZESwVersionlb: TmmLabel
    Left = 11
    Top = 120
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)ZE Software Version:'
    FocusControl = mlbZESwVersionStr
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbZESwVersionStr: TmmLabel
    Left = 136
    Top = 120
    Width = 34
    Height = 13
    Caption = 'V10.13'
    Visible = True
    AutoLabel.Control = mlbZESwVersionlb
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbZESwOptionlb: TmmLabel
    Left = 11
    Top = 140
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)ZE Software Option:'
    FocusControl = mlbZESwOptionStr
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbZESwOptionStr: TmmLabel
    Left = 136
    Top = 140
    Width = 43
    Height = 13
    Caption = 'LabPack'
    Visible = True
    AutoLabel.Control = mlbZESwOptionlb
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbSPCheckLength: TmmLabel
    Left = 11
    Top = 228
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Spleissprueflaenge:'
    FocusControl = medSPCheckLength
    Visible = True
    AutoLabel.Control = mplbSPCheckLength
    AutoLabel.Distance = 51
    AutoLabel.LabelPosition = lpRight
  end
  object mlbLongStopDef: TmmLabel
    Left = 715
    Top = 252
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Lang-Stopp Dauer:'
    FocusControl = medLongStopDef
    Visible = True
    AutoLabel.Control = mplbLongStopDef
    AutoLabel.Distance = 51
    AutoLabel.LabelPosition = lpRight
  end
  object mplbLongStopDef: TmmLabel
    Left = 886
    Top = 252
    Width = 32
    Height = 13
    Caption = '(4)Min.'
    FocusControl = mlbLongStopDef
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbKnifePower: TmmLabel
    Left = 715
    Top = 276
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Messertrennleistung:'
    FocusControl = mcobKnifePower
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbCfgCodeALabel: TmmLabel
    Left = 14
    Top = 344
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)A:'
    FocusControl = mlbCfgCodeA
    Visible = True
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbCfgCodeA: TGBConfigCode
    Left = 136
    Top = 344
    Width = 30
    Height = 13
    AutoSize = False
    Caption = '42405'
    Visible = True
    AutoLabel.Control = mlbCfgCodeALabel
    AutoLabel.LabelPosition = lpLeft
    ConfigCode = 42405
    OnChange = DoChange
  end
  object mlbCfgCodeBLabel: TmmLabel
    Left = 352
    Top = 347
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)B:'
    FocusControl = mlbCfgCodeB
    Visible = True
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbCfgCodeB: TGBConfigCode
    Tag = 1
    Left = 474
    Top = 347
    Width = 30
    Height = 13
    AutoSize = False
    Caption = '42405'
    Visible = True
    AutoLabel.Control = mlbCfgCodeBLabel
    AutoLabel.LabelPosition = lpLeft
    ConfigCode = 42405
    OnChange = DoChange
  end
  object mlbCfgCodeCLabel: TmmLabel
    Left = 718
    Top = 347
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)C:'
    FocusControl = mlbCfgCodeC
    Visible = True
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbCfgCodeC: TGBConfigCode
    Tag = 2
    Left = 840
    Top = 347
    Width = 30
    Height = 13
    AutoSize = False
    Caption = '42405'
    Visible = True
    AutoLabel.Control = mlbCfgCodeCLabel
    AutoLabel.LabelPosition = lpLeft
    ConfigCode = 42405
    OnChange = DoChange
  end
  object mplbSPCheckLength: TmmLabel
    Left = 182
    Top = 228
    Width = 26
    Height = 13
    Caption = '(4)cm'
    FocusControl = mlbSPCheckLength
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbHeadstock: TmmLabel
    Left = 11
    Top = 204
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Maschinenkopf:'
    FocusControl = mcobHeadstock
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlaAWETypeStr: TmmLabel
    Left = 136
    Top = 56
    Width = 43
    Height = 13
    Caption = 'AWE800'
    Visible = True
    AutoLabel.Control = mlaAWEType
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object mlaAWEType: TmmLabel
    Left = 11
    Top = 56
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(17)AWE Typ:'
    FocusControl = mlaAWETypeStr
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbDefaultSpeedRamp: TmmLabel
    Left = 11
    Top = 276
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Anlaufdauer:'
    FocusControl = medDefaultSpeedRamp
    Visible = True
    AutoLabel.Control = mplbDefaultSpeedRamp
    AutoLabel.Distance = 51
    AutoLabel.LabelPosition = lpRight
  end
  object mplbDefaultSpeedRamp: TmmLabel
    Left = 182
    Top = 276
    Width = 5
    Height = 13
    Caption = 's'
    FocusControl = mlbDefaultSpeedRamp
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbDefaultSpeed: TmmLabel
    Left = 11
    Top = 252
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Geschwindigkeit:'
    FocusControl = medDefaultSpeed
    Visible = True
    AutoLabel.Control = mplbDefaultSpeed
    AutoLabel.Distance = 51
    AutoLabel.LabelPosition = lpRight
  end
  object mplbDefaultSpeed: TmmLabel
    Left = 182
    Top = 252
    Width = 29
    Height = 13
    Caption = 'm/min'
    FocusControl = mlbDefaultSpeed
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbAWEMachineTypelb: TmmLabel
    Left = 11
    Top = 160
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)AWE Maschinentyp:'
    FocusControl = mlbAWEMachineTypeStr
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mlbAWEMachineTypeStr: TmmLabel
    Left = 136
    Top = 160
    Width = 32
    Height = 13
    Caption = 'AC338'
    Visible = True
    AutoLabel.Control = mlbAWEMachineTypelb
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object mcobSensingHead: TmmComboBox
    Left = 136
    Top = 32
    Width = 89
    Height = 21
    Style = csDropDownList
    Color = clWindow
    ItemHeight = 13
    TabOrder = 0
    Visible = True
    OnChange = DoChange
    AutoLabel.Control = mlaSensingHead
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object mcbAdjustCut: TmmCheckBox
    Left = 288
    Top = 32
    Width = 170
    Height = 17
    Caption = '(25)Schnitt vor Abgleich'
    TabOrder = 1
    Visible = True
    OnClick = DoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbZeroAdjust: TmmCheckBox
    Tag = 2
    Left = 504
    Top = 32
    Width = 185
    Height = 17
    Caption = '(25)Null Abgleich Ueberwachung'
    TabOrder = 3
    Visible = True
    OnClick = DoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbDriftConeChange: TmmCheckBox
    Tag = 3
    Left = 504
    Top = 56
    Width = 233
    Height = 17
    Caption = '(40)Driftkompensation nach Kreuzspulwechsel'
    TabOrder = 4
    Visible = True
    OnClick = DoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbKnifeMonitor: TmmCheckBox
    Tag = 4
    Left = 784
    Top = 32
    Width = 150
    Height = 17
    Caption = '(20)Schnittueberwachung'
    TabOrder = 5
    Visible = True
    OnClick = DoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbAdjustRemove: TmmCheckBox
    Tag = 1
    Left = 288
    Top = 56
    Width = 177
    Height = 17
    Caption = '(25)Garn nach Abgleich absaugen'
    TabOrder = 2
    Visible = True
    OnClick = DoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object medSPCheckLength: TKeySpinEdit
    Tag = 1
    Left = 136
    Top = 224
    Width = 41
    Height = 21
    Color = clWindow
    TabOrder = 18
    Visible = True
    AutoLabel.Control = mlbSPCheckLength
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    TextType = ttFloat
    OnConfirm = DoConfirm
  end
  object medCutRetries: TKeySpinEdit
    Left = 912
    Top = 56
    Width = 15
    Height = 21
    Color = clWindow
    TabOrder = 6
    Text = '2.00'
    Visible = True
    AutoLabel.Control = mlbCutRetries
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    TextType = ttFloat
    Value = 2
    OnConfirm = DoConfirm
  end
  object medLongStopDef: TKeySpinEdit
    Tag = 2
    Left = 840
    Top = 248
    Width = 41
    Height = 21
    Color = clWindow
    TabOrder = 19
    Visible = True
    AutoLabel.Control = mlbLongStopDef
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    TextType = ttFloat
    OnConfirm = DoConfirm
  end
  object mcbSpindleBlockOnAlarm: TmmCheckBox
    Left = 352
    Top = 112
    Width = 255
    Height = 17
    Caption = '(40)Blockierung  bei Alarm'
    TabOrder = 7
    Visible = True
    OnClick = SpindleBlockOnAlarmClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbCountBlock: TmmCheckBox
    Tag = 1
    Left = 352
    Top = 136
    Width = 255
    Height = 17
    Caption = '(40)Blockierung bei Nummerabweichungs-Alarm'
    TabOrder = 8
    Visible = True
    OnClick = SpindleBlockOnAlarmClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbClusterBlock: TmmCheckBox
    Tag = 2
    Left = 352
    Top = 160
    Width = 255
    Height = 17
    Caption = '(40)Blockierung bei Fehlerschwarm-Alarm'
    TabOrder = 9
    Visible = True
    OnClick = SpindleBlockOnAlarmClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbSFIBlock: TmmCheckBox
    Tag = 3
    Left = 352
    Top = 184
    Width = 255
    Height = 17
    Caption = '(40)Blockierung bei SFI-Alarm'
    TabOrder = 10
    Visible = True
    OnClick = SpindleBlockOnAlarmClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbFFBlock: TmmCheckBox
    Tag = 4
    Left = 352
    Top = 208
    Width = 255
    Height = 17
    Caption = '(40)Blockierung bei FF-Alarm'
    TabOrder = 11
    Visible = True
    OnClick = SpindleBlockOnAlarmClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbFFClusterBlock: TmmCheckBox
    Tag = 5
    Left = 352
    Top = 232
    Width = 255
    Height = 17
    Caption = '(40)Blockierung bei FF-Schwarm-Alarm'
    TabOrder = 12
    Visible = True
    OnClick = SpindleBlockOnAlarmClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbCutFailBlock: TmmCheckBox
    Tag = 6
    Left = 352
    Top = 256
    Width = 255
    Height = 17
    Caption = '(40)Blockierung bei Schnittausfall'
    TabOrder = 13
    Visible = True
    OnClick = SpindleBlockOnAlarmClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbExtMurItf: TmmCheckBox
    Tag = 6
    Left = 712
    Top = 136
    Width = 255
    Height = 17
    Caption = '(40)Erweiterte Murata Schnittstelle'
    TabOrder = 14
    Visible = True
    OnClick = DoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbOneDrumPuls: TmmCheckBox
    Tag = 7
    Left = 712
    Top = 160
    Width = 255
    Height = 17
    Caption = '(40)Seed Simulation mit 1NTP/Umdrehung'
    TabOrder = 15
    Visible = True
    OnClick = DoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcbConeChangeCondition: TmmCheckBox
    Tag = 8
    Left = 712
    Top = 184
    Width = 255
    Height = 17
    Caption = '(40)Kreuzspulwechsel Interpretation aktiv'
    TabOrder = 16
    Visible = True
    OnClick = DoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mcobKnifePower: TmmComboBox
    Tag = 2
    Left = 840
    Top = 272
    Width = 73
    Height = 21
    Color = clWindow
    ItemHeight = 13
    TabOrder = 20
    Visible = True
    OnChange = DoChange
    AutoLabel.Control = mlbKnifePower
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object mcobHeadstock: TmmComboBox
    Tag = 1
    Left = 136
    Top = 200
    Width = 57
    Height = 21
    Color = clWindow
    ItemHeight = 13
    TabOrder = 17
    Visible = True
    OnChange = DoChange
    AutoLabel.Control = mlbHeadstock
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object medDefaultSpeedRamp: TKeySpinEdit
    Tag = 4
    Left = 136
    Top = 272
    Width = 41
    Height = 21
    Color = clWindow
    TabOrder = 21
    Visible = True
    AutoLabel.Control = mlbDefaultSpeedRamp
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    TextType = ttFloat
    OnConfirm = DoConfirm
  end
  object medDefaultSpeed: TKeySpinEdit
    Tag = 3
    Left = 136
    Top = 248
    Width = 41
    Height = 21
    Color = clWindow
    TabOrder = 22
    Visible = True
    AutoLabel.Control = mlbDefaultSpeed
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    TextType = ttFloat
    OnConfirm = DoConfirm
  end
  object mcbUpperYarnCheck: TmmCheckBox
    Tag = 5
    Left = 712
    Top = 112
    Width = 255
    Height = 17
    Caption = '(40)Oberfadenpruefung'
    TabOrder = 23
    Visible = True
    OnClick = DoClick
    AutoLabel.LabelPosition = lpLeft
  end
end
