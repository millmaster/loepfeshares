(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMParaEditBox.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: EditBoxes for YarnMaster Settings.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 15.11.1999  0.00  Kr  | Initial Release
| 04.09.2003  0.01  Kr  | SFI/D-Reference auf eine Kommastelle genau.
| 11.09.2003  0.01  Kr  | -Limit keine Kommastelle
|=============================================================================*)
unit YMParaEditBox;

interface

uses Classes, SysUtils, EditBox, KeySpinEdit, YMParaDef, YMParaUtils,
  MMUGlobal, PresetValue, IvDictio;

  //...........................................................................

resourcestring
  rsCHCaption = '(8)Kanal'; //ivlm
  rsSPCaption = '(8)Spleiss'; //ivlm
  rsSwOff = '(3)&Aus'; //ivlm
  rsMeasureProcent = '(1)%';  //ivlm
  rsMeasureCm = '(2)cm'; //ivlm
  rsNepDiameter = '(2)N'; //ivlm
  rsNepDiameterHint = '(*)Noppen Durchmesser:'; //ivlm
  rsShortDiameter = '(2)DS'; //ivlm
  rsShortDiameterHint = '(*)Kurzfehler Durchmesser:'; //ivlm
  rsShortLength = '(2)LS'; //ivlm
  rsShortLengthHint = '(*)Kurzfehler Laenge:'; //ivlm
  rsLongDiameter = '(2)DL'; //ivlm
  rsLongDiameterHint = '(*)Langfehler Durchmesser:'; //ivlm
  rsLongLength = '(2)LL'; //ivlm
  rsLongLengthHint = '(*)Langfehler Laenge:'; //ivlm
  rsThinDiameter = '(2)-D'; //ivlm
  rsThinDiameterHint = '(*)Duennfehler Durchmesser:'; //ivlm
  rsThinLength = '(2)-L'; //ivlm
  rsThinLengthHint = '(*)Duennfehler Laenge:'; //ivlm

  rsUpYDiameter = '(3)UpY'; //ivlm
  rsUpYDiameterHint = '(*)Oberfaden Durchmesser:'; //ivlm

  rsYCCaption = '(11)Garnnummer'; //ivlm
  rsYCPosDiaDiff = '(9)DiaDiff +'; //ivlm
  rsYCPosDiaDiffHint = '(*)Durchmesserzunahme:'; //ivlm
  rsYCNegDiaDiff = '(9)DiaDiff -'; //ivlm
  rsYCNegDiaDiffHint = '(*)Durchmesserabnahme:'; //ivlm

  rsYCCount = '(9)Feinheit'; //ivlm
  rsYCCorse = '(9)Grob'; //ivlm
  rsYCCorseHint = '(*)Grenzwert grob:'; //ivlm
  rsYCFine = '(9)Fein'; //ivlm
  rsYCFineHint = '(*)Grenzwert fein:'; //ivlm

  rsYCLength = '(7)Laenge'; //ivlm
  rsMeasureM = '(1)m'; //ivlm
  rsYCLengthHint = '(*)Bezugslaenge:'; //ivlm

  rsFCCaption = '(11)Fehlerschwarm'; //ivlm
  rsFCDiameter = '(12)Durchmesser'; //ivlm
  rsFCDiameterHint = '(*)Fehlerschwarm Durchmesser:'; //ivlm
  rsFCLength = '(8)Laenge'; //ivlm
  rsFCLengthHint = '(*)Bobachtungslaenge:'; //ivlm
  rsFCFaults = '(7)Fehler'; //ivlm
  rsFCFaultsHint = '(*)Fehlerzahl bezogen auf die Bobachtungslaenge:'; //ivlm

  rsSFICaption = '(11)SFI/D'; //ivlm
  rsSFIReferenc = '(7)Referenz'; //ivlm
  rsSFIReferencHint = '(*)SFI/D-Referenz:'; //ivlm
  rsSFIFloat = '(9)&gleitend'; //ivlm
  rsSFIPosLimit = '(7)+Grenze'; //ivlm
  rsSFIPosLimitHint = '(*)Positive Abweichung vom Referenzwert:'; //ivlm
  rsSFINegLimit = '(7)-Grenze'; //ivlm
  rsSFINegLimitHint = '(*)Negative Abweichung vom Referenzwert:'; //ivlm

  rsFFCCaption = '(11)FF-Cluster'; //ivlm
  rsFFCFaults = '(7)FF'; //ivlm
  rsFFCFaultsHints = '(*)Fehlerzahl bezogen auf die Bobachtungslaenge:'; //ivlm

const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Channel Values definitions
//------------------------------------------------------------------------------
  //...........................................................................
  cChN = 0;
  cChDS = 1;
  cChLS = 2;
  cChDL = 3;
  cChLL = 4;
  cChDT = 5;
  cChLT = 6;

  cTestCaption: string = 'DDDDDDDDDDD';
  cCHCaption: string = rsCHCaption;
  cSPCaption: string = rsSPCaption;

  cVSCHNepDiameter: TValueSpecRec = (
    minValue: cMinNepsDia / 100;
    maxValue: cMaxNepsDia / 100;
    format: '%3.1f';
    caption: rsNepDiameter;
    measure: '';
    hint: rsNepDiameterHint;
    );

  cPTCHNepDiameter: array[0..9] of TPresetTextRec = (
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '3.5'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '4.5'; ),
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '5.5'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '6.5'; ),
    (typ: ttFloat; text: '7.0'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));
  //...........................................................................

  cVSCHShortDiameter: TValueSpecRec = (
    minValue: cMinShortDia / 100;
    maxValue: cMaxShortDia / 100;
    format: '%4.2f';
    caption: rsShortDiameter;
    measure: ''; //ivlm
    hint: rsShortDiameterHint;
    );

  cPTCHShortDiameter: array[0..26] of TPresetTextRec = (
    (typ: ttFloat; text: '1.50'; ),
    (typ: ttFloat; text: '1.60'; ),
    (typ: ttFloat; text: '1.70'; ),
    (typ: ttFloat; text: '1.80'; ),
    (typ: ttFloat; text: '1.90'; ),
    (typ: ttFloat; text: '2.00'; ),
    (typ: ttFloat; text: '2.10'; ),
    (typ: ttFloat; text: '2.20'; ),
    (typ: ttFloat; text: '2.30'; ),
    (typ: ttFloat; text: '2.40'; ),
    (typ: ttFloat; text: '2.50'; ),
    (typ: ttFloat; text: '2.60'; ),
    (typ: ttFloat; text: '2.70'; ),
    (typ: ttFloat; text: '2.80'; ),
    (typ: ttFloat; text: '2.90'; ),
    (typ: ttFloat; text: '3.00'; ),
    (typ: ttFloat; text: '3.10'; ),
    (typ: ttFloat; text: '3.20'; ),
    (typ: ttFloat; text: '3.30'; ),
    (typ: ttFloat; text: '3.40'; ),
    (typ: ttFloat; text: '3.50'; ),
    (typ: ttFloat; text: '3.60'; ),
    (typ: ttFloat; text: '3.70'; ),
    (typ: ttFloat; text: '3.80'; ),
    (typ: ttFloat; text: '3.90'; ),
    (typ: ttFloat; text: '4.00'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));

  cVSCHShortLength: TValueSpecRec = (
    minValue: cMinShortLen / 10;
    maxValue: cMaxShortLen / 10;
    format: '%4.1f';
    caption: rsShortLength;
    measure: rsMeasureCm;
    hint: rsShortLengthHint;
    );

  cPTCHShortLength: array[0..29] of TPresetTextRec = (
    (typ: ttFloat; text: '1.1'; ),
    (typ: ttFloat; text: '1.2'; ),
    (typ: ttFloat; text: '1.3'; ),
    (typ: ttFloat; text: '1.4'; ),
    (typ: ttFloat; text: '1.5'; ),
    (typ: ttFloat; text: '1.6'; ),
    (typ: ttFloat; text: '1.7'; ),
    (typ: ttFloat; text: '1.8'; ),
    (typ: ttFloat; text: '1.9'; ),
    (typ: ttFloat; text: '2.0'; ),
    (typ: ttFloat; text: '2.1'; ),
    (typ: ttFloat; text: '2.2'; ),
    (typ: ttFloat; text: '2.3'; ),
    (typ: ttFloat; text: '2.4'; ),
    (typ: ttFloat; text: '2.5'; ),
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '3.5'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '4.5'; ),
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '5.5'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '6.5'; ),
    (typ: ttFloat; text: '7.0'; ),
    (typ: ttFloat; text: '7.5'; ),
    (typ: ttFloat; text: '8.0'; ),
    (typ: ttFloat; text: '8.5'; ),
    (typ: ttFloat; text: '9.0'; ),
    (typ: ttFloat; text: '9.5'; ),
    (typ: ttFloat; text: '10.0'; ));
  //...........................................................................

  cVSCHLongDiameter: TValueSpecRec = (
    minValue: cMinLongDia / 100;
    maxValue: cMaxLongDia / 100;
    format: '%4.2f';
    caption: rsLongDiameter;
    measure: ''; //ivlm
    hint: rsLongDiameterHint;
    );

  cPTCHLongDiameter: array[0..28] of TPresetTextRec = (
    (typ: ttFloat; text: '1.10'; ),
    (typ: ttFloat; text: '1.12'; ),
    (typ: ttFloat; text: '1.14'; ),
    (typ: ttFloat; text: '1.16'; ),
    (typ: ttFloat; text: '1.18'; ),
    (typ: ttFloat; text: '1.20'; ),
    (typ: ttFloat; text: '1.22'; ),
    (typ: ttFloat; text: '1.24'; ),
    (typ: ttFloat; text: '1.26'; ),
    (typ: ttFloat; text: '1.28'; ),
    (typ: ttFloat; text: '1.30'; ),
    (typ: ttFloat; text: '1.32'; ),
    (typ: ttFloat; text: '1.34'; ),
    (typ: ttFloat; text: '1.36'; ),
    (typ: ttFloat; text: '1.38'; ),
    (typ: ttFloat; text: '1.40'; ),
    (typ: ttFloat; text: '1.45'; ),
    (typ: ttFloat; text: '1.50'; ),
    (typ: ttFloat; text: '1.55'; ),
    (typ: ttFloat; text: '1.60'; ),
    (typ: ttFloat; text: '1.65'; ),
    (typ: ttFloat; text: '1.70'; ),
    (typ: ttFloat; text: '1.75'; ),
    (typ: ttFloat; text: '1.80'; ),
    (typ: ttFloat; text: '1.85'; ),
    (typ: ttFloat; text: '1.90'; ),
    (typ: ttFloat; text: '1.95'; ),
    (typ: ttFloat; text: '2.00'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));

  cVSCHLongLength: TValueSpecRec = (
    minValue: cMinLongLen / 10;
    maxValue: cMaxLongLen / 10;
    format: '%3.0f';
    caption: rsLongLength;
    measure: rsMeasureCm;
    hint: rsLongLengthHint;
    );

  cPTCHLongThinLength: array[0..19] of TPresetTextRec = (
    (typ: ttFloat; text: '6'; ),
    (typ: ttFloat; text: '9'; ),
    (typ: ttFloat; text: '12'; ),
    (typ: ttFloat; text: '15'; ),
    (typ: ttFloat; text: '18'; ),
    (typ: ttFloat; text: '21'; ),
    (typ: ttFloat; text: '24'; ),
    (typ: ttFloat; text: '27'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttFloat; text: '60'; ),
    (typ: ttFloat; text: '70'; ),
    (typ: ttFloat; text: '80'; ),
    (typ: ttFloat; text: '90'; ),
    (typ: ttFloat; text: '100'; ),
    (typ: ttFloat; text: '110'; ),
    (typ: ttFloat; text: '120'; ),
    (typ: ttFloat; text: '160'; ),
    (typ: ttFloat; text: '200'; ));
  //...........................................................................

  cVSCHThinDiameter: TValueSpecRec = (
    minValue: 100 - cMaxThinDiaAbove;
    maxValue: 100 - cMinThinDia;
    format: '%2.0f';
    caption: rsThinDiameter;
    measure: rsMeasureProcent;
    hint: rsThinDiameterHint;
    );

  cPTCHThinDiameter: array[0..17] of TPresetTextRec = (
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '12'; ),
    (typ: ttFloat; text: '14'; ),
    (typ: ttFloat; text: '16'; ),
    (typ: ttFloat; text: '18'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '22'; ),
    (typ: ttFloat; text: '24'; ),
    (typ: ttFloat; text: '26'; ),
    (typ: ttFloat; text: '28'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '35'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '45'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttFloat; text: '55'; ),
    (typ: ttFloat; text: '60'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));

  cVSCHThinLength: TValueSpecRec = (
    minValue: cMinThinLen / 10;
    maxValue: cMaxThinLen / 10;
    format: '%3.0f';
    caption: rsThinLength;
    measure: rsMeasureCm;
    hint: rsThinLengthHint;
    );
  //...........................................................................

//------------------------------------------------------------------------------
// Splice Values definitions
//------------------------------------------------------------------------------
  //...........................................................................
  cSPDUpY = cChLT + 1;

  cVSSPUpYDiameter: TValueSpecRec = (
    minValue: cMinLongDia / 100;
    maxValue: cMaxLongDia / 100;
    format: '%4.2f';
    caption: rsUpYDiameter;
    measure: ''; //ivlm
    hint: rsUpYDiameterHint;
    );
  //...........................................................................

//------------------------------------------------------------------------------
// Yarn Count Monitor Values definitions
//------------------------------------------------------------------------------
  //...........................................................................

  cYCPDiaDiff = 0;
  cYCNDiaDiff = 1;
  CYCCount = 2;
  CYCCorse = 3;
  CYCFine = 4;
  cYCLength = 5;

  cYCCaption: string = rsYCCaption;

  cVSYCPosDiaDiff: TValueSpecRec = (
    minValue: cMinDiaDiff / 10;
    maxValue: cMaxDiaDiff / 10;
    format: '%4.1f';
    caption: rsYCPosDiaDiff;
    measure: rsMeasureProcent;
    hint: rsYCPosDiaDiffHint;
    );

  cVSYCNegDiaDiff: TValueSpecRec = (
    minValue: cMinDiaDiff / 10;
    maxValue: cMaxDiaDiff / 10;
    format: '%4.1f';
    caption: rsYCNegDiaDiff;
    measure: rsMeasureProcent;
    hint: rsYCNegDiaDiffHint;
    );

  cPTYCDiaDiff: array[0..20] of TPresetTextRec = (
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '4.5'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '7.0'; ),
    (typ: ttFloat; text: '8.0'; ),
    (typ: ttFloat; text: '9.0'; ),
    (typ: ttFloat; text: '10.0'; ),
    (typ: ttFloat; text: '12.0'; ),
    (typ: ttFloat; text: '14.0'; ),
    (typ: ttFloat; text: '16.0'; ),
    (typ: ttFloat; text: '18.0'; ),
    (typ: ttFloat; text: '20.0'; ),
    (typ: ttFloat; text: '24.0'; ),
    (typ: ttFloat; text: '28.0'; ),
    (typ: ttFloat; text: '32.0'; ),
    (typ: ttFloat; text: '36.0'; ),
    (typ: ttFloat; text: '40.0'; ),
    (typ: ttFloat; text: '44.0'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));
  //...........................................................................

  cVSYCCount: TValueSpecRec = (
    minValue: cMinYarnCount / 10;
    maxValue: cMaxYarnCount / 10;
    format: '%4.1f';
    caption: rsYCCount;
    measure: ''; //ivlm
    hint: ''; //ivlm
    );

  cVSYCCorse: TValueSpecRec = (
    minValue: cMinYarnCount / 10;
    maxValue: cMaxYarnCount / 10;
    format: '%4.1f';
    caption: rsYCCorse;
    measure: '';
    hint: rsYCCorseHint;
    );

  cVSYCFine: TValueSpecRec = (
    minValue: cMinYarnCount / 10;
    maxValue: cMaxYarnCount / 10;
    format: '%4.1f';
    caption: rsYCFine;
    measure: ''; //ivlm
    hint: rsYCFineHint;
    );

  cPTYCCount: array[0..1] of TPresetTextRec = (
    (typ: ttCmdDisabled; text: rsSwOff; ),
    (typ: ttEmpty; text: ''; ));
  cPTYarnCount: array[0..0] of TPresetTextRec = (
    (typ: ttEmpty; text: ''; ));
  cPTNoPreset: array[0..0] of TPresetTextRec = (());
  //...........................................................................

  cVSYCLength: TValueSpecRec = (
    minValue: 10;
    maxValue: 50;
    format: '%2.0f';
    caption: rsYCLength;
    measure: rsMeasureM;
    hint: rsYCLengthHint;
    );

  cPTYCLength: array[0..4] of TPresetTextRec = (
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '50'; ));
  //...........................................................................

//------------------------------------------------------------------------------
// Fault Cluster Monitor Values definitions
//------------------------------------------------------------------------------
  //...........................................................................

  cFCDiameter = 0;
  cFCLength = 1;
  CFCFaults = 2;

  cFCCaption: string = rsFCCaption;

  cVSFCDiameter: TValueSpecRec = (
    minValue: cMinClusterDia / 100;
    maxValue: cMaxClusterDia / 100;
    format: '%3.2f';
    caption: rsFCDiameter;
    measure: ''; //ivlm
    hint: rsFCDiameterHint;
    );

  cPTFCDiameter: array[0..26] of TPresetTextRec = (
    (typ: ttFloat; text: '1.50'; ),
    (typ: ttFloat; text: '1.60'; ),
    (typ: ttFloat; text: '1.70'; ),
    (typ: ttFloat; text: '1.80'; ),
    (typ: ttFloat; text: '1.90'; ),
    (typ: ttFloat; text: '2.00'; ),
    (typ: ttFloat; text: '2.10'; ),
    (typ: ttFloat; text: '2.20'; ),
    (typ: ttFloat; text: '2.30'; ),
    (typ: ttFloat; text: '2.40'; ),
    (typ: ttFloat; text: '2.50'; ),
    (typ: ttFloat; text: '2.60'; ),
    (typ: ttFloat; text: '2.70'; ),
    (typ: ttFloat; text: '2.80'; ),
    (typ: ttFloat; text: '2.90'; ),
    (typ: ttFloat; text: '3.00'; ),
    (typ: ttFloat; text: '3.10'; ),
    (typ: ttFloat; text: '3.20'; ),
    (typ: ttFloat; text: '3.30'; ),
    (typ: ttFloat; text: '3.40'; ),
    (typ: ttFloat; text: '3.50'; ),
    (typ: ttFloat; text: '3.60'; ),
    (typ: ttFloat; text: '3.70'; ),
    (typ: ttFloat; text: '3.80'; ),
    (typ: ttFloat; text: '3.90'; ),
    (typ: ttFloat; text: '4.00'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));
  //...........................................................................

  cVSFCLength: TValueSpecRec = (
    minValue: cMinObsLength / 100;
    maxValue: cMaxObsLength / 100;
    format: '%4.1f';
    caption: rsFCLength;
    measure: rsMeasureM; //ivlm
    hint: rsFCLengthHint;
    );

  cPTFCLength: array[0..18] of TPresetTextRec = (
    (typ: ttFloat; text: '0.1'),
    (typ: ttFloat; text: '0.4'),
    (typ: ttFloat; text: '0.7'),
    (typ: ttFloat; text: '1.0'),
    (typ: ttFloat; text: '2.0'; ),
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '7.0'; ),
    (typ: ttFloat; text: '8.0'; ),
    (typ: ttFloat; text: '9.0'; ),
    (typ: ttFloat; text: '10.0'; ),
    (typ: ttFloat; text: '20.0'; ),
    (typ: ttFloat; text: '30.0'; ),
    (typ: ttFloat; text: '40.0'; ),
    (typ: ttFloat; text: '50.0'; ),
    (typ: ttFloat; text: '80.0'; ),
(*
    (typ: ttFloat; text: '150.0'; ),
    (typ: ttFloat; text: '200.0'; ),
    (typ: ttFloat; text: '250.0'; ),
    (typ: ttFloat; text: '300.0'; ),
*)
    (typ: ttCmdDisabled; text: rsSwOff; )); //ivlm
  //...........................................................................

  cVSFCFaults: TValueSpecRec = (
    minValue: cMinDefects;
    maxValue: cMaxDefects;
    format: '%4.0f';
    caption: rsFCFaults;
    measure: ''; //ivlm
    hint: rsFCFaultsHint;
    );

  cPTFCFaults: array[0..17] of TPresetTextRec = (
    (typ: ttFloat; text: '1'),
    (typ: ttFloat; text: '2'),
    (typ: ttFloat; text: '3'),
    (typ: ttFloat; text: '4'),
    (typ: ttFloat; text: '5'),
    (typ: ttFloat; text: '6'),
    (typ: ttFloat; text: '7'),
    (typ: ttFloat; text: '8'),
    (typ: ttFloat; text: '9'),
    (typ: ttFloat; text: '10'),
    (typ: ttFloat; text: '12'),
    (typ: ttFloat; text: '14'),
    (typ: ttFloat; text: '16'),
    (typ: ttFloat; text: '18'),
    (typ: ttFloat; text: '20'),
    (typ: ttFloat; text: '25'),
    (typ: ttFloat; text: '30'),
    (typ: ttCmdDisabled; text: rsSwOff; )); //ivlm
  //...........................................................................

//------------------------------------------------------------------------------
// SFI/D Values definitions
//------------------------------------------------------------------------------
  //...........................................................................

  cSFIReference = 0;
  CSFIPosLimit = 1;
  CSFINegLimit = 2;

  cSFICaption: string = rsSFICaption;

  cVSSFIReferenc: TValueSpecRec = (
    minValue: cMinSFIRef / 100;
    maxValue: cMaxSFIRef / 100;
    format: '%2.1f';
    caption: rsSFIReferenc;
    measure: ''; //ivlm
    hint: rsSFIReferencHint;
    );

  cPTSFIReferenc: array[0..21] of TPresetTextRec = (
    (typ: ttFloat; text: '5.0'),
    (typ: ttFloat; text: '6.0'),
    (typ: ttFloat; text: '7.0'),
    (typ: ttFloat; text: '8.0'),
    (typ: ttFloat; text: '9.0'; ),
    (typ: ttFloat; text: '10.0'; ),
    (typ: ttFloat; text: '11.0'; ),
    (typ: ttFloat; text: '12.0'; ),
    (typ: ttFloat; text: '13.0'; ),
    (typ: ttFloat; text: '14.0'; ),
    (typ: ttFloat; text: '15.0'; ),
    (typ: ttFloat; text: '16.0'; ),
    (typ: ttFloat; text: '17.0'; ),
    (typ: ttFloat; text: '18.0'; ),
    (typ: ttFloat; text: '19.0'; ),
    (typ: ttFloat; text: '20.0'; ),
    (typ: ttFloat; text: '21.0'; ),
    (typ: ttFloat; text: '22.0'; ),
    (typ: ttFloat; text: '23.0'; ),
    (typ: ttFloat; text: '24.0'; ),
    (typ: ttFloat; text: '25.0'; ),
    (typ: ttCmdDisabled; text: rsSFIFloat; ));
  //...........................................................................

  cVSSFIPosLimit: TValueSpecRec = (
    minValue: cMinSFIDiffUp / 10;
    maxValue: cMaxSFIDiffUp / 10;
    format: '%2.0f';
    caption: rsSFIPosLimit;
    measure: rsMeasureProcent;
    hint: rsSFIPosLimitHint;
    );

  cVSSFINegLimit: TValueSpecRec = (
    minValue: cMinSFIDiffLw / 10;
    maxValue: cMaxSFIDiffLw / 10;
    format: '%2.0f';
    caption: rsSFINegLimit;
    measure: rsMeasureProcent;
    hint: rsSFINegLimitHint;
    );

  cPTSFILimit: array[0..24] of TPresetTextRec = (
    (typ: ttFloat; text: '5'),
    (typ: ttFloat; text: '6'),
    (typ: ttFloat; text: '7'),
    (typ: ttFloat; text: '8'),
    (typ: ttFloat; text: '9'; ),
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '11'; ),
    (typ: ttFloat; text: '12'; ),
    (typ: ttFloat; text: '13'; ),
    (typ: ttFloat; text: '14'; ),
    (typ: ttFloat; text: '15'; ),
    (typ: ttFloat; text: '16'; ),
    (typ: ttFloat; text: '18'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '22'; ),
    (typ: ttFloat; text: '24'; ),
    (typ: ttFloat; text: '26'; ),
    (typ: ttFloat; text: '28'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '32'; ),
    (typ: ttFloat; text: '34'; ),
    (typ: ttFloat; text: '36'; ),
    (typ: ttFloat; text: '38'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));
  //...........................................................................

//------------------------------------------------------------------------------
// FF Cluster Values definitions
//------------------------------------------------------------------------------
  //...........................................................................
  cFFCLength = 0;
  CFFCFaults = 1;

  cFFCCaption: string = rsFFCCaption;

  cVSFFCFaults: TValueSpecRec = (
    minValue: cMinDefects;
    maxValue: cMaxDefects;
    format: '%4.0f';
    caption: rsFFCFaults;
    measure: ''; //ivlm
    hint: rsFFCFaultsHints;
    );

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------
  //...........................................................................
type
  TSynchronizeFFMatrix = procedure of object;

  TChannelEditBox = class(TGroupEditBox)
  private
    { Private declarations }
    fPrintable: Boolean;

    procedure SetPrintable(const aValue: Boolean);

  protected
    procedure DoConfirm(aTag: Integer); override;

  public
    { Public declarations }

    constructor Create(aOwner: TComponent); override;

    procedure BuildBox; override;
    procedure GetPara(var aPara: TChannelSettingsRec); virtual;
    procedure PutPara(var aPara: TChannelSettingsRec); virtual;
    procedure PutYMSettings(var aSettings: TYMSettingsRec); override;

  published
    property Printable: Boolean read fPrintable write SetPrintable;

  end;
  //...........................................................................

  TSpliceEditBox = class(TChannelEditBox)
  private
    { Private declarations }
    mUpperYarn: TDiameterRec;
    procedure SetUpperYarn(const aValue: Boolean);
    function GetUpperYarn: Boolean;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;

    procedure BuildBox; override;
    procedure GetPara(var aPara: TChannelSettingsRec); override;
    procedure PutPara(var aPara: TChannelSettingsRec); override;
    procedure PutYMSettings(var aSettings: TYMSettingsRec); override;

  published
    property UpperYarn: Boolean read GetUpperYarn write SetUpperYarn;

  end;
  //...........................................................................

  TFaultClusterSettingsRec = record
    clusterDia: Word;
    clusterLength: Word;
    clusterDefects: Word;
  end;

  TFaultClusterEditBox = class(TGroupEditBox)
  private
    { Private declarations }
    fPrintable: Boolean;

    procedure SetPrintable(const aValue: Boolean);

  protected
    procedure DoConfirm(aTag: Integer); override;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;

    procedure BuildBox; override;
    procedure GetPara(var aPara: TFaultClusterSettingsRec);
    procedure PutPara(var aPara: TFaultClusterSettingsRec);
    procedure PutYMSettings(var aSettings: TYMSettingsRec); override;

  published
    property Printable: Boolean read fPrintable write SetPrintable default False;

  end;
  //...........................................................................

  TYarnCountSettingsRec = record
    posDiaDiff: Word;
    negDiaDiff: Word;
    countLength: Byte;
  end;

  TYarnCountEditBox = class(TGroupEditBox)
  private
    { Private declarations }
    fPlus: Boolean;
    fPrintable: Boolean;
    fYarnCount: Integer;
    fYarnUnit: TYarnUnits;

    function GetPlus: Boolean;

    procedure PutPara(aPara: TYarnCountSettingsRec);
    procedure PutPlus(const aValue: Boolean);
    procedure PutYarnCount(const aValue: Integer);
    procedure PutYarnUnit(const aValue: TYarnUnits);
    procedure SetPrintable(const aValue: Boolean);
    procedure ValidatePara(aTag: Integer);

  protected
    procedure DoConfirm(aTag: Integer); override;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;

    procedure BuildBox; override;
    procedure GetPara(var aPara: TYarnCountSettingsRec);
    procedure PutYMSettings(var aSettings: TYMSettingsRec); override;

  published
    property Plus: Boolean read GetPlus write PutPlus;
    property Printable: Boolean read fPrintable write SetPrintable default False;
    property YarnCount: Integer read fYarnCount write PutYarnCount;
      // YarnCount := current Yarn Count;
    property YarnUnit: TYarnUnits read fYarnUnit write PutYarnUnit;

  end;
  //...........................................................................

  TSFISettingsRec = record
    absRef: Word; // Reference: constant x100	[5.0..25.0] (500..2500)
                                        // Reference: float [0]
    upper: Word; // +Limi x10	[5..40]	(50, 400,off)[%]
    lower: Word; // -Limi x10	[5..40]	(50, 400,off)[%]
  end;

  TSFIEditBox = class(TGroupEditBox)
  private
    { Private declarations }
    fPrintable: Boolean;

    procedure PutPara(var aPara: TSFISettingsRec);
    procedure SetPrintable(const aValue: Boolean);
    procedure ValidatePara(aTag: Integer);

  protected
    procedure DoConfirm(aTag: Integer); override;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;

    procedure BuildBox; override;
    procedure GetPara(var aPara: TSFISettingsRec);
    procedure PutYMSettings(var aSettings: TYMSettingsRec); override;

  published
    property Printable: Boolean read fPrintable write SetPrintable default False;

  end;
  //...........................................................................

  TFFClusterSettingsRec = record
    clusterLength: Word;
    clusterDefects: Word;
  end;

  TFFClusterEditBox = class(TGroupEditBox)
  private
    { Private declarations }
    fPrintable: Boolean;
    fSynchronizeFFMatrix: TSynchronizeFFMatrix;

    procedure SetPrintable(const aValue: Boolean);
//    procedure SetEnabled(const aValue: Boolean);

  protected
    procedure DoConfirm(aTag: Integer); override;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;

    procedure BuildBox; override;
    procedure GetPara(var aPara: TFFClusterSettingsRec);
    procedure PutPara(var aPara: TFFClusterSettingsRec);
    procedure PutYMSettings(var aSettings: TYMSettingsRec); override;

  published
    property Printable: Boolean read fPrintable write SetPrintable default False;
    property SynchronizeFFMatrix: TSynchronizeFFMatrix read fSynchronizeFFMatrix write fSynchronizeFFMatrix;
//    property Enabled: Boolean read fEnabled write SetEnabled default True;

  end;
  //...........................................................................

  //------------------------------------------------------------------------------
// Functions and procedures
//------------------------------------------------------------------------------
  //...........................................................................
function ParaSwitchToTextType(aSwitch: Word): TTextType;
function TextTypToParaSwitch(aTextType: TTextType): Word;
// procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//******************************************************************************
// Functions and procedures
//******************************************************************************
//------------------------------------------------------------------------------

function ParaSwitchToTextType(aSwitch: Word): TTextType;
begin
  if aSwitch = cChOff then
    Result := ttCmdDisabled
  else
    Result := ttFloat;
end;
//------------------------------------------------------------------------------
(*
procedure Register;
begin
  RegisterComponents('YMClearer', [TChannelEditBox, TSpliceEditBox, TYarnCountEditBox, TFaultClusterEditBox]);
end;
*)
//------------------------------------------------------------------------------

function TextTypToParaSwitch(aTextType: TTextType): Word;
begin
  if aTextType = ttCmdDisabled then
    Result := cChOff
  else
    Result := cChOn;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// TChannelEditBox
//******************************************************************************
//------------------------------------------------------------------------------

procedure TChannelEditBox.BuildBox;
var
  xSpace: Integer;
begin
  inherited;

  xSpace := VerticalSpace[0];
  VerticalSpace[cChN] := xSpace - 2;
  AddField(cChN, cVSCHNepDiameter, cPTCHNepDiameter, Printable);

  VerticalSpace[cChDS] := xSpace + 0;
  AddField(cChDS, cVSCHShortDiameter, cPTCHShortDiameter, Printable);

  VerticalSpace[cChLS] := xSpace - 6;
  AddField(cChLS, cVSCHShortLength, cPTCHShortLength, Printable);

  VerticalSpace[cChDL] := xSpace + 0;
  AddField(cChDL, cVSCHLongDiameter, cPTCHLongDiameter, Printable);

  VerticalSpace[cChLL] := xSpace - 6;
  AddField(cChLL, cVSCHLongLength, cPTCHLongThinLength, Printable);

  VerticalSpace[cChDT] := xSpace + 0;
  AddField(cChDT, cVSCHThinDiameter, cPTCHThinDiameter, Printable);

  VerticalSpace[cChLT] := xSpace - 6;
  AddField(cChLT, cVSCHThinLength, cPTCHLongThinLength, Printable);

end;
//------------------------------------------------------------------------------

constructor TChannelEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  CaptionWidth := 23;
//  ValueWidth := 38;  // textsize 3, (4)
  ValueWidth := 31; // textsize 3, (3)
  MeasureWidth := 16;

  Caption := cCHCaption;
//  Caption := cTestCaption;

  fPrintable := False;

  BuildBox;
end;
//------------------------------------------------------------------------------

procedure TChannelEditBox.DoConfirm(aTag: Integer);
var
  xPara: TChannelSettingsRec;
begin
  GetPara(xPara);
  with xPara do
    case aTag of
      cChDT:
        begin

          if thin.dia >= cMaxThinDiaBelow then
            thinLen := TYMSettingsUtils.FitValue(thinLen, cThinLenThreshold, cMaxThinLen)
          else
            thinLen := TYMSettingsUtils.FitValue(thinLen, cMinThinLen, cMaxThinLen);
        end;

      cChLT:
        begin

          if thinLen >= cThinLenThreshold then
            thin.dia := TYMSettingsUtils.FitValue(thin.dia, cMinThinDia, cMaxThinDiaAbove)
          else
            thin.dia := TYMSettingsUtils.FitValue(thin.dia, cMinThinDia, cMaxThinDiaBelow);
        end;
    end;
  PutPara(xPara);

  inherited;
end;
//------------------------------------------------------------------------------

procedure TChannelEditBox.GetPara(var aPara: TChannelSettingsRec);
begin
//  Double  Round
  aPara.neps.dia := Round(Values[cChN] * 100);
  aPara.neps.sw := TextTypToParaSwitch(TextType[cChN]);

  aPara.short.dia := Round(Values[cChDS] * 100);
  aPara.short.sw := TextTypToParaSwitch(TextType[cChDS]);
  aPara.shortLen := Round(Values[cChLS] * 10);

  aPara.long.dia := Round(Values[cChDL] * 100);
  aPara.long.sw := TextTypToParaSwitch(TextType[cChDL]);
  aPara.longLen := Round(Values[cChLL] * 10);

  aPara.thin.dia := 100 - Round(Values[cChDT]);
  aPara.thin.sw := TextTypToParaSwitch(TextType[cChDT]);
  aPara.thinLen := Round(Values[cChLT] * 10);
end;
//------------------------------------------------------------------------------

procedure TChannelEditBox.PutPara(var aPara: TChannelSettingsRec);
begin
  Values[cChN] := aPara.neps.dia / 100;
  TextType[cChN] := ParaSwitchToTextType(aPara.neps.sw);

  Values[cChDS] := aPara.short.dia / 100;
  TextType[cChDS] := ParaSwitchToTextType(aPara.short.sw);
  Values[cChLS] := aPara.shortLen / 10;
  TextType[cChLS] := ttFloat;

  Values[cChDL] := aPara.long.dia / 100;
  TextType[cChDL] := ParaSwitchToTextType(aPara.long.sw);
  Values[cChLL] := aPara.longLen / 10;
  TextType[cChLL] := ttFloat;

  Values[cChDT] := 100 - aPara.thin.dia;
  TextType[cChDT] := ParaSwitchToTextType(aPara.thin.sw);
  Values[cChLT] := aPara.thinLen / 10;
  TextType[cChLT] := ttFloat;
end;
//------------------------------------------------------------------------------

procedure TChannelEditBox.PutYMSettings(var aSettings: TYMSettingsRec);
begin
  PutPara(aSettings.channel);
end;
//------------------------------------------------------------------------------

procedure TChannelEditBox.SetPrintable(const aValue: Boolean);
var
  xPara: TChannelSettingsRec;
begin
  if aValue <> fPrintable then
  begin
    GetPara(xPara);
    fPrintable := aValue;
    BuildBox;
    PutPara(xPara);
  end;

end;
//------------------------------------------------------------------------------

//******************************************************************************
// TFaultClusterEditBox
//******************************************************************************
//------------------------------------------------------------------------------

procedure TFaultClusterEditBox.BuildBox;
var
  xSpace: Integer;
begin
  inherited;

  xSpace := VerticalSpace[0];
  VerticalSpace[cFCDiameter] := xSpace - 2;
  AddField(cFCDiameter, cVSFCDiameter, cPTFCDiameter, Printable);

  VerticalSpace[cFCLength] := xSpace + 0;
  AddField(cFCLength, cVSFCLength, cPTFCLength, Printable);
  VerticalSpace[cFCFaults] := xSpace - 6;
  AddField(cFCFaults, cVSFCFaults, cPTFCFaults, Printable);

end;
//------------------------------------------------------------------------------

constructor TFaultClusterEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  CaptionWidth := 75;
//  ValueWidth := 38;  // textsize 3, (4)
  ValueWidth := 35; // textsize 3, (3)
  MeasureWidth := 10;
  Caption := cFCCaption;
//  Caption := cTestCaption;

  fPrintable := False;
  BuildBox;
end;
//------------------------------------------------------------------------------

procedure TFaultClusterEditBox.DoConfirm(aTag: Integer);
begin
  case aTag of
    cFCLength:
      if TextType[cFCLength] = ttCmdDisabled then
      begin
        TextType[cFCDiameter] := ttCmdDisabled;
        TextType[cFCFaults] := ttCmdDisabled;
        Captions[cFCFaults] := Translate(cVSFCFaults.caption);
      end
      else
      begin
        TextType[cFCDiameter] := ttFloat;
        TextType[cFCFaults] := ttFloat;
        Captions[cFCFaults] := Translate(cVSFCFaults.caption) + '/' +
          Format(cVSFCLength.format, [Values[cFCLength]]) + ' ' +
          Translate(cVSFCLength.measure);
      end;

    CFCFaults:
      if TextType[cFCFaults] = ttCmdDisabled then
      begin
        TextType[cFCDiameter] := ttCmdDisabled;
        TextType[cFCLength] := ttCmdDisabled;
        Captions[cFCFaults] := Translate(cVSFCFaults.caption);
      end
      else
      begin
        TextType[cFCDiameter] := ttFloat;
        TextType[cFCLength] := ttFloat;
        Captions[cFCFaults] := Translate(cVSFCFaults.caption) + '/' +
          Format(cVSFCLength.format, [Values[cFCLength]]) + ' ' +
          Translate(cVSFCLength.measure);
      end;

  end;

  inherited;

end;
//------------------------------------------------------------------------------

procedure TFaultClusterEditBox.GetPara(
  var aPara: TFaultClusterSettingsRec);
begin
  if TextType[cFCLength] = ttCmdDisabled then
    aPara.clusterLength := 0 // 0 = Off
  else
    aPara.clusterLength := Round(Values[cFCLength] * 100);

  aPara.clusterDia := Round(Values[cFCDiameter] * 100);
  aPara.clusterDefects := Round(Values[cFCFaults]);

end;
//------------------------------------------------------------------------------

procedure TFaultClusterEditBox.PutPara(var aPara: TFaultClusterSettingsRec);
begin

  with aPara do
  begin

    if clusterLength = 0 then
    begin
      TextType[cFCDiameter] := ttCmdDisabled;
      TextType[cFCLength] := ttCmdDisabled;
      TextType[cFCFaults] := ttCmdDisabled;
      Captions[cFCFaults] := Translate(cVSFCFaults.caption);
    end
    else
    begin
      TextType[cFCDiameter] := ttFloat;
      Values[cFCDiameter] := clusterDia / 100;

      TextType[cFCLength] := ttFloat;
      Values[cFCLength] := clusterLength / 100;

      TextType[cFCFaults] := ttFloat;
      Values[cFCFaults] := clusterDefects;
      Captions[cFCFaults] := Translate(cVSFCFaults.caption) + '/' +
        Format(cVSFCLength.format, [Values[cFCLength]]) + ' ' +
        Translate(cVSFCLength.measure);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TFaultClusterEditBox.PutYMSettings(
  var aSettings: TYMSettingsRec);
begin
  with aSettings do
  begin

    if additional.option.clusterLength = 0 then
    begin
      TextType[cFCDiameter] := ttCmdDisabled;
      TextType[cFCLength] := ttCmdDisabled;
      TextType[cFCFaults] := ttCmdDisabled;
      Captions[cFCFaults] := Translate(cVSFCFaults.caption);
    end
    else
    begin
      if channel.short.sw = cChOn then
      begin
        TextType[cFCDiameter] := ttFloat;
        if additional.option.clusterDia > channel.short.dia then
          Values[cFCDiameter] := channel.short.dia / 100
        else
          Values[cFCDiameter] := additional.option.clusterDia / 100;
      end
      else
        TextType[cFCDiameter] := ttCmdDisabled;

      TextType[cFCLength] := ttFloat;
      Values[cFCLength] := additional.option.clusterLength / 100;

      TextType[cFCFaults] := ttFloat;
      Values[cFCFaults] := additional.option.clusterDefects;
      Captions[cFCFaults] := Translate(cVSFCFaults.caption) + '/' +
        Format(cVSFCLength.format, [Values[cFCLength]]) + ' ' +
        Translate(cVSFCLength.measure);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TFaultClusterEditBox.SetPrintable(const aValue: Boolean);
begin
  if aValue <> fPrintable then
  begin
    fPrintable := aValue;
  end;
  BuildBox;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// TYarnCountEditBox
//******************************************************************************
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.BuildBox;
var
  xSpace: Integer;
begin
  inherited;

  xSpace := VerticalSpace[cYCPDiaDiff];
  VerticalSpace[cYCPDiaDiff] := xSpace - 2;
  AddField(cYCPDiaDiff, cVSYCPosDiaDiff, cPTYCDiaDiff, Printable);

  VerticalSpace[cYCNDiaDiff] := xSpace - 6;
  AddField(cYCNDiaDiff, cVSYCNegDiaDiff, cPTYCDiaDiff, Printable);

  VerticalSpace[cYCCount] := xSpace + 0;
  AddField(cYCCount, cVSYCCount, cPTYCCount, True);
  EnabledField[cYCCount] := True;

  VerticalSpace[cYCCorse] := xSpace - 6;
  AddField(cYCCorse, cVSYCCorse, cPTYCCount, Printable);
  EnabledField[cYCCorse] := True;

  VerticalSpace[cYCFine] := xSpace - 6;
  AddField(cYCFine, cVSYCFine, cPTYCCount, Printable);
  EnabledField[cYCFine] := True;

  VerticalSpace[cYCLength] := xSpace + 0;
  AddField(cYCLength, cVSYCLength, cPTYCLength, Printable);

end;
//------------------------------------------------------------------------------

constructor TYarnCountEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fPlus := True;

  CaptionWidth := 50;
//  ValueWidth := 38;  // textsize 3, (4)
  ValueWidth := 44; // textsize 3, (3)
  MeasureWidth := 12;

  fPrintable := False;
  BuildBox;

  Caption := cYCCaption;
//  Caption := cTestCaption;

  YarnUnit := yUNm;
  fYarnCount := 1000; // Init YarnCount
  Values[cYCCount] := fYarnCount / 10;

//  Plus := False;
end;
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.DoConfirm(aTag: Integer);
begin
  ValidatePara(aTag);

  inherited;

end;
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.GetPara(var aPara: TYarnCountSettingsRec);
var
  xDisableCnt: Integer;
begin
  xDisableCnt := 0;
  if TextType[cYCPDiaDiff] = ttCmdDisabled then
    Inc(xDisableCnt);
  if TextType[cYCNDiaDiff] = ttCmdDisabled then
    Inc(xDisableCnt);
  if (TextType[cYCCorse] = ttCmdDisabled) or (TextType[cYCCorse] = ttEmpty) then
    Inc(xDisableCnt);
  if (TextType[cYCFine] = ttCmdDisabled) or (TextType[cYCFine] = ttEmpty)then
    Inc(xDisableCnt);

  if (xDisableCnt = 4) or (xDisableCnt = 1) then
  begin
    aPara.posDiaDiff := 0;
    aPara.negDiaDiff := 0;
  end
  else
  begin
    aPara.posDiaDiff := Round(Values[cYCPDiaDiff] * 10);
    if Plus then
      aPara.negDiaDiff := Round(Values[cYCNDiaDiff] * 10)
    else
      aPara.negDiaDiff := Round(Values[cYCPDiaDiff] * 10);
  end;
  aPara.countLength := Round(Values[cYCLength] / 10);
end;
//------------------------------------------------------------------------------

function TYarnCountEditBox.GetPlus: Boolean;
begin
  Result := fPlus;
end;
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.PutPara(aPara: TYarnCountSettingsRec);
begin
  if aPara.posDiaDiff = 0 then
  begin
    TextType[cYCPDiaDiff] := ttCmdDisabled;
    TextType[cYCNDiaDiff] := ttCmdDisabled;
    if TextType[cYCCount] <> ttEmpty then
    begin
      TextType[cYCCorse] := ttCmdDisabled;
      TextType[cYCFine] := ttCmdDisabled;
    end;
  end
  else
  begin
    TextType[cYCPDiaDiff] := ttFloat;
    Values[cYCPDiaDiff] := aPara.posDiaDiff / 10;

    TextType[cYCNDiaDiff] := ttFloat;

    if Plus then
      Values[cYCNDiaDiff] := aPara.negDiaDiff / 10
    else
      Values[cYCNDiaDiff] := aPara.posDiaDiff / 10;

    if TextType[cYCCount] <> ttEmpty then
    begin
      TextType[cYCCorse] := ttFloat;
      TextType[cYCFine] := ttFloat;
    end;

    if TYarnUnits(YarnUnit) in cYarnUnitsByLength then
    begin
      Values[cYCCorse] := CalculateCountByLength(1000 + aPara.posDiaDiff, YarnCount) / 10;
      Values[cYCFine] := CalculateCountByLength(1000 - aPara.negDiaDiff, YarnCount) / 10;
    end
    else
    begin
      Values[cYCCorse] := CalculateCountByWeight(1000 + aPara.posDiaDiff, YarnCount) / 10;
      Values[cYCFine] := CalculateCountByWeight(1000 - aPara.negDiaDiff, YarnCount) / 10;
    end;

  end;

  if Plus then
    Values[cYCLength] := aPara.countLength * 10
  else
    Values[cYCLength] := 10;

end;
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.PutPlus(const aValue: Boolean);
begin
  fPlus := aValue;
  EnabledField[cYCNDiaDiff] := fPlus;
  EnabledField[cYCLength] := fPlus;

  ValidatePara(cYCPDiaDiff);
end;
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.PutYarnCount(const aValue: Integer);
var
  xPara: TYarnCountSettingsRec;
begin

  if aValue <> 0 then
  begin

    fYarnCount := aValue;
    Values[cYCCount] := fYarnCount / 10;

    if not EnabledField[cYCCount] then
    begin
      EnabledField[cYCCount] := True;
      EnabledField[cYCCorse] := True;
      EnabledField[cYCFine] := True;
      TextType[cYCCount] := ttFloat;
      TextType[cYCCorse] := TextType[cYCPDiaDiff];
      TextType[cYCFine] := TextType[cYCPDiaDiff];
    end;
  end
  else
  begin
    fYarnCount := cInitYarnCount;
    Values[cYCCount] := fYarnCount / 10;

    if EnabledField[cYCCount] then
    begin
      EnabledField[cYCCount] := False;
      EnabledField[cYCCorse] := False;
      EnabledField[cYCFine] := False;
      TextType[cYCCount] := ttEmpty;
      TextType[cYCCorse] := ttEmpty;
      TextType[cYCFine] := ttEmpty;
    end;
  end;


  GetPara(xPara);
  if (xPara.posDiaDiff <> 0) or (xPara.negDiaDiff <> 0) then
  begin

    if TYarnUnits(YarnUnit) in cYarnUnitsByLength then
    begin
      Values[cYCCorse] := CalculateCountByLength(1000 + xPara.posDiaDiff, YarnCount) / 10;
      Values[cYCFine] := CalculateCountByLength(1000 - xPara.negDiaDiff, YarnCount) / 10;
    end
    else
    begin
      Values[cYCCorse] := CalculateCountByWeight(1000 + xPara.posDiaDiff, YarnCount) / 10;
      Values[cYCFine] := CalculateCountByWeight(1000 - xPara.negDiaDiff, YarnCount) / 10;
    end;

  end;
end;
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.PutYarnUnit(const aValue: TYarnUnits);
begin
  fYarnUnit := aValue;
  if EnabledField[cYCCount] then
    YarnCount := YarnCount
  else
    YarnCount := 0;
end;
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.PutYMSettings(var aSettings: TYMSettingsRec);
var
  xCount: TYarnCountSettingsRec;
  xMachineAttributes: TMachineAttributes;
begin
  xCount.posDiaDiff := aSettings.additional.option.diaDiff;
  xCount.negDiaDiff := aSettings.additional.extOption.negDiaDiff;
  xCount.countLength := aSettings.additional.extOption.countLength;

  xMachineAttributes := TMachineAttributes.Create(aSettings);
  Plus := xMachineAttributes.IsPlus;
  xMachineAttributes.Free;

  PutPara(xCount);
end;
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.SetPrintable(const aValue: Boolean);
begin
  if aValue <> fPrintable then
  begin
    fPrintable := aValue;
  end;
  BuildBox;
end;
//------------------------------------------------------------------------------

procedure TYarnCountEditBox.ValidatePara(aTag: Integer);
var
  xPara: TYarnCountSettingsRec;
  xValues: Integer;
begin
  GetPara(xPara);
  if xPara.posDiaDiff <> 0 then
    case aTag of
      cYCCorse:
        begin
          xValues := Round(Values[cYCCorse] * 10);

          if TYarnUnits(YarnUnit) in cYarnUnitsByLength then
            if xValues > YarnCount then
              xPara.posDiaDiff := CalculateDiaDiffByLength(xValues, YarnCount)
            else
              xPara.posDiaDiff := cMinDiaDiff
          else if xValues < YarnCount then
            xPara.posDiaDiff := CalculateDiaDiffByWeight(xValues, YarnCount)
          else
            xPara.posDiaDiff := cMinDiaDiff;
        end;

      cYCFine:
        begin
          xValues := Round(Values[cYCFine] * 10);

          if TYarnUnits(YarnUnit) in cYarnUnitsByLength then
            if xValues < YarnCount then
              xPara.negDiaDiff := CalculateDiaDiffByLength(xValues, YarnCount)
            else
              xPara.negDiaDiff := cMinDiaDiff
          else if xValues > YarnCount then
            xPara.negDiaDiff := CalculateDiaDiffByWeight(xValues, YarnCount)
          else
            xPara.negDiaDiff := cMinDiaDiff;
          if not Plus then
            xPara.posDiaDiff := xPara.negDiaDiff;
        end;
    end;
  PutPara(xPara);

end;
//------------------------------------------------------------------------------

//******************************************************************************
// TSpliceEditBox
//******************************************************************************
//------------------------------------------------------------------------------

procedure TSpliceEditBox.BuildBox;
begin
  inherited;

//  VerticalSpace[cSPDUpY] := xSpace + 0;
  AddField(cSPDUpY, cVSSPUpYDiameter, cPTCHLongDiameter, Printable);

end;
//------------------------------------------------------------------------------

constructor TSpliceEditBox.Create(aOwner: TComponent);
begin
  inherited;
  Caption := cSPCaption;
  BuildBox;

  mUpperYarn.dia := Round(Values[cSPDUpY] * 100);
  mUpperYarn.sw := TextTypToParaSwitch(TextType[cSPDUpY]);

  UpperYarn := False;
end;
//------------------------------------------------------------------------------

procedure TSpliceEditBox.GetPara(var aPara: TChannelSettingsRec);
begin
  inherited;
  aPara.upperYarn.dia := Round(Values[cSPDUpY] * 100);
  aPara.upperYarn.sw := TextTypToParaSwitch(TextType[cSPDUpY]);

end;
//------------------------------------------------------------------------------

function TSpliceEditBox.GetUpperYarn: Boolean;
begin
  Result := EnabledField[cSPDUpY];
end;
//------------------------------------------------------------------------------

procedure TSpliceEditBox.PutPara(var aPara: TChannelSettingsRec);
begin
  inherited;
  if UpperYarn then
  begin
    Values[cSPDUpY] := aPara.upperYarn.dia / 100;
    TextType[cSPDUpY] := ParaSwitchToTextType(aPara.upperYarn.sw);
  end;
  mUpperYarn := aPara.upperYarn;
end;
//------------------------------------------------------------------------------

procedure TSpliceEditBox.PutYMSettings(var aSettings: TYMSettingsRec);
var
  xMachineAttributes: TMachineAttributes;
  xConfigCode: TConfigurationCode;
begin

  xConfigCode := TConfigurationCode.Create(aSettings);
  xMachineAttributes := TMachineAttributes.Create(aSettings);

  UpperYarn := xMachineAttributes.IsUpperYarn or xConfigCode.UpperYarnCheck;

  xMachineAttributes.Free;
  xConfigCode.Free;

  PutPara(aSettings.splice);
end;
//------------------------------------------------------------------------------

procedure TSpliceEditBox.SetUpperYarn(const aValue: Boolean);
begin
  if EnabledField[cSPDUpY] <> aValue then
  begin
    EnabledField[cSPDUpY] := aValue;
    if aValue then
    begin
      Values[cSPDUpY] := mUpperYarn.dia / 100;
      TextType[cSPDUpY] := ParaSwitchToTextType(mUpperYarn.sw);
    end
    else
    begin
      mUpperYarn.dia := Round(Values[cSPDUpY] * 100);
      mUpperYarn.sw := TextTypToParaSwitch(TextType[cSPDUpY]);
      TextType[cSPDUpY] := ttCmdDisabled;
    end;
  end;

end;
//------------------------------------------------------------------------------

//******************************************************************************
// TSFIEditBox
//******************************************************************************
//------------------------------------------------------------------------------

procedure TSFIEditBox.BuildBox;
var
  xSpace: Integer;
begin
  inherited;

  xSpace := VerticalSpace[cSFIReference];
  VerticalSpace[cSFIReference] := xSpace - 2;
  AddField(cSFIReference, cVSSFIReferenc, cPTSFIReferenc, Printable);

  VerticalSpace[cSFIPosLimit] := xSpace + 0;
  AddField(cSFIPosLimit, cVSSFIPosLimit, cPTSFILimit, Printable);

  VerticalSpace[cSFINegLimit] := xSpace - 6;
  AddField(cSFINegLimit, cVSSFINegLimit, cPTSFILimit, Printable);

end;

//------------------------------------------------------------------------------

constructor TSFIEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  CaptionWidth := 60;
  ValueWidth := 50;
  MeasureWidth := 16;

  Caption := cSFICaption;

  fPrintable := False;

  BuildBox;
end;
//------------------------------------------------------------------------------

procedure TSFIEditBox.DoConfirm(aTag: Integer);
begin

  ValidatePara(aTag);

  inherited;
end;
//------------------------------------------------------------------------------

procedure TSFIEditBox.GetPara(var aPara: TSFISettingsRec);
begin

  if TextType[cSFIReference] = ttCmdDisabled then
    aPara.absRef := cFloatRefSFI
  else
    aPara.absRef := Round(Values[cSFIReference] * 100);

  if TextType[cSFIPosLimit] = ttCmdDisabled then
    aPara.upper := cSFILimitOff
  else
    aPara.upper := Round(Values[cSFIPosLimit] * 10);

  if TextType[cSFINegLimit] = ttCmdDisabled then
    aPara.lower := cSFILimitOff
  else
    aPara.lower := Round(Values[cSFINegLimit] * 10);

end;
//------------------------------------------------------------------------------

procedure TSFIEditBox.PutPara(var aPara: TSFISettingsRec);
begin

  if aPara.absRef = cFloatRefSFI then
    TextType[cSFIReference] := ttCmdDisabled
  else
  begin
    TextType[cSFIReference] := ttFloat;
    Values[cSFIReference] := aPara.absRef / 100;
  end;

  if (aPara.upper = cSFILimitOff) or (aPara.lower = cSFILimitOff) then
  begin
    TextType[cSFIPosLimit] := ttCmdDisabled;
    TextType[cSFINegLimit] := ttCmdDisabled
  end
  else
  begin
    TextType[cSFIPosLimit] := ttFloat;
    Values[cSFIPosLimit] := aPara.upper / 10;

    TextType[cSFINegLimit] := ttFloat;
    Values[cSFINegLimit] := aPara.lower / 10;
  end

end;
//------------------------------------------------------------------------------

procedure TSFIEditBox.PutYMSettings(var aSettings: TYMSettingsRec);
var
  xPara: TSFISettingsRec;
  xMachineAttributes: TMachineAttributes;
begin

  xMachineAttributes := TMachineAttributes.Create(aSettings);

  Enabled := xMachineAttributes.IsSFI;

  xMachineAttributes.Free;

  xPara.absRef := aSettings.sFI.absRef;
  xPara.upper := aSettings.sFI.upper;
  xPara.lower := aSettings.sFI.lower;

  PutPara(xPara);

end;
//------------------------------------------------------------------------------

procedure TSFIEditBox.SetPrintable(const aValue: Boolean);
var
  xPara: TSFISettingsRec;
begin

  if aValue <> fPrintable then
  begin
    GetPara(xPara);
    fPrintable := aValue;
    BuildBox;
    PutPara(xPara);
  end;
end;
//------------------------------------------------------------------------------

procedure TSFIEditBox.ValidatePara(aTag: Integer);
begin
  case aTag of
    cSFIPosLimit:
      begin

        if TextType[cSFIPosLimit] = ttCmdDisabled then
          TextType[cSFINegLimit] := ttCmdDisabled
        else if TextType[cSFINegLimit] = ttCmdDisabled then
        begin
          TextType[cSFINegLimit] := ttFloat;
          Values[cSFINegLimit] := Values[cSFIPosLimit];
        end;
      end;

    cSFINegLimit:
      begin
        if TextType[cSFINegLimit] = ttCmdDisabled then
          TextType[cSFIPosLimit] := ttCmdDisabled
        else if TextType[cSFIPosLimit] = ttCmdDisabled then
        begin
          TextType[cSFIPosLimit] := ttFloat;
          Values[cSFIPosLimit] := Values[cSFINegLimit];
        end;
      end;
  end;

end;
//------------------------------------------------------------------------------

//******************************************************************************
// TFFClusterEditBox
//******************************************************************************
//------------------------------------------------------------------------------

procedure TFFClusterEditBox.BuildBox;
var
  xSpace: Integer;
begin
  inherited;

  xSpace := VerticalSpace[0];
  VerticalSpace[cFFCLength] := xSpace - 2;
  AddField(cFFCLength, cVSFCLength, cPTFCLength, Printable);
  VerticalSpace[cFFCFaults] := xSpace - 8;
  AddField(cFFCFaults, cVSFFCFaults, cPTFCFaults, Printable);

end;
//------------------------------------------------------------------------------

constructor TFFClusterEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  CaptionWidth := 65;
  ValueWidth := 35; // textsize 3, (3)
  MeasureWidth := 10;

  fPrintable := False;
  fSynchronizeFFMatrix := nil;

  Caption := cFFCCaption;
  fPrintable := False;

  BuildBox;

end;
//------------------------------------------------------------------------------

procedure TFFClusterEditBox.DoConfirm(aTag: Integer);
begin
  case aTag of
    cFFCLength:
      if TextType[cFFCLength] = ttCmdDisabled then
      begin
        TextType[cFFCFaults] := ttCmdDisabled;
        Captions[cFFCFaults] := Translate(cVSFFCFaults.caption);
      end
      else
      begin
        TextType[cFFCFaults] := ttFloat;
        Captions[cFFCFaults] := Translate(cVSFFCFaults.caption) + '/' +
          Format(cVSFCLength.format, [Values[cFFCLength]]) + ' ' +
          Translate(cVSFCLength.measure);
      end;

    cFFCFaults:
      if TextType[cFFCFaults] = ttCmdDisabled then
      begin
        TextType[cFFCLength] := ttCmdDisabled;
        Captions[cFFCFaults] := Translate(cVSFFCFaults.caption);
      end
      else
      begin
        TextType[cFFCLength] := ttFloat;
        Captions[cFFCFaults] := Translate(cVSFFCFaults.caption) + '/' +
          Format(cVSFCLength.format, [Values[cFFCLength]]) + ' ' +
          Translate(cVSFCLength.measure);
      end;
  end;

  inherited;
end;
//------------------------------------------------------------------------------

procedure TFFClusterEditBox.GetPara(var aPara: TFFClusterSettingsRec);
begin

  if TextType[cFFCLength] = ttCmdDisabled then
    aPara.clusterLength := 0 // 0 = Off
  else
    aPara.clusterLength := Round(Values[cFFCLength] * 100);

  aPara.clusterDefects := Round(Values[cFFCFaults]);

end;
//------------------------------------------------------------------------------

procedure TFFClusterEditBox.PutPara(var aPara: TFFClusterSettingsRec);
begin
  with aPara do
  begin

    if clusterLength = 0 then
    begin
      TextType[cFFCLength] := ttCmdDisabled;
      TextType[cFFCFaults] := ttCmdDisabled;
      Captions[cFFCFaults] := Translate(cVSFFCFaults.caption);
    end
    else
    begin

      TextType[cFFCLength] := ttFloat;
      Values[cFFCLength] := clusterLength / 100;

      TextType[cFFCFaults] := ttFloat;
      Values[cFFCFaults] := clusterDefects;
      Captions[cFFCFaults] := Translate(cVSFFCFaults.caption) + '/' +
        Format(cVSFCLength.format, [Values[cFFCLength]]) + ' ' +
        Translate(cVSFCLength.measure);
    end;
  end;

end;
//------------------------------------------------------------------------------

procedure TFFClusterEditBox.PutYMSettings(var aSettings: TYMSettingsRec);
var
  xPara: TFFClusterSettingsRec;
  xMachineAttributes: TMachineAttributes;
  xConfigCode: TConfigurationCode;
begin

  xConfigCode := TConfigurationCode.Create(aSettings);
  xMachineAttributes := TMachineAttributes.Create(aSettings);

  Enabled := xMachineAttributes.IsFFCluster and xConfigCode.FFDetection
    and not xConfigCode.FFBDDetection;

  xMachineAttributes.Free;
  xConfigCode.Free;

  xPara.clusterLength := aSettings.fFCluster.obsLength;
  xPara.clusterDefects := aSettings.fFCluster.defects;

  PutPara(xPara);
  if Assigned(SynchronizeFFMatrix) then
    SynchronizeFFMatrix;
end;
//------------------------------------------------------------------------------

procedure TFFClusterEditBox.SetPrintable(const aValue: Boolean);
var
  xPara: TFFClusterSettingsRec;
begin

  if aValue <> fPrintable then
  begin
    GetPara(xPara);

    fPrintable := aValue;
    BuildBox;
    PutPara(xPara);
  end;

end;
//------------------------------------------------------------------------------

end.

