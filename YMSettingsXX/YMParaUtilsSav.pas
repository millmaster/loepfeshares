(*=========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMParaUtils.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Supports initialisation and other helper functions for YarnMaster settings
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.10.1999  1.00  Kr  | Initial Release, Definitive Yarn Count Units fehlt
| 03.11.1999  1.01  Mg  | TVoidDataItem and
| 16.11.1999  1.02NueKr | Swap's bei Get*-Methoden (Aufruf von Extract) rausgenommen.
| 12.04.2000  1.03  Nue | KorrekturHack temporaer!!!!!!!!!! NueWSCPatch added
| 08.05.2000  1.04  Kr  | GetProdGrpYMPara and PutProdGrpYMPara countUnit and threadCnt added
|=========================================================================================*)
unit YMParaUtilsSav;

interface

uses
  YMParaDef, LoepfeGlobal, MMUGlobal, Windows, sysutils;
const
//------------------------------------------------------------------------------
// Test initialisation (more or less Memory C)
//------------------------------------------------------------------------------
  //...........................................................................
{(*}

 cYMSettingsC: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      fill0:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      6;
     machType:         2;
      speedRamp:        5;
      inoperativePara: ( 0, 0 );
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            550;
        sw:             cChOn;
//        sw:             cChOff;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         13;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            500;
        sw:             cChOn;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          150;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      upperYarn: (
        dia:            141;
        sw:             cChOn;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $80, $C0, $F0, $F8, $FC, $FE, $FF, $FF,
                  $F0, $C0, $80, $00, $00, $C0, $E0, $F8);
    siroClear: ( $00, $F0, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        180;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         3;
        siroStartupRep: 6;
        clusterDia:     135;
        clusterLength:  1000;
        clusterDefects: 30;
      );
      configA:          $001B;
      configB:          $0020;
      configC:          $0000;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     160;
       countLength:		1;
       clusterRep:     3;
        cutRetries:     3;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      10;
      lengthWindow:     11;
      lengthMode:       12;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            100;
      lower:            100;
      rep:              3;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        1000;
      defects:          30;
      rep:              3;
      fill0:            0;
      siroClear: ( $8F, $8F, $CF, $EF, $EF, $FF, $FF, $FF );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsTest: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      fill0:            0;
      group:            1;
      prodGrpID:        1291;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      6;
     machType:         2;
      speedRamp:        5;
      inoperativePara: ( (cIPMNoPlus or cIPMNoSpeedSim or cIPMNoUpperYarn or cIPMSHTypeF), 0 );
//      inoperativePara: ( (cIPMNoPlus or cIPMNoUpperYarn or cIPMNoSpeedSim), 0 );
//      inoperativePara: ( (cIPMNoPlus or cIPMNoUpperYarn), 0 );
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            cMinNepsDia;
        sw:             cChOn;
//        sw:             cChOff;
      );
      short: (
        dia:            cMinShortDia;
        sw:             cChOn;
      );
      shortLen:         cMinShortLen;
      long: (
        dia:            cMinLongDia;
        sw:             cChOn;
      );
      longLen:          cMinLongLen;
      thin: (
        dia:            cMaxThinDia;
        sw:             cChOn;
      );
      thinLen:          cMinThinLen;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            cMinNepsDia;
        sw:             cChOn;
//        sw:             cChOff;
      );
      short: (
        dia:            cMinShortDia;
        sw:             cChOn;
      );
      shortLen:         cMinShortLen;
      long: (
        dia:            cMinLongDia;
        sw:             cChOn;
      );
      longLen:          cMinLongLen;
      thin: (
        dia:            cMaxThinDia;
        sw:             cChOn;
      );
      thinLen:          cMinThinLen;
      upperYarn: (
        dia:            141;
        sw:             cChOn;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $80, $C0, $F0, $F8, $FC, $FE, $FF, $FF,
                  $F0, $C0, $80, $00, $00, $C0, $E0, $F8);
    siroClear: ( $00, $F0, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        180;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         3;
        siroStartupRep: 6;
        clusterDia:     135;
        clusterLength:  1000;
        clusterDefects: 30;
      );
      configA:          $001B;
      configB:          $0020;
      configC:          $0000;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     160;
       countLength:		1;
       clusterRep:     3;
        cutRetries:     3;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      10;
      lengthWindow:     11;
      lengthMode:       12;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            100;
      lower:            100;
      rep:              3;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        1000;
      defects:          30;
      rep:              3;
      fill0:            0;
      siroClear: ( $8F, $8F, $CF, $EF, $EF, $FF, $FF, $FF );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

  cChannelOff: TChannelSettingsRec = (
    neps: (
      dia: cMinNepsDia;
      sw: cChOff;
    );
    short: (
      dia: cMinShortDia;
      sw: cChOff;
    );
    shortLen: cMinShortLen;
    long: (
      dia: cMinLongDia;
      sw: cChOff;
    );
    longLen: cMinLongLen;
    thin: (
      dia: cMaxThinDia;
      sw: cChOff;
    );
    thinLen: cMinThinLen;
    splice: (
      len: 150;
      sw: cChOff;
    );
    speed: 600;
    spare: 0;
  );

  //...........................................................................

{*)}

type
//------------------------------------------------------------------------------
// Byte buffer for YM Settings
//------------------------------------------------------------------------------
  //...........................................................................

  TYMSettingsByteArr = array[1..400] of Byte; // Has to be bigger then size of
                                    // YMParaDef.TYMSettingsRec (actual 240B)
  PYMSettingsByteArr = ^TYMSettingsByteArr;
  //...........................................................................

//------------------------------------------------------------------------------
// Data rules for not available data or data needs to process
//------------------------------------------------------------------------------
  //...........................................................................

  TYMDataRulesRec = record
    voidData: Integer;
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Void Data Items
//------------------------------------------------------------------------------
  //............................................................................
  TVoidDataItem = (vdCones,             // Availability of Cones
    vdCops,                             // Availability of Cops
    vdIPOld,                            // Availability of IPOld
    vdIPNew,                            // dito
    vdIPPlus,                           // dito
    vdSFIVar,                           // dito
    vdSFICnt);                          // dito
  //............................................................................

//------------------------------------------------------------------------------
// Machine dependent parameter
//------------------------------------------------------------------------------
  //...........................................................................
  TMachineYMParaRec = record
    sensingHead: Byte;                  // TK730:0,TK740:1,TK750:2,TK770:3,TK780:4,TK930:5..
    machType: Byte;                     // enum WINDER_TYPE
    checkLen: Word;                     // splice check Length, (0..1200), [0..120 cm]
    cutRetries: Byte;                   // Cut Retries, (Repetitions+1: 1..6)
    configA: Word;
    configB: Word;
    configC: Word;
    inoperativePara: array[0..1] of Integer; // Bitset for ionoperative settings
                                              // only valid on ExtractMachineYMPara
    machBez: TMachBezArr;               // Machine name
    longStopDef: Word;
    yMType: Byte;                       // yMType ....
    yMSWVersion: TYMSWVersionArr;       // ZE Version string
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Production group dependent parameter
//------------------------------------------------------------------------------
  //...........................................................................

  TProdGrpYMParaRec = record
    group: Byte;                        // Local Group (0..5: AC338 Informator, 0..11 ZE)
    prodGrpID: Integer;                 // MillMaster's group ID, 0=not defined
    spdl: TSpindelRangeRec;
    pilotSpindles: Byte;
    speedRamp: Byte;                    // only machines with Speed Simulation
    speed: Word;                        // winding speed, (20..1600), [20..1600 m/Min]
    yarnCnt: Word;                      // Yarn count [cMinYarnCount..cMinYarnCount]
    yarnCntUnit: Byte;                  // Yarn count unit only valid on PutProdGrpYMPara
    nrOfThreads: Byte;                  // threadCnt
    // to define from Nue
    lengthWindow: Word;
    lengthMode: Byte;
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Pointer to YM Settings
//------------------------------------------------------------------------------
  //...........................................................................

  PYMSettings = ^TYMSettingsRec;
  //...........................................................................

//------------------------------------------------------------------------------
// Parameter Source Indentifier
//------------------------------------------------------------------------------
  //...........................................................................

  TParameterSource = (psUnknown,
    psChannel,                          // Channel Parameter Box Identifier
    psSplice,                           // Splice Parameter Box Identifier
    psCluster,                          // Cluster Parameter Box Identifier
    psYarnCount,                        // YarnCount Parameter Box Identifier
    psSFI,                              // SFI Parameter Box Identifier
    psNSLTClass,                        // NSLT Class Parameter Box Identifier
    psFFClass);                         // FF Class Parameter Box Identifier
  //...........................................................................

  TOnParameterChange = procedure(aParameterSource: TParameterSource) of object;
  //...........................................................................

//------------------------------------------------------------------------------
// Class Definition
//------------------------------------------------------------------------------
  //...........................................................................

  TYMSettingsUtils = class(TObject)
  private
    { Private declarations }
    class procedure ChangeEndian(var aSource, aDest: TYMSettingsRec); overload;
  public
    { Public declarations }
    function DefaultYMPara: TYMSettingsRec;

    class procedure ChangeEndian(var aSettings: TYMSettingsByteArr; aSize: Word); overload;
    class procedure CompleteSettingsFromZE(var aSettings: TYMSettingsByteArr;
      aSize: Word; aYarnUnit: TYarnUnits; aThreadCnt: Byte);
    class procedure ConvertSettingsFromAC338(var aSettings: TYMSettingsByteArr; aSize: Word);
    class procedure ConvertSettingsToAC338(var aSettings: TYMSettingsByteArr; aSize: Word);
    class procedure ValidateChannelSettings(var aPara: TChannelSettingsRec);

    class function ExtractProdGrpID(var aSettings: TYMSettingsByteArr; aSize: Word): Integer;
    class function ExtractProdGrpYMPara(var aSettings: TYMSettingsByteArr; aSize: Word): TProdGrpYMParaRec;

    procedure ExtractMachineYMPara(var aSettings: TYMSettingsRec; var aMachineYMPara: TMachineYMParaRec);
    procedure FilterMachineYMPara(var aSettings: TYMSettingsRec);
    procedure GetDataRules(var aSettings: TYMSettingsRec; var aDataRules: TYMDataRulesRec);
    class procedure GetProdGrpYMPara(var aSettings: TYMSettingsRec; var aProdGrpYMPara: TProdGrpYMParaRec);
    procedure InitYMSettings(var aSettings: TYMSettingsRec);
    procedure InsertMachineYMPara(var aMachineYMPara: TMachineYMParaRec; var aSettings: TYMSettingsRec);
    procedure InsertInopSettingsToSack(var aSettings: TYMSettingsByteArr; aMachinePara: TMachineYMParaRec);
    procedure PutProdGrpYMPara(var aProdGrpYMPara: TProdGrpYMParaRec; var aSettings: TYMSettingsRec;
      aAssignment: Byte
                            // cAssigGroup: Assign settings and its prodGrpID to the Maschinegroup
                           // cAssigMemory:	Assign settings to a memory (not yet used)
                            // cAssigProdGrpIDOnly: Assign only the prodGrpID to the Maschinegroup
      );

{nue}
//    class function NueWSCPatch(aProdGrpYMPara: TProdGrpYMParaRec; var aSettings: TYMSettingsByteArr): Integer;


  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Free functions
//------------------------------------------------------------------------------
  //............................................................................

function CalculateCountByLength(aMinMaxDia, aCount: Cardinal): Cardinal;
function CalculateCountByWeight(aMinMaxDia, aCount: Cardinal): Cardinal;
function CalculateDiaDiffByLength(aCorseFine, aCount: Cardinal): Cardinal;
function CalculateDiaDiffByWeight(aCorseFine, aCount: Cardinal): Cardinal;
function GetAvailabilityOfDataItem(aVoidData: DWord; aDataItem: TVoidDataItem): boolean;
  //............................................................................

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
//------------------------------------------------------------------------------

function CalculateCountByLength(aMinMaxDia, aCount: Cardinal): Cardinal;
var xScratch: Cardinal;
begin
  xScratch := aMinMaxDia;
  try
    xScratch := (xScratch * xScratch div 100) * aCount div 1000;
    if (xScratch mod 10) >= 5 then
      Result := Word(xScratch div 10 + 1)
    else
      Result := Word(xScratch div 10);
  except
    on e: Exception do
    begin
      raise Exception.Create('CalculateCountByLength failed.' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

function CalculateCountByWeight(aMinMaxDia, aCount: Cardinal): Cardinal;
var xScratch: Cardinal;
begin
  xScratch := aMinMaxDia;

  try
    xScratch := (Cardinal(aCount) * 10000) div (xScratch * xScratch div 1000);
    if (xScratch mod 10) >= 5 then
      Result := Word(xScratch div 10 + 1)
    else
      Result := Word(xScratch div 10);
  except
    on e: Exception do
    begin
      raise Exception.Create('CalculateCountByWeight failed.' + e.Message);
    end;
  end;

end;
//------------------------------------------------------------------------------

function CalculateDiaDiffByLength(aCorseFine, aCount: Cardinal): Cardinal;
var xScratch: Cardinal;
begin
  try
    xScratch := Round(Sqrt(aCorseFine / aCount) * 1000);
  except
    on e: Exception do
    begin
      raise Exception.Create('CalculateDiaDiffByLength failed.' + e.Message);
    end;
  end;

  if xScratch < 1000 then
    Result := 1000 - xScratch
  else
    Result := xScratch - 1000;
end;
//------------------------------------------------------------------------------

function CalculateDiaDiffByWeight(aCorseFine, aCount: Cardinal): Cardinal;
var xScratch: Cardinal;
begin

  try
    xScratch := Round(Sqrt(aCount / aCorseFine) * 1000);
  except
    on e: Exception do
    begin
      raise Exception.Create('CalculateDiaDiffByWeight failed.' + e.Message);
    end;
  end;

  if xScratch < 1000 then
    Result := 1000 - xScratch
  else
    Result := xScratch - 1000;
end;
//------------------------------------------------------------------------------

function GetAvailabilityOfDataItem(aVoidData: DWord; aDataItem: TVoidDataItem): boolean;
begin
  case aDataItem of
    vdCones: Result := false;
    vdCops: Result := false;
    vdIPOld: Result := false;
    vdIPNew: Result := true;
    vdIPPlus: Result := false;
    vdSFIVar: Result := true;
    vdSFICnt: Result := true;
  else
    Result := true;
  end;
end;
//------------------------------------------------------------------------------


  //------------------------------------------------------------------------------
//******************************************************************************
// TYMSettingsUtils
//******************************************************************************
//------------------------------------------------------------------------------

class procedure TYMSettingsUtils.ChangeEndian(var aSource, aDest: TYMSettingsRec);
begin
  Move(aSource, aDest, SizeOf(aDest));

  with aSource.available do
  begin
    aDest.available.prodGrpID := SwapDWord(prodGrpID);
    aDest.available.spdl.contents := Swap(spdl.contents); // Word
    aDest.available.inoperativePara[0] := SwapDWord(inoperativePara[0]);
    aDest.available.inoperativePara[1] := SwapDWord(inoperativePara[1]);
    aDest.available.voidData := SwapDWord(voidData);
    aDest.available.spare := SwapDWord(spare);
  end;

  with aSource.channel do
  begin
    aDest.channel.neps.dia := Swap(neps.dia);
    aDest.channel.neps.sw := Swap(neps.sw);
    aDest.channel.short.dia := Swap(short.dia);
    aDest.channel.short.sw := Swap(short.sw);
    aDest.channel.shortLen := Swap(shortLen);
    aDest.channel.long.dia := Swap(long.dia);
    aDest.channel.long.sw := Swap(long.sw);
    aDest.channel.longLen := Swap(longLen);
    aDest.channel.thin.dia := Swap(thin.dia);
    aDest.channel.thin.sw := Swap(thin.sw);
    aDest.channel.thinLen := Swap(thinLen);
    aDest.channel.splice.len := Swap(splice.len);
    aDest.channel.splice.sw := Swap(splice.sw);
    aDest.channel.speed := Swap(speed);
    aDest.channel.spare := Swap(spare);
  end;

  with aSource.splice do
  begin
    aDest.splice.neps.dia := Swap(neps.dia);
    aDest.splice.neps.sw := Swap(neps.sw);
    aDest.splice.short.dia := Swap(short.dia);
    aDest.splice.short.sw := Swap(short.sw);
    aDest.splice.shortLen := Swap(shortLen);
    aDest.splice.long.dia := Swap(long.dia);
    aDest.splice.long.sw := Swap(long.sw);
    aDest.splice.longLen := Swap(longLen);
    aDest.splice.thin.dia := Swap(thin.dia);
    aDest.splice.thin.sw := Swap(thin.sw);
    aDest.splice.thinLen := Swap(thinLen);
    aDest.splice.upperYarn.dia := Swap(upperYarn.dia);
    aDest.splice.upperYarn.sw := Swap(upperYarn.sw);
    aDest.splice.checkLen := Swap(checkLen);
    aDest.splice.spare := Swap(spare);
  end;

  with aSource.additional.option do
  begin
    aDest.additional.option.diaDiff := Swap(diaDiff);
    aDest.additional.option.yarnCnt := Swap(yarnCnt);
    aDest.additional.option.yarnUnit := Swap(yarnUnit);
    aDest.additional.option.threadCnt := Swap(threadCnt);
    aDest.additional.option.clusterDia := Swap(clusterDia);
    aDest.additional.option.clusterLength := Swap(clusterLength);
    aDest.additional.option.clusterDefects := Swap(clusterDefects);
  end;

  with aSource.additional do
  begin
    aDest.additional.configA := Swap(configA);
    aDest.additional.configB := Swap(configB);
    aDest.additional.configC := Swap(configC);
    aDest.additional.spare[0] := SwapDWord(spare[0]);
    aDest.additional.spare[1] := SwapDWord(spare[1]);
  end;

  with aSource.additional.extOption do
  begin
    aDest.additional.extOption.negDiaDiff := Swap(negDiaDiff);
    aDest.additional.extOption.spare[0] := SwapDWord(spare[0]);
    aDest.additional.extOption.spare[1] := SwapDWord(spare[1]);
  end;

  with aSource.machSet do
  begin
    aDest.machSet.longStopDef := Swap(longStopDef);
    aDest.machSet.lengthWindow := Swap(lengthWindow);
    aDest.machSet.spare[0] := SwapDWord(spare[0]);
    aDest.machSet.spare[1] := SwapDWord(spare[1]);
  end;

  with aSource.sFI do
  begin
    aDest.sFI.absRef := Swap(absRef);
    aDest.sFI.upper := Swap(upper);
    aDest.sFI.lower := Swap(lower);
  end;

  with aSource.fFCluster do
  begin
    aDest.fFCluster.obsLength := Swap(obsLength);
    aDest.fFCluster.defects := Swap(defects);
  end;

  with aSource.spare do
  begin
    aDest.spare.fill[0] := SwapDWord(fill[0]);
    aDest.spare.fill[1] := SwapDWord(fill[1]);
    aDest.spare.fill[2] := SwapDWord(fill[2]);
    aDest.spare.fill[3] := SwapDWord(fill[3]);
    aDest.spare.fill[4] := SwapDWord(fill[4]);
    aDest.spare.fill[5] := SwapDWord(fill[5]);
  end;
end;
//------------------------------------------------------------------------------

class procedure TYMSettingsUtils.ChangeEndian(var aSettings: TYMSettingsByteArr;
  aSize: Word);
begin
  if aSize >= SizeOf(TYMSettingsRec) then
    ChangeEndian(PYMSettings(@aSettings)^, PYMSettings(@aSettings)^);
end;
//------------------------------------------------------------------------------

class procedure TYMSettingsUtils.CompleteSettingsFromZE(
  var aSettings: TYMSettingsByteArr; aSize: Word; aYarnUnit: TYarnUnits;
  aThreadCnt: Byte);
begin

  if aSize >= SizeOf(TYMSettingsRec) then
  begin
    with PYMSettings(@aSettings)^ do
    begin

      additional.option.yarnUnit := Word(aYarnUnit);
      additional.option.threadCnt := Word(aThreadCnt);
    end;
  end;
end;
//------------------------------------------------------------------------------

class procedure TYMSettingsUtils.ConvertSettingsFromAC338(
  var aSettings: TYMSettingsByteArr; aSize: Word);
begin
  if aSize >= SizeOf(TYMSettingsRec) then
  begin
    with PYMSettings(@aSettings)^ do
    begin

    // Thin Diameter Conversion
      channel.thin.dia := 100 - channel.thin.dia;
      splice.thin.dia := 100 - splice.thin.dia;

    // Repetition Conversion
      additional.option.cntRep := additional.option.cntRep + 1;
      additional.option.siroStartupRep := additional.option.siroStartupRep + 1;
      additional.extOption.clusterRep := additional.extOption.clusterRep + 1;
      additional.extOption.cutRetries := additional.extOption.cutRetries + 1;
      fFCluster.rep := fFCluster.rep + 1;
    end;
  end;
end;
//------------------------------------------------------------------------------

class procedure TYMSettingsUtils.ConvertSettingsToAC338(
  var aSettings: TYMSettingsByteArr; aSize: Word);
begin
  if aSize >= SizeOf(TYMSettingsRec) then
  begin
    with PYMSettings(@aSettings)^ do
    begin

    // Thin Diameter Conversion
      channel.thin.dia := 100 - channel.thin.dia;
      splice.thin.dia := 100 - splice.thin.dia;

    // Repetition Conversion
      additional.option.cntRep := additional.option.cntRep - 1;
      additional.option.siroStartupRep := additional.option.siroStartupRep - 1;
      additional.extOption.clusterRep := additional.extOption.clusterRep - 1;
      additional.extOption.cutRetries := additional.extOption.cutRetries - 1;
      fFCluster.rep := fFCluster.rep - 1;
    end;
  end;
end;
//------------------------------------------------------------------------------

function TYMSettingsUtils.DefaultYMPara: TYMSettingsRec;
begin
  Result := cYMSettingsC;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsUtils.ExtractMachineYMPara(var aSettings: TYMSettingsRec;
  var aMachineYMPara: TMachineYMParaRec);
begin
  with aMachineYMPara, aSettings do
  begin

    sensingHead := available.sensingHead;
    machType := available.machType;
    inoperativePara[0] := available.inoperativePara[0];
    inoperativePara[1] := available.inoperativePara[1];
    checkLen := splice.checkLen;
    cutRetries := additional.extOption.cutRetries;
    configA := additional.configA;
    configB := additional.configB;
    configC := additional.configC;
    machBez := machSet.machBez;
    longStopDef := machSet.longStopDef;
    yMType := machSet.yMType;
    yMSWVersion := machSet.yMSWVersion;

    if (machType = cWTAC338) or (machType = cWTAC338Spectra) then
    begin

      configA := configA and cCCA338Machine;
      configB := configB and cCCB338Machine;
      configC := configC and cCCC338Machine;
    end
    else
    begin

      configA := configA and cCCAMachine;
      configB := configB and cCCBMachine;
      configC := configC and cCCCMachine;
    end;
  end;

  FilterMachineYMPara(aSettings);
end;
//------------------------------------------------------------------------------

class function TYMSettingsUtils.ExtractProdGrpID(
  var aSettings: TYMSettingsByteArr; aSize: Word): Integer;
begin
  if aSize >= SizeOf(TYMSettingsRec) then
    Result := PYMSettings(@aSettings).available.prodGrpID
  else
    Result := 0;
end;
//------------------------------------------------------------------------------

class function TYMSettingsUtils.ExtractProdGrpYMPara(
  var aSettings: TYMSettingsByteArr; aSize: Word): TProdGrpYMParaRec;
begin

  if aSize >= SizeOf(TYMSettingsRec) then
    GetProdGrpYMPara(PYMSettings(@aSettings)^, Result)
  else
    FillChar(Result, SizeOf(Result), 0);
end;
//------------------------------------------------------------------------------

procedure TYMSettingsUtils.FilterMachineYMPara(
  var aSettings: TYMSettingsRec);
begin
  with aSettings.additional, aSettings.available do
  begin
   // Extract
    if (machType = cWTAC338) or (machType = cWTAC338Spectra) then
    begin

      configA := configA and not (cCCA338Machine and not cCCAProduction);
      configB := configB and not (cCCB338Machine and not cCCBProduction);
      configC := configC and not (cCCC338Machine and not cCCCProduction);
    end
    else
    begin

      configA := configA and not (cCCAMachine and not cCCAProduction);
      configB := configB and not (cCCBMachine and not cCCBProduction);
      configC := configC and not (cCCCMachine and not cCCCProduction);
    end;
  end;

end;
//------------------------------------------------------------------------------

procedure TYMSettingsUtils.GetDataRules(var aSettings: TYMSettingsRec;
  var aDataRules: TYMDataRulesRec);
begin
  with aDataRules, aSettings do
  begin
    voidData := available.voidData;
  end;
end;
//------------------------------------------------------------------------------

class procedure TYMSettingsUtils.GetProdGrpYMPara(var aSettings: TYMSettingsRec;
  var aProdGrpYMPara: TProdGrpYMParaRec);
begin
  with aProdGrpYMPara, aSettings do
  begin
    group := available.group;
    prodGrpID := available.prodGrpID;
    spdl.contents := available.spdl.contents;
    pilotSpindles := available.pilotSpindles;
    speedRamp := available.speedRamp;
    speed := channel.speed;
    yarnCnt := additional.option.yarnCnt;
    yarnCntUnit := additional.option.yarnUnit;
    nrOfThreads := additional.option.threadCnt;
    lengthWindow := machSet.lengthWindow;
    lengthMode := machSet.lengthMode;
  end;

end;
//------------------------------------------------------------------------------

procedure TYMSettingsUtils.InitYMSettings(var aSettings: TYMSettingsRec);
begin
  FillChar(aSettings, SizeOf(aSettings), 0);
  aSettings.channel.splice.sw := cChExtSp;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsUtils.InsertInopSettingsToSack(
  var aSettings: TYMSettingsByteArr; aMachinePara: TMachineYMParaRec);
begin
  PYMSettings(@aSettings).available.inoperativePara[0] := aMachinePara.inoperativePara[0];
  PYMSettings(@aSettings).available.inoperativePara[1] := aMachinePara.inoperativePara[1];
end;
//------------------------------------------------------------------------------

procedure TYMSettingsUtils.InsertMachineYMPara(
  var aMachineYMPara: TMachineYMParaRec; var aSettings: TYMSettingsRec);
begin
  with aMachineYMPara, aSettings do
  begin
    available.sensingHead := sensingHead;
    available.machType := machType;
    available.inoperativePara[0] := 0;
    available.inoperativePara[1] := 0;
    splice.checkLen := checkLen;
    additional.extOption.cutRetries := cutRetries;
    machSet.machBez := machBez;
    machSet.longStopDef := longStopDef;
    machSet.yMType := yMType;
    machSet.yMSWVersion := yMSWVersion;

    if (machType = cWTAC338) or (machType = cWTAC338Spectra) then
    begin

      additional.configA := additional.configA or
        ((additional.configA or not cCCA338Production) and
        (configA and cCCA338Machine));

      additional.configB := additional.configB or
        ((additional.configB or not cCCB338Production) and
        (configB and cCCB338Machine));

      additional.configC := additional.configC or
        ((additional.configC or not cCCC338Production) and
        (configC and cCCC338Machine));
    end
    else
    begin

      additional.configA := additional.configA or
        ((additional.configA or not cCCAProduction) and
        (configA and cCCAMachine));

      additional.configB := additional.configB or
        ((additional.configB or not cCCBProduction) and
        (configB and cCCBMachine));

      additional.configC := additional.configC or
        ((additional.configC or not cCCCProduction) and
        (configC and cCCCMachine));
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsUtils.PutProdGrpYMPara(var aProdGrpYMPara: TProdGrpYMParaRec;
  var aSettings: TYMSettingsRec;
  aAssignment: Byte
  // cAssigGroup: Assign settings and its prodGrpID to the Maschinegroup
 // cAssigMemory:	Assign settings to a memory (not yet used)
  // cAssigProdGrpIDOnly: Assign only the prodGrpID to the Maschinegroup
  );
begin

  with aProdGrpYMPara, aSettings do
  begin
    available.assignement := aAssignment;
    available.group := group;
    available.prodGrpID := prodGrpID;
    available.spdl.contents := spdl.contents;
    available.pilotSpindles := pilotSpindles;

    available.speedRamp := speedRamp;
    channel.speed := speed;

    additional.option.yarnCnt := yarnCnt;
    additional.option.threadCnt := nrOfThreads;
    additional.option.yarnUnit := yarnCntUnit;

    machSet.lengthWindow := lengthWindow;
    machSet.lengthMode := lengthMode;

(*    if TYarnUnits(yarnCntUnit) in cYarnUnitsByLength then begin
      additional.option.minCnt := Word(CalculateCountByLength(
                                  1000+additional.option.diaDiff, yarnCnt));
      additional.option.maxCnt := Word(CalculateCountByLength(
                                  1000-additional.extOption.negDiaDiff, yarnCnt));
    end
    else begin
      additional.option.minCnt := Word(CalculateCountByWeight(
                                  1000+additional.option.diaDiff, yarnCnt));
      additional.option.maxCnt := Word(CalculateCountByWeight(
                                  1000-additional.extOption.negDiaDiff, yarnCnt));
    end;
*)

  end;
end;
//------------------------------------------------------------------------------
{not longer used
{nue
class function TYMSettingsUtils.NueWSCPatch(aProdGrpYMPara: TProdGrpYMParaRec; var aSettings: TYMSettingsByteArr): Integer;
begin
  with aProdGrpYMPara do begin
    PYMSettings(@aSettings).available.group := group;
    PYMSettings(@aSettings).available.spdl.contents := spdl.contents;
    PYMSettings(@aSettings).additional.option.yarnCnt := 600;   //Fix drin, wenn hier = ist gibt es Absturz in Storagehandler (CalcWeight)
  end;
  Result := 1;
end;
}
//------------------------------------------------------------------------------

class procedure TYMSettingsUtils.ValidateChannelSettings(
  var aPara: TChannelSettingsRec);

function FitValue(aValue, aMin, aMax: Word): Word;
  begin
    Result := aValue;
    if (aMax > aMin) then
    begin
      if aValue < aMin then
        Result := aMin
      else if aValue > aMax then
        Result := aMax;
    end;
  end;

  function ValidateSwitch(aSwitch: Word): Word;
  begin
    if aSwitch <> cChOn then
      Result := cChOff
    else
      Result := cChOn;
  end;
begin
  with aPara do
  begin
  // Neps
    neps.dia := FitValue(neps.dia, cMinNepsDia, cMaxNepsDia);
    neps.sw := ValidateSwitch(neps.sw);
  // Short
    short.dia := FitValue(short.dia, cMinShortDia, cMaxShortDia);
    short.sw := ValidateSwitch(short.sw);
    shortLen := FitValue(shortLen, cMinShortLen, cMaxShortLen);
  // Long
    long.dia := FitValue(long.dia, cMinLongDia, cMaxLongDia);
    long.sw := ValidateSwitch(long.sw);
    longLen := FitValue(longLen, cMinLongLen, cMaxLongLen);
  // Thin
    thin.dia := FitValue(thin.dia, cMinThinDia, cMaxThinDia);
    thin.sw := ValidateSwitch(thin.sw);
    thinLen := FitValue(thinLen, cMinThinLen, cMaxThinLen);
  end;
end;

end.

