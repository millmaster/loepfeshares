(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: PresetValue.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: PresetValue supports a Value with additional preset table.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 13.03.2000  0.00  Kr  | Initial Release
|=============================================================================*)
unit PresetValue;

interface

uses Classes, SysUtils, IvDictio;
//const

type
  //...........................................................................

  TTextType = (ttText, ttCmdDisabled, ttFloat, ttEmpty);

  TPresetTextRec = record
    typ: TTextType;
    text: string;
  end;
  //...........................................................................

  IPresetValue = interface(IUnknown)
    function GetValueHint: string;
    function GetValueText: string;
    procedure SetValueHint(aValue: string);
    procedure SetValueText(aValue: string);
  end;
  //...........................................................................

  TValueSpecRec = record
    minValue: Double;
    maxValue: Double;
    format: string; // format of the folat value '%4.2f' use float format strings only!
    caption: string; // label of value, if null string no label
    measure: string; // unit of mesurement, if null string no unit
    hint: string; // hint text, if null string no hint
  end;
  //...........................................................................

  TPresetValue = class(TObject)

  private
    fMinValue: Double;
    fMaxValue: Double;
    fTextType: TTextType;

    mInterface: IPresetValue;
    mPresetIndex: Integer;
    mPresetTbl: array of TPresetTextRec;
    mValueFormat: string;
    mValue: Double;

    function GetHint: string;
    function GetText: string;
    function GetValue: Double;

    procedure SetHint(const aValue: string);
    procedure SetText(const aValue: string);
    procedure SetTextType(const aValue: TTextType);
    procedure SetValue(const aNewValue: Double);

    property Text: string read GetText write SetText;
    property Hint: string read GetHint write SetHint;

  protected

    { Private declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;

    function CheckValue(aNewValue: Double): Double;
    function FindTargetText(aKey: Char): Boolean;
    function NextText: string;
    function PreviousText: string;

    procedure AttachInterface(aInterface: IPresetValue);
    procedure LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec);

    property MaxValue: Double read fMaxValue write fMaxValue;
    property MinValue: Double read fMinValue write fMinValue;
    property TextType: TTextType read fTextType write SetTextType;
    property Value: Double read GetValue write SetValue;
  end;
  //...........................................................................

//------------------------------------------------------------------------------
//  Functions and procedures
//------------------------------------------------------------------------------
  //...........................................................................
procedure CircularIncrement(var aIndex: Integer; aMaxIndex: Integer);
procedure CircularDecrement(var aIndex: Integer; aMaxIndex: Integer);

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------

procedure CircularIncrement(var aIndex: Integer; aMaxIndex: Integer);
begin
  if aIndex < aMaxIndex then Inc(aIndex)
  else aIndex := 0;
end;
//------------------------------------------------------------------------------

procedure CircularDecrement(var aIndex: Integer; aMaxIndex: Integer);
begin
  if aIndex > 0 then Dec(aIndex)
  else aIndex := aMaxIndex;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// TPresetValue
//******************************************************************************
//------------------------------------------------------------------------------

procedure TPresetValue.AttachInterface(aInterface: IPresetValue);
begin
  mInterface := aInterface;
end;
//------------------------------------------------------------------------------

function TPresetValue.CheckValue(aNewValue: Double): Double;
begin
  Result := aNewValue;
  if (fMaxValue > fMinValue) then
  begin
    if aNewValue < fMinValue then
      Result := minValue
    else if aNewValue > fMaxValue then
      Result := fMaxValue;
  end;
end;
//------------------------------------------------------------------------------

constructor TPresetValue.Create;
begin
  inherited Create;

  mInterface := nil;
  mValueFormat := '%4.2f';

  MinValue := 0;
  MaxValue := 0;
  TextType := ttFloat;
  Text := '0';
  Value := 0;

end;
//------------------------------------------------------------------------------

destructor TPresetValue.Destroy;
begin

  Finalize(mPresetTbl);
  inherited;
end;
//------------------------------------------------------------------------------

function TPresetValue.FindTargetText(aKey: Char): Boolean;
var
  i: Integer;
  function FindAcceleratorChar(aText: string): char;
  var
    xPos: Integer;
  begin
    xPos := Pos('&', aText);
    if xPos <> 0 then begin
      if (xPos + 1) <= Length(aText) then
        Result := aText[xPos + 1]
      else
        Result := #0;
    end
    else
      Result := #0;
  end;
begin
  Result := False;

  for i := 0 to High(mPresetTbl) do begin
//            if (mPresetTbl[i].typ <> ttFloat) and (Pos(UpCase(aKey), mPresetTbl[i].text) = 1) then begin
    if (mPresetTbl[i].typ <> ttFloat) and (UpCase(aKey) = UpCase(FindAcceleratorChar(mPresetTbl[i].text))) then begin
      mPresetIndex := i;
      Text := StringReplace(Translate(mPresetTbl[mPresetIndex].text), '&', '', [rfReplaceAll]);
      fTextType := mPresetTbl[mPresetIndex].typ;
      Result := True;
      Break;
    end;
  end;
end;
//------------------------------------------------------------------------------

function TPresetValue.GetHint: string;
begin
  if Assigned(mInterface) then
    Result := mInterface.GetValueHint;
end;
//------------------------------------------------------------------------------

function TPresetValue.GetText: string;
begin
  if Assigned(mInterface) then
    Result := mInterface.GetValueText;
end;
//------------------------------------------------------------------------------

function TPresetValue.GetValue: Double;
begin
  if TextType = ttFloat then begin
    if Text <> '' then begin
      try
        Text := StringReplace(Text, ' ', '', [rfReplaceAll]);
        Result := StrToFloat(Text);
      except
        Result := FMinValue;
      end;

    end
    else
      Result := FMinValue;
  end
  else Result := mValue;
  Result := CheckValue(Result)
end;
//------------------------------------------------------------------------------

procedure TPresetValue.LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec);
var
  i: Integer;
begin
// Value Specifications
  fMinValue := aValueSpec.minValue;
  fMaxValue := aValueSpec.maxValue;
  mValueFormat := aValueSpec.format;
  if aValueSpec.hint <> '' then begin
    Hint := Format(Translate(aValueSpec.hint) + mValueFormat + '..' + mValueFormat + ' %s',
      [MinValue, MaxValue, aValueSpec.measure]);
  end;


// Preset Table
  SetLength(mPresetTbl, Length(aPresetTbl));
  for i := 0 to High(aPresetTbl) do begin
    mPresetTbl[i] := aPresetTbl[i];
    if aPresetTbl[i].typ = ttCmdDisabled then
      Hint := Hint + '; ' + StringReplace(Translate(mPresetTbl[i].text), '&', '', [rfReplaceAll]);
  end;

  mPresetIndex := 0;
  if aPresetTbl[mPresetIndex].typ = ttFloat then
    Text := StringReplace(aPresetTbl[mPresetIndex].text, '.', DecimalSeparator, [rfReplaceAll])
  else
    Text := aPresetTbl[mPresetIndex].text;
  TextType := aPresetTbl[mPresetIndex].typ;
end;
//------------------------------------------------------------------------------

function TPresetValue.NextText: string;
begin

  if Assigned(mPresetTbl) then begin
    CircularIncrement(mPresetIndex, High(mPresetTbl));
    if mPresetTbl[mPresetIndex].typ <> ttFloat then
      Result := StringReplace(Translate(mPresetTbl[mPresetIndex].text), '&', '', [rfReplaceAll])
    else
      Result := StringReplace(mPresetTbl[mPresetIndex].text, '.', DecimalSeparator, [rfReplaceAll]);
    fTextType := mPresetTbl[mPresetIndex].typ;
  end
  else begin

    Result := '';
  end;
end;
//------------------------------------------------------------------------------

function TPresetValue.PreviousText: string;
begin
  if Assigned(mPresetTbl) then begin
    CircularDecrement(mPresetIndex, High(mPresetTbl));
    if mPresetTbl[mPresetIndex].typ <> ttFloat then
      Result := StringReplace(Translate(mPresetTbl[mPresetIndex].text), '&', '', [rfReplaceAll])
    else
      Result := StringReplace(mPresetTbl[mPresetIndex].text, '.', DecimalSeparator, [rfReplaceAll]);
    fTextType := mPresetTbl[mPresetIndex].typ;
  end
  else begin
    Result := '';
  end;
end;
//------------------------------------------------------------------------------

procedure TPresetValue.SetHint(const aValue: string);
begin
  if Assigned(mInterface) then
    mInterface.SetValueHint(aValue);
end;
//------------------------------------------------------------------------------

procedure TPresetValue.SetText(const aValue: string);
begin
  if Assigned(mInterface) then
    mInterface.SetValueText(aValue);
end;
//------------------------------------------------------------------------------

procedure TPresetValue.SetTextType(const aValue: TTextType);
var
  i: Integer;
begin
  if fTextType <> aValue then begin
    if aValue = ttFloat then begin
      mValue := CheckValue(mValue);
      Text := Format(mValueFormat, [mValue]);
      fTextType := ttFloat;
    end
    else begin
  // find a possibly preset text
      if Assigned(mPresetTbl) then
        for i := 0 to High(mPresetTbl) do
          if mPresetTbl[i].typ = aValue then begin
            Text := StringReplace(Translate(mPresetTbl[i].text), '&', '', [rfReplaceAll]);
            fTextType := mPresetTbl[i].typ;
          end;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TPresetValue.SetValue(const aNewValue: Double);
begin
  if (TextType <> ttEmpty) then
  begin
    if (TextType <> ttFloat) then
      TextType := ttFloat;
    Text := Format(mValueFormat, [CheckValue(aNewValue)]);
  end;
  mValue := aNewValue;
end;
//------------------------------------------------------------------------------

end.

