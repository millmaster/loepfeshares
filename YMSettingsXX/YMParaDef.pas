(*==========================================================================================SP
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMParaDef.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Supports records and definitions for YarnMaster settings
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 31.10.1999  1.00  Kr  | Initial Release
| 03.05.2000  1.10  Kr  | TOptionRec changed. minCount and maxCount replaced by yarnUnit.
| 31.10.2000  1.11  Kr  | TMachineYMConfigRec included
| 30.11.2000  1.12  Kr  | Software option added
| 18.09.2001  1.13 NueKr| TText50 from BaseGlobal to YMParaDef; TYMExtensionRec added.
| 23.01.2002  1.14  Nue | Release 2.03.01 erstellt
|=========================================================================================*)

//**************************************************
// Last change: 23.01.2002/Nue (V2.03.01)
//**************************************************

unit YMParaDef;

interface

const
//------------------------------------------------------------------------------
// Global YM depending constants
//------------------------------------------------------------------------------
  //...........................................................................

 //==== Handler Const; also used by MM ====
  cZESpdGroupLimit = 12; //Max.12 ProdGrp. per Machine
  cWSCSpdGroupLimit = 6; //Max.6 ProdGrp. per Machine

//------------------------------------------------------------------------------
// Constants for Parmeter Availability
//------------------------------------------------------------------------------
  //...........................................................................

  cSpdlRngNotDefined = $FFFF; // Spindle Range

  //==== Assignement Type ====
  cAssigGroup = 0; // 0 Assign settings and its prodGrpID to the Group
  cAssigMemory = 1; // 1 Assign settings to a memory
  cAssigProdGrpIDOnly = 2; // 2 Assign only the prodGrpID to the Group
  //...........................................................................

  //==== SoftwareOption ====
  cSWOBasic = 0; // Basic functions
  cSWOQuality = 1; // QualityPack
  cSWOIPI = 2; // Imperfections/IPI
  cSWOLabor = 3; // LabPack
  cSWOTrial = 4; // LabPack Trial

type

  TText50 = array[1..50] of Char; // Fuer Informator V5.25  (Nue:18.9.01)

  // fSwoEnd has always be the last enumator
  TFrontSwOption = (fSwOBasic, fSwOQuality, fSwOIPI, fSwOLabor, fSwOTrial, fSwOEnd);
const
  cFrontSwOption: array[fSwOBasic..fSwOEnd] of string =
    ('Basic functions', 'OualityPack', 'Imperfections/IPI', 'LabPack',
    'LabPack Trial', 'Unknown option'); //ivlm
  //...........................................................................

 //==== Sensing Head ====
  cFirstTK = 0; // Note! MillMaster is depending on this figures
  cTK730 = cFirstTK; // if anything changes the MM DB must know it!!
  cTK740 = cFirstTK + 1;
  cTK750 = cFirstTK + 2;
  cTK770 = cFirstTK + 3;
  cTK780 = cFirstTK + 4;
  cTK930F = cFirstTK + 5;
  cTK940BD = cFirstTK + 6;
  cTK930H = cFirstTK + 7;
  cTK940F = cFirstTK + 8;
  cTK930S = cFirstTK + 9;
  cLastTK = cTK930S;
  //...........................................................................

 //==== Winder Type ====
  cWTUnknownWinder = 0;
  cWTAC238 = 1;
  cWTEspero = 2;
  cWTMurIndInv = 3;
  cWTAC238Sys = 4;
  cWTAWESS = 5;
  cWTWSCCConer = 6;
  cWTAC338 = 7;
  cWTOrion = 8;

  cWTAC338Spectra = 9;
  cWTFirstSpectra = cWTAC338Spectra;
  cWTOrionSpectra = 10;
  cWTMurSpectra = 11;
  cWTEsperoSpectra = 12;
  cWTAWESSSpectra = 13;
  cWTAC238Spectra = 14;
  cWTMur21CSpectra = 15;
  cWTLastSpectra = cWTMur21CSpectra;

  cWTAC238SysSpectra = $FE;
  cWTDemoWinder = $FF;
  //...........................................................................

 //==== Speedramp ====
  cMinSpeedRamp = 1; // [s]
  cMaxSpeedRamp = 30; // [s]
  //...........................................................................

 //==== inoperativePara[0] Mask ====
  cIP0MClassClearing = $00000001; // !Basic Functios (!YM80), ClassClearing
                                        // is available
  cIP0MUpperYarnChannel = $00000008; // Upper Yarn Channel is available

  cIP0MSpeed = $00000020; // Speed setting is available
  cIP0MSpeedramp = $00000040; // Speedramp setting is available

  cIP0MMMMachineConfig = $00001000; // MillMaster MachineConfigRec is available
    // on the FrontZE (is estumated on AC338)

  cIP0MOffCountNegDiaDff = $00040000; // Plus functionality, independing -DiaDiff
                                        // Off Count Monitor is available
  cIP0MOffCountLength = $00080000; // Plus functionality, Length for Off Count Monitor
  cIP0MCutRetries = $00000080; // Plus functionality, Cut retries)

  cIP0MSFIMonitor = $02000000; // SFI Settings is available
  cIP0MSFIRepetitons = $04000000; // SFI repetitions is available
  cIP0MSFILock = $08000000; // SFI block on SFI alarm setting is available

  // Default except SS Machines
  cIP0MDefault = (cIP0MClassClearing or cIP0MUpperYarnChannel or
    cIP0MOffCountNegDiaDff or cIP0MOffCountLength or cIP0MCutRetries or
    cIP0MSFIMonitor or cIP0MSFIRepetitons or cIP0MSFILock);

  // Default for SS Machines
  cIP0MSSDefault = (cIP0MDefault or cIP0MSpeed or cIP0MSpeedramp);
  cIP0MAll = $FFFFFFFF;

 //==== inoperativePara[1] Mask ====
  cIP1MFFClearing = $00000001; // FF Clearing is available
  cIP1MFFCluster = $00000002; // FF Cluster Settings is available
  cIP1MFFSensorEnabled = $00000004; // FF Senor is active
  cIP1MFFSensingHeads = $00000F00; // FF SensingHead Bit's

  // Default
  cIP1MDefault = 0;
  // Default for FF TK
  cIP1MFFDefault = (cIP1MDefault or cIP1MFFClearing or cIP1MFFCluster or
    cIP1MFFSensorEnabled);
  cIP1MAll = $FFFFF0FF;
  //...........................................................................

 //==== voidData Mask ====

  //...........................................................................

//------------------------------------------------------------------------------
// Constants for Channel Settings
//------------------------------------------------------------------------------
  //...........................................................................

  cChOn = 2;
  cChOff = 3;
  cChExtSp = 6; // Channel Switch
  cChannelOff = $FFFF; // value for Diameter channels to switch them off

  cMinNepsDia = 150; // min Nep Diameter, [%]
  cMaxNepsDia = 700; // max Nep Diameter, [%]

  cMinShortDia = 110; // min Short Diameter, [%]
  cMaxShortDia = 400; // max Short Diameter, [%]

  cMinShortLen = 10; // min Short Length, [mm]
  cMaxShortLen = 100; // max Short Length, [mm]
  cMaxMurShortLen = 99; // max Short Length for Murata Inside only, [mm]

  cMinLongDia = 104; // min Long Diameter, [%]
  cMaxLongDia = 200; // max Long Diameter, [%]

  cMinLongLen = 60; // min Long Length, [mm]
  cMaxLongLen = 2000; // max Long Length, [mm]

  cMinThinDia = 40; // min Thin Diameter, [%]
//  cMaxThinDia = 94;                // max Thin Diameter above thin length treshold, [%]
  cMaxThinDiaAbove = 94; // max Thin Diameter above thin length treshold, [%]
  cMaxThinDiaBelow = 80; // max Thin Diameter below thin length treshold, [%]

//  cMinThinLen = 60;                     // min Thin Length, [mm]
  cMinThinLen = 20; // min Thin Length, [mm]
  cThinLenThreshold = 50; // Thin Length threshold, [mm]
  cMaxThinLen = 2000; // max Thin Length, [mm]

  cMinDia = cMinThinDia;
  cMaxDia = cMaxNepsDia;
  cMinLen = cMinShortLen;
  cMaxLen = cMaxLongLen;

 //==== Winding Speed ====
  cMinSpeed = 20; // min speed   20m/Min
  cMaxSpeed = 1600; // max speed 1600m/Min
  cDefaultSpeed = 900; // [m/Min]
 //.........................................................................

//------------------------------------------------------------------------------
// Constants for Splice Monitor
//------------------------------------------------------------------------------
  //...........................................................................

  cMinUpperYarn = cMinLongDia; // min UpperYarn Diameter, [%]
  cMaxUpperYarn = cMaxLongDia; // max UpperYarn Diameter, [%]

  cMinSpliceLen = 60; // min Splice Length, [mm]
  cMaxSpliceLen = 2000; // max Splice Length, [mm]
  cMaxMurSpliceLen = 990; // max Splice Length for Murata Inside only, [mm]

  cMinSpliceCheck = 150; // min Splice Check length [cm]
  cMaxSpliceCheck = 1200; // max Splice Check length [cm]
  cSpliceCheckOff = 0; // Seperate Splice Check Off
  cSpliceCheckNotInit = $FF;
  cDefaultSpliceCheck = 25; // Default Splice Check length 25 [cm]
  cDefaultAC338SpliceCheck = 35; // Default Splice Check length for AC338 35 [cm]
  cDefaultMurSpliceCheck = 50; // Default Splice Check length for MURATA 50[cm]
  //...........................................................................

//------------------------------------------------------------------------------
// Constants for Additional Settings
//------------------------------------------------------------------------------
  //...........................................................................
  //==== Config Code Bit Mask ====

// Config Code A Bits
  cCCAFFSensorActiveBit = Word($0001);
  cCCADFSSensitivity1Bit = Word($0002);
  cCCASFSSensitivityBit = Word($0004);
  cCCANoFFClearingOnSpliceBit = Word($0008);
  cCCAZeroTestMonitorDisabledBit = Word($0010);
  cCCANoBunchMonitorBit = Word($0020);
  cCCAOneDrumPulsBit = Word($00080);
  cCCAKnifePowerHighBit = Word($0100); // AC338 only
  cCCARemoveYarnAfterAdjustBit = Word($0200); // AC338 and Orion only
  cCCAFFBlockDisabledBit = Word($0400);
  cCCADriftCompensationOnConeChangeBit = Word($0800);

  cCCACutFailBlockDisabledBit = Word($2000);
  cCCAClusterBlockDisabledBit = Word($4000);
  cCCACountBlockDisabledBit = Word($8000);

// Config Code B Bits
  cCCBFFBDSensorActiveBit = Word($0002);
  cCCBNoFFAdjAtOfflimitBit = Word($0004);
  cCCBMurataExtendedItfBit = Word($0008);
  cCCBFFAdjAfterAlarmBit = Word($0010);
  cCCBCutBeforeAdjustBit = Word($0020);

// Config Code C Bits
  cCCBUpperYarnCheckBit = Word($0001);
  cCCBDFSSensitivity2Bit = Word($0040);
  cCCBCutOnYarnBreakBit = Word($0080);
  cCCBConeChangeDetectionDisabledBit = Word($0100);
  cCCBKnifeMonitorBit = Word($0200);
  cCCBHeadstockRightBit = Word($8000);
  cCCCSFIBlockEnabledBit = Word($0001);
  cCCCFFClusterBlockEnabledBit = Word($0002);
  //...........................................................................

 //==== Config Code Mask AC 338 ====

  cCCA338Machine: Word = $2F11; // Machine depended:     0010111100010001
  cCCA338Production: Word = $002F; // Production dependent: 0000000000101111
  cCCA338Diagnosis: Word = $1040; // Diagnosis dependent:  0001000001000000
  cCCA338Unused: Word = $C080; // Not used:             1100000010000000

  cCCB338Machine: Word = $0023; // Machine depended:     0000000000100011
  cCCB338Production: Word = $00D6; // Production dependent: 0000000011010110
  cCCB338Diagnosis: Word = $0000; // Diagnosis dependent:  0000000000000000
  cCCB338Unused: Word = $FF08; // Not used:             1111111100001000

  cCCC338Machine: Word = $0000; // Machine depended:     0000000000000000
  cCCC338Production: Word = $0000; // Production dependent: 0000000000000000
  cCCC338Diagnosis: Word = $0000; // Diagnosis dependent:  0000000000000000
  cCCC338Unused: Word = $FFFF; // Not used:             1111111111111111
  //...........................................................................

 //==== Config Code Mask All others machine ====

  cCCAMachine: Word = $EC91; // Machine depended:     1110110010010001
  cCCAProduction: Word = $002F; // Production dependent: 0000000000101111
  cCCADiagnosis: Word = $1040; // Diagnosis dependent:  0001000001000000
  cCCAUnused: Word = $0300; // Not used:             0000001100000000

  cCCBMachine: Word = $832B; // Machine depended:     1000001100101011
  cCCBProduction: Word = $00D6; // Production dependent: 0000000011010110
  cCCBDiagnosis: Word = $0000; // Diagnosis dependent:  0000000000000000
  cCCBUnused: Word = $7C00; // Not used:             0111110000000000

  cCCCMachine: Word = $0003; // Machine depended:     0000000000000011
  cCCCProduction: Word = $0000; // Production dependent: 0000000000000000
  cCCCDiagnosis: Word = $0000; // Diagnosis dependent:  0000000000000000
  cCCCUnused: Word = $FFFC; // Not used:             1111111111111100
  //...........................................................................

 //==== Config Code Mask Universal ====

  cCCAUniversalProduction: Word = $002F;
  cCCBUniversalProduction: Word = $00D6;
  cCCCUniversalProduction: Word = $0000;

  //...........................................................................

 //==== Config Code Mask Default ====
  cCCADefault = Word($001A);
  cCCBDefault = Word($0020);
  cCCCDefault = Word($0003);

  // Savio Inside Espero / Orion
  cCCASIDefault = Word($001A);
  cCCBSIDefault = Word($0020);
  cCCCSIDefault = Word($0000);

  // Murata inside
  cCCAMIDefault = Word($E41A);
  cCCBMIDefault = Word($0008);
  cCCCMIDefault = Word($0000);

  // AC338
  cCCAAC338Default = Word($201A);
  cCCBAC338Default = Word($0020);
  cCCCAC338Default = Word($0000);
  //...........................................................................

(*
 //==== Config Code Mask AC 338 ====
  cCCA338Machine = Word($0000);         // Machine depended:     0
  cCCA338Production = Word($0001);      // Production dependent: 0
  cCCA338Diagnosis = Word($0000);       // Diagnosis dependent:  0
  cCCA338Unused = Word($FFFF);          // Not used:             0

  cCCB338Machine = Word($0000);         // Machine depended:     1
  cCCB338Production = Word($0000);      // Production dependent: 0
  cCCB338Diagnosis = Word($0000);       // Diagnosis dependent:  0
  cCCB338Unused = Word($FFFF);          // Not used:             0

  cCCC338Machine = Word($0000);         // Machine depended:     1
  cCCC338Production = Word($0000);      // Production dependent: 0
  cCCC338Diagnosis = Word($0000);       // Diagnosis dependent:  0
  cCCC338Unused = Word($FFFF);          // Not used:             0

 //==== Config Code Mask All others machine ====
  cCCAMachine = Word($EC91);            // Machine depended:     1110110010010001
  cCCAProduction = Word($002F);         // Production dependent: 0000000000101111
  cCCADiagnosis = Word($1040);          // Diagnosis dependent:  0001000001000000
  cCCAUnused = Word($0300);             // Not used:             0000001100000000

  cCCBMachine = Word($832B);            // Machine depended:     1000001100101011
  cCCBProduction = Word($00D6);         // Production dependent: 0000000011010110
  cCCBDiagnosis = Word($0000);          // Diagnosis dependent:  0000000000000000
  cCCBUnused = Word($7C00);             // Not used:             0111110000000000

  cCCCMachine = Word($0000);            // Machine depended:     1
  cCCCProduction = Word($0000);         // Production dependent: 0
  cCCCDiagnosis = Word($0000);          // Diagnosis dependent:  0
  cCCCUnused = Word($FFFF);             // Not used:             0
*)

 //==== Yarn Count Monitor ====
  cMinDiaDiff = 30; // min Diameter Difference [%]
  cMaxDiaDiff = 440; // max Diameter Difference [%]

// Yarncount * 10 is stored
  cMinYarnCount = 20;
  cMaxYarnCount = 32000;
  cInitYarnCount = 1000;
  cInitFineCount = cInitYarnCount + 1;
  cInitCoarseCount = cInitYarnCount - 1;

// #define MIN_CNT_DIFF	(DIV_BY_10_RO((MIN_DIA_DIFF+100)*(MIN_DIA_DIFF+100)/10)-100)		// [%]
// #define MAX_CNT_DIFF	(DIV_BY_10_RO((MAX_DIA_DIFF+100)*(MAX_DIA_DIFF+100)/10)-100)	  // [%]

  cMaxYCCheckLength = 5; // max Yarn count check length, MAX_YC_CHECK_LENGTH*10m
  cDefaultYCCheckLength = 1;

  cMinRepetitions = 1;
  cMaxRepetitions = 6;
  cDefaultClusterRep = 1; // Default Cluster Repetition = 0 (epetitions+1: 1..6)
  cDefaultSiroStartupRep = 6; // Default Siro Startup Repetition = 5 (Repetitions+1: 1..6)
  cDefaultRepetitions = 3; // Default Repetition = 2 (Repetitions+1: 1..6)
  cDefaultCutRetries = 1; // Default Cut Retries = 0 (Repetitions+1: 1..6)

 //==== Fault Cluster Monitor ====
  cMinClusterDia = 105; // min Cluster Diameter
  cMaxClusterDia = 400; // max Cluster Diameter

 //==== Fault Cluster and  FF Cluster Monitor ====
  cClusterOffObsLength = 0; // Cluster Off (Observation Length)
  cMinObsLength = 10; // min Observation Length [cm]
  cMaxObsLength = 8000; // max Observation Length [cm]
  cDfaultObsLength = 3000; // Default Observation Length [cm]

  cMinDefects = 1; // min Faults
  cMaxDefects = 9999; // max Faults
//  cMinDefectLen = 5;                    // min Observation Length [cm]

  //==== Fine Adjust, Cone Startup ====
  cInitFadjSetting = 0;
  cDefaultFadjSetting = cInitFadjSetting;
  cMinFadjSettings = 0;
  cMaxFadjSettings = 250;
  cFadjSettingsOff = $FF; // Adjust Offlimit
 //.........................................................................

//------------------------------------------------------------------------------
// Constants for Machine set
//------------------------------------------------------------------------------
  //...........................................................................

 //==== Long Stop ====
  cMinLongStopDef = 1; // Minimal long stop definition, [1/10Min.]
  cMaxLongStopDef = 300; // Maximal long stop definition, [1/10Min.]
  cInitLongStopDef = 50; // Initial value of long stop definition, [1/10Min.]

 //==== Data Record Mode, lengthMode ====
  cDRMFirst = 6;
  cDRMLast = 7;
  cDRMCone = 8;

 //==== YM-Type declarations ====
  cYM80 = 0;
  cYM800 = cYM80 + 1;
  cYM900 = cYM80 + 2;
  cYM80I = cYM80 + 3;
  cYM800I = cYM80 + 4;
  cYM900I = cYM80 + 5;
  cYMX00 = 20;
  cYMX00I = cYMX00 + 1;
  cYM_NOT_DEFINED = cYMX00 + 2;
  cYM_UNKNOWN = cYMX00 + 3;
  //...........................................................................

//----------------------------------------------------------------------------
// Constants for SFI Monitor
//----------------------------------------------------------------------------
 //.........................................................................

  cFloatRefSFI = 0; // Float, Constant=0
  cMinSFIRef = 500; // Reference  5.0
  cMaxSFIRef = 2500; // Reference 25.0

  cSFILimitOff = 0; // +Limit, -Limit = OFF
  cMinSFIDiffUp = 50; // +Limit 5   %
  cMinSFIDiffLw = 50; // -Limit 5   %
  cMaxSFIDiffUp = 400; // +Limit 40   %
  cMaxSFIDiffLw = 400; // -Limit 40   %

  cMaxSFIRepetitions = cMaxRepetitions; //  SFI Repetitions (5)
  //...........................................................................

type
//------------------------------------------------------------------------------
// Parmeter Availability records, 30 byte
//------------------------------------------------------------------------------
  //...........................................................................
  TSpdlRangeList = (Spec, Range);

  TSpindleRangeRec = packed record
    case TSpdlRangeList of
      Spec: (
        contents: Word;
        );
      Range: (
        start: Byte; // [1..MaxSpindleOfProfGrp]
        stop: Byte; // [start..MaxSpindleOfProfGrp]
        );
  end;
  //...........................................................................

  TAvailabilityRec = packed record
    assignement: Byte; // Parameter Assignement
    restart: Byte; // AC338 Informator, internal use only!
    swOption: Byte; // r: Software Option
    group: Byte; // Local Group (1..6: AC338 Informator, 1..12 ZE)
    prodGrpID: Integer; // MillMaster's group ID, 0=not defined
    spdl: TSpindleRangeRec;
    pilotSpindles: Byte;
    sensingHead: Byte; // TK730:0,TK740:1,TK750:2,TK770:3,TK780:4,TK930:5..
    machType: Byte; // enum WINDER_TYPE
    speedRamp: Byte; // only machines with Speed Simulation, [s]
    inoperativePara: array[0..1] of Integer; // Bitset for ionoperative settings
    voidData: Integer; // Bitset not available data
    spare: Integer;
  end; // 30 byte
  //...........................................................................

//------------------------------------------------------------------------------
// Channel Settings records, 30 byte
//------------------------------------------------------------------------------
  //...........................................................................
  TDiameterRec = packed record
    dia: Word;
    sw: Word;
  end; // 4 byte

  TLengthRec = packed record
    len: Word;
    sw: Word;
  end; // 4 byte
  //...........................................................................

  TSettingsList = (Base, Splice);

  TChannelSettingsRec = packed record
    neps: TDiameterRec; // neps Diameter, (150..700/Off), [1.5..7.0]
    short: TDiameterRec; // short Diameter, (110..400/Off), [1.5..4.0]
    shortLen: Word; // short Length, (10..100), [1.0..10.0 cm]
    long: TDiameterRec; // long Diameter, (110..200/Off), [1.1..2.0]
    longLen: Word; // long Length, (60..2000), [6.0..200.0 cm]
    thin: TDiameterRec; // thin Diameter, (40..90/Off), [-10..-60 %]
    thinLen: Word; // long Length, (60..2000), [6.0..200.0 cm]
    case TSettingsList of
      Base: (
        splice: TLengthRec; // splice Length, (60..2000/LL), [6.0..200.0 cm]
        speed: Word; // winding speed, (500..1200), [500..1200 m/Min]
        spare: Word);
      Splice: (
        upperYarn: TDiameterRec; // upper yarn Diameter, (110..200/Off), [1.1..2.0]
        checkLen: Word; // splice check Length, (0..1200), [0..120 cm]
        spare_: Word);
  end; // 30 byte
  //...........................................................................

//------------------------------------------------------------------------------
// Class Clear Settings, 16 byte
//------------------------------------------------------------------------------
  //...........................................................................

  TClassClearSettingsArr = packed array[0..15] of Byte;
  //...........................................................................

//------------------------------------------------------------------------------
// FF Clear Settings, 8 byte
//------------------------------------------------------------------------------
  //...........................................................................

  TSiroClearSettingsArr = packed array[0..7] of Byte;
  //...........................................................................

  TColorClearSettingsArr = packed array[0..15] of Byte;
  //...........................................................................

//------------------------------------------------------------------------------
// Additional Settings records, 46 byte
//------------------------------------------------------------------------------
  //...........................................................................

  TOptionRec = packed record
    diaDiff: Word; // Diameter difference of Yarn Count, [OFF,+-MIN_DIA_DIFF*10..MAX_DIA_DIFF*10 ,1/1000]
    yarnCnt: Word; // Yarn count*10 [MIN_YARN_COUNT..MAX_YARNCOUNT]
    yarnUnit: Word; // Yarn Unit TYarnUnit
    threadCnt: Word; // Number of threads
    cntRep: Byte; // DiaDiff repetitions, (Repetitions+1: 1..6)
    siroStartupRep: Byte; // Startup repetition, (Repetitions+1: 1..6)
    clusterDia: Word; // Cluster Diameter, (110..400/Off), [1.1..4.0]
    clusterLength: Word; // Observation length, clusterLength=0: Off  [MIN_OBS_LENGTH..MAX_OBS_LENGTH mm]
    clusterDefects: Word; // Defects per observation length
  end; // 16 byte
  //...........................................................................

  TExtendedOptionRec = packed record
    negDiaDiff: Word; // Diameter difference of Yarn Count, [OFF,-MIN_DIA_DIFF*10..MAX_DIA_DIFF*10 ,1/1000] */
    countLength: Byte; // Yarn Count Count length, [0..5] 0,1:10m, 2:20m, 3:30m, 4:40m, 5:50m */
    clusterRep: Byte; // Cluster Repetitions, (Repetitions+1: 1..6)
    cutRetries: Byte; // Cut Retries, (Repetitions+1: 1..6)
    fill: Byte;
    spare: array[0..1] of Integer;
  end; // 14 byte
  //...........................................................................

  TAdditionalRec = packed record
    option: TOptionRec; // 16 byte
    configA: Word;
    configB: Word;
    configC: Word;
    fAdjConeReduction: Byte; // reduction douring fineAdjust and Cone change
    adjRequest: Byte; // Adjust Steuerung START_AD.., STOP_AD, NO_REQ.. */
    spare: array[0..1] of Integer;
    extOption: TExtendedOptionRec; // 14 byte
  end; // 46 byte
  //...........................................................................

//------------------------------------------------------------------------------
// Machine Set record, 34 byte
//------------------------------------------------------------------------------
  //...........................................................................

const
  cMaxMachNameStrSz = 10;
  cMaxVersiomStrSz = 8;
  //...........................................................................

type
  TMachBezArr = array[0..cMaxMachNameStrSz - 1] of Byte;
  //...........................................................................

  TYMSWVersionArr = array[0..cMaxVersiomStrSz - 1] of Byte;
  //...........................................................................

  TMachineSetRec = packed record
    machBez: TMachBezArr; // Machine name
    longStopDef: Word; // Long Stop definition, [0,1s]
    lengthWindow: Word; // Window Length, [km]
    lengthMode: Byte; // Lengt Mode, [cDRMFirst, cDRMLast, cDRMCone]
    yMType: Byte; // yMType ....
    spare: array[0..1] of Integer;
    reinVer: Byte; // AWE-Software Version
    reinTech: Byte; // AWE-Technischer Stand
    yMSWVersion: TYMSWVersionArr; // ZE Version string
  end; // 34 byte
  //...........................................................................

//------------------------------------------------------------------------------
// SFI (Surface Index) Settings record, 8 byte
//------------------------------------------------------------------------------
  //...........................................................................

  TSFIParaRec = packed record
    absRef: Word; // Reference: constant x100	[5.0..25.0] (500..2500)
                                        // Reference: float [0]
    upper: Word; // +Limi x10	[5..40]	(50, 400,off)[%]
    lower: Word; // -Limi x10	[5..40]	(50, 400,off)[%]
    rep: Byte; //  SFI Repetitions, (Repetitions+1: 1..6)
    fill0: Byte;
  end; // 8 byte
  //...........................................................................

//------------------------------------------------------------------------------
// FF Cluster Settings record, 14 byte
//------------------------------------------------------------------------------
  //...........................................................................

  TFFClusterRec = packed record
    obsLength: Word;
    defects: Word;
    rep: Byte;
    fill0: Byte;
    siroClear: TSiroClearSettingsArr; //  8 byte
  end; // 14 byte
  //...........................................................................

//------------------------------------------------------------------------------
// Spare Set record, 24 bytes
//------------------------------------------------------------------------------
  //...........................................................................

  TSpareSetRec = packed record
    fill: array[0..5] of Integer;
  end; // 24 byte
  //...........................................................................

//------------------------------------------------------------------------------
// YM Settings (telegramsubID: SID_SETTINGS), 240 byte
//------------------------------------------------------------------------------
  //...........................................................................

  TYMSettingsRec = packed record
    available: TAvailabilityRec; // 30 byte
    channel: TChannelSettingsRec; // 30 byte
    splice: TChannelSettingsRec; // 30 byte
    classClear: TClassClearSettingsArr; // 16 byte
    siroClear: TSiroClearSettingsArr; //  8 byte
    additional: TAdditionalRec; // 46 byte
    machSet: TMachineSetRec; // 34 byte
    sFI: TSFIParaRec; //  8 byte
    fFCluster: TFFClusterRec; // 14 byte
    spare: TSpareSetRec; // 24 byte
  end; // 240 byte
  //...........................................................................

//------------------------------------------------------------------------------
// YM Settings and extension fields
//------------------------------------------------------------------------------
  //...........................................................................

  TYMExtensionRec = packed record
    YM_Set_Name: TText50;
    YMSettings: TYMSettingsRec; // 240 byte
  end;
  //...........................................................................

  //------------------------------------------------------------------------------
// General definitions and default initialisation
//------------------------------------------------------------------------------
  //...........................................................................

const
{(*}
  cTopSixSettings: TChannelSettingsRec = (
    neps: (dia: 400; sw: cChOn);
    short: (dia: 220; sw: cChOn);
    shortLen: 13;
    long: (dia: 120; sw: cChOn);
    longLen: 100;
    thin: (dia: 90; sw: cChOn);
    thinLen: 450;
    splice: (len: 200; sw: cChOn);
    checkLen: 250;
    spare: 0
  );
{*)}
type
//------------------------------------------------------------------------------
// Byte buffer for YM Settings
//------------------------------------------------------------------------------
  //...........................................................................

  TYMSettingsByteArr = array[1..400] of Byte; // Has to be bigger then size of
                                    // YMParaDef.TYMSettingsRec (actual 240B)
  PYMSettingsByteArr = ^TYMSettingsByteArr;
  //...........................................................................

//------------------------------------------------------------------------------
// Pointer to YM Settings
//------------------------------------------------------------------------------
  //...........................................................................

  PYMSettings = ^TYMSettingsRec;
  //...........................................................................

//------------------------------------------------------------------------------
// Data rules for not available data or data needs to process
//------------------------------------------------------------------------------
  //...........................................................................

  TYMDataRulesRec = record
    voidData: Integer;
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Void Data Items
//------------------------------------------------------------------------------
  //............................................................................
  TVoidDataItem = (vdCones, // Availability of Cones
    vdCops, // Availability of Cops
    vdIPOld, // Availability of IPOld
    vdIPNew, // dito
    vdIPPlus, // dito
    vdSFIVar, // dito
    vdSFICnt); // dito
  //............................................................................

//------------------------------------------------------------------------------
// Machine dependent parameter
// Old machine config record
//------------------------------------------------------------------------------
  //...........................................................................

  TMachineYMParaRec = record
    sensingHead: Byte; // TK730:0,TK740:1,TK750:2,TK770:3,TK780:4,TK930:5..
    machType: Byte; // enum WINDER_TYPE
    checkLen: Word; // splice check Length, (0..1200), [0..120 cm]
    cutRetries: Byte; // Cut Retries, (Repetitions+1: 1..6)
    configA: Word;
    configB: Word;
    configC: Word;
    inoperativePara: array[0..1] of Integer; // Bitset for ionoperative settings
                                              // only valid on ExtractMachineYMPara
    machBez: TMachBezArr; // Machine name
    longStopDef: Word; // Long Stop definition, [s]
    yMType: Byte; // yMType ....
    yMSWVersion: TYMSWVersionArr; // ZE Version string
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Machine dependent parameter
// New machine config record
//------------------------------------------------------------------------------
  //...........................................................................

  TAWEMachType = (amtUnknown, amtAC238, amtESPERO, amtMUR_IND_INV, amtAC238_SYS,
    amtAWE_SS, amtWSC_C_CONER, amtAC338, amtOrion, amtMurata21C, amtDemoWinder);
  //...........................................................................

  TFrontType = (ftUnknown, ftZE80, ftZE80i, ftZE800, ftZE800i, ftSWSInformatorSBC5);
  //...........................................................................

  TAWEType = (atUnknown, atAWE800, atAWESpectra);
  //...........................................................................
  (* Note: When adding new sensing heads don't forget to check out the const:
  cFirstTK up to cLastTK and , the set: cOtherSensingHeadTypes, cFFSensingHeadFTypes
  and cFFSensingHeadTypes, the method: TYMSettingsUtils.ExtractMachineYMConfig.ExtractSensingHead
  and TYMMachineConfig.SetBuildMaxMachineConfig.
   *)
  TSensingHeadType = (htTK830, htTK840, htTK850, htTK870, htTK880, htTK930F, htTK940BD,
    htTK930H, htTK940F, htTK930S);

  //...........................................................................
  PTSensingHeadClass = ^TSensingHeadClass;
  TSensingHeadClass = (hcTK8xx, hcTK9xx, hcTK9xxH, hcTK9xxBD);
  //...........................................................................

const
  //...........................................................................

  cFirstSensingHead = Ord(htTK830);
  //...........................................................................

  cLastSensingHead = Ord(htTK930S);
  //...........................................................................

  cFirstFFSensingHead = Ord(htTK930F);
  //...........................................................................

  cOtherSensingHeadTypes: set of TSensingHeadType =
    [htTK830, htTK840, htTK850, htTK870, htTK880];
  //...........................................................................

  cFFSensingHeadTypes: set of TSensingHeadType =
    [htTK930F, htTK940BD, htTK930H, htTK940F, htTK930S];
  //...........................................................................

  cFFSensingHeadFTypes: set of TSensingHeadType =
    [htTK930F, htTK940F, htTK930S];
  //...........................................................................

  cFFSensingHeadBDTypes: set of TSensingHeadType =
    [htTK940BD];
  //...........................................................................

  cAWEMachTypNames: array[amtUnknown..amtDemoWinder] of string =
    ('Unknown winder', 'AC238', 'ESPERO', 'MUR_IND_INV', 'AC238_SYS', 'AWE_SS',
     'WSC_C_CONER', 'AC338', 'Orion', 'Muarata 21C', 'DemoWinder'); //ivlm

  //...........................................................................

  //  NOTE: cWTAC238SysSpectra = $FE and cWTDemoWinder = $FF has to have a special handling !!

  cAWEMachTypXRefTbl: array[cWTUnknownWinder..cWTMur21CSpectra] of TAWEMachType =
    (amtUnknown, amtAC238, amtESPERO, amtMUR_IND_INV, amtAC238_SYS, amtAWE_SS,
    amtWSC_C_CONER, amtAC338, amtOrion, amtAC338, amtOrion, amtMUR_IND_INV,
    amtESPERO, amtAWE_SS, amtAC238, amtMurata21C);
  //...........................................................................

  cFrontTypeNames: array[ftUnknown..ftSWSInformatorSBC5] of string =
    ('Unknown front type', 'ZE80', 'ZE80inside', 'ZE800', 'ZE800inside',
    'SWS Informator'); //ivlm
  //...........................................................................

  cSensingHeadTypeNames: array[htTK830..htTK930S] of string =
    ('TK830', 'TK840', 'TK850', 'TK870', 'TK880', 'TK930F', 'TK940BD',
    'TK930H', 'TK940F', 'TK930S'); //ivlm
  //...........................................................................

  cSensingHeadTypeOrder: array[htTK830..htTK930S] of Integer =
    (6, 7, 8, 9, 10, 2, 5, 3, 4, 1);
  //...........................................................................

  cSensingHeadClassNames: array[hcTK8xx..hcTK9xxBD] of string =
    ('TK8xx', 'TK9xxF/S', 'TK9xxH', 'TK9xxBD'); //ivlm
  //...........................................................................

  cAWETypeNames: array[atUnknown..atAWESpectra] of string =
    ('Unknown AWE type', 'AWE800', 'AWESpectra'); //ivlm
  //...........................................................................

type
  TGroupSpecificMachConfigRec = record
    spindle: TSpindleRangeRec; // r/w: Spindlerange where the machine spec are valide,
                                        // cSpdlRngNotDefined: if no group defined
    aWEType: TAWEType; // r: enum atAWE800, atAWESpectra
    sensingHead: TSensingHeadType; // r/w: htTK830...
    configA: Word; // r/w: Config Code A
    configB: Word; // r/w: Config Code B
    configC: Word; // r/w: Config Code C
    inoperativePara: array[0..1] of Integer; // r: Bitset for ionoperative settings, (machine sections)
  end;
  //...........................................................................

  TMachineYMConfigRec = record
    spec: array[1..cZESpdGroupLimit] of TGroupSpecificMachConfigRec;
    checkLen: Word; // r/w: splice check Length, (0..1200), [0..120 cm]
    cutRetries: Byte; // r/w: Cut Retries, (Repetitions+1: 1..6)
                                        // only valid on ExtractMachineYMPara
    longStopDef: Word; // r/w: Long Stop definition, [s]
    defaultSpeedRamp: Byte; // r/w: default only machines with Speed Simulation, [s]
    defaultSpeed: Word; // r/w: default winding speed, (500..1200), [500..1200 m/Min]
    frontType: TFrontType; // r: ftZE80..
    frontSWOption: TFrontSwOption; // r: fSwOBasic..
    frontSWVersion: TYMSWVersionArr; // r: ZE Version string
    aWEMachType: TAWEMachType; // r: enum TAWEMachType
    machBez: TMachBezArr; // w: Machine name
    inoperativePara: array[0..1] of Integer; // r: Bitset for ionoperative settings, (machine)
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Production group dependent parameter
//------------------------------------------------------------------------------
  //...........................................................................

  TProdGrpYMParaRec = record
    group: Byte; // Local Group (0..5: AC338 Informator, 0..11 ZE)
    prodGrpID: Integer; // MillMaster's group ID, 0=not defined
    spdl: TSpindleRangeRec;
    pilotSpindles: Byte;
    speedRamp: Byte; // only machines with Speed Simulation
    speed: Word; // winding speed, (20..1600), [20..1600 m/Min]
    yarnCnt: Word; // Yarn count [cMinYarnCount..cMinYarnCount]
    yarnCntUnit: Byte; // Yarn count unit only valid on PutProdGrpYMPara
    nrOfThreads: Byte; // threadCnt
    // to define from Nue
    lengthWindow: Word;
    lengthMode: Byte;
  end;
  //...........................................................................

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.

