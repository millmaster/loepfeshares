(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMParaEditBox.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: EditBoxes for YarnMaster Basic Settings.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.01.2000  0.00  Kr  | Initial Release
| 17.01.2002  0.00  Wss | ShowMode = Extended enabled to show ReadOnly mode
|=============================================================================*)
unit YMBasicProdParaBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmGroupBox, Spin, mmLabel, ExtCtrls, mmBevel, ComCtrls,
  mmHeaderControl, mmEdit, mmPanel, mmCheckBox, Buttons, mmBitBtn, MMUGlobal,
  PresetValue, mmSpinEdit, mmStaticText, YMParaUtils, YMParaDef, KeySpinEdit,
  DialogConfigCode, mmLineLabel, mmComboBox;

//------------------------------------------------------------------------------
// Basic Production definitions
//------------------------------------------------------------------------------
  //...........................................................................

resourcestring
  //...........................................................................

  rsSensingHeadClass = '(28)Tastkopf-Klasse'; //ivlm
  rsSFSSensitifityHint = '(*)SFS Empfindlichkeitsfaktor:'; //ivlm
  rsDFSSensitifityHint = '(*)DFS Empfindlichkeitsfaktor:'; //ivlm
  rsReductionHint = '(*)Reduktion der Numerabweichungs- und Fehlerschwarm-Empfindlichkeit waehrend den ersten 12km des Feinabgleichs:'; //ivlm
  rsSwOff = '(3)&Aus'; //ivlm
  rsYarnCountRepHint = '(*)Nummerabweichungsschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsFaultClusterRepHint = '(*)Fehlerschwarmschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsSFIRepHint = '(*)SFI Schnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsFFClusterRepHint = '(*)FF Schwarmschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsFFStartupRepHint = '(*)FF Anlaufschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  //...........................................................................

const
  //...........................................................................

 //==== Propery index for production group specific settings ====
  cPGYarnCount = 0;
  cPGYarnUnit = 1;
  cPGPilotSpindle = 2;
  cPGSpeedRamp = 3;
  cPGSpeed = 4;

 //==== Propery index for machine specific config code ====
  cMConfigA = 1;
  cMConfigB = 2;
  cMConfigC = 3;

 //==== Propery index for enabeling entry fields ====
  cBSSSWinder = 0;
  cBSFFDetection = 1;
  cBSCBFFDetection = 2;
  cBSFFAlarm = 3;
  //...........................................................................

  cVSBSSFSSensitifity: TValueSpecRec = (
    minValue: 1.0;
    maxValue: 2.0;
    format: '%1.0f';
    caption: '';
    measure: '';
    hint: rsSFSSensitifityHint;
    );

  cPTBSSFSSensitifity: array[0..1] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ));
  //...........................................................................

  cVSBSDFSSensitifity: TValueSpecRec = (
    minValue: 1.0;
    maxValue: 4.0;
    format: '%1.0f';
    caption: '';
    measure: '';
    hint: rsDFSSensitifityHint;
    );

  cPTBSDFSSensitifity: array[0..2] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '4'; ));
  //...........................................................................

  cVSBSReduction: TValueSpecRec = (
    minValue: cMinFadjSettings;
    maxValue: cMaxFadjSettings;
    format: '%3.0f';
    caption: '';
    measure: '';
    hint: rsReductionHint;
    );

  cPTBSReduction: array[0..6] of TPresetTextRec = (
    (typ: ttFloat; text: '0'; ),
    (typ: ttFloat; text: '25'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttFloat; text: '75'; ),
    (typ: ttFloat; text: '100'; ),
    (typ: ttFloat; text: '150'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));
  //...........................................................................

  cVSBSYarnCountRep: TValueSpecRec = (
    minValue: cMinRepetitions - 1;
    maxValue: cMaxRepetitions - 1;
    format: '%1.0f';
    caption: '';
    measure: '';
    hint: rsYarnCountRepHint;
    );

  cVSBSFaultClusterRep: TValueSpecRec = (
    minValue: cMinRepetitions - 1;
    maxValue: cMaxRepetitions - 1;
    format: '%1.0f';
    caption: '';
    measure: '';
    hint: rsFaultClusterRepHint;
    );

  cVSBSSFIRep: TValueSpecRec = (
    minValue: cMinRepetitions - 1;
    maxValue: cMaxRepetitions - 1;
    format: '%1.0f';
    caption: '';
    measure: '';
    hint: rsSFIRepHint;
    );


  cVSBSFFClusterRep: TValueSpecRec = (
    minValue: cMinRepetitions - 1;
    maxValue: cMaxRepetitions - 1;
    format: '%1.0f';
    caption: '';
    measure: '';
    hint: rsFFClusterRepHint;
    );

  cVSBSFFStartupRep: TValueSpecRec = (
    minValue: cMinRepetitions - 1;
    maxValue: cMaxRepetitions - 1;
    format: '%1.0f';
    caption: '';
    measure: '';
    hint: rsFFStartupRepHint;
    );

  cPTBSRepetition: array[0..4] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '3'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '5'; ));
  //...........................................................................

type
 //...........................................................................

  TYMSettinsGUIMode = (gmTemplate, gmAssignment, gmTransparent);
 //...........................................................................

  TOnLoadYMSettings = procedure of object;
 //...........................................................................

  TBasicProdSettingsRec = record
    cntRep: Byte;                       // DiaDiff repetitions, (Repetitions+1: 1..6)
    clusterRep: Byte;                   // Cluster Repetitions, (Repetitions+1: 1..6)
    siroStartupRep: Byte;               // Startup repetition, (Repetitions+1: 1..6)
    sFIRep: Byte;                       //  SFI Repetitions, (Repetitions+1: 1..6)
    FFClusterRep: Byte;                 //  FF Cluster Repetitions, (Repetitions+1: 1..6)
    fAdjConeReduction: Byte;
    checkLen: Word;                     // splice check Length, (0..1200), [0..120 cm]
    configA: Word;
    configB: Word;
    configC: Word;
  end;
 //...........................................................................

  TGBBasicProdPara = class(TFrame)
    mBasicPanel: TmmPanel;
    mbvlGeneral: TmmBevel;
    mbvlYarnSpec: TmmBevel;
    mbvlRepetition: TmmBevel;
    mbvlTopSpace: TmmBevel;
    mlaYarnSpec: TmmLabel;
    mlbPilotLabel: TmmLabel;
    mlbDFSSensitivity: TmmLabel;
    mlbReduction: TmmLabel;
    mlbFFStartupRep: TmmLabel;
    mlbYarnCountRep: TmmLabel;
    mlbFaultClusterRep: TmmLabel;
    mlbSFIRep: TmmLabel;
    mlbFFClusterRep: TmmLabel;
    mlbRepetition: TmmLabel;
    mlbSpeedLabel: TmmLabel;
    mlbSpeedRampLabel: TmmLabel;
    mlbPilotCount: TmmLabel;
    mlbTimes2: TmmLabel;
    mlbTimes1: TmmLabel;
    mcbFFDetection: TmmCheckBox;
    mcbFFAdjustOnOfflimit: TmmCheckBox;
    mcbFFAdjustAfterOfflimitAlarm: TmmCheckBox;
    mcbSeperatSpliceSettings: TmmCheckBox;
    mcbFFClearingOnSplice: TmmCheckBox;
    mcbBunchMonitor: TmmCheckBox;
    mcbAdditionalCutOnBreak: TmmCheckBox;
    mbDefault: TmmBitBtn;
    medSFSSensitivity: TKeySpinEdit;
    medDFSSensitivity: TKeySpinEdit;
    medReduction: TKeySpinEdit;
    mlbSFSSensitivity: TmmLabel;
    mlbSpeedRamp: TmmLabel;
    mlbSpeed: TmmLabel;
    medYarnCountRep: TKeySpinEdit;
    medFaultClusterRep: TKeySpinEdit;
    medSFIRep: TKeySpinEdit;
    medFFClusterRep: TKeySpinEdit;
    medFFStartupRep: TKeySpinEdit;
    mlbGeneral: TmmLabel;
    mlbSpeedUnit: TmmLabel;
    mlbStartupUnit: TmmLabel;
    mlbReductionUnit: TmmLabel;
    mbvlCfgCode: TmmBevel;
    mlbCfgCode: TmmStaticText;
    mlbCfgCodeALabel: TmmLabel;
    mlbCfgCodeA: TGBConfigCode;
    mlbCfgCodeBLabel: TmmLabel;
    mlbCfgCodeC: TGBConfigCode;
    mlbCfgCodeCLabel: TmmLabel;
    mlbCfgCodeB: TGBConfigCode;
    mcobSensingHeadClass: TmmComboBox;
    mlbSensingHeadClass: TmmLabel;
    procedure DoClickConfigCode(aSender: TObject);
    procedure DoChangeConfigCode(aSender: TObject);
    procedure DoChange(aSender: TObject);
//    procedure DoChangeConfig(Sender: TObject);
    procedure DoConfirm(aTag: Integer);
    procedure DoConfirmConfig(aTag: Integer);
    procedure mbDefaultClick(Sender: TObject);
    procedure mcobSensingHeadClassChange(Sender: TObject);
//    procedure FFDetectionClick(Sender: TObject);
//    procedure FFAdjustOnOfflimitClick(Sender: TObject);
  private
    { Private declarations }
    fOnConfirm: TOnConfirm;
    fOnLoadYMSettings: TOnLoadYMSettings;
    fRangeSpecificConfig: TGroupSpecificMachConfigRec;
    fReadOnly: Boolean;
    fYMSettinsGUIMode: TYMSettinsGUIMode;

    mDoChangedEnabled: Boolean;
    mMachineConfigCode: TConfigurationCode;
    mMachineYMConfig: TYMMachineConfig;
    mProductionConfigCode: TConfigurationCode;
    mRange: TSpindleRangeRec;

    function CobGetSelectedSensingHeadClass: TSensingHeadClass;
    function GetBuildMaxMachineConfig: Boolean;
    function GetEnable(const aIndex: Integer): Boolean;
    function GetRangeSpecificConfig: TGroupSpecificMachConfigRec;
    function EqualSensingHeadClassInRange(const aFirstSpindle, aLastSpindle: Byte;
      const aUseSensigHeadClassArgument: Boolean;
      const aSensingHeadClass: TSensingHeadClass): Boolean; overload;

    procedure CobFillSensingHeadClass;
    procedure CobSynchronizeSensingHeadClass(const aValue: TSensingHeadType);
    procedure DisplayConfigCode;
    procedure LoadTemplateMachineConfig;
    procedure PutEnable(const aIndex: Integer; const aValue: Boolean);
//    procedure PutMachineConfig(const aIndex: Integer; const aValue: Word);
    procedure PutMachineConfig(const aValue: TMachineYMConfigRec);
    procedure PutPara(var aPara: TBasicProdSettingsRec);
    procedure PutProdGrpYMPara(const aIndex, aValue: Integer);
    procedure SetCollectMaxMachineConfig(const aValue: TMachineYMConfigRec);
    procedure SetBuildMaxMachineConfig(const aValue: Boolean);
    procedure SetReadOnly(const aValue: Boolean);
    procedure SetSpindleRange(const aIndex: Integer; const aValue: Byte);
    procedure SetYMSettinsGUIMode(const aValue: TYMSettinsGUIMode);

    procedure UpdateConfigCode;
    procedure UpdateMachineConfigCode;

    property EnableCBFFDetection: Boolean index cBSCBFFDetection read GetEnable write PutEnable;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

    function EqualSensingHeadClassInRange(const aFirstSpindle,
      aLastSpindle: Byte): Boolean; overload;
    function EqualSensingHeadClassInRange(const aFirstSpindle,
      aLastSpindle: Byte; const aSensingHeadClass: TSensingHeadClass): Boolean; overload;

    procedure GetPara(var aPara: TBasicProdSettingsRec);
    procedure PutYMSettings(var aSettings: TYMSettingsRec);
    procedure SetMaxSensingHeadClass;
    procedure SetSensingHeadDependency;
    // Insert Max SensingHeadClass to the already loaded YMSettings

    property BuildMaxMachineConfig: Boolean read GetBuildMaxMachineConfig write SetBuildMaxMachineConfig;
    property CollectMaxMachineConfig: TMachineYMConfigRec write SetCollectMaxMachineConfig;

    property EnableFFAlarm: Boolean index cBSFFAlarm read GetEnable write PutEnable;
    property EnableFFDetection: Boolean index cBSFFDetection read GetEnable write PutEnable;
    property EnableSSWinder: Boolean index cBSSSWinder read GetEnable write PutEnable;

    property FromSpindle: Byte index cSRFrom write SetSpindleRange;

    property MachineConfig: TMachineYMConfigRec write PutMachineConfig;
    property Mode: TYMSettinsGUIMode read fYMSettinsGUIMode write SetYMSettinsGUIMode;

    property OnConfirm: TOnConfirm read fOnConfirm write fOnConfirm;
    property OnLoadYMSettings: TOnLoadYMSettings read fOnLoadYMSettings write fOnLoadYMSettings;

    property ProdGrpPilot: Integer index cPGPilotSpindle write PutProdGrpYMPara;
      // ProdGrpPilot := number of pilot spindles, [1..MaxSpindleOfProfGrp, default: 4];
    property ProdGrpSpeed: Integer index cPGSpeed write PutProdGrpYMPara;
      // ProdGrpSpeed := Winding Speed (SS Winder only), [20m/Min..1600m/Min, default: 900m/Min];
    property ProdGrpSpeedRamp: Integer index cPGSpeedRamp write PutProdGrpYMPara;
      // ProdGrpSpeedRamp := Speed Ramp (SS Winder only), [1s..30s];

    property RangeSpecificConfig: TGroupSpecificMachConfigRec read GetRangeSpecificConfig;
    property ReadOnly: Boolean read fReadOnly write SetReadOnly;

    property ToSpindle: Byte index cSRTo write SetSpindleRange;

  end;
 //...........................................................................

//procedure Register;
 //...........................................................................

const
  //...........................................................................

  cDefaultProdSettings: TBasicProdSettingsRec = (
    cntRep: cDefaultRepetitions;
    clusterRep: cDefaultClusterRep;
    siroStartupRep: cDefaultSiroStartupRep;
    sFIRep: cDefaultRepetitions;
    FFClusterRep: cDefaultRepetitions;
    fAdjConeReduction: cDefaultFadjSetting;
    checkLen: cDefaultYCCheckLength;
    configA: cCCADefault;
    configB: cCCBDefault;
    configC: cCCCDefault;
    );

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{$R *.DFM}
//******************************************************************************
(*
procedure Register;
begin
  RegisterComponents('YMClearer', [TGBBasicProdPara]);
end;
*)

//******************************************************************************
// TGBBasicProdPara
//******************************************************************************
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.CobFillSensingHeadClass;
var
  i: Integer;
begin

  mcobSensingHeadClass.Items.Clear;
  with mMachineYMConfig do
    for i := 0 to SensingHeadClassList.Count - 1 do
    begin
      mcobSensingHeadClass.Items.Append(cSensingHeadClassNames[SensingHeadClassList.Items[i]]);
    end;
  CobSynchronizeSensingHeadClass(mMachineYMConfig.SensingHead);

end;
//------------------------------------------------------------------------------

function TGBBasicProdPara.CobGetSelectedSensingHeadClass: TSensingHeadClass;
var
  i: TSensingHeadClass;
  xFound: Boolean;
begin
  Result := hcTK8xx;
  with mcobSensingHeadClass do
  begin

    i := Low(cSensingHeadClassNames);
    xFound := False;
    while (xFound = False) and (i <= High(cSensingHeadClassNames)) do
    begin
      if CompareStr(Items.Strings[ItemIndex], cSensingHeadClassNames[i]) = 0 then
      begin
        Result := i;
        xFound := True;
      end;

      Inc(i);
    end;

  end;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.CobSynchronizeSensingHeadClass(const aValue: TSensingHeadType);
var
  i: Integer;
begin

  with mcobSensingHeadClass do
    for i := 0 to Items.Count - 1 do

      if CompareStr(Items.Strings[i],
        cSensingHeadClassNames[mMachineYMConfig.ConvertSensingHeadTypeToClass(aValue)])
      = 0 then
        ItemIndex := i;

end;
//------------------------------------------------------------------------------

constructor TGBBasicProdPara.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fOnConfirm := nil;
  fOnLoadYMSettings := nil;

  mDoChangedEnabled := True;
  mRange.contents := cSpdlRngNotDefined;
  mMachineYMConfig := TYMMachineConfig.Create;
  mProductionConfigCode := TConfigurationCode.Create;
  mMachineConfigCode := TConfigurationCode.Create;

  medSFSSensitivity.LoadValueSpec(cVSBSSFSSensitifity, cPTBSSFSSensitifity);
  medSFSSensitivity.Value := 1;
  medSFSSensitivity.OnConfirm := DoConfirmConfig;

  medDFSSensitivity.LoadValueSpec(cVSBSDFSSensitifity, cPTBSDFSSensitifity);
  medDFSSensitivity.Value := 2;
  medDFSSensitivity.OnConfirm := DoConfirmConfig;

  medReduction.LoadValueSpec(cVSBSReduction, cPTBSReduction);
  medReduction.Value := 0;
  medReduction.TextType := ttFloat;
  medReduction.OnConfirm := DoConfirm;

  medYarnCountRep.LoadValueSpec(cVSBSYarnCountRep, cPTBSRepetition);
  medFaultClusterRep.LoadValueSpec(cVSBSFaultClusterRep, cPTBSRepetition);
  medSFIRep.LoadValueSpec(cVSBSSFIRep, cPTBSRepetition);
  medFFClusterRep.LoadValueSpec(cVSBSFFClusterRep, cPTBSRepetition);
  medFFStartupRep.LoadValueSpec(cVSBSFFStartupRep, cPTBSRepetition);

  mlbCfgCodeA.DialogCaption := 'ConfigCodeA';
  mlbCfgCodeB.DialogCaption := 'ConfigCodeB';
  mlbCfgCodeC.DialogCaption := 'ConfigCodeC';

  EnableSSWinder := False;
  EnableFFDetection := True;

  ReadOnly := False;
  Mode := gmTransparent;
  fRangeSpecificConfig := mMachineYMConfig.GroupSpecificConfig;

  // TComboBox.mcobSensingHeadClass
  mlbSensingHeadClass.Caption := rsSensingHeadClass;
  mcobSensingHeadClass.Items.Clear;
//  mcobSensingHeadClass.Items.Add(cSensingHeadClassNames[mMachineYMConfig.SensingHeadClass]);
  mcobSensingHeadClass.ItemIndex := 0;

  UpdateConfigCode;
end;
//------------------------------------------------------------------------------

destructor TGBBasicProdPara.Destroy;
begin

  mMachineYMConfig.Free;
  mProductionConfigCode.Free;
  mMachineConfigCode.Free;

  inherited;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.DisplayConfigCode;
begin

  with mProductionConfigCode do
  begin
    mlbCfgCodeA.ConfigCode := (mMachineConfigCode.MachA and not ProdPatternA) or ProdA;
    mlbCfgCodeB.ConfigCode := (mMachineConfigCode.MachB and not ProdPatternB) or ProdB;
    mlbCfgCodeC.ConfigCode := (mMachineConfigCode.MachC and not ProdPatternC) or ProdC;
  end;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.DoChange(aSender: TObject);
begin

  if mDoChangedEnabled then
    DoConfirm(0);
end;
//------------------------------------------------------------------------------
(*
procedure TGBBasicProdPara.DoChangeConfig(Sender: TObject);
begin

  if not ReadOnly then
    DoConfirmConfig(0);

end;
*)
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.DoChangeConfigCode(aSender: TObject);
var
  xCfg: TConfigurationCode;

begin
  if not ReadOnly and mDoChangedEnabled then
  begin
    if aSender is TGBConfigCode then
    begin
      xCfg := TConfigurationCode.Create;
      xCfg.FrontType := mMachineYMConfig.FrontType;
      xCfg.AWEMachType := mMachineYMConfig.AWEMachType;
      xCfg.SensingHead := mMachineConfigCode.SensingHead;

      case TGBConfigCode(aSender).Tag of
        0:
          begin
(* %%
            xCfg.A := mlbCfgCodeA.ConfigCode;

            xCfg.Filter := cfMachine;
            mMachineConfigCode.A := xCfg.A;

            xCfg.Filter := cfProduction;
            mMachineConfigCode.A := mMachineConfigCode.A and not xCfg.A;

            xCfg.Filter := cfNone;
            mProductionConfigCode.A := xCfg.A and not mMachineConfigCode.A;
*)
            xCfg.A := mlbCfgCodeA.ConfigCode;
            mProductionConfigCode.A := xCfg.ProdA and not mMachineConfigCode.MachA;
          end;

        1:
          begin
(* %%
            xCfg.B := mlbCfgCodeB.ConfigCode;
            xCfg.Filter := cfMachine;
            mMachineConfigCode.B := xCfg.B;

            xCfg.Filter := cfProduction;
            mMachineConfigCode.B := mMachineConfigCode.B and not xCfg.B;

            xCfg.Filter := cfNone;
            mProductionConfigCode.B := xCfg.B and not mMachineConfigCode.B;
*)
            xCfg.B := mlbCfgCodeB.ConfigCode;
            mProductionConfigCode.B := xCfg.ProdB and not mMachineConfigCode.MachB;
          end;

        2:
          begin
(* %%
            xCfg.C := mlbCfgCodeC.ConfigCode;

            xCfg.Filter := cfMachine;
            mMachineConfigCode.C := xCfg.C;

            xCfg.Filter := cfProduction;
            mMachineConfigCode.C := mMachineConfigCode.C and not xCfg.C;

            xCfg.Filter := cfNone;
            mProductionConfigCode.C := xCfg.C and not mMachineConfigCode.C;
*)
            xCfg.C := mlbCfgCodeC.ConfigCode;
            mProductionConfigCode.C := xCfg.ProdC and not mMachineConfigCode.MachC;
          end;
      end;

      xCfg.Free;
    end;

    UpdateConfigCode;

    DoConfirm(0);
  end;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.DoClickConfigCode(aSender: TObject);
begin

  if mDoChangedEnabled then
  begin
    if aSender is TmmCheckBox then

      with mProductionConfigCode do
      begin

        case TmmCheckBox(aSender).Tag of

          1:
            with mcbFFDetection do
              FFDetection := Checked;

          2:
            with mcbFFAdjustOnOfflimit do
              FFAdjOnOfflimit := Checked;

          3:
            with mcbFFAdjustAfterOfflimitAlarm do
              FFAdjAfterAlarm := Checked;

          4:
            with mcbFFClearingOnSplice do
              FFClearingOnSplice := Checked;

          5:
            with mcbBunchMonitor do
              BunchMonitor := Checked;
          6:
            with mcbAdditionalCutOnBreak do
              CutOnYarnBreak := Checked;
        end;
      end;

    UpdateConfigCode;

    DoConfirm(0);
  end;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.DoConfirm(aTag: Integer);
begin

  if not ReadOnly and mDoChangedEnabled and Assigned(fOnConfirm) then
    fOnConfirm(aTag);
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.DoConfirmConfig(aTag: Integer);
begin

  if mDoChangedEnabled then
  begin
    with mProductionConfigCode do
    begin

      case aTag of

        0:
          with medSFSSensitivity do
            SFSSensitivity := Round(Value);

        1:
          with medDFSSensitivity do
            DFSSensitivity := Round(Value);
      end;
    end;

    UpdateConfigCode;

    DoConfirm(aTag);
  end;
end;
//------------------------------------------------------------------------------
(*
procedure TGBBasicProdPara.FFAdjustOnOfflimitClick(Sender: TObject);
begin

  EnableFFAlarm := not mcbFFAdjustOnOfflimit.Checked;

  DoConfirmConfig(0);
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.FFDetectionClick(Sender: TObject);
begin

  EnableFFDetection := mcbFFDetection.Checked;

  DoConfirmConfig(0);
end;
*)
//------------------------------------------------------------------------------

function TGBBasicProdPara.EqualSensingHeadClassInRange(const aFirstSpindle,
  aLastSpindle: Byte): Boolean;
begin

  Result := EqualSensingHeadClassInRange(aFirstSpindle, aLastSpindle,
    False, hcTK8xx);
end;
//------------------------------------------------------------------------------

function TGBBasicProdPara.EqualSensingHeadClassInRange(const aFirstSpindle,
  aLastSpindle: Byte; const aSensingHeadClass: TSensingHeadClass): Boolean;
begin

  Result := EqualSensingHeadClassInRange(aFirstSpindle, aLastSpindle,
    True, aSensingHeadClass);
end;
//------------------------------------------------------------------------------

function TGBBasicProdPara.EqualSensingHeadClassInRange(const aFirstSpindle,
  aLastSpindle: Byte; const aUseSensigHeadClassArgument: Boolean;
  const aSensingHeadClass: TSensingHeadClass): Boolean;
var
  xRange: TSpindleRangeRec;
  xGroupList: TGroupListArr;
  i: Integer;
  xSensingHeadClass: TSensingHeadClass;
begin

  if Mode = gmAssignment then
  begin
    xRange.start := aFirstSpindle;
    xRange.stop := aLastSpindle;
    xGroupList := mMachineYMConfig.GetIntersectedGroups(xRange);
    if not aUseSensigHeadClassArgument and
      (xGroupList[Low(xGroupList)] <> cNoGroup) then
      xSensingHeadClass := TYMMachineConfig.ConvertSensingHeadTypeToClass(
        mMachineYMConfig.MachineConfig.spec[xGroupList[Low(xGroupList)]].sensingHead)
    else
      xSensingHeadClass := aSensingHeadClass;

    Result := True;
    i := Low(xGroupList);

    while (i <= High(xGroupList)) and Result do
    begin
      if xGroupList[i] <> cNoGroup then
      begin
        if (TYMMachineConfig.ConvertSensingHeadTypeToClass(
          mMachineYMConfig.MachineConfig.spec[xGroupList[i]].sensingHead) <>
          hcTK8xx) and
          (TYMMachineConfig.ConvertSensingHeadTypeToClass(
          mMachineYMConfig.MachineConfig.spec[xGroupList[i]].sensingHead) <>
          xSensingHeadClass) and (xSensingHeadClass <> hcTK8xx) then
          Result := False;

      end
      else if i = Low(xGroupList) then
        Result := False;

      Inc(i);
    end;

  end
  else
    Result := False;

end;
//------------------------------------------------------------------------------

function TGBBasicProdPara.GetBuildMaxMachineConfig: Boolean;
begin

  Result := mMachineYMConfig.BuildMaxMachineConfig;
end;
//------------------------------------------------------------------------------

function TGBBasicProdPara.GetEnable(const aIndex: Integer): Boolean;
begin
  Result := False;

  case aIndex of
    cBSSSWinder:
      begin
        Result := mlbSpeed.Enabled;
      end;

    cBSFFDetection:
      begin
        Result := mcbFFAdjustOnOfflimit.Enabled;
      end;
    cBSFFAlarm:
      Result := mcbFFAdjustAfterOfflimitAlarm.Enabled;
  end;

end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.GetPara(var aPara: TBasicProdSettingsRec);
begin

  aPara.cntRep := Round(medYarnCountRep.Value) + 1;
  aPara.clusterRep := Round(medFaultClusterRep.Value) + 1;
  aPara.siroStartupRep := Round(medFFStartupRep.Value) + 1;
  aPara.sFIRep := Round(medSFIRep.Value) + 1;
  aPara.FFClusterRep := Round(medFFClusterRep.Value) + 1;

  if medReduction.TextType = ttCmdDisabled then
    aPara.fAdjConeReduction := cFadjSettingsOff
  else
    aPara.fAdjConeReduction := Round(medReduction.Value);

  if mcbSeperatSpliceSettings.Checked then
    aPara.checkLen := 1
  else
    aPara.checkLen := 0;

//  aPara.speed := Round(medSpeed.Value);
//  aPara.speedRamp := Round(medSpeedRamp.Value);

  with mProductionConfigCode do
  begin
    aPara.configA := ProdA or (mMachineConfigCode.MachA and not ProdPatternA);
    aPara.configB := ProdB or (mMachineConfigCode.MachB and not ProdPatternB);
    aPara.configC := ProdC or (mMachineConfigCode.MachC and not ProdPatternC);
  end;
end;
//------------------------------------------------------------------------------

function TGBBasicProdPara.GetRangeSpecificConfig: TGroupSpecificMachConfigRec;
var
  xCfg: TConfigurationCode;

begin

  xCfg := TConfigurationCode.Create;

  with fRangeSpecificConfig, xCfg do
  begin

    FrontType := mMachineYMConfig.FrontType;
    AWEMachType := mMachineYMConfig.AWEMachType;
    SensingHead := fRangeSpecificConfig.sensingHead;
    A := configA;
    B := configB;
    C := configC;

    configA := (MachA and not ProdPatternA) or mProductionConfigCode.ProdA;
    configB := (MachB and not ProdPatternB) or mProductionConfigCode.ProdB;
    configC := (MachC and not ProdPatternC) or mProductionConfigCode.ProdC;
  end;

  xCfg.Free;
  Result := fRangeSpecificConfig;

end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.LoadTemplateMachineConfig;
var
  xCfg: TConfigurationCode;
  xMachineConfig: TMachineYMConfigRec;

begin

  xCfg := TConfigurationCode.Create;
  xCfg.SensingHead := mMachineYMConfig.MaxMachineConfig.spec[1].sensingHead;
  xCfg.A := mMachineYMConfig.MaxMachineConfig.spec[1].configA;
  xCfg.B := mMachineYMConfig.MaxMachineConfig.spec[1].configB;
  xCfg.C := mMachineYMConfig.MaxMachineConfig.spec[1].configC;

  xMachineConfig := mMachineYMConfig.MaxMachineConfig;

  xMachineConfig.spec[1].configA := xCfg.MachA and xCfg.ProdPatternA;
  xMachineConfig.spec[1].configB := xCfg.MachB and xCfg.ProdPatternB;
  xMachineConfig.spec[1].configC := xCfg.MachC and xCfg.ProdPatternC;

  mMachineYMConfig.MachineConfig := xMachineConfig;

  xCfg.Free;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.mbDefaultClick(Sender: TObject);
var
  xPara: TBasicProdSettingsRec;
  xPFilter, xMFilter: TCfgFilter;
begin

  if mDoChangedEnabled then
  begin

    xPara := cDefaultProdSettings;

    with xPara, mProductionConfigCode do
    begin
      xPFilter := Filter;
      Filter := cfProduction;
      A := DefaultA;
      configA := A;
      B := DefaultB;
      configB := B;
      C := DefaultC;
      configC := C;
      Filter := xPFilter;
    end;

    with mMachineConfigCode do
    begin
      xMFilter := Filter;
      Filter := cfMachine;
(*
    A := DefaultA and not mProductionConfigCode.FilterPatternA;
    B := DefaultB and not mProductionConfigCode.FilterPatternB;
    C := DefaultC and not mProductionConfigCode.FilterPatternC;
*)
      A := DefaultA;
      B := DefaultB;
      C := DefaultC;

      Filter := xMFilter;
    end;

    PutPara(xPara);

    DoConfirm(0);
  end;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.mcobSensingHeadClassChange(Sender: TObject);
var
  xSensingHeadClass: TSensingHeadClass;
begin
  if mDoChangedEnabled then
    with mMachineYMConfig do
    begin
      xSensingHeadClass := CobGetSelectedSensingHeadClass;
      if xSensingHeadClass <> ConvertSensingHeadTypeToClass(SensingHead) then
      begin
        SensingHeadClass := xSensingHeadClass;
        UpdateMachineConfigCode;
        DoConfirm(0);
      end;
    end;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.PutEnable(const aIndex: Integer;
  const aValue: Boolean);
begin

  case aIndex of
    cBSSSWinder:
      begin
//      mlbSpeedLabel.Enabled := aValue;
        mlbSpeed.Enabled := aValue;
//      mlbSpeedRampLabel.Enabled := aValue;
        mlbSpeedRamp.Enabled := aValue;
      end;

    cBScbFFDetection:
      begin
        mcbFFAdjustOnOfflimit.Enabled := aValue;
        EnableFFAlarm := aValue;
        mcbFFClearingOnSplice.Enabled := aValue;
      end;

    cBSFFDetection:
      begin
        EnableCBFFDetection := aValue;
        medFFStartupRep.Enabled := aValue;
        medFFClusterRep.Enabled := aValue;
      end;

    cBSFFAlarm:
      if mcbFFAdjustOnOfflimit.Enabled then
        mcbFFAdjustAfterOfflimitAlarm.Enabled := aValue
      else
        mcbFFAdjustAfterOfflimitAlarm.Enabled := False;
  end;
end;
//------------------------------------------------------------------------------
(*
procedure TGBBasicProdPara.PutMachineConfig(const aIndex: Integer;
  const aValue: Word);
begin
  case aIndex of

    cMConfigA:
      mMConfigA := aValue and not cCCAProduction;

    cMConfigB:
      mMConfigB := aValue and not cCCBProduction;

    cMConfigC:
      mMConfigC := aValue and not cCCCProduction;
  end;

  DisplayConfigCode;
end;
*)
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.PutMachineConfig(
  const aValue: TMachineYMConfigRec);
begin

  if Mode <> gmTemplate then
  begin
    mMachineYMConfig.MachineConfig := aValue;

    UpdateMachineConfigCode;
  end;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.PutPara(var aPara: TBasicProdSettingsRec);
var
  xCfg: TConfigurationCode;
begin

  mDoChangedEnabled := False;

  with aPara do
  begin
    medYarnCountRep.Value := cntRep - 1;
    medFaultClusterRep.Value := clusterRep - 1;
    medFFStartupRep.Value := siroStartupRep - 1;
    medSFIRep.Value := sFIRep - 1;
    medFFClusterRep.Value := FFClusterRep - 1;

    if fAdjConeReduction = cFadjSettingsOff then
      medReduction.TextType := ttCmdDisabled
    else
    begin
      medReduction.Value := fAdjConeReduction;
      medReduction.TextType := ttFloat;
//      medReduction.Value := fAdjConeReduction;
    end;

    if checkLen = 0 then
      mcbSeperatSpliceSettings.Checked := False
    else
      mcbSeperatSpliceSettings.Checked := True;

    xCfg := TConfigurationCode.Create;

    xCfg.FrontType := mMachineYMConfig.FrontType;
    xCfg.AWEMachType := mProductionConfigCode.AWEMachType;
    xCfg.SensingHead := mProductionConfigCode.SensingHead;

    with mProductionConfigCode do
    begin
(* %%
      xFilter := Filter;
      FrontType := mMachineYMConfig.FrontType;
      AWEMachType := mMachineYMConfig.AWEMachType;
      SensingHead := mMachineConfigCode.SensingHead;

      Filter := cfProduction;
      xCfg.Filter := cfMachine;

      mProductionConfigCode.A := aPara.configA and
        (not xCfg.FilterPatternA or (xCfg.FilterPatternA and mProductionConfigCode.FilterPatternA));
      B := aPara.configB and
        (not xCfg.FilterPatternB or (xCfg.FilterPatternB and FilterPatternB));
      C := aPara.configC and
        (not xCfg.FilterPatternC or (xCfg.FilterPatternC and FilterPatternC));

      Filter := xFilter;
*)

      xCfg.A := aPara.configA;
      xCfg.B := aPara.configB;
      xCfg.C := aPara.configC;

      xCfg.SensingHead := mProductionConfigCode.SensingHead;

      A := xCfg.ProdA;
      B := xCfg.ProdB;
      C := xCfg.ProdC;

    end;

    xCfg.Free;

    if Mode <> gmTransparent then
      UpdateMachineConfigCode
    else
    begin
      CobSynchronizeSensingHeadClass(mProductionConfigCode.SensingHead);

      UpdateConfigCode;
    end
  end;

  mDoChangedEnabled := True;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.PutProdGrpYMPara(const aIndex, aValue: Integer);
begin

  case aIndex of

    cPGPilotSpindle:
      if aValue <> 0 then
      begin
        mlbPilotLabel.Enabled := True;
        mlbPilotCount.Enabled := True;
        mlbPilotCount.Caption := Format('%d', [aValue]);
      end
      else
      begin
        mlbPilotLabel.Enabled := False;
        mlbPilotCount.Enabled := False;
        mlbPilotCount.Caption := '';
      end;

    cPGSpeedRamp:
      if aValue <> 0 then
        mlbSpeedRamp.Caption := Format('%d', [aValue])
      else
      begin
        mlbSpeedRamp.Caption := '';
        EnableSSWinder := False;
      end;

    cPGSpeed:
      if aValue <> 0 then
        mlbSpeed.Caption := Format('%d', [aValue])
      else
      begin
        mlbSpeed.Caption := '';
        EnableSSWinder := False;
      end;
  end;

end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.PutYMSettings(var aSettings: TYMSettingsRec);
var
  xPara: TBasicProdSettingsRec;
  xMachineAttributes: TMachineAttributes;
begin

  mDoChangedEnabled := False;

  xMachineAttributes := TMachineAttributes.Create(aSettings);
  EnableSSWinder := xMachineAttributes.IsSpeedSimulation;

  xPara.cntRep := aSettings.additional.option.cntRep;
  xPara.clusterRep := aSettings.additional.extOption.clusterRep;

  xPara.siroStartupRep := aSettings.additional.option.siroStartupRep;
  xPara.sFIRep := aSettings.sFI.rep;
  xPara.FFClusterRep := aSettings.fFCluster.rep;

  xPara.fAdjConeReduction := aSettings.additional.fAdjConeReduction;

  xPara.checkLen := aSettings.splice.checkLen;

  if Mode = gmTransparent then
  begin
    mMachineYMConfig.SensingHead := TYMMachineSettings.ExtractSensingHead(aSettings);
    mMachineConfigCode.SensingHead := mMachineYMConfig.SensingHead;
    mProductionConfigCode.SensingHead := mMachineYMConfig.SensingHead;

    mMachineConfigCode.AWEMachType := TYMMachineSettings.ExtractAWEMachineType(aSettings);
    mProductionConfigCode.AWEMachType := mMachineYMConfig.AWEMachType;

    with aSettings.additional do
    begin
      mMachineConfigCode.A := configA;
      mMachineConfigCode.B := configB;
      mMachineConfigCode.C := configC;
    end;
  end
  else if Mode = gmTemplate then
  begin

    mMachineYMConfig.SensingHead := xMachineAttributes.SensingHead;

    UpdateMachineConfigCode;
  end;
// ConfigCode

  with aSettings.additional do
  begin
    xPara.configA := configA;           // and cCCAProduction;
    xPara.configB := configB;           // and cCCBProduction;
    xPara.configC := configC;           // and cCCCProduction;
  end;

  PutPara(xPara);

  if Assigned(fOnLoadYMSettings) then
    OnLoadYMSettings;

  xMachineAttributes.Free;

  mDoChangedEnabled := True;

end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.SetBuildMaxMachineConfig(const aValue: Boolean);
begin

  mMachineYMConfig.BuildMaxMachineConfig := aValue;

  if aValue then
  begin
    CobFillSensingHeadClass;

    if Mode = gmTemplate then
      LoadTemplateMachineConfig;
  end;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.SetCollectMaxMachineConfig(
  const aValue: TMachineYMConfigRec);
begin

  mMachineYMConfig.CollectMaxMachineConfig := aValue;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.SetMaxSensingHeadClass;
begin
  if Mode <> gmTransparent then
  begin

    with mMachineYMConfig do
    begin

      if SensingHeadClassList.Count > 0 then

        SensingHeadClass := SensingHeadClassList.Items[0];
//        ConvertSensingHeadTypeToClass(MaxMachineConfig.spec[1].sensingHead);

      mMachineConfigCode.SensingHead := SensingHead;
      mMachineConfigCode.SetSensingHeadDependency;
      mProductionConfigCode.SensingHead := SensingHead;
      mProductionConfigCode.SetSensingHeadDependency;
    end;

    UpdateMachineConfigCode;

    if Assigned(fOnLoadYMSettings) then
      OnLoadYMSettings;

  end;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.SetReadOnly(const aValue: Boolean);
begin
  fReadOnly := aValue;

  medYarnCountRep.ReadOnly := aValue;
  medFaultClusterRep.ReadOnly := aValue;
  medFFStartupRep.ReadOnly := aValue;
  medSFIRep.ReadOnly := aValue;
  medFFClusterRep.ReadOnly := aValue;
  medReduction.ReadOnly := aValue;
  mcbSeperatSpliceSettings.Enabled := not aValue;

  mcobSensingHeadClass.ReadOnly := aValue;
  if Mode = gmTemplate then
    mcobSensingHeadClass.Enabled := not aValue;

  mbDefault.Enabled := not aValue;

  UpdateConfigCode;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.SetSensingHeadDependency;
begin

  mMachineConfigCode.SetSensingHeadDependency;
  mProductionConfigCode.ValidateSensingHeadDependency;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.SetSpindleRange(const aIndex: Integer;
  const aValue: Byte);
begin

  case aIndex of

    cSRFrom:
      if (aValue >= 1) and (aValue <= cMaxSpindeln) then
      begin

        mRange.start := aValue;
        if mRange.start > mRange.stop then
          mRange.stop := mRange.start;
      end
      else
        mRange.contents := cSpdlRngNotDefined;

    cSRTo:
      if (aValue >= 1) and (aValue <= cMaxSpindeln) then
      begin

        mRange.stop := aValue;
        if mRange.start > mRange.stop then
          mRange.start := mRange.stop;
      end
      else
        mRange.contents := cSpdlRngNotDefined;

  end;

  UpdateMachineConfigCode;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.SetYMSettinsGUIMode(
  const aValue: TYMSettinsGUIMode);
begin
  fYMSettinsGUIMode := aValue;

  case fYMSettinsGUIMode of

    gmTemplate:
      begin
        mcobSensingHeadClass.Enabled := True;

        if mMachineYMConfig.BuildMaxMachineConfig then
          LoadTemplateMachineConfig;
      end;

    gmAssignment:
      begin
        mcobSensingHeadClass.Enabled := False;
      end;

    gmTransparent:
      begin
        mcobSensingHeadClass.Enabled := False;
      end;
  end;

  UpdateMachineConfigCode;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.UpdateConfigCode;
begin
  with mProductionConfigCode do
  begin

    with mcbFFDetection do
    begin
      Checked := FFDetection;
      Enabled := not ReadOnly and FFDetectionAvailable;
    end;

    with mcbFFAdjustOnOfflimit do
    begin
      Checked := FFAdjOnOfflimit;
      Enabled := not ReadOnly and FFAdjOnOfflimitAvailable;
    end;

    with mcbFFAdjustAfterOfflimitAlarm do
    begin
      Checked := FFAdjAfterAlarm;
      Enabled := not ReadOnly and FFAdjAfterAlarmAvailable;
    end;

    with mcbFFClearingOnSplice do
    begin
      Checked := FFClearingOnSplice;
      Enabled := not ReadOnly and FFClearingOnSpliceAvailable;
    end;

    with mcbBunchMonitor do
    begin
      Checked := BunchMonitor;
      Enabled := not ReadOnly;
    end;

    with mcbAdditionalCutOnBreak do
    begin
      Checked := CutOnYarnBreak;
      Enabled := not ReadOnly;
    end;

    with medSFSSensitivity do
    begin
      Value := Round(SFSSensitivity);
      ReadOnly := self.ReadOnly;
    end;

    with medDFSSensitivity do
    begin
      Value := Round(DFSSensitivity);
      ReadOnly := self.ReadOnly;
    end;
  end;

  DisplayConfigCode;
end;
//------------------------------------------------------------------------------

procedure TGBBasicProdPara.UpdateMachineConfigCode;
//var
//  xMachineFilterPattern: Word;
begin
  if Mode <> gmTransparent then
  begin
    fRangeSpecificConfig := mMachineYMConfig.CastGroupSpecificConfig(mRange);
(* %%
  xFilter := mMachineConfigCode.Filter;

  mMachineConfigCode.FrontType := mMachineYMConfig.FrontType;
  mMachineConfigCode.AWEMachType := mMachineYMConfig.AWEMachType;
  mMachineConfigCode.SensingHead := fRangeSpecificConfig.SensingHead;

  mMachineConfigCode.Filter := cfMachine;
  xMachineFilterPattern := mMachineConfigCode.FilterPatternA;
  mMachineConfigCode.Filter := cfProduction;

  mMachineConfigCode.A := fRangeSpecificConfig.configA and
    ((not mMachineConfigCode.FilterPatternA) or xMachineFilterPattern);

  mMachineConfigCode.Filter := cfMachine;
  xMachineFilterPattern := mMachineConfigCode.FilterPatternB;
  mMachineConfigCode.Filter := cfProduction;

  mMachineConfigCode.B := fRangeSpecificConfig.configB and
    ((not mMachineConfigCode.FilterPatternB) or xMachineFilterPattern);

  mMachineConfigCode.Filter := cfMachine;
  xMachineFilterPattern := mMachineConfigCode.FilterPatternC;
  mMachineConfigCode.Filter := cfProduction;

  mMachineConfigCode.C := fRangeSpecificConfig.configC and
    ((not mMachineConfigCode.FilterPatternC) or xMachineFilterPattern);

  mMachineConfigCode.Filter := xFilter;
*)

    mMachineConfigCode.FrontType := mMachineYMConfig.FrontType;
    mMachineConfigCode.AWEMachType := mMachineYMConfig.AWEMachType;
    mMachineConfigCode.SensingHead := fRangeSpecificConfig.SensingHead;

    mMachineConfigCode.A := fRangeSpecificConfig.configA;
    mMachineConfigCode.B := fRangeSpecificConfig.configB;
    mMachineConfigCode.C := fRangeSpecificConfig.configC;

    mProductionConfigCode.FrontType := mMachineYMConfig.FrontType;
    mProductionConfigCode.AWEMachType := mMachineYMConfig.AWEMachType;
    mProductionConfigCode.SensingHead := mMachineConfigCode.SensingHead;
// %%
//    mMachineConfigCode.SetSensingHeadDependency;
//    mProductionConfigCode.SetSensingHeadDependency;

    CobSynchronizeSensingHeadClass(mMachineConfigCode.SensingHead);
    UpdateConfigCode;
  end;
end;
//------------------------------------------------------------------------------

end.

