(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMParaDBAccess.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: Clearer Settings
| Process(es)...: -
| Description...: DB access for Clearer Settings
| Info..........: -
| Develop.system: Compaq Deskpro Pentium Pro 200, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 10.01.01 | 1.00 | Kr | Datei erstellt. TYMSettingsForGUI implementiert.
| 24.01.01 | 1.00 | SDo| fDBAccess := True; & fQuery := NIL; in
|          |      |    | TYMSettingsForGUI.create -> sonst keine DBDaten [ LoadFromDB() ]
| 18.09.01 | 1.01|NueKr| Handling of YM_set_name added.
| 20.09.02         Nue | Umbau ADO
|=============================================================================*)

unit YMParaDBAccessForGUI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmQuery, YMParaDBAccess, YMParaEditBox, QualityMatrix, QualityMatrixDef,
  YMBasicProdParaBox, YMParaUtils, YMParaDef, adoDBAccess;

type
  TmmQuery = TNativeAdoQuery;
//..............................................................................

  TOnChange = procedure(aOwner: TComponent) of object;
//..............................................................................

  TYMSettings = class(TObject)
  private
    fChannelEditBox: TChannelEditBox;
    fClassMatrixEditBox: TQualityMatrix;
    fFaultClusterEditBox: TFaultClusterEditBox;
    fFFMatrixEditBox: TQualityMatrix;
    fGBBasicProdPara: TGBBasicProdPara;
    fSpliceEditBox: TSpliceEditBox;
    fYarnCountEditBox: TYarnCountEditBox;
    fSFIEditBox: TSFIEditBox;
    fFFClusterEditBox: TFFClusterEditBox;

    mOriginalSettings: TYMSettingsRec;
    mSettingsDBAssistant: TSettingsDBAssistant;
    mYMSettingsUtils: TYMSettingsUtils;
    mMachineAttributes: TMachineAttributes;
// %%    mMachineYMPara:       TMachineYMParaRec;

    procedure PutChannelEditBox(const aValue: TChannelEditBox);
    procedure PutClassMatrixEditBox(const aValue: TQualityMatrix);
    procedure PutFaultClusterEditBox(const aValue: TFaultClusterEditBox);
    procedure PutFFMatrixEditBox(const aValue: TQualityMatrix);
    procedure PutGBBasicProdPara(const Value: TGBBasicProdPara);
    procedure PutSpliceEditBox(const aValue: TSpliceEditBox);
    procedure PutYarnCountEditBox(const aValue: TYarnCountEditBox);
    procedure PutSFIEditBox(const aValue: TSFIEditBox);
    procedure PutFFClusterEditBox(const aValue: TFFClusterEditBox);
    function GetChanged: Boolean; overload;
    function GetChangedProperty: Boolean;
    function GetInopSettings0: DWord;
    function GetInopSettings1: DWord;
    procedure SetInopSettings0(const aValue: DWord);
    procedure SetInopSettings1(const aValue: DWord);

  public
    constructor Create; overload;
    constructor Create(aQuery: TmmQuery); overload;
    constructor CreateStandAlone;
    destructor Destroy; override;

    function GetChanged(var aSettings: TYMSettingsRec): Boolean; overload;
    function NewOnDB: Integer; overload;
{Mg} function NewOnDB(aName: string): integer; overload;
{Mg} function NewTemplate(aName: string): integer;

    procedure Delete(aSetID: Integer);
    procedure EstablishDBAccess; overload;
    procedure EstablishDBAccess(aQuery: TmmQuery); overload;
    procedure GetFromScreen(var aSettings: TYMSettingsRec); overload;
    procedure GetFromScreen(var aSettings: TYMSettingsByteArr; var aSize: Word); overload;
    procedure InsertScreenToSettings(var aSettings: TYMSettingsRec); overload;
    procedure InsertScreenToSettings(var aSettings: TYMSettingsByteArr; var aSize: Word); overload;
    procedure IntroduceOriginalSettings;
    procedure LoadFromDB(aSetID: Integer; var aYMSetName: TText50);
    procedure PutDefaultToScreen;
    procedure PutToScreenPrivat(var aSettings: TYMSettingsRec);
    procedure PutToScreen(var aSettings: TYMSettingsRec); overload;
    procedure PutToScreen(var aSettings: TYMSettingsByteArr); overload;
    procedure UpdateOnDB(aSetID: Integer);

    property Changed: Boolean read GetChangedProperty;
    property ChannelEditBox: TChannelEditBox read fChannelEditBox write PutChannelEditBox;
    property ClassMatrixEditBox: TQualityMatrix read fClassMatrixEditBox write PutClassMatrixEditBox;
    property FaultClusterEditBox: TFaultClusterEditBox read fFaultClusterEditBox write PutFaultClusterEditBox;
    property FFClusterEditBox: TFFClusterEditBox read fFFClusterEditBox write PutFFClusterEditBox;
    property FFMatrixEditBox: TQualityMatrix read fFFMatrixEditBox write PutFFMatrixEditBox;
    property GBBasicProdPara: TGBBasicProdPara read fGBBasicProdPara write PutGBBasicProdPara;

    property SFIEditBox: TSFIEditBox read fSFIEditBox write PutSFIEditBox;
    property SpliceEditBox: TSpliceEditBox read fSpliceEditBox write PutSpliceEditBox;
    property YarnCountEditBox: TYarnCountEditBox read fYarnCountEditBox write PutYarnCountEditBox;

    property InopSettings0: DWord read GetInopSettings0 write SetInopSettings0;
    property InopSettings1: DWord read GetInopSettings1 write SetInopSettings1;
  end;
//..............................................................................

  TYMSettingsForGUI = class(TComponent)
  private
    { Private declarations }
    fDBAccess: Boolean;
    fQuery: TmmQuery;
    fOnChange: TOnChange;

    mYMSettings: TYMSettings;

    procedure DoChanged(aTag: Integer);
    procedure DoConfirm(aTag: Integer);
    procedure DoParameterChange(aSource: TParameterSource);
    procedure PutChannelEditBox(const aValue: TChannelEditBox);
    procedure PutClassMatrixEditBox(const aValue: TQualityMatrix);
    procedure PutFaultClusterEditBox(const aValue: TFaultClusterEditBox);
    procedure PutFFClusterEditBox(const aValue: TFFClusterEditBox);
    procedure PutFFMatrixEditBox(const aValue: TQualityMatrix);
    procedure PutGBBasicProdPara(const aValue: TGBBasicProdPara);
    procedure PutSFIEditBox(const aValue: TSFIEditBox);
    procedure PutSpliceEditBox(const aValue: TSpliceEditBox);
    procedure PutYarnCountEditBox(const aValue: TYarnCountEditBox);
    function GetChannelEditBox: TChannelEditBox;
    function GetClassMatrixEditBox: TQualityMatrix;
    function GetFaultClusterEditBox: TFaultClusterEditBox;
    function GetFFClusterEditBox: TFFClusterEditBox;
    function GetFFMatrixEditBox: TQualityMatrix;
    function GetGBBasicProdPara: TGBBasicProdPara;
    function GetSFIEditBox: TSFIEditBox;
    function GetSpliceEditBox: TSpliceEditBox;
    function GetYarnCountEditBox: TYarnCountEditBox;

  protected
    { Protected declarations }
    procedure Loaded; override;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

    function NewOnDB: Integer; overload;
    function NewOnDB(aName: string): integer; overload;
    function NewTemplate(aName: string): integer;
    procedure Delete(aSetID: Integer);

    function GetFromScreen(var aSettings: TYMSettingsRec): Boolean; overload;
    function GetFromScreen(var aSettings: TYMSettingsByteArr; var aSize: Word): Boolean; overload;
    procedure InsertScreenToSettings(var aSettings: TYMSettingsRec); overload;
    procedure InsertScreenToSettings(var aSettings: TYMSettingsByteArr; var aSize: Word); overload;
    procedure LoadFromDB(aSetID: Integer; var aYMSetName: TText50);
    procedure PutDefaultToScreen;
    procedure PutToScreen(var aSettings: TYMSettingsRec); overload;
    procedure PutToScreen(var aSettings: TYMSettingsByteArr); overload;
    procedure UpdateOnDB(aSetID: Integer);

  published
    { Published declarations }

    property ChannelEditBox: TChannelEditBox read GetChannelEditBox write PutChannelEditBox;
    property ClassMatrixEditBox: TQualityMatrix read GetClassMatrixEditBox write PutClassMatrixEditBox;

    property DBAccess: Boolean read fDBAccess write fDBAccess default True;

    property FaultClusterEditBox: TFaultClusterEditBox read GetFaultClusterEditBox write PutFaultClusterEditBox;
    property FFClusterEditBox: TFFClusterEditBox read GetFFClusterEditBox write PutFFClusterEditBox;
    property FFMatrixEditBox: TQualityMatrix read GetFFMatrixEditBox write PutFFMatrixEditBox;
    property GBBasicProdPara: TGBBasicProdPara read GetGBBasicProdPara write PutGBBasicProdPara;

    property OnChange: TOnChange read fOnChange write fOnChange default nil;
      // Notifies the user if settings on Screen has changed

    property Query: TmmQuery read fQuery write fQuery default nil;

    property SFIEditBox: TSFIEditBox read GetSFIEditBox write PutSFIEditBox;
    property SpliceEditBox: TSpliceEditBox read GetSpliceEditBox write PutSpliceEditBox;
    property YarnCountEditBox: TYarnCountEditBox read GetYarnCountEditBox write PutYarnCountEditBox;
  end;

// procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

(*
procedure Register;
begin
  RegisterComponents('YMClearer', [TYMSettingsForGUI]);
end;
*)
//******************************************************************************
// TYMSettings
//******************************************************************************
//------------------------------------------------------------------------------

constructor TYMSettings.Create;
begin
  CreateStandAlone;
  EstablishDBAccess;
end;
//------------------------------------------------------------------------------

constructor TYMSettings.Create(aQuery: TmmQuery);
begin
  CreateStandAlone;
  EstablishDBAccess(aQuery);
end;
//------------------------------------------------------------------------------

constructor TYMSettings.CreateStandAlone;
begin
  inherited Create;
  mSettingsDBAssistant := nil;
  mYMSettingsUtils := TYMSettingsUtils.Create;

  mMachineAttributes := TMachineAttributes.Create;
  FillChar(mOriginalSettings, SizeOf(mOriginalSettings), 0);

  fChannelEditBox := nil;
  fClassMatrixEditBox := nil;
  fFaultClusterEditBox := nil;
  fFFClusterEditBox := nil;
  fFFMatrixEditBox := nil;
  fSFIEditBox := nil;
  fSpliceEditBox := nil;
  fYarnCountEditBox := nil;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.Delete(aSetID: integer);
begin
  mSettingsDBAssistant.Delete(aSetID);
end;
//------------------------------------------------------------------------------

destructor TYMSettings.Destroy;
begin
  FreeAndNil(mSettingsDBAssistant);
  FreeAndNil(mYMSettingsUtils);
  FreeAndNil(mMachineAttributes);

  fChannelEditBox := nil;
  fClassMatrixEditBox := nil;
  fFaultClusterEditBox := nil;
  fFFClusterEditBox := nil;
  fFFMatrixEditBox := nil;
  fSFIEditBox := nil;
  fSpliceEditBox := nil;
  fYarnCountEditBox := nil;

  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.EstablishDBAccess;
begin

  if not Assigned(mSettingsDBAssistant) then
    mSettingsDBAssistant := TSettingsDBAssistant.Create;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.EstablishDBAccess(aQuery: TmmQuery);
begin

  if not Assigned(mSettingsDBAssistant) then
    mSettingsDBAssistant := TSettingsDBAssistant.Create(aQuery);
end;
//------------------------------------------------------------------------------

function TYMSettings.GetChanged: Boolean;
var
  xSettings: TYMSettingsRec;
begin
  GetFromScreen(xSettings);

  Result := GetChanged(xSettings);
end;
//------------------------------------------------------------------------------

function TYMSettings.GetChangedProperty: Boolean;
begin

  Result := GetChanged;
end;
//------------------------------------------------------------------------------

function TYMSettings.GetChanged(var aSettings: TYMSettingsRec): Boolean;
begin

// Filter operation undependend settings
  mOriginalSettings.additional.option.yarnCnt :=
    aSettings.additional.option.yarnCnt;
  mOriginalSettings.additional.option.yarnUnit :=
    aSettings.additional.option.yarnUnit;
  mOriginalSettings.additional.option.threadCnt :=
    aSettings.additional.option.threadCnt;

  Result := not CompareMem(@aSettings, @mOriginalSettings, SizeOf(aSettings));
end;
//------------------------------------------------------------------------------


procedure TYMSettings.GetFromScreen(var aSettings: TYMSettingsByteArr;
  var aSize: Word);
//var
//  xSettings: TYMSettingsRec;
begin
  GetFromScreen(PYMSettings(@aSettings)^);
//  GetFromScreen(xSettings);
  aSize := SizeOf(TYMSettingsRec);
//  TYMSettingsUtils.ChangeEndian(aSettings, aSize);
  // mYMSettingsUtils.ChangeEndian(xSettings, PYMSettings(@aSettings)^)
end;
//------------------------------------------------------------------------------

procedure TYMSettings.GetFromScreen(var aSettings: TYMSettingsRec);
begin

  mYMSettingsUtils.InitYMSettings(aSettings);

(*  aSettings.available.inoperativePara[0] := mOriginalSettings.available.inoperativePara[0];
  aSettings.available.inoperativePara[1] := mOriginalSettings.available.inoperativePara[1];
*)

  InsertScreenToSettings(aSettings);

end;
//------------------------------------------------------------------------------

function TYMSettings.GetInopSettings0: DWord;
begin
  Result := mMachineAttributes.InopSettings0;
end;
//------------------------------------------------------------------------------

function TYMSettings.GetInopSettings1: DWord;
begin
  Result := mMachineAttributes.InopSettings1;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.InsertScreenToSettings(
  var aSettings: TYMSettingsRec);
var
  xCount: TYarnCountSettingsRec;
  xCluster: TFaultClusterSettingsRec;
  xFFCluster: TFFClusterSettingsRec;
  xSFI: TSFISettingsRec;
  xGBBasicProdPara: TBasicProdSettingsRec;
begin

  if Assigned(ChannelEditBox) then
    ChannelEditBox.GetPara(aSettings.channel)
  else if Assigned(ClassMatrixEditBox) then
    aSettings.channel := ClassMatrixEditBox.GetChannelPara;

  if Assigned(SpliceEditBox) then
    SpliceEditBox.GetPara(aSettings.splice)
  else if Assigned(ClassMatrixEditBox) then
    aSettings.splice := ClassMatrixEditBox.GetSplicePara;

  if Assigned(YarnCountEditBox) then
  begin
    YarnCountEditBox.GetPara(xCount);
    aSettings.additional.option.diaDiff := xCount.posDiaDiff;
    aSettings.additional.extOption.negDiaDiff := xCount.negDiaDiff;
    aSettings.additional.extOption.countLength := xCount.countLength;
    aSettings.additional.option.yarnCnt := YarnCountEditBox.YarnCount;
    aSettings.additional.option.yarnUnit := Byte(YarnCountEditBox.YarnUnit);
//    aSettings.additional.option.minCnt := 0;
//    aSettings.additional.option.maxCnt := 0;
  end;

  if Assigned(FaultClusterEditBox) then
  begin
    FaultClusterEditBox.GetPara(xCluster);
    aSettings.additional.option.clusterDia := xCluster.clusterDia;
    aSettings.additional.option.clusterLength := xCluster.clusterLength;
    aSettings.additional.option.clusterDefects := xCluster.clusterDefects;
//    FaultClusterEditBox.PutYMSettings(aSettings);
  end
  else if Assigned(ClassMatrixEditBox) then
    aSettings.additional.option.clusterDia := ClassMatrixEditBox.GetClusterDiameter;

  if Assigned(SFIEditBox) then
  begin
    SFIEditBox.GetPara(xSFI);
    aSettings.sFI.absRef := xSFI.absRef;
    aSettings.sFI.upper := xSFI.upper;
    aSettings.sFI.lower := xSFI.lower;
  end;

  if Assigned(FFClusterEditBox) then
  begin
    FFClusterEditBox.GetPara(xFFCluster);
    aSettings.fFCluster.obsLength := xFFCluster.clusterLength;
    aSettings.fFCluster.defects := xFFCluster.clusterDefects;
  end;

  if Assigned(GBBasicProdPara) then
  begin
    GBBasicProdPara.GetPara(xGBBasicProdPara);

    aSettings.additional.option.cntRep := xGBBasicProdPara.cntRep;
    aSettings.additional.extOption.clusterRep := xGBBasicProdPara.clusterRep;

    aSettings.additional.option.siroStartupRep := xGBBasicProdPara.siroStartupRep;
    aSettings.sFI.rep := xGBBasicProdPara.sFIRep;
    aSettings.fFCluster.rep := xGBBasicProdPara.FFClusterRep;

    aSettings.additional.fAdjConeReduction := xGBBasicProdPara.fAdjConeReduction;

    aSettings.splice.checkLen := xGBBasicProdPara.checkLen;

    // ConfigCode
    with aSettings.additional do
    begin
      configA := xGBBasicProdPara.configA;
      configB := xGBBasicProdPara.configB;
      configC := xGBBasicProdPara.configC;

(*      configA := configA or xGBBasicProdPara.configA;
      configB := configB or xGBBasicProdPara.configB;
      configC := configC or xGBBasicProdPara.configC;
*)
    end;
  end;

  if Assigned(ClassMatrixEditBox) then
  begin
    ClassMatrixEditBox.GetFieldStateTable(aSettings.classClear);
  end;

  if Assigned(FFMatrixEditBox) then
    FFMatrixEditBox.GetFieldStateTable(aSettings.siroClear, aSettings.FFCluster.siroClear);

  aSettings.available.inoperativePara[0] := mMachineAttributes.InopSettings0;
  aSettings.available.inoperativePara[1] := mMachineAttributes.InopSettings1;

end;
//------------------------------------------------------------------------------

procedure TYMSettings.InsertScreenToSettings(
  var aSettings: TYMSettingsByteArr; var aSize: Word);
begin

  InsertScreenToSettings(PYMSettings(@aSettings)^);
  aSize := SizeOf(TYMSettingsRec);
end;
//------------------------------------------------------------------------------

procedure TYMSettings.IntroduceOriginalSettings;
begin

  GetFromScreen(mOriginalSettings);
end;
//------------------------------------------------------------------------------

procedure TYMSettings.LoadFromDB(aSetID: Integer; var aYMSetName: TText50);
var
  xYMExtension: TYMExtensionRec;
begin
(*  if not(assigned(mSettingsDBAssistant)) then
    CodeSite.SendError('TYMSettings.LoadFromDB: mSettingsDBAssistant = nil');*)
  mSettingsDBAssistant.Get(aSetID, xYMExtension);
  aYMSetName := xYMExtension.YM_Set_Name;
  PutToScreen(xYMExtension.YMSettings);
end;
//------------------------------------------------------------------------------

function TYMSettings.NewOnDB: Integer;
var
  xSettings: TYMSettingsRec;
begin
  GetFromScreen(xSettings);
  Result := mSettingsDBAssistant.New(xSettings);
end;
//------------------------------------------------------------------------------

function TYMSettings.NewOnDB(aName: string): integer;
var
  xSettings: TYMSettingsRec;
begin
  GetFromScreen(xSettings);
  Result := mSettingsDBAssistant.New(xSettings, aName);
end;
//------------------------------------------------------------------------------

function TYMSettings.NewTemplate(aName: string): integer;
var
  xSettings: TYMSettingsRec;
  xMachineAttributes: TMachineAttributes;
begin
  GetFromScreen(xSettings);

  xMachineAttributes := TMachineAttributes.Create(xSettings);
  xMachineAttributes.EnableAll;
  xSettings.available.inoperativePara[0] := xMachineAttributes.InopSettings0;
  xSettings.available.inoperativePara[1] := xMachineAttributes.InopSettings1;
  xMachineAttributes.Free;

  Result := mSettingsDBAssistant.NewTemplate(xSettings, aName);


end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutChannelEditBox(const aValue: TChannelEditBox);
var
  xSettings: TYMSettingsRec;
begin
  fChannelEditBox := aValue;

  if Assigned(ChannelEditBox) then
  begin
    xSettings := mYMSettingsUtils.DefaultYMPara;
    ChannelEditBox.PutYMSettings(xSettings);
  // Update used settings only!
    GetFromScreen(mOriginalSettings);
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutClassMatrixEditBox(const aValue: TQualityMatrix);
var
  xSettings: TYMSettingsRec;
begin
  fClassMatrixEditBox := aValue;

  if Assigned(ClassMatrixEditBox) then
  begin
    xSettings := mYMSettingsUtils.DefaultYMPara;
    ClassMatrixEditBox.PutYMSettings(xSettings);
  // Update used settings only!
    GetFromScreen(mOriginalSettings);
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutDefaultToScreen;
var
  xSettings: TYMSettingsRec;
begin
  xSettings := mYMSettingsUtils.DefaultYMPara;
  PutToScreen(xSettings);
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutFaultClusterEditBox(
  const aValue: TFaultClusterEditBox);
var
  xSettings: TYMSettingsRec;
begin
  fFaultClusterEditBox := aValue;

  if Assigned(FaultClusterEditBox) then
  begin
    xSettings := mYMSettingsUtils.DefaultYMPara;
    FaultClusterEditBox.PutYMSettings(xSettings);
  // Update used settings only!
    GetFromScreen(mOriginalSettings);
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutFFMatrixEditBox(const aValue: TQualityMatrix);
var
  xSettings: TYMSettingsRec;
begin
  fFFMatrixEditBox := aValue;

  if Assigned(FFMatrixEditBox) then
  begin
    xSettings := mYMSettingsUtils.DefaultYMPara;
    FFMatrixEditBox.PutYMSettings(xSettings);
  // Update used settings only!
    GetFromScreen(mOriginalSettings);
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutGBBasicProdPara(const Value: TGBBasicProdPara);
var
  xSettings: TYMSettingsRec;
begin
  fGBBasicProdPara := Value;

  if Assigned(GBBasicProdPara) then
  begin
    xSettings := mYMSettingsUtils.DefaultYMPara;
    GBBasicProdPara.PutYMSettings(xSettings);
  // Update used settings only!
    GetFromScreen(mOriginalSettings);
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutSpliceEditBox(const aValue: TSpliceEditBox);
var
  xSettings: TYMSettingsRec;
begin
  fSpliceEditBox := aValue;

  if Assigned(SpliceEditBox) then
  begin
    xSettings := mYMSettingsUtils.DefaultYMPara;
    SpliceEditBox.PutYMSettings(xSettings);
  // Update used settings only!
    GetFromScreen(mOriginalSettings);
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutToScreen(var aSettings: TYMSettingsRec);
begin

  PutToScreenPrivat(aSettings);
  if Assigned(GBBasicProdPara) then
    GBBasicProdPara.SetSensingHeadDependency;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutToScreen(var aSettings: TYMSettingsByteArr);
begin
  PutToScreen(PYMSettings(@aSettings)^);
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutToScreenPrivat(var aSettings: TYMSettingsRec);
begin

// %% Test
//  mYMSettingsUtils.ExtractMachineYMPara(aSettings, mMachineYMPara);

  if Assigned(ChannelEditBox) then
    ChannelEditBox.PutYMSettings(aSettings);

  if Assigned(SpliceEditBox) then
    SpliceEditBox.PutYMSettings(aSettings);

  if Assigned(YarnCountEditBox) then
    YarnCountEditBox.PutYMSettings(aSettings);

  if Assigned(FaultClusterEditBox) then
    FaultClusterEditBox.PutYMSettings(aSettings);

  if Assigned(ClassMatrixEditBox) then
    ClassMatrixEditBox.PutYMSettings(aSettings);

  if Assigned(SFIEditBox) then
    SFIEditBox.PutYMSettings(aSettings);

  if Assigned(FFMatrixEditBox) then
    FFMatrixEditBox.PutYMSettings(aSettings);

  if Assigned(FFClusterEditBox) then
    FFClusterEditBox.PutYMSettings(aSettings);

  if Assigned(GBBasicProdPara) then
    GBBasicProdPara.PutYMSettings(aSettings);

  GetFromScreen(mOriginalSettings);

  mMachineAttributes.InopSettings0 := aSettings.available.inoperativePara[0];
  mMachineAttributes.InopSettings1 := aSettings.available.inoperativePara[1];
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutSFIEditBox(const aValue: TSFIEditBox);
var
  xSettings: TYMSettingsRec;
begin
  fSFIEditBox := aValue;

  if Assigned(SFIEditBox) then
  begin
    xSettings := mYMSettingsUtils.DefaultYMPara;
    SFIEditBox.PutYMSettings(xSettings);
  // Update used settings only!
    GetFromScreen(mOriginalSettings);
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutYarnCountEditBox(const aValue: TYarnCountEditBox);
var
  xSettings: TYMSettingsRec;
begin
  fYarnCountEditBox := aValue;

  if Assigned(YarnCountEditBox) then
  begin
    xSettings := mYMSettingsUtils.DefaultYMPara;
    YarnCountEditBox.PutYMSettings(xSettings);
  // Update used settings only!
    GetFromScreen(mOriginalSettings);
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.UpdateOnDB(aSetID: Integer);
var
  xSettings: TYMSettingsRec;
begin
  GetFromScreen(xSettings);
  mSettingsDBAssistant.Update(aSetID, xSettings);
end;
//------------------------------------------------------------------------------

procedure TYMSettings.PutFFClusterEditBox(const aValue: TFFClusterEditBox);
var
  xSettings: TYMSettingsRec;
begin
  fFFClusterEditBox := aValue;

  if Assigned(FFClusterEditBox) then
  begin
    xSettings := mYMSettingsUtils.DefaultYMPara;
    FFClusterEditBox.PutYMSettings(xSettings);
  // Update used settings only!
    GetFromScreen(mOriginalSettings);
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettings.SetInopSettings0(const aValue: DWord);
begin

  mMachineAttributes.InopSettings0 := aValue;
//  mOriginalSettings.available.inoperativePara[0] := aValue;
end;

//------------------------------------------------------------------------------

procedure TYMSettings.SetInopSettings1(const aValue: DWord);
begin

  mMachineAttributes.InopSettings1 := aValue;
//  mOriginalSettings.available.inoperativePara[1] := aValue;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// TYMSettingsForGUI
//******************************************************************************
//------------------------------------------------------------------------------

constructor TYMSettingsForGUI.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fDBAccess := True;
  fQuery := nil;
  mYMSettings := TYMSettings.CreateStandAlone;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.Delete(aSetID: Integer);
begin

  if DBAccess then
    mYMSettings.Delete(aSetID);
end;
//------------------------------------------------------------------------------

destructor TYMSettingsForGUI.Destroy;
begin

  mYMSettings.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.DoChanged(aTag: Integer);
begin
  if Assigned(fOnChange) then
    fOnChange(self.Owner);
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.DoConfirm(aTag: Integer);
var
  xSettings: TYMSettingsRec;
begin
  if mYMSettings.Changed then
  begin

    mYMSettings.GetFromScreen(xSettings);
    mYMSettings.PutToScreen(xSettings);

    DoChanged(aTag);

  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.DoParameterChange(aSource: TParameterSource);
var
  xPara: TChannelSettingsRec;
  xCluster: TFaultClusterSettingsRec;
  xSettings: TYMSettingsRec;
begin

  case aSource of

    psChannel:
      begin

        xPara := ClassMatrixEditBox.GetChannelPara;
        if Assigned(ChannelEditBox) then
          ChannelEditBox.PutPara(xPara);
        mYMSettings.GetFromScreen(xSettings);
        ClassMatrixEditBox.PutYMSettings(xSettings);
        DoChanged(0);
      end;

    psCluster:
      begin

        if Assigned(FaultClusterEditBox) then
        begin
          FaultClusterEditBox.GetPara(xCluster);
          xCluster.clusterDia := ClassMatrixEditBox.GetClusterDiameter;
          FaultClusterEditBox.PutPara(xCluster);
        end;
        mYMSettings.GetFromScreen(xSettings);
        ClassMatrixEditBox.PutYMSettings(xSettings);
        DoChanged(0);
      end;

    psSplice:
      begin

        xPara := ClassMatrixEditBox.GetSplicePara;
        if Assigned(SpliceEditBox) then
          SpliceEditBox.PutPara(xPara);
        mYMSettings.GetFromScreen(xSettings);
        ClassMatrixEditBox.PutYMSettings(xSettings);
        DoChanged(0);

      end;

  end;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetChannelEditBox: TChannelEditBox;
begin

  Result := mYMSettings.ChannelEditBox;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetClassMatrixEditBox: TQualityMatrix;
begin

  Result := mYMSettings.ClassMatrixEditBox;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetFaultClusterEditBox: TFaultClusterEditBox;
begin

  Result := mYMSettings.FaultClusterEditBox;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetFFClusterEditBox: TFFClusterEditBox;
begin

  Result := mYMSettings.FFClusterEditBox;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetFFMatrixEditBox: TQualityMatrix;
begin

  Result := mYMSettings.FFMatrixEditBox;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetFromScreen(
  var aSettings: TYMSettingsByteArr; var aSize: Word): Boolean;
begin

  mYMSettings.GetFromScreen(aSettings, aSize);
  Result := mYMSettings.GetChanged(PYMSettings(@aSettings)^);
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetFromScreen(var aSettings: TYMSettingsRec): Boolean;
begin

  mYMSettings.GetFromScreen(aSettings);
  Result := mYMSettings.GetChanged(aSettings);
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetGBBasicProdPara: TGBBasicProdPara;
begin

  Result := mYMSettings.GBBasicProdPara;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetSFIEditBox: TSFIEditBox;
begin

  Result := mYMSettings.SFIEditBox;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetSpliceEditBox: TSpliceEditBox;
begin

  Result := mYMSettings.SpliceEditBox;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.GetYarnCountEditBox: TYarnCountEditBox;
begin

  Result := mYMSettings.YarnCountEditBox;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.InsertScreenToSettings(
  var aSettings: TYMSettingsRec);
begin

  mYMSettings.InsertScreenToSettings(aSettings);
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.InsertScreenToSettings(
  var aSettings: TYMSettingsByteArr; var aSize: Word);
begin

  mYMSettings.InsertScreenToSettings(aSettings, aSize);
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.Loaded;
begin
  inherited;

  if DBAccess then
    if Query <> nil then
      mYMSettings.EstablishDBAccess(Query)
    else
      mYMSettings.EstablishDBAccess;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.LoadFromDB(aSetID: Integer; var aYMSetName: TText50);
begin
  if DBAccess then
    mYMSettings.LoadFromDB(aSetID, aYMSetName);
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.NewOnDB: Integer;
begin

  if DBAccess then
    Result := mYMSettings.NewOnDB
  else
    Result := 0;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.NewOnDB(aName: string): integer;
begin

  if DBAccess then
    Result := mYMSettings.NewOnDB(aName)
  else
    Result := 0;
end;
//------------------------------------------------------------------------------

function TYMSettingsForGUI.NewTemplate(aName: string): integer;
begin

  if DBAccess then
    Result := mYMSettings.NewTemplate(aName)
  else
    Result := 0;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutChannelEditBox(
  const aValue: TChannelEditBox);
begin
  with mYMSettings do
  begin

    ChannelEditBox := aValue;
    if Assigned(aValue) then
    begin
      ChannelEditBox.OnConfirm := DoConfirm;
      ChannelEditBox.Printable := False;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutClassMatrixEditBox(
  const aValue: TQualityMatrix);
begin
  with mYMSettings do
  begin

    ClassMatrixEditBox := aValue;
    if Assigned(aValue) then
    begin
      ClassMatrixEditBox.OnConfirm := DoChanged;
      ClassMatrixEditBox.OnParameterChange := DoParameterChange;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutDefaultToScreen;
begin

  mYMSettings.PutDefaultToScreen;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutFaultClusterEditBox(
  const aValue: TFaultClusterEditBox);
begin

  with mYMSettings do
  begin
    FaultClusterEditBox := aValue;
    if Assigned(aValue) then
    begin
      FaultClusterEditBox.OnConfirm := DoConfirm;
      FaultClusterEditBox.Printable := False;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutFFClusterEditBox(
  const aValue: TFFClusterEditBox);
begin

  with mYMSettings do
  begin
    FFClusterEditBox := aValue;
    if Assigned(aValue) then
    begin
      FFClusterEditBox.OnConfirm := DoConfirm;
      FFClusterEditBox.Printable := False;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutFFMatrixEditBox(
  const aValue: TQualityMatrix);
begin
  with mYMSettings do
  begin
    FFMatrixEditBox := aValue;
    if Assigned(aValue) then
    begin
      FFMatrixEditBox.MatrixMode := mmSelectCutFields;
      FFMatrixEditBox.OnConfirm := DoChanged;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutGBBasicProdPara(
  const aValue: TGBBasicProdPara);
begin

  with mYMSettings do
  begin
    GBBasicProdPara := aValue;
    if Assigned(aValue) then
      GBBasicProdPara.OnConfirm := DoConfirm;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutSFIEditBox(const aValue: TSFIEditBox);
begin

  with mYMSettings do
  begin
    SFIEditBox := aValue;
    if Assigned(aValue) then
    begin
      SFIEditBox.OnConfirm := DoChanged;
      SFIEditBox.Printable := False;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutSpliceEditBox(const aValue: TSpliceEditBox);
begin

  with mYMSettings do
  begin
    SpliceEditBox := aValue;
    if Assigned(aValue) then
    begin
      SpliceEditBox.OnConfirm := DoConfirm;
      SpliceEditBox.Printable := False;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutToScreen(var aSettings: TYMSettingsRec);
begin

  mYMSettings.PutToScreen(aSettings);
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutToScreen(var aSettings: TYMSettingsByteArr);
begin

  mYMSettings.PutToScreen(aSettings);
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.PutYarnCountEditBox(
  const aValue: TYarnCountEditBox);
begin

  with mYMSettings do
  begin
    YarnCountEditBox := aValue;
    if Assigned(aValue) then
    begin
      YarnCountEditBox.OnConfirm := DoConfirm;
      YarnCountEditBox.Printable := False;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TYMSettingsForGUI.UpdateOnDB(aSetID: Integer);
begin

  if DBAccess then
    mYMSettings.UpdateOnDB(aSetID);
end;
//------------------------------------------------------------------------------




end.

