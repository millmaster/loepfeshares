(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QualityMatrixDef.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.05.1999  0.00  Kr  | Initial Release
|=========================================================================================*)
unit QualityMatrixDef;

interface

uses Windows, Graphics, SysUtils;

type
//------------------------------------------------------------------------------
// Published Propertie Types
//------------------------------------------------------------------------------
  //...........................................................................
  TDisplayMode = (dmNone, dmValues, dmColor, dmDots, dmSCValues, dmCalculateSCValues);
  TLastCutMode = (lcNone, lcValue, lcField);
  TMatrixMode = (mmSelectSettings, mmSelectCutFields, mmSelectFFClusterFields,
    mmSelectSCMembers, mmDisplayData);
(*
    , mmDisplayColor, mmDisplayDots,
    mmDisplaySCData, mmDisplayCalculateSCData);
*)
  TMatrixSubType = (mstNone, mstSiroF, mstSiroH, mstSiroBD, mstZenitF);
  TMatrixType = (mtNone, mtShortLongThin, mtSiro, mtSplice, mtColor);
  TSelectionMode = (smCutField, smFFClusterField, smSCMemberField);
  //...........................................................................

//------------------------------------------------------------------------------
// Misc Types
//------------------------------------------------------------------------------
  //...........................................................................

  TCalculationBase = (cbAbsolute, cbRelative);
  TCoordinateMode = (cmActual, cmLeft, cmRight, cmTop, cmBottom, cmOutside, cmInside);
  TItemAlignement = (iaLeft, iaRight, iaCenter, iaTop, iaBottom);
  //...........................................................................

//------------------------------------------------------------------------------
// Base records
//------------------------------------------------------------------------------
  //...........................................................................

  TRangeRec = record
    start, stop: Integer;               // start, stop position (pixel units)
  end;
  //...........................................................................

  TPenRec = record
    width: Integer;                     // line width (pixel units)
    color: TColor;                      // line color
    mode: TPenMode;                     // line mode
    style: TPenStyle;                   // line style
  end;
  //...........................................................................

  TBrushRec = record
    color: TColor;                      // Brush color
    style: TBrushStyle;                 // Brush style
  end;
  //...........................................................................

  TFontRec = record
    color: TColor;                      // Font color
//    pitch:        TFontPitch;   // Font pitch
    size: Integer;                      // point size of the font
    style: TFontStyles;                 // Font style
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Field records
//------------------------------------------------------------------------------
  //...........................................................................

  TFieldRec = record
    top: Integer;                       // top position (raster units)
    left: Integer;                      // left position (raster units)
    width: Integer;                     // if non zero individual field width
                   // else default width (raster units)
    height: Integer;                    // if non zero individual field hight
          // else default height (raster units)
    pBackground: ^TBrushRec;            // pointer to individual background color
                // settings, if nil the default background setting is active
  end;
  //...........................................................................
  PFieldRecArray = ^TFieldRecArray;
  TFieldRecArray = array[0..32767] of TFieldRec;

  TFieldsRec = record
    fieldWidth: Integer;                // default field width (raster units)
    fieldHeight: Integer;               // default field height (raster units)
    background: TBrushRec;              // default background setting
//    table:				array of		TFieldRec;
    table: PFieldRecArray;
  end;
  //...........................................................................

  TMatrixRasterRec = record
    width: Integer;                     // width of class scheme (raster units)
    height: Integer;                    // hight of class scheme (raster units)
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Curve Layer records (SLT Matrix)
//------------------------------------------------------------------------------
  //...........................................................................

  TScaleRec = record
    value: Integer;                     // Scale value, (x axis: length, [mm]/y axis: diameter *100)
    pixcelToNextScale: Integer;         // Pixcel distance to next scale, if 0 then end of scale
    numberOfSamples: Integer;           // number of calculated sampels, (x axis only!)
  end;

  //...........................................................................  TChannelCurve = class(TObject)

  PIntegerArray = ^TIntegerArray;
  TIntegerArray = array[0..32767] of Integer;

  TCurveLayerRec = record
// Caution: When using open array (array of) for array pointers, everthin runs nice except if
// RANGE CHECK is enabled! Range check has a problem when no dynamic array is assigned to
// an open array.
//    numberOfSampleTbl: array of Integer;    // number of calculated sampels per Scale, (xAxis only)
//    numberOfSampleTbl: PIntegerArray;   // number of calculated sampels per Scale, (xAxis only)
    numberOfSamplePerScale: Integer;
    visibleNSLRange: TRect;             // Visible Range of NSL-Curve (Scale value, x axis: length[mm]/y axsis diameter * 100)
    visibleTRange: TRect;               // Visible Range of T-Curve (Scale value, x axis: length[mm]/y axsis diameter * 100)
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Display Layer records
//------------------------------------------------------------------------------
  //...........................................................................

const
  cMaxSuperClassMember = 16;
  cSCDelimiter = $FF;
  //...........................................................................

type
  TFormatTextRec = record
    pFont: ^TFontRec;                   // Text Font, if nil Font will not be changed
    horAlign: TItemAlignement;
    vertAlign: TItemAlignement;
    background: TColor;                 // Backgroundcolor of text field
    preText: string;
    formatText: string;
    postText: string;
  end;
  //...........................................................................

  TNumberFormatRec = record
    cuts: TFormatTextRec;
    defects: TFormatTextRec;
  end;
  //...........................................................................

  TSCMemberArray = array[0..cMaxSuperClassMember - 1] of Byte;

  TSuperClassRec = record
    PNumber: ^TNumberFormatRec;
    boarder: TFieldRec;
    memberTbl: TSCMemberArray;
  end;
  //...........................................................................

  PSuperClassArray = ^TSuperClassArray;
  TSuperClassArray = array[0..32767] of TSuperClassRec;

  TDisplayLayerRec = record
    number: TNumberFormatRec;
    roughClassTbl: PSuperClassArray;
    defaultSuperClassTbl: PSuperClassArray;
  end;
  //...........................................................................

  //------------------------------------------------------------------------------
// Scale Layer records
//------------------------------------------------------------------------------
  //...........................................................................

  TLineRec = record
    pos: Integer;                       // Horizontal or vertical position (raster units)
    start: Integer;                     // start position of line (raster units)
    stop: Integer;                      // stop position of line (raster units)
    pPen: ^TPenRec;                     // pointer to pen settings, if nil the lines
                   // default pen settings is activ
  end;
  //...........................................................................

  TTextRec = record
    pFont: ^TFontRec;                   // Text Font, if nil Font will not be changed
    horAlign: TItemAlignement;          // Text alignement to it's Horizontal position
    vertAlign: TItemAlignement;         // Text alignement to it's Vertical position
    xPos, yPos: Integer;                // position (raster units)
    text: string;
  end;
  //...........................................................................
  PLineRecArray = ^TLineRecArray;
  TLineRecArray = array[0..32767] of TLineRec;

  TLinesRec = record
    pen: TPenRec;
// Caution: When using open array (array of) for array pointers, everthin runs nice except if
// RANGE CHECK is enabled! Range check has a problem when no dynamic array is assigned to
// an open array.
// 	table: array of	TLineRec;
    table: PLineRecArray;
  end;
  //...........................................................................
  PTextRecArray = ^TTextRecArray;
  TTextRecArray = array[0..32767] of TTextRec;

  TScaleLayerRec = record
    horLine: TLinesRec;
    vertLine: TLinesRec;
//    xAxis: array of   TTextRec;
//    yAxis: array of   TTextRec;
//    caption: array of TTextRec;
    xAxis: PTextRecArray;
    yAxis: PTextRecArray;
    caption: PTextRecArray;
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Selection Layer records
//------------------------------------------------------------------------------
  //...........................................................................

  TSelectionStateRec = record
    pActiveColor: ^TBrushRec;           // pointer to the color if cut is activated
                                        // if nil no color is assigned
    pPassiveColor: ^TBrushRec;          // pointer to the color if cut is activated
                                        // if nil no color is assigned
  end;
  //...........................................................................

  TSelectionLayerRec = record
    cutState: TSelectionStateRec;
    fFClusterState: TSelectionStateRec;
    sCMemberState: TSelectionStateRec;
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// Quality Matrx Descriptor record
//------------------------------------------------------------------------------
  //...........................................................................

  TQualityMatrixDescRec = record
    pRaster: ^TMatrixRasterRec;
    pField: ^TFieldsRec;
    pScaleLayer: ^TScaleLayerRec;
    pDisplayLayer: ^TDisplayLayerRec;
    pSelectionLayer: ^TSelectionLayerRec;
    pCurveLayer: ^TCurveLayerRec;
  end;
  //...........................................................................

//------------------------------------------------------------------------------
// General definitions and default initialisation
//------------------------------------------------------------------------------
  //...........................................................................

const
  clDisableText = clBlack;
  fsDisableText = 10;
  clLine = clGray;
  clRoughLine = clBlack;// clBlack; // clDkGray;
  clScale = clBlack;
//  clScale = clGray;
  clQualityCutOn = clAqua;
  clQualityBackground = $0090FFFF;
//  clQualityCutOn = $00FFFF80;
  clSuperClassMemberOn = clLime;
//  clSuperClassMemberOn = clLtGray;
  //clAqua;
  //clBlue;
  //...........................................................................

{(*}
 cBackground0: TBrushRec = (
   color:        clBlue;
   style:        bsSolid;
 );

 cCutOn: TBrushRec = (
   color:        clQualityCutOn;
   style:        bsSolid;
 );

 cFFClusterOn: TBrushRec = (
    color:        clQualityCutOn;
//   style:        bsBDiagonal, bsCross...;  Scheissspiel: Background wird schwarz !!?
    style:        bsSolid;
 );

 cSCMemberOn: TBrushRec = (
   color:        clSuperClassMemberOn;
   style:        bsSolid;
 );

 cBackgroundUnassigned: TBrushRec = (
   color:        clBackground;
   style:        bsSolid;
 );

  //...........................................................................

 cFontAxis: TFontRec = (
    color:      clScale;
    size:       10;
    style:			[];
  );

 cFontCuts: TFontRec = (
    color:      clRed;
    size:       8;
    style:			[];
  );

 cFontDefects: TFontRec = (
    color:      clBlack;
    size:       8;
    style:			[];
  );

 cFontCaption: TFontRec = (
    color:      clBlack;
    size:       12;
    style:			[fsBold];
  );

 cFontScaleEnds: TFontRec = (
    color:      clNone;
//    size:       8;
    size:       10;
//    style:			[fsBold];
    style:			[];
  );

 cFontUnassigned: TFontRec = (
    color:				clBlack;
    size:         7;
    style:				[];
  );
  //...........................................................................

 cLinePenRough: TPenRec = (
     width:				3;
      color:        clRoughLine;
      mode:         pmCopy;
      style:        psSolid;
  );

 cLinePenUnassigned: TPenRec = (
  width:				1;
   color:        clBlack;
   mode:         pmBlack;
   style:        psSolid;
 );

 cChannelCurvePen: TPenRec = (
  width:				2;
   color:        clRed;
   mode:         pmCopy;
   style:        psSolid;
 );

 cClusterCurvePen: TPenRec = (
  width:				2;
   color:        clPurple;
   mode:         pmCopy;
   style:        psSolid;
 );

 cSpliceCurvePen: TPenRec = (
  width:				2;
   color:        clBlue;
   mode:         pmCopy;
   style:        psSolid;
 );
  //...........................................................................

 cFormatTextUnassigned: TFormatTextRec = (
    pFont:      nil;
    horAlign:   iaCenter;
    vertAlign:  iaCenter;
    preText:    '';
    formatText: '';
    postText:   '';
 );

 cTextUnassigned: TTextRec = (
    pFont:      nil;
    horAlign:   iaCenter;
    vertAlign:  iaCenter;
   xPos:       0;
    yPos:       0;
    text:       '';
 );
  //...........................................................................

{*)}

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.

