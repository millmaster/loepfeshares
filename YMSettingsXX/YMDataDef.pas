(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: EditorServiceClasses.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.03.2000  1.00  Mg  | Datei erstellt
| 12.07.2000  1.00  Kr  | Enumeration Type for TBaseData added
| 18.09.2001  1.00  Wss | adjust renamed do AdjustBase for SFI calculation
|=========================================================================================*)
unit YMDataDef;
interface
type

//------------------------------------------------------------------------------
// MM NT Telegram contents if  telegram subID SID_BASE_DATA, 118 byte
//------------------------------------------------------------------------------
  //...........................................................................
TBaseData = packed record
  //var Name MM        type MM    //  type YM   var Name YM     Demo value from YM-ZE
  status: BYTE;                         //  uchar	status;         0         0       0         /* bit0=0: konsistent, bit0=1: inkonsistent */
  fill0: BYTE;                          //  uchar	fill0;          0         0       0
  watchTime: CARDINAL;                  //  ulong	watchTime;      1784      1922    1931      /* observation time, [s] */
  operatingTime: CARDINAL;              //  ulong	operatingTime;  1784      1364    1931      /* Time during which at least one spindle works */
  runTime: CARDINAL;                    //  ulong	runTime;        1659      1295    1795      /* run time, [s] */
  length: CARDINAL;                     //  ulong	length;         6999400   7110400 7503000   /* spooled length, [5mm] */
  nepCuts: Byte;                        //  uchar	nepCuts;        1         1       2         /* number of nep cuts, when UPDR_CHANNEL_CUT or UPDR_CLASS_CUT */
  shortCuts: Byte;                      //  uchar	shortCuts;      2         1       2         /* number of short cuts, when UPDR_CHANNEL_CUT or UPDR_CLASS_CUT */
  longCuts: Byte;                       //  uchar	longCuts;       0         0       0         /* number of long cuts, when UPDR_CHANNEL_CUT or UPDR_CLASS_CUT */
  thinCuts: Byte;                       //  uchar	thinCuts;       0         0       1         /* number of thin cuts, when UPDR_CHANNEL_CUT or UPDR_CLASS_CUT */
  clusterCuts: Byte;                    //  uchar	clusterCuts;    0         1       0         /* number of cluster cuts: UPDR_CLUSTER_CUT */
  offCountCuts: Byte;                   //  uchar	offCountCuts;   0         0       0         /* number of yarn count cuts: UPDR_YARN_CNT_CUT */
  offCountLocks: Byte;                  //  uchar	offCountLocks;  0         0       0         /* number of spindle locks because of yarncount interventions: YARN_CNT_CUT&&SPINDLE_LOCKED_M */
  siroCuts: Byte;                       //  uchar	siroCuts;       1         0       2         /* number of siro cuts: UPDR_SIRO_CUT */
  totalYarnCuts: Word;                  //  ushort	totalYarnCuts;  4         3       7         /* total "textil" yarn cuts: UPDR_CHANNEL_CUT||UPDR_CLASS_CUT||UPDR_SIRO_CUT||UPDR_CLUSTER_CUT||UPDR_YARN_CNT_CUT */
  systemCuts: Byte;                     //  uchar	systemCuts;     0         0       0         /* number of system cuts: UPDR_SYSTEM_CUT||UPDR_ADJUST_CUT||UPDR_ADDITIONAL_CUT */
  systemLocks: Byte;                    //  uchar	systemLocks;    0         0       0         /* number of spindle locks because of system problem:!(CLUSTER_CUT||YARN_CNT_CUT)&&SPINDLE_LOCKED_M */
  bunchCuts: Byte;                      //  uchar	bunchCuts;      0         0       0         /* cuts because of jumping yarn */
  cones: Byte;                          //  uchar	cone;           0         0       0         /* number of produced cones: FULL_CONE_M */
  cops: Word;                           //  ushort	cops;           5         5       6         /* number of used cops: ESSADR_M&&EMPTY_COPS_M */
  yarnBreaks: Byte;                     //  uchar	yarnBreaks;     1         0       1         /* number of yarn breaks: UPDR_NO_SPEC_CUT||UPDR_NO_CUT */
  delBunchCuts: Byte;                   //  uchar	delBunchCuts;   0         0       0         /* delaied bunch cuts */
  splices: Word;                        //  ushort	splices;        10        8       14        /* number of spooled splices: UPDR_ADDITIONAL_CUT||UPDR_CLUSTER_CUT||UPDR_YARN_CNT_CUT||UPDR_CHANNEL_CUT||UPDR_CLASS_CUT||UPDR_SIRO_CUT||UPDR_ADJUST_CUT||UPDR_BUNCH_CUT||UPDR_SYSTEM_CUT||UPDR_NO_SPEC_CUT||UPDR_NO_CUT ) */
  spliceCuts: Byte;                     //  uchar	spliceCuts;     0         0       0         /* number of splice cuts: UPDR_SPLICE_CUT */
  spliceRep: Byte;                      //  uchar	spliceRep;      1         3       0         /* number of splice repetitions: splrep */
  upperYarnCuts: Byte;                  //  uchar	upperYarnCuts;  0         0       0         /* number of muratas upper yarn cuts: UPDR_UPPERYARN_CUT */
  longStops: Byte;                      //  uchar	longStops;      0         0       0         /* long stops */
  longStopTime: CARDINAL;               //  ulong	longStopTime;   0         0       0         /* long stop time, [s] */
  shortClustArea: CARDINAL;             //  ulong	shortClustArea; 0         0       0         /* number of short defects in cluster area: */
  longClustArea: CARDINAL;              //  ulong	longClustArea;  0         0       0         /* number of long defects in cluster area: */
  thinClustArea: CARDINAL;              //  ulong	thinClustArea;  0         0       0         /* number of thin defects in cluster area: */
  warnings: Byte;                       //  uchar	warnings;       0         0       0         /* number of warning messages */
  alarms: Byte;                         //  uchar	alarms;         0         0       0         /* number of alarm messages */
  systemEvents: Byte;                   //  uchar	systemEvents;   0         0       0         /* number of fatal system events */
  siroLocks: Byte;                      //  uchar	siroLocks;      0         0       0         /* number of spindle locks because of siro startup cuts */
  diaMeanValue: Word;                   //  ushort	diaMeanValue;   0         0       0         /* diameter mean value */
  diaStrdDevi: Word;                    //  ushort	diaStrdDevi;    0         0       0         /* diameter standard devation */
  AdjustBase: Word;                     //  ushort	adjust;         0         0       0         /* adjust value */
  clusterLocks: byte;                   //  uchar	clusterLocks;   0         0       0         /* cluster Locks */
  sfiCuts: byte;                        //  uchar	sfiCuts;        0         0       0
  sfiLocks: byte;                       //  uchar	sfiLocks;       0         0       0
  fill1: byte;                          //  uchar	fill1;          0         0       0
  variCnt: word;                        //  ushort	variCnt;        0         0       0         /* number of members of "summe of varianz" */
  variTotal: CARDINAL;                  //  ulong	variTotal;      0         0       0         /* "summe of varianz" */
  siroClusterCuts: byte;                //  uchar     siroClusterCuts
  siroClusterLocks: byte;               //  uchar     siroClusterLocks
  fill2: word;                          //  ushort    fill2
  spare: array[1..9] of
  CARDINAL;                             //  ulong	spare[10];      all 0     all 0   all 0
end;                                    // total 118 Bytes

//------------------------------------------------------------------------------
// Enumerator of  TBASE_DATA: Take care to update a new record mmber here too!!
//------------------------------------------------------------------------------
  //...........................................................................

  TBaseDataEnum = (
    bdStatus,                           // 0
    bdWatchTime,
    bdOperatingTime,
    bdRunTime,
    bdLength,
    bdNepCuts,
    bdShortCuts,
    bdLongCuts,
    bdThinCuts,
    bdClusterCuts,
    bdOffCountCuts,                     // 10
    bdOffCountLocks,
    bdSiroCuts,
    bdTotalYarnCuts,
    bdSystemCuts,
    bdSystemLocks,
    bdBunchCuts,
    bdDelBunchCuts,
    bdCones,
    bdCops,
    bdYarnBreaks,                     // 20
    bdSplices,
    bdSpliceCuts,
    bdSpliceRep,
    bdUpperYarnCuts,
    bdLongStops,
    bdLongStopTime,
    bdShortClustArea,
    bdLongClustArea,
    bdThinClustArea,
    bdWarnings,                         // 30
    bdAlarms,
    bdSystemEvents,
    bdSiroLocks,
    bdDiaMeanValue,
    bdDiaStrdDevi,
    bdAdjust,
    bdClusterLocks,
    bdSfiCuts,
    bdSfiLocks,
    bdVariCnt,                          // 40
    bdVariTotal,
    bdSiroClusterCuts,
    bdSiroClusterLocks,
    bdSpare1,
    bdSpare2,
    bdSpare3,
    bdSpare4,
    bdSpare5,
    bdSpare6,
    bdSpare7,                           // 50
    bdSpare8,
    bdSpare9,
    bdBaseDataSize);                     // 53


//------------------------------------------------------------------------------
const
  cYarnDefectSize = 264;
  cYarnCutDefectSize = 128;
  cSpliceDefectSize = 144;
  cSpliceCutDefectSize = 128;
  cSIRODefectSize = 90;
  cSIROCutDefectSize = 64;
//...Constant Arrays for unpacking of Cut and Uncut data........................
// The arrays defines how many bytes on which position has to be read
  cTXNYarnDefXArray: array[1..128] of byte =
    (4, 4, 2, 2, 2, 2, 1, 1,
    2, 2, 2, 2, 2, 2, 1, 1,
    2, 2, 2, 2, 2, 2, 1, 1,
    2, 2, 2, 2, 1, 1, 1, 1,
    2, 2, 2, 2, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    4, 4, 4, 4, 4, 4, 4, 2,
    2, 4, 4, 4, 4, 4, 4, 2,
    2, 4, 4, 4, 4, 4, 4, 2,
    2, 4, 4, 4, 4, 2, 2, 2,
    2, 2, 2, 4, 4, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 1, 1,
    2, 2, 2, 2, 2, 2, 1, 1,
    1, 1, 2, 2, 2, 2, 1, 1);

  cTXNSpliceDefXArray: array[1..128] of byte =
    (1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    2, 2, 2, 2, 2, 2, 2, 2,
    1, 1, 2, 2, 2, 2, 1, 1,
    1, 1, 2, 2, 2, 2, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1);

  cTXNSIRODefXArray: array[1..64] of byte =
    (4, 4, 4, 2, 2, 2, 1, 1,
    4, 2, 2, 2, 1, 1, 1, 1,
    2, 2, 2, 2, 1, 1, 1, 1,
    2, 2, 1, 1, 1, 1, 1, 1,
    2, 2, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1);

type
//------------------------------------------------------------------------------
  PTXNDefectData = ^TTXNDefectData;
  TTXNDefectData = packed record
    yarnDefect: array[1..cYarnDefectSize] of BYTE; // Class Uncutted  : 264
    cutYarnDef: array[1..cYarnCutDefectSize] of BYTE; // Class Cutted    : 128
    spliceDefect: array[1..cSpliceDefectSize] of BYTE; // Splice Uncutted : 144
    cutSpliceDef: array[1..cSpliceCutDefectSize] of BYTE; // Splice Cutted   : 128
  end;                                  //  Total          : 664 Bytes
//------------------------------------------------------------------------------
  PTXNSiroData = ^TTXNSiroData;
  TTXNSiroData = packed record
    siroDefect: array[1..cSIRODefectSize] of BYTE; // Siro Uncutted  :  90
    cutSiroDef: array[1..cSIROCutDefectSize] of BYTE; // Siro Cutted    :  64
  end;                                  // Total          : 154
//------------------------------------------------------------------------------
  PSpdTXNRec = ^TSpdTXNRec;
  TSpdTXNRec = packed record
    baseData: TBaseData;                //                : 118
    defectData: TTXNDefectData;         //                : 664
    siroData: TTXNSiroData;             //                : 154
  end;                                  // Total          : 890
//------------------------------------------------------------------------------
  PWSCDefectData = ^TWSCDefectData;
  TWSCDefectData = packed record
    yarnDefect: array[1..128] of CARDINAL; // Class Uncutted  :  512
    cutYarnDef: array[1..128] of byte;  // Class Cutted    :  128
    spliceDefect: array[1..128] of Word; // Splice Uncutted :  256
    cutSpliceDef: array[1..128] of byte; // Splice Cutted   :  128
  end;                                  // Total           : 1024
//------------------------------------------------------------------------------
  PWSCSiroData = ^TWSCSiroData;
  TWSCSiroData = packed record
    siroDefect: array[1..64] of CARDINAL; // Siro Uncutted   :  256
    cutSiroDef: array[1..64] of byte;   // Siro Cutted     :   64
  end;                                  // Total           :  320
//------------------------------------------------------------------------------
  PSpdWSCRec = ^TSpdWSCRec;
  TSpdWSCRec = packed record
    baseData: TBaseData;                //                 :  118
    defectData: TWSCDefectData;         //                 : 1024
    siroData: TWSCSiroData;             //                 :  320
  end;                                  // Total           : 1462
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
end.

