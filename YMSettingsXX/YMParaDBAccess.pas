(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMParaDBAccess.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: Clearer Settings
| Process(es)...: -
| Description...: DB access for Clearer Settings
| Info..........: -
| Develop.system: Compaq Deskpro Pentium Pro 200, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 16.09.99 | 1.00 | Mg | Datei erstellt. TSettingsDBAccess implementiert.
| 13.10.99 | 1.00 | Kr | TSettingsDBAssistant implementiert.
| 14.10.99 | 1.00 | Nue| GetProdGrpYMPara und GetMachineYMPara implementiert.
| 26.10.99 | 1.00 | Kr | TYMSettings implementiert und unnoetige aSize parameter
|          |      |    | entfernt.
| 26.10.99 | 1.00 | Nue| aSize Parameter von Integer auf Word.
| 27.10.99 | 1.00 | Nue| Neue Methoden erstellt TSettingsDBAssistant.SaveAll,
|          |      |    | TSettingsDBAccess.SaveMachineYMPara,TSettingsDBAccess.SaveProdGrpYMPara
| 14.12.99 | 1.00 | Kr | c_SpliceParams aktiviert: True: Gesonderte Spleisspruef-Einstellungen
|          |      |    | False: Spleisspruef-Einstellungen mit Standard Kanal-Einstellungen
| 05.01.00 | 1.00 | Mg | Delete eingefuegt
| 29.02.00 | 1.00 | Nue| In cSaveProdGrpYMPara das Feld c_yarncnt_unit rausgenommen, weil
|          |      |    | dieses Feld von der ZE nicht gesendet wird!
| 08.05.00 | 1.00 | Nue| Feld c_yarncnt_unit wieder eingefuegt, weil
|          |      |    | dieses Feld neu in Record eingefuegt wird! c_nr_of_threads eingefuegt.
| 08.06.00 | 1.00 |NueKr|Temporaere InopSettings eingefuegt.
| 29.08.00 | 1.01 |Nue | TSettingsDBAccess.GetMachineYMPara in Query falschen Parameter (aMachine)
|          |      |    | durch richtigen aSpindleFirst! (Bugfix!)
| 30.10.00 | 1.02 |Nue | TSettingsDBAccess.GetMachineYMConfig erstellt mit zugehoerigen Queries
| 10.01.01 | 1.03 |Kr  | TYMSettings.EstablishDBAccess and TYMSettings.InsertScreenToSettings added
| 22.01.01 | 1.04 |Kr  | TYMSettings moved to YMParaAccessFor GUI in order to reach independence
| 13.03.01 | 1.05 |Kr,Nue| Extract...-Routines called from YMParaUtils and not from inside this modul
| 25.03.01 | 1.07 | Nue| Col. c_efficiency removed from cQueryMachInfoTot
| 16.03.01 | 1.08 | Nue| Col. c_slip added in cQueryMachInfoTot
| 17.05.01 | 1.09 | Nue| Col. c_floor_id and c_adapt_or_name deleted in cQueryMachInfoTot
| 15.08.01 | 1.10 | Nue| Handling of t_ym_settings.c_head_class added.
| 17.09.01 | 1.11 | Nue| Handling of YM_set_name added.
| 25.09.01 | 1.12 | Nue| aSize set (like in earlier versions!!!) in TSettingsDBAssistant.Get.
| 02.10.01 | 1.13 | Nue| Handling of YM_set_name modified in TSettingsDBAccess.Save and CheckAvailabilityOfSettings.
| 28.02.02 | 1.14 | Nue| Changings because of yarnCnt-Handling.
| 08.07.02 | 1.15 | Nue| RollbackTransaction in comment in TSettingsDBAccess.Save
| 24.07.02 | 1.16 | Nue| In TSettingsDBAccess.Save handling that cX1 is always added correctly
| 20.09.02         LOK | Umbau ADO
| 22.01.03 | 1.17 | Nue| m.c_last_confirmed_upload in cQueryMachInfoTot added.
| 11.02.03 | 1.18 | Nue| In GetMachineYMConfig changed access to logical machines for EASY.
| 15.12.03 | 1.19 | Nue| cUpdateYMSetName von implementation nach interface.
| 26.04.04 | 1.20 | Nue| Bei raise Exception.create(Error.msg) ersetzt durch raise Exception.create(<Wirkliche Msg>),
|                      | weil Error.msg jeweils nur ein shortstring ist und somit lange Meldungen abgeschnitten wurden.
|=============================================================================*)
unit YMParaDBAccess;

interface

uses BaseGlobal, AdoDBAccess, MMUGlobal, YMParaUtils, YMParaDef,
LoepfeGlobal;

const
  cUpdateYMSetName =      //Nue:17.9.01
    'update t_ym_settings set c_YM_set_name = :c_YM_set_name ' +
    'where c_ym_set_id = :c_ym_set_id';
//..............................................................................

type
//..............................................................................
  TChar64Arr = array[0..63] of char;
//..............................................................................
  TChar128Arr = array[0..127] of char;
//..............................................................................
  (* Allgemeines zur Klasse
     - Bei allen Fehlern wird eine Exception ausgeloest.
     - Die Fehlermeldungen koennen in Exception.message oder
       im Error Property ausgelesen werden.
     - Das im Konstructor uebergebene Query wird direkt verwendet
       ( es wird keine Kopie angelegt, d.h. alle im Query enthaltenen Daten
         werden geloescht )
  *)
  TSettingsDBAccess = class(TObject)
  private
    mDB: TAdoDBAccess;
    mQuery: TNativeAdoQuery;
    mYMSettingsUtils: TYMSettingsUtils;
    fError: TErrorRec;
    function CheckAvailabilityOfSetID(aSetID: Integer): boolean; // prueft das Vorhandensein der aSetID
    function CheckAvailabilityOfSettings(aSettings: TYMSettingsRec; aYM_Set_Name: TText50; var aTemplate: Boolean; var aSetID: Integer): boolean; // ueberprueft, ob schon ein gleiches Set wie aSettings auf DB vorhanden
    function InsertNewRecord(aSettings: TYMSettingsRec; aName: string; aTemplate: boolean): Integer; // neuer Eintrag in DB

    procedure BeginTransaction;
    procedure CommitTransaction;
    procedure InitSettingsByQuery(var aSettings: TYMExtensionRec); // kopiert die Settings vom Query in aSettings
    procedure InitQueryBySettings(aSettings: TYMSettingsRec; aForChecking: Boolean=False); // kopiert die aSettings in mQuery
    procedure RollbackTransaction;

  public
    constructor Create(aQuery: TNativeAdoQuery); overload;
    constructor Create; overload;
    destructor Destroy; override;

    function GetProdGrpYMPara(aProdGrpID: Longint): TProdGrpYMParaRec;
       //(Nue)Holt den Teil der YMPara, welche in DB-Tabelle t_prodgroup abgelegt sind
    procedure SaveProdGrpYMPara(aProdGrpID: Longint; aProdGrpPara: TProdGrpYMParaRec);
       //(Nue)Speichert den Teil der YMPara, welche in DB-Tabelle t_prodgroup abgelegt sind
    function GetMachineYMConfig(aMachineID: Word): TMachineYMConfigRec;
       //(Nue)Fuellt die Felder von TMachineYMConfigRec, welche in DB-Tabellen t_machine, t_machine_type und t_spindle abgelegt sind.
 {Delete 25.1.01 Nue
    function GetMachineYMPara(aMachineID: Word; aSpindleFirst: Byte): TMachineYMParaRec;
       //(Nue)Holt den Teil der YMPara, welche in DB-Tabelle t_machine abgelegt sind
    procedure SaveMachineYMPara(aMachineID: Word; aSpindleFirst, aSpindleLast: Byte; aMachinePara: TMachineYMParaRec);
      //(Nue)Speichert den Teil der YMPara, welche in DB-Tabelle t_machine abgelegt sind
{}
//    procedure SaveMachineYMConfig(aMachineID: Word; aSpindleFirst, aSpindleLast: Byte; aMachineConfig: TMachineYMConfigRec);
      //(Nue)Speichert den Teil der YMPara, welche in DB-Tabelle t_machine abgelegt sind
    function New(aSettings: TYMSettingsRec; aName: string): Integer; overload;
    function New(aSettings: TYMSettingsRec): Integer; overload;
    (* - Die function New legt einen neuen Settings Record
         auf der DB an. Dabei wird nicht nach schon bestehenden
         gleichen Settings gesucht. New macht immer einen Insert.
       - New gibt die ID des neuen Settings Records zurueck.
       - Im Falle einer Exception darf der Rueckgabewert nicht weiter-
         verwendet werden.
    *)
    function NewTemplate(aSettings: TYMSettingsRec; aName: string): Integer;
    (* - Die function NewTemplate legt einen neuen Settings Record
         auf der DB an, das Flag template erhaelt den wert true.
         Dabei wird nicht nach schon bestehenden
         gleichen Settings gesucht. NewTemplate macht immer einen Insert.
       - New gibt die ID des neuen Settings Records zurueck.
       - Im Falle einer Exception darf der Rueckgabewert nicht weiter-
         verwendet werden.
    *)
    function Save(aSettings: TYMSettingsRec; aYM_Set_Name: TText50; aYM_Set_Changed: Boolean (*Nue:13.9.01*)): Integer; overload;
    (* - Speichert, falls noch nicht vorhanden, aSettings auf DB
         und gibt ID des neuen Datensatzes zurueck.
       - Falls aSettings bereiz auf DB vorhanden, wird die ID des
         bestehenden Datensatzes zurueckgegeben.
    *)

    procedure Get(aSetID: Integer; var aSettings: TYMExtensionRec); overload;
    (* - Die Procedur Get sucht den Record mit aSetID auf der DB und
         schreibt ihn in aSettings.
       - Werden keine Settings mit aSetID auf der DB gefunden,
         wird eine Exception ausgegeben.
    *)
    procedure Update(aSetID: Integer; aSettings: TYMSettingsRec);
    (* - Die Procedur Update ueberschreibt den Settings Record mit aSetID
         auf der DB mit den Werten in aSettings.
       - Enthaelt die DB keinen Settings Record mit aSetID, wird eine
         Exception ausgeloest.
    *)
//    procedure UpdateSpliceParams ( aSetID : Integer; aValue : boolean );
    (* - Die Procedur UpdateSpliceParams ueberschreibt den SpliceParams
         des Settings Record mit aSetID
       - Beinhaltet die DB keinen Settings Record mit aSetID, wird eine
         Exception ausgeloest.
    *)
    procedure Delete(aSetID: integer);
    (* - Loescht den mit aSetID markierten Datensatz
    *)
    property Error: TErrorRec read fError;
  end;
//..............................................................................
// To do: Um das Endian Format der jeweiligen Argumente vom Type TYMSettingsByteArr zu unterstreichen,
// soll je ein Type fuer die Formate "litle endian" und "big endian" definiert werden und die entsprechenden
// Funktionen angepasst werden. Nue und Mg benachrichtigen wenn die Aenderung in Kraft gesetzt wird.
// Kr 05.01.2000

  TSettingsDBAssistant = class(TSettingsDBAccess)
  private
    mYMSettingsUtils: TYMSettingsUtils;
    mYMMachineSettings: TYMMachineSettings;
  public
    constructor Create(aQuery: TNativeAdoQuery); overload;
    constructor Create; overload;
    destructor Destroy; override;

    function Save(var aSettings: TYMSettingsByteArr; aYM_Set_Name: TText50; aYM_Set_Changed: Boolean (*Nue:13.9.01*)): Integer; overload;
    (* - Konvertiert aSettings in den TYMSettingsRec und uebergibt ihn der
         Funktion TSettingsDBAccess.Save.
       - Allfaellige Exceptions werden weitergeleited
    *)

    function SaveAll(var aSettings: TYMSettingsByteArr; aProdGrpId: Integer;
      aMachineID: Word; aSpindleFirst, aSpindleLast: Byte;
      aYM_Set_Name: TText50; aYM_Set_Changed: Boolean;  //Nue:13.9.01
      aMachineUpdate: Boolean): Integer;
    (* (Nue) Speichert alle Daten in aSettings auf DB (t_ym_settings, t_prodgroup, t_machine) *)

    procedure AddMachineYMConfig(var aSettings: TYMSettingsByteArr;
      aMachineYMConfig: TMachineYMConfigRec);
    (* - Fuegt Machinen-Abhaengige Parameter von aProdGrpYmPara in aSettings ein.
    *)
// to clean    procedure AddMachineYMPara(var aSettings: TYMSettingsByteArr;
//      aMachineYMPara: TMachineYMParaRec);
    (* - Fuegt Machinen-Abhaengige Parameter von aProdGrpYmPara in aSettings ein.
    *)
    procedure AddProdGrpYMPara(var aSettings: TYMSettingsByteArr;
      aProdGrpYMPara: TProdGrpYMParaRec;
      aAssignment: Byte
                            // cAssigGroup: Assign settings and its prodGrpID to the Maschinegroup
                           // cAssigMemory:	Assign settings to a memory (not yet used)
                            // cAssigProdGrpIDOnly: Assign only the prodGrpID to the Maschinegroup
      );
    (* - Fuegt P1roduktionsgruppen-Abhaengige Parameter von aProdGrpYmPara in
         aSettings ein.
    *)
    procedure ExtractDataRules(var aSettings: TYMSettingsByteArr;
      var aRules: TYMDataRulesRec);
    (* - Extrahiert Data Rules aus aSettings und uebergibt sie an aRules.
    *)
{Deleted 25.01.01 Nue
    procedure ExtractMachineYMPara(var aSettings: TYMSettingsByteArr;
      var aMachineYMPara: TMachineYMParaRec);
    (* - Extrahiert Maschinen-Abhaengige Parameter aus aSettings und
         uebergibt sie an aMachineYmPara.
    *)
{}
{Kr,Nue 13.03.2001    function ExtractMachineYMConfig(var aSettings: TSettingsArr): TMachineYMConfigRec;
    (* - Extrahiert Maschinen-Abhaengige Parameter aus aSettings und
         uebergibt sie an aMachineYmPara.
    *)
{}
{Kr,Nue 13.03.2001    procedure ExtractProdGrpYMPara(var aSettings: TYMSettingsByteArr;
      var aProdGrpYMPara: TProdGrpYMParaRec);
    (* - Extrahiert Produktionsgruppen-Abhaengige Parameter aus aSettings und
         uebergibt sie an aProdGrpYmPara.
    *)
{}
    procedure Get(aSetID: Integer; var aSettings: TYMSettingsByteArr; var aSize: Word); overload;
    (* - Der Setting Record wird von TSettingsDBAccess.Get von der DB geholt
         und in aSetting konvertiert.
       - Allfaellige Exceptions werden weitergeleited
    *)
  end;
//..............................................................................

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
 Windows, sysutils, mmCS;  // Unit fuer CodeSite

var
  lTSettingsDBAccessCount:integer = 0;

const
//..............................................................................
  cCutFlag = 'C';
  cUncutFlag = 'U';
//..............................................................................
  cCheckAvailabilityOfSetID =
    'select c_YM_set_id from t_ym_settings where c_YM_set_id = :c_YM_set_id';
//..............................................................................
  cGetSettings =
    'select * from t_YM_settings where c_YM_set_id = :c_YM_set_id';
//------------------------------------------------------------------------------
  //Gleiches Query wie in AssignComp.cQueryMachInfo
  cQueryMachInfoTot =
    'select m.c_node_id, m.c_maoffset_id, m.c_starting, m.c_data_collection, ' +
    'm.c_online_speed, m.c_ls_in_prod, m.c_ls_break, m.c_ls_wait_mat, m.c_ls_wait_rev, ' +
    'm.c_ls_clean, m.c_ls_free, m.c_ls_define, m.c_spindle_pos, m.c_longStopDef, m.c_cutRetries, ' +
    'm.c_checkLen, m.c_inopSettings0, m.c_inopSettings1, m.c_optionCode, m.c_YM_version, m.c_AWE_mach_type, ' +
    'mt.c_machine_manuf, mt.c_machine_model, mt.c_spind_pos, mt.c_machine_type, mt.c_default_net_id, ' +
    'm.c_machine_name, m.c_nr_of_spindles, m.c_net_id, ' +
    'm.c_speedRamp, m.c_fixSpindlerange, m.c_slip, nn.c_front_type, ' +
    'm.c_opt_speed, m.c_configA, m.c_configB, m.c_configC, ' +
    'm.c_inopsettings0, m.c_inopsettings1, m.c_last_confirmed_upload ' +
    'from t_machine m, t_net_node nn, t_machine_type mt ' +
    'where m.c_node_id = nn.c_node_id ' +
    'and c_machine_id =:c_machine_id ' +
    'and m.c_machine_type = mt.c_machine_type ';
//Comment Nue:11.2.03------------------------------------------------------------------------------
//  cQryGetLogMachines =
//    'select c_head_type, c_configA, c_configB, c_configC, c_inopSettings0, c_inopSettings1, c_AWE_type, ' +
//    'spindFirst=MIN(c_spindle_id), spindLast=MAX(c_spindle_id) from t_spindle ' +
//    'where c_machine_id=:c_machine_id ' +
//    'group by c_head_type, c_configA, c_configB, c_configC, c_inopSettings0, c_inopSettings1, c_AWE_type '+
//    'order by spindFirst ';
//..............................................................................
  cQryGetLogMachinesRanges =
    'select c_spindle_id, c_head_type, c_configA, c_configB, c_configC, c_inopSettings0, c_inopSettings1, c_AWE_type ' +
    'from t_spindle ' +
    'where c_machine_id=:c_machine_id ' +
    'order by c_spindle_id ';
//..............................................................................
  cGetMachineYMPara =                   //Nue
    'select s.c_head_type,m.c_machine_type,m.c_checkLen,m.c_cutRetries,' +
    'm.c_configA,m.c_configB,m.c_configC,m.c_machine_name,m.c_longStopDef,' +
    'n.c_front_type,m.c_inopSettings0,m.c_inopSettings1,m.c_YM_version ' +
    'from t_machine m, t_spindle s, t_net_node n ' +
    'where (m.c_node_id=n.c_node_id) and (m.c_machine_id=s.c_machine_id) and ' +
    '(s.c_spindle_id=:c_spindle_id) and (m.c_machine_id = :c_machine_id)';
//..............................................................................
  cSaveMachineYMPara =                  //Nue
    'update t_machine set c_machine_type=:c_machine_type, c_checkLen=:c_checkLen, ' +
    'c_cutRetries=:c_cutRetries, c_configA=:c_configA, c_configB=:c_configB, c_configC=:c_configC, ' +
    'c_machine_name=:c_machine_name, c_longStopDef=:c_longStopDef, ' +
    'c_inopSettings0=:c_inopSettings0,c_inopSettings1=:c_inopSettings1,' +
    'c_YM_version=:c_YM_version ' +
    'where c_machine_id=:c_machine_id ' +
    'update t_spindle set c_head_type=:c_head_type ' +
    'where (c_machine_id=:c_machine_id) and (c_spindle_id>=:spindle_first) and (c_spindle_id<=:spindle_last) ' +
    'update t_net_node set c_front_type=:c_front_type ' +
    'where c_node_id=(select c_node_id from t_machine where (c_machine_id=:c_machine_id)) ';
//..............................................................................
  cGetProdGrpYMPara =                   //Nue
    'select * from t_prodgroup where c_prod_id = :c_prod_id';
//..............................................................................
  cSaveProdGrpYMPara =                  //Nue
    'update t_prodgroup set c_machineGroup=:c_machineGroup, c_spindle_first=:c_spindle_first, ' +
    'c_spindle_last=:c_spindle_last, c_pilotSpindles=:c_pilotSpindles, c_speedRamp=:c_speedRamp, ' +
    'c_speed=:c_speed, c_act_yarncnt=:c_act_yarncnt, c_yarncnt_unit=:c_yarncnt_unit, ' +
    'c_nr_of_threads=:c_nr_of_threads, c_lengthWindow=:c_lengthWindow, c_lengthMode=:c_lengthMode ' +
    'where c_prod_id = :c_prod_id';
//..............................................................................
  cInsertSettings =
    'insert t_ym_settings ( ' +
    'c_YM_set_id, c_YM_set_name, c_template_set, c_spliceParams, c_DN, ' +
    'c_switchNeps, c_DS, c_switchShort, c_LS, c_DL, c_switchLong, ' +
    'c_LL, c_DT, c_switchThin, c_LT, c_SDN, c_SswitchNeps, c_SDS, c_SswitchShort, ' +
    'c_SLS, c_SDL, c_SswitchLong, c_SLL, c_SDT, c_SswitchThin, c_SLT, c_UpY, ' +
    'c_switchUpY, c_classClear, c_siroClear, c_configA, c_configB, c_configC, ' +
    'c_fAdjConeReduction, c_DiaDiff, c_CntRep, c_siroStartupRep, c_ClDia, ' +
    'c_ClLen, c_ClDef, c_negDiaDiff, c_CntLen, c_ClRep, c_SFI_absref, c_SFI_upper, ' +
    'c_SFI_lower, c_SFI_rep, c_inopSettings0, c_inopSettings1, c_FFClLen, c_FFClDef, ' +
    'c_FFClRep, c_FFClSiroClear, c_spare1, c_spare2, c_spare3, c_spare4, c_spare5, c_spare6, ' +
    'c_head_class ) '+   //Nue:15.08.01
    'values ( ' +
    ':c_YM_set_id, :c_YM_set_name, :c_template_set, :c_spliceParams, :c_DN, ' +
    ':c_switchNeps, :c_DS, :c_switchShort, :c_LS, :c_DL, :c_switchLong, ' +
    ':c_LL, :c_DT, :c_switchThin, :c_LT, :c_SDN, :c_SswitchNeps, :c_SDS, ' +
    ':c_SswitchShort, :c_SLS, :c_SDL, :c_SswitchLong, :c_SLL, :c_SDT, ' +
    ':c_SswitchThin, :c_SLT, :c_UpY, :c_switchUpY, :c_classClear, :c_siroClear, ' +
    ':c_configA, :c_configB, :c_configC, :c_fAdjConeReduction, :c_DiaDiff, ' +
    ':c_CntRep, :c_siroStartupRep, :c_ClDia, :c_ClLen, :c_ClDef, :c_negDiaDiff, ' +
    ':c_CntLen, :c_ClRep, :c_SFI_absref, :c_SFI_upper, ' +
    ':c_SFI_lower, :c_SFI_rep, :c_inopSettings0, :c_inopSettings1, :c_FFClLen, ' +
    ':c_FFClDef, :c_FFClRep, :c_FFClSiroClear, :c_spare1, :c_spare2, :c_spare3, ' +
    ':c_spare4, :c_spare5, :c_spare6, :c_head_class )';
//..............................................................................
  cUpdateSpliceParams =
    'update t_ym_settings set c_spliceParams = :c_spliceParams ' +
    'where c_ym_set_id = :c_ym_set_id';
//..............................................................................
  cUpdateSettings =
    'update t_ym_settings set ' +
    'c_spliceParams  = :c_spliceParams, ' +
    'c_DN            = :c_DN, ' +
    'c_switchNeps    = :c_switchNeps, ' +
    'c_DS            = :c_DS, ' +
    'c_switchShort   = :c_switchShort, ' +
    'c_LS            = :c_LS, ' +
    'c_DL            = :c_DL, ' +
    'c_switchLong    = :c_switchLong, ' +
    'c_LL            = :c_LL, ' +
    'c_DT            = :c_DT, ' +
    'c_switchThin    = :c_switchThin, ' +
    'c_LT            = :c_LT, ' +
    'c_SDN           = :c_SDN, ' +
    'c_SswitchNeps   = :c_SswitchNeps, ' +
    'c_SDS           = :c_SDS, ' +
    'c_SswitchShort  = :c_SswitchShort, ' +
    'c_SLS           = :c_SLS, ' +
    'c_SDL           = :c_SDL, ' +
    'c_SswitchLong   = :c_SswitchLong, ' +
    'c_SLL           = :c_SLL, ' +
    'c_SDT           = :c_SDT, ' +
    'c_SswitchThin   = :c_SswitchThin, ' +
    'c_SLT           = :c_SLT, ' +
    'c_UpY           = :c_UpY, ' +
    'c_switchUpY     = :c_switchUpY, ' +
    'c_classClear    = :c_classClear, ' +
    'c_siroClear     = :c_siroClear, ' +
    'c_configA       = :c_configA, ' +
    'c_configB       = :c_configB, ' +
    'c_configC       = :c_configC, ' +
    'c_fAdjConeReduction = :c_fAdjConeReduction, ' +
    'c_DiaDiff       = :c_DiaDiff, ' +
    'c_CntRep        = :c_CntRep, ' +
    'c_siroStartupRep = :c_siroStartupRep, ' +
    'c_ClDia         = :c_ClDia, ' +
    'c_ClLen         = :c_ClLen, ' +
    'c_ClDef         = :c_ClDef, ' +
    'c_negDiaDiff    = :c_negDiaDiff, ' +
    'c_CntLen        = :c_CntLen, ' +
    'c_ClRep         = :c_ClRep, ' +
    'c_SFI_absref    = :c_SFI_absref, ' +
    'c_SFI_upper     = :c_SFI_upper, ' +
    'c_SFI_lower     = :c_SFI_lower, ' +
    'c_SFI_rep       = :c_SFI_rep, ' +
    'c_inopSettings0 = :c_inopSettings0, ' +
    'c_inopSettings1 = :c_inopSettings1, ' +
    'c_FFClLen       = :c_FFClLen, ' +
    'c_FFClDef       = :c_FFClDef, ' +
    'c_FFClRep       = :c_FFClRep, ' +
    'c_FFClSiroClear = :c_FFClSiroClear, ' +
    'c_spare1        = :c_spare1, ' +
    'c_spare2        = :c_spare2, ' +
    'c_spare3        = :c_spare3, ' +
    'c_spare4        = :c_spare4, ' +
    'c_spare5        = :c_spare5, ' +
    'c_spare6        = :c_spare6, ' +
    'c_head_class    = :c_head_class '+   //Nue:15.08.01
    'where c_ym_set_id = :c_ym_set_id';
//..............................................................................
{Alt bis 17.9.01
  cCheckAvailabilityOfSettings =
    'select c_ym_set_id from t_ym_settings ' +
    'where c_template_set <> 1 and ' +
    'c_spliceParams  = :c_spliceParams and  ' +
    'c_DN            = :c_DN and  ' +
    'c_switchNeps    = :c_switchNeps and  ' +
    'c_DS            = :c_DS and  ' +
    'c_switchShort   = :c_switchShort and  ' +
    'c_LS            = :c_LS and  ' +
    'c_DL            = :c_DL and  ' +
    'c_switchLong    = :c_switchLong and  ' +
    'c_LL            = :c_LL and  ' +
    'c_DT            = :c_DT and  ' +
    'c_switchThin    = :c_switchThin and  ' +
    'c_LT            = :c_LT and  ' +
    'c_SDN           = :c_SDN and  ' +
    'c_SswitchNeps   = :c_SswitchNeps and  ' +
    'c_SDS           = :c_SDS and  ' +
    'c_SswitchShort  = :c_SswitchShort and  ' +
    'c_SLS           = :c_SLS and  ' +
    'c_SDL           = :c_SDL and  ' +
    'c_SswitchLong   = :c_SswitchLong and  ' +
    'c_SLL           = :c_SLL and  ' +
    'c_SDT           = :c_SDT and  ' +
    'c_SswitchThin   = :c_SswitchThin and  ' +
    'c_SLT           = :c_SLT and  ' +
    'c_UpY           = :c_UpY and  ' +
    'c_switchUpY     = :c_switchUpY and  ' +
    'c_classClear    = :c_classClear and  ' +
    'c_siroClear     = :c_siroClear and  ' +
    'c_configA       = :c_configA and  ' +
    'c_configB       = :c_configB and  ' +
    'c_configC       = :c_configC and  ' +
    'c_fAdjConeReduction = :c_fAdjConeReduction and  ' +
    'c_DiaDiff       = :c_DiaDiff and  ' +
    'c_CntRep        = :c_CntRep and  ' +
    'c_siroStartupRep = :c_siroStartupRep and  ' +
    'c_ClDia         = :c_ClDia and  ' +
    'c_ClLen         = :c_ClLen and  ' +
    'c_ClDef         = :c_ClDef and  ' +
    'c_negDiaDiff    = :c_negDiaDiff and  ' +
    'c_CntLen        = :c_CntLen and  ' +
    'c_ClRep         = :c_ClRep and  ' +
    'c_SFI_absref    = :c_SFI_absref and  ' +
    'c_SFI_upper     = :c_SFI_upper and  ' +
    'c_SFI_lower     = :c_SFI_lower and  ' +
    'c_SFI_rep       = :c_SFI_rep and  ' +
    'c_inopSettings0 = :c_inopSettings0 and  ' +
    'c_inopSettings1 = :c_inopSettings1 and  ' +
    'c_FFClLen       = :c_FFClLen and  ' +
    'c_FFClDef       = :c_FFClDef and  ' +
    'c_FFClRep       = :c_FFClRep and  ' +
    'c_FFClSiroClear = :c_FFClSiroClear and  ' +
    'c_spare1        = :c_spare1 and  ' +
    'c_spare2        = :c_spare2 and  ' +
    'c_spare3        = :c_spare3 and  ' +
    'c_spare4        = :c_spare4 and  ' +
    'c_spare5        = :c_spare5 and  ' +
    'c_spare6        = :c_spare6 and  '+
    'c_head_class    = :c_head_class ';   //Nue:15.08.01
{}
//..............................................................................
//Nue:17.9.01
  cCheckAvailabilityOfSettings =
    'select c_ym_set_id,c_ym_set_name,c_template_set,c_configA,c_configB,c_configC,c_inopSettings0,c_inopSettings1 from t_ym_settings ' +
    'where ' +
//Nue:25.9.01    c_template_set <> 1 and ' +
    'c_spliceParams  = :c_spliceParams and  ' +
    'c_DN            = :c_DN and  ' +
    'c_switchNeps    = :c_switchNeps and  ' +
    'c_DS            = :c_DS and  ' +
    'c_switchShort   = :c_switchShort and  ' +
    'c_LS            = :c_LS and  ' +
    'c_DL            = :c_DL and  ' +
    'c_switchLong    = :c_switchLong and  ' +
    'c_LL            = :c_LL and  ' +
    'c_DT            = :c_DT and  ' +
    'c_switchThin    = :c_switchThin and  ' +
    'c_LT            = :c_LT and  ' +
    'c_SDN           = :c_SDN and  ' +
    'c_SswitchNeps   = :c_SswitchNeps and  ' +
    'c_SDS           = :c_SDS and  ' +
    'c_SswitchShort  = :c_SswitchShort and  ' +
    'c_SLS           = :c_SLS and  ' +
    'c_SDL           = :c_SDL and  ' +
    'c_SswitchLong   = :c_SswitchLong and  ' +
    'c_SLL           = :c_SLL and  ' +
    'c_SDT           = :c_SDT and  ' +
    'c_SswitchThin   = :c_SswitchThin and  ' +
    'c_SLT           = :c_SLT and  ' +
    'c_UpY           = :c_UpY and  ' +
    'c_switchUpY     = :c_switchUpY and  ' +
    'c_classClear    = :c_classClear and  ' +
    'c_siroClear     = :c_siroClear and  ' +
//    'c_configA       = :c_configA and  ' +
//    'c_configB       = :c_configB and  ' +
//    'c_configC       = :c_configC and  ' +
    'c_fAdjConeReduction = :c_fAdjConeReduction and  ' +
    'c_DiaDiff       = :c_DiaDiff and  ' +
    'c_CntRep        = :c_CntRep and  ' +
    'c_siroStartupRep = :c_siroStartupRep and  ' +
    'c_ClDia         = :c_ClDia and  ' +
    'c_ClLen         = :c_ClLen and  ' +
    'c_ClDef         = :c_ClDef and  ' +
    'c_negDiaDiff    = :c_negDiaDiff and  ' +
    'c_CntLen        = :c_CntLen and  ' +
    'c_ClRep         = :c_ClRep and  ' +
    'c_SFI_absref    = :c_SFI_absref and  ' +
    'c_SFI_upper     = :c_SFI_upper and  ' +
    'c_SFI_lower     = :c_SFI_lower and  ' +
    'c_SFI_rep       = :c_SFI_rep and  ' +
//    'c_inopSettings0 = :c_inopSettings0 and  ' +
//    'c_inopSettings1 = :c_inopSettings1 and  ' +
    'c_FFClLen       = :c_FFClLen and  ' +
    'c_FFClDef       = :c_FFClDef and  ' +
    'c_FFClRep       = :c_FFClRep and  ' +
    'c_FFClSiroClear = :c_FFClSiroClear and  ' +
//    'c_spare1        = :c_spare1 and  ' +
//    'c_spare2        = :c_spare2 and  ' +
//    'c_spare3        = :c_spare3 and  ' +
//    'c_spare4        = :c_spare4 and  ' +
//    'c_spare5        = :c_spare5 and  ' +
//    'c_spare6        = :c_spare6 and  '+
    'c_head_class    = :c_head_class ';   //Nue:15.08.01
  //..............................................................................
  cDeleteSetting =
    'delete from t_ym_settings ' +
    'where c_ym_set_id = :c_ym_set_id';

//******************************************************************************
// TSettingsDBAccess
//******************************************************************************
//------------------------------------------------------------------------------

procedure TSettingsDBAccess.BeginTransaction;
begin
  try
    mQuery.Connection.BeginTrans;
  except
    on e: Exception do
    begin
      raise Exception.Create('BeginTransaction failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

function TSettingsDBAccess.CheckAvailabilityOfSetID(aSetID: Integer): boolean;
begin
  Result := false;
  try
    with mQuery do
    begin
      SQL.Text := cCheckAvailabilityOfSetID;
      ParamByName('c_YM_set_id').AsInteger := aSetID;
      Open;
      Result := FindFirst;
      Close;
    end;
  except
    on e: Exception do
    begin
      raise Exception.create('CheckAvailabilityOfSetID failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

function TSettingsDBAccess.CheckAvailabilityOfSettings(aSettings: TYMSettingsRec; aYM_Set_Name: TText50; var aTemplate: Boolean; var aSetID: Integer): boolean;
var
  xStr: string;
  xEqualSetFound: Boolean;
begin
EnterMethod('TSettingsDBAccess.CheckAvailabilityOfSettings');
  xStr := StrPas(@aYM_Set_Name);
  Result := false;
  aSetID := 0;
  aTemplate := False;
  xEqualSetFound := False;
  try
    with mQuery do
    begin
      SQL.Text := cCheckAvailabilityOfSettings;
      InitQueryBySettings(aSettings, True);
      Open;
{Nue:17.9.01
      if FindFirst then
      begin
        Result := true;
        aSetID := FieldByName('c_ym_set_id').AsInteger;
      end;
{}
{Nue:2.10.01
      while (not EOF) and (not Result) do begin
        if TConfigurationCode.CompatibleConfigCodes(FieldByName('c_configA').AsInteger, FieldByName('c_configB').AsInteger, FieldByName('c_configC').AsInteger,
             aSettings.additional.configA, aSettings.additional.configB, aSettings.additional.configC) AND
           TMachineAttributes.CompatibleInopSettings(FieldByName('c_inopSettings0').AsInteger, FieldByName('c_inopSettings1').AsInteger,
             aSettings.available.inoperativePara[0], aSettings.available.inoperativePara[1]) then begin
          codesite.SendFmtMsg('TSettingsDBAccess.CheckAvailabilityOfSettings: YM set for setname:%s found: Mastername:%s, SetID=%d ',
           [xStr, FieldByName('c_ym_set_name').AsString, FieldByName('c_ym_set_id').AsInteger]);
          xEqualSetFound := True;
          if xStr=FieldByName('c_ym_set_name').AsString then begin
            codesite.SendFmtMsg('TSettingsDBAccess.CheckAvailabilityOfSettings: Identical YM set for setname:%s found: Mastername:%s, SetID=%d ',
             [xStr, FieldByName('c_ym_set_name').AsString, FieldByName('c_ym_set_id').AsInteger]);
            Result := true;
          end
          else if Pos(xStr,FieldByName('c_ym_set_name').AsString)<>0 then begin
            codesite.SendFmtMsg('TSettingsDBAccess.CheckAvailabilityOfSettings: Equal (same basename;new in existing) YM set for setname:%s found: Mastername:%s, SetID=%d ',
             [xStr, FieldByName('c_ym_set_name').AsString, FieldByName('c_ym_set_id').AsInteger]);
            Result := true;
          end
          else if Pos(FieldByName('c_ym_set_name').AsString, xStr)<>0 then begin
            codesite.SendFmtMsg('TSettingsDBAccess.CheckAvailabilityOfSettings: Equal (same basename;existing in new) YM set for setname:%s found: Mastername:%s, SetID=%d ',
             [xStr, FieldByName('c_ym_set_name').AsString, FieldByName('c_ym_set_id').AsInteger]);
            Result := true;
          end
          else if sizeOf(xStr)=0 then begin
            codesite.SendFmtMsg('TSettingsDBAccess.CheckAvailabilityOfSettings: Equal (NOT same basename) YM set for setname:%s found: Mastername:%s, SetID=%d ',
             [xStr, FieldByName('c_ym_set_name').AsString, FieldByName('c_ym_set_id').AsInteger]);
          end;
          aTemplate := (FieldByName('c_template_set').AsBoolean);
          aSetID := FieldByName('c_ym_set_id').AsInteger;
        end;
        Next;
      end;
{}
      while (not EOF) and (not Result) do begin
        if TConfigurationCode.CompatibleConfigCodes(FieldByName('c_configA').AsInteger, FieldByName('c_configB').AsInteger, FieldByName('c_configC').AsInteger,
                                                    aSettings.additional.configA, aSettings.additional.configB, aSettings.additional.configC) AND
           TMachineAttributes.CompatibleInopSettings(FieldByName('c_inopSettings0').AsInteger, FieldByName('c_inopSettings1').AsInteger,
                                                     aSettings.available.inoperativePara[0], aSettings.available.inoperativePara[1]) then
        begin
          codesite.SendFmtMsg('TSettingsDBAccess.CheckAvailabilityOfSettings: YM set for setname:%s found: Mastername:%s, SetID=%d ', [xStr, FieldByName('c_ym_set_name').AsString, FieldByName('c_ym_set_id').AsInteger]);
          if xStr=FieldByName('c_ym_set_name').AsString then begin
            codesite.SendFmtMsg('TSettingsDBAccess.CheckAvailabilityOfSettings: Identical YM set for setname:%s found: Mastername:%s, SetID=%d ', [xStr, FieldByName('c_ym_set_name').AsString, FieldByName('c_ym_set_id').AsInteger]);
            aTemplate := (FieldByName('c_template_set').AsBoolean);
            aSetID := FieldByName('c_ym_set_id').AsInteger;
            if NOT aTemplate then
              Result := true;
          end
        end;
        Next;
      end;

//      if xEqualSetFound then
//        Result := True;

    end;
  except
    on e: Exception do
    begin
codesite.SendMsg('EXEPCTION:TSettingsDBAccess.CheckAvailabilityOfSettings');
      raise Exception.create('CheckAvailabilityOfSettings failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TSettingsDBAccess.CommitTransaction;
begin
  try
    mQuery.Connection.CommitTrans;
  except
    on e: Exception do
    begin
      raise Exception.Create('CommitTransaction failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

constructor TSettingsDBAccess.Create;
begin
  inherited Create;
  mYMSettingsUtils := TYMSettingsUtils.Create;
  mDB := TAdoDBAccess.Create(1, true);
  try mDB.Init except end;
  mQuery := mDB.Query[cPrimaryQuery];
end;
//------------------------------------------------------------------------------

constructor TSettingsDBAccess.Create(aQuery: TNativeAdoQuery);
begin
  inherited Create;
  mYMSettingsUtils := TYMSettingsUtils.Create;
  mQuery := aQuery;
  mDB := nil;
  try mQuery.Close except end;
end;
//------------------------------------------------------------------------------

procedure TSettingsDBAccess.Delete(aSetID: integer);
begin
  try
    with mQuery do
    begin
      SQL.Text := cDeleteSetting;
      Params.ParamByName('c_ym_set_id').AsInteger := aSetID;
      ExecSQL;
      Close;
    end;
  except
    on e: Exception do
    begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Delete failed. ' + e.message);
      raise Exception.create('Delete failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

destructor TSettingsDBAccess.Destroy;
begin
  FreeAndNil(mYMSettingsUtils);
  if Assigned(mDB) then
    mDB.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TSettingsDBAccess.Get(aSetID: Integer; var aSettings: TYMExtensionRec);
begin
  try
    with mQuery do
    begin
      Close;
      SQL.Text := cGetSettings;
      Params.ParamByName('c_YM_set_id').AsInteger := aSetID;
      Open;
      if not FindFirst then
      begin
        fError := SetError(ERROR_INVALID_HANDLE, etMMError, Format('YM_SetID %d not available on Database.', [aSetID]));
        raise Exception.Create(Format('YM_SetID %d not available on Database.', [aSetID]));
      end;
      InitSettingsByQuery(aSettings);
      Close;
    end;
  except
    on e: Exception do
    begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Get failed. ' + e.message);
      raise Exception.create('Get failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

function TSettingsDBAccess.GetMachineYMConfig(aMachineID: Word): TMachineYMConfigRec;
var
  xStr: Shortstring;
  xCount: Word;
  xMaxSpd: Word;
  xFirst: Boolean;
begin
  FillChar(Result, sizeof(Result), 0);
  try
    if not Assigned(mQuery) then
      raise Exception.Create('Query not assigned.');
    with mQuery do
    begin
      Close;
      SQL.Text := cQueryMachInfoTot;
      Params.ParamByName('c_machine_id').AsInteger := aMachineID;
      Open;
      if EOF then
        raise Exception.Create('No Machine available with Machine ID = ' + IntToStr(aMachineID))
      else
      begin
        with Result do
        begin
          longStopDef := FieldByName('c_longStopDef').AsInteger;
          cutRetries := FieldByName('c_cutRetries').AsInteger;
          checkLen := FieldByName('c_checkLen').AsInteger;
          xStr := FieldByName('c_optionCode').AsString;
          frontSWOption := LOW(TFrontSwOption);
          for xCount := ORD(fSwOBasic) to ORD(fSwOEnd) do
          begin
            if (xStr = cFrontSwOption[TFrontSwOption(xCount)]) then
            begin
              frontSWOption := TFrontSwOption(xCount);
              break;
            end;
          end;
          StrCopy(@frontSWVersion, PChar(FieldByName('c_YM_version').AsString));
          StrCopy(@machBez, PChar(FieldByName('c_machine_name').AsString));
          FrontType := TFrontType(FieldByName('c_front_type').AsInteger);
          defaultSpeedRamp := FieldByName('c_speedRamp').AsInteger;
          defaultSpeed := FieldByName('c_opt_speed').AsInteger;
          aWEMachType := TAWEMachType(FieldByName('c_AWE_mach_type').AsInteger);
          inoperativePara[0] := FieldByName('c_inopSettings0').AsInteger;
          inoperativePara[1] := FieldByName('c_inopSettings1').AsInteger;
        end;
      end;

//      Close;
//      SQL.Text := cQryGetLogMachines;
//      Params.ParamByName('c_machine_id').AsInteger := aMachineID;
//      Open;
//      if EOF then
//        raise Exception.Create('TSettingsDBAccess.GetMachineYMConfig: No spindle available with Machine ID = ' + IntToStr(aMachineID));
//
//      xCount := 1;
//      while not EOF do
//      begin
//        with Result.spec[xCount] do
//        begin
//          Result.spec[xCount] := Result.spec[1];
//          spindle.start := FieldByName('spindFirst').AsInteger;
//          spindle.stop := FieldByName('spindLast').AsInteger;
//          sensingHead := TSensingHeadType(FieldByName('c_head_type').AsInteger);
//          configA := FieldByName('c_configA').AsInteger;
//          configB := FieldByName('c_configB').AsInteger;
//          configC := FieldByName('c_configC').AsInteger;
//          inoperativePara[0] := FieldByName('c_inopSettings0').AsInteger;
//          inoperativePara[1] := FieldByName('c_inopSettings1').AsInteger;
//          aWEType := TAWEType(FieldByName('c_AWE_type').AsInteger);
//          INC(xCount);
//          Next;
//        end;
//      end

      Close;
      SQL.Text := cQryGetLogMachinesRanges;
      Params.ParamByName('c_machine_id').AsInteger := aMachineID;
      Open;
      if EOF then
        raise Exception.Create('TSettingsDBAccess.GetMachineYMConfig: No spindle available with Machine ID = ' + IntToStr(aMachineID));

      //Zusammenstellen der logischen Maschinen (aufeinanderfolgende Spindlen mit gleichen Tastköpfen)
      xCount := 1;
      xFirst:= True;
      while not EOF do begin
        xMaxSpd := FieldByName('c_spindle_id').AsInteger;
        if xFirst then begin
          with Result.spec[xCount] do begin
//          Result.spec[xCount] := Result.spec[1];
            spindle.start := FieldByName('c_spindle_id').AsInteger;
            sensingHead := TSensingHeadType(FieldByName('c_head_type').AsInteger);
            spindle.stop := FieldByName('c_spindle_id').AsInteger;
            configA := FieldByName('c_configA').AsInteger;
            configB := FieldByName('c_configB').AsInteger;
            configC := FieldByName('c_configC').AsInteger;
            inoperativePara[0] := FieldByName('c_inopSettings0').AsInteger;
            inoperativePara[1] := FieldByName('c_inopSettings1').AsInteger;
            aWEType := TAWEType(FieldByName('c_AWE_type').AsInteger);
          end;//with
          xFirst := False;
        end
        else if (Result.spec[xCount].sensingHead <> TSensingHeadType(FieldByName('c_head_type').AsInteger)) then begin
          INC(xCount);
          with Result.spec[xCount] do begin
            spindle.start := FieldByName('c_spindle_id').AsInteger;
            sensingHead := TSensingHeadType(FieldByName('c_head_type').AsInteger);
            spindle.stop := FieldByName('c_spindle_id').AsInteger;
            configA := FieldByName('c_configA').AsInteger;
            configB := FieldByName('c_configB').AsInteger;
            configC := FieldByName('c_configC').AsInteger;
            inoperativePara[0] := FieldByName('c_inopSettings0').AsInteger;
            inoperativePara[1] := FieldByName('c_inopSettings1').AsInteger;
            aWEType := TAWEType(FieldByName('c_AWE_type').AsInteger);
          end;//with
          Result.spec[xCount-1].spindle.stop := xMaxSpd-1;
        end; //else if
        Next;
      end; //while
      if NOT xFirst then
        Result.spec[xCount].spindle.stop := xMaxSpd;

    end;
  except
    on e: Exception do
    begin
      raise Exception.Create('TSettingsDBAccess.GetMachineYMConfig failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
 {Delete 25.1.01 Nue
function TSettingsDBAccess.GetMachineYMPara(aMachineID: Word; aSpindleFirst: Byte): TMachineYMParaRec; //Holt den Teil der YMPara, welche in DB-Tabelle t_machine abgelegt sind
var
  xStr: Shortstring;
begin
  FillChar(Result, sizeof(Result), 0);
  try
    with mQuery do
    begin
      SQL.Text := cGetMachineYMPara;
      ParamByName('c_spindle_id').AsInteger := aSpindleFirst;
      ParamByName('c_machine_id').AsInteger := aMachineID;
      Open;
      if FindFirst then
      begin
        with Result do
        begin
          sensingHead := FieldByName('c_head_type').AsInteger; // TK730:0,TK740:1,TK750:2,TK770:3,TK780:4,TK930:5..
          machType := FieldByName('c_machine_type').AsInteger; // enum WINDER_TYPE
          checkLen := FieldByName('c_checkLen').AsInteger div cCheckLenFactor; // splice check Length, (0..1200), [0..120 cm]
          cutRetries := FieldByName('c_cutRetries').AsInteger; // Cut Retries, (Repetitions+1: 1..6)
          configA := FieldByName('c_configA').AsInteger;
          configB := FieldByName('c_configB').AsInteger;
          configC := FieldByName('c_configC').AsInteger;
          xStr := FieldByName('c_machine_name').AsString; // Machine name
          Move(xStr[1], machBez, Length(xStr));
          longStopDef := FieldByName('c_longStopDef').AsInteger;
          inoperativePara[0] := FieldByName('c_inopSettings0').AsInteger; // Bitset for ionoperative settings
          inoperativePara[1] := FieldByName('c_inopSettings1').AsInteger; // Bitset for ionoperative settings
          yMType := FieldByName('c_front_type').AsInteger; // yMType ....
          xStr := FieldByName('c_YM_version').AsString; // ZE Version string
          Move(xStr, yMSWVersion, Length(xStr));
        end;
      end;
      Close;
    end;
  except
    on e: Exception do
    begin
      raise Exception.create('TSettingsDBAccess.GetMachineYMPara failed. ' + e.Message);
    end;
  end;
end;
{}
//------------------------------------------------------------------------------

function TSettingsDBAccess.GetProdGrpYMPara(aProdGrpID: Longint): TProdGrpYMParaRec; //Holt den Teil der YMPara, welche in DB-Tabelle t_prodgroup abgelegt sind
begin
  FillChar(Result, sizeof(Result), 0);
  try
    with mQuery do
    begin
      SQL.Text := cGetProdGrpYMPara;
      ParamByName('c_prod_id').AsInteger := aProdGrpID;
      Open;
      if FindFirst then
      begin
        with Result do
        begin
          group := FieldByName('c_machineGroup').AsInteger; // Local Group (0..5: AC338 Informator, 0..11 ZE)
          prodGrpID := aProdGrpID;      // MillMaster's group ID, 0=not defined
          spdl.start := FieldByName('c_spindle_first').AsInteger; //Spindle first
          spdl.stop := FieldByName('c_spindle_last').AsInteger; //Spindle first
          pilotSpindles := FieldByName('c_pilotSpindles').AsInteger;
          speedRamp := FieldByName('c_speedRamp').AsInteger; // only machines with Speed Simulation
          speed := FieldByName('c_speed').AsInteger; // winding speed, (20..1600), [20..1600 m/Min]
//Old til 28.2.02 Nue          yarnCnt := FieldByName('c_act_yarncnt').AsInteger; // Yarn count [cMinYarnCount..cMinYarnCount]
          yarnCntUnit := FieldByName('c_yarncnt_unit').AsInteger; // Yarn unit in PutProdGrpYMPara
          //New 28.2.02 Nue
          yarnCnt := Round(YarnCountConvert(yuNm, TYarnUnits(yarnCntUnit), FieldByName('c_act_yarncnt').AsFloat) * cYarnCntFactor); // wss
//          yarnCnt := Round(YarnCountConvert(yuNm, TYarnUnits(yarnCntUnit), FieldByName('c_act_yarncnt').AsFloat*cYarnCntFactor));
          nrOfThreads := FieldByName('c_nr_of_threads').AsInteger; // Nr of threads in PutProdGrpYMPara
          lengthWindow := FieldByName('c_lengthWindow').AsInteger;
          lengthMode := FieldByName('c_lengthMode').AsInteger; // yMType ....
        end;
      end;
      Close;
    end;
  except
    on e: Exception do
    begin
      raise Exception.create('TSettingsDBAccess.GetProdGrpYMPara failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TSettingsDBAccess.InitQueryBySettings(aSettings: TYMSettingsRec; aForChecking: Boolean);
var x, x1: Integer;
  xField: Integer;
  xChar64: TChar64Arr;
  xChar128: TChar128Arr;

  function IsSet(aBitNr: Integer; aValue: Byte): boolean;
  begin
    if aBitNr > 7 then raise Exception.Create('Nr. of Bit out of range.');
    Result := boolean((1 shl aBitNr) and aValue);
  end;
begin
  try
    with mQuery do
    begin
        // aSettings.splice.checkLen indicates from the user if Zero then: seperate splice check settings
        // else :  NO seperate splice check settings
      ParamByName('c_spliceParams').AsBoolean := boolean(aSettings.splice.checkLen);
      ParamByName('c_DN').AsInteger := aSettings.channel.neps.dia;
      ParamByName('c_switchNeps').AsInteger := aSettings.channel.neps.sw;
      ParamByName('c_DS').AsInteger := aSettings.channel.short.dia;
      ParamByName('c_switchShort').AsInteger := aSettings.channel.short.sw;
      ParamByName('c_LS').AsInteger := aSettings.channel.shortLen;
      ParamByName('c_DL').AsInteger := aSettings.channel.long.dia;
      ParamByName('c_switchLong').AsInteger := aSettings.channel.long.sw;
      ParamByName('c_LL').AsInteger := aSettings.channel.longLen;
      ParamByName('c_DT').AsInteger := aSettings.channel.thin.dia;
      ParamByName('c_switchThin').AsInteger := aSettings.channel.thin.sw;
      ParamByName('c_LT').AsInteger := aSettings.channel.thinLen;
        // Splice
      ParamByName('c_SDN').AsInteger := aSettings.splice.neps.dia;
      ParamByName('c_SswitchNeps').AsInteger := aSettings.splice.neps.sw;
      ParamByName('c_SDS').AsInteger := aSettings.splice.short.dia;
      ParamByName('c_SswitchShort').AsInteger := aSettings.splice.short.sw;
      ParamByName('c_SLS').AsInteger := aSettings.splice.shortLen;
      ParamByName('c_SDL').AsInteger := aSettings.splice.long.dia;
      ParamByName('c_SswitchLong').AsInteger := aSettings.splice.long.sw;
      ParamByName('c_SLL').AsInteger := aSettings.splice.longLen;
      ParamByName('c_SDT').AsInteger := aSettings.splice.thin.dia;
      ParamByName('c_SswitchThin').AsInteger := aSettings.splice.thin.sw;
      ParamByName('c_SLT').AsInteger := aSettings.splice.thinLen;
      ParamByName('c_UpY').AsInteger := aSettings.splice.upperYarn.dia;
      ParamByName('c_switchUpY').AsInteger := aSettings.splice.upperYarn.sw;
      xField := 0;
      for x := 0 to 15 do
        for x1 := 0 to 7 do
        begin
          if IsSet(x1, aSettings.classClear[x]) then
            xChar128[xField] := cCutFlag
          else
            xChar128[xField] := cUncutFlag;
          inc(xField);
        end;
      ParamByName('c_classClear').AsString := xChar128;
      xField := 0;
      for x := 0 to 7 do
        for x1 := 0 to 7 do
        begin
          if IsSet(x1, aSettings.siroClear[x]) then
            xChar64[xField] := cCutFlag
          else
            xChar64[xField] := cUncutFlag;
          inc(xField);
        end;
      ParamByName('c_siroClear').AsString := xChar64;
        //AdditionalRec
      ParamByName('c_fAdjConeReduction').AsInteger := aSettings.additional.fAdjConeReduction;
        //Option
      ParamByName('c_DiaDiff').AsInteger := aSettings.additional.option.diaDiff;
      ParamByName('c_CntRep').AsInteger := aSettings.additional.option.cntRep;
      ParamByName('c_siroStartupRep').AsInteger := aSettings.additional.option.siroStartupRep;
      ParamByName('c_ClDia').AsInteger := aSettings.additional.option.clusterDia;
      ParamByName('c_ClLen').AsInteger := aSettings.additional.option.clusterLength;
      ParamByName('c_ClDef').AsInteger := aSettings.additional.option.clusterDefects;
        //Extended option
      ParamByName('c_negDiaDiff').AsInteger := aSettings.additional.extOption.negDiaDiff;
      ParamByName('c_CntLen').AsInteger := aSettings.additional.extOption.countLength;
      ParamByName('c_ClRep').AsInteger := aSettings.additional.extOption.clusterRep;
        // SFIPara
      ParamByName('c_SFI_absref').AsInteger := aSettings.sfi.absRef;
      ParamByName('c_SFI_upper').AsInteger := aSettings.sfi.upper;
      ParamByName('c_SFI_lower').AsInteger := aSettings.sfi.lower;
      ParamByName('c_SFI_rep').AsInteger := aSettings.sfi.rep;

        // FFCluster
      ParamByName('c_FFClLen').AsInteger := aSettings.fFCluster.obsLength;
      ParamByName('c_FFClDef').AsInteger := aSettings.fFCluster.defects;
      ParamByName('c_FFClRep').AsInteger := aSettings.fFCluster.rep;
      xField := 0;
      for x := 0 to 7 do
        for x1 := 0 to 7 do
        begin
          if IsSet(x1, aSettings.fFCluster.siroClear[x]) then
            xChar64[xField] := cCutFlag
          else
            xChar64[xField] := cUncutFlag;
          inc(xField);
        end;
      ParamByName('c_FFClSiroClear').AsString := xChar64;
      ParamByName('c_head_class').AsInteger := Ord(mYMSettingsUtils.GetSensingHeadClass(aSettings));

      //Nue:17.9.01
      if not aForChecking then begin
        //AdditionalRec
        ParamByName('c_configA').AsInteger := Smallint(aSettings.additional.configA);
        ParamByName('c_configB').AsInteger := Smallint(aSettings.additional.configB);
        ParamByName('c_configC').AsInteger := Smallint(aSettings.additional.configC);
          // Availability
        ParamByName('c_inopSettings0').AsInteger := aSettings.available.inoperativePara[0];
        ParamByName('c_inopSettings1').AsInteger := aSettings.available.inoperativePara[1];

        ParamByName('c_spare1').AsInteger := aSettings.spare.fill[0];
        ParamByName('c_spare2').AsInteger := aSettings.spare.fill[1];
        ParamByName('c_spare3').AsInteger := aSettings.spare.fill[2];
        ParamByName('c_spare4').AsInteger := aSettings.spare.fill[3];
        ParamByName('c_spare5').AsInteger := aSettings.spare.fill[4];
        ParamByName('c_spare6').AsInteger := aSettings.spare.fill[5];
      end;
    end;
  except
    on e: Exception do
    begin
      raise Exception.Create('InitQueryBySettings failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TSettingsDBAccess.InitSettingsByQuery(var aSettings: TYMExtensionRec);
var x, x1: Integer;
  xField: Integer;
  xChar64: TChar64Arr;
  xChar128: TChar128Arr;
  xTmpStr: string;

  procedure SetBit(aBitNr: Integer; var aValue: byte);
  begin
    if aBitNr > 7 then raise Exception.Create('Nr. of Bit out of range.');
    aValue := aValue or (1 shl aBitNr);
  end;
begin
  try
//      FillChar ( aSettings, sizeof ( TYMSettingsRec ), 0 );
    mYMSettingsUtils.InitYMSettings(aSettings.YMSettings);
    FillChar ( aSettings.YM_Set_Name, sizeof ( TText50 ), 0 );  //Nue:18.9.01
    with mQuery, aSettings.YMSettings do
    begin
      xTmpStr := FieldByName('c_YM_set_name').AsString;
      StrLCopy(@aSettings.YM_Set_Name, PChar(xTmpStr), sizeOf(aSettings.YM_Set_Name));
      channel.neps.dia := FieldByName('c_DN').AsInteger;
      channel.neps.sw := FieldByName('c_switchNeps').AsInteger;
      channel.short.dia := FieldByName('c_DS').AsInteger;
      channel.short.sw := FieldByName('c_switchShort').AsInteger;
      channel.shortLen := FieldByName('c_LS').AsInteger;
      channel.long.dia := FieldByName('c_DL').AsInteger;
      channel.long.sw := FieldByName('c_switchLong').AsInteger;
      channel.longLen := FieldByName('c_LL').AsInteger;
      channel.thin.dia := FieldByName('c_DT').AsInteger;
      channel.thin.sw := FieldByName('c_switchThin').AsInteger;
      channel.thinLen := FieldByName('c_LT').AsInteger;
        // splice.checkLen is needed here to indicate the user if Zero then: NO seperate splice check settings
        // else : seperate splice check settings
      if FieldByName('c_spliceParams').AsBoolean then
        splice.checkLen := 1;
      splice.neps.dia := FieldByName('c_SDN').AsInteger;
      splice.neps.sw := FieldByName('c_SswitchNeps').AsInteger;
      splice.short.dia := FieldByName('c_SDS').AsInteger;
      splice.short.sw := FieldByName('c_SswitchShort').AsInteger;
      splice.shortLen := FieldByName('c_SLS').AsInteger;
      splice.long.dia := FieldByName('c_SDL').AsInteger;
      splice.long.sw := FieldByName('c_SswitchLong').AsInteger;
      splice.longLen := FieldByName('c_SLL').AsInteger;
      splice.thin.dia := FieldByName('c_SDT').AsInteger;
      splice.thin.sw := FieldByName('c_SswitchThin').AsInteger;
      splice.thinLen := FieldByName('c_SLT').AsInteger;
      splice.upperYarn.dia := FieldByName('c_UpY').AsInteger;
      splice.upperYarn.sw := FieldByName('c_switchUpY').AsInteger;
      StrCopy(xChar128, PChar(FieldByName('c_classClear').AsString));
      xField := 0;
      for x := 0 to 15 do
        for x1 := 0 to 7 do
        begin
          if xChar128[xField] = cCutFlag then
            SetBit(x1, classClear[x]);
          inc(xField);
        end;
      StrCopy(xChar64, PChar(FieldByName('c_siroClear').AsString));
      xField := 0;
      for x := 0 to 7 do
        for x1 := 0 to 7 do
        begin
          if xChar64[xField] = cCutFlag then
            SetBit(x1, siroClear[x]);
          inc(xField);
        end;
        //AdditionalRec
      Smallint(additional.configA) := FieldByName('c_configA').AsInteger;
      Smallint(additional.configB) := FieldByName('c_configB').AsInteger;
      Smallint(additional.configC) := FieldByName('c_configC').AsInteger;
      additional.fAdjConeReduction := FieldByName('c_fAdjConeReduction').AsInteger;
        //Option
      additional.option.diaDiff := FieldByName('c_DiaDiff').AsInteger;
      additional.option.cntRep := FieldByName('c_CntRep').AsInteger;
      additional.option.siroStartupRep := FieldByName('c_siroStartupRep').AsInteger;
      additional.option.clusterDia := FieldByName('c_ClDia').AsInteger;
      additional.option.clusterLength := FieldByName('c_ClLen').AsInteger;
      additional.option.clusterDefects := FieldByName('c_ClDef').AsInteger;
        //Extended option
      additional.extOption.negDiaDiff := FieldByName('c_negDiaDiff').AsInteger;
      additional.extOption.countLength := FieldByName('c_CntLen').AsInteger;
      additional.extOption.clusterRep := FieldByName('c_ClRep').AsInteger;
        // SFIPara
      sfi.absRef := FieldByName('c_SFI_absref').AsInteger;
      sfi.upper := FieldByName('c_SFI_upper').AsInteger;
      sfi.lower := FieldByName('c_SFI_lower').AsInteger;
      sfi.rep := FieldByName('c_SFI_rep').AsInteger;
        // Availability
      available.inoperativePara[0] := FieldByName('c_inopSettings0').AsInteger;
      available.inoperativePara[1] := FieldByName('c_inopSettings1').AsInteger;
        // FFCluster
      fFCluster.obsLength := FieldByName('c_FFClLen').AsInteger;
      fFCluster.defects := FieldByName('c_FFClDef').AsInteger;
      fFCluster.rep := FieldByName('c_FFClRep').AsInteger;
      StrCopy(xChar64, PChar(FieldByName('c_FFClSiroClear').AsString));
      xField := 0;
      for x := 0 to 7 do
        for x1 := 0 to 7 do
        begin
          if xChar64[xField] = cCutFlag then
            SetBit(x1, fFCluster.siroClear[x]);
          inc(xField);
        end;
      spare.fill[0] := FieldByName('c_spare1').AsInteger;
      spare.fill[1] := FieldByName('c_spare2').AsInteger;
      spare.fill[2] := FieldByName('c_spare3').AsInteger;
      spare.fill[3] := FieldByName('c_spare4').AsInteger;
      spare.fill[4] := FieldByName('c_spare5').AsInteger;
      spare.fill[5] := FieldByName('c_spare6').AsInteger;
    end;
  except
    on e: Exception do
    begin
      raise Exception.Create('InitSettingsByQuery failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

function TSettingsDBAccess.InsertNewRecord(aSettings: TYMSettingsRec; aName: string; aTemplate: boolean): Integer;
begin
EnterMethod('TSettingsDBAccess.InsertNewRecord');
  try
    Result := 0;
    with mQuery do
    begin
      SQL.Text := cInsertSettings;
      InitQueryBySettings(aSettings);
      ParamByName('c_YM_set_name').AsString := aName;
      ParamByName('c_template_set').AsBoolean := aTemplate;
codesite.SendFmtMsg('aSettings YarnCnt:%d',[aSettings.additional.option.yarncnt]);
codesite.SendMsg('TSettingsDBAccess.InsertNewRecord: Before InsertSQL-Call');
      Result := mQuery.InsertSQL('t_ym_settings', 'c_YM_set_id', high(Smallint), 1);
codesite.SendMsg('TSettingsDBAccess.InsertNewRecord: After InsertSQL-Call');
      Close;

      //Nue:17.9.01
      if aName='' then begin
        SQL.Text := cUpdateYMSetName;
        ParamByName('c_YM_set_name').AsString := Format(cNoYMNamePrefix,[Result]);
        ParamByName('c_YM_set_id').AsInteger := Result;
        ExecSQL;
        Close;
      end;

    end;
  except
    on e: Exception do
    begin
codesite.SendMsg('EXEPCTION:TSettingsDBAccess.InsertNewRecord');
      raise Exception.Create('InsertNewRecord failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

function TSettingsDBAccess.New(aSettings: TYMSettingsRec; aName: string): Integer;
begin
  Result := 0;
  try
    Result := InsertNewRecord(aSettings, aName, false);
  except
    on e: Exception do
    begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'New failed. ' + e.message);
      raise Exception.create('New failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

function TSettingsDBAccess.New(aSettings: TYMSettingsRec): Integer;
begin
  Result := 0;
  try
    Result := InsertNewRecord(aSettings, '', false);
  except
    on e: Exception do
    begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'New failed. ' + e.message);
      raise Exception.create('New failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

function TSettingsDBAccess.NewTemplate(aSettings: TYMSettingsRec; aName: string): Integer;
begin
  Result := 0;
  try
    Result := InsertNewRecord(aSettings, aName, true);
  except
    on e: Exception do
    begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'NewTemplate failed. ' + e.message);
      raise Exception.create('NewTemplate failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TSettingsDBAccess.RollbackTransaction;
begin
  try
    mQuery.Connection.RollbackTrans;
  except
    on e: Exception do
    begin
      raise Exception.Create('RollbackTransaction failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

function TSettingsDBAccess.Save(aSettings: TYMSettingsRec; aYM_Set_Name: TText50; aYM_Set_Changed: Boolean (*Nue:13.9.01*)): Integer;
var xSetID: Integer;
    xTemplate: Boolean;
    xYM_Set_Name: string;
begin
EnterMethod('TSettingsDBAccess.Save');
  Result := 0;
  try
//Nue:There is an outer BeginTransaction in SetSettings.AnalyseJob (19.9.01)   BeginTransaction;
    xYM_Set_Name := StrPas(@aYM_Set_Name); //If aYM_Set_Name is empty (from ZE) AutoName-<ID> will filled in in InsertNewRecord
    codesite.SendFmtMsg('TSettingsDBAccess.Save: YM_Set_Name = %s ', [xYM_Set_Name]);
    if (xYM_Set_Name<>'') and (Pos(cX1, xYM_Set_Name)=0) and aYM_Set_Changed then begin
//Alt      xYM_Set_Name := StrPas(StrLCat(@aYM_Set_Name,PChar(cX1), sizeOf(aYM_Set_Name)));
      xYM_Set_Name := StrPas(@aYM_Set_Name);    //Nue:24.7.02
      xYM_Set_Name := Copy(xYM_Set_Name,0,SizeOf(TText50)-SizeOf(cX1))+cX1; //Nue:24.7.02
    end;
    if not CheckAvailabilityOfSettings(aSettings, aYM_Set_Name, xTemplate, xSetID) then begin
      xSetID := InsertNewRecord(aSettings, xYM_Set_Name, false);
    end
    else if xTemplate then begin
      xSetID := InsertNewRecord(aSettings, xYM_Set_Name, false);
    end;
codesite.SendFmtMsg('TSettingsDBAccess.Save: YMSetID:%d ', [xSetID]);
    Result := xSetID;
//Nue:There is an outer CommitTransaction in SetSettings.AnalyseJob (19.9.01)    CommitTransaction;
  except
    on e: Exception do
    begin
codesite.SendMsg('EXEPCTION:TSettingsDBAccess.Save');
//Nue: There is an outer RollbackTransaction in SetSettings.AnalyseJob (08.07.02)    RollbackTransaction;
//      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Update failed. ' + e.message);
//      raise Exception.create(Error.Msg);

      raise Exception.create('Update failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
 {Delete 25.1.01 Nue
procedure TSettingsDBAccess.SaveMachineYMPara(aMachineID: Word; aSpindleFirst, aSpindleLast: Byte; aMachinePara: TMachineYMParaRec); //Speichert den Teil der YMPara, welche in DB-Tabelle t_machine abgelegt sind
var
  xStr: array[1..30] of Char;
begin
  FillChar(xStr, sizeof(xStr), 0);
  try
    with mQuery do
    begin
      SQL.Text := cSaveMachineYMPara;
      with aMachinePara do
      begin
        ParamByName('c_machine_type').AsInteger := machType;
        ParamByName('c_checkLen').AsInteger := checkLen * cCheckLenFactor;
        ParamByName('c_cutRetries').AsInteger := cutRetries;
        ParamByName('c_configA').AsInteger := configA;
        ParamByName('c_configB').AsInteger := configB;
        ParamByName('c_configC').AsInteger := configC;
        Move(machBez, xStr, Length(machBez));
        ParamByName('c_machine_name').AsString := string(xStr);
        ParamByName('c_longStopDef').AsInteger := longStopDef;
        ParamByName('c_inopSettings0').AsInteger := inoperativePara[0]; //Nue 18.05.2000
        ParamByName('c_inopSettings1').AsInteger := inoperativePara[1]; //Nue 18.05.2000
        Move(yMSWVersion, xStr, Length(yMSWVersion));
        ParamByName('c_YM_version').AsString := string(xStr);
        ParamByName('c_machine_id').AsInteger := aMachineID;
        ParamByName('c_head_type').AsInteger := sensingHead;
        ParamByName('c_machine_id').AsInteger := aMachineID;
        ParamByName('spindle_first').AsInteger := aSpindleFirst;
        ParamByName('spindle_last').AsInteger := aSpindleLast;
        ParamByName('c_front_type').AsInteger := yMType;
        ParamByName('c_machine_id').AsInteger := aMachineID;
      end;                              //with
      ExecSQL;
      Close;
    end;
  except
    on e: Exception do
    begin
      raise Exception.create('TSettingsDBAccess.SaveMachineYMPara failed. ' + e.Message);
    end;
  end;
end;
{}
//------------------------------------------------------------------------------

procedure SaveMachineYMConfig(aMachineID: Word; aSpindleFirst, aSpindleLast: Byte; aMachineConfig: TMachineYMConfigRec);
begin
  //Nue Has to be coded when needed!!!!
end;

//------------------------------------------------------------------------------

procedure TSettingsDBAccess.SaveProdGrpYMPara(aProdGrpID: Longint; aProdGrpPara: TProdGrpYMParaRec); //Speichert den Teil der YMPara, welche in DB-Tabelle t_prodgroup abgelegt sind
begin
  try
    with mQuery do
    begin
      SQL.Text := cSaveProdGrpYMPara;
      with aProdGrpPara do
      begin
        ParamByName('c_machineGroup').AsInteger := group;
        ParamByName('c_spindle_first').AsInteger := spdl.start;
        ParamByName('c_spindle_last').AsInteger := spdl.stop;
        ParamByName('c_pilotSpindles').AsInteger := pilotSpindles;
        ParamByName('c_speedRamp').AsInteger := speedRamp;
        ParamByName('c_speed').AsInteger := speed;
//Old til 28.2.02 Nue        ParamByName('c_act_yarncnt').AsInteger := yarnCnt;
        //New 28.2.02 Nue
        ParamByName('c_act_yarncnt').AsFloat := YarnCountConvert(TYarnUnits(yarnCntUnit), yuNm, yarnCnt / cYarnCntFactor); // wss
//        ParamByName('c_act_yarncnt').AsFloat := YarnCountConvert(TYarnUnits(yarnCntUnit), yuNm, yarnCnt, cYarnCntFactor);
        ParamByName('c_yarncnt_unit').AsInteger := yarnCntUnit;
        ParamByName('c_nr_of_threads').AsInteger := nrOfThreads;
        ParamByName('c_lengthWindow').AsInteger := lengthWindow;
        ParamByName('c_lengthMode').AsInteger := lengthMode;
        ParamByName('c_prod_id').AsInteger := prodGrpID;
      end;                              //with
      ExecSQL;
      Close;
    end;
  except
    on e: Exception do
    begin
      raise Exception.create('TSettingsDBAccess.SaveProdGrpYMPara failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TSettingsDBAccess.Update(aSetID: Integer; aSettings: TYMSettingsRec);
begin
  try
    BeginTransaction;
    if not CheckAvailabilityOfSetID(aSetID) then
    begin
      fError := SetError(ERROR_INVALID_HANDLE, etMMError, Format('YM_SetID %s not available on Database.', [aSetID]));
      raise Exception.Create(Format('YM_SetID %s not available on Database.', [aSetID]));
    end;
    with mQuery do
    begin
      SQL.Text := cUpdateSettings;
      Params.ParamByName('c_ym_set_id').AsInteger := aSetID;
      InitQueryBySettings(aSettings);
      ExecSQL;
      Close;
    end;
    CommitTransaction;
  except
    on e: Exception do
    begin
      RollbackTransaction;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Update failed. ' + e.message);
      raise Exception.create('Update failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
{ Not uset yet
  procedure TSettingsDBAccess.UpdateSpliceParams ( aSetID : Integer; aValue : boolean );
  begin
    try
      BeginTransaction;
      if not CheckAvailabilityOfSetID ( aSetID ) then begin
        fError := SetError ( ERROR_INVALID_HANDLE, etMMError, Format ( 'YM_SetID %s not available on Database.',[aSetID] ) );
        raise Exception.Create ( Error.Msg  );
      end;
      with mQuery do begin
        SQL.Text := cUpdateSpliceParams;
        ParamByName ( 'c_ym_set_id' ).AsInteger := aSetID;
        ParamByName ( 'c_SpliceParams' ).AsBoolean := aValue;
        ExecSQL;
        Close;
      end;
      CommitTransaction;
    except
      on e:Exception do begin
        RollbackTransaction;
        fError := SetError ( ERROR_INVALID_FUNCTION, etDBError, 'UpdateSpliceParams failed. ' + e.message );
        raise Exception.create ( Error.Msg );
      end;
    end;
  end;
}
//------------------------------------------------------------------------------

//******************************************************************************
// TSettingsDBAssistant
//******************************************************************************
//------------------------------------------------------------------------------

procedure TSettingsDBAssistant.AddMachineYMConfig(var aSettings: TYMSettingsByteArr;
  aMachineYMConfig: TMachineYMConfigRec);
begin
{Delete Nue/Kr 25.01.01
  if PYMSettings(@aSettings)^.splice.checkLen = 0 then
  // c_SpliceParams = False: no seperate splice check settings
    aMachineYMConfig.checkLen := 0;  //Nue 25.1.01 ??
}
  mYMMachineSettings.InsertYMMachineConfig(aMachineYMConfig, PYMSettings(@aSettings)^);
end;
//------------------------------------------------------------------------------
(*
procedure TSettingsDBAssistant.AddMachineYMPara(var aSettings: TYMSettingsByteArr;
  aMachineYMPara: TMachineYMParaRec);
begin

  if PYMSettings(@aSettings)^.splice.checkLen = 0 then
  // c_SpliceParams = False: no seperate splice check settings
    aMachineYMPara.checkLen := 0;

  mYMMachineSettings.InsertMachineYMPara(aMachineYMPara, PYMSettings(@aSettings)^);
end;
*)
//------------------------------------------------------------------------------

procedure TSettingsDBAssistant.AddProdGrpYMPara(var aSettings: TYMSettingsByteArr;
  aProdGrpYMPara: TProdGrpYMParaRec;
  aAssignment: Byte
  // cAssigGroup: Assign settings and its prodGrpID to the Maschinegroup
 // cAssigMemory:	Assign settings to a memory (not yet used)
  // cAssigProdGrpIDOnly: Assign only the prodGrpID to the Maschinegroup
  );
begin
  mYMSettingsUtils.PutProdGrpYMPara(aProdGrpYMPara, PYMSettings(@aSettings)^, aAssignment);
end;
//------------------------------------------------------------------------------

constructor TSettingsDBAssistant.Create;
begin
  inherited Create;
  mYMSettingsUtils := TYMSettingsUtils.Create;
  mYMMachineSettings := TYMMachineSettings.Create;
end;
//------------------------------------------------------------------------------

constructor TSettingsDBAssistant.Create(aQuery: TNativeAdoQuery);
begin
  inherited Create(aQuery);
  mYMSettingsUtils := TYMSettingsUtils.Create;
  mYMMachineSettings := TYMMachineSettings.Create;
end;
//------------------------------------------------------------------------------

destructor TSettingsDBAssistant.Destroy;
begin
  FreeAndNil(mYMSettingsUtils);
  FreeAndNil(mYMMachineSettings);
  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TSettingsDBAssistant.ExtractDataRules(var aSettings: TYMSettingsByteArr;
  var aRules: TYMDataRulesRec);
begin
  mYMSettingsUtils.GetDataRules(PYMSettings(@aSettings)^, aRules);
end;
//------------------------------------------------------------------------------
{//Kr,Nue 13.03.2001
function TSettingsDBAssistant.ExtractMachineYMConfig(var aSettings: TSettingsArr): TMachineYMConfigRec;
begin

  Result := mYMMachineSettings.ExtractYMMachineConfig(PSettingsArr(@aSettings)^);

end;
{}
//------------------------------------------------------------------------------
{Deleted 25.01.01 Nue

procedure TSettingsDBAssistant.ExtractMachineYMPara(var aSettings: TYMSettingsByteArr;
  var aMachineYMPara: TMachineYMParaRec);
begin

  mYMSettingsUtils.ExtractMachineYMPara(PYMSettings(@aSettings)^, aMachineYMPara);

end;
//------------------------------------------------------------------------------
{//Kr,Nue 13.03.2001

procedure TSettingsDBAssistant.ExtractProdGrpYMPara(var aSettings: TYMSettingsByteArr;
  var aProdGrpYMPara: TProdGrpYMParaRec);
begin
  mYMSettingsUtils.GetProdGrpYMPara(PYMSettings(@aSettings)^, aProdGrpYMPara);

end;
{}
//------------------------------------------------------------------------------

procedure TSettingsDBAssistant.Get(aSetID: Integer; var aSettings: TYMSettingsByteArr; var aSize: Word);
  var
    xSettings : TYMExtensionRec;
begin
  FillChar(xSettings,sizeOf(xSettings),0);
  inherited Get(aSetID, xSettings);
  PYMSettings(@aSettings)^ := xSettings.YMSettings;
  aSize := SizeOf(xSettings.YMSettings);

//  inherited Get(aSetID, xSettings);

//  mYMSettingsUtils.ChangeEndian(xSettings, xSettings);
//  aSize := SizeOf(xSettings);
//  Move(xSettings, aSettings, aSize);
end;
//------------------------------------------------------------------------------

function TSettingsDBAssistant.Save(var aSettings: TYMSettingsByteArr;
                                   aYM_Set_Name: TText50; aYM_Set_Changed: Boolean (*Nue:13.9.01*)): Integer;
var
  xSettings: TYMSettingsRec;
begin

  Move(aSettings, xSettings, SizeOf(xSettings));

//Rausgenommen Kr,Nue 13.03.2001; Settings muessen ungefiltert gespeichert werden (kr)
//  mYMMachineSettings.FilterMachineConfigCode(xSettings);

  Result := inherited Save(xSettings, aYM_Set_Name, aYM_Set_Changed);
end;
//------------------------------------------------------------------------------

function TSettingsDBAssistant.SaveAll(var aSettings: TYMSettingsByteArr; aProdGrpId: Integer;
  aMachineID: Word; aSpindleFirst, aSpindleLast: Byte;
  aYM_Set_Name: TText50; aYM_Set_Changed: Boolean;  //Nue:13.9.01
  aMachineUpdate: Boolean): Integer;
var
//  xSettings: TSettingsArr;
//  xMachinePara, xTmpMachinePara: TMachineYMConfigRec;
  xProdGrpPara: TProdGrpYMParaRec;
begin
  xProdGrpPara := TYMSettingsUtils.ExtractProdGrpYMPara(aSettings, sizeof(aSettings));
  SaveProdGrpYMPara(aProdGrpId, xProdGrpPara);

{The following lines are not necessary! Nue 25.01.01
 Saving MachineYMConfig on the DB happens exclusivly in the application
 MaConfig when uploading machineconfig and save the settings there explicit!!

  ExtractMachineYMPara(aSettings, xMachinePara);
  xMachinePara := ExtractMachineYMConfig(aSettings);
  if aMachineUpdate then
//    SaveMachineYMPara(aMachineID, aSpindleFirst, aSpindleLast, xMachinePara);
    SaveMachineYMConfig(aMachineID, aSpindleFirst, aSpindleLast, xMachinePara);
{}
  Result := Save(aSettings, aYM_Set_Name, aYM_Set_Changed);            //Return is YM_set_id
end;
//------------------------------------------------------------------------------


end.

