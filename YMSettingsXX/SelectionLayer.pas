(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SelectionLayer.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr | Initial Release
|=========================================================================================*)
unit SelectionLayer;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, ExtCtrls, QualityMatrixDef,
  QualityMatrixBase, YMParaDef;

type
  //...........................................................................
  TFieldStateTbl = array of Byte;
  //...........................................................................

//  TSelectionLayer = class(TPaintBox)
  TSelectionLayer = class(TObject)
  private
    { Private declarations }

    fActiveVisible: Boolean;
    fCutActiveColor: TBrushRec;
    fCutState: TFieldStateTbl;
    fFFClusterActiveColor: TBrushRec;
    fFFClusterState: TFieldStateTbl;
    fInactiveColor: TBrushRec;
    fMode: TSelectionMode;
    fSCActiveColor: TBrushRec;
    fSCMemberState: TFieldStateTbl;
    fSubType: TMatrixSubType;
    mQualityMatrixBase: TQualityMatrixBase;

    function GetCertainFieldState(aFieldID: Integer; aMode: TSelectionMode): Boolean;
    function GetCertainFieldStateColor(aID, aIndex: Integer; aMode: TSelectionMode): TBrushRec;
    function GetFieldState(aFieldID: Integer): Boolean;
    function GetFieldStateColor(aID, aIndex: Integer): TBrushRec;
    function GetStateColor(const aIndex: Integer): TColor;
    function GetStateStyle(const aIndex: Integer): TBrushStyle;

    procedure SetCertainFieldState(aFieldID: Integer; aState: Boolean; aMode: TSelectionMode);
    procedure SetFieldState(aFieldID: Integer; aState: Boolean);
    procedure SetStateColor(const aIndex: Integer; const aValue: TColor);
    procedure SetStateStyle(const aIndex: Integer; const aValue: TBrushStyle);
    procedure SetSubType(const aValue: TMatrixSubType);
    procedure SetType(aType: TMatrixType);
    procedure SetMode(const aValue: TSelectionMode);

  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(aQualityMatrixBase: TQualityMatrixBase); reintroduce;
    destructor Destroy; override;

    procedure PaintField(aFieldID: Integer);

    procedure GetFieldStateTable(var aClassClear: TClassClearSettingsArr); overload;
    procedure GetFieldStateTable(var aFFClass, aFFCluster: TSiroClearSettingsArr); overload;
    procedure GetFieldStateTable(var aColorClass, aColorCluster: TColorClearSettingsArr); overload;

    procedure SetDefaultColor;

    procedure SetFieldStateTable(aClassClear: TClassClearSettingsArr); overload;
    procedure SetFieldStateTable(aFFClass, aFFCluster: TSiroClearSettingsArr); overload;
    procedure SetFieldStateTable(aColorClass, aColorCluster: TColorClearSettingsArr); overload;

    property ActiveFieldColor[aID: Integer]: TBrushRec index cCutActive read GetFieldStateColor;
    property FieldState[aFieldID: Integer]: Boolean read GetFieldState write SetFieldState;
    property MatrixType: TMatrixType write SetType;
    property MatrixSubType: TMatrixSubType write SetSubType;
    property InactiveFieldColor[aID: Integer]: TBrushRec index cCutPassive read GetFieldStateColor;
    property SelectionMode: TSelectionMode read fMode write SetMode;

    property ActiveColor: TColor index cCutActive read GetStateColor write SetStateColor;
    property ActiveStyle: TBrushStyle index cCutActive read GetStateStyle write SetStateStyle;
    property ActiveVisible: Boolean read fActiveVisible write fActiveVisible;

    property InactiveColor: TColor index cCutPassive read GetStateColor write SetStateColor;

  published
    { Published declarations }

  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
//------------------------------------------------------------------------------
//******************************************************************************
// TSelectionLayer
//******************************************************************************
//------------------------------------------------------------------------------

constructor TSelectionLayer.Create(aQualityMatrixBase: TQualityMatrixBase);
begin
  inherited Create;

  mQualityMatrixBase := aQualityMatrixBase;

//  SelectionMode := smSCMemberField;
  SelectionMode := smCutField;


  SetDefaultColor;

end;
//------------------------------------------------------------------------------

destructor TSelectionLayer.Destroy;
begin
  Finalize(fCutState);
  Finalize(fFFClusterState);
  Finalize(fSCMemberState);

  inherited Destroy;
end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetCertainFieldState(aFieldID: Integer;
  aMode: TSelectionMode): Boolean;
var
  xArrayIndex, xBitIndex: Integer;
  xByte: Byte;
  xState: TFieldStateTbl;
const
  Bit0 = $01;
begin

  case aMode of
    smCutField:
      xState := fCutState;

    smFFClusterField:
      xState := fFFClusterState;

    smSCMemberField:
      xState := fSCMemberState;
  else
    xState := fCutState;
  end;

  Result := False;
  if aFieldID < mQualityMatrixBase.TotFields then
  begin
    if aFieldID < 64 then
    begin
      xArrayIndex := 0;
    end
    else
    begin
      xArrayIndex := 8;
      aFieldID := aFieldID - 64;
    end;
    xArrayIndex := xArrayIndex + (aFieldID mod 8);
    xBitIndex := aFieldID div 8;

    if xArrayIndex < Length(xState) then
    begin
      xByte := xState[xArrayIndex] and (Bit0 shl xBitIndex);

      if xByte <> 0 then
        Result := True
      else
        Result := False;
    end
    else
      Result := False;

  end;

end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetCertainFieldStateColor(aID, aIndex: Integer;
  aMode: TSelectionMode): TBrushRec;
begin
  Result := cBackgroundUnassigned;
  Result.style := bsClear;

  with mQualityMatrixBase do
    case aIndex of

      cCutActive:
        case aMode of

          smCutField:
            begin

              if fCutActiveColor.color <> clNone then
              begin
                if fCutActiveColor.style = bsClear then
                  Result := CutColor[aID]
                else
                  Result.style := fCutActiveColor.style;
                Result.color := fCutActiveColor.color;
              end
              else
              begin
                Result := CutColor[aID];
                if fCutActiveColor.style <> bsClear then
                  Result.style := fCutActiveColor.style;
              end;
            end;

          smFFClusterField:
            begin

              if fFFClusterActiveColor.color <> clNone then
              begin
                if fFFClusterActiveColor.style = bsClear then
                  Result := FFClusterColor[aID]
                else
                  Result.style := fFFClusterActiveColor.style;
                Result.color := fFFClusterActiveColor.color;
              end
              else
              begin
                Result := FFClusterColor[aID];
                if fFFClusterActiveColor.style <> bsClear then
                  Result.style := fFFClusterActiveColor.style;
              end;
            end;

          smSCMemberField:
            begin

              if fSCActiveColor.color <> clNone then
              begin
                if fSCActiveColor.style = bsClear then
                  Result := SCMemberColor[aID]
                else
                  Result.style := fSCActiveColor.style;
                Result.color := fSCActiveColor.color;
              end
              else
              begin
                Result := SCMemberColor[aID];
                if fSCActiveColor.style <> bsClear then
                  Result.style := fSCActiveColor.style;
              end;
            end;
        end;

      cCutPassive:
        begin
          if fInactiveColor.color = clNone then
          begin
            case SelectionMode of

              smCutField:
                Result := NoCutColor[aID];

              smFFClusterField:
                Result := NoFFClusterColor[aID];

              smSCMemberField:
                Result := NoSCMemberColor[aID];
            end;
            if fInactiveColor.style <> bsClear then
              Result.style := fInactiveColor.style;
          end
          else
          begin
            if fInactiveColor.style = bsClear then
              case SelectionMode of

                smCutField:
                  Result := NoCutColor[aID];

                smFFClusterField:
                  Result := NoFFClusterColor[aID];

                smSCMemberField:
                  Result := NoSCMemberColor[aID];
              end
            else
              Result.style := fInactiveColor.style;
            Result.color := fInactiveColor.color;
          end;
        end;
    end;
end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetFieldState(aFieldID: Integer): Boolean;
begin

  Result := GetCertainFieldState(aFieldID, SelectionMode);
end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetFieldStateColor(aID, aIndex: Integer): TBrushRec;
begin

  Result := GetCertainFieldStateColor(aID, aIndex, SelectionMode);
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.GetFieldStateTable(
  var aClassClear: TClassClearSettingsArr);
var
  i: Integer;
begin
  if SizeOf(aClassClear) = Length(fCutState) then
    for i := 0 to High(aClassClear) do
      aClassClear[i] := fCutState[i];
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.GetFieldStateTable(
  var aFFClass, aFFCluster: TSiroClearSettingsArr);
var
  i: Integer;
begin
  if SizeOf(aFFClass) = Length(fCutState) then
    for i := 0 to High(aFFClass) do
    begin

      aFFClass[i] := fCutState[i];

      if fSubType <> mstSiroBD then
      begin
        fFFClusterState[i] := fFFClusterState[i] or fCutState[i];
        aFFCluster[i] := fFFClusterState[i];
      end
      else
        aFFCluster[i] := 0;
    end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.GetFieldStateTable(var aColorClass,
  aColorCluster: TColorClearSettingsArr);
var
  i: Integer;
begin

  if SizeOf(aColorClass) = Length(fCutState) then
    for i := 0 to High(aColorClass) do
    begin

      aColorClass[i] := fCutState[i];

      fFFClusterState[i] := fFFClusterState[i] or fCutState[i];
      aColorCluster[i] := fFFClusterState[i];
    end;

end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetStateColor(const aIndex: Integer): TColor;
var
  xColor: TBrushRec;
begin
  Result := clNone;

  with mQualityMatrixBase do
    case aIndex of

      cCutActive:
        case SelectionMode of

          smCutField:
            if fCutActiveColor.color = clNone then
            begin
              xColor := CutColor[0];
              Result := xColor.color;
            end
            else
              Result := fCutActiveColor.color;

          smFFClusterField:
            if fFFClusterActiveColor.color = clNone then
            begin
              xColor := FFClusterColor[0];
              Result := xColor.color;
            end
            else
              Result := fFFClusterActiveColor.color;

          smSCMemberField:
            if fSCActiveColor.color = clNone then
            begin
              xColor := SCMemberColor[0];
              Result := xColor.color;
            end
            else
              Result := fSCActiveColor.color;
        end;

      cCutPassive:
        if fInactiveColor.color = clNone then
        begin

          case SelectionMode of
            smSCMemberField:
              xColor := NoSCMemberColor[0];

            smCutField:
              xColor := NoCutColor[0];

            smFFClusterField:
              xColor := NoFFClusterColor[0];
          end;

          Result := xColor.color;
        end
        else
          Result := fInactiveColor.color;
    end;
end;
//------------------------------------------------------------------------------

function TSelectionLayer.GetStateStyle(const aIndex: Integer): TBrushStyle;
var
  xColor: TBrushRec;
begin

  Result := cBackgroundUnassigned.style;

  with mQualityMatrixBase do
    case aIndex of

      cCutActive:
        case SelectionMode of

          smCutField:
            if fCutActiveColor.style = bsClear then
            begin
              xColor := CutColor[0];
              Result := xColor.style;
            end
            else
              Result := fCutActiveColor.style;

          smFFClusterField:
            if fFFClusterActiveColor.style = bsClear then
            begin
              xColor := FFClusterColor[0];
              Result := xColor.style;
            end
            else
              Result := fFFClusterActiveColor.style;

          smSCMemberField:
            if fSCActiveColor.style = bsClear then
            begin
              xColor := SCMemberColor[0];
              Result := xColor.style;
            end
            else
              Result := fSCActiveColor.style;
        end;

//    cCutPassive:
    end;

end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.PaintField(aFieldID: Integer);
var
  xRect: TRect;
  xBoarder: Integer;
  xTBrush: TBrushRec;
begin
  with mQualityMatrixBase do
  begin

    CoordinateMode := cmInside;
    xRect := Field[aFieldID];

    if ActiveVisible then
    begin


      case SelectionMode of

        smCutField, smFFClusterField:
          begin


            if (LastCutMode = lcField) and (LastCutField = aFieldId) then
            begin
              xTBrush := InactiveFieldColor[aFieldID];
              xTBrush.color := LastCutColor;
              FillField(xRect, xTBrush);
            end
            else begin

              if GetCertainFieldState(aFieldID, smCutField) then

                FillField(xRect, GetCertainFieldStateColor(aFieldID, cCutActive, smCutField))

              else if GetCertainFieldState(aFieldID, smFFClusterField) then
              begin
                FillField(xRect, InactiveFieldColor[aFieldID]);

                with xRect do
                begin
                  xBoarder := (Right - Left) div 4;
                  Left := Left + xBoarder;
                  Right := Right - xBoarder;
                  xBoarder := (Bottom - Top) div 4;
                  Top := Top + xBoarder;
                  Bottom := Bottom - xBoarder;
                end;

                FillField(xRect, GetCertainFieldStateColor(aFieldID, cCutActive, smFFClusterField));
              end
              else
                FillField(xRect, InactiveFieldColor[aFieldID]);

            end;
          end;

        smSCMemberField:
          begin
            if GetCertainFieldState(aFieldID, smSCMemberField) then
              FillField(xRect, GetCertainFieldStateColor(aFieldID, cCutActive, smSCMemberField))
            else
              FillField(xRect, InactiveFieldColor[aFieldID]);
          end;
      end;
    end
    else
      FillField(xRect, InactiveFieldColor[aFieldID]);

  end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetCertainFieldState(aFieldID: Integer;
  aState: Boolean; aMode: TSelectionMode);
var
  xArrayIndex, xBitPosition: Integer;
//  xState: TFieldStateTbl;

  function SetBit(aByte: Byte; aBitPosition: Integer; aValue: Boolean): Byte;
  const
    cBit0 = $01;
  begin
    if aValue then
      Result := aByte or (cBit0 shl aBitPosition)
    else
      Result := aByte and not (cBit0 shl aBitPosition);
  end;
begin

  if aFieldID < mQualityMatrixBase.TotFields then
  begin
    if aFieldID < 64 then
    begin
      xArrayIndex := 0;
    end
    else
    begin
      xArrayIndex := 8;
      aFieldID := aFieldID - 64;
    end;
    xArrayIndex := xArrayIndex + (aFieldID mod 8);
    xBitPosition := aFieldID div 8;

    case aMode of
      smCutField:
        if (xArrayIndex < Length(fCutState)) and
          (xArrayIndex < Length(fFFClusterState)) then
        begin
          fFFClusterState[xArrayIndex] := SetBit(fFFClusterState[xArrayIndex], xBitPosition, aState);
          fCutState[xArrayIndex] := SetBit(fCutState[xArrayIndex], xBitPosition, aState);
        end;

      smFFClusterField:
        if (xArrayIndex < Length(fCutState)) and
          (xArrayIndex < Length(fFFClusterState)) then
        begin
//          if aState then
//            fCutState[xArrayIndex] := SetBit(fCutState[xArrayIndex], xBitPosition, not aState)
//          else
//            fCutState[xArrayIndex] := SetBit(fCutState[xArrayIndex], xBitPosition, aState);
          fFFClusterState[xArrayIndex] := SetBit(fFFClusterState[xArrayIndex], xBitPosition, aState)
            or fCutState[xArrayIndex];
        end;

      smSCMemberField:
        if (xArrayIndex < Length(fSCMemberState)) then
        begin
          fSCMemberState[xArrayIndex] := SetBit(fSCMemberState[xArrayIndex], xBitPosition, aState);
        end;
    end;
  end;
end;

//------------------------------------------------------------------------------

procedure TSelectionLayer.SetDefaultColor;
begin

  fCutActiveColor.color := clNone;
  fCutActiveColor.style := bsClear;

  fFFClusterActiveColor.color := clNone;
  fFFClusterActiveColor.style := bsClear;

  fInactiveColor.color := clNone;
  fInactiveColor.style := bsClear;

  fSCActiveColor.color := clNone;
  fSCActiveColor.style := bsClear;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetFieldState(aFieldID: Integer; aState: Boolean);
begin
  SetCertainFieldState(aFieldID, aState, SelectionMode);
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetFieldStateTable(
  aClassClear: TClassClearSettingsArr);
var
  i: Integer;
begin
  if SizeOf(aClassClear) = Length(fCutState) then
    for i := 0 to High(aClassClear) do
      fCutState[i] := aClassClear[i];
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetFieldStateTable(
  aFFClass, aFFCluster: TSiroClearSettingsArr);
var
  i: Integer;
begin
  if SizeOf(aFFClass) = Length(fCutState) then
    for i := 0 to High(aFFClass) do
    begin
      fCutState[i] := aFFClass[i];

      if fSubType <> mstSiroBD then
      begin
        fFFClusterState[i] := aFFCluster[i];
        fFFClusterState[i] := fFFClusterState[i] or fCutState[i];
      end;
    end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetFieldStateTable(aColorClass,
  aColorCluster: TColorClearSettingsArr);
var
  i: Integer;
begin

  if SizeOf(aColorClass) = Length(fCutState) then
    for i := 0 to High(aColorClass) do
    begin
      fCutState[i] := aColorClass[i];

      fFFClusterState[i] := aColorCluster[i];
      fFFClusterState[i] := fFFClusterState[i] or fCutState[i];
    end;

end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetMode(const aValue: TSelectionMode);
begin
  fMode := aValue;
  ActiveVisible := True;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetStateColor(const aIndex: Integer;
  const aValue: TColor);
begin

  case aIndex of

    cCutActive:
      case SelectionMode of

        smCutField:
          fCutActiveColor.color := aValue;

        smFFClusterField:
          fFFClusterActiveColor.color := aValue;

        smSCMemberField:
          fSCActiveColor.color := aValue;
      end;

    cCutPassive:
      fInactiveColor.color := aValue;
  end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetStateStyle(const aIndex: Integer;
  const aValue: TBrushStyle);
begin

  case aIndex of

    cCutActive:
      case SelectionMode of

        smCutField:
          fCutActiveColor.style := aValue;

        smFFClusterField:
          fFFClusterActiveColor.style := aValue;

        smSCMemberField:
          fSCActiveColor.style := aValue;
      end;

//    cCutPassive:
  end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetSubType(const aValue: TMatrixSubType);
var
  i: Integer;
begin
  if fSubType <> aValue then begin
    if (aValue = mstSiroBD) or (fSubType = mstSiroBD) then begin
      for i := 0 to High(fFFClusterState) do
        fFFClusterState[i] := $0;

      for i := 0 to High(fCutState) do
        fCutState[i] := $0;
    end;

    fSubType := aValue;
  end;
end;
//------------------------------------------------------------------------------

procedure TSelectionLayer.SetType(aType: TMatrixType);
var
  i, xRemainder, xLength: Integer;
begin
  with mQualityMatrixBase do
  begin
    if (TotFields mod 8) <> 0 then
      xRemainder := 1
    else
      xRemainder := 0;
    xLength := TotFields div 8 + xRemainder;

    if xLength <> Length(fCutState) then
    begin
      SetLength(fCutState, xLength);
      for i := 0 to High(fCutState) do
        fCutState[i] := $0;
    end;

    if xLength <> Length(fFFClusterState) then
    begin
      SetLength(fFFClusterState, xLength);
      for i := 0 to High(fFFClusterState) do
        fFFClusterState[i] := $0;
    end;

    if xLength <> Length(fSCMemberState) then
    begin
      SetLength(fSCMemberState, xLength);
      for i := 0 to High(fSCMemberState) do
        fSCMemberState[i] := $0;
    end;
  end;
end;
//------------------------------------------------------------------------------
end.

