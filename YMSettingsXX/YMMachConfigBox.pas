(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMSettingsGUI.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: Clearer Settings
| Process(es)...: -
| Description...: Graphical User Interface for YarnMaster Mach Config Settings
| Info..........: -
| Develop.system: Compaq Deskpro Pentium Pro 2000, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 15.01.01 | 1.00 | Kr | Initial release
| 25.01.01 | 1.01 | Kr | Interface INavChange implemented
| 22.03.02 | 1.01 | Wss| Strings missed at scanning files with Multilizer -> marked with // ivlm
|=============================================================================*)
unit YMMachConfigBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmLineLabel, mmCheckBox, mmComboBox, mmLabel, KeySpinEdit, PresetValue,
  mmEdit, YMParaUtils, YMParaDef, MachNodeComp, BaseGlobal, DialogConfigCode;
  //...........................................................................

resourcestring
  //...........................................................................
  rsCutRetriesHint = '(*)Schnittwiederholungen bei Schnittfehler bis zur Alarmintervention:'; //ivlm
  rsSPCheckLengthHint = '(*)Anlauflaenge waehrend dem die Spleisspruef-Einstellungen aktiv sind:'; //ivlm
  rsLongStopDefHint = '(*)Dauer bis eine stehende Spullstelle in den Lang-Stop-Zustand uebertritt:'; //ivlm
  rsDefaultSpeedHint = '(*)Standardgeschwindigkeit fuer Maschinen ohne Geschwindigkeitsinformation:'; //ivlm
  rsDefaultSpeedRampHint = '(*)Standardanlaufdauer fuer Maschinen ohne Geschwindigkeitsinformation:'; //ivlm
  rsHeadstockPositionRight = '(6)rechts'; //ivlm
  rsHeadstockPositionLeft = '(6)links'; //ivlm
  rsKnifePowerHigh = '(6)hoch'; //ivlm
  rsKnifePowerRegular = '(6)normal'; //ivlm

  //...........................................................................

const
  //...........................................................................
{(*}
  cVSBSCutRetries: TValueSpecRec = (
    minValue: cMinRepetitions - 1;
    maxValue: cMaxRepetitions - 1;
    format: '%1.0f';
    caption: '';
    measure: '';
    hint: rsCutRetriesHint;
    );

  cPTBSRepetition: array[0..4] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '3'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '5'; ));
  //...........................................................................

  cVSBSSPCheckLength: TValueSpecRec = (
    minValue: 0;
    maxValue: cMaxSpliceCheck/10;
    format: '%3.0f';
    caption: '';
    measure: '';
    hint: rsSPCheckLengthHint;
    );

  cPTBSSPCheckLength: array[0..13] of TPresetTextRec = (
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '15'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '25'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '35'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttFloat; text: '60'; ),
    (typ: ttFloat; text: '70'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '80'; ),
    (typ: ttFloat; text: '100'; ),
    (typ: ttFloat; text: '120'; ));
  //...........................................................................

  cVSBSLongStopDef: TValueSpecRec = (
    minValue: cMinLongStopDef/10;
    maxValue: cMaxLongStopDef/10;
    format: '%2.1f';
    caption: '';
    measure: '';
    hint: rsLongStopDefHint;
    );

  cPTBSLongStopDef: array[0..18] of TPresetTextRec = (
    (typ: ttFloat; text: '0.2'; ),
    (typ: ttFloat; text: '0.4'; ),
    (typ: ttFloat; text: '0.8'; ),
    (typ: ttFloat; text: '1.0'; ),
    (typ: ttFloat; text: '1.5'; ),
    (typ: ttFloat; text: '2.0'; ),
    (typ: ttFloat; text: '2.5'; ),
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '3.5'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '8.0'; ),
    (typ: ttFloat; text: '10.0'; ),
    (typ: ttFloat; text: '12.0'; ),
    (typ: ttFloat; text: '14.0'; ),
    (typ: ttFloat; text: '16.0'; ),
    (typ: ttFloat; text: '18.0'; ),
    (typ: ttFloat; text: '20.0'; ),
    (typ: ttFloat; text: '25.0'; ) );
  //...........................................................................

  cVSBSDefaultSpeed: TValueSpecRec = (
    minValue: cMinSpeed;
    maxValue: cMaxSpeed;
    format: '%4.0f';
    caption: '';
    measure: '';
    hint: rsDefaultSpeedHint;
    );

  cPTBSDefaultSpeed: array[0..18] of TPresetTextRec = (
    (typ: ttFloat; text: '500'; ),
    (typ: ttFloat; text: '550'; ),
    (typ: ttFloat; text: '600'; ),
    (typ: ttFloat; text: '650'; ),
    (typ: ttFloat; text: '700'; ),
    (typ: ttFloat; text: '750'; ),
    (typ: ttFloat; text: '800'; ),
    (typ: ttFloat; text: '850'; ),
    (typ: ttFloat; text: '900'; ),
    (typ: ttFloat; text: '950'; ),
    (typ: ttFloat; text: '1000'; ),
    (typ: ttFloat; text: '1050'; ),
    (typ: ttFloat; text: '1100'; ),
    (typ: ttFloat; text: '1150'; ),
    (typ: ttFloat; text: '1200'; ),
    (typ: ttFloat; text: '1300'; ),
    (typ: ttFloat; text: '1400'; ),
    (typ: ttFloat; text: '1500'; ),
    (typ: ttFloat; text: '1600'; ));
  //...........................................................................

  cVSBSDefaultSpeedRamp: TValueSpecRec = (
    minValue: cMinSpeedRamp;
    maxValue: cMaxSpeedRamp;
    format: '%2.0f';
    caption: '';
    measure: '';
    hint: rsDefaultSpeedRampHint;
    );

  cPTBSDefaultSpeedRamp: array[0..16] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '3'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '5'; ),
    (typ: ttFloat; text: '6'; ),
    (typ: ttFloat; text: '7'; ),
    (typ: ttFloat; text: '8'; ),
    (typ: ttFloat; text: '9'; ),
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '12'; ),
    (typ: ttFloat; text: '14'; ),
    (typ: ttFloat; text: '16'; ),
    (typ: ttFloat; text: '18'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '25'; ),
    (typ: ttFloat; text: '30' ));
{*)}
  //...........................................................................

type
//..............................................................................

  TGBMachConfigBox = class(TFrame, INavChange)
    mllGarnreiniger: TmmLineLabel;
    mllFacility: TmmLineLabel;
    mlaSensingHead: TmmLabel;
    mcobSensingHead: TmmComboBox;
    mcbAdjustCut: TmmCheckBox;
    mcbZeroAdjust: TmmCheckBox;
    mlbCutRetries: TmmLabel;
    mcbDriftConeChange: TmmCheckBox;
    mcbKnifeMonitor: TmmCheckBox;
    mllConfigCode: TmmLineLabel;
    mcbAdjustRemove: TmmCheckBox;
    mlbZESwVersionlb: TmmLabel;
    mlbZESwVersionStr: TmmLabel;
    mlbZESwOptionlb: TmmLabel;
    mlbZESwOptionStr: TmmLabel;
    medSPCheckLength: TKeySpinEdit;
    mlbSPCheckLength: TmmLabel;
    medCutRetries: TKeySpinEdit;
    mlbLongStopDef: TmmLabel;
    medLongStopDef: TKeySpinEdit;
    mplbLongStopDef: TmmLabel;
    mcbSpindleBlockOnAlarm: TmmCheckBox;
    mcbCountBlock: TmmCheckBox;
    mcbClusterBlock: TmmCheckBox;
    mcbSFIBlock: TmmCheckBox;
    mcbFFBlock: TmmCheckBox;
    mcbFFClusterBlock: TmmCheckBox;
    mcbCutFailBlock: TmmCheckBox;
    mcbExtMurItf: TmmCheckBox;
    mcbOneDrumPuls: TmmCheckBox;
    mcbConeChangeCondition: TmmCheckBox;
    mcobKnifePower: TmmComboBox;
    mlbKnifePower: TmmLabel;
    mlbCfgCodeALabel: TmmLabel;
    mlbCfgCodeA: TGBConfigCode;
    mlbCfgCodeBLabel: TmmLabel;
    mlbCfgCodeB: TGBConfigCode;
    mlbCfgCodeCLabel: TmmLabel;
    mlbCfgCodeC: TGBConfigCode;
    mplbSPCheckLength: TmmLabel;
    mlbHeadstock: TmmLabel;
    mcobHeadstock: TmmComboBox;
    mlaAWETypeStr: TmmLabel;
    mlaAWEType: TmmLabel;
    mlbDefaultSpeedRamp: TmmLabel;
    mplbDefaultSpeedRamp: TmmLabel;
    medDefaultSpeedRamp: TKeySpinEdit;
    mlbDefaultSpeed: TmmLabel;
    mplbDefaultSpeed: TmmLabel;
    medDefaultSpeed: TKeySpinEdit;
    mlbAWEMachineTypelb: TmmLabel;
    mlbAWEMachineTypeStr: TmmLabel;
    mcbUpperYarnCheck: TmmCheckBox;
    procedure SpindleBlockOnAlarmClick(aSender: TObject);
    procedure DoChange(aSender: TObject);
    procedure DoClick(aSender: TObject);
    procedure DoConfirm(aTag: Integer);
  private
    { Private declarations }
    fAdminMachNode: TAdminMachNode;

    fOnMachNodeChanged: TNotifyEvent;

    mMachineYMConfig: TYMMachineConfig;
    mYMSettingsUtils: TYMSettingsUtils;
    mDoChangedEnabled: Boolean;
    function CheckUserValue: boolean;
    function GetActiveGroup: Integer;
    function GetMachineConfig: TMachineYMConfigRec;

    procedure AlarmBlockCheckBoxesEnabled(aValue: Boolean);
    procedure InitComboAndEditBoxes;
    procedure NotifyChange;
    procedure SetActiveGroup(const aValue: Integer);
    procedure SetAdminMachNode(const aValue: TAdminMachNode);
//    procedure SetFullMachineSettings(aValue: TSettingsArr);
    procedure SetMachineConfig(const aValue: TMachineYMConfigRec);

    procedure UpdateAlarmBlockCheckBoxes;
    procedure UpdateCheckBoxes;
    procedure UpdateComboAndEditBoxes(aActiveGroup: Integer; aMachineConfig: TMachineYMConfigRec);
    procedure UpdateComponent;
    procedure UpdateConfigCode(aActiveGroup: Integer; aMachineConfig: TMachineYMConfigRec);
    procedure UpdateMachConfigBox;
    procedure UpdateStrings(aMachineConfig: TMachineYMConfigRec);
    function GetDependendSpdleRange: Boolean;
    function GetSpeedSimulation: Boolean;

  protected

  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;

    destructor Destroy; override;
(*
    property FullMachineSettings: TSettingsArr write SetFullMachineSettings;
    // Real Machine based MachConfig not used now
*)
    procedure SetDefaultMachineYMConfig;

  published
    property AdminMachNode: TAdminMachNode read fAdminMachNode write SetAdminMachNode default nil;
    property MachineConfig: TMachineYMConfigRec read GetMachineConfig write SetMachineConfig;
    property ActiveGroup: Integer read GetActiveGroup write SetActiveGroup;
    property OnMachNodeChanged: TNotifyEvent read fOnMachNodeChanged write fOnMachNodeChanged; //Nue

    property DependendSpdleRange: Boolean read GetDependendSpdleRange;
    property SpeedSimulation: Boolean read GetSpeedSimulation;

  end;
//..............................................................................

const
//..............................................................................
  cHeadstockPosition: array[ssOn..ssOff] of string =
    (rsHeadstockPositionRight, rsHeadstockPositionLeft);
//..............................................................................
  cKnifePower: array[ssOn..ssOff] of string =
    (rsKnifePowerHigh, rsKnifePowerRegular);
//..............................................................................
// procedure Register;
//..............................................................................

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}
//------------------------------------------------------------------------------

{
procedure Register;
begin
  RegisterComponents('MaConfig', [TGBMachConfigBox]);
end;
{}

//******************************************************************************
// TGBMachConfigBox
//******************************************************************************
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.AlarmBlockCheckBoxesEnabled(aValue: Boolean);
begin
  with mMachineYMConfig do
  begin
    mcbSpindleBlockOnAlarm.Checked := aValue;

    mcbCountBlock.Enabled := aValue;
    mcbClusterBlock.Enabled := aValue;
    mcbSFIBlock.Enabled := aValue;

    if FFBlockAvailable then
      mcbFFBlock.Enabled := aValue;
    if FFClusterBlockAvailable then
      mcbFFClusterBlock.Enabled := aValue;
    if CutFailBlockAvailable then
      mcbCutFailBlock.Enabled := aValue;
  end;
end;
//------------------------------------------------------------------------------

// Interface INavChange: CheckUserValue, Kr has no idea whatfor

function TGBMachConfigBox.CheckUserValue: boolean;
begin
  Result := True;
end;
//------------------------------------------------------------------------------

constructor TGBMachConfigBox.Create(aOwner: TComponent);
begin
  inherited;

//  if aOwner is TForm then
//    (aOwner as TForm).FormStyle := fsMDIForm;

  fAdminMachNode := nil;
  fOnMachNodeChanged := nil;

  mMachineYMConfig := TYMMachineConfig.Create;
  mYMSettingsUtils := TYMSettingsUtils.Create;

  mMachineYMConfig.MachineConfig := mYMSettingsUtils.InitMachineYMConfigRec;

  mDoChangedEnabled := True;

  mlbCfgCodeA.DialogCaption := 'ConfigCodeA';
  mlbCfgCodeB.DialogCaption := 'ConfigCodeB';
  mlbCfgCodeC.DialogCaption := 'ConfigCodeC';

  ActiveGroup := Low(MachineConfig.spec);

  InitComboAndEditBoxes;

  UpdateMachConfigBox;
end;
//------------------------------------------------------------------------------

destructor TGBMachConfigBox.Destroy;
begin
  mMachineYMConfig.Free;
  mYMSettingsUtils.Free;

  inherited;
end;
//------------------------------------------------------------------------------

// TmmComboBox.OnChange only!

procedure TGBMachConfigBox.DoChange(aSender: TObject);
begin

  if mDoChangedEnabled then
  begin

    mMachineYMConfig.RecordChanges;

    if aSender is TComboBox then
    begin
      with aSender as TComboBox, mMachineYMConfig do
      begin
        case Tag of
          0:
          begin                                      // mcobSensingHead
            SensingHead := TYMMachineConfig.IndexToSensigHeadType(ItemIndex);
            SetSensingHeadDependency;
          end;

          1:                            // mcobHeadstock
            begin
              if TYMMachineConfig.IndexToSwitchState(ItemIndex) = ssOn then
                HeadstockRight := True
              else
                HeadstockRight := False;
            end;

          2:                            // mcobKnifePower
            begin
              if TYMMachineConfig.IndexToSwitchState(ItemIndex) = ssOn then
                KnifePowerHigh := True
              else
                KnifePowerHigh := False;
            end;
        end;
      end;
    end
    else if aSender is TGBConfigCode then
      with aSender as TGBConfigCode, mMachineYMConfig do
      begin
        case Tag of
          0:                            // mlbCfgCodeA
            A := ConfigCode;
          1:                            // mlbCfgCodeB
            B := ConfigCode;
          2:                            // mlbCfgCodeC
            C := ConfigCode;

        end;
      end;

    UpdateConfigCode(ActiveGroup, MachineConfig);
    UpdateAlarmBlockCheckBoxes;
    UpdateCheckBoxes;

    NotifyChange;
  end;
end;
//------------------------------------------------------------------------------

// TmmCheckBox.OnClick only!

procedure TGBMachConfigBox.DoClick(aSender: TObject);
begin

  if mDoChangedEnabled then
  begin

    mMachineYMConfig.RecordChanges;

    if aSender is TmmCheckBox then

      with mMachineYMConfig do
      begin

        case TmmCheckBox(aSender).Tag of

          0:
            with mcbAdjustCut do
              CutBeforeAdjust := Checked;

          1:
            with mcbAdjustRemove do
              AdjustRemove := Checked;

          2:
            with mcbZeroAdjust do
              ZeroAdjust := Checked;

          3:
            with mcbDriftConeChange do
              DriftConeChange := Checked;

          4:
            with mcbKnifeMonitor do
              KnifeMonitor := Checked;

          5:
            with mcbUpperYarnCheck do
              UpperYarnCheck := Checked;

          6:
            with mcbExtMurItf do
              ExtMurItf := Checked;

          7:
            with mcbOneDrumPuls do
              OneDrumPuls := Checked;

          8:
            with mcbConeChangeCondition do
              ConeChangeCondition := Checked;

        end;

        UpdateComboAndEditBoxes(ActiveGroup, MachineConfig);
        UpdateConfigCode(ActiveGroup, MachineConfig);
        UpdateAlarmBlockCheckBoxes;

        NotifyChange;
      end;
  end;
end;
//------------------------------------------------------------------------------

// TKeySpinEdit.OnConfirm only!

procedure TGBMachConfigBox.DoConfirm(aTag: Integer);
var
  xMachineConfig: TMachineYMConfigRec;
begin

  if mDoChangedEnabled then
  begin

    mMachineYMConfig.RecordChanges;

    xMachineConfig := mMachineYMConfig.MachineConfig;

    case aTag of
      0:                                // CutRetries
        xMachineConfig.CutRetries := Round(medCutRetries.Value + 1);

      1:                                // CheckLength
        xMachineConfig.checkLen := Round(medSPCheckLength.Value);

      2:                                // LongSTopDef
        xMachineConfig.longStopDef := Round(medLongStopDef.Value * 60);

      3:                                // DefaultSpeed
        xMachineConfig.defaultSpeed := Round(medDefaultSpeed.Value);

      4:                                // DefaultSpeedRamp
        xMachineConfig.defaultSpeedRamp := Round(medDefaultSpeedRamp.Value);

    end;

    mMachineYMConfig.MachineConfig := xMachineConfig;

    NotifyChange;

  end;
end;
//------------------------------------------------------------------------------

function TGBMachConfigBox.GetActiveGroup: Integer;
begin

  Result := mMachineYMConfig.ActiveGroup;
end;
//------------------------------------------------------------------------------

function TGBMachConfigBox.GetDependendSpdleRange: Boolean;
begin

  Result := mMachineYMConfig.DependendSpdleRangeAvailable;
end;
//------------------------------------------------------------------------------

function TGBMachConfigBox.GetMachineConfig: TMachineYMConfigRec;
begin

  Result := mMachineYMConfig.MachineConfig;
end;
//------------------------------------------------------------------------------

function TGBMachConfigBox.GetSpeedSimulation: Boolean;
begin

  Result := mMachineYMConfig.SpeedSimulationAvailable;
end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.InitComboAndEditBoxes;
var
  xSh: TSensingHeadType;
  xSs: TSwitchState;
begin

// Sensing Head
  for xSh := Low(cSensingHeadTypeNames) to High(cSensingHeadTypeNames) do
    mcobSensingHead.Items.Insert(TYMMachineConfig.SensigHeadTypeToIndex(xSh),
      cSensingHeadTypeNames[xSh]);

  mcobSensingHead.DropDownCount := TYMMachineConfig.SensigHeadTypeToIndex(High(cSensingHeadTypeNames)) + 1;
//  mcobSensingHead.ItemIndex := TMachineYMConfig.SensigHeadTypeToIndex(mMachineYMConfig.GroupSpecificConfig.SensingHead);

// Headstock
  for xSs := Low(cHeadstockPosition) to High(cHeadstockPosition) do
    mcobHeadstock.Items.Insert(TYMMachineConfig.SwichStateToIndex(xSs),
      cHeadstockPosition[xSs]);
  mcobHeadstock.DropDownCount := TYMMachineConfig.SwichStateToIndex(High(cHeadstockPosition)) + 1;
(*
  if mMachineYMConfig.HeadstockRight then
    mcobHeadstock.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOn)
  else
    mcobHeadstock.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOff);
*)

// KnifePower
  for xSs := Low(cKnifePower) to High(cKnifePower) do
    mcobKnifePower.Items.Insert(TYMMachineConfig.SwichStateToIndex(xSs),
      cKnifePower[xSs]);
  mcobKnifePower.DropDownCount := TYMMachineConfig.SwichStateToIndex(High(cKnifePower)) + 1;
(*
  if mMachineYMConfig.KnifePowerHigh then
    mcobKnifePower.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOn)
  else
    mcobKnifePower.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOff);
*)

// Cut Retries
  medCutRetries.LoadValueSpec(cVSBSCutRetries, cPTBSRepetition);
// LongStopDef
  medLongStopDef.LoadValueSpec(cVSBSLongStopDef, cPTBSLongStopDef);
// SpliceCheckLength
  medSPCheckLength.LoadValueSpec(cVSBSSPCheckLength, cPTBSSPCheckLength);

// DefaultSpeed
  medDefaultSpeed.LoadValueSpec(cVSBSDefaultSpeed, cPTBSDefaultSpeed);
// DefaultSpeedRamp
  medDefaultSpeedRamp.LoadValueSpec(cVSBSDefaultSpeedRamp, cPTBSDefaultSpeedRamp);

end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.NotifyChange;
begin

  if mMachineYMConfig.Changed then
    if Assigned(fAdminMachNode) then
      AdminMachNode.ItemChanged(ucMachineYMConfigRec); //Nue
end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.SetActiveGroup(const aValue: Integer);
begin

  mMachineYMConfig.ActiveGroup := aValue;

  UpdateMachConfigBox;
end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.SetAdminMachNode(const aValue: TAdminMachNode);
begin

  if aValue <> fAdminMachNode then
  begin
    fAdminMachNode := aValue;
    if Assigned(fAdminMachNode) then
      fAdminMachNode.Add(Self);
  end;
end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.SetDefaultMachineYMConfig;
begin

  mMachineYMConfig.RecordChanges;

  mMachineYMConfig.SetDefaultMachineYMConfig;

  UpdateMachConfigBox;

  NotifyChange;
end;
//------------------------------------------------------------------------------
(*
procedure TGBMachConfigBox.SetFullMachineSettings(
  aValue: TSettingsArr);
begin

  mMachineYMConfig.MachineConfig := mYMSettingsUtils.ExtractMachineYMConfig(aValue);
end;
*)
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.SetMachineConfig(
  const aValue: TMachineYMConfigRec);
begin

  mMachineYMConfig.MachineConfig := aValue;

  UpdateMachConfigBox;
end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.SpindleBlockOnAlarmClick(aSender: TObject);
begin

  if mDoChangedEnabled then
  begin

    if aSender is TmmCheckBox then

      with mMachineYMConfig do
      begin

        mMachineYMConfig.RecordChanges;

        case TmmCheckBox(aSender).Tag of

          0:
            with mcbSpindleBlockOnAlarm do
            begin
              BlockEnabled := Checked;
              UpdateAlarmBlockCheckBoxes;
            end;

          1:
            with mcbCountBlock do
              CountBlock := Checked;

          2:
            with mcbClusterBlock do
              ClusterBlock := Checked;

          3:
            with mcbSFIBlock do
              SFIBlock := Checked;

          4:
            with mcbFFBlock do
              FFBlock := Checked;

          5:
            with mcbFFClusterBlock do
              FFClusterBlock := Checked;

          6:
            with mcbCutFailBlock do
              CutFailBlock := Checked;
        end;

        UpdateConfigCode(ActiveGroup, MachineConfig);

        NotifyChange;
      end;

  end;
end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.UpdateAlarmBlockCheckBoxes;
begin

  with mMachineYMConfig do
  begin
    AlarmBlockCheckBoxesEnabled(BlockEnabled);

    mcbCountBlock.Checked := CountBlock;
    mcbClusterBlock.Checked := ClusterBlock;
    mcbSFIBlock.Checked := SFIBlock;
    mcbFFBlock.Checked := FFBlock;
    mcbFFBlock.Enabled := FFBlockAvailable;
    mcbFFClusterBlock.Checked := FFClusterBlock;
    mcbFFClusterBlock.Enabled := FFClusterBlockAvailable;
    mcbCutFailBlock.Checked := CutFailBlock;
    mcbCutFailBlock.Enabled := CutFailBlockAvailable;

  end;
end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.UpdateCheckBoxes;
begin

  with mMachineYMConfig do
  begin

    mcbAdjustCut.Checked := CutBeforeAdjust;

    mcbAdjustRemove.Checked := AdjustRemove;
    mcbAdjustRemove.Enabled := AdjustRemoveAvailable;

    mcbZeroAdjust.Checked := ZeroAdjust;

    mcbDriftConeChange.Checked := DriftConeChange;

    mcbKnifeMonitor.Checked := KnifeMonitor;

    mcbUpperYarnCheck.Checked := UpperYarnCheck;
    mcbExtMurItf.Checked := ExtMurItf;
    mcbExtMurItf.Enabled := ExtMurItfAvailable;

    mcbOneDrumPuls.Checked := OneDrumPuls;
    mcbOneDrumPuls.Enabled := OneDrumPulsAvailable;

    mcbConeChangeCondition.Checked := ConeChangeCondition;

  end;

end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.UpdateComboAndEditBoxes(aActiveGroup: Integer;
  aMachineConfig: TMachineYMConfigRec);
begin
  with aMachineConfig.spec[aActiveGroup] do
  begin
    mcobSensingHead.ItemIndex := TYMMachineConfig.SensigHeadTypeToIndex(sensingHead);
    mlaAWETypeStr.Caption := cAWETypeNames[aWEType];
  end;

  with aMachineConfig do
  begin
// Knife Power
    if mMachineYMConfig.KnifePowerHigh then
      mcobKnifePower.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOn)
    else
      mcobKnifePower.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOff);
    mcobKnifePower.Enabled := mMachineYMConfig.KnifePowerHighAvailable;

    if mMachineYMConfig.HeadstockRight then
      mcobHeadstock.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOn)
    else
      mcobHeadstock.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOff);

// CutRetries
    medCutRetries.Value := cutRetries - 1;
    medCutRetries.Enabled := mMachineYMConfig.CutRetriesAvailable;

// Splice Check Length
    medSPCheckLength.Value := checkLen;

// LongSTopDef
    medLongStopDef.Value := longStopDef / 60;

    medDefaultSpeed.Value := defaultSpeed;
    medDefaultSpeed.Enabled := mMachineYMConfig.DefaultSpeedAvailable;

    medDefaultSpeedRamp.Value := defaultSpeedRamp;
    medDefaultSpeedRamp.Enabled := mMachineYMConfig.DefaultSpeedRampAvailable;

  end;
end;
//------------------------------------------------------------------------------
// Interface INavChange: UpdateComponent is called by the Administrator on an
// AdminMachNode.ItemChanged(ucMachineYMConfigRec)  event.

procedure TGBMachConfigBox.UpdateComponent;
begin

  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.UpdateConfigCode(aActiveGroup: Integer;
  aMachineConfig: TMachineYMConfigRec);
begin

  with aMachineConfig.spec[aActiveGroup] do
  begin
    mlbCfgCodeA.ConfigCode := configA;
    mlbCfgCodeB.ConfigCode := configB;
    mlbCfgCodeC.ConfigCode := configC;
  end;
end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.UpdateMachConfigBox;
begin

  mDoChangedEnabled := False;

  UpdateComboAndEditBoxes(ActiveGroup, mMachineYMConfig.MachineConfig);

  UpdateStrings(mMachineYMConfig.MachineConfig);

  UpdateCheckBoxes;

  UpdateAlarmBlockCheckBoxes;

  UpdateConfigCode(ActiveGroup, mMachineYMConfig.MachineConfig);

  mDoChangedEnabled := True;

end;
//------------------------------------------------------------------------------

procedure TGBMachConfigBox.UpdateStrings(
  aMachineConfig: TMachineYMConfigRec);
var
  i: Integer;
begin
  with aMachineConfig do
  begin

    i := Low(frontSWVersion);
    mlbZESwVersionStr.Caption := '';
    while (i <= High(frontSWVersion)) and (frontSWVersion[i] <> 0) do
    begin

      mlbZESwVersionStr.Caption := mlbZESwVersionStr.Caption +
        Char(frontSWVersion[i]);
      Inc(i);
    end;

    if frontSWOption > fSwOEnd then
      frontSWOption := fSwOEnd;
    mlbZESwOptionStr.Caption := cFrontSwOption[frontSWOption];

    if aWEMachType > amtDemoWinder then
      aWEMachType := amtUnknown;

    mlbAWEMachineTypeStr.Caption := cAWEMachTypNames[aWEMachType];
  end;
end;
//------------------------------------------------------------------------------

end.

