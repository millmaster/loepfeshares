object GUIYMSettings: TGUIYMSettings
  Left = 0
  Top = 0
  Width = 1010
  Height = 595
  TabOrder = 0
  object mPageControl: TmmPageControl
    Left = 0
    Top = 0
    Width = 1010
    Height = 595
    ActivePage = mQuality
    Align = alClient
    TabHeight = 20
    TabOrder = 0
    object mQuality: TTabSheet
      Caption = '(*)&Qualitaetsueberwachung'
      object mStructureGroupBox: TmmGroupBox
        Left = 0
        Top = 0
        Width = 706
        Height = 565
        Align = alClient
        Caption = '(*)Garnstruktur'
        TabOrder = 1
        CaptionSpace = True
        object mClassMatrix: TQualityMatrix
          Left = 10
          Top = 17
          Width = 594
          Height = 383
          Color = clWhite
          LastCutMode = lcNone
          LastCutColor = clLime
          LastCutField = 0
          MatrixType = mtShortLongThin
          MatrixSubType = mstNone
          ActiveColor = clAqua
          ActiveVisible = True
          Anchors = [akLeft, akTop, akRight, akBottom]
          ChannelColor = clRed
          ChannelStyle = psSolid
          ChannelVisible = True
          ClusterColor = clPurple
          ClusterStyle = psSolid
          ClusterVisible = False
          CutsColor = clRed
          DefectsColor = clBlack
          DisableMessage = 'Quality Matrix disabled'
          DisplayMode = dmValues
          DotsColor = clBlack
          Enabled = True
          InactiveColor = 9502719
          MatrixMode = mmSelectSettings
          SpliceColor = clBlue
          SpliceStyle = psSolid
          SpliceVisible = False
          SubFieldX = 0
          SubFieldY = 0
          ZeroLimit = 0.01
        end
        object mCurveSelection: TmmPanel
          Left = 452
          Top = 22
          Width = 132
          Height = 62
          Anchors = [akTop, akRight]
          BevelOuter = bvNone
          TabOrder = 3
          object mcbChannelCurve: TmmCheckBox
            Left = 6
            Top = 6
            Width = 123
            Height = 17
            Caption = '(13)Kanal Kurve'
            Checked = True
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            State = cbChecked
            TabOrder = 0
            Visible = True
            OnClick = mcbChannelCurveClick
            AutoLabel.LabelPosition = lpLeft
          end
          object mcbSpliceCurve: TmmCheckBox
            Left = 6
            Top = 38
            Width = 121
            Height = 17
            Caption = '(13)Spleiss Kurve'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 2
            Visible = True
            OnClick = mcbSpliceCurveClick
            AutoLabel.LabelPosition = lpLeft
          end
          object mcbClusterCurve: TmmCheckBox
            Left = 6
            Top = 22
            Width = 120
            Height = 17
            Caption = '(13)Fehlerschwarm'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 1
            Visible = True
            OnClick = mcbClusterCurveClick
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mYarnCountEditBox: TYarnCountEditBox
          Left = 8
          Top = 406
          Width = 122
          Height = 153
          Anchors = [akLeft, akBottom]
          Caption = '(11)Garnnummer'
          TabOrder = 1
          CaptionSpace = True
          CaptionWidth = 50
          MeasureWidth = 12
          ReadOnly = False
          ValueWidth = 44
          Plus = True
          YarnCount = 32000
          YarnUnit = yuNm
        end
        object mChannelEditBox: TChannelEditBox
          Left = 612
          Top = 12
          Width = 86
          Height = 195
          Anchors = [akTop, akRight]
          Caption = '(8)Kanal'
          TabOrder = 0
          CaptionSpace = True
          CaptionWidth = 23
          MeasureWidth = 16
          ReadOnly = False
          ValueWidth = 31
          Printable = False
        end
        object mSpliceEditBox: TSpliceEditBox
          Left = 612
          Top = 336
          Width = 86
          Height = 223
          Anchors = [akRight, akBottom]
          Caption = '(8)Spleiss'
          TabOrder = 4
          CaptionSpace = True
          CaptionWidth = 23
          MeasureWidth = 16
          ReadOnly = False
          ValueWidth = 31
          Printable = False
          UpperYarn = True
        end
        object mSFIEditBox: TSFIEditBox
          Left = 278
          Top = 406
          Width = 142
          Height = 89
          Anchors = [akLeft, akBottom]
          Caption = '(11)SFI/D'
          Enabled = False
          TabOrder = 5
          CaptionSpace = True
          CaptionWidth = 60
          MeasureWidth = 16
          ReadOnly = False
          ValueWidth = 50
        end
        object mFaultClusterEditBox: TFaultClusterEditBox
          Left = 136
          Top = 406
          Width = 136
          Height = 89
          Anchors = [akLeft, akBottom]
          Caption = '(11)Fehlerschwarm'
          TabOrder = 6
          CaptionSpace = True
          CaptionWidth = 75
          MeasureWidth = 10
          ReadOnly = False
          ValueWidth = 35
        end
      end
      object mFFGroupBox: TmmGroupBox
        Left = 706
        Top = 0
        Width = 296
        Height = 565
        Align = alRight
        Caption = '(*)Fremdfasern'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        CaptionSpace = True
        object mFFMatrix: TQualityMatrix
          Left = 10
          Top = 17
          Width = 279
          Height = 192
          Color = clWhite
          LastCutMode = lcNone
          LastCutColor = clLime
          LastCutField = 0
          MatrixType = mtSiro
          MatrixSubType = mstSiroH
          ActiveColor = clAqua
          ActiveVisible = True
          ChannelColor = clSilver
          ChannelStyle = psSolid
          ChannelVisible = False
          ClusterColor = clSilver
          ClusterStyle = psSolid
          ClusterVisible = False
          CutsColor = clRed
          DefectsColor = clBlack
          DisableMessage = 'Quality Matrix disabled'
          DisplayMode = dmValues
          DotsColor = clBlack
          Enabled = True
          InactiveColor = 9502719
          MatrixMode = mmSelectCutFields
          SpliceColor = clSilver
          SpliceStyle = psSolid
          SpliceVisible = False
          SubFieldX = 0
          SubFieldY = 0
          ZeroLimit = 0.01
        end
        object mFFClusterEditBox: TFFClusterEditBox
          Left = 10
          Top = 216
          Width = 279
          Height = 65
          Caption = '(11)FF-Cluster'
          Enabled = False
          TabOrder = 1
          CaptionSpace = True
          CaptionWidth = 65
          MeasureWidth = 10
          ReadOnly = False
          ValueWidth = 35
          object mFFClusterButton: TmmButton
            Left = 165
            Top = 22
            Width = 100
            Height = 25
            Caption = '(16)FF-Einstellung'
            TabOrder = 2
            Visible = True
            OnClick = mFFClusterButtonClick
            AutoLabel.LabelPosition = lpLeft
          end
        end
      end
    end
    object mSetup: TTabSheet
      Caption = '(*)&Grundeinstellungen'
      ImageIndex = 1
      object mGBBasicProdPara: TGBBasicProdPara
        Left = 0
        Top = 0
        Width = 1002
        Height = 565
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object mpnSensingHeadClass: TmmPanel
    Left = 416
    Top = 0
    Width = 185
    Height = 22
    BevelOuter = bvLowered
    Caption = '(28)Tastkopf-Klasse'
    TabOrder = 1
  end
end
