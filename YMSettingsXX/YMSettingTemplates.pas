(*=========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMTemplateSettings.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Supports template settings for YarnMaster memories
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 05.02.2002  1.00  Kr  | Initial Release,
|=========================================================================================*)
unit YMSettingTemplates;

interface
uses
  YMParaDef, MMUGlobal;
//  BaseGlobal, MMUGlobal, LoepfeGlobal, Windows, sysutils, classes;
const

//------------------------------------------------------------------------------
// Memories
//        sw:             cChOff;
//------------------------------------------------------------------------------
{(*}

 cYMSettingsMemory01: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            600;
        sw:             cChOn;
      );
      short: (
        dia:            260;
        sw:             cChOn;
      );
      shortLen:         13;
      long: (
        dia:            130;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            75;
        sw:             cChOn;
      );
      thinLen:          900;
      splice: (
        len:            200;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            500;
        sw:             cChOff;
      );
      short: (
        dia:            260;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            130;
        sw:             cChOn;
      );
      longLen:          200;
      thin: (
        dia:            80;
        sw:             cChOff;
      );
      thinLen:          900;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $00, $80, $80, $E0, $F0, $F8, $FC, $FE,
                  $E0, $80, $00, $00, $00, $80, $C0, $F0);
    siroClear: ( $00, $00, $E0, $F0, $F8, $F8, $FC, $FC );

   additional: (
     option: (
       diaDiff:        250;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     135;
        clusterLength:  0;
        clusterDefects: 30;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     250;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            350;
      lower:            350;
      rep:              2;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        0;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ( $00, $00, $E0, $F0, $F8, $F8, $FC, $FC );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsMemory02: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            500;
        sw:             cChOn;
      );
      short: (
        dia:            220;
        sw:             cChOn;
      );
      shortLen:         13;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          1200;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            700;
        sw:             cChOn;
      );
      short: (
        dia:            220;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          150;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );
    classClear: ( $00, $80, $E0, $F0, $FC, $FE, $FF, $FF,
                  $E0, $C0, $80, $00, $00, $C0, $E0, $F0);
    siroClear: ( $00, $00, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        200;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     212;
        clusterLength:  8000;
        clusterDefects: 60;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     200;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            250;
      lower:            250;
      rep:              1;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        0;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ( $00, $00, $F8, $FC, $FC, $FC, $FC, $FC );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsMemory03: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            500;
        sw:             cChOn;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         13;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            700;
        sw:             cChOn;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          150;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );
   classClear: ( $80, $C0, $F0, $F8, $FC, $FE, $FF, $FF,
                  $F0, $C0, $80, $00, $00, $C0, $E0, $F8);
    siroClear: ($00, $F0, $F8, $FC, $FC, $FC, $FC, $FC);

   additional: (
     option: (
       diaDiff:        180;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     205;
        clusterLength:  8000;
        clusterDefects: 30;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     180;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            250;
      lower:            250;
      rep:              2;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        0;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ( $00, $F0, $F8, $FC, $FC, $FC, $FC, $FC );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsMemory04: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            500;
        sw:             cChOn;
      );
      short: (
        dia:            210;
        sw:             cChOn;
      );
      shortLen:         22;
      long: (
        dia:            120;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            75;
        sw:             cChOn;
      );
      thinLen:          600;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            500;
        sw:             cChOff;
      );
      short: (
        dia:            240;
        sw:             cChOn;
      );
      shortLen:         30;
      long: (
        dia:            130;
        sw:             cChOn;
      );
      longLen:          120;
      thin: (
        dia:            80;
        sw:             cChOff;
      );
      thinLen:          900;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );
    classClear: ( $C0, $C0, $F0, $F0, $FE, $FE, $FE, $FE,
                  $E0, $C0, $00, $00, $00, $F0, $F8, $F8);
    siroClear: ($00, $C0, $F0, $F8, $FC, $FC, $FC, $FC);

   additional: (
     option: (
       diaDiff:        160;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     135;
        clusterLength:  0;
        clusterDefects: 30;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     160;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            300;
      lower:            300;
      rep:              2;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        0;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ($00, $C0, $F0, $F8, $FC, $FC, $FC, $FC);
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsMemory05: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            700;
        sw:             cChOn;
      );
      short: (
        dia:            280;
        sw:             cChOn;
      );
      shortLen:         16;
      long: (
        dia:            130;
        sw:             cChOn;
      );
      longLen:          600;
      thin: (
        dia:            75;
        sw:             cChOn;
      );
      thinLen:          900;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            700;
        sw:             cChOn;
      );
      short: (
        dia:            260;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            130;
        sw:             cChOn;
      );
      longLen:          150;
      thin: (
        dia:            75;
        sw:             cChOn;
      );
      thinLen:          600;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $00, $00, $80, $80, $E0, $F8, $FC, $FE,
                  $E0, $80, $00, $00, $00, $80, $C0, $E0);
    siroClear: ( $00, $00, $F0, $F8, $F8, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        150;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     191;
        clusterLength:  8000;
        clusterDefects: 30;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     150;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            280;
      lower:            280;
      rep:              2;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        0;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ($00, $00, $F0, $F8, $F8, $FC, $FC, $FC);
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsMemory06: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            700;
        sw:             cChOn;
      );
      short: (
        dia:            260;
        sw:             cChOn;
      );
      shortLen:         20;
      long: (
        dia:            130;
        sw:             cChOn;
      );
      longLen:          600;
      thin: (
        dia:            75;
        sw:             cChOn;
      );
      thinLen:          900;
      splice: (
        len:            200;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            700;
        sw:             cChOn;
      );
      short: (
        dia:            260;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            130;
        sw:             cChOn;
      );
      longLen:          200;
      thin: (
        dia:            70;
        sw:             cChOn;
      );
      thinLen:          600;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $00, $00, $80, $80, $F0, $F8, $FC, $FE,
                  $E0, $80, $00, $00, $00, $80, $C0, $F8);
    siroClear: ( $00, $00, $F8, $F8, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        120;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     184;
        clusterLength:  4000;
        clusterDefects: 10;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     120;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            200;
      lower:            200;
      rep:              2;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        0;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ( $00, $00, $F8, $F8, $FC, $FC, $FC, $FC );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsMemory07: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            600;
        sw:             cChOn;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         22;
      long: (
        dia:            122;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            82;
        sw:             cChOn;
      );
      thinLen:          400;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            700;
        sw:             cChOn;
      );
      short: (
        dia:            220;
        sw:             cChOn;
      );
      shortLen:         30;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          150;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          300;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $C0, $C0, $E0, $F0, $F8, $FC, $FE, $FE,
                  $F8, $E0, $C0, $C0, $00, $C0, $E0, $F8);
    siroClear: ($00, $00, $F8, $F8, $FC, $FC, $FE, $FE);

   additional: (
     option: (
       diaDiff:        100;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     135;
        clusterLength:  0;
        clusterDefects: 30;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     100;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            400;
      lower:            0;
      rep:              2;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        0;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ( $00, $00, $F8, $F8, $FC, $FC, $FE, $FE );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsMemory08: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            600;
        sw:             cChOn;
      );
      short: (
        dia:            250;
        sw:             cChOn;
      );
      shortLen:         16;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          300;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          300;
      splice: (
        len:            200;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            700;
        sw:             cChOn;
      );
      short: (
        dia:            250;
        sw:             cChOn;
      );
      shortLen:         25;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          120;
      thin: (
        dia:            75;
        sw:             cChOn;
      );
      thinLen:          250;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $00, $00, $80, $80, $F0, $F0, $FE, $FE,
                  $F8, $C0, $80, $00, $80, $E0, $F0, $F8);
    siroClear: ( $C0, $F0, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        180;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     198;
        clusterLength:  1500;
        clusterDefects: 4;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     180;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            250;
      lower:            250;
      rep:              2;
      fill0:            0;
    );

   fFCluster: (
     obsLength:         0;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ( $C0, $F0, $F8, $FC, $FC, $FC, $FC, $FC );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsMemory09: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            150;
        sw:             cChOn;
      );
      short: (
        dia:            110;
        sw:             cChOn;
      );
      shortLen:         10;
      long: (
        dia:            104;
        sw:             cChOn;
      );
      longLen:          60;
      thin: (
        dia:            94;
        sw:             cChOn;
      );
      thinLen:          60;
      splice: (
        len:            60;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            150;
        sw:             cChOn;
      );
      short: (
        dia:            110;
        sw:             cChOn;
      );
      shortLen:         10;
      long: (
        dia:            104;
        sw:             cChOn;
      );
      longLen:          60;
      thin: (
        dia:            94;
        sw:             cChOn;
      );
      thinLen:          60;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );



    classClear: ($FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF,
                  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF);
    siroClear: ( $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF);

   additional: (
     option: (
       diaDiff:        30;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     105;
        clusterLength:  10;
        clusterDefects: 1;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     30;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            50;
      lower:            50;
      rep:              2;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        10;
      defects:          1;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ( $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsMemory10: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for B�ler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            600;
        sw:             cChOff;
      );
      short: (
        dia:            260;
        sw:             cChOff;
      );
      shortLen:         13;
      long: (
        dia:            130;
        sw:             cChOff;
      );
      longLen:          400;
      thin: (
        dia:            75;
        sw:             cChOff;
      );
      thinLen:          900;
      splice: (
        len:            200;
        sw:             cChOff;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            500;
        sw:             cChOff;
      );
      short: (
        dia:            260;
        sw:             cChOff;
      );
      shortLen:         40;
      long: (
        dia:            130;
        sw:             cChOff;
      );
      longLen:          200;
      thin: (
        dia:            80;
        sw:             cChOff;
      );
      thinLen:          900;
      upperYarn: (
        dia:            150;
        sw:             cChOff;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $00, $00, $00, $00, $00, $00, $00, $00,
                  $00, $00, $00, $00, $00, $00, $00, $00);
    siroClear: ( $00, $00, $00, $00, $00, $00, $00, $00 );

   additional: (
     option: (
       diaDiff:        0;
       yarnCnt:        cInitYarnCount;
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     135;
        clusterLength:  0;
        clusterDefects: 30;
      );
      configA:          Word($001B);
      configB:          cCCBSIDefault;
      configC:          cCCCSIDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     0;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultCutRetries;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            0;
      lower:            0;
      rep:              2;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        0;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
      siroClear: ( $00, $00, $00, $00, $00, $00, $00, $00 );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

{*)}
  //...........................................................................

//------------------------------------------------------------------------------
// Free functions
//------------------------------------------------------------------------------
  //............................................................................

function GetSettingTemplate(aIndex: Integer): TYMSettingsRec;
  //............................................................................


implementation
//------------------------------------------------------------------------------

function GetSettingTemplate(aIndex: Integer): TYMSettingsRec;
begin
  case aIndex of
    1:
      Result := cYMSettingsMemory01;
    2:
      Result := cYMSettingsMemory02;
    3:
      Result := cYMSettingsMemory03;
    4:
      Result := cYMSettingsMemory04;
    5:
      Result := cYMSettingsMemory05;
    6:
      Result := cYMSettingsMemory06;
    7:
      Result := cYMSettingsMemory07;
    8:
      Result := cYMSettingsMemory08;
    9:
      Result := cYMSettingsMemory09;
    10:
      Result := cYMSettingsMemory10;
    else
      Result := cYMSettingsMemory01;
  end;
end;

end.

