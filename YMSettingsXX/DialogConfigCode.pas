(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMParaEditBox.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Dialog for ConfigCode Bits.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 07.02.2000  0.00  Kr  | Initial Release
| 19.12.2003  0.01  Kr  | rsConfigCodeA wihout /ivlm
|=============================================================================*)
unit DialogConfigCode;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, StdCtrls, Buttons, mmBitBtn, CheckLst, mmCheckListBox, mmLabel,
  mmButton;

resourcestring
  rsConfigCodeA = 'CfgA';

//------------------------------------------------------------------------------
// Channel Values definitions
//------------------------------------------------------------------------------
  //...........................................................................

type
  //...........................................................................
  TGBDialogConfigCode = class(TmmDialog)
    mbSet: TmmBitBtn;
    mbReject: TmmBitBtn;
    mclbConfigBits: TmmCheckListBox;
    mbClose: TmmButton;
    procedure mbSetClick(Sender: TObject);
    procedure mbRejectClick(Sender: TObject);
    procedure DoDialogCloseClick(aSender: TObject);
  private
    { Private declarations }
    fOnChange: TNotifyEvent;
    mConfigCode: Word;

    function CheckConfigCodeBit(aConfigCode: Word;
      aBitPosition: Integer): Boolean;
    function GetChecked: Word;
    function GetConfigCode: Word;
    function SetConfigCodeBit(aConfigCode: Word;
      aBitPosition: Integer; aValue: Boolean): Word;

    procedure SetChecked(const aValue: Word);
    procedure SetConfigCode(const aValue: Word);
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;

    property ConfigCode: Word read GetConfigCode write SetConfigCode;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  end;
  //...........................................................................

  TGBConfigCode = class(TmmLabel)
  private
    { Private declarations }
    fConfigCode: Word;
    fDialogCaption: TCaption;
    fOnChange: TNotifyEvent;

    mDialog: TGBDialogConfigCode;

    function GetConfigCode: Word;

    procedure DoChange(aSender: TObject);
    procedure DoMouseDown(aSender: TObject;
      aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer);
    procedure SetDialogCaption(const aValue: TCaption);
    procedure SetConfigCode(const aValue: Word);
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

  published
    property ConfigCode: Word read GetConfigCode write SetConfigCode;
    property DialogCaption: TCaption read fDialogCaption write SetDialogCaption;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;

  end;

var
  GBDialogConfigCode: TGBDialogConfigCode;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

//******************************************************************************
// TGBDialogConfigCode
//******************************************************************************
//------------------------------------------------------------------------------
const
  cWordBit0: Word = 1;

function TGBDialogConfigCode.CheckConfigCodeBit(aConfigCode: Word;
  aBitPosition: Integer): Boolean;
var xMask: Word;
begin
  xMask := cWordBit0 shl aBitPosition;

  if xMask = (xMask and aConfigCode) then
    Result := True
  else
    Result := False;
end;
//------------------------------------------------------------------------------

constructor TGBDialogConfigCode.Create(aOwner: TComponent);
begin
  inherited;
  fOnChange := nil;
  ConfigCode := $A5A5;
  Caption := rsConfigCodeA;
end;
//------------------------------------------------------------------------------

function TGBDialogConfigCode.GetChecked: Word;
var
  i: Integer;
begin
  Result := 0;
  with mclbConfigBits do
    for i := 0 to Items.Count - 1 do
    begin
      Result := SetConfigCodeBit(Result, i, Checked[i]);
    end;
end;
//------------------------------------------------------------------------------

function TGBDialogConfigCode.GetConfigCode: Word;
begin

  Result := mConfigCode;
end;
//------------------------------------------------------------------------------

procedure TGBDialogConfigCode.SetChecked(const aValue: Word);
var
  i: Integer;
begin

  with mclbConfigBits do
    for i := 0 to Items.Count - 1 do
    begin
      Checked[i] := CheckConfigCodeBit(aValue, i);
    end;
end;
//------------------------------------------------------------------------------

procedure TGBDialogConfigCode.SetConfigCode(const aValue: Word);
begin

  mConfigCode := aValue;
  SetChecked(aValue);
end;
//------------------------------------------------------------------------------

function TGBDialogConfigCode.SetConfigCodeBit(aConfigCode: Word;
  aBitPosition: Integer; aValue: Boolean): Word;
var xMask: Word;
begin
  xMask := cWordBit0 shl aBitPosition;

  if aValue then
    Result := xMask or aConfigCode
  else
    Result := (not xMask) and aConfigCode;
end;
//------------------------------------------------------------------------------

procedure TGBDialogConfigCode.mbRejectClick(Sender: TObject);
begin
  inherited;

  SetChecked(mConfigCode);
end;
//------------------------------------------------------------------------------

procedure TGBDialogConfigCode.mbSetClick(Sender: TObject);
begin
  inherited;

  if mConfigCode <> GetChecked then
  begin
    mConfigCode := GetChecked;
    if Assigned(fOnChange) then
      fOnChange(self);
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// TGBConfigCode
//******************************************************************************
//------------------------------------------------------------------------------

constructor TGBConfigCode.Create(aOwner: TComponent);
begin
  inherited;

  fOnChange := nil;
  mDialog := nil;
  ConfigCode := $A5A5;
  OnMouseDown := DoMouseDown;
end;
//------------------------------------------------------------------------------

destructor TGBConfigCode.Destroy;
begin

  mDialog.Free;

  inherited;
end;
//------------------------------------------------------------------------------

procedure TGBConfigCode.DoChange(aSender: TObject);
begin
  if Assigned(fOnChange) then
    fOnChange(self);
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

procedure TGBConfigCode.DoMouseDown(aSender: TObject;
  aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer);
var
  xPoint: TPoint;

begin

  if [ssAlt] <= aShift then
  begin

    if not Assigned(mDialog) then
      mDialog := TGBDialogConfigCode.Create(self);

    with mDialog do
    begin
      Caption := fDialogCaption;
      ConfigCode := fConfigCode;
      OnChange := DoChange;
      xPoint := Self.ClientToScreen(Point(aX, aY));
      Left := xPoint.x - (Width div 2);
      Top := xPoint.y - (Height + 8);

      FormStyle := fsStayOnTop;

      Show;

    end;

  end
end;
//------------------------------------------------------------------------------

function TGBConfigCode.GetConfigCode: Word;
begin
  if Assigned(mDialog) then
    fConfigCode := mDialog.ConfigCode;

  Result := fConfigCode;
end;
//------------------------------------------------------------------------------

procedure TGBConfigCode.SetConfigCode(const aValue: Word);
begin

  fConfigCode := aValue;
  Caption := Format('%d', [fConfigCode]);

  if Assigned(mDialog) then
    mDialog.ConfigCode := fConfigCode;
end;
//------------------------------------------------------------------------------

procedure TGBConfigCode.SetDialogCaption(const aValue: TCaption);
begin
  fDialogCaption := aValue;
  if Assigned(mDialog) then
    mDialog.Caption := fDialogCaption;
end;
//------------------------------------------------------------------------------

procedure TGBDialogConfigCode.DoDialogCloseClick(aSender: TObject);
begin
  Close;
end;

end.

