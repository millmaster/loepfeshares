object GUIYMSettings: TGUIYMSettings
  Left = 0
  Top = 0
  Width = 998
  Height = 602
  TabOrder = 0
  OnResize = FrameResize
  object mPageControl: TmmPageControl
    Left = 0
    Top = 0
    Width = 998
    Height = 602
    ActivePage = mQuality
    Align = alClient
    TabHeight = 20
    TabOrder = 0
    object mQuality: TTabSheet
      Caption = '(*)&Qualitaetsueberwachung'
      object mStructureGroupBox: TmmGroupBox
        Left = 0
        Top = 0
        Width = 727
        Height = 572
        Align = alClient
        Caption = '(*)Garnstruktur'
        TabOrder = 1
        CaptionSpace = True
        object mmPanel2: TmmPanel
          Left = 507
          Top = 15
          Width = 218
          Height = 555
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object mChannelEditBox: TChannelEditBox
            Left = 20
            Top = 0
            Width = 88
            Height = 158
            Anchors = [akTop, akRight]
            Caption = '(8)Kanal'
            TabOrder = 0
            CaptionSpace = True
            ReadOnly = False
          end
          object mSpliceEditBox: TSpliceEditBox
            Left = 111
            Top = 0
            Width = 88
            Height = 180
            Anchors = [akTop, akRight]
            Caption = '(8)Spleiss'
            TabOrder = 1
            CaptionSpace = True
            ReadOnly = False
          end
          object mYarnCountEditBox: TYarnCountEditBox
            Left = 2
            Top = 224
            Width = 216
            Height = 154
            Anchors = [akLeft, akBottom]
            Caption = '(11)Garnnummer'
            TabOrder = 2
            CaptionSpace = True
            ReadOnly = False
          end
          object mSFIEditBox: TSFIEditBox
            Left = 2
            Top = 387
            Width = 136
            Height = 80
            Anchors = [akLeft, akBottom]
            Caption = '(11)SFI/D'
            Enabled = False
            TabOrder = 3
            OnClick = mSFIEditBoxClick
            CaptionSpace = True
            ReadOnly = False
          end
          object mVCVEditBox: TVCVEditBox
            Left = 2
            Top = 473
            Width = 136
            Height = 80
            Anchors = [akLeft, akBottom]
            Caption = '(11)VCV'
            TabOrder = 4
            CaptionSpace = True
            ReadOnly = False
          end
        end
        object mmPanel1: TmmPanel
          Left = 2
          Top = 15
          Width = 505
          Height = 555
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object mmPanel3: TmmPanel
            Left = 0
            Top = 440
            Width = 505
            Height = 115
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object mFaultClusterEditBox: TFaultClusterEditBox
              Left = 228
              Top = 1
              Width = 262
              Height = 112
              Anchors = [akTop, akRight]
              Caption = '(11)Fehlerschwarm'
              TabOrder = 0
              CaptionSpace = True
              ReadOnly = False
            end
          end
          object mmPanel4: TmmPanel
            Left = 0
            Top = 0
            Width = 505
            Height = 440
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 2
            TabOrder = 1
            object mClassMatrix: TQualityMatrix
              Left = 2
              Top = -6
              Width = 505
              Height = 388
              Color = clWhite
              LastCutColor = clLime
              LastCutField = 0
              LastCutMode = lcNone
              MatrixSubType = mstNone
              MatrixType = mtShortLongThin
              ActiveColor = clAqua
              ActiveVisible = True
              Anchors = [akLeft, akTop, akRight]
              ChannelColor = clRed
              ChannelStyle = psSolid
              ChannelVisible = True
              ClusterColor = clPurple
              ClusterStyle = psSolid
              ClusterVisible = False
              CutsColor = clRed
              DefectsColor = clBlack
              DisableMessage = 'Quality Matrix disabled'
              DisplayMode = dmValues
              DotsColor = clBlack
              Enabled = True
              InactiveColor = 9502719
              MatrixMode = mmSelectSettings
              SpliceColor = clBlue
              SpliceStyle = psSolid
              SpliceVisible = False
              SubFieldX = 0
              SubFieldY = 0
              ZeroLimit = 0.01
            end
            object mSpliceMatrix: TQualityMatrix
              Left = 2
              Top = -6
              Width = 505
              Height = 388
              Color = clWhite
              LastCutColor = clLime
              LastCutField = 0
              LastCutMode = lcNone
              MatrixSubType = mstNone
              MatrixType = mtSplice
              ActiveColor = clFuchsia
              ActiveVisible = True
              Anchors = [akLeft, akTop, akRight]
              ChannelColor = clRed
              ChannelStyle = psSolid
              ChannelVisible = False
              ClusterColor = clPurple
              ClusterStyle = psSolid
              ClusterVisible = False
              CutsColor = clRed
              DefectsColor = clBlack
              DisableMessage = 'Quality Matrix disabled'
              DisplayMode = dmValues
              DotsColor = clBlack
              Enabled = True
              InactiveColor = 9502719
              MatrixMode = mmSelectSettings
              SpliceColor = clBlue
              SpliceStyle = psSolid
              SpliceVisible = True
              SubFieldX = 0
              SubFieldY = 0
              ZeroLimit = 0.01
            end
            object mCurveSelection: TmmPanel
              Left = 363
              Top = 6
              Width = 132
              Height = 62
              BevelOuter = bvNone
              TabOrder = 1
              object mcbChannelCurve: TmmCheckBox
                Left = 6
                Top = 6
                Width = 123
                Height = 17
                Caption = '(13)Kanal Kurve'
                Checked = True
                Color = clBtnFace
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                State = cbChecked
                TabOrder = 0
                Visible = True
                OnClick = mcbChannelCurveClick
                AutoLabel.LabelPosition = lpLeft
              end
              object mcbSpliceCurve: TmmCheckBox
                Left = 6
                Top = 38
                Width = 121
                Height = 17
                Caption = '(13)Spleiss Kurve'
                Color = clBtnFace
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                TabOrder = 2
                Visible = True
                OnClick = mcbSpliceCurveClick
                AutoLabel.LabelPosition = lpLeft
              end
              object mcbClusterCurve: TmmCheckBox
                Left = 6
                Top = 22
                Width = 120
                Height = 17
                Caption = '(13)Fehlerschwarm'
                Color = clBtnFace
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clPurple
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                TabOrder = 1
                Visible = True
                OnClick = mcbClusterCurveClick
                AutoLabel.LabelPosition = lpLeft
              end
            end
          end
        end
      end
      object mFFGroupBox: TmmGroupBox
        Left = 727
        Top = 0
        Width = 263
        Height = 572
        Align = alRight
        Caption = '(*)Fremdfasern'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        CaptionSpace = True
        object mFFClusterEditBox: TFFClusterEditBox
          Left = 4
          Top = 415
          Width = 249
          Height = 58
          Anchors = [akLeft, akBottom]
          Caption = '(*)Fremdfasern'
          Enabled = False
          TabOrder = 0
          CaptionSpace = True
          ReadOnly = False
          object mFFClusterButton: TmmButton
            Left = 141
            Top = 22
            Width = 100
            Height = 25
            Caption = '(16)FF-Einstellung'
            TabOrder = 0
            Visible = True
            OnClick = mFFClusterButtonClick
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mmPanel5: TmmPanel
          Left = 2
          Top = 15
          Width = 259
          Height = 394
          Anchors = [akLeft, akTop, akRight, akBottom]
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 1
          object mFFMatrix: TQualityMatrix
            Left = 2
            Top = 2
            Width = 259
            Height = 143
            Color = clWhite
            LastCutColor = clLime
            LastCutField = 0
            LastCutMode = lcNone
            MatrixSubType = mstSiroF
            MatrixType = mtSiro
            ActiveColor = clAqua
            ActiveVisible = True
            Anchors = [akLeft, akTop, akRight]
            ChannelColor = clSilver
            ChannelStyle = psSolid
            ChannelVisible = False
            ClusterColor = clSilver
            ClusterStyle = psSolid
            ClusterVisible = False
            CutsColor = clRed
            DefectsColor = clBlack
            DisableMessage = 'Quality Matrix disabled'
            DisplayMode = dmValues
            DotsColor = clBlack
            Enabled = True
            InactiveColor = 9502719
            MatrixMode = mmSelectCutFields
            SpliceColor = clSilver
            SpliceStyle = psSolid
            SpliceVisible = False
            SubFieldX = 0
            SubFieldY = 0
            ZeroLimit = 0.01
          end
        end
        object mPEditBox: TPEditBox
          Left = 4
          Top = 510
          Width = 142
          Height = 58
          Anchors = [akLeft, akBottom]
          Caption = '(11)P Einstellungen'
          TabOrder = 2
          CaptionSpace = True
          ReadOnly = False
        end
      end
    end
    object mSetup: TTabSheet
      Caption = '(*)&Grundeinstellungen'
      ImageIndex = 1
      object mGBBasicProdPara: TGBBasicProdPara
        Left = 0
        Top = 0
        Width = 990
        Height = 572
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object mpnSensingHeadClass: THeadClassEditBox
    Left = 408
    Top = 0
    Width = 201
    Height = 22
    BevelOuter = bvLowered
    BorderWidth = 2
    TabOrder = 1
    Visible = False
  end
end
