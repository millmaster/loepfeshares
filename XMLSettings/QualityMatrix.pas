(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QualityMatrix.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr  | Initial Release
|       2005  0.01  Wss | Umbau nach XML
| 13.01.2005  0.00  Wss | Assign und AssignProperties hinzugef�gt
| 21.01.2005  0.00  SDo | FieldState[] reset in Func. ClearValues
| 24.01.2005  1.01  SDo | -> mstSiroBD wird nicht mehr eingesetzt!! (Mj, Jm)
                        | SiroBD wird nicht mehr angezeigt -> SetSubType()
| 20.12.2007  1.02  Nue | TK ZENITC Behandlungen zugef�gt.
| 08.04.2008        Nue | Erg�nzung in Methode SetMode f�r Spleiss-Klassenreinigung.
| 04.06.2008        Nue | SetSelectionLayerVisability f�r Anzeige / Nichtanzeige Spleiss-Klassenreinigung (ZENIT-Feature).
|=========================================================================================*)
unit QualityMatrix;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QualityMatrixBase, DisplayLayer, CurveLayer, ScaleLayer, SelectionLayer,
  QualityMatrixDef, YMParaDef, YMParaUtils, KeySpinEdit, IvDictio,
  XMLSettingsModel, VCLXMLSettingsModel;

  //...........................................................................

resourcestring
  rsQMFFOff          = '(40)Fremdfaser-Reinigung ausgeschaltet'; //ivlm
  rsQMFFNotAvailable = '(40)Fremdfaser-Reinigung nicht verfuegbar'; //ivlm
  //...........................................................................

type
  TOnFieldClick = procedure (aFieldId: Integer; aButton: TMouseButton; aShift: TShiftState) of object;
  TQualityMatrix = class(TQualityMatrixBase)
  private
    fDisableMessage: string;
    fDisplayMode: TDisplayMode;
    fEnabled: Boolean;
    fMode: TMatrixMode;
    fModel: TVCLXMLSettingsModel;
    fOnConfirm: TOnConfirm;
    fOnFieldClick: TOnFieldClick;
    fOnFieldDblClick: TOnFieldClick;
    fOnParameterChange: TOnParameterChange;
    fOnTouch: TMouseEvent;
    mCurveLayer: TCurveLayer;
    mDisplayLayer: TDisplayLayer;
    mMatrixController: TMatrixController;
    mPixelAid: TPixelAid;
    mScaleLayer: TScaleLayer;
    mSelectionLayer: TSelectionLayer;
    mTouchFieldID: Integer;
    mTouchFieldState: Boolean;
    procedure DoParameterChange(aSource: TParameterSource);
    function GetActiveVisible: Boolean;
    function GetCurveColor(const aIndex: Integer): TColor;
    function GetCurveStyle(const aIndex: Integer): TPenStyle;
    function GetCurveVisible(const aIndex: Integer): Boolean;
    function GetDisplayMode: TDisplayMode;
    function GetDotsColor: TColor;
    function GetFieldColor(aFieldID: Integer): TColor;
    function GetFieldDots(aFieldID: Integer): Integer;
    function GetFieldState(aFieldID: Integer): Boolean;
    function GetStateColor(const aIndex: Integer): TColor;
    function GetSubField(const aIndex: Integer): Integer;
    function GetTotClasse: Integer;
    function GetTotValues: Integer;
    function GetValueColor(const aIndex: Integer): TColor;
    function GetZeroLimit: Double;
    procedure PutActiveVisible(const aValue: Boolean);
    procedure PutCurveColor(const aIndex: Integer; const aValue: TColor);
    procedure PutCurveStyle(const aIndex: Integer; const aValue: TPenStyle);
    procedure PutCurveVisible(const aIndex: Integer; const aValue: Boolean);
    procedure SetDisableMessage(const aValue: string);
    procedure SetDisplayMode(const aValue: TDisplayMode);
    procedure SetDotsColor(const aValue: TColor);
    procedure SetFieldColor(aFieldID: Integer; const aValue: TColor);
    procedure SetFieldDots(aFieldID: Integer; const aValue: Integer);
    procedure SetFieldState(aFieldID: Integer; aState: Boolean);
    procedure SetMode(aMode: TMatrixMode);
    procedure SetModel(const aValue: TVCLXMLSettingsModel);
    procedure SetSelectionLayerVisability;
    procedure SetStateColor(const aIndex: Integer; const aValue: TColor);
    procedure SetSubField(const aIndex, aValue: Integer);
    procedure SetValueColor(const aIndex: Integer; const aValue: TColor);
    procedure SetZeroLimit(const aValue: Double);
    procedure UpdateSuperClassHint;
  protected
    procedure SetEnabled(const aValue: Boolean); reintroduce;
    procedure ValueSave;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure SetFModeBD(const aValue: Boolean); override;
    procedure SetMatrixSubType(aValue: TMatrixSubType); override;
    procedure SetMatrixType(const aValue: TMatrixType); override;
//    procedure PutYMSettings(var aSettings: TYMSettingsRec);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure AssignProperties(Source: TPersistent);
    procedure ClearValues;
    function GetCutCurve(const aCurveType: TCurveTypes): TCutCurveList;
    procedure GetFieldStateTable(var aClassification: String); overload;
    procedure GetFieldStateTable(var aFClass, aFCluster: String); overload;
    function GetValue(aFieldID, aIndex: Integer): Double;
    procedure MouseDown(aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer); override;
    procedure MouseMove(aShift: TShiftState; aX, aY: Integer); override;
    procedure MouseUp(aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer); override;
    procedure Paint; override;
    procedure PaintClass(aClassID: Integer);
    procedure PaintDisableMessage;
    procedure PaintField(aFieldID: Integer);
    procedure Resize; override;
    procedure SetDefaultColor;
    procedure SetFieldStateTable(aClassification: String); overload;
    procedure SetFieldStateTable(aFClass, aFCluster: String); overload;
    procedure SetValue(aFieldID, aIndex: Integer; aValue: Double);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
    property Cuts[aFieldID: Integer]: Double index cCuts read GetValue write SetValue;
    property Defects[aFieldID: Integer]: Double index cDefects read GetValue write SetValue;
    property FieldColor[aFieldID: Integer]: TColor read GetFieldColor write SetFieldColor;
    property FieldDots[aFieldID: Integer]: Integer read GetFieldDots write SetFieldDots;
    property FieldState[aFieldID: Integer]: Boolean read GetFieldState write SetFieldState;
  published
    property ActiveColor: TColor index cCutActive read GetStateColor write SetStateColor;
    property ActiveVisible: Boolean read GetActiveVisible write PutActiveVisible;
    property Anchors;
    property ChannelColor: TColor index cChannelCurve read GetCurveColor write PutCurveColor;
    property ChannelStyle: TPenStyle index cChannelCurve read GetCurveStyle write PutCurveStyle;
    property ChannelVisible: Boolean index cChannelCurve read GetCurveVisible write PutCurveVisible;
    property ClusterColor: TColor index cClusterCurve read GetCurveColor write PutCurveColor;
    property ClusterStyle: TPenStyle index cClusterCurve read GetCurveStyle write PutCurveStyle;
    property ClusterVisible: Boolean index cClusterCurve read GetCurveVisible write PutCurveVisible;
    property Color;
    property CutsColor: TColor index cCuts read GetValueColor write SetValueColor;
    property DefectsColor: TColor index cDefects read GetValueColor write SetValueColor;
    property DisableMessage: string read fDisableMessage write SetDisableMessage;
    property DisplayMode: TDisplayMode read GetDisplayMode write SetDisplayMode;
    property DotsColor: TColor read GetDotsColor write SetDotsColor;
    property Enabled: Boolean read fEnabled write SetEnabled;
    property InactiveColor: TColor index cCutPassive read GetStateColor write SetStateColor;
    property MatrixMode: TMatrixMode read fMode write SetMode;
    property Model: TVCLXMLSettingsModel read fModel write SetModel;
    property OnConfirm: TOnConfirm read fOnConfirm write fOnConfirm;
    property OnFieldClick: TOnFieldClick read fOnFieldClick write fOnFieldClick;
    property OnFieldDblClick: TOnFieldClick read fOnFieldDblClick write fOnFieldDblClick;
    property OnParameterChange: TOnParameterChange read fOnParameterChange write fOnParameterChange;
    property OnTouch: TMouseEvent read fOnTouch write fOnTouch;
    property SpliceColor: TColor index cSpliceCurve read GetCurveColor write PutCurveColor;
    property SpliceStyle: TPenStyle index cSpliceCurve read GetCurveStyle write PutCurveStyle;
    property SpliceVisible: Boolean index cSpliceCurve read GetCurveVisible write PutCurveVisible;
    property SubFieldX: Integer index cXAxis read GetSubField write SetSubField;
    property SubFieldY: Integer index cYAxis read GetSubField write SetSubField;
    property TotClasses: Integer read GetTotClasse;
    property TotValues: Integer read GetTotValues;
    property ZeroLimit: Double read GetZeroLimit write SetZeroLimit;
  end;
  

//------------------------------------------------------------------------------
function InsertButtonToShiftState(aButton: TMouseButton;
  aShift: TShiftState): TShiftState;

implementation
uses
  mmMBCS, XMLDef, mmCS;
//------------------------------------------------------------------------------

function InsertButtonToShiftState(aButton: TMouseButton;
  aShift: TShiftState): TShiftState;
begin
  case aButton of
    mbLeft:
      Result := aShift + [ssLeft];

    mbRight:
      Result := aShift + [ssRight];

    mbMiddle:
      Result := aShift + [ssMiddle];
  end;
end;
//------------------------------------------------------------------------------

//:---------------------------------------------------------------------------
//:--- Class: TQualityMatrix
//:---------------------------------------------------------------------------
constructor TQualityMatrix.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  Color      := clWhite; // Panel Background Color
  BevelOuter := bvNone; // No Bevel
  BevelInner := bvNone; // No Bevel
  Width      := 200;
  Height     := 100;

  fDisableMessage    := 'Quality Matrix disabled';
  fEnabled           := True;
  fOnConfirm         := nil;
  fOnFieldClick      := nil;
  fOnFieldDblClick   := nil;
  fOnParameterChange := nil;
  fOnTouch           := nil;

  mMatrixController := TMatrixController.Create;
  mMatrixController.OnValueUpdate       := ValueUpdate;
  mMatrixController.OnMachineInitialize := MachineInitialize;
//  mMatrixController.ChangeKinds         := [ckChannel]; wird in SetMatrixType definiert

  mDisplayLayer := nil;
  mCurveLayer := nil;
  mScaleLayer := nil;
  mSelectionLayer := nil;

  // Set default Properties
  mPixelAid := TPixelAid.Create;

  MatrixType := mtShortLongThin;
  MatrixMode := mmDisplayData;
  DisplayMode := dmValues;
end;

//:---------------------------------------------------------------------------
destructor TQualityMatrix.Destroy;
begin
  mMatrixController.Free;

  mDisplayLayer.Free;
  mCurveLayer.Free;
  mScaleLayer.Free;
  mSelectionLayer.Free;
  mPixelAid.Free;

  inherited Destroy;
end;

//------------------------------------------------------------------------------

//******************************************************************************
// TRepQMatrix
//******************************************************************************
procedure TQualityMatrix.Assign(Source: TPersistent);
var
  i: Integer;
  xCount: Integer;
  xSourceMatrix: TQualityMatrix;
begin
  if Source is TQualityMatrix then begin
    AssignProperties(Source);

    xSourceMatrix := TQualityMatrix(Source);
    xCount := TotFields;
    for i:=0 to xCount-1 do begin
      SetValue(i, cCuts,    xSourceMatrix.GetValue(i, cCuts));
      SetValue(i, cDefects, xSourceMatrix.GetValue(i, cDefects));
      FieldDots[i]  := xSourceMatrix.FieldDots[i];
      FieldColor[i] := xSourceMatrix.FieldColor[i];
    end;
  end else
    inherited Assign(Source);
end;

//------------------------------------------------------------------------------
procedure TQualityMatrix.AssignProperties(Source: TPersistent);
begin
  if Source is TQualityMatrix then begin
    with TQualityMatrix(Source) do begin
      Self.MatrixType     := MatrixType;
//      Self.DataView       := DataView;
      Self.DisplayMode    := DisplayMode;
//      Self.LenBase        := LenBase;
      Self.MatrixMode     := MatrixMode;
      Self.ZeroLimit      := ZeroLimit;

      Self.ActiveColor    := ActiveColor;
      Self.ActiveVisible  := ActiveVisible;
      Self.InactiveColor  := InactiveColor;

      Self.CutsColor      := CutsColor;
      Self.DefectsColor   := DefectsColor;

      Self.ChannelColor   := ChannelColor;
      Self.ChannelStyle   := ChannelStyle;
      Self.ChannelVisible := ChannelVisible;

      Self.ClusterColor   := ClusterColor;
      Self.ClusterStyle   := ClusterStyle;
      Self.ClusterVisible := ClusterVisible;

      Self.SpliceColor    := SpliceColor;
      Self.SpliceStyle    := SpliceStyle;
      Self.SpliceVisible  := SpliceVisible;
    end;
  end;
end;

//------------------------------------------------------------------------------
procedure TQualityMatrix.ClearValues;
var
  i: Integer;
  xCount: Integer;
begin
  CodeSite.SendMsgEx(csmRed, 'TQualityMatrix.ClearValues');
  xCount := TotFields;
  for i:=0 to xCount-1 do begin
    SetValue(i, cCuts,    0);
    SetValue(i, cDefects, 0);
//    FieldState[i] := FALSE;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.DoParameterChange(aSource: TParameterSource);
begin
  if Assigned(fOnParameterChange) then
    fOnParameterChange(aSource);
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetActiveVisible: Boolean;
begin
  if Assigned(mSelectionLayer) then
    Result := mSelectionLayer.ActiveVisible
  else
    Result := False;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetCurveColor(const aIndex: Integer): TColor;
begin
  Result := clSilver;

  if Assigned(mCurveLayer) then begin
    with mCurveLayer do
      case aIndex of
        cChannelCurve: Result := ChannelColor;
        cClusterCurve: Result := ClusterColor;
        cSpliceCurve:  Result := SpliceColor;
      else
      end; // case
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetCurveStyle(const aIndex: Integer): TPenStyle;
begin
  Result := psSolid;
  
  if Assigned(mCurveLayer) then begin
    with mCurveLayer do
      case aIndex of
        cChannelCurve: Result := ChannelStyle;
        cClusterCurve: Result := ClusterStyle;
        cSpliceCurve:  Result := SpliceStyle;
      else
      end;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetCurveVisible(const aIndex: Integer): Boolean;
begin
  Result := False;
  
  if Assigned(mCurveLayer) then begin
    with mCurveLayer do
      case aIndex of
        cChannelCurve: Result := ChannelVisible;
        cClusterCurve: Result := ClusterVisible;
        cSpliceCurve:  Result := SpliceVisible;
      end;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetCutCurve(const aCurveType: TCurveTypes): TCutCurveList;
begin
  if Assigned(mCurveLayer) then
    Result := mCurveLayer.GetCutCurve(aCurveType)
  else
    Result := nil;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetDisplayMode: TDisplayMode;
begin
  
  if Assigned(mDisplayLayer) then
    fDisplayMode := mDisplayLayer.Mode;
  
  Result := fDisplayMode;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetDotsColor: TColor;
begin
  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.DotsColor
  else
    Result := clBlack;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetFieldColor(aFieldID: Integer): TColor;
begin
  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.FieldColor[aFieldID]
  else
    Result := clBlack;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetFieldDots(aFieldID: Integer): Integer;
begin
  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.FieldDots[aFieldID]
  else
    Result := 0;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetFieldState(aFieldID: Integer): Boolean;
begin
  if Assigned(mSelectionLayer) then
    Result := mSelectionLayer.FieldState[aFieldID]
  else
    Result := False;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.GetFieldStateTable(var aClassification: String);
begin
  if Assigned(mSelectionLayer) then
    mSelectionLayer.GetFieldStateTable(aClassification);
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.GetFieldStateTable(var aFClass, aFCluster: String);
begin
  if Assigned(mSelectionLayer) then
    mSelectionLayer.GetFieldStateTable(aFClass, aFCluster);
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetStateColor(const aIndex: Integer): TColor;
begin
  Result := clNone;

  if Assigned(mSelectionLayer) then begin
    case aIndex of
      cCutActive:  Result := mSelectionLayer.ActiveColor;
      cCutPassive: Result := mSelectionLayer.InactiveColor;
    else
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetSubField(const aIndex: Integer): Integer;
begin
  case aIndex of
    cXAxis: Result := inherited SubFieldX;
    cYAxis: Result := inherited SubFieldY;
  else
    Result := 0;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetTotClasse: Integer;
begin
  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.TotClasses
  else
    Result := 0;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetTotValues: Integer;
begin
  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.TotValues
  else
    Result := 0;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetValue(aFieldID, aIndex: Integer): Double;
begin
  Result := 0;

  if Assigned(mDisplayLayer) then begin
    case aIndex of
      cCuts:    Result := mDisplayLayer.Cuts[aFieldID];
      cDefects: Result := mDisplayLayer.Defects[aFieldID];
    else
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetValueColor(const aIndex: Integer): TColor;
begin
  Result := clNone;
  
  if Assigned(mDisplayLayer) then begin
    case aIndex of
      cCuts:    Result := mDisplayLayer.CutsColor;
      cDefects: Result := mDisplayLayer.DefectsColor;
    else
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrix.GetZeroLimit: Double;
begin
  Result := 0.01;
  if Assigned(mDisplayLayer) then
    Result := mDisplayLayer.ZeroLimit;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.MouseDown(aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer);
var
  xFieldID, i: Integer;
begin
  aShift := InsertButtonToShiftState(aButton, aShift);

  inherited MouseDown(aButton, aShift, aX, aY);

  if Enabled then begin
    // erst mal den Klick oder Doppelklick auf ein Feld nach aussen weiterleiten (Event)
    xFieldID := GetFieldID(aX, aY);
    if xFieldID < TotFields then begin
      if ssDouble in aShift then begin
        if Assigned(fOnFieldDblClick) then
          fOnFieldDblClick(xFieldID, aButton, aShift);
      end
      else if Assigned(fOnFieldClick) then
        fOnFieldClick(xFieldID, aButton, aShift);
    end;

    // Falls �ber einer Kurve die rechte Maustaste gedr�ckt wird -> entsprechend verarbeiten
    if Assigned(mCurveLayer) then
      mCurveLayer.CustomMouseDown(aShift, aX, aY);

    // keine Kurver vorhanden oder diese wurde "eingefroren" f�r schnellere Anzeige
    // -> Klick auswerten f�r Toggle der Klassierreinigung des Feldes
    if not Assigned(mCurveLayer) or not mCurveLayer.ImageStored then begin
      case fMode of
        mmSelectSettings, mmSelectCutFields, mmSelectSCMembers, mmSelectFFClusterFields: begin
            if ActiveVisible and (xFieldID < TotFields) then begin
              // Ok, Maustaste wurde sicher gedr�ckt
              if ssShift in aShift then begin
                // getoggelter Zustand und Startfeld merken, damit in MouseMove mehrere
                // Felder ebenfalls diesen Status bekommen k�nnen
                mTouchFieldID    := xFieldID;
                mTouchFieldState := not FieldState[xFieldID];
                if Assigned(fOnConfirm) and (xFieldId < TotFields) then
                  fOnConfirm(0);
                // Shift-Taste und Linke Maustate = Alle Felder oberhalb mitselektieren
                if ssLeft in aShift then begin
                  NeighbourField := iaTop;
                  repeat
                    // neuer getoggelter Status f�r das aktuelle Feld �bernehmen...
                    FieldState[xFieldID] := mTouchFieldState;
                    //...und gleich neu zeichnen
//                    PaintField(xFieldID);
                    xFieldID := GetNeighbourFieldID(aX, aY);
                  until xFieldID >= TotFields;
                end
                // Shift-Tate und Rechte Maustate = Alle Felder rechts oberhalb mitselektieren
                else if ssRight in aShift then begin
                  repeat
                    NeighbourField := iaTop;
                    i := aY;
                    repeat
                      FieldState[xFieldID] := mTouchFieldState;
//                      PaintField(xFieldID);
                      xFieldID := GetNeighbourFieldID(aX, i);
                    until xFieldID >= TotFields;

                    NeighbourField := iaRight;
                    xFieldID := GetNeighbourFieldID(aX, aY);
                  until xFieldID >= TotFields;

                end; // if ssRight in aShift
                // Speichert die Klassiersettings ins XML
                // -> MouseUp
//                ValueSave;
//
//                UpdateSuperClassHint;
//                if Assigned(mCurveLayer) then
//                  mCurveLayer.CustomPaint;
              end
              // Nur Linke Maustaste = 1 Feld toggeln
              else if ssLeft in aShift then begin
                CodeSite.SendFmtMsg('Clicked FieldIndex: %d', [xFieldID]);

                mTouchFieldID        := xFieldID;
                mTouchFieldState     := not FieldState[xFieldID];
                FieldState[xFieldID] := mTouchFieldState;

                if Assigned(fOnConfirm) and (xFieldId < TotFields) then
                  fOnConfirm(0);

//                PaintField(xFieldID);

                // Speichert die Klassiersettings ins XML
                // -> MouseUp
//                ValueSave;
//
//                UpdateSuperClassHint;
//                if Assigned(mCurveLayer) then
//                  mCurveLayer.CustomPaint;
              end;
            end; // if ActiveVisible and (xFieldID < TotFields) then begin
          end;
      end; // case fMode
    end; // if not
    if Assigned(fOnTouch) then
      fOnTouch(self, aButton, aShift, aX, aY);
  end;
end;

//procedure TQualityMatrix.MouseDown(aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer);
//var
//  xFieldID, i: Integer;
//begin
//  aShift := InsertButtonToShiftState(aButton, aShift);
//
//  inherited MouseDown(aButton, aShift, aX, aY);
//
//  if Enabled then begin
//    // erst mal den Klick oder Doppelklick auf ein Feld nach aussen weiterleiten (Event)
//    xFieldID := GetFieldID(aX, aY);
//    if xFieldID < TotFields then begin
//      if ssDouble in aShift then begin
//        if Assigned(fOnFieldDblClick) then
//          fOnFieldDblClick(xFieldID, aButton, aShift);
//      end
//  // wss
//      else if Assigned(fOnFieldClick) then
//  //      else if Assigned(fOnFieldDblClick) then
//        fOnFieldClick(xFieldID, aButton, aShift);
//    end;
//
//    // Falls �ber einer Kurve die rechte Maustaste gedr�ckt wird -> entsprechend verarbeiten
//    if Assigned(mCurveLayer) then
//      mCurveLayer.CustomMouseDown(aShift, aX, aY);
//  
//    // keine Kurver vorhanden oder diese wurde "eingefroren" f�r schnellere Anzeige
//    // -> Klick auswerten f�r Toggle der Klassierreinigung des Feldes
//    if not Assigned(mCurveLayer) or not mCurveLayer.ImageStored then begin
//      case fMode of
//        mmSelectSettings, mmSelectCutFields, mmSelectSCMembers, mmSelectFFClusterFields: begin
//  // xFieldID wurde ja schon weiter oben ermittelt
//  //wss          xFieldID := GetFieldID(aX, aY);
//            if xFieldID < TotFields then begin
//              // Shift-Tate und Linke Maustate = Alle Felder oberhalb mitselektieren
//              if ([ssShift, ssLeft] <= aShift) and ActiveVisible then begin
//                // getoggelter Zustand merken, damit in MouseMove mehrere Felder ebenfalls diesen
//                // Status bekommen k�nnen
//                mTouchFieldID    := xFieldID;
//                mTouchFieldState := not FieldState[xFieldID];
//                if Assigned(fOnConfirm) and (xFieldId < TotFields) then
//                  fOnConfirm(0);
//
//                NeighbourField := iaTop;
//                repeat
//                  // neuer getoggelter Status f�r das aktuelle Feld �bernehmen...
//                  FieldState[xFieldID] := mTouchFieldState;
//                  //...und gleich neu zeichnen
//                  PaintField(xFieldID);
//                  xFieldID := GetNeighbourFieldID(aX, aY);
//                until xFieldID >= TotFields;
//
//                UpdateSuperClassHint;
//
//                if Assigned(mCurveLayer) then
//                  mCurveLayer.CustomPaint;
//              end
//              // Shift-Tate und Rechte Maustate = Alle Felder rechts oberhalb mitselektieren
//              else if ([ssShift, ssRight] <= aShift) and ActiveVisible then begin
//                mTouchFieldID    := xFieldID;
//                mTouchFieldState := not FieldState[xFieldID];
//                if Assigned(fOnConfirm) and (xFieldId < TotFields) then
//                  fOnConfirm(0);
//
//                repeat
//                  NeighbourField := iaTop;
//                  i := aY;
//                  repeat
//                    FieldState[xFieldID] := mTouchFieldState;
//                    PaintField(xFieldID);
//                    xFieldID := GetNeighbourFieldID(aX, i);
//                  until xFieldID >= TotFields;
//
//                  NeighbourField := iaRight;
//                  xFieldID := GetNeighbourFieldID(aX, aY);
//
//                until xFieldID >= TotFields;
//
//                UpdateSuperClassHint;
//
//                if Assigned(mCurveLayer) then
//                  mCurveLayer.CustomPaint;
//              end
//              // Nur Linke Maustaste = 1 Feld toggeln
//              else if ([ssLeft] <= aShift) and ActiveVisible then begin
//                mTouchFieldID        := xFieldID;
//                mTouchFieldState     := not FieldState[xFieldID];
//                FieldState[xFieldID] := not FieldState[xFieldID];
//
//                if Assigned(fOnConfirm) and (xFieldId < TotFields) then
//                  fOnConfirm(0);
//
//                PaintField(xFieldID);
//  
//                UpdateSuperClassHint;
//                if Assigned(mCurveLayer) then
//                  mCurveLayer.CustomPaint;
//              end
//  
//            end;
//          end;
//      end;
//    end;
//    if Assigned(fOnTouch) then
//      fOnTouch(self, aButton, aShift, aX, aY);
//  end;
//end;
//
//:---------------------------------------------------------------------------
procedure TQualityMatrix.MouseMove(aShift: TShiftState; aX, aY: Integer);
var
  xFieldID: Integer;
begin
  inherited MouseMove(aShift, aX, aY);
  
  if Enabled then begin
    if Assigned(mCurveLayer) then
      mCurveLayer.CustomMouseMove(aShift, aX, aY);

    if not Assigned(mCurveLayer) or not mCurveLayer.ImageStored then begin
      case fMode of
        mmSelectSettings, mmSelectCutFields, mmSelectSCMembers, mmSelectFFClusterFields: begin
            if [ssLeft] <= aShift then begin
              xFieldID := GetFieldID(aX, aY);
              if (xFieldId < TotFields) and (xFieldID <> mTouchFieldID) then begin
                mTouchFieldID := xFieldID;
                FieldState[xFieldID] := mTouchFieldState;
                if Assigned(fOnConfirm) then
                  fOnConfirm(0);

                PaintField(xFieldID);

                UpdateSuperClassHint;

                if Assigned(mCurveLayer) then
                  mCurveLayer.CustomPaint;
              end;
            end;
          end; // mmSelectSettings,
      end;
    end;
  
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.MouseUp(aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer);
begin
  aShift := InsertButtonToShiftState(aButton, aShift);
  
  inherited;
  
  if Enabled then begin
    if Assigned(mCurveLayer) then
      mCurveLayer.CustomMouseUp(aShift, aX, aY);
//neu
    if not Assigned(mCurveLayer) or not mCurveLayer.ImageStored then begin
      // Aktualisieren nur wenn mit der Maustaste mehrere Felder markiert wurden (ohne Shift oder mbRight)
//      if aShift = [ssLeft] then begin
        case fMode of
          mmSelectSettings, mmSelectCutFields, mmSelectSCMembers, mmSelectFFClusterFields: begin
              // Speichert die Klassiersettings ins XML
              ValueSave;

              UpdateSuperClassHint;
              if Assigned(mCurveLayer) then
                mCurveLayer.CustomPaint;
          end; // mmSelectSettings,
        else
        end;
//      end;
    end; // if not Assigned(mCurveLayer)
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.Paint;
var
  i: Integer;
begin
  inherited Paint;
  
  if Enabled then begin
  
    if Assigned(mCurveLayer) and mCurveLayer.ImageStored then
      mCurveLayer.PaintImage
    else begin
      if Assigned(mScaleLayer) then
        mScaleLayer.CustomPaint;
  
      for i := 0 to TotFields - 1 do begin
        PaintField(i);
      end;
  
      for i := 0 to TotClasses - 1 do begin
        PaintClass(i);
      end;
    end;
  
    if Assigned(mCurveLayer) then
      mCurveLayer.CustomPaint;
  end
  else
    PaintDisableMessage;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.PaintClass(aClassID: Integer);
begin
  case fDisplayMode of
    dmSCValues, dmCalculateSCValues: mDisplayLayer.PaintField(aClassID);
  else
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.PaintDisableMessage;
var
  xTextRect: TRect;
  xScratch: Integer;
begin
  Canvas.Font.Color  := clDisableText;
  Canvas.Font.Style  := [];
  Canvas.Font.Size   := fsDisableText;
  Canvas.Brush.Style := bsClear;
  
  xScratch := Canvas.TextWidth(fDisableMessage);
  if Width > xScratch then begin
    xTextRect.Left  := (Width - xScratch) div 2;
    xTextRect.Right := xTextRect.Left + xScratch;
  end
  else begin
    xTextRect.Left  := 0;
    xTextRect.Right := Width;
  end;
  
  xScratch := Canvas.TextHeight(fDisableMessage);
  if Height > xScratch then begin
    xTextRect.Top    := (Height - xScratch) div 2;
    xTextRect.Bottom := xTextRect.Top + xScratch;
  end
  else begin
    xTextRect.Top    := 0;
    xTextRect.Bottom := Height;
  end;
  
  Canvas.TextRect(xTextRect, xTextRect.Left, xTextRect.Top, fDisableMessage);
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.PaintField(aFieldID: Integer);
begin
  case fDisplayMode of
    dmNone, dmSCValues, dmCalculateSCValues:
      if Assigned(mSelectionLayer) then
        mSelectionLayer.PaintField(aFieldID);

    dmValues, dmColor, dmDots: begin
        if Assigned(mSelectionLayer) then
          mSelectionLayer.PaintField(aFieldID);
        if Assigned(mDisplayLayer) then
          mDisplayLayer.PaintField(aFieldID);
      end;
  else
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.PutActiveVisible(const aValue: Boolean);
begin
  if Assigned(mSelectionLayer) then
    mSelectionLayer.ActiveVisible := aValue;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.PutCurveColor(const aIndex: Integer; const aValue: TColor);
begin
  if Assigned(mCurveLayer) then begin
    with mCurveLayer do
      case aIndex of
        cChannelCurve: ChannelColor := aValue;
        cClusterCurve: ClusterColor := aValue;
        cSpliceCurve:  SpliceColor  := aValue;
      else
      end;
  
    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.PutCurveStyle(const aIndex: Integer; const aValue: TPenStyle);
begin
  if Assigned(mCurveLayer) then begin
    with mCurveLayer do
      case aIndex of
        cChannelCurve: ChannelStyle := aValue;
        cClusterCurve: ClusterStyle := aValue;
        cSpliceCurve:  SpliceStyle  := aValue;
      else
      end;
  
    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.PutCurveVisible(const aIndex: Integer; const aValue: Boolean);
begin
  if Assigned(mCurveLayer) then begin
    with mCurveLayer do
      case aIndex of
        cChannelCurve: ChannelVisible := aValue;
        cClusterCurve: ClusterVisible := aValue;
        cSpliceCurve:  SpliceVisible  := aValue;
      else
      end;

    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.Resize;
begin
  inherited;
  if Assigned(mCurveLayer) then
    mCurveLayer.CustomResize;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetDefaultColor;
begin
  
  if Assigned(mCurveLayer) then
    mCurveLayer.SetDefaultColor;
  
  if Assigned(mSelectionLayer) then
    mSelectionLayer.SetDefaultColor;
  
  if Assigned(mDisplayLayer) then
    mDisplayLayer.SetDefaultColor;
  
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetDisableMessage(const aValue: string);
begin
  fDisableMessage := aValue;
  Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetDisplayMode(const aValue: TDisplayMode);
begin
  fDisplayMode := aValue;
  
  if Assigned(mDisplayLayer) then begin
    mDisplayLayer.Mode := fDisplayMode;
    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetDotsColor(const aValue: TColor);
begin
  if Assigned(mDisplayLayer) then
    mDisplayLayer.DotsColor := aValue;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetEnabled(const aValue: Boolean);
begin
  if aValue <> fEnabled then begin
    fEnabled := aValue;
  
    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetFieldColor(aFieldID: Integer; const aValue: TColor);
begin
  if Assigned(mDisplayLayer) then
    mDisplayLayer.FieldColor[aFieldID] := aValue;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetFieldDots(aFieldID: Integer; const aValue: Integer);
begin
  if Assigned(mDisplayLayer) then
    mDisplayLayer.FieldDots[aFieldID] := aValue;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetFieldState(aFieldID: Integer; aState: Boolean);
begin
  if Assigned(mSelectionLayer) then begin
    mSelectionLayer.FieldState[aFieldID] := aState;
    PaintField(aFieldID);
  //    if Assigned(mCurveLayer) then
  //      mCurveLayer.CustomPaint;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetFieldStateTable(aClassification: String);
begin
  if Assigned(mSelectionLayer) then begin
    mSelectionLayer.SetFieldStateTable(aClassification);
    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetFieldStateTable(aFClass, aFCluster: String);
begin
  if Assigned(mSelectionLayer) then begin
    mSelectionLayer.SetFieldStateTable(aFClass, aFCluster);
    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetMode(aMode: TMatrixMode);
var
  xType: TMatrixType;
begin
  xType := MatrixType;
  fMode := aMode;
  
  if mScaleLayer = nil then begin
    mScaleLayer := TScaleLayer.Create(self);
  //    mScaleLayer.Canvas := Canvas;
  end;
  
  if mDisplayLayer = nil then
    mDisplayLayer := TDisplayLayer.Create(self);
  
  with mDisplayLayer do begin
    MatrixType := xType;
    Mode       := fDisplayMode;
  end;
  
  //wss: Objekt an einem Ort 1x erstellen und nicht in jedem case Fall
  if mSelectionLayer = nil then begin
    mSelectionLayer            := TSelectionLayer.Create(self);
    mSelectionLayer.MatrixType := xType;
  end;
  
  case aMode of
    mmSelectSettings, mmSelectCutFields, mmSelectFFClusterFields: begin
  //       if mSelectionLayer = nil then begin
  //         mSelectionLayer := TSelectionLayer.Create(self);
  //         mSelectionLayer.MatrixType := xType;
  //       end;

        with mSelectionLayer do begin
          if aMode = mmSelectFFClusterFields then begin
//            if ((xType = mtSiro) and (self.MatrixSubType <> mstSiroBD)) OR
//               (xType = mtColor) then
            if (xType = mtSiro) and (self.MatrixSubType <> mstSiroBD) then
              SelectionMode := smFFClusterField
            else begin
              fMode := mmSelectCutFields;
              SelectionMode := smCutField;
            end;
          end
          else
            SelectionMode := smCutField;
        end;
  
        if Assigned(mCurveLayer) then begin
  // Temporary for Release because Curve draging is not finished yet
          if aMode = mmSelectSettings then
            mCurveLayer.SelectCurve := True
          else
            mCurveLayer.SelectCurve := False;
        end;

        //ACHTUNG: Folgender Block musste wegen der neuen Spleiss-Klassenreinigung eingef�hrt werden.
        //   Es muss hier auf die Info im FPattern zugegriffen werden und die Info mit dem aktivieren oder deaktivieren des
        //   SelectionLayer umgesetzt werden. (Nue:08.04.08)
        if (MatrixType = mtSplice) then
          if Assigned(fModel) then begin   //Nue:07.04.08
            ActiveVisible := YMParaUtils.IsZenitOrHigher(fModel);  //Nue:10.06.08
          //            SetSelectionLayerVisability;
          end;


        ShowHint := False;
        self.MatrixType := xType;
      end;
  (* %%
      mmDisplayCalculateSCData,
        mmDisplaySCData,
        mmDisplayData,
        mmDisplayColor,
        mmDisplayDots:
  *)
    mmDisplayData: begin
  //       if mSelectionLayer = nil then begin
  //         mSelectionLayer := TSelectionLayer.Create(self);
  //         mSelectionLayer.MatrixType := xType;
  //       end;
  
        with mSelectionLayer do
          SelectionMode := smCutField;
  
        if Assigned(mCurveLayer) then
          mCurveLayer.SelectCurve := False;
  
        ShowHint := False;
        self.MatrixType := xType;
      end;
  
    mmSelectSCMembers: begin
  //       if mSelectionLayer = nil then
  //         mSelectionLayer := TSelectionLayer.Create(self);
  
        with mSelectionLayer do begin
          SelectionMode := smSCMemberField;
  //         MatrixType := xType;
        end;
  
        if mDisplayLayer = nil then
          mDisplayLayer := TDisplayLayer.Create(self);
  
        with mDisplayLayer do
          MatrixType := self.MatrixType;
  
        if Assigned(mCurveLayer) then
          mCurveLayer.SelectCurve := False;
  
        ShowHint := True;
        self.MatrixType := xType;
      end;
  
  else
    //wss: else kann nie vorkommen, da alle case F�lle behandelt sind!!
    fMode := mmSelectSettings;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  if fModel <> aValue then begin
    if Assigned(fModel) then
      fModel.UnregisterObserver(mMatrixController);

    fModel := aValue;

    // Ein neues Model wird verlinkt -> die Observer registrieren lassen
    // Durch das Registrieren wird vom Model �ber den Observer ein ValueUpdate ausgel�st
    if Assigned(fModel) then
      fModel.RegisterObserver(mMatrixController);

    // Ein neues Model wird verlinkt -> weiterleiten
    if Assigned(mCurveLayer) then
      mCurveLayer.Model := fModel;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetStateColor(const aIndex: Integer; const aValue: TColor);
begin
  if Assigned(mSelectionLayer) then
    case aIndex of
      cCutActive:  mSelectionLayer.ActiveColor   := aValue;
      cCutPassive: mSelectionLayer.InactiveColor := aValue;
    else
    end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetSubField(const aIndex, aValue: Integer);
begin
  case aIndex of
    cXAxis: begin
        inherited SubFieldX := aValue;
        if Assigned(mCurveLayer) then
          mCurveLayer.SetSampleBufferLength;
      end;
    cYAxis: inherited SubFieldY := aValue;
  else
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetValue(aFieldID, aIndex: Integer; aValue: Double);
begin
  if mDisplayLayer <> nil then
    with mDisplayLayer do
      case aIndex of
        cCuts:    Cuts[aFieldID] := aValue;
        cDefects: Defects[aFieldID] := aValue;
      else
      end;
  
  Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetValueColor(const aIndex: Integer; const aValue: TColor);
begin
  if Assigned(mDisplayLayer) then
    case aIndex of
      cCuts:    mDisplayLayer.CutsColor := aValue;
      cDefects: mDisplayLayer.DefectsColor := aValue;
    else
    end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetZeroLimit(const aValue: Double);
begin
  if Assigned(mDisplayLayer) then
    mDisplayLayer.ZeroLimit := aValue
  
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.UpdateSuperClassHint;
var
  i: Integer;
  xCuts, xDefects: Real;
  xFormat: TFormatTextRec;
begin
  if fMode = mmSelectSCMembers then begin
    xCuts    := 0;
    xDefects := 0;
    for i := 0 to TotFields - 1 do begin
      if FieldState[i] = True then begin
        xCuts    := xCuts + Cuts[i];
        xDefects := xDefects + Defects[i];
      end;
    end;

    xFormat := DefectsFormat[0];
    with xFormat do
      if xDefects <> 0 then
        Hint := preText + Format(formatText, [xDefects]) + postText + '   '
      else
        Hint := '';

    xFormat := CutsFormat[0];
    with xFormat do
      if xCuts <> 0 then Hint := Hint + preText + Format(formatText, [xCuts]) + postText;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.ValueSave;
var
  xClass, xCluster: String;
  xSize: Integer;
begin
  case MatrixType of
//Nue:16.01.08    mtSplice:;  // Spleiss hat keine Klassierung
    mtSplice: begin  //Nue:16.01.08
      // Splice-Klassierfelder ins XML schreiben
      GetFieldStateTable(xClass);
      mMatrixController.Change(cXPSpliceClassificationItem, xClass);
    end;
    mtSiro: begin
        // Hole mal die Class- und Clusterfelder
        GetFieldStateTable(xClass, xCluster);
        xSize := Length(xClass) div 2;
        CodeSite.SendInteger('xClass size', xSize);
        // Dunkle (normale) Class- und Clusterfelder in XML schreiben
        mMatrixController.Value[cXPFDarkClassificationItem] := Copy(xClass, 1, xSize);
        mMatrixController.Change(cXPDarkClusterClassificationItem, Copy(xCluster, 1, xSize));

        // Helle Daten abschneiden und noch den Rest schreiben
        Delete(xClass, 1, xSize);
        Delete(xCluster, 1, xSize);
        // Dunkle Class- und Clusterfelder in XML schreiben
        mMatrixController.Value[cXPBrightClusterClassificationItem] := xCluster;
        mMatrixController.Change(cXPFBrightClassificationItem, xClass);
      end;
  else // mtShortLongThin
    // Klassierfelder ins XML schreiben
    GetFieldStateTable(xClass);
    mMatrixController.Change(cXPYMSettingBasicClassificationItem, xClass);
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.ValueUpdate(Sender: TBaseXMLSettingsObserver);
var
  xCuts, xClusters: String;
begin
  if MatrixType = mtSiro then begin
    if mMatrixController.Model.MaConfigReader.Available then begin
      // Wenn MaConfig verf�gbar, dann noch auf das F-Bit pr�fen
      if mMatrixController.Model.IsAvailable then
        Enabled := Boolean(mMatrixController.Model.ValueDef[cXPFSensorActiveItem, False])
      else
        Enabled := True;
    end
    else begin
      // Im Berichtsmodus ist kein MaConfig verf�gbar -> FPattern von Settings auswerten
      // Ist gem�ss ConfigCode die F-Reinigung eingeschaltet?
      Enabled := mMatrixController.ValueDef[cXPFSensorActiveItem, False];
      if Enabled then begin
        // F-Reinigung eingeschaltet. Nun noch ermitteln was f�r ein Sensinghead verwendet wird -> aus MaConfig-FPattern
        case GetSensingHeadClass(mMatrixController.Model) of
          shc9xH:                MatrixSubType := mstSiroH;
          shc9xBD:               MatrixSubType := mstSiroBD;
//          shcZenitF, shcZenitFP: MatrixSubType := mstZenitF;
// Anpassungen wegen Einbau TK ZENITC  (Nue:20.12.07)
          shcZenitF, shcZenitFP, shcZenitCF, shcZenitCFP: MatrixSubType := mstZenitF;
        else // shc9xFS
          MatrixSubType := mstSiroF;
        end;
      end
    end; // if not MaConfigReader.Available
//    if not mMatrixController.Model.MaConfigReader.Available then begin
//      // Ist gem�ss ConfigCode die F-Reinigung eingeschaltet?
//      Enabled := mMatrixController.ValueDef[cXPFSensorActiveItem, False];
//      if Enabled then begin
//        // F-Reinigung eingeschaltet. Nun noch ermitteln was f�r ein Sensinghead verwendet wird -> aus MaConfig-FPattern
//        case GetSensingHeadClass(mMatrixController.Model) of
//          shc9xH:                MatrixSubType := mstSiroH;
//          shc9xBD:               MatrixSubType := mstSiroBD;
//          shcZenitF, shcZenitFP: MatrixSubType := mstZenitF;
//        else // shc9xFS
//          MatrixSubType := mstSiroF;
//        end;
//      end
//    end; // if not MaConfigReader.Available

    if mMatrixController.Model.IsAvailable then begin
      if not Enabled then begin
        // F-Reinigung ist ausgeschaltet -> Ursache ermitteln
        if IsFSensingHead(mMatrixController.Model) then
          // per F-Pattern w�re sie verf�gbar -> also einfach ausgeschaltet
          DisableMessage := Translate(rsQMFFOff)
        else
          // sonst war sie per Hardware schon gar nicht verf�gbar
          DisableMessage := Translate(rsQMFFNotAvailable);
      end; // if not Enabled

      // Beim Zenit: ist der Bright/Dark Modus aktiv?
      FModeBD := (mMatrixController.ValueDef[cXPFModeBDItem, 0] <> 0);
      // Klassierfelder f�r Siro setzen
      // Dark- und Bright-Klassierfelder sind immer je 64 Felder separat vorhanden
      // -> SetFieldStateTable() erwartet beide kombiniert als Parameter
      xCuts     := mMatrixController.ValueDef[cXPFDarkClassificationItem, ''];
      xClusters := mMatrixController.ValueDef[cXPDarkClusterClassificationItem, ''];
      xCuts     := xCuts + mMatrixController.ValueDef[cXPFBrightClassificationItem, ''];
      xClusters := xClusters + mMatrixController.ValueDef[cXPBrightClusterClassificationItem, ''];
      SetFieldStateTable(xCuts, xClusters);
//      SetFieldStateTable(String(mMatrixController.ValueDef[cXPFDarkClassificationItem, '']),
//                         String(mMatrixController.ValueDef[cXPDarkClusterClassificationItem, '']));
    end
    else begin
      // Wenn kein Model vorhanden, dann einfach mal die SiroF Matrix verwenden
      MatrixSubType := mstSiroF;
    end;
  end

  else if MatrixType = mtSplice then begin   //Splice eingef�gt! Nue:16.01.08
    Enabled := True;
  CodeSite.SendMsgEx(csmRed, 'MatrixType = mtSplice');
  CodeSite.SendBooleanEx(csmRed,'mMatrixController.ValueDef[cXPFP_ZenitItem]',Boolean(mMatrixController.Value[cXPFP_ZenitItem]));
    // Splice-Klassierfelder f�r Kanal setzen
    if mMatrixController.Model.IsAvailable then begin
      mSelectionLayer.MatrixSubType := mstSplice;  //Nue:04.06.08   Forciert das l�schen der SpliceClassCut-Fields auf dem GUI
      if YMParaUtils.IsZenitOrHigher(mMatrixController.Model) then begin //Nue:10.06.08
        ActiveVisible := True;
        SetFieldStateTable(String(mMatrixController.ValueDef[cXPSpliceClassificationItem, '']));
      end
      else begin
/////////        ActiveVisible := True;
        ActiveVisible := False;    //Nue:12.8.08
      end;
//      SetFieldStateTable(String(mMatrixController.ValueDef[cXPYMSettingBasicClassificationItem, false]));
    end;
  end

  else begin  //mtShortLongThin
    Enabled := True;
    // Klassierfelder f�r Kanal setzen
    if mMatrixController.Model.IsAvailable then
      SetFieldStateTable(String(mMatrixController.ValueDef[cXPYMSettingBasicClassificationItem, '']));
//      SetFieldStateTable(String(mMatrixController.ValueDef[cXPYMSettingBasicClassificationItem, false]));
  end;
  Invalidate;
end;
//procedure TQualityMatrix.ValueUpdate(Sender: TBaseXMLSettingsObserver);
//var
//  xStr: String;
//begin
//  if MatrixType = mtSiro then begin
//    if mMatrixController.Model.IsAvailable then begin
//      // Im Berichtsmodus ist kein MaConfig verf�gbar -> FPattern von Settings auswerten
//      if not mMatrixController.Model.MaConfigReader.Available then begin
//        // Ist gem�ss ConfigCode die F-Reinigung eingeschaltet?
//        Enabled := mMatrixController.ValueDef[cXPFSensorActiveItem, False];
//        if Enabled then begin
//          // Ja, nun noch ermitteln was f�r ein Sensinghead verwendet wird -> aus FPattern
////          FModeBD := (mMatrixController.ValueDef[cXPFModeBDItem, 0] <> 0);
//          case GetSensingHeadClass(mMatrixController.Model) of
//            shc9xH:                MatrixSubType := mstSiroH;
//            shc9xBD:               MatrixSubType := mstSiroBD;
//            shcZenitF, shcZenitFP: MatrixSubType := mstZenitF;
//          else // shc9xFS
//            MatrixSubType := mstSiroF;
//          end;
//        end
//        else begin
//          // Nein, F-Reinigung ist ausgeschaltet. Ursache ermitteln
//          if IsFSensingHead(mMatrixController.Model) then
//            // per F-Pattern w�re sie verf�gbar -> also einfach ausgeschaltet
//            DisableMessage := Translate(rsQMFFOff)
//          else
//            // sonst war sie per Hardware schon gar nicht verf�gbar
//            DisableMessage := Translate(rsQMFFNotAvailable);
//        end; // if Enabled
//      end; // if not MaConfigReader.Available
//
//      // Beim Zenit: ist der Bright/Dark Modus aktiv?
//      FModeBD := (mMatrixController.ValueDef[cXPFModeBDItem, 0] <> 0);
//      // Klassierfelder f�r Siro setzen
//      SetFieldStateTable(String(mMatrixController.ValueDef[cXPFDarkClassificationItem, false]),
//                         String(mMatrixController.ValueDef[cXPDarkClusterClassificationItem, false]));
//    end
//    else begin
//      MatrixSubType := mstSiroF;
//    end;
//  end
//  else begin
//    Enabled := True;
//    // Klassierfelder f�r Kanal setzen
//    if mMatrixController.Model.IsAvailable then
//      SetFieldStateTable(String(mMatrixController.ValueDef[cXPYMSettingBasicClassificationItem, false]));
//  end;
//  Invalidate;
//end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TQualityMatrix.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  // Bedingung: Applikation setzt aussen die Sichtweise. Entweder gesamte Maschine oder Spindelbereich
  if MatrixType = mtSiro then begin
    //TODO: Brauchts die Abfrage f�r Available �berhaupt?
    if mMatrixController.Model.MaConfigReader.Available then begin
      // Ist gem�ss FPattern �berhaupt F-Reinigung m�glich?
      Enabled  := IsFSensingHead(mMatrixController.Model);
      if Enabled then begin
        // Ja, nun noch ermitteln was f�r ein Sensinghead verwendet wird -> FPattern
        case GetSensingHeadClass(mMatrixController.Model) of
          shc9xH:                MatrixSubType := mstSiroH;
          shc9xBD:               MatrixSubType := mstSiroBD;
//          shcZenitF, shcZenitFP: MatrixSubType := mstZenitF;
// Anpassungen wegen Einbau TK ZENITC  (Nue:20.12.07)
          shcZenitF, shcZenitFP, shcZenitCF, shcZenitCFP: MatrixSubType := mstZenitF;
        else // shc9xFS
          MatrixSubType := mstSiroF;
        end;
      end
      else begin
        // Nein, F-Reinigung ist Hardwarem�ssig schon nicht m�glich
        DisableMessage := Translate(rsQMFFNotAvailable);
      end; // if FPattern.F Enabled
    end; // if MaConfigReader.Available
  end; // if MatrixType = mtSiro
  Invalidate;
end;

procedure TQualityMatrix.SetFModeBD(const aValue: Boolean);
begin
  if FModeBD <> aValue then begin
    inherited SetFModeBD(aValue);
    if Assigned(mSelectionLayer) then
      mSelectionLayer.CheckArrayLength;
    if Assigned(mDisplayLayer) then
      mDisplayLayer.MatrixType := MatrixType;
  end;
end;

//procedure TQualityMatrix.MachineInitialize(Sender: TBaseXMLSettingsObserver);
//begin
//  // Bedingung: Applikation setzt aussen die Sichtweise. Entweder gesamte Maschine oder Spindelbereich
//  if MatrixType = mtSiro then begin
//    //TODO: Brauchts die Abfrage f�r Available �berhaupt?
//    if mMatrixController.Model.MaConfigReader.Available then begin
//      // Ist gem�ss FPattern �berhaupt F-Reinigung m�glich?
//      Enabled  := IsFSensingHead(mMatrixController.Model);
//      if Enabled then begin
//        // Ja, nun noch ermitteln was f�r ein Sensinghead verwendet wird -> FPattern
//        FModeBD := (mMatrixController.ValueDef[cXPFModeBDItem, 0] <> 0);
//        case GetSensingHeadClass(mMatrixController.Model) of
//          shc9xH:                MatrixSubType := mstSiroH;
//          shc9xBD:               MatrixSubType := mstSiroBD;
//          shcZenitF, shcZenitFP: MatrixSubType := mstZenitF;
//        else // shc9xFS
//          MatrixSubType := mstSiroF;
//        end;
//      end
//      else begin
//        // Nein, F-Reinigung ist Hardwarem�ssig schon nicht m�glich
//        DisableMessage := Translate(rsQMFFNotAvailable);
//      end; // if FPattern.F Enabled
//    end; // if MaConfigReader.Available
//  end; // if MatrixType = mtSiro
//  Invalidate;
//end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetMatrixSubType(aValue: TMatrixSubType);
begin
  inherited SetMatrixSubType(aValue);

  // 24.01.2005 mstSiroBD wird nicht mehr eingesetzt!! (Mj, Jm)
//  if (aValue = mstSiroBD) and (MatrixMode = mmSelectFFClusterFields) then
//    MatrixMode := mmSelectCutFields;
  if Assigned(mSelectionLayer) then
    mSelectionLayer.MatrixSubType := MatrixSubType;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrix.SetMatrixType(const aValue: TMatrixType);
var
  xRepaint: Boolean;
  //..........................................................
  procedure CheckAndCreateCurveLayer;
  begin
    if mCurveLayer = nil then begin
      mCurveLayer := TCurveLayer.Create(self);
      mCurveLayer.OnParameterChange := DoParameterChange;
      mCurveLayer.Model             := fModel;
    end;
  end;
  //..........................................................
begin
  if aValue <> MatrixType then begin
    inherited SetMatrixType(aValue);
    xRepaint := False;

    if Assigned(mSelectionLayer) then begin
      mSelectionLayer.MatrixType := aValue;
      xRepaint                   := True;
    end;

    if mDisplayLayer <> nil then begin
      mDisplayLayer.MatrixType := aValue;
      xRepaint                 := True;
    end;

    if aValue = mtShortLongThin then begin
      mMatrixController.ListenToKindSet := [ckChannel, ckCluster, ckSplice];
      CheckAndCreateCurveLayer;

      ChannelVisible := True;
      ClusterVisible := False;
      SpliceVisible  := False;
      ActiveVisible  := True;
      xRepaint       := True;
    end
    else if aValue = mtSplice then begin
      mMatrixController.ListenToKindSet := [ckSplice];
      CheckAndCreateCurveLayer;

      ChannelVisible := False;
      ClusterVisible := False;
      SpliceVisible  := True;

      ActiveVisible  := True;    //Nue:16.01.08

      xRepaint       := True;
    end
    else begin
      mMatrixController.ListenToKindSet := [ckFCluster, ckBasic];
      FreeAndNil(mCurveLayer);
      ActiveVisible := True;
    end;

    if xRepaint then Invalidate;
  end;
end;

procedure TQualityMatrix.SetSelectionLayerVisability;
begin
  if not (Boolean(mMatrixController.ValueDef[cXPFP_ZenitItem, False])) then  //Nue:03.04.08
//  if not (Boolean(fModel.ValueDef[cXPFP_ZenitItem, False])) then  //Nue:03.04.08
    ActiveVisible  := False
  else
    ActiveVisible  := True;    //Nue:16.01.08;
end;

end.

