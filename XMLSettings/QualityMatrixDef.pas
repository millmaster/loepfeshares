(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QualityMatrixDef.pas
| Projectpart...: MillMaster 
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.05.1999  0.00  Kr  | Initial Release
|       2005  0.01  Wss | Umbau nach XML
|       2006  0.02  Wss | In TMatrixSubType mstSplice eingefügt um SpiceClassierung zu handeln.
| 18.09.2008  0.03  Nue |  clSpliceQualityCutOn = clFuchsia;  eingefügt.
|=========================================================================================*)
unit QualityMatrixDef;          

interface

uses Windows, Graphics, SysUtils;

type
//------------------------------------------------------------------------------
// Published Propertie Types
//------------------------------------------------------------------------------
  //...........................................................................
  TDisplayMode = (dmNone, dmValues, dmColor, dmDots, dmSCValues, dmCalculateSCValues);
  TLastCutMode = (lcNone, lcValue, lcField);
  TMatrixMode = (mmSelectSettings, mmSelectCutFields, mmSelectFFClusterFields,
    mmSelectSCMembers, mmDisplayData);
(*
    , mmDisplayColor, mmDisplayDots,
    mmDisplaySCData, mmDisplayCalculateSCData);
*)
  TMatrixSubType = (mstNone, mstSiroF, mstSiroH, mstSiroBD, mstZenitF, mstSplice);   //Nue:04.06.08 mstSplice eingefügt
  TMatrixType = (mtNone, mtShortLongThin, mtSiro, mtSplice{, mtColor});
  TSelectionMode = (smCutField, smFFClusterField, smSCMemberField);
  //...........................................................................

//------------------------------------------------------------------------------
// Misc Types
//------------------------------------------------------------------------------
  //...........................................................................

  TCalculationBase = (cbAbsolute, cbRelative);
  TCoordinateMode = (cmActual, cmLeft, cmRight, cmTop, cmBottom, cmOutside, cmInside);
  TItemAlignement = (iaLeft, iaRight, iaCenter, iaTop, iaBottom);
  //...........................................................................

//------------------------------------------------------------------------------
// Base records
//------------------------------------------------------------------------------
  //...........................................................................

  TRangeRec = record
    start: Integer;
    stop: Integer;
  end;
  
  //...........................................................................

  TPenRec = record
    width: Integer;
    color: TColor;
    mode: TPenMode;
    style: TPenStyle;
  end;
  
  //...........................................................................

  TBrushRec = record
    color: TColor;
    style: TBrushStyle;
  end;
  
  //...........................................................................

  TFontRec = record
    color: TColor;
    size: Integer;
    style: TFontStyles;
  end;
  
  //...........................................................................

//------------------------------------------------------------------------------
// Field records
//------------------------------------------------------------------------------
  //...........................................................................

  TFieldRec = record
    top: Integer;
    left: Integer;
    width: Integer;
    height: Integer;
    pBackground: ^TBrushRec;
  end;
  
  //...........................................................................
  PFieldRecArray = ^TFieldRecArray;
  TFieldRecArray = array[0..32767] of TFieldRec;

  TFieldsRec = record
    fieldWidth: Integer;
    fieldHeight: Integer;
    background: TBrushRec;
    table: PFieldRecArray;
  end;
  
  //...........................................................................

  TMatrixRasterRec = record
    width: Integer;
    height: Integer;
  end;
  
  //...........................................................................

//------------------------------------------------------------------------------
// Curve Layer records (SLT Matrix)
//------------------------------------------------------------------------------
  //...........................................................................

  TScaleRec = record
    value: Single; //Integer;
    pixcelToNextScale: Integer;
    numberOfSamples: Integer;
  end;
  
  //...........................................................................  TChannelCurve = class(TObject)

  PIntegerArray = ^TIntegerArray;
  TIntegerArray = array[0..32767] of Integer;

  TSingleRect = record
     Left, Top, Right, Bottom: Single;
  end;

  TCurveLayerRec = record
    numberOfSamplePerScale: Integer;
    visibleNSLRange: TSingleRect;
    visibleTRange: TSingleRect;
//    visibleNSLRange: TRect;
//    visibleTRange: TRect;
  end;
  
  //...........................................................................

//------------------------------------------------------------------------------
// Display Layer records
//------------------------------------------------------------------------------
  //...........................................................................

const
  cMaxSuperClassMember = 16;
  cSCDelimiter = $FF;
  //...........................................................................

type
  TFormatTextRec = record
    pFont: ^TFontRec;
    horAlign: TItemAlignement;
    vertAlign: TItemAlignement;
    background: TColor;
    preText: string;
    formatText: string;
    postText: string;
  end;
  
  //...........................................................................

  TNumberFormatRec = record
    cuts: TFormatTextRec;
    defects: TFormatTextRec;
  end;
  
  //...........................................................................

  TSCMemberArray = array[0..cMaxSuperClassMember - 1] of Byte;

  TSuperClassRec = record
    PNumber: ^TNumberFormatRec;
    boarder: TFieldRec;
    memberTbl: TSCMemberArray;
  end;
  
  //...........................................................................

  PSuperClassArray = ^TSuperClassArray;
  TSuperClassArray = array[0..32767] of TSuperClassRec;

  TDisplayLayerRec = record
    number: TNumberFormatRec;
    roughClassTbl: PSuperClassArray;
    defaultSuperClassTbl: PSuperClassArray;
  end;
  
  //...........................................................................

  //------------------------------------------------------------------------------
// Scale Layer records
//------------------------------------------------------------------------------
  //...........................................................................

  TLineRec = record
    pos: Integer;
    start: Integer;
    stop: Integer;
    pPen: ^TPenRec;
  end;
  
  //...........................................................................

  TTextRec = record
    pFont: ^TFontRec;
    horAlign: TItemAlignement;
    vertAlign: TItemAlignement;
    xPos: Integer;
    yPos: Integer;
    text: string;
  end;
  
  //...........................................................................
  PLineRecArray = ^TLineRecArray;
  TLineRecArray = array[0..32767] of TLineRec;

  TLinesRec = record
    pen: TPenRec;
    table: PLineRecArray;
  end;
  
  //...........................................................................
  PTextRecArray = ^TTextRecArray;
  TTextRecArray = array[0..32767] of TTextRec;

  TScaleLayerRec = record
    horLine: TLinesRec;
    vertLine: TLinesRec;
    xAxis: PTextRecArray;
    yAxis: PTextRecArray;
    caption: PTextRecArray;
  end;
  
  //...........................................................................

//------------------------------------------------------------------------------
// Selection Layer records
//------------------------------------------------------------------------------
  //...........................................................................

  TSelectionStateRec = record
    pActiveColor: ^TBrushRec;
    pPassiveColor: ^TBrushRec;
  end;
  
  //...........................................................................

  TSelectionLayerRec = record
    cutState: TSelectionStateRec;
    fFClusterState: TSelectionStateRec;
    sCMemberState: TSelectionStateRec;
  end;
  
  //...........................................................................

//------------------------------------------------------------------------------
// Quality Matrx Descriptor record
//------------------------------------------------------------------------------
  //...........................................................................

  TQualityMatrixDescRec = record
    pRaster: ^TMatrixRasterRec;
    pField: ^TFieldsRec;
    pScaleLayer: ^TScaleLayerRec;
    pDisplayLayer: ^TDisplayLayerRec;
    pSelectionLayer: ^TSelectionLayerRec;
    pCurveLayer: ^TCurveLayerRec;
  end;
  
  //...........................................................................

//------------------------------------------------------------------------------
// General definitions and default initialisation
//------------------------------------------------------------------------------
  //...........................................................................

const
  clDisableText = clBlack;
  fsDisableText = 10;
  clLine = clGray;
  clRoughLine = clBlack;// clBlack; // clDkGray;
  clScale = clBlack;
//  clScale = clGray;
  clSpliceQualityCutOn = clFuchsia;
  clQualityCutOn = clAqua;
  clQualityBackground = $0090FFFF;
//  clQualityCutOn = $00FFFF80;
  clSuperClassMemberOn = clLime;
//  clSuperClassMemberOn = clLtGray;
  //clAqua;
  //clBlue;
  //...........................................................................

{(*}
 cBackground0: TBrushRec = (
   color:        clBlue;
   style:        bsSolid;
 );

 cCutOn: TBrushRec = (
   color:        clQualityCutOn;
   style:        bsSolid;
 );

 cFFClusterOn: TBrushRec = (
    color:        clQualityCutOn;
//   style:        bsBDiagonal, bsCross...;  Scheissspiel: Background wird schwarz !!?
    style:        bsSolid;
 );

 cSCMemberOn: TBrushRec = (
   color:        clSuperClassMemberOn;
   style:        bsSolid;
 );

 cBackgroundUnassigned: TBrushRec = (
   color:        clBackground;
   style:        bsSolid;
 );

  //...........................................................................

 cFontAxis: TFontRec = (
    color:      clScale;
    size:       10;
    style:			[];
  );

 cFontCuts: TFontRec = (
    color:      clRed;
    size:       8;
    style:			[];
  );

 cFontDefects: TFontRec = (
    color:      clBlack;
    size:       8;
    style:			[];
  );

 cFontCaption: TFontRec = (
    color:      clBlack;
    size:       12;
    style:			[fsBold];
  );

 cFontScaleEnds: TFontRec = (
    color:      clNone;
//    size:       8;
    size:       10;
//    style:			[fsBold];
    style:			[];
  );

 cFontUnassigned: TFontRec = (
    color:				clBlack;
    size:         7;
    style:				[];
  );
  //...........................................................................

 cLinePenRough: TPenRec = (
     width:				3;
      color:        clRoughLine;
      mode:         pmCopy;
      style:        psSolid;
  );

 cLinePenUnassigned: TPenRec = (
  width:				1;
   color:        clBlack;
   mode:         pmBlack;
   style:        psSolid;
 );

 cChannelCurvePen: TPenRec = (
  width:				2;
   color:        clRed;
   mode:         pmCopy;
   style:        psSolid;
 );

 cClusterCurvePen: TPenRec = (
  width:				2;
   color:        clPurple;
   mode:         pmCopy;
   style:        psSolid;
 );

 cSpliceCurvePen: TPenRec = (
  width:				2;
   color:        clBlue;
   mode:         pmCopy;
   style:        psSolid;
 );
  //...........................................................................

 cFormatTextUnassigned: TFormatTextRec = (
    pFont:      nil;
    horAlign:   iaCenter;
    vertAlign:  iaCenter;
    preText:    '';
    formatText: '';
    postText:   '';
 );

 cTextUnassigned: TTextRec = (
    pFont:      nil;
    horAlign:   iaCenter;
    vertAlign:  iaCenter;
   xPos:       0;
    yPos:       0;
    text:       '';
 );
  //...........................................................................

{*)}

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.

