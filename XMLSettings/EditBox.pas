(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: EditBox.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...: EditBox Component supports to add edit fields with label.
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.11.1999  0.00  Kr  | Initial Release
|       2005  0.01  Wss | Umbau nach XML
| 19.05.2005  0.01  Wss | Properties Datentypen zu Extended gewechselt
|=============================================================================*)
unit EditBox;

interface

uses StdCtrls, Classes, KeySpinEdit, SysUtils, YMParaDef, Windows, IvDictio,
    mmLabel, PresetValue, Controls, mmGroupBox, XMLDef, VCLXMLSettingsModel;

const
  cGEBBoarder      = 7;
  cGEBSpace        = 5;
  cGEBTopBoarder   = 17;
  cHeightPrintable = 15;
  cHeightEdit      = 17;

type
  TComponentRec = record
    Measure: TmmLabel;
    Title: TmmLabel;
    Value: TControl;
    XPValue: string;
    XPSwitch: string;
    Hint: string;
    FormatStr: string;
    DisplayFactor: Single;
  end;
  

  TPrintableValue = class(TmmLabel, IPresetValue)
  private
    mPresetValue: TPresetValue;
    function GetAsFloat: Double;
    function GetHintText: string;
    function GetTextType: TTextType;
    function GetValueText: string;
    procedure SetAsFloat(const aValue: Double);
    procedure SetHintText(const aValue: string);
    procedure SetTextType(const aValue: TTextType);
    procedure SetValueText(const aValue: string);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec);
  end;


  TGroupEditBox = class(TmmGroupBox)
  private
    fCaptionWidth: Integer;
    fMeasureWidth: Integer;
    fModel: TVCLXMLSettingsModel;
    fOnConfirm: TOnConfirm;
    fPrintable: Boolean;
    fReadOnly: Boolean;
    fValueWidth: Integer;
    mComponentHeight: Integer;
    procedure CleanUpComponents;
    function GetCaption(aIndex: Integer): string;
    function GetEnabledField(aIndex: Integer): Boolean;
    function GetTextType(aIndex: Integer): TTextType;
    function GetValue(aIndex: Integer): Extended;
    function GetXPValue(aIndex: Integer): Extended;
    function GetXPSwitch(aIndex: Integer): TSwitch;
    procedure SetCaption(aIndex: Integer; const aValue: string);
    procedure SetEnabledFiled(aIndex: Integer; const aValue: Boolean);
    procedure SetModel(const aValue: TVCLXMLSettingsModel);
    procedure SetPrintable(const aValue: Boolean);
    procedure SetReadOnly(const aValue: Boolean);
    procedure SetTextType(aIndex: Integer; const aValue: TTextType);
    procedure SetValue(aIndex: Integer; const aValue: Extended);
    procedure SetXPValue(aIndex: Integer; const aValue: Extended);
    procedure SetXPSwitch(aIndex: Integer; const aValue: TSwitch);
  protected
    mComponentArr: Array of TComponentRec;
    mController: TBaseGUIController;
    mLeftPos: Integer;
    mTopPos: Integer;
    procedure AddField(aIndex: Integer; aValueSpec: TValueSpecRec; aPresetTextTbl: array of TPresetTextRec; aPrintable: Boolean);
    procedure AddTitle(var aLabel: TmmLabel; aCaption: String; aAlignment: TAlignment; aAutosize: Boolean);
    function CheckIndexException(aIndex: Integer): Boolean;
    procedure DoConfirm(aTag: Integer); virtual;
    procedure GetCreateController(var aController: TBaseGUIController); virtual; abstract;
    procedure Notification(aComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
    procedure SetMeasureWidth(const Value: Integer); virtual;
    function ValueIntf(aIndex: Integer): IPresetValue;
    property XPValue[aIndex: Integer]: Extended read GetXPValue write SetXPValue;
    property XPSwitch[aIndex: Integer]: TSwitch read GetXPSwitch write SetXPSwitch;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure BuildBox; virtual;
    procedure ClearValues; virtual;
    property Captions[aIndex: Integer]: string read GetCaption write SetCaption;
    property CaptionWidth: Integer read fCaptionWidth write fCaptionWidth;
    property EnabledField[aIndex: Integer]: Boolean read GetEnabledField write SetEnabledFiled;
    property MeasureWidth: Integer read fMeasureWidth write SetMeasureWidth;
    property OnConfirm: TOnConfirm read fOnConfirm write fOnConfirm;
    property TextType[aIndex: Integer]: TTextType read GetTextType write SetTextType;
    property Values[aIndex: Integer]: Extended read GetValue write SetValue;
    property ValueWidth: Integer read fValueWidth write fValueWidth;
  published
    property Enabled write SetEnabled;
    property Model: TVCLXMLSettingsModel read fModel write SetModel;
    property Printable: Boolean read fPrintable write SetPrintable default False;
    property ReadOnly: Boolean read fReadOnly write SetReadOnly;
  end;
  
  EGroupEditBox = class(Exception)
  end;
  

implementation
uses
  mmMBCS,
  LoepfeGlobal, mmcs;

//:---------------------------------------------------------------------------
//:--- Class: TGroupEditBox
//:---------------------------------------------------------------------------
constructor TGroupEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  CaptionSpace := True;
  Height       := 25;

  fCaptionWidth  := 20;
  fMeasureWidth  := 20;
  fModel         := Nil;
  fOnConfirm     := Nil;
  fPrintable     := False;
  fValueWidth    := 30;

  mComponentHeight := cHeightEdit;

  GetCreateController(mController);
end;

//:---------------------------------------------------------------------------
destructor TGroupEditBox.Destroy;
begin
  CleanUpComponents;
  //:...................................................................
  FreeAndNil(mController);
  //:...................................................................
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.AddField(aIndex: Integer; aValueSpec: TValueSpecRec; aPresetTextTbl: array of TPresetTextRec; aPrintable: Boolean);
begin
  if CheckIndexException(aIndex) then begin
    // Create Edit field
    if Assigned(mComponentArr[aIndex].Value) then
      FreeAndNil(mComponentArr[aIndex].Value);

    mComponentArr[aIndex].DisplayFactor := aValueSpec.Factor;
    mComponentArr[aIndex].XPValue       := aValueSpec.XPValue;
    mComponentArr[aIndex].XPSwitch      := aValueSpec.XPSwitch;
    if aPrintable then begin
      mComponentArr[aIndex].Value := TPrintableValue.Create(Self);
      with TPrintableValue(mComponentArr[aIndex].Value) do begin
        AutoSize  := False;
        Alignment := taRightJustify;
        Layout    := tlCenter;
        LoadValueSpec(aValueSpec, aPresetTextTbl);
      end; // with
    end else begin
      mComponentArr[aIndex].Value := TKeySpinEdit.Create(Self);
      with TKeySpinEdit(mComponentArr[aIndex].Value) do begin
        Autosize  := False;
        Alignment := taRightJustify;
        LoadValueSpec(aValueSpec, aPresetTextTbl);
        OnConfirm := DoConfirm;
        ReadOnly  := False;
        ShowMode  := smExtended;
      end; // with
    end;

    with TControl(mComponentArr[aIndex].Value) do begin
      Parent := Self;
      Height := mComponentHeight;
      Width  := ValueWidth;
      Top    := mTopPos + aValueSpec.Space; // xTotHeight + xTotSpace + cGEBTopBoarder;
      Left   := mLeftPos + CaptionWidth;
      Tag    := aIndex;
      mTopPos := Top + Height;
    end;
    // Size of GroupBox
    Height := mTopPos + cGEBBoarder;
    Width  := mLeftPos + CaptionWidth + ValueWidth + 2 + MeasureWidth + cGEBBoarder;

    // create Title
    if Assigned(mComponentArr[aIndex].Title) then
      FreeAndNil(mComponentArr[aIndex].Title);
    if aValueSpec.caption <> '' then begin
      mComponentArr[aIndex].Title := TmmLabel.Create(Self);
      with mComponentArr[aIndex].Title do begin
        Parent   := Self;
        Caption  := Translate(aValueSpec.caption);
        AutoSize := False;
        Height   := mComponentHeight;
//        Height   := 17;
        Layout   := tlCenter;
        Left     := mLeftPos;
        Top      := TControl(mComponentArr[aIndex].Value).Top;
        Width    := CaptionWidth;
      end;
    end;

    // create Measure
    if Assigned(mComponentArr[aIndex].Measure) then
      FreeAndNil(mComponentArr[aIndex].Measure);
    if aValueSpec.measure <> '' then begin
      mComponentArr[aIndex].Measure := TmmLabel.Create(Self);
      with mComponentArr[aIndex].Measure do begin
        Parent   := Self;
        Caption  := Translate(aValueSpec.Measure);
        AutoSize := False;
        Height   := mComponentHeight;
//        Height   := 17;
        Layout   := tlCenter;
        Left     := mLeftPos + CaptionWidth + ValueWidth + 2;
        Top      := TControl(mComponentArr[aIndex].Value).Top;
        Width    := MeasureWidth;
      end;
    end;

    EnabledField[aIndex] := Enabled;
  end; // if
end;

procedure TGroupEditBox.AddTitle(var aLabel: TmmLabel; aCaption: String; aAlignment: TAlignment; aAutosize: Boolean);
begin
  FreeAndNil(aLabel);
  aLabel := TmmLabel.Create(Self);
  with aLabel do begin
    Caption   := Translate(aCaption);
    Alignment := aAlignment;
    Layout    := tlCenter;
    Autosize  := aAutosize;
    if not Autosize then
      Width := ValueWidth;
    Parent    := Self;
    Left      := mLeftPos + CaptionWidth;
    Top       := mTopPos;
    mTopPos   := mTopPos + Height + 2;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.BuildBox;
begin
  CleanUpComponents;
end;

//:---------------------------------------------------------------------------
function TGroupEditBox.CheckIndexException(aIndex: Integer): Boolean;
begin
  Result := (aIndex <= High(mComponentArr));
  if not Result then
    raise EGroupEditBox.Create('CheckIndexException: Edit field not available ' + IntToStr(aIndex));
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.CleanUpComponents;
var
  i: Integer;
begin
  // ab dieser Top-Positon f�ngt die erste Komponente an
  mLeftPos := cGEBBoarder;
  mTopPos  := cGEBTopBoarder;

  for i := 0 to High(mComponentArr) do
    with mComponentArr[i] do begin
      FreeAndNil(Title);
      FreeAndNil(Measure);
      FreeAndNil(Value);
    end;
end;

procedure TGroupEditBox.ClearValues;
var
  i: Integer;
begin
  for i:=0 to High(mComponentArr) do
    ValueIntf(i).ValueText := '';
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.DoConfirm(aTag: Integer);
begin
  if Assigned(fOnConfirm) then
    fOnConfirm(aTag);
end;

//:---------------------------------------------------------------------------
function TGroupEditBox.GetCaption(aIndex: Integer): string;
begin
  if CheckIndexException(aIndex) then
    Result := mComponentArr[aIndex].Title.Caption;
end;

//:---------------------------------------------------------------------------
function TGroupEditBox.GetEnabledField(aIndex: Integer): Boolean;
begin
  Result := False;
  if CheckIndexException(aIndex) then begin
    with mComponentArr[aIndex] do begin
      if (Value is TKeySpinEdit) then
        Result := TKeySpinEdit(Value).Enabled
      else if (Value is TPrintableValue) then
        Result := TPrintableValue(Value).Enabled;
    end; // with
  end;
end;

//:---------------------------------------------------------------------------
function TGroupEditBox.GetTextType(aIndex: Integer): TTextType;
begin
  Result := ValueIntf(aIndex).TextType;
end;

//:---------------------------------------------------------------------------
function TGroupEditBox.GetValue(aIndex: Integer): Extended;
begin
  Result := ValueIntf(aIndex).AsFloat / mComponentArr[aIndex].DisplayFactor;
end;
//:-----------------------------------------------------------------------------
function TGroupEditBox.GetXPValue(aIndex: Integer): Extended;
var
  xVariant: Variant;
begin
  Result := 0;
  if CheckIndexException(aIndex) then 
    Result := mController.ValueDef[mComponentArr[aIndex].XPValue, 0];
end;
//:-----------------------------------------------------------------------------
function TGroupEditBox.GetXPSwitch(aIndex: Integer): TSwitch;
begin
  Result := swOff;
  if CheckIndexException(aIndex) then begin
    if mComponentArr[aIndex].XPSwitch <> '' then
      Result := mController.SwitchValueDef[mComponentArr[aIndex].XPSwitch, swOff];
  end;
end;

//------------------------------------------------------------------------------
procedure TGroupEditBox.Notification(aComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);

  if (Operation = opRemove) and (aComponent = fModel) then begin
    fModel := nil;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetCaption(aIndex: Integer; const aValue: string);
begin
  if CheckIndexException(aIndex) then
    mComponentArr[aIndex].Title.Caption := aValue;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetEnabled(Value: Boolean);
var
  i: Integer;
begin
  inherited SetEnabled(Value);
  for i:=0 to High(mComponentArr) do
    EnabledField[i] := Value;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetEnabledFiled(aIndex: Integer; const aValue: Boolean);
begin
  if CheckIndexException(aIndex) then begin
    with mComponentArr[aIndex] do begin
      if Assigned(Title) then
        Title.Enabled := aValue;
  
      if (Value is TKeySpinEdit) then
        TKeySpinEdit(Value).Enabled := aValue
      else if (Value is TPrintableValue) then
        TPrintableValue(Value).Enabled := aValue;
  
      if Assigned(Measure) then
        Measure.Enabled := aValue;
    end; // with
  end; // if
end;

procedure TGroupEditBox.SetMeasureWidth(const Value: Integer);
begin
  fMeasureWidth := Value;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  if fModel <> aValue then
  begin
  //:...................................................................
    // Wenn vorher schon ein Model dran war, erst die Observer unregistrieren
    if Assigned(fModel) then
      fModel.UnregisterObserver(mController);
  //:...................................................................
    fModel := aValue;
  //:...................................................................
    // Ein neues Model wird verlinkt -> die Observer registrieren lassen
    // Durch das Registrieren wird vom Model �ber den Observer ein ValueUpdate ausgel�st
    if Assigned(fModel) then
      fModel.RegisterObserver(mController);
  //:...................................................................
  end;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetPrintable(const aValue: Boolean);
begin
  if aValue <> fPrintable then begin
    fPrintable := aValue;
    if fPrintable then mComponentHeight := cHeightPrintable
                  else mComponentHeight := cHeightEdit;
    BuildBox;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetReadOnly(const aValue: Boolean);
var
  i: Integer;
begin
  fReadOnly := aValue;

  for i:=0 to High(mComponentArr) do
    with mComponentArr[i] do begin
      if (Value is TKeySpinEdit) then
        TKeySpinEdit(Value).ReadOnly := fReadOnly;
    end; // with
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetTextType(aIndex: Integer; const aValue: TTextType);
begin
  ValueIntf(aIndex).TextType := aValue;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetValue(aIndex: Integer; const aValue: Extended);
begin
  ValueIntf(aIndex).AsFloat := aValue * mComponentArr[aIndex].DisplayFactor;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetXPValue(aIndex: Integer; const aValue: Extended);
begin
  if CheckIndexException(aIndex) then
    mController.Value[mComponentArr[aIndex].XPValue] := aValue;
end;

//:---------------------------------------------------------------------------
procedure TGroupEditBox.SetXPSwitch(aIndex: Integer; const aValue: TSwitch);
begin
  if CheckIndexException(aIndex) then begin
    if mComponentArr[aIndex].XPSwitch <> '' then
      mController.Value[mComponentArr[aIndex].XPSwitch] := cSwitchNames[aValue];
  end;
end;

//:---------------------------------------------------------------------------
function TGroupEditBox.ValueIntf(aIndex: Integer): IPresetValue;
begin
  Result := Nil;
  if CheckIndexException(aIndex) then begin
    with mComponentArr[aIndex] do begin
      if not Value.GetInterface(IPresetValue, Result) then
        raise EGroupEditBox.Create('ValueIntf: IPresetValue interface not suported!');
    end; // with
  end; // if
end;

//:---------------------------------------------------------------------------
//:--- Class: TPrintableValue
//:---------------------------------------------------------------------------
constructor TPrintableValue.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  mPresetValue := TPresetValue.Create;
  mPresetValue.AttachInterface(self);
end;

//:---------------------------------------------------------------------------
destructor TPrintableValue.Destroy;
begin
  FreeAndNil(mPresetValue);
  inherited;
end;

//:---------------------------------------------------------------------------
function TPrintableValue.GetAsFloat: Double;
begin
  Result := mPresetValue.Value;
end;

//:---------------------------------------------------------------------------
function TPrintableValue.GetHintText: string;
begin
  Result := Hint;
end;

//:---------------------------------------------------------------------------
function TPrintableValue.GetTextType: TTextType;
begin
  Result := mPresetValue.TextType;
end;

//:---------------------------------------------------------------------------
function TPrintableValue.GetValueText: string;
begin
  Result := Text;
end;

//:---------------------------------------------------------------------------
procedure TPrintableValue.LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec);
begin

  mPresetValue.LoadValueSpec(aValueSpec, aPresetTbl);
end;

//:---------------------------------------------------------------------------
procedure TPrintableValue.SetAsFloat(const aValue: Double);
begin
  mPresetValue.Value := aValue;
  //  Caption := mPresetValue.Text;
end;

//:---------------------------------------------------------------------------
procedure TPrintableValue.SetHintText(const aValue: string);
begin
  Hint := aValue;
end;

//:---------------------------------------------------------------------------
procedure TPrintableValue.SetTextType(const aValue: TTextType);
begin
  mPresetValue.TextType := aValue;
end;

//:---------------------------------------------------------------------------
procedure TPrintableValue.SetValueText(const aValue: string);
begin
  Text := aValue;
end;

end.

