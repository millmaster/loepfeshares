object GBMachConfigBox: TGBMachConfigBox
  Left = 0
  Top = 0
  Width = 975
  Height = 422
  TabOrder = 0
  object laGarnreiniger: TmmLineLabel
    Left = 0
    Top = 0
    Width = 975
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '(*)Garnreiniger'
    Distance = 2
  end
  object laFacility: TmmLineLabel
    Left = 0
    Top = 65
    Width = 975
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '(20)Maschinensektionen'
    Distance = 2
  end
  object laSensingHead: TmmLabel
    Left = 11
    Top = 19
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(17)Tastkopf:'
    FocusControl = cobSensingHead
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laCutRetries: TmmLabel
    Left = 787
    Top = 42
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Schnittwiederholungen:'
    FocusControl = edCutRetries
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laZESwVersionlb: TmmLabel
    Left = 619
    Top = 252
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)ZE Software Version:'
    FocusControl = laZESwVersionStr
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laZESwVersionStr: TmmLabel
    Left = 744
    Top = 252
    Width = 24
    Height = 13
    Caption = '1234'
    Visible = True
    AutoLabel.Control = laZESwVersionlb
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object laZESwOptionlb: TmmLabel
    Left = 619
    Top = 272
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)ZE Software Option:'
    FocusControl = laZESwOptionStr
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laZESwOptionStr: TmmLabel
    Left = 744
    Top = 272
    Width = 24
    Height = 13
    Caption = '1234'
    Visible = True
    AutoLabel.Control = laZESwOptionlb
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object laSPCheckLength: TmmLabel
    Left = 11
    Top = 284
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Spleissprueflaenge:'
    FocusControl = edSPCheckLength
    Visible = True
    AutoLabel.Control = laSPCheckLengthUnit
    AutoLabel.Distance = 51
    AutoLabel.LabelPosition = lpRight
  end
  object laLongStopDef: TmmLabel
    Left = 11
    Top = 308
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Lang-Stopp Dauer:'
    FocusControl = edLongStopDef
    Visible = True
    AutoLabel.Control = laLongStopDefUnit
    AutoLabel.Distance = 51
    AutoLabel.LabelPosition = lpRight
  end
  object laLongStopDefUnit: TmmLabel
    Left = 182
    Top = 308
    Width = 32
    Height = 13
    Caption = '(4)Min.'
    FocusControl = laLongStopDef
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laKnifePower: TmmLabel
    Left = 11
    Top = 260
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Messertrennleistung:'
    FocusControl = cobKnifePower
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laSPCheckLengthUnit: TmmLabel
    Left = 182
    Top = 284
    Width = 26
    Height = 13
    Caption = '(4)cm'
    FocusControl = laSPCheckLength
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laHeadstock: TmmLabel
    Left = 11
    Top = 236
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Maschinenkopf:'
    FocusControl = cobHeadstock
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laAWETypeStr: TmmLabel
    Left = 136
    Top = 42
    Width = 24
    Height = 13
    Caption = '1234'
    Visible = True
    AutoLabel.Control = laAWEType
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object laAWEType: TmmLabel
    Left = 11
    Top = 42
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(17)AWE Typ:'
    FocusControl = laAWETypeStr
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laDefaultSpeedRamp: TmmLabel
    Left = 11
    Top = 108
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Anlaufdauer:'
    FocusControl = edDefaultSpeedRamp
    Visible = True
    AutoLabel.Control = mplbDefaultSpeedRamp
    AutoLabel.Distance = 51
    AutoLabel.LabelPosition = lpRight
  end
  object mplbDefaultSpeedRamp: TmmLabel
    Left = 182
    Top = 108
    Width = 5
    Height = 13
    Caption = 's'
    FocusControl = laDefaultSpeedRamp
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laDefaultSpeed: TmmLabel
    Left = 11
    Top = 84
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Geschwindigkeit:'
    FocusControl = edDefaultSpeed
    Visible = True
    AutoLabel.Control = laDefaultSpeedUnit
    AutoLabel.Distance = 51
    AutoLabel.LabelPosition = lpRight
  end
  object laDefaultSpeedUnit: TmmLabel
    Left = 182
    Top = 84
    Width = 29
    Height = 13
    Caption = 'm/min'
    FocusControl = laDefaultSpeed
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laFrontmodelCaption: TmmLabel
    Left = 619
    Top = 232
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)Frontmodell'
    FocusControl = laFrontmodel
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laFrontmodel: TmmLabel
    Left = 744
    Top = 232
    Width = 24
    Height = 13
    Caption = '1234'
    Visible = True
    AutoLabel.Control = laFrontmodelCaption
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLineLabel1: TmmLineLabel
    Left = 0
    Top = 217
    Width = 975
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '(*)Maschine'
    Distance = 2
  end
  object laDFSDeltaUnit: TmmLabel
    Left = 174
    Top = 132
    Width = 8
    Height = 13
    Caption = '%'
    FocusControl = mmLabel1
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel1: TmmLabel
    Left = 11
    Top = 132
    Width = 120
    Height = 13
    AutoSize = False
    Caption = '(20)DFS Delta:'
    FocusControl = edDFSDelta
    Visible = True
    AutoLabel.Control = laDFSDeltaUnit
    AutoLabel.Distance = 43
    AutoLabel.LabelPosition = lpRight
  end
  object cobSensingHead: TmmComboBox
    Tag = 1
    Left = 136
    Top = 15
    Width = 89
    Height = 21
    Style = csDropDownList
    Color = clWindow
    ItemHeight = 13
    TabOrder = 0
    Visible = True
    OnChange = OnValueChange
    AutoLabel.Control = laSensingHead
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object cbAdjustCut: TmmCheckBox
    Tag = 2
    Left = 288
    Top = 17
    Width = 210
    Height = 17
    Caption = '(25)Schnitt vor Abgleich'
    TabOrder = 1
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object cbZeroAdjust: TmmCheckBox
    Tag = 4
    Left = 504
    Top = 17
    Width = 250
    Height = 17
    Caption = '(25)Null Abgleich Ueberwachung'
    TabOrder = 3
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object cbDriftConeChange: TmmCheckBox
    Tag = 5
    Left = 504
    Top = 40
    Width = 250
    Height = 17
    Caption = '(40)Driftkompensation nach Kreuzspulwechsel'
    TabOrder = 4
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object cbKnifeMonitor: TmmCheckBox
    Tag = 6
    Left = 784
    Top = 17
    Width = 180
    Height = 17
    Caption = '(20)Schnittueberwachung'
    TabOrder = 5
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object cbAdjustRemove: TmmCheckBox
    Tag = 3
    Left = 288
    Top = 40
    Width = 210
    Height = 17
    Caption = '(25)Garn nach Abgleich absaugen'
    TabOrder = 2
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object edSPCheckLength: TKeySpinEdit
    Tag = 9
    Left = 136
    Top = 280
    Width = 41
    Height = 21
    Color = clWindow
    TabOrder = 16
    Visible = True
    Alignment = taRightJustify
    AutoLabel.Control = laSPCheckLength
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    ValidateMode = [vmInput]
    OnConfirm = DoConfirm
  end
  object edCutRetries: TKeySpinEdit
    Tag = 7
    Left = 912
    Top = 38
    Width = 15
    Height = 21
    Color = clWindow
    TabOrder = 6
    Text = '2.00'
    Visible = True
    Alignment = taRightJustify
    AutoLabel.Control = laCutRetries
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
    ValidateMode = [vmInput]
    OnConfirm = DoConfirm
  end
  object edLongStopDef: TKeySpinEdit
    Tag = 23
    Left = 136
    Top = 304
    Width = 41
    Height = 21
    Color = clWindow
    TabOrder = 17
    Visible = True
    Alignment = taRightJustify
    AutoLabel.Control = laLongStopDef
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    ValidateMode = [vmInput]
    OnConfirm = DoConfirm
  end
  object cbExtMurItf: TmmCheckBox
    Tag = 20
    Left = 288
    Top = 256
    Width = 270
    Height = 17
    Caption = '(40)Erweiterte Murata Schnittstelle'
    TabOrder = 19
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object cbOneDrumPuls: TmmCheckBox
    Tag = 21
    Left = 288
    Top = 304
    Width = 270
    Height = 17
    Caption = '(40)Seed Simulation mit 1NTP/Umdrehung'
    TabOrder = 21
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object cbConeChangeCondition: TmmCheckBox
    Tag = 22
    Left = 288
    Top = 280
    Width = 270
    Height = 17
    Caption = '(40)Kreuzspulwechsel Interpretation aktiv'
    TabOrder = 20
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object cobKnifePower: TmmComboBox
    Tag = 24
    Left = 136
    Top = 256
    Width = 73
    Height = 21
    Style = csDropDownList
    Color = clWindow
    ItemHeight = 13
    TabOrder = 15
    Visible = True
    OnChange = OnValueChange
    AutoLabel.Control = laKnifePower
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object cobHeadstock: TmmComboBox
    Tag = 8
    Left = 136
    Top = 232
    Width = 73
    Height = 21
    Style = csDropDownList
    Color = clWindow
    ItemHeight = 13
    TabOrder = 14
    Visible = True
    OnChange = OnValueChange
    AutoLabel.Control = laHeadstock
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object edDefaultSpeedRamp: TKeySpinEdit
    Tag = 11
    Left = 136
    Top = 104
    Width = 41
    Height = 21
    Color = clWindow
    TabOrder = 8
    Visible = True
    Alignment = taRightJustify
    AutoLabel.Control = laDefaultSpeedRamp
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    ValidateMode = [vmInput]
    OnConfirm = DoConfirm
  end
  object edDefaultSpeed: TKeySpinEdit
    Tag = 10
    Left = 136
    Top = 80
    Width = 41
    Height = 21
    Color = clWindow
    TabOrder = 7
    Visible = True
    Alignment = taRightJustify
    AutoLabel.Control = laDefaultSpeed
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    ValidateMode = [vmInput]
    OnConfirm = DoConfirm
  end
  object cbUpperYarnCheck: TmmCheckBox
    Tag = 19
    Left = 288
    Top = 232
    Width = 270
    Height = 17
    Caption = '(40)Oberfadenpruefung'
    TabOrder = 18
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object pnBlocks: TmmPanel
    Left = 288
    Top = 80
    Width = 329
    Height = 129
    BevelOuter = bvNone
    TabOrder = 10
    object pnBlocksZenit: TmmPanel
      Left = 0
      Top = 0
      Width = 329
      Height = 17
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object cbBlockAtAlarm: TmmCheckBox
        Tag = 12
        Left = 0
        Top = 0
        Width = 270
        Height = 17
        Caption = '(40)Blockierung  bei Alarm'
        TabOrder = 0
        Visible = True
        OnClick = OnValueChange
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object pnBlocksSpectra: TmmPanel
      Left = 0
      Top = 17
      Width = 329
      Height = 96
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object cbBlockAtYarnCountAlarm: TmmCheckBox
        Tag = 13
        Left = 0
        Top = 0
        Width = 270
        Height = 17
        Caption = '(40)Blockierung bei Nummerabweichungs-Alarm'
        TabOrder = 0
        Visible = True
        OnClick = OnValueChange
        AutoLabel.LabelPosition = lpLeft
      end
      object cbBlockAtClusterAlarm: TmmCheckBox
        Tag = 14
        Left = 0
        Top = 32
        Width = 270
        Height = 17
        Caption = '(40)Blockierung bei Fehlerschwarm-Alarm'
        TabOrder = 2
        Visible = True
        OnClick = OnValueChange
        AutoLabel.LabelPosition = lpLeft
      end
      object cbBlockAtSFIAlarm: TmmCheckBox
        Tag = 15
        Left = 0
        Top = 48
        Width = 270
        Height = 17
        Caption = '(40)Blockierung bei SFI-Alarm'
        TabOrder = 3
        Visible = True
        OnClick = OnValueChange
        AutoLabel.LabelPosition = lpLeft
      end
      object cbBlockAtFAlarm: TmmCheckBox
        Tag = 16
        Left = 0
        Top = 64
        Width = 270
        Height = 17
        Caption = '(40)Blockierung bei FF-Alarm'
        TabOrder = 4
        Visible = True
        OnClick = OnValueChange
        AutoLabel.LabelPosition = lpLeft
      end
      object cbBlockAtFClusterAlarm: TmmCheckBox
        Tag = 17
        Left = 0
        Top = 80
        Width = 270
        Height = 17
        Caption = '(40)Blockierung bei FF-Schwarm-Alarm'
        TabOrder = 5
        Visible = True
        OnClick = OnValueChange
        AutoLabel.LabelPosition = lpLeft
      end
      object cbBlockAtShortCountAlarm: TmmCheckBox
        Tag = 26
        Left = 0
        Top = 16
        Width = 270
        Height = 17
        Caption = '(40)Blockierung bei Kurznummerabweichungs-Alarm'
        TabOrder = 1
        Visible = True
        OnClick = OnValueChange
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object pnBlocksBoth: TmmPanel
      Left = 0
      Top = 113
      Width = 329
      Height = 17
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object cbBlockAtCutFailureAlarm: TmmCheckBox
        Tag = 18
        Left = 0
        Top = 0
        Width = 270
        Height = 17
        Caption = '(40)Blockierung bei Schnittausfall'
        TabOrder = 0
        Visible = True
        OnClick = OnValueChange
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object edDFSDelta: TKeySpinEdit
    Tag = 25
    Left = 136
    Top = 128
    Width = 33
    Height = 21
    Color = clWindow
    TabOrder = 9
    Visible = True
    Alignment = taRightJustify
    AutoLabel.Control = mmLabel1
    AutoLabel.Distance = 5
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
    ValidateMode = [vmInput]
    OnConfirm = DoConfirm
  end
  object cbShortSFID: TmmCheckBox
    Tag = 27
    Left = 648
    Top = 80
    Width = 250
    Height = 17
    Caption = '(25)Kurz SFI (Messlaenge 10m)'
    TabOrder = 11
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object cbSHZeroCut: TmmCheckBox
    Tag = 28
    Left = 648
    Top = 96
    Width = 250
    Height = 17
    Caption = '(25)Nullung nach Schnitt'
    TabOrder = 12
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
  object cbSuckOffLastCutOnly: TmmCheckBox
    Tag = 30
    Left = 648
    Top = 112
    Width = 250
    Height = 17
    Caption = '(25)Nur letzter Schnitt wird abgesaugt'
    TabOrder = 13
    Visible = True
    OnClick = OnValueChange
    AutoLabel.LabelPosition = lpLeft
  end
end
