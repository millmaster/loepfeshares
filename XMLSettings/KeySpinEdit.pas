(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: KeySpinEdit.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Spin Edit Component without Spin Buttons, Up- Down- Keys only.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 29.10.1999  0.00  Kr  | Initial Release
|=============================================================================*)
unit KeySpinEdit;

interface

uses Mask, StdCtrls, Classes, SysUtils, Windows, IvDictio, mmEdit, PresetValue; // ExtCtrls, Controls, Messages, SysUtils,

type
  TOnConfirm = procedure (aTag: Integer) of object;
  TKeySpinEdit = class(TmmEdit, IPresetValue)
  private
    fOnConfirm: TOnConfirm;
    mOnEnterText: string;
    mOnEnterTextType: TTextType;
    mPresetValue: TPresetValue;
    procedure DoConfirm(aIndex: Integer);
    function GetAsFloat: Double;
    function GetHintText: string;
    function GetMaxValue: Double;
    function GetMinValue: Double;
    function GetTextType: TTextType;
    function GetValueHint: string;
    function GetValueText: string;
    function IsValidChar(var aKey: Char): Boolean; virtual;
    procedure SetHintText(const aValue: string);
    procedure SetMaxValue(const aValue: Double);
    procedure SetMinValue(const aValue: Double);
    procedure SetTextType(const aValue: TTextType);
    procedure SetValueHint(aValue: string);
    procedure SetValueText(const aValue: string);
  protected
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure KeyDown(var aKey: Word; aShift: TShiftState); override;
    procedure KeyPress(var aKey: Char); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec);
    procedure SetAsFloat(const aValue: Double);
    property TextType: TTextType read GetTextType write SetTextType;
  published
    property AutoSelect;
    property AutoSize;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property MaxLength;
    property MaxValue: Double read GetMaxValue write SetMaxValue;
    property MinValue: Double read GetMinValue write SetMinValue;
    property OnChange;
    property OnClick;
    property OnConfirm: TOnConfirm read fOnConfirm write fOnConfirm;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
  end;
  


implementation
uses
  mmMBCS, mmcs;
//------------------------------------------------------------------------------

//:---------------------------------------------------------------------------
//:--- Class: TKeySpinEdit
//:---------------------------------------------------------------------------
constructor TKeySpinEdit.Create(aOwner: TComponent);
begin
  inherited Create (aOwner);
  
  mPresetValue := TPresetValue.Create;
  mPresetValue.AttachInterface(self);
  
  fOnConfirm       := nil;
  mOnEnterText     := Text;
  mOnEnterTextType := GetTextType;
end;

//:---------------------------------------------------------------------------
destructor TKeySpinEdit.Destroy;
begin
  FreeAndNil(mPresetValue);
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.DoConfirm(aIndex: Integer);
begin
  if Assigned(fOnConfirm) then
    fOnConfirm(aIndex);
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.DoEnter;
begin
  mOnEnterText     := Text;
  mOnEnterTextType := GetTextType;
  inherited DoEnter;
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.DoExit;
begin
  if GetTextType = ttFloat then
    SetAsFloat(GetAsFloat);
  
  DoConfirm(Tag);
  inherited DoExit;
end;

//:---------------------------------------------------------------------------
function TKeySpinEdit.GetAsFloat: Double;
begin
  // auch wenn das Feld auf Off ist, wird trotzdem ein g�ltier Wert ausgelesen
  // da das mPresetValue Objekt dies verwaltet
  Result := mPresetValue.Value;
end;

//:---------------------------------------------------------------------------
function TKeySpinEdit.GetHintText: string;
begin
  Result := Hint;
end;

//:---------------------------------------------------------------------------
function TKeySpinEdit.GetMaxValue: Double;
begin
  Result := mPresetValue.MaxValue;
end;

//:---------------------------------------------------------------------------
function TKeySpinEdit.GetMinValue: Double;
begin
  Result := mPresetValue.MinValue;
end;

//:---------------------------------------------------------------------------
function TKeySpinEdit.GetTextType: TTextType;
begin
  Result := mPresetValue.TextType;
end;

//:---------------------------------------------------------------------------
function TKeySpinEdit.GetValueHint: string;
begin
  Result := Hint;
end;

//:---------------------------------------------------------------------------
function TKeySpinEdit.GetValueText: string;
begin
  Result := Text;
end;

//:---------------------------------------------------------------------------
function TKeySpinEdit.IsValidChar(var aKey: Char): Boolean;
begin
  // True, da Sondertasten wie Back oder Cursorbewegungen auch g�ltig sind aber NichtVisuell
  Result := True;
  // Visible characters
  if aKey in [' '..'~'] then begin
    if ReadOnly then
      Result := False
    else begin
      // Eingabefeld ist bereits im Zahlenmodus
      if GetTextType = ttFloat then begin
        if aKey in [DecimalSeparator, '+', '-', '0'..'9'] then begin
          // Only one decimal seperator, +, -
          if (aKey in [DecimalSeparator, '+', '-']) and (AnsiPos(aKey, Text) > 0) then
            Result := False
        end
        else begin
          Result := mPresetValue.FindHotkeyText(aKey);
          if Result then begin
            aKey := #0;
            DoConfirm(Tag);
          end;
        end;
      end
      // Eingabefeld ist noch im Textmodus ('Aus') -> wechseln nach Zahlenmodus
      else begin
        if aKey in [DecimalSeparator, '+', '-', '0'..'9'] then begin
          SetTextType(ttFloat);
          Text := '';
        end
        else
          Result := False
      end; // if TextType = ttFloat
    end;
  end; // if aKey in
  
  // Result := True;
  // // Visible characters
  // if aKey in [' '..'~'] then begin
  //   if not ReadOnly then begin
  //     // Eingabefeld ist bereits im Zahlenmodus
  //     if GetTextType = ttFloat then begin
  //       if aKey in [DecimalSeparator, '+', '-', '0'..'9'] then begin
  //         // Only one decimal seperator, +, -
  //         if (aKey in [DecimalSeparator, '+', '-']) and (AnsiPos(aKey, Text) > 0) then
  //           Result := False
  //       end
  //       else begin
  //         Result := mPresetValue.FindHotkeyText(aKey);
  //         if Result then begin
  //           aKey := #0;
  //           DoConfirm(Tag);
  //         end;
  //       end;
  //     end
  //     // Eingabefeld ist noch im Textmodus ('Aus') -> wechseln nach Zahlenmodus
  //     else begin
  //       if aKey in [DecimalSeparator, '+', '-', '0'..'9'] then begin
  //         SetTextType(ttFloat);
  //         Text := '';
  //       end
  //       else
  //         Result := False
  //     end; // if TextType = ttFloat
  //   end else
  //     Result := False;
  // end; // if aKey in
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.KeyDown(var aKey: Word; aShift: TShiftState);
begin
  // KeyDown wird IMMER als erstes aufgerufen, egal welche Taste gedr�ckt wurde
  if ReadOnly then begin
    aKey := 0;
    MessageBeep(0);
  end
  else begin
    case aKey of
      VK_UP: begin
          mPresetValue.SelectNext;
          DoConfirm(Tag);
        end;
      VK_DOWN: begin
          mPresetValue.SelectPrevious;
          DoConfirm(Tag);
        end;
    else
      inherited KeyDown(aKey, aShift);
    end; // case aKey of
  end; // if ReadOnly
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.KeyPress(var aKey: Char);
begin
  // KeyPress wird als zweites nur bei normalen Tasten ohne Sonderzeichen aufgerufen (z.B. Up/Down/Del)
  case aKey of
    // Escape
    #27: begin
        aKey     := #0;
        Text     := mOnEnterText;
        TextType := mOnEnterTextType;
        DoConfirm(Tag);
      end;
    // Return
    #13: begin
        aKey := #0;
        mPresetValue.ReadValueFromText;
        DoConfirm(Tag);
      end;
  else
    if IsValidChar(aKey) then
      inherited KeyPress(aKey)
    else begin
      aKey := #0;
      MessageBeep(0);
    end;
  end; // case aKey
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec);
begin
  mPresetValue.LoadValueSpec(aValueSpec, aPresetTbl);
  
  if Hint <> '' then
    ShowHint := True;
  mOnEnterText := Text;
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.SetAsFloat(const aValue: Double);
begin
  mPresetValue.Value := aValue;
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.SetHintText(const aValue: string);
begin
  Hint := aValue;
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.SetMaxValue(const aValue: Double);
begin
  mPresetValue.MaxValue := aValue;
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.SetMinValue(const aValue: Double);
begin
  mPresetValue.MinValue := aValue;
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.SetTextType(const aValue: TTextType);
begin
  mPresetValue.TextType := aValue;
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.SetValueHint(aValue: string);
begin
  Hint := aValue;
end;

//:---------------------------------------------------------------------------
procedure TKeySpinEdit.SetValueText(const aValue: string);
begin
  Text := aValue;
end;

//function TKeySpinEdit.IsValidChar(var aKey: Char): Boolean;
//begin
//  Result := True;
//
//  // Visible characters
//  if aKey in [' '..'~'] then begin
//    if not ReadOnly then begin
//      // Eingabefeld ist bereits im Zahlenmodus
//      if GetTextType = ttFloat then begin
//        if aKey in [DecimalSeparator, '+', '-', '0'..'9'] then begin
//          // Only one decimal seperator, +, -
////          if (aKey = DecimalSeparator) and (AnsiPos(DecimalSeparator, Text) > 0) then
//          if (aKey in [DecimalSeparator, '+', '-']) and (AnsiPos(aKey, Text) > 0) then
//            Result := False
//        end
//        else begin
//          Result := mPresetValue.FindHotkeyText(aKey);
//          if Result then begin
//            aKey := #0;
//            DoConfirm(Tag);
//          end;
//        end;
//      end
//      // Eingabefeld ist noch im Textmodus ('Aus') -> wechseln nach Zahlenmodus
//      else begin
//        if aKey in [DecimalSeparator, '+', '-', '0'..'9'] then begin
//          SetTextType(ttFloat);
//          Text := '';
//        end
//        else
//          Result := False
//      end; // if TextType = ttFloat
//    end
//    else
//      Result := False;
//  {
//    end
//    else begin
//      if ReadOnly and
//        ((aKey = Char(VK_BACK)) or (aKey = Char(VK_DELETE)) or
//         (aKey = Chr(VK_RETURN)) or (aKey = Char(VK_ESCAPE))) then
//        Result := False
//      else if aKey = Chr(VK_RETURN) then begin
//        if GetTextType = ttFloat then
//          SetAsFloat(GetAsFloat);
//        aKey := #0;
//
//        DoConfirm(Tag);
//      end
//      else if aKey = Char(VK_ESCAPE) then begin
//        Text := mOnEnterText;
//        SetTextType(mOnEnterTextType);
//        aKey := #0;
//      end
//      else if not ((aKey = Char(VK_BACK)) or (aKey = Char(VK_DELETE))) then
//        Result := False;
//  {}
//  end;
//end;
//
end.
