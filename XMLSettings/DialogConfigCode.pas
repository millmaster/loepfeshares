(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMParaEditBox.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Dialog for ConfigCode Bits.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 07.02.2000  0.00  Kr  | Initial Release
| 19.12.2003  0.01  Kr  | rsConfigCodeA wihout /ivlm
| 08.04.2005        Lok | Umbau auf Observer und Eliminierung der M�glichkeit den CC zu ver�ndern
| 10.03.2006        Lok | Anzeige ConfigCode disablen, wenn NMetztyp LX oder nicht bekannt
|=============================================================================*)
unit DialogConfigCode;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, StdCtrls, Buttons, mmBitBtn, CheckLst, mmCheckListBox, mmLabel,
  mmButton, mmEdit, XMLSettingsModel, xmlMaConfigClasses, VCLXMLSettingsModel, XMLDef;

resourcestring
  rsConfigCodeA = 'CfgA';

type
  TConfigCodeType = (ccNone, ccA, ccB, ccC, ccD);
const
  // Namen und Selektionspfade f�r die verschiedenen ConfigCodes
  cConfogCodeNameArray: Array [TConfigCodeType] of Char = (' ', 'A', 'B', 'C', 'D');
  cConfigCodeXPArray: Array [TConfigCodeType] of string = ('', cXPConfigCodeAItem, cXPConfigCodeBItem, cXPConfigCodeCItem, cXPConfigCodeDItem);

type
  // Berechnung des ConfigCodes aus den Einzel-Elementen und der ganzen Zahl (WORD)
  TConfigCodeCalculator = class(TObject)
  private
    FConfigCodeType: TConfigCodeType;
    FModel: TXMLSettingsModel;
  public
    function CalculateConfigCode: Word;
    function IsElementConfigCode(aSelPath: string): Boolean;
    property Model: TXMLSettingsModel read FModel write FModel;
  published
    property ConfigCodeType: TConfigCodeType read FConfigCodeType write FConfigCodeType;
  end;// TConfigCodeCalculator = class(TObject)

  TGBDialogConfigCode = class(TmmDialog)
    mbClose: TmmButton;
    mclbConfigBits: TmmCheckListBox;
    procedure mbCloseClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FConfigCodeType: TConfigCodeType;
    FModel: TXMLSettingsModel;
    mBasicController: TBasicController;
    mConfigCode: Word;
    procedure CalculateCC(aChangedElement: string);
    procedure SetConfigCode(const aValue: Word);
    procedure SetModel(const aModel: TXMLSettingsModel);
  protected
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure ShowConfigCode(aConfigCode: WORD);
    property ConfigCode: Word write SetConfigCode;
    property ConfigCodeType: TConfigCodeType read FConfigCodeType write FConfigCodeType;
    property Model: TXMLSettingsModel read FModel write SetModel;
  end;


  TGBConfigCode = class(TmmLabel)
  private
    FConfigCodeType: TConfigCodeType;
    FDialogCaption: TCaption;
    FModel: TXMLSettingsModel;
    mDialog: TGBDialogConfigCode;
    mBasicController: TBasicController;
    procedure CalculateCC(aChangedElement: string);
    procedure DoMouseDown(aSender: TObject; aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer);
    procedure SetModel(const aModel: TXMLSettingsModel);
  protected
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property Model: TXMLSettingsModel read FModel write SetModel;
  published
    property ConfigCodeType: TConfigCodeType read FConfigCodeType write FConfigCodeType;
    property DialogCaption: TCaption read FDialogCaption write FDialogCaption;
  end;

var
  GBDialogConfigCode: TGBDialogConfigCode;

implementation 
uses
  mmMBCS, MMEventLog, MSXML2_TLB, XMLGlobal, XMLMappingDef, mmcs, BaseGlobal;

{$R *.DFM}

const
  cWordBit0: Word = 1;

//:---------------------------------------------------------------------------
//:--- Class: TGBDialogConfigCode
//:---------------------------------------------------------------------------
constructor TGBDialogConfigCode.Create(aOwner: TComponent);
begin
  inherited;
  ConfigCode := $A5A5;
  Caption := rsConfigCodeA;
  
  mBasicController := TBasicController.Create;
  mBasicController.OnValueUpdate       := ValueUpdate;
  mBasicController.OnMachineInitialize := MachineInitialize;
end;

//:---------------------------------------------------------------------------
procedure TGBDialogConfigCode.SetConfigCode(const aValue: Word);
begin
  mConfigCode := aValue;
end;


//:---------------------------------------------------------------------------
//:--- Class: TGBConfigCode
//:---------------------------------------------------------------------------
constructor TGBConfigCode.Create(aOwner: TComponent);
begin
  inherited;

  FConfigCodeType := ccNone;
  mDialog := nil;
  OnMouseDown := DoMouseDown;

  mBasicController := TBasicController.Create;
  mBasicController.OnValueUpdate       := ValueUpdate;
  mBasicController.OnMachineInitialize := MachineInitialize;
end;

//:---------------------------------------------------------------------------
destructor TGBConfigCode.Destroy;
begin

  // Der Observer deregistriert sich selber
  mBasicController.Free;
  mDialog.Free;

  inherited;
end;

(*---------------------------------------------------------
  Berechnet den ConfigCode, damit die einzelnen Bits abgef�llt werden k�nnen
----------------------------------------------------------*)
procedure TGBConfigCode.CalculateCC(aChangedElement: string);
begin
  // Config Code berechnen
  with TConfigCodeCalculator.Create do try
    Model := self.Model;
    ConfigCodeType := self.ConfigCodeType;
    // Bei der Initialisierung der Maschine gibt es kein ge�ndertes Element
    if (aChangedElement = '') or (IsElementConfigCode(aChangedElement)) then try
      Caption := IntToStr(CalculateConfigCode);
////      Caption := fModel.ValueDef[cConfigCodeXPArray[ConfigCodeType], '0'];   //Test Nue/Lok 18.10.06
      if (Self.Model.MaConfigReader.Available) and (Self.Model.MaConfigReader.NetTyp in [ntNone, ntLX]) then
        Enabled := False;
    except
      CodeSite.SendWarning('Exception in TGBConfigCode.CalculateCC');
      // Keine Exceptinos durchlassen
    end;// if (aChangedElement = '') or (IsElementConfigCode(aChangedElement)) then try
  finally
    free;
  end;
end;// procedure TGBConfigCode.CalculateCC(aChangedElement: string);

(*---------------------------------------------------------
  Bei gedr�ckter ALT Taste wird bei einem Klick der Dialog angezeigt
----------------------------------------------------------*)
procedure TGBConfigCode.DoMouseDown(aSender: TObject; aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer);
var
  xPoint: TPoint;
begin

  if ssAlt in aShift then begin
    if not Assigned(mDialog) then
      mDialog := TGBDialogConfigCode.Create(self);

    with mDialog do begin
      Caption := fDialogCaption;
      xPoint := Self.ClientToScreen(Point(aX, aY));
      Left := xPoint.x - (Width div 2);
      Top := xPoint.y - (Height + 8);
      Model := self.Model;

      ConfigCodeType := self.ConfigCodeType;
      FormStyle := fsStayOnTop;
      Show;
    end;// with mDialog do begin
  end;// if ssAlt in aShift then begin
end;// procedure TGBConfigCode.DoMouseDown(aSender: TObject; aButton: TMouseButton; aShift: TShiftState; aX, aY: Integer);

(*---------------------------------------------------------
  Wird vom Observer aufgerufen, wenn eine neue Maschine geladen wird
----------------------------------------------------------*)
procedure TGBConfigCode.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CalculateCC('');
end;// procedure TGBConfigCode.MachineInitialize(Sender: TBaseXMLSettingsObserver);

(*---------------------------------------------------------
  Wenn das Modell zugewiesen wird, dann muss auch der Observer registriert werden
----------------------------------------------------------*)
procedure TGBConfigCode.SetModel(const aModel: TXMLSettingsModel);
begin
  if aModel <> fModel then begin
    if Assigned(fModel) then
      fModel.UnregisterObserver(mBasicController);

    fModel := aModel;

    // Ein neues Model wird verlinkt -> die Observer registrieren lassen
    // Durch das Registrieren wird vom Model �ber den Observer ein ValueUpdate ausgel�st
    if Assigned(fModel) then
      fModel.RegisterObserver(mBasicController);
  end;// if aModel <> fModel then begin
end;// procedure TGBConfigCode.SetModel(const aModel: TXMLSettingsModel);

(*---------------------------------------------------------
  Wird vom Observer aufgerufen, wenn sich ein Element ge�ndert hat
----------------------------------------------------------*)
procedure TGBConfigCode.ValueUpdate(Sender: TBaseXMLSettingsObserver);
begin
  CalculateCC(Sender.ChangedElementXP);
end;// procedure TGBConfigCode.ValueUpdate(Sender: TBaseXMLSettingsObserver);

//:---------------------------------------------------------------------------
destructor TGBDialogConfigCode.Destroy;
begin
  // Der Observer deregistriert sich selbst
  mBasicController.Free;
  inherited Destroy;
end;// destructor TGBDialogConfigCode.Destroy;

(*---------------------------------------------------------
  Berechnet den ConfigCode, damit die einzelnen Bits abgef�llt werden k�nnen
----------------------------------------------------------*)
procedure TGBDialogConfigCode.CalculateCC(aChangedElement: string);
begin
  // Config Code berechnen
  with TConfigCodeCalculator.Create do try
    Model := self.Model;
    ConfigCodeType := self.ConfigCodeType;
    // Bei der Initialisierung der Maschine gibt es kein ge�ndertes Element
    if (aChangedElement = '') or (IsElementConfigCode(aChangedElement)) then
      ShowConfigCode(CalculateConfigCode);
  finally
    free;
  end;
end;// procedure TGBDialogConfigCode.CalculateCC(aChangedElement: string);

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TGBDialogConfigCode.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CalculateCC('');
end;// procedure TGBDialogConfigCode.MachineInitialize(Sender: TBaseXMLSettingsObserver);

(*---------------------------------------------------------
  Wenn das Modell zugewiesen wird, dann muss auch der Observer registriert werden
----------------------------------------------------------*)
procedure TGBDialogConfigCode.SetModel(const aModel: TXMLSettingsModel);
begin
  if aModel <> fModel then begin
    if Assigned(fModel) then
      fModel.UnregisterObserver(mBasicController);

    fModel := aModel;

    // Ein neues Model wird verlinkt -> die Observer registrieren lassen
    // Durch das Registrieren wird vom Model �ber den Observer ein ValueUpdate ausgel�st
    if Assigned(fModel) then
      fModel.RegisterObserver(mBasicController);
  end;// if aModel <> fModel then begin
end;// procedure TGBDialogConfigCode.SetModel(const aModel: TXMLSettingsModel);

(*---------------------------------------------------------
  Zeigt den ConfigCode mit je einem Bit in einer Zeile, in einer TCheckListBox an.
  Ausserdem werden die Elementnamen aus dem XML angezeigt, soweit ein Name zugeordnet ist.
----------------------------------------------------------*)
procedure TGBDialogConfigCode.ShowConfigCode(aConfigCode: WORD);
var
  xElement: IXMLDOMElement;
  j: Integer;
  xSelPath: string;
  xElements: IXMLDOMNodeList;
  i: Integer;
begin
  for i := 0 to 15 do begin
    if assigned(fModel) and (fModel.MaConfigReader.Available) then begin
      // Im Defaults-XML alle Elemente suchen die als gew�nschter Configcode bezeichnet sind
      xElements := fModel.MaConfigReader.Dom.SelectNodes(Format('//*[@%s="%s" and @%s="%d"]',
                                                                [cAtrConfigCode, cConfogCodeNameArray[ConfigCodeType], cAtrBitNr, i]));
      for j := 0 to xElements.Length - 1 do begin
        // Im MMXML muss geschaut werden, ob das Element auch im MMXML vorkommt (Zenit, Spectra problematik)
        xSelPath := TXMLConstraints.GetMMSelPathFromElement(xElements[j]);
        // Liefert das Element aus dem MMXML
        xElement := fModel.Node[xSelPath];
        // Wenn das Element vorhanden ist, dann wird dieser Name zugeordnet
        if assigned(xElement)then
          mclbConfigBits.Items[i] := Format('Bit%d (%s)', [i, xElement.nodeName]);
      end;// for j := 0 to xElements.Length - 1 do begin
    end;// if assigned(fModel) and (fModel.MaConfigReader.Available) then begin
    mclbConfigBits.Checked[i] := ((aConfigCode and (1 shl i)) > 0);
  end;// for i := 0 to 15 do begin
end;// procedure TGBDialogConfigCode.ShowConfigCode(aConfigCode: WORD);

//:---------------------------------------------------------------------------
procedure TGBDialogConfigCode.ValueUpdate(Sender: TBaseXMLSettingsObserver);
begin
  // Nur aktualisieren, wenn auch sichtbar
  if Visible then
    CalculateCC(Sender.ChangedElementXP);
end;// procedure TGBDialogConfigCode.ValueUpdate(Sender: TBaseXMLSettingsObserver);

(*---------------------------------------------------------
  Berechnet den ConfigCode als Zahl. Sofern vorhanden wird das REsultat mit dem
  ganzen Config Code initialisiert (z.B. Das Element <ConfigCodeA/>). Die einzelnen
  boolschen Werte werden dann explitit als einzelne Bits in diese Zahl
  "hineingesetzt" oder "herausgel�scht".
----------------------------------------------------------*)
function TConfigCodeCalculator.CalculateConfigCode: Word;
var
  xValue: boolean;
  xInvert: boolean;
  i: Integer;
  xElements: IXMLDOMNodeList;
  xBitNr: Integer;
  xSelPath: string;
begin
  Result := 0;
  xValue := False;
  if assigned(fModel) then begin
    if ConfigCodeType=ccB then
      CodeSite.SendString('XMl-ConfigCode', FormatXML(FModel.xmlAsDOM));

    // Wenn das Model vorhanden ist, kann der Config Code als Zahl gelesen werden (sofern existent)
    result := fModel.ValueDef[cConfigCodeXPArray[ConfigCodeType], result];

    // Im Defaults XML (MaConfig) steht, welches Element f�r welchen ConfigCode steht
//    if (fModel.MaConfigReader.Available) then begin
    if (fModel.MaConfigReader.Available) and (not(fModel.MaConfigReader.NetTyp in [ntNone, ntLX])) then begin
      // Alle Bits durchlaufen
      for xBitNr := 0 to 15 do begin
        // Im Defaults-XML alle Elemente suchen die als gew�nschter Configcode bezeichnet sind (z.B alle B6)
        xElements := fModel.MaConfigReader.Dom.SelectNodes(Format('//*[@%s="%s" and @%s="%d"]',
                                                                  [cAtrConfigCode, cConfogCodeNameArray[ConfigCodeType], cAtrBitNr, xBitNr]));
        (* Die Liste durchsuchen und das Element verwenden, das im MMXML vorhanden ist.
           (Durch Zenit oder Spectra k�nnen mehrere Elemente vorhanden sein) *)
        for i := 0 to xElements.Length - 1 do begin
          // Selektionspfad aus dem MaConfig entspricht nicht dem MMXML Pfad
          xSelPath := TXMLConstraints.GetMMSelPathFromElement(xElements[i]);

          // Jetzt muss noch festgestellt werden, ob der CC im MMXML invertiert gespeichert wird
          xInvert := AttributeToBoolDef(cAtrConfigCodeInverted, xElements[i], false);
          // ... Und den Wert aus dem MMXML holen, und ggf invertieren
          try
            xValue := fModel.ValueDef[xSelPath, xInvert] ;
          except
            on e: EVariantError do
              xValue := xInvert;
          end;

          if xInvert then
            xValue := not(xValue);

          (* Ein gesetztes Bit mit Or schreiben.
             Ein gel�schtes Bit mit AND ausmaskieren. *)
          if xValue then
            Result := Result or (1 shl xBitNr)
          else
            Result := Result and(not(1 shl xBitNr));

        end;// for i := 0 to xElements.Length - 1 do begin
      end;// for i := 0 to 15 do begin
      // Das Resultat am Ende wieder ins MMXML schreiben, damit die Einzel-Elemente mit der Zahl syncron sind
      fModel.Value[cConfigCodeXPArray[ConfigCodeType]] := result;
    end;// if (fModel.MaConfigReader.Available) and (not(fModel.MaConfigReader.NetTyp in [ntLZE])) then begin
  end;// if assigned(fModel) then begin
end;// function TConfigCodeCalculator.CalculateConfigCode: DWord;

(*---------------------------------------------------------
  Stellt fest, ob das ge�nderte Element in einem ConfigCode vorkommt.
  Ist das der Fall, dann muss die Anzeige aktualisiert werden.
----------------------------------------------------------*)
function TConfigCodeCalculator.IsElementConfigCode(aSelPath: string): Boolean;
var
  xElement: IXMLDOMElement;
begin
  Result := false;
  if assigned(fModel) and (fModel.MaConfigReader.Available) then begin
    // Holt das Element aus den Defaults (Im Defaults Element steht ob das Element ein ConfigCode ist)
    xElement := fModel.MaConfigReader.Constraints[aSelPath].DOMElement;
    if assigned(xElement) then
      result := (AttributeToStringDef(cAtrConfigCode, xElement, '') > '');
  end;// if assigned(fModel) and (fModel.MaConfigReader.Available) then begin
end;// function TConfigCodeCalculator.IsElementConfigCode(aSelPath: string): Boolean;

//:---------------------------------------------------------------------------
procedure TGBDialogConfigCode.mbCloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;// procedure TGBDialogConfigCode.mbCloseClick(Sender: TObject);

(*---------------------------------------------------------
  Beim Anzeigen des Formulars muss der aktuelle ConfigCode ermittelt werden
----------------------------------------------------------*)
procedure TGBDialogConfigCode.FormShow(Sender: TObject);
begin
  CalculateCC('');
end;// procedure TGBDialogConfigCode.FormShow(Sender: TObject);

end.
