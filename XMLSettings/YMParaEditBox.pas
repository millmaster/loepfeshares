(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMParaEditBox.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...: EditBoxes for YarnMaster Settings.
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 15.11.1999  0.00  Kr  | Initial Release
| 04.09.2003  0.01  Kr  | SFI/D-Reference auf eine Kommastelle genau.
| 11.09.2003  0.01  Kr  | -Limit keine Kommastelle
|       2005  0.01  Wss | Umbau nach XML
| 23.03.2005  0.01  Wss | Property Plus von TYarnCountEditBox entfernt. NegDia ist immer verf�gbar
| 03.05.2005  0.01  Wss | PresetTable f�r Shortcount korrigiert
| 23.05.2005  0.01  Wss | Bug bei YarnCount behoben
| 09.06.2005  0.01  Wss | NSLT Wdh bei Channel hinzugef�gt
| 14.06.2005  0.01  Wss | Wiederholungen und Ext P-Params eigene Komponente
| 12.07.2005  0.01  Wss | PStartupRepSwitchItem: Konst-Name hat ge�nert durch export von XMLDef
| 21.07.2005  0.01  Wss | TYarnCountEditBox.SetYarnCount: Coarse/Fine wurde nicht berechnet -> fix
| 12.08.2005        Lok | TYarnCountEditBox.CheckComponents: pr�fen ob ein Modell existiert
| 24.11.2005        Wss | - P-Settings: cPRefLength immer auslesen
                          - F-Cluster: Faults immer auslesen
                          - F-Cluster: Faults keinen XPath mehr f�r Switch
| 13.02.2006        Wss | Pauschales Enablen von TYarnCountEditBox in CheckComponents()
                          f�hrte dazu, dass im Template Modus die Coarse/Fine Felder pl�tzlich
                          auch verf�gbar waren -> Enabled := True entfernt
| 21.03.2006        Nue | MeasureWidth f�r cYCCount auf 40 erh�ht, damit z.B. NeC vollst�ndig angezeigt wird.
| 27.07.2006        Wss | IPI Parameter Definition und GroupBox eingef�gt
| 06.09.2006        Wss | NSLT/P-Repetition: beim aufsplitten in die Rep-Groupbox ist
                          die Behandlung des Off-Status verloren gegangen -> fixed
| 21.12.2006        Nue | Anpassen DisplayFormat Yarncount von 1 auf 2 Stellen nach dem Komma.
| 10.01.2008        Nue | VCV eingef�gt.  2.4.08:VCVLength in Update noch nachgetragen    cPTVCVLength
| 29.04.2008        Nue | cPTVCVLength implementiert.
| 07.05.2008        Nue | Aenderungen in ValueUpdate von SFI und VCV get�tigt.
| 26.05.2008        Nue | Aenderungen VCV-Lower/Upper-Bereich (neu 100) get�tigt.
|=============================================================================*)
unit YMParaEditBox;

interface

uses
  Classes, SysUtils, mmPanel, EditBox, KeySpinEdit, YMParaDef, YMParaUtils,
  MMUGlobal, PresetValue, IvDictio, XMLDef, XMLSettingsModel, VCLXMLSettingsModel, mmLabel;

type
  TSettingsGroup = (sgNone, sgChannel, sgSplice, sgCluster, sgFCluster, sgSFI, sgVCV, sgYarnCount, sgP, sgPExt, sgRepetition, sgIPILimits);

resourcestring
  rsShortTitle        = '(40)stat_kurz'; // ivlm
  rsLongTitle         = '(40)stat_lang'; // ivlm
  rsThinTitle         = '(40)stat_duenn'; // ivlm
  rsHeadClassTitle    = '(28)Tastkopf-Klasse:'; // ivlm

  rsNormalCountTitle  = '(15)Normal'; // ivlm
  rsShortCountTitle   = '(40)laenge_kurz'; // ivlm

  rsRepetition        = '(12)Wiederholung'; // ivlm
//  rsRep               = '(3)Wdh'; 
  rsObsLength         = '(12)Ueberw.-Laenge'; //ivlm
  rsFaults            = '(7)Fehler'; //ivlm

  rsCHCaption         = '(8)Kanal'; //ivlm
  rsSPCaption         = '(8)Spleiss'; //ivlm
  rsSwOff             = '(3)&Aus'; //ivlm
  rsMeasureProcent    = '%';
  rsMeasureCm         = 'cm';

  rsNepDiameter       = 'N';
  rsNepDiameterHint   = '(*)Noppen Durchmesser:'; //ivlm
  rsShortDiameter     = 'DS';
  rsShortLength       = 'LS';
  rsShortDiameterHint = '(*)Kurzfehler Durchmesser:'; //ivlm
  rsShortLengthHint   = '(*)Kurzfehler Laenge:'; //ivlm
  rsLongDiameter      = 'DL';
  rsLongLength        = 'LL';
  rsLongDiameterHint  = '(*)Langfehler Durchmesser:'; //ivlm
  rsLongLengthHint    = '(*)Langfehler Laenge:'; //ivlm
  rsThinDiameter      = '-D';
  rsThinLength        = '-L';
  rsThinDiameterHint  = '(*)Duennfehler Durchmesser:'; //ivlm
  rsThinLengthHint    = '(*)Duennfehler Laenge:'; //ivlm

  rsUpYDiameter       = 'UpY';
  rsUpYDiameterHint   = '(*)Oberfaden Durchmesser:'; //ivlm

  rsYCCaption         = '(11)Garnnummer'; //ivlm
  rsYCPosDiaDiff      = '(9)DiaDiff +'; //ivlm
  rsYCPosDiaDiffHint  = '(*)Durchmesserzunahme:'; //ivlm
  rsYCNegDiaDiff      = '(9)DiaDiff -'; //ivlm
  rsYCNegDiaDiffHint  = '(*)Durchmesserabnahme:'; //ivlm

  rsYCCount           = '(9)Feinheit'; //ivlm
  rsYCCorse           = '(9)Grob'; //ivlm
  rsYCCorseHint       = '(*)Grenzwert grob:'; //ivlm
  rsYCFine            = '(9)Fein'; //ivlm
  rsYCFineHint        = '(*)Grenzwert fein:'; //ivlm

  rsYCLength          = '(7)Laenge'; //ivlm
  rsMeasureM          = 'm';
  rsYCLengthHint      = '(*)Bezugslaenge:'; //ivlm

  rsClusterCaption    = '(11)Fehlerschwarm'; //ivlm
  rsStartupCutCaption = '(12)Anlaufschnitt'; //ivlm
  rsFClusterCaption   = '(*)Fremdfasern'; //ivlm
  rsFCDiameter        = '(12)Durchmesser'; //ivlm
  rsFCDiameterHint    = '(*)Fehlerschwarm Durchmesser:'; //ivlm
  rsFCLength          = '(8)Laenge'; //ivlm
  rsFCShortLengthHint = '(*)Kurzfehler Laenge:'; //ivlm
  rsFCObsLengthHint   = '(*)Beobachtungslaenge:'; //ivlm
  rsFCFaultsHint      = '(*)Fehlerzahl bezogen auf die Bobachtungslaenge:'; //ivlm
  rsFCRepetitionHint  = '(*)Fehlerschwarmschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
//  rsFDarkCaption      = '(11)Dunkel'; // ivl
//  rsFBrightCaption    = '(11)Hell'; // ivl
//  rsFStartupRepCaption = '(12)Anlaufschnitt';
//  rsFRepetitionHint   = '(*)Fremdmaterialienschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsFRepetitionHint    = '(*)FF Schwarmschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsSFIRepetitionHint  = '(*)SFI Schnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsVCVRepetitionHint  = '(*)VCV Schnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsYarnCountRepHint   = '(*)Nummerabweichungsschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsFStartupRepHint    = '(*)FF Anlaufschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsNSLTStartupRepHint = '(*)NSLT Anlaufschnittwiederholungen bis zur Alarmintervention:'; //ivlm

  rsSFICaption        = '(11)SFI/D'; //ivlm
  rsSFIReferenc       = '(7)Referenz'; //ivlm
  rsSFIReferencHint   = '(*)SFI/D-Referenz:'; //ivlm
  rsSFIFloat          = '(9)&gleitend'; //ivlm
  rsSFIPosLimit       = '(7)+Grenze'; //ivlm
  rsSFIPosLimitHint   = '(*)Positive Abweichung vom Referenzwert:'; //ivlm
  rsSFINegLimit       = '(7)-Grenze'; //ivlm
  rsSFINegLimitHint   = '(*)Negative Abweichung vom Referenzwert:'; //ivlm

  rsVCVCaption        = '(11)VCV'; //ivlm
  rsVCVPosLimit       = '(7)+Grenze'; //ivlm
  rsVCVPosLimitHint   = '(*)Positive Abweichung vom Referenzwert:'; //ivlm
  rsVCVNegLimit       = '(7)-Grenze'; //ivlm
  rsVCVNegLimitHint   = '(*)Negative Abweichung vom Referenzwert:'; //ivlm

  rsIPICaption    = '(11)IPI Limits'; //ivlm
  rsIPINeps       = '(11)Neps'; //ivlm
  rsIPISmallPlus  = '(11)Small Plus'; //ivlm
  rsIPISmallMinus = '(11)Small Minus'; //ivlm
  rsIPIThick      = '(11)Thick'; //ivlm
  rsIPIThin       = '(11)Thin'; //ivlm

//  rsFFCCaption        = '(11)FF-Cluster'; 
  rsFFCFaults         = 'F';
  rsFFCFaultsHint     = '(*)Fehlerzahl bezogen auf die Bobachtungslaenge:'; //ivlm

  rsPCaption          = '(11)P Einstellungen'; //ivlm
  rsPExtCaption       = '(15)P Konfiguration'; //ivlm
  rsPLimitCaption     = '(11)Limit'; //ivlm
  rsPLimitHint        = '(*)Empfindlichkeits-Limite:'; //ivlm
  rsPRefLengthCaption = '(11)Ref.-Laenge'; //ivlm
  rsPLowLimitCaption  = '(11)Low Limit'; //ivlm
  rsPLowBitCaption    = '(11)Low Bit'; //ivlm
  rsPReductionCaption = '(11)Reduktion'; //ivlm
  rsPRefLengthHint    = '(*)Referenz-Laenge des Polypropylen-Fehlers:'; //ivlm
  rsPStartupRepHint   = '(*)P Anlaufschnitt-Wiederholungen bis zur Alarmintervention:'; //ivlm
  rsPLowLimitHint     = '(*)Hilfseinstellung zu den P-Parameter:'; //ivlm
  rsPLowBitHint       = '(*)Hilfseinstellung zu den P-Parameter:'; //ivlm
  rsPReductionHint    = '(*)Reduktion des Durchmessereinflusses bei der Polypropylenreinigung:'; //ivlm

//------------------------------------------------------------------------------
// Channel Values definitions
//------------------------------------------------------------------------------
const
  cChN    = 0;
  cChDS   = 1;
  cChLS   = 2;
  cChDL   = 3;
  cChLL   = 4;
  cChDT   = 5;
  cChLT   = 6;
  cChNSLT = 7;

  cTestCaption: string = 'DDDDDDDDDDD';
//  cCHCaption: string   = rsCHCaption;
//  cSPCaption: string   = rsSPCaption;

  cPTNoPreset: array[0..0] of TPresetTextRec = (());

  cPTCHNepDiameter: array[0..9] of TPresetTextRec = (
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '3.5'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '4.5'; ),
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '5.5'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '6.5'; ),
    (typ: ttFloat; text: '7.0'; ),
    (typ: ttCmdDisabled; text: rsSwOff; )
  );

  cPTCHShortDiameter: array[0..26] of TPresetTextRec = (
    (typ: ttFloat; text: '1.50'; ),
    (typ: ttFloat; text: '1.60'; ),
    (typ: ttFloat; text: '1.70'; ),
    (typ: ttFloat; text: '1.80'; ),
    (typ: ttFloat; text: '1.90'; ),
    (typ: ttFloat; text: '2.00'; ),
    (typ: ttFloat; text: '2.10'; ),
    (typ: ttFloat; text: '2.20'; ),
    (typ: ttFloat; text: '2.30'; ),
    (typ: ttFloat; text: '2.40'; ),
    (typ: ttFloat; text: '2.50'; ),
    (typ: ttFloat; text: '2.60'; ),
    (typ: ttFloat; text: '2.70'; ),
    (typ: ttFloat; text: '2.80'; ),
    (typ: ttFloat; text: '2.90'; ),
    (typ: ttFloat; text: '3.00'; ),
    (typ: ttFloat; text: '3.10'; ),
    (typ: ttFloat; text: '3.20'; ),
    (typ: ttFloat; text: '3.30'; ),
    (typ: ttFloat; text: '3.40'; ),
    (typ: ttFloat; text: '3.50'; ),
    (typ: ttFloat; text: '3.60'; ),
    (typ: ttFloat; text: '3.70'; ),
    (typ: ttFloat; text: '3.80'; ),
    (typ: ttFloat; text: '3.90'; ),
    (typ: ttFloat; text: '4.00'; ),
    (typ: ttCmdDisabled; text: rsSwOff; )
  );

  cPTCHShortLength: array[0..29] of TPresetTextRec = (
    (typ: ttFloat; text: '1.1'; ),
    (typ: ttFloat; text: '1.2'; ),
    (typ: ttFloat; text: '1.3'; ),
    (typ: ttFloat; text: '1.4'; ),
    (typ: ttFloat; text: '1.5'; ),
    (typ: ttFloat; text: '1.6'; ),
    (typ: ttFloat; text: '1.7'; ),
    (typ: ttFloat; text: '1.8'; ),
    (typ: ttFloat; text: '1.9'; ),
    (typ: ttFloat; text: '2.0'; ),
    (typ: ttFloat; text: '2.1'; ),
    (typ: ttFloat; text: '2.2'; ),
    (typ: ttFloat; text: '2.3'; ),
    (typ: ttFloat; text: '2.4'; ),
    (typ: ttFloat; text: '2.5'; ),
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '3.5'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '4.5'; ),
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '5.5'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '6.5'; ),
    (typ: ttFloat; text: '7.0'; ),
    (typ: ttFloat; text: '7.5'; ),
    (typ: ttFloat; text: '8.0'; ),
    (typ: ttFloat; text: '8.5'; ),
    (typ: ttFloat; text: '9.0'; ),
    (typ: ttFloat; text: '9.5'; ),
    (typ: ttFloat; text: '10.0'; )
  );

  cPTCHLongDiameter: array[0..28] of TPresetTextRec = (
    (typ: ttFloat; text: '1.10'; ),
    (typ: ttFloat; text: '1.12'; ),
    (typ: ttFloat; text: '1.14'; ),
    (typ: ttFloat; text: '1.16'; ),
    (typ: ttFloat; text: '1.18'; ),
    (typ: ttFloat; text: '1.20'; ),
    (typ: ttFloat; text: '1.22'; ),
    (typ: ttFloat; text: '1.24'; ),
    (typ: ttFloat; text: '1.26'; ),
    (typ: ttFloat; text: '1.28'; ),
    (typ: ttFloat; text: '1.30'; ),
    (typ: ttFloat; text: '1.32'; ),
    (typ: ttFloat; text: '1.34'; ),
    (typ: ttFloat; text: '1.36'; ),
    (typ: ttFloat; text: '1.38'; ),
    (typ: ttFloat; text: '1.40'; ),
    (typ: ttFloat; text: '1.45'; ),
    (typ: ttFloat; text: '1.50'; ),
    (typ: ttFloat; text: '1.55'; ),
    (typ: ttFloat; text: '1.60'; ),
    (typ: ttFloat; text: '1.65'; ),
    (typ: ttFloat; text: '1.70'; ),
    (typ: ttFloat; text: '1.75'; ),
    (typ: ttFloat; text: '1.80'; ),
    (typ: ttFloat; text: '1.85'; ),
    (typ: ttFloat; text: '1.90'; ),
    (typ: ttFloat; text: '1.95'; ),
    (typ: ttFloat; text: '2.00'; ),
    (typ: ttCmdDisabled; text: rsSwOff; )
  );

  cPTCHThinDiameter: array[0..17] of TPresetTextRec = (
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '12'; ),
    (typ: ttFloat; text: '14'; ),
    (typ: ttFloat; text: '16'; ),
    (typ: ttFloat; text: '18'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '22'; ),
    (typ: ttFloat; text: '24'; ),
    (typ: ttFloat; text: '26'; ),
    (typ: ttFloat; text: '28'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '35'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '45'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttFloat; text: '55'; ),
    (typ: ttFloat; text: '60'; ),
    (typ: ttCmdDisabled; text: rsSwOff; )
  );

  cPTCHLongThinLength: array[0..19] of TPresetTextRec = (
    (typ: ttFloat; text: '6'; ),
    (typ: ttFloat; text: '9'; ),
    (typ: ttFloat; text: '12'; ),
    (typ: ttFloat; text: '15'; ),
    (typ: ttFloat; text: '18'; ),
    (typ: ttFloat; text: '21'; ),
    (typ: ttFloat; text: '24'; ),
    (typ: ttFloat; text: '27'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttFloat; text: '60'; ),
    (typ: ttFloat; text: '70'; ),
    (typ: ttFloat; text: '80'; ),
    (typ: ttFloat; text: '90'; ),
    (typ: ttFloat; text: '100'; ),
    (typ: ttFloat; text: '110'; ),
    (typ: ttFloat; text: '120'; ),
    (typ: ttFloat; text: '160'; ),
    (typ: ttFloat; text: '200'; )
  );

  cChannelValuesDef: Array[cChN..cChLT] of TValueSpecRec = (
     // Nep Dia
    (MinValue: cNepsDiaMin; MaxValue: cNepsDiaMax;
     Space: 0; Factor: 1; Format: '%3.1f'; Measure: '';
     Caption: rsNepDiameter;
     Hint: rsNepDiameterHint;
     XPValue:  cXPChannelNepsDiaItem;
     XPSwitch: cXPChannelNepsSwitchItem),
     // Short Dia
    (MinValue: cShortDiaMin; MaxValue: cShortDiaMax;
     Space: cGEBSpace; Factor: 1; Format: '%4.2f'; Measure: '';
     Caption: rsShortDiameter;
     Hint: rsShortDiameterHint;
     XPValue: cXPChannelShortDiaItem;
     XPSwitch: cXPChannelShortSwitchItem),
     // Short Len
    (MinValue: cShortLenMin; MaxValue: cShortLenMax;
     Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureCm;
     Caption: rsShortLength;
     Hint: rsShortLengthHint;
     XPValue: cXPChannelShortLengthItem;
     XPSwitch: cXPChannelShortSwitchItem),
     // Long Dia
    (MinValue: cLongDiaMin; MaxValue: cLongDiaMax;
     Space: cGEBSpace; Factor: 1; Format: '%4.2f'; Measure: '';
     Caption: rsLongDiameter;
     Hint: rsLongDiameterHint;
     XPValue: cXPChannelLongDiaItem;
     XPSwitch: cXPChannelLongSwitchItem),
     // Long Len
    (MinValue: cLongLenMin; MaxValue: cLongLenMax;
     Space: 0; Factor: 1; Format: '%3.0f'; Measure: rsMeasureCm;
     Caption: rsLongLength;
     Hint: rsLongLengthHint;
     XPValue: cXPChannelLongLengthItem;
     XPSwitch: cXPChannelLongSwitchItem),
     // Thin Dia
    (MinValue: (1-cThinDiaMaxAbove)*100; MaxValue: (1-cThinDiaMin)*100;
     Space: cGEBSpace; Factor: 100; Format: '%2.0f'; Measure: rsMeasureProcent;
     Caption: rsThinDiameter;
     Hint: rsThinDiameterHint;
     XPValue: cXPChannelThinDiaItem;
     XPSwitch: cXPChannelThinSwitchItem),
     // Thin Len
    (MinValue: cThinLenMin; MaxValue: cThinLenMax;
     Space: 0; Factor: 1; Format: '%3.0f'; Measure: rsMeasureCm;
     Caption: rsThinLength;
     Hint: rsThinLengthHint;
     XPValue: cXPChannelThinLengthItem;
     XPSwitch: cXPChannelThinSwitchItem)
//     // NSLT Repetition (only Zenit)
//    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
//     Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
//     Caption: rsRep;
//     Hint: rsNSLTStartupRepHint;
//     XPValue: cXPNSLTStartupRepetitionItem;
//     XPSwitch: cXPChannelNSLTSwitchItem)
  );

//------------------------------------------------------------------------------
// Splice Values definitions
//------------------------------------------------------------------------------
  cSpN    = 0;
  cSpDS   = 1;
  cSpLS   = 2;
  cSpDL   = 3;
  cSpLL   = 4;
  cSpDT   = 5;
  cSpLT   = 6;
  cSPDUpY = 7;

  cSpliceValuesDef: Array[cSpN..cSPDUpY] of TValueSpecRec = (
     // Nep Dia
    (MinValue: cNepsDiaMin; MaxValue: cNepsDiaMax;
     Space: 0; Factor: 1; Format: '%3.1f'; Measure: '';
     Caption: rsNepDiameter;
     Hint: rsNepDiameterHint;
     XPValue:  cXPSpliceNepsDiaItem;
     XPSwitch: cXPSpliceNepsSwitchItem),
     // Short Dia
    (MinValue: cShortDiaMin; MaxValue: cShortDiaMax;
     Space: cGEBSpace; Factor: 1; Format: '%4.2f';
     Caption: rsShortDiameter;
     Hint: rsShortDiameterHint;
     XPValue: cXPSpliceShortDiaItem;
     XPSwitch: cXPSpliceShortSwitchItem),
     // Short Len
    (MinValue: cShortLenMin; MaxValue: cShortLenMax;
     Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureCm;
     Caption: rsShortLength;
     Hint: rsShortLengthHint;
     XPValue: cXPSpliceShortLengthItem;
     XPSwitch: cXPSpliceShortSwitchItem),
     // Long Dia
    (MinValue: cLongDiaMin; MaxValue: cLongDiaMax;
     Space: cGEBSpace; Factor: 1; Format: '%4.2f';
     Caption: rsLongDiameter;
     Hint: rsLongDiameterHint;
     XPValue: cXPSpliceLongDiaItem;
     XPSwitch: cXPSpliceLongSwitchItem),
     // Long Len
    (MinValue: cLongLenMin; MaxValue: cLongLenMax;
     Space: 0; Factor: 1; Format: '%3.0f'; Measure: rsMeasureCm;
     Caption: rsLongLength;
     Hint: rsLongLengthHint;
     XPValue: cXPSpliceLongLengthItem;
     XPSwitch: cXPSpliceLongSwitchItem),
     // Thin Dia
    (MinValue: (1-cThinDiaMaxAbove)*100; MaxValue: (1-cThinDiaMin)*100;
     Space: cGEBSpace; Factor: 100; Format: '%2.0f'; Measure: rsMeasureProcent;
     Caption: rsThinDiameter;
     Hint: rsThinDiameterHint;
     XPValue: cXPSpliceThinDiaItem;
     XPSwitch: cXPSpliceThinSwitchItem),
     // Thin Len
    (MinValue: cThinLenMin; MaxValue: cThinLenMax;
     Space: 0; Factor: 1; Format: '%3.0f'; Measure: rsMeasureCm;
     Caption: rsThinLength;
     Hint: rsThinLengthHint;
     XPValue: cXPSpliceThinLengthItem;
     XPSwitch: cXPSpliceThinSwitchItem),
     // Upper Yarn
    (MinValue: cLongDiaMin; MaxValue: cLongDiaMax;
     Space: cGEBSpace; Factor: 1; Format: '%4.2f'; Measure: '';
     Caption: rsUpYDiameter;
     Hint: rsUpYDiameterHint;
     XPValue: cXPSpliceUpperYarnDiaItem;
     XPSwitch: cXPSpliceUpperYarnSwitchItem)
  );

//------------------------------------------------------------------------------
// Yarn Count Monitor Values definitions
//------------------------------------------------------------------------------
  // Yarn Count
  cYCPDiaDiff   = 0;
  cYCNDiaDiff   = 1;
  CYCCount      = 2;
  CYCCorse      = 3;
  CYCFine       = 4;
  cYCLength     = 5;
//  cYCRepetition = 6;
  // Short Count
  cSCPDiaDiff   = 6;
  cSCNDiaDiff   = 7;
  cSCCorse      = 8;
  cSCFine       = 9;
  cSCLength     = 10;
//  cSCRepetition = 12;


  cPTYCDiaDiff: array[0..20] of TPresetTextRec = (
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '4.5'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '7.0'; ),
    (typ: ttFloat; text: '8.0'; ),
    (typ: ttFloat; text: '9.0'; ),
    (typ: ttFloat; text: '10.0'; ),
    (typ: ttFloat; text: '12.0'; ),
    (typ: ttFloat; text: '14.0'; ),
    (typ: ttFloat; text: '16.0'; ),
    (typ: ttFloat; text: '18.0'; ),
    (typ: ttFloat; text: '20.0'; ),
    (typ: ttFloat; text: '24.0'; ),
    (typ: ttFloat; text: '28.0'; ),
    (typ: ttFloat; text: '32.0'; ),
    (typ: ttFloat; text: '36.0'; ),
    (typ: ttFloat; text: '40.0'; ),
    (typ: ttFloat; text: '44.0'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));

  cPTYCCount: array[0..1] of TPresetTextRec = (
    (typ: ttCmdDisabled; text: rsSwOff; ),
    (typ: ttEmpty; text: ''; ));

  cPTYCLength: array[0..4] of TPresetTextRec = (
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '50'; ));

  cPTSCLength: array[0..6] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '8'; ),
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '32'; ));

  cYarnCountValuesDef: Array[cYCPDiaDiff..cSCLength] of TValueSpecRec = (
     // Normal Pos DiaDiff
    (MinValue: cDiaDiffMin; MaxValue: cDiaDiffMax;
     Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureProcent;
     Caption: rsYCPosDiaDiff;
     Hint: rsYCPosDiaDiffHint;
     XPValue: cXPCountPosDiaDiffItem;
     XPSwitch: cXPYarnCountSwitchItem),
     // Normal Neg DiaDiff
    (MinValue: cDiaDiffMin; MaxValue: cDiaDiffMax;
     Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureProcent;
     Caption: rsYCNegDiaDiff;
     Hint: rsYCNegDiaDiffHint;
     XPValue: cXPCountNegDiaDiffItem;
     XPSwitch: cXPYarnCountSwitchItem),
     // Normal YarnCount
    (MinValue: cYarnCountMin; MaxValue: cYarnCountMax;
     Space: 4; Factor: 1; Format: '%4.2f'; Measure: ' ';
     Caption: rsYCCount;
     Hint: '';
     XPValue: '';
     XPSwitch: cXPYarnCountSwitchItem),
     // Normal Coarse
    (MinValue: cYarnCountMin; MaxValue: cYarnCountMax;
     Space: 4; Factor: 1; Format: '%4.2f'; Measure: '';
     Caption: rsYCCorse;
     Hint: rsYCCorseHint;
     XPValue: '';
     XPSwitch: cXPYarnCountSwitchItem),
     // Normal Fine
    (MinValue: cYarnCountMin; MaxValue: cYarnCountMax;
     Space: 0; Factor: 1; Format: '%4.2f'; Measure: '';
     Caption: rsYCFine;
     Hint: rsYCFineHint;
     XPValue: '';
     XPSwitch: cXPYarnCountSwitchItem),
     // Normal YarnCount Len
    (MinValue: 10; MaxValue: 50;
     Space: cGEBSpace; Factor: 1; Format: '%2.0f'; Measure: rsMeasureM;
     Caption: rsYCLength;
     Hint: rsYCLengthHint;
     XPValue:  cXPYarnCountObsLengthItem;
     XPSwitch: ''), //cXPYarnCountSwitchItem),
//     // Normal YarnCount Repetition
//    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
//     Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
//     Caption: rsRepetition;
//     Hint: rsYarnCountRepHint;
//     XPValue: cXPYarnCountRepetitionItem;
//     XPSwitch: ''),

     // Short Pos DiaDiff
    (MinValue: cDiaDiffMin; MaxValue: cDiaDiffMax;
     Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureProcent;
     Caption: '';
     Hint: rsYCPosDiaDiffHint;
     XPValue: cXPShortCountPosDiaDiffItem;
     XPSwitch: cXPYarnShortCountSwitchItem),
     // Short Neg DiaDiff
    (MinValue: cDiaDiffMin; MaxValue: cDiaDiffMax;
     Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureProcent;
     Caption: '';
     Hint: rsYCNegDiaDiffHint;
     XPValue: cXPShortCountNegDiaDiffItem;
     XPSwitch: cXPYarnShortCountSwitchItem),
     // Short Coarse
    (MinValue: cYarnCountMin; MaxValue: cYarnCountMax;
     Space: 4; Factor: 1; Format: '%4.1f'; Measure: '';
     Caption: '';
     Hint: rsYCCorseHint;
     XPValue: '';
     XPSwitch: cXPYarnShortCountSwitchItem),
     // Short Fine
    (MinValue: cYarnCountMin; MaxValue: cYarnCountMax;
     Space: 0; Factor: 1; Format: '%4.1f'; Measure: '';
     Caption: '';
     Hint: rsYCFineHint;
     XPValue: '';
     XPSwitch: cXPYarnShortCountSwitchItem),
     // Short YarnCount Len
    (MinValue: 1; MaxValue: 32;
     Space: cGEBSpace; Factor: 1; Format: '%2.0f'; Measure: rsMeasureM;
     Caption: '';
     Hint: rsYCLengthHint;
     XPValue: cXPYarnShortCountObsLengthItem;
     XPSwitch: '')
//     // Short YarnCount Repetition
//    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
//     Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
//     Caption: '';
//     Hint: rsYarnCountRepHint;
//     XPValue: cXPYarnShortCountRepetitionItem;
//     XPSwitch: '')
  );

//------------------------------------------------------------------------------
// Fault Cluster Monitor Values definitions
//------------------------------------------------------------------------------
  cPTClObsLength: array[0..18] of TPresetTextRec = (
    (typ: ttFloat; text: '0.1'; ),
    (typ: ttFloat; text: '0.4'; ),
    (typ: ttFloat; text: '0.7'; ),
    (typ: ttFloat; text: '1.0'; ),
    (typ: ttFloat; text: '2.0'; ),
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '7.0'; ),
    (typ: ttFloat; text: '8.0'; ),
    (typ: ttFloat; text: '9.0'; ),
    (typ: ttFloat; text: '10.0'; ),
    (typ: ttFloat; text: '20.0'; ),
    (typ: ttFloat; text: '30.0'; ),
    (typ: ttFloat; text: '40.0'; ),
    (typ: ttFloat; text: '50.0'; ),
    (typ: ttFloat; text: '80.0'; ),
    (typ: ttCmdDisabled; text: rsSwOff; )
  );

  cPTClFaults: array[0..16] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '3'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '5'; ),
    (typ: ttFloat; text: '6'; ),
    (typ: ttFloat; text: '7'; ),
    (typ: ttFloat; text: '8'; ),
    (typ: ttFloat; text: '9'; ),
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '12'; ),
    (typ: ttFloat; text: '14'; ),
    (typ: ttFloat; text: '16'; ),
    (typ: ttFloat; text: '18'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '25'; ),
    (typ: ttFloat; text: '30'; )
//    (typ: ttCmdDisabled; text: rsSwOff; )
  );

  cPTRepetition: array[0..8] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '3'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '5'; ),
    (typ: ttFloat; text: '6'; ),
    (typ: ttFloat; text: '7'; ),
    (typ: ttFloat; text: '8'; ),
    (typ: ttFloat; text: '9'; )
  );

  cPTRepetitionOff: array[0..9] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '3'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '5'; ),
    (typ: ttFloat; text: '6'; ),
    (typ: ttFloat; text: '7'; ),
    (typ: ttFloat; text: '8'; ),
    (typ: ttFloat; text: '9'; ),
    (typ: ttCmdDisabled; text: rsSwOff; )
  );

  cClSDiameter   = 0;
  cClSLength     = 1;
  cClSObsLength  = 2;
  CClSFaults     = 3;
//  CClSRepetition = 4;

  cClLDiameter   = 4;
  cClLLength     = 5;
  cClLObsLength  = 6;
  CClLFaults     = 7;
//  CClLRepetition = 9;

  cClTDiameter   = 8;
  cClTLength     = 9;
  cClTObsLength  = 10;
  CClTFaults     = 11;
//  CClTRepetition = 14;

//  cFCCaption: string = rsFCCaption;
  cClusterValuesDef: Array[cClSDiameter..CClTFaults] of TValueSpecRec = (
    // Short Dia
   (MinValue: cClShortDiaMin; MaxValue: cClShortDiaMax;
    Space: 0; Factor: 1; Format: '%3.2f'; Measure: '';
    Caption: rsFCDiameter;
    Hint: rsFCDiameterHint;
    XPValue: cXPClusterShortDiaItem;
    XPSwitch: cXPClusterShortSwitchItem),
    // Short Len
   (MinValue: cClShortLenMin; MaxValue: cClShortLenMax;
    Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureCm;
    Caption: rsFCLength;
    Hint: rsShortLengthHint;
    XPValue: cXPClusterShortLengthItem;
    XPSwitch: cXPClusterShortSwitchItem),
    // Short ObsLength
   (MinValue: cClObsLengthMin; MaxValue: cClObsLengthMax;
    Space: cGEBSpace; Factor: 1; Format: '%4.1f'; Measure: rsMeasureM;
    Caption: rsObsLength;
    Hint: rsFCObsLengthHint;
    XPValue: cXPClusterShortObsLengthItem;
    XPSwitch: cXPClusterShortSwitchItem),
    // Short Defects
   (MinValue: cDefectsMin; MaxValue: cDefectsMax;
    Space: 0; Factor: 1; Format: '%4.0f'; Measure: '';
    Caption: rsFaults;
    Hint: rsFCFaultsHint;
    XPValue: cXPClusterShortDefectsItem;
    XPSwitch: ''), //cXPClusterShortSwitchItem),
//    // Short Repetitions
//   (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
//    Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: rsRepetition;
//    Hint: rsFCRepetitionHint;
//    XPValue: cXPClusterShortRepetitionItem;
//    XPSwitch: ''), //cXPClusterShortSwitchItem),

    // Long Dia
   (MinValue: cClLongDiaMin; MaxValue: cClLongDiaMax;
    Space: 0; Factor: 1; Format: '%3.2f'; Measure: '';
    Caption: '';
    Hint: rsFCDiameterHint;
    XPValue: cXPClusterLongDiaItem;
    XPSwitch: cXPClusterLongSwitchItem),
    // Long Len
   (MinValue: cClLongLenMin; MaxValue: cClLongLenMax;
    Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureCm;
    Caption: '';
    Hint: rsShortLengthHint;
    XPValue: cXPClusterLongLengthItem;
    XPSwitch: cXPClusterLongSwitchItem),
    // Long ObsLength
   (MinValue: cClObsLengthMin; MaxValue: cClObsLengthMax;
    Space: cGEBSpace; Factor: 1; Format: '%4.1f'; Measure: rsMeasureM;
    Caption: '';
    Hint: rsFCObsLengthHint;
    XPValue: cXPClusterLongObsLengthItem;
    XPSwitch: cXPClusterLongSwitchItem),
    // Long Defects
   (MinValue: cDefectsMin; MaxValue: cDefectsMax;
    Space: 0; Factor: 1; Format: '%4.0f'; Measure: '';
    Caption: '';
    Hint: rsFCFaultsHint;
    XPValue: cXPClusterLongDefectsItem;
    XPSwitch: ''), //cXPClusterLongSwitchItem),
    // Long Repetitions
//   (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
//    Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: '';
//    Hint: rsFCRepetitionHint;
//    XPValue: cXPClusterLongRepetitionItem;
//    XPSwitch: ''), //cXPClusterLongSwitchItem),

    // Thin Dia
   (MinValue: (1-cClThinDiaMaxAbove)*100; MaxValue: (1-cClThinDiaMin)*100;
    Space: 0; Factor: 100; Format: '%2.0f';  Measure: rsMeasureProcent;
    Caption: '-';
    Hint: rsFCDiameterHint;
    XPValue: cXPClusterThinDiaItem;
    XPSwitch: cXPClusterThinSwitchItem),
    // Thin Len
   (MinValue: cClThinLenMin; MaxValue: cClThinLenMax;
    Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureCm;
    Caption: '';
    Hint: rsShortLengthHint;
    XPValue: cXPClusterThinLengthItem;
    XPSwitch: cXPClusterThinSwitchItem),
    // Thin ObsLength
   (MinValue: cClObsLengthMin; MaxValue: cClObsLengthMax;
    Space: cGEBSpace; Factor: 1; Format: '%4.1f'; Measure: rsMeasureM;
    Caption: '';
    Hint: rsFCObsLengthHint;
    XPValue: cXPClusterThinObsLengthItem;
    XPSwitch: cXPClusterThinSwitchItem),
    // Thin Defects
   (MinValue: cDefectsMin; MaxValue: cDefectsMax;
    Space: 0; Factor: 1; Format: '%4.0f'; Measure: '';
    Caption: '';
    Hint: rsFCFaultsHint;
    XPValue: cXPClusterThinDefectsItem;
    XPSwitch: '')
//    // Thin Repetitions
//   (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
//    Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: '';
//    Hint: rsFCRepetitionHint;
//    XPValue: cXPClusterThinRepetitionItem;
//    XPSwitch: '') //cXPClusterThinSwitchItem)
  );
//------------------------------------------------------------------------------
// SFI/D Values definitions
//------------------------------------------------------------------------------
  cSFIReference  = 0;
  cSFIPosLimit   = 1;
  cSFINegLimit   = 2;
//  cSFIRepetition = 3;

  cPTSFIReferenc: array[0..21] of TPresetTextRec = (
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '7.0'; ),
    (typ: ttFloat; text: '8.0'; ),
    (typ: ttFloat; text: '9.0'; ),
    (typ: ttFloat; text: '10.0'; ),
    (typ: ttFloat; text: '11.0'; ),
    (typ: ttFloat; text: '12.0'; ),
    (typ: ttFloat; text: '13.0'; ),
    (typ: ttFloat; text: '14.0'; ),
    (typ: ttFloat; text: '15.0'; ),
    (typ: ttFloat; text: '16.0'; ),
    (typ: ttFloat; text: '17.0'; ),
    (typ: ttFloat; text: '18.0'; ),
    (typ: ttFloat; text: '19.0'; ),
    (typ: ttFloat; text: '20.0'; ),
    (typ: ttFloat; text: '21.0'; ),
    (typ: ttFloat; text: '22.0'; ),
    (typ: ttFloat; text: '23.0'; ),
    (typ: ttFloat; text: '24.0'; ),
    (typ: ttFloat; text: '25.0'; ),
    (typ: ttCmdDisabled; text: rsSFIFloat; ));

  cPTSFILimit: array[0..8] of TPresetTextRec = (
    (typ: ttFloat; text: '5'; ),
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '15'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '25'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '35'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));

  cSFIValuesDef: Array[cSFIReference..cSFINegLimit] of TValueSpecRec = (
    // Reference
   (MinValue: cSFIRefMin; MaxValue: cSFIRefMax;
    Space: 0; Factor: 1; Format: '%2.1f'; Measure: '';
    Caption: rsSFIReferenc;
    Hint: rsSFIReferencHint;
    XPValue: cXPReferenceItem;
    XPSwitch: cXPYMSettingSFISwitchItem),
    // +Grenze
   (MinValue: cSFIDiffUpMin; MaxValue: cSFIDiffUpMax;
    Space: cGEBSpace; Factor: 1; Format: '%2.0f'; Measure: rsMeasureProcent;
    Caption: rsSFIPosLimit;
    Hint: rsSFIPosLimitHint;
    XPValue: cXPSFIUpperItem;
    XPSwitch: cXPYMSettingSFISwitchItem),
    // -Grenze
   (MinValue: cSFIDiffLwMin; MaxValue: cSFIDiffLwMax;
    Space: 0; Factor: 1; Format: '%2.0f'; Measure: rsMeasureProcent;
    Caption: rsSFINegLimit;
    Hint: rsSFINegLimitHint;
    XPValue: cXPSFILowerItem;
    XPSwitch: cXPYMSettingSFISwitchItem)
//    // SFI Repetition
//   (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
//    Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: rsRepetition;
//    Hint: rsSFIRepetitionHint;
//    XPValue: cXPYMSettingSFIRepetitionItem;
//    XPSwitch: cXPYMSettingSFISwitchItem)
  );

//------------------------------------------------------------------------------
// VCV Values definitions
//------------------------------------------------------------------------------
  cVCVPosLimit   = 0;
  cVCVNegLimit   = 1;
  cVCVLength     = 2;

  cPTVCVLimit: array[0..14] of TPresetTextRec = (
    (typ: ttFloat; text: '5'; ),
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '15'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '25'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '35'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttFloat; text: '60'; ),
    (typ: ttFloat; text: '70'; ),
    (typ: ttFloat; text: '80'; ),
    (typ: ttFloat; text: '90'; ),
    (typ: ttFloat; text: '100'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));

  //Werte angelehnt an Implementation auf AC5 Nue:29.4.08 (Da sonst nichts definiert war!!!!!)
  cPTVCVLength: array[0..9] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '3'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));

  cVCVValuesDef: Array[cVCVPosLimit..cVCVLength] of TValueSpecRec = (
    // +Grenze
   (MinValue: cVCVDiffUpMin; MaxValue: cVCVDiffUpMax;
    Space: cGEBSpace; Factor: 1; Format: '%3.0f'; Measure: rsMeasureProcent;
    Caption: rsVCVPosLimit;
    Hint: rsVCVPosLimitHint;
    XPValue: cXPVCVUpperItem;
    XPSwitch: cXPYMSettingVCVSwitchItem),
    // -Grenze
   (MinValue: cVCVDiffLwMin; MaxValue: cVCVDiffLwMax;
    Space: 0; Factor: 1; Format: '%3.0f'; Measure: rsMeasureProcent;
    Caption: rsVCVNegLimit;
    Hint: rsVCVNegLimitHint;
    XPValue: cXPVCVLowerItem;
    XPSwitch: cXPYMSettingVCVSwitchItem),
    // Length
   (MinValue: cVCVObsLengthMin; MaxValue: cVCVObsLengthMax;
    Space: 0; Factor: 1; Format: '%2.0f'; Measure: rsMeasureM;
    Caption: rsObsLength;
    Hint: rsYCLengthHint;
    XPValue: cXPYMSettingVCVLengthItem;
    XPSwitch: cXPYMSettingVCVSwitchItem)
  );

//------------------------------------------------------------------------------
// IPI Values definitions
//------------------------------------------------------------------------------
  cIPINeps       = 0;
  cIPISmallPlus  = 1;
  cIPISmallMinus = 2;
  cIPIThick      = 3;
  cIPIThin       = 4;

  cPTIPINeps: array[0..7] of TPresetTextRec = (
    (typ: ttFloat; text: '0.0'; ),
    (typ: ttFloat; text: '1.0'; ),
    (typ: ttFloat; text: '2.0'; ),
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '5.0'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '7.0'; ));

  cPTIPIOthers: array[0..7] of TPresetTextRec = (
    (typ: ttFloat; text: '0.0'; ),
    (typ: ttFloat; text: '0.25'; ),
    (typ: ttFloat; text: '0.50'; ),
    (typ: ttFloat; text: '0.75'; ),
    (typ: ttFloat; text: '1.0'; ),
    (typ: ttFloat; text: '1.25'; ),
    (typ: ttFloat; text: '1.50'; ),
    (typ: ttFloat; text: '1.75'; ));

  //TODO: ResourceStrings f�r Hints noch definieren wenn Beschreibung klar ist
  cIPIValuesDef: Array[cIPINeps..cIPIThin] of TValueSpecRec = (
    // IPINeps
   (MinValue: 0; MaxValue: 7;
    Space: 0; Factor: 1; Format: '%1.3f'; Measure: '';
    Caption: rsIPINeps;
    Hint: '';
    XPValue: cXPNepsItem;
    XPSwitch: ''),
    // IPISmallPlus
   (MinValue: 0; MaxValue: 1.75;
    Space: 0; Factor: 1; Format: '%1.2f'; Measure: '';
    Caption: rsIPISmallPlus;
    Hint: '';
    XPValue: cXPSmallPlusItem;
    XPSwitch: ''),
    // IPISmallMinus
   (MinValue: 0; MaxValue: 1.75;
    Space: 0; Factor: 1; Format: '%1.2f'; Measure: '';
    Caption: rsIPISmallMinus;
    Hint: '';
    XPValue: cXPSmallMinusItem;
    XPSwitch: ''),
    // IPIThick
   (MinValue: 0; MaxValue: 1.75;
    Space: 0; Factor: 1; Format: '%1.2f'; Measure: '';
    Caption: rsIPIThick;
    Hint: '';
    XPValue: cXPThickItem;
    XPSwitch: ''),
    // IPIThin
   (MinValue: 0; MaxValue: 1.75;
    Space: 0; Factor: 1; Format: '%1.2f'; Measure: '';
    Caption: rsIPIThin;
    Hint: '';
    XPValue: cXPThinItem;
    XPSwitch: '')
  );

//------------------------------------------------------------------------------
// FF Cluster Values definitions
//------------------------------------------------------------------------------
//  cFStartupCut        = 0;

  cFCDarkLength       = 0;
  cFCDarkFaults       = 1;
//  cFCDarkRepetition   = 3;
//  cFCBrightLength     = 4;
//  cFCBrightFaults     = 5;
//  cFCBrightRepetition = 6;

  // F�r Bright-Cluster werden die gleichen Settings wie f�r Dark verwendet -> ergo nur 1 Parameter f�r beide
  cFClusterValuesDef: Array[cFCDarkLength..cFCDarkFaults] of TValueSpecRec = (
//    // F Startup Cut
//   (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
//    Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: rsFStartupRepCaption;
//    Hint: rsFStartupRepHint;
//    XPValue: cXPFStartupRepetitionItem;
//    XPSwitch: ''),
    // Dark Length
   (MinValue: cClObsLengthMin; MaxValue: cClObsLengthMax;
    Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureM;
    Caption: rsObsLength;
    Hint: rsFCObsLengthHint;
    XPValue: cXPDarkClusterObsLengthItem;
    XPSwitch: cXPDarkClusterSwitchItem),
    // Dark Faults
   (MinValue: cDefectsMin; MaxValue: cDefectsMax;
    Space: 0; Factor: 1; Format: '%4.0f'; Measure: '';
    Caption: rsFaults;
    Hint: rsFFCFaultsHint;
    XPValue: cXPDarkClusterDefectsItem;
    XPSwitch: '')  //cXPDarkClusterSwitchItem)
//    // Dark Repetition
//   (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
//    Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: rsRepetition;
//    Hint: rsFRepetitionHint;
//    XPValue: cXPDarkClusterRepetitionItem;
//    XPSwitch: cXPDarkClusterSwitchItem)
{
    // Bright Length
   (MinValue: cClObsLengthMin; MaxValue: cClObsLengthMax;
    Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureM;
    Caption: '';
    Hint: rsFCObsLengthHint;
    XPValue: cXPBrightClusterObsLengthItem;
    XPSwitch: cXPBrightClusterSwitchItem),
    // Bright Faults
   (MinValue: cDefectsMin; MaxValue: cDefectsMax;
    Space: 0; Factor: 1; Format: '%4.0f'; Measure: '';
    Caption: '';
    Hint: rsFFCFaultsHints;
    XPValue: cXPBrightClusterDefectsItem;
    XPSwitch: cXPBrightClusterSwitchItem),
    // Bright Repetition
   (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
    Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
    Caption: '';
    Hint: rsFRepetitionHint;
    XPValue: cXPBrightClusterRepetitionItem;
    XPSwitch: cXPBrightClusterSwitchItem)
{}
  );

//------------------------------------------------------------------------------
// P Values definitions
//------------------------------------------------------------------------------
  cPTPLimit: array[0..19] of TPresetTextRec = (
    (typ: ttFloat; text: '0.5'),
    (typ: ttFloat; text: '1.0'),
    (typ: ttFloat; text: '1.5'),
    (typ: ttFloat; text: '2.0'),
    (typ: ttFloat; text: '2.5'),
    (typ: ttFloat; text: '3.0'),
    (typ: ttFloat; text: '3.5'),
    (typ: ttFloat; text: '4.0'),
    (typ: ttFloat; text: '4.5'),
    (typ: ttFloat; text: '5.0'),
    (typ: ttFloat; text: '5.5'),
    (typ: ttFloat; text: '6.0'),
    (typ: ttFloat; text: '6.5'),
    (typ: ttFloat; text: '7.0'),
    (typ: ttFloat; text: '7.5'),
    (typ: ttFloat; text: '8.0'),
    (typ: ttFloat; text: '8.5'),
    (typ: ttFloat; text: '9.0'),
    (typ: ttFloat; text: '9.5'),
    (typ: ttCmdDisabled; text: rsSwOff));

  cPTPRefLength: array[0..7] of TPresetTextRec = (
    (typ: ttFloat; text: '0.5'),
    (typ: ttFloat; text: '1.0'),
    (typ: ttFloat; text: '1.5'),
    (typ: ttFloat; text: '2.0'),
    (typ: ttFloat; text: '2.5'),
    (typ: ttFloat; text: '3.0'),
    (typ: ttFloat; text: '3.5'),
    (typ: ttFloat; text: '4.0'));

  cPTPercent: array[0..9] of TPresetTextRec = (
    (typ: ttFloat; text: '10'),
    (typ: ttFloat; text: '20'),
    (typ: ttFloat; text: '30'),
    (typ: ttFloat; text: '40'),
    (typ: ttFloat; text: '50'),
    (typ: ttFloat; text: '60'),
    (typ: ttFloat; text: '70'),
    (typ: ttFloat; text: '80'),
    (typ: ttFloat; text: '90'),
    (typ: ttFloat; text: '100'));

  cPTLowBit: array[0..4] of TPresetTextRec = (
    (typ: ttFloat; text: '0'),
    (typ: ttFloat; text: '1'),
    (typ: ttFloat; text: '2'),
    (typ: ttFloat; text: '3'),
    (typ: ttFloat; text: '4'));

  cPLimit             = 0;
  cPRefLength         = 1;
//  cPStartupRepetition = 2;

  cPValuesDef: Array[cPLimit..cPRefLength] of TValueSpecRec = (
    // Limit
   (MinValue: cPLimitMin; MaxValue: cPLimitMax;
    Space: 0; Factor: 1; Format: '%4.1f'; Measure: '';
    Caption: rsPLimitCaption;
    Hint: rsPLimitHint;
    XPValue: cXPLimitItem;
    XPSwitch: cXPForeignPSwitchItem),
    // RefLength
   (MinValue: cPRefLengthMin; MaxValue: cPRefLengthMax;
    Space: 0; Factor: 1; Format: '%4.1f'; Measure: rsMeasureCm;
    Caption: rsPRefLengthCaption;
    Hint: rsPRefLengthHint;
    XPValue: cXPRefLengthItem;
    XPSwitch: cXPForeignPSwitchItem)
  );

  cPExtLowLimit  = 0;
  cPExtLowBit    = 1;
  cPExtReduction = 2;

  cPExtValuesDef: Array[cPExtLowLimit..cPExtReduction] of TValueSpecRec = (
    // Low Limit
   (MinValue: cPLowLimitMin; MaxValue: cPLowLimitMax;
    Space: 0; Factor: 1; Format: '%4.0f'; Measure: rsMeasureProcent;
    Caption: rsPLowLimitCaption;
    Hint: rsPLowLimitHint;
    XPValue: cXPLimitLowFactorItem;
    XPSwitch: ''), //cXPStartupRepetitionSwitchItem),
    // Low Bit
   (MinValue: cPLowBitMin; MaxValue: cPLowBitMax;
    Space: 0; Factor: 1; Format: '%4.1f'; Measure: '';
    Caption: rsPLowBitCaption;
    Hint: rsPLowBitHint;
    XPValue: cXPBitLowFactorItem;
    XPSwitch: ''), //cXPStartupRepetitionSwitchItem),
    // Reduction
   (MinValue: cPReductionMin; MaxValue: cPRecuctionMax;
    Space: 0; Factor: 1; Format: '%4.0f'; Measure: rsMeasureProcent;
    Caption: rsPReductionCaption;
    Hint: rsPReductionHint;
    XPValue: cXPPReductionItem;
    XPSwitch: '') //cXPStartupRepetitionSwitchItem)
  );

//------------------------------------------------------------------------------
// Repetition Values definitions
//------------------------------------------------------------------------------
  cRepNSLT        = 0;
  cRepSCnt        = 1;
  cRepYCnt        = 2;
  cRepSFI         = 3;
  cRepVCV         = 4;
  cRepFStartup    = 5;
  cRepPStartup    = 6;
  cRepClS         = 7;
  cRepClL         = 8;
  cRepClT         = 9;
  cRepFCluster    = 10;

  // F�r Bright-Cluster werden die gleichen Settings wie f�r Dark verwendet -> ergo nur 1 Parameter f�r beide
  cRepetitionValuesDef: Array[cRepNSLT..cRepFCluster] of TValueSpecRec = (
     // NSLT Repetition (only Zenit)
    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: 'NSLT';
     Hint: rsNSLTStartupRepHint;
     XPValue: cXPNSLTStartupRepetitionItem;
     XPSwitch: cXPChannelNSLTSwitchItem),
     // Short YarnCount Repetition
    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: rsShortCountTitle;
     Hint: rsYarnCountRepHint;
     XPValue: cXPYarnShortCountRepetitionItem;
     XPSwitch: ''),
     // Normal YarnCount Repetition
    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: rsNormalCountTitle;
     Hint: rsYarnCountRepHint;
     XPValue: cXPYarnCountRepetitionItem;
     XPSwitch: ''),
    // SFI Repetition
   (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: 'SFI';
     Hint: rsSFIRepetitionHint;
     XPValue: cXPYMSettingSFIRepetitionItem;
     XPSwitch: cXPYMSettingSFISwitchItem),
    // VCV Repetition
   (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: cGEBSpace; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: 'VCV';
     Hint: rsVCVRepetitionHint;
     XPValue: cXPYMSettingVCVRepetitionItem;
     XPSwitch: cXPYMSettingVCVSwitchItem),
    // F Startup Cut
    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: 'F'; //rsFStartupRepCaption;
     Hint: rsFStartupRepHint;
     XPValue: cXPFStartupRepetitionItem;
     XPSwitch: ''),
    // P Startup Repetition
    (MinValue: cMinRepetitions; MaxValue: cMaxSynRepetitions;
     Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: 'P'; //rsRepetition;
     Hint: rsPStartupRepHint;
     XPValue: cXPPStartupRepetitionItem;
     XPSwitch: cXPPStartupRepetitionSwitchItem),
    // Short Cluster Repetitions
    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: rsShortTitle;
     Hint: rsFCRepetitionHint;
     XPValue: cXPClusterShortRepetitionItem;
     XPSwitch: ''),
    // Long Cluster Repetitions
    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: rsLongTitle;
     Hint: rsFCRepetitionHint;
     XPValue: cXPClusterLongRepetitionItem;
     XPSwitch: ''),
    // Thin Cluster Repetitions
    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: rsThinTitle;
     Hint: rsFCRepetitionHint;
     XPValue: cXPClusterThinRepetitionItem;
     XPSwitch: ''),
    // F Dark Cluster Repetition
    (MinValue: cMinRepetitions; MaxValue: cMaxRepetitions;
     Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
     Caption: rsFClusterCaption; // ivlm
     Hint: rsFRepetitionHint;
     XPValue: cXPDarkClusterRepetitionItem;
     XPSwitch: cXPDarkClusterSwitchItem)
  );


//------------------------------------------------------------------------------
// HeadClass Values definitions
//------------------------------------------------------------------------------
  cHeadClass = 0;

  // F�r Bright-Cluster werden die gleichen Settings wie f�r Dark verwendet -> ergo nur 1 Parameter f�r beide
  cHeadClassValuesDef: Array[cHeadClass..cHeadClass] of TValueSpecRec = (
    // Limit
   (MinValue: 0; MaxValue: 0;
    Space: 0; Factor: 1; Format: ''; Measure: '';
    Caption: rsHeadClassTitle;
    Hint: '';
    XPValue: '';
    XPSwitch: ''));
//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------
type
  TSynchronizeFFMatrix = procedure of object;
  TChannelEditBox = class(TGroupEditBox)
  private
    procedure CheckComponents;
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure BuildBox; override;
  end;

  //...........................................................................

  TSpliceEditBox = class(TGroupEditBox)
  private
    procedure CheckComponents;
    function GetUpperYarn: Boolean;
    procedure SetUpperYarn(const aValue: Boolean);
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
    property UpperYarn: Boolean read GetUpperYarn write SetUpperYarn;
  published
  end;
  
  //...........................................................................

  TFaultClusterEditBox = class(TGroupEditBox)
  private
    mHasExtCluster: Boolean;
    mShortLabel: TmmLabel;
    mLongLabel: TmmLabel;
    mThinLabel: TmmLabel;
    procedure CheckComponents;
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure SetEnabled(Value: Boolean); override;
    procedure SetMeasureWidth(const Value: Integer); override;
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
  end;
  
  //...........................................................................

  TYarnCountEditBox = class(TGroupEditBox)
  private
    FYarnCount: Extended;
    fYarnUnit: TYarnUnit;
    mHasShortCount: Boolean;
    mNormalCountLabel: TmmLabel;
    mShortCountLabel: TmmLabel;
    mTemplateMode: Boolean;
    procedure CalcCoarseFineYarnCount(aDiffIndex, aCountIndex: Integer);
    procedure CheckComponents;
    procedure SetYarnCount(const aValue: Extended);
    procedure SetYarnUnit(const aValue: TYarnUnit);
    procedure ValidatePara(var aTag: Integer);
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
    property YarnCount: Extended read FYarnCount write SetYarnCount;
    property YarnUnit: TYarnUnit read fYarnUnit write SetYarnUnit;
  published
  end;
  
  //...........................................................................

  TSFIEditBox = class(TGroupEditBox)
  private
    procedure CheckComponents;
    procedure ValidatePara(aTag: Integer);
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
  end;
  
  //...........................................................................

  TFFClusterEditBox = class(TGroupEditBox)
  private
    fSynchronizeFFMatrix: TSynchronizeFFMatrix;
    procedure CheckComponents;
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
  published
    property SynchronizeFFMatrix: TSynchronizeFFMatrix read fSynchronizeFFMatrix write fSynchronizeFFMatrix;
  end;

  TPEditBox = class(TGroupEditBox)
  private
    procedure CheckComponents;
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
  published
  end;

  THeadClassEditBox = class(TmmPanel)
  private
    fModel: TVCLXMLSettingsModel;
    mHeadClassTitle: TmmLabel;
    mHeadClassValue: TmmLabel;
    procedure BuildBox; virtual;
    procedure CheckComponents;
    procedure SetModel(const aValue: TVCLXMLSettingsModel);
  protected
    mController: TBaseGUIController;
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
  published
    property Model: TVCLXMLSettingsModel read fModel write SetModel;
  end;

  //...........................................................................

  TRepetitionEditBox = class(TGroupEditBox)
  private
    mClusterLabel: TmmLabel;
    mYarnCountLabel: TmmLabel;
    mStartupCutLabel: TmmLabel;
    mHasExtCluster: Boolean;
    mHasShortCount: Boolean;
    procedure CheckComponents;
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
  end;

  TPExtEditBox = class(TGroupEditBox)
  private
    procedure CheckComponents;
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
  published
  end;

type
  //...........................................................................

  TIPIEditBox = class(TGroupEditBox)
  private
    procedure CheckComponents;
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
  end;

type
  //...........................................................................

  TVCVEditBox = class(TGroupEditBox)
  private
    procedure CheckComponents;
    procedure ValidatePara(aTag: Integer);
  protected
    procedure DoConfirm(aTag: Integer); override;
    procedure GetCreateController(var aController: TBaseGUIController); override;
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    procedure BuildBox; override;
  end;

  //...........................................................................

//------------------------------------------------------------------------------
// Functions and procedures
//------------------------------------------------------------------------------
//function ParaSwitchToTextType(aSwitch: Word): TTextType;
//function TextTypToParaSwitch(aTextType: TTextType): Word;

implementation
uses
  mmMBCS, mmCS, Controls, StdCtrls, BaseGlobal;

//******************************************************************************
// Functions and procedures
//******************************************************************************
function ParaSwitchToTextType(aSwitch: TSwitch): TTextType;
begin
  if aSwitch = swOn then Result := ttFloat
                    else Result := ttCmdDisabled;
end;
//------------------------------------------------------------------------------
function TextTypToParaSwitch(aTextType: TTextType): TSwitch;
begin
  if aTextType = ttCmdDisabled then Result := swOff
                               else Result := swOn;
end;
//------------------------------------------------------------------------------
function FitValue(aValue, aMin, aMax: Extended): Extended;
begin
  Result := aValue;
  if (aMax > aMin) then begin
    if aValue < aMin then
      Result := aMin
    else if aValue > aMax then
      Result := aMax;
  end;
end;
//------------------------------------------------------------------------------


//:---------------------------------------------------------------------------
//:--- Class: TChannelEditBox
//:---------------------------------------------------------------------------
constructor TChannelEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  //:...................................................................
  CaptionWidth := 25;
  //  ValueWidth := 38;  // textsize 3, (4)
  ValueWidth   := 31; // textsize 3, (3)
  MeasureWidth := 16;
  Caption      := rsCHCaption;
  
  BuildBox;
end;

//:---------------------------------------------------------------------------
destructor TChannelEditBox.Destroy;
begin
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TChannelEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cChannelValuesDef) then
    SetLength(mComponentArr, Length(cChannelValuesDef));

  inherited BuildBox;

  AddField(cChN,    cChannelValuesDef[cChN],    cPTCHNepDiameter,    Printable);
  AddField(cChDS,   cChannelValuesDef[cChDS],   cPTCHShortDiameter,  Printable);
  AddField(cChLS,   cChannelValuesDef[cChLS],   cPTCHShortLength,    Printable);
  AddField(cChDL,   cChannelValuesDef[cChDL],   cPTCHLongDiameter,   Printable);
  AddField(cChLL,   cChannelValuesDef[cChLL],   cPTCHLongThinLength, Printable);
  AddField(cChDT,   cChannelValuesDef[cChDT],   cPTCHThinDiameter,   Printable);
  AddField(cChLT,   cChannelValuesDef[cChLT],   cPTCHLongThinLength, Printable);
end;

procedure TChannelEditBox.CheckComponents;
begin
  Enabled := True;
//  EnabledField[cChNSLT] := mController.Model.FPHandler.Value[cXPFP_ZenitItem];
end;

//:---------------------------------------------------------------------------
procedure TChannelEditBox.DoConfirm(aTag: Integer);
var
  xDia, xLen: Extended;
begin
  { F�r den Thin-Parameter wird folgendes noch gepr�ft:
    Thin length > 5cm: -6 ...-60,
    Thin length <= 5cm: -20 ...-60

    Wird der Diameter ver�ndert so wird die L�nge gepr�ft, wird die L�nge editiert
    so wird der Diameter angepasst.
  {}
  case aTag of
    cChDT: begin
          xDia := 1.0 - Values[cChDT];
          xLen := Values[cChLT];
          if xDia >= cThinDiaMaxBelow then
            xLen := FitValue(xLen, cThinLenThreshold, cThinLenMax)
          else
            xLen := FitValue(xLen, cThinLenMin, cThinLenMax);

          XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);
          XPValue[cChLT] := xLen;
          mController.Change(mComponentArr[aTag].XPValue, xDia);
      end;
    cChLT: begin
          xDia := 1.0 - Values[cChDT];
          xLen := Values[cChLT];
          if xLen >= cThinLenThreshold then
            xDia := FitValue(xDia, cThinDiaMin, cThinDiaMaxAbove)
          else
            xDia := FitValue(xDia, cThinDiaMin, cThinDiaMaxBelow);

          XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);
          XPValue[cChDT] := xDia;
          mController.Change(mComponentArr[aTag].XPValue, xLen);
      end;
  else
    XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);
    mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);
  end; // with

  inherited;
end;

//:---------------------------------------------------------------------------
procedure TChannelEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TChannelController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
  aController.ListenToKindSet     := [ckChannel, ckCluster];
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TChannelEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TChannelEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
var
  i: Integer;
begin
  CheckComponents;
  for i:=0 to High(mComponentArr) do begin
    if Assigned(mComponentArr[i].Value) then begin
      if i = cChDT then Values[i] := (1.0 - XPValue[i])
                   else Values[i] := XPValue[i];
      TextType[i] := ParaSwitchToTextType(XPSwitch[i]);
    end; // if
  end; // for
end;


//:---------------------------------------------------------------------------
//:--- Class: TFaultClusterEditBox
//:---------------------------------------------------------------------------
constructor TFaultClusterEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  ValueWidth   := 35; // textsize 3, (3)
  MeasureWidth := 16;
  Caption      := rsClusterCaption;

  mShortLabel  := Nil;
  mLongLabel   := Nil;
  mThinLabel   := Nil;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TFaultClusterEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cClusterValuesDef) then
    SetLength(mComponentArr, Length(cClusterValuesDef));

  inherited BuildBox;

  // Short Cluster
  CaptionWidth := 75;
  mLeftPos     := cGEBBoarder;
  mTopPos      := cGEBTopBoarder;
  AddTitle(mShortLabel, rsShortTitle, taRightJustify, False);
  AddField(cClSDiameter,   cClusterValuesDef[cClSDiameter],   cPTCHShortDiameter, Printable);
  AddField(cClSLength,     cClusterValuesDef[cClSLength],     cPTCHShortLength, Printable);
  AddField(cClSObsLength,  cClusterValuesDef[cClSObsLength],  cPTClObsLength, Printable);
  AddField(cClSFaults,     cClusterValuesDef[cClSFaults],     cPTClFaults, Printable);

  // Long Cluster
  CaptionWidth := 0;
  mLeftPos     := Width;
  mTopPos      := cGEBTopBoarder;
  AddTitle(mLongLabel, rsLongTitle, taRightJustify, False);
  AddField(cClLDiameter,   cClusterValuesDef[cClLDiameter],   cPTCHLongDiameter, Printable);
  AddField(cClLLength,     cClusterValuesDef[cClLLength],     cPTCHLongThinLength, Printable);
  AddField(cClLObsLength,  cClusterValuesDef[cClLObsLength],  cPTClObsLength, Printable);
  AddField(cClLFaults,     cClusterValuesDef[cClLFaults],     cPTClFaults, Printable);

  // Thin Cluster
  CaptionWidth := 0;
  mLeftPos     := Width;
  mTopPos      := cGEBTopBoarder;
  AddTitle(mThinLabel, rsThinTitle, taRightJustify, False);
  // F�r das Minuszeichen der Thin Dia Editbox muss die mLeftPos noch angepasst werden
  CaptionWidth := 6;
  dec(mLeftPos, 6);
  AddField(cClTDiameter,   cClusterValuesDef[cClTDiameter],   cPTCHThinDiameter, Printable);
  // Danach wieder normales verhalten
  CaptionWidth := 0;
  inc(mLeftPos, 6);
  AddField(cClTLength,     cClusterValuesDef[cClTLength],     cPTCHLongThinLength, Printable);
  AddField(cClTObsLength,  cClusterValuesDef[cClTObsLength],  cPTClObsLength, Printable);
  AddField(cClTFaults,     cClusterValuesDef[cClTFaults],     cPTClFaults, Printable);
end;

procedure TFaultClusterEditBox.CheckComponents;
  //...........................................................
  procedure SetEnabledField(aBaseIndex: Integer; aEnabled: Boolean);
  begin
    EnabledField[aBaseIndex + cClSDiameter]   := aEnabled;
    EnabledField[aBaseIndex + cClSLength]     := aEnabled;
    EnabledField[aBaseIndex + cClSObsLength]  := aEnabled;
    EnabledField[aBaseIndex + cClSFaults]     := aEnabled;
//    EnabledField[aBaseIndex + cClSRepetition] := aEnabled;
  end;
  //...........................................................
begin
  // Normaler Cluster verf�gbahr?
  Enabled := Boolean(mController.Model.FPHandler.Value[cXPFP_ClusterItem]);
  if Enabled then begin
    mHasExtCluster := Boolean(mController.Model.FPHandler.Value[cXPFP_ExtendedClusterItem]);
    // wenn kein Extended Cluster verf�gbar => Spectra -> L�nge f�r Short Cluster von Channel verwenden
    if mHasExtCluster then mComponentArr[cClSLength].XPValue := cXPClusterShortLengthItem
                      else mComponentArr[cClSLength].XPValue := cXPChannelShortLengthItem;
    EnabledField[cClSLength] := mHasExtCluster;
    // Extended Cluster verf�gbahr? (SLT-Cluster)
    SetEnabledField(cClLDiameter, mHasExtCluster);
    SetEnabledField(cClTDiameter, mHasExtCluster);
  end else
    mHasExtCluster := False;

  mShortLabel.Enabled := Enabled;
  mLongLabel.Enabled  := mHasExtCluster;
  mThinLabel.Enabled  := mHasExtCluster;
end;

//:---------------------------------------------------------------------------
procedure TFaultClusterEditBox.DoConfirm(aTag: Integer);
var
  xDia: Extended;
  xLen: Extended;
  xParaOffset: Integer;
begin
  xParaOffset := (aTag mod cClLDiameter); // Gibt immer einen Wert 0-4 zur�ck f�r Dia, Len, ObsLen, Faults, Reps
  // ObsLen und Faults auf Switch pr�fen, f�r die anderen Werte gibt es keinen Off Zustand und die Werte werden immer angezeigt
  if (xParaOffset = cClSObsLength) or (xParaOffset = CClSFaults) then
    XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);

  // Dann die Werte
  case aTag of
    cClTDiameter: begin
          xDia := 1.0 - Values[cClTDiameter];
          xLen := Values[cClTLength];
          if xDia >= cThinDiaMaxBelow then
            xLen := FitValue(xLen, cThinLenThreshold, cThinLenMax)
          else
            xLen := FitValue(xLen, cThinLenMin, cThinLenMax);

          XPValue[cClTLength] := xLen;
          mController.Change(mComponentArr[aTag].XPValue, xDia);
      end;
    cClTLength: begin
          xDia := 1.0 - Values[cClTDiameter];
          xLen := Values[cClTLength];
          if xLen >= cThinLenThreshold then
            xDia := FitValue(xDia, cThinDiaMin, cThinDiaMaxAbove)
          else
            xDia := FitValue(xDia, cThinDiaMin, cThinDiaMaxBelow);

          XPValue[cClTDiameter] := xDia;
          mController.Change(mComponentArr[aTag].XPValue, xLen);
      end;
  else
    mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);
  end; // with

  inherited;
end;

//:---------------------------------------------------------------------------
procedure TFaultClusterEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TClusterController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
  aController.ListenToKindSet     := [ckChannel, ckCluster];
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TFaultClusterEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TFaultClusterEditBox.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
end;

procedure TFaultClusterEditBox.SetMeasureWidth(const Value: Integer);
begin
  inherited SetMeasureWidth(Value);
end;

//:---------------------------------------------------------------------------
procedure TFaultClusterEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
  //.............................................................
//  procedure SetTextTypes(aOffset: Integer; aTextType: TTextType);
//  begin
//    TextType[aOffset+0] := aTextType;  // Dia
//    TextType[aOffset+1] := aTextType;  // Len
//    TextType[aOffset+2] := aTextType;  // ObsLen
//    TextType[aOffset+3] := aTextType;  // Faults
//    TextType[aOffset+4] := aTextType;  // Repetitions
//  end;
  //.............................................................
  procedure SetValues(aIndex: Integer);
  begin
    TextType[aIndex] := ttFloat;
    if aIndex = cClTDiameter then Values[aIndex] := (1.0 - XPValue[aIndex])
                             else Values[aIndex] := XPValue[aIndex];
  end;
  //.............................................................
  procedure UpdateValues(aBaseIndex: Integer; aXPSwitchForChannel: String);
  begin
    // Zustand wird �ber die Beobachtungsl�nge definiert
    if XPSwitch[aBaseIndex + cClSObsLength] = swOff then begin
      TextType[aBaseIndex+cClSObsLength] := ttCmdDisabled;  // ObsLen
      TextType[aBaseIndex+CClSFaults]    := ttCmdDisabled;  // Faults
    end
    else begin
      SetValues(aBaseIndex + cClSObsLength);
      SetValues(aBaseIndex + cClSFaults);
    end;

    // Wenn der Kurzkanal ausgeschaltet ist, dann nur den ClusterDia ausschalten -> Rest ist eingeschaltet
//    if GetSwitchValue(mController.Value[aXPSwitchForChannel]) = swOn then SetValues(aBaseIndex + cClSDiameter)
    if mController.SwitchValueDef[aXPSwitchForChannel, swOff] = swOn then SetValues(aBaseIndex + cClSDiameter)
                                                                     else TextType[aBaseIndex + cClSDiameter] := ttCmdDisabled;
    SetValues(aBaseIndex + cClSLength);
//    SetValues(aBaseIndex + cClSRepetition);
  end;
  //.............................................................
begin
  // Im Berichtsmodus hat es keine g�ltige MaConfig XML Definition. Extended Cluster aus normalen Settings auslesen
  CheckComponents;
  if Enabled then begin
    // Short Cluster
    UpdateValues(cClSDiameter, cXPChannelShortSwitchItem);
    if mHasExtCluster then begin
      // Long Cluster
      UpdateValues(cClLDiameter, cXPChannelLongSwitchItem);
      // Thin Cluster
      UpdateValues(cClTDiameter, cXPChannelThinSwitchItem);
    end;
  end else
    ClearValues;
end;


//:---------------------------------------------------------------------------
//:--- Class: TYarnCountEditBox
//:---------------------------------------------------------------------------
constructor TYarnCountEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fYarnCount     := 0; // Init YarnCount
  fYarnUnit      := yuNm;

  ValueWidth     := 44; // textsize 3, (3)
  MeasureWidth   := 16;
  Caption        := rsYCCaption;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TYarnCountEditBox.BuildBox;
var
  xMeasureWidth: Integer;
begin
  if Length(mComponentArr) <> Length(cYarnCountValuesDef) then
    SetLength(mComponentArr, Length(cYarnCountValuesDef));

  inherited BuildBox;

  // Normal Count
  CaptionWidth := 75;
  mLeftPos     := cGEBBoarder;
  mTopPos      := cGEBTopBoarder;
  AddTitle(mNormalCountLabel, rsNormalCountTitle, taRightJustify, False);
  AddField(cYCPDiaDiff,   cYarnCountValuesDef[cYCPDiaDiff],   cPTYCDiaDiff, Printable);
  AddField(cYCNDiaDiff,   cYarnCountValuesDef[cYCNDiaDiff],   cPTYCDiaDiff, Printable);
  xMeasureWidth := MeasureWidth; //Nue:21.3.06
  MeasureWidth := 40;
  AddField(cYCCount,      cYarnCountValuesDef[cYCCount],      cPTYCCount, True);
  MeasureWidth := xMeasureWidth;
  AddField(cYCCorse,      cYarnCountValuesDef[cYCCorse],      cPTYCCount, Printable);
  AddField(cYCFine,       cYarnCountValuesDef[cYCFine],       cPTYCCount, Printable);
  AddField(cYCLength,     cYarnCountValuesDef[cYCLength],     cPTYCLength, Printable);

  // Short Count
  CaptionWidth := 0;
  mLeftPos     := Width;
  mTopPos      := cGEBTopBoarder;
  AddTitle(mShortCountLabel, rsShortCountTitle, taRightJustify, False);
  AddField(cSCPDiaDiff,   cYarnCountValuesDef[cSCPDiaDiff],   cPTYCDiaDiff, Printable);
  AddField(cSCNDiaDiff,   cYarnCountValuesDef[cSCNDiaDiff],   cPTYCDiaDiff, Printable);
  inc(mTopPos, 17 + 4);
  AddField(cSCCorse,      cYarnCountValuesDef[cSCCorse],      cPTYCCount, Printable);
  AddField(cSCFine,       cYarnCountValuesDef[cSCFine],       cPTYCCount, Printable);
  AddField(cSCLength,     cYarnCountValuesDef[cSCLength],     cPTSCLength, Printable);

  // Sodele, nach einem Printable Wechsel nochmals die Garnunit setzen -> Label/Value wird angepasst
  YarnCount := fYarnCount;
end;
//:-----------------------------------------------------------------------------
procedure TYarnCountEditBox.CalcCoarseFineYarnCount(aDiffIndex, aCountIndex: Integer);
var
  xYarnUnitByLength: Boolean;
begin
  xYarnUnitByLength := YarnUnit in cYarnUnitsByLength;
  Values[aCountIndex+0] := CalculateCountFromDiaDiff(100 + Values[aDiffIndex+0], fYarnCount, xYarnUnitByLength);
  Values[aCountIndex+1] := CalculateCountFromDiaDiff(100 - Values[aDiffIndex+1], fYarnCount, xYarnUnitByLength);
end;
//:-----------------------------------------------------------------------------
procedure TYarnCountEditBox.CheckComponents;
begin
  // ShortCount verf�gbar?
  mHasShortCount := false;
  // lok 12.8.2005: Abfragen ob das Modell zugeordnet ist
  if assigned(mController.Model) then
    mHasShortCount := Boolean(mController.Model.FPHandler.Value[cXPFP_ShortCountItem]);
  mShortCountLabel.Enabled    := mHasShortCount;
  EnabledField[cSCPDiaDiff]   := mHasShortCount;
  EnabledField[cSCNDiaDiff]   := mHasShortCount;
  EnabledField[cSCCorse]      := mHasShortCount and not mTemplateMode;
  EnabledField[cSCFine]       := mHasShortCount and not mTemplateMode;
  EnabledField[cSCLength]     := mHasShortCount;
//  EnabledField[cSCRepetition] := mHasShortCount;
  if not mHasShortCount then begin
    TextType[cSCPDiaDiff]   := ttCmdDisabled;
    TextType[cSCNDiaDiff]   := ttCmdDisabled;
    TextType[cSCCorse]      := ttEmpty;
    TextType[cSCFine]       := ttEmpty;
    TextType[cSCLength]     := ttEmpty;
//    TextType[cSCRepetition] := ttEmpty;
  end;
end;

//:---------------------------------------------------------------------------
procedure TYarnCountEditBox.DoConfirm(aTag: Integer);
begin
  if fYarnCount > 0 then
    // Bei Eingaben �ber Grob/Fein Garnnummer werden die Prozentzahlen entsprechend nachgerechnet
    ValidatePara(aTag);

  XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);
  mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);

  inherited;

//  if fYarnCount > 0 then
//    // Bei Eingaben �ber Grob/Fein Garnnummer werden die Prozentzahlen entsprechend nachgerechnet
//    ValidatePara(aTag);
//
//  XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);
//  mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);
//
//  inherited;
end;

//:---------------------------------------------------------------------------
procedure TYarnCountEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TYarnCountController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TYarnCountEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
{
  //TODO: Brauchts die Abfrage f�r Available �berhaupt?
  if mMatrixController.Model.MaConfigReader.Available then begin
    // Ist gem�ss FPattern �berhaupt F-Reinigung m�glich?
    if Boolean(mMatrixController.Model.MaConfigReader.FPHandler.Value[cXPFItem]) then begin
    end; // if FPattern.F Enabled
  end; // if MaConfigReader.Available
  Invalidate;
{}
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TYarnCountEditBox.SetYarnCount(const aValue: Extended);
begin
  fYarnCount       := aValue;
  mTemplateMode    := (fYarnCount = 0);
  Values[cYCCount] := fYarnCount;
  if mTemplateMode then begin
    if EnabledField[cYCCount] then begin
      EnabledField[cYCCount] := False;
      EnabledField[cYCCorse] := False;
      EnabledField[cYCFine]  := False;
      TextType[cYCCount]     := ttEmpty;
      TextType[cYCCorse]     := ttEmpty;
      TextType[cYCFine]      := ttEmpty;
      // ShortCount auch ausschalten
      EnabledField[cSCCorse] := False;
      EnabledField[cSCFine]  := False;
      TextType[cSCCorse]     := ttEmpty;
      TextType[cSCFine]      := ttEmpty;
    end;
  end
  else begin
    fYarnCount := aValue;
    if not EnabledField[cYCCount] then begin
      EnabledField[cYCCount] := True;
      EnabledField[cYCCorse] := True;
      EnabledField[cYCFine]  := True;
      TextType[cYCCorse]     := TextType[cYCPDiaDiff];
      TextType[cYCFine]      := TextType[cYCPDiaDiff];
      // ShortCount auch einschalten
      EnabledField[cSCCorse] := mHasShortCount;
      EnabledField[cSCFine]  := mHasShortCount;
      if mHasShortCount then begin
        TextType[cSCCorse]     := TextType[cSCPDiaDiff];
        TextType[cSCFine]      := TextType[cSCPDiaDiff];
      end;
    end;
    TextType[cYCCount]     := ttFloat;
    ValueUpdate(Nil);
  end;
end;

//:---------------------------------------------------------------------------
procedure TYarnCountEditBox.SetYarnUnit(const aValue: TYarnUnit);
begin
  if aValue <> fYarnUnit then begin
    fYarnUnit := aValue;
    mComponentArr[cYCCount].Measure.Caption := cYarnUnitsStr[fYarnUnit];
  end;

//  if aValue <> fYarnUnit then begin
//    fYarnUnit := aValue;
//    mComponentArr[cYCCount].Title.Caption := Format('%s [%s]', [Translate(cYarnCountValuesDef[cYCCount].Caption), cYarnUnitsStr[fYarnUnit]]);
//    if EnabledField[cYCCount] then
//      YarnCount := fYarnCount
//    else
//      YarnCount := 0;
//  end;
end;

//:---------------------------------------------------------------------------
procedure TYarnCountEditBox.ValidatePara(var aTag: Integer);
  //...........................................................
  procedure CheckCoarseCount(aDiffIndex, aCountIndex: Integer);
  var
    xValues: Extended;
  begin
    xValues := Values[aCountIndex];
    if YarnUnit in cYarnUnitsByLength then begin
      if xValues > YarnCount then
        Values[aDiffIndex] := CalculateDiaDiffFromCounts(xValues, YarnCount, True)
      else
        Values[aDiffIndex] := cDiaDiffMin;
    end else if xValues < YarnCount then
      Values[aDiffIndex] := CalculateDiaDiffFromCounts(xValues, YarnCount, False)
    else
      Values[aDiffIndex] := cDiaDiffMin;
  end;
  //...........................................................
  procedure CheckFineCount(aDiffIndex, aCountIndex: Integer);
  var
    xValues: Extended;
  begin
    xValues := Values[aCountIndex];
    if YarnUnit in cYarnUnitsByLength then begin
      if xValues < YarnCount then
        Values[aDiffIndex] := CalculateDiaDiffFromCounts(xValues, YarnCount, True)
      else
        Values[aDiffIndex] := cDiaDiffMin;
    end else if xValues > YarnCount then
      Values[aDiffIndex] := CalculateDiaDiffFromCounts(xValues, YarnCount, False)
    else
      Values[aDiffIndex] := cDiaDiffMin;
  end;
  //...........................................................
begin
  //  Yarn Count                               Short Count
  if ((aTag = cYCCorse) or (aTag = cYCFine) or (aTag = cSCCorse) or (aTag = cSCFine)) and
     (TextType[aTag] <> ttEmpty) then begin
    // Das Berechnen der Prozentzahl wird ben�tigt wenn der Benutzer �ber Grob/Fein
    // die Grenzen der Garnnummer eingibt.
    case aTag of
      cYCCorse: CheckCoarseCount(cYCPDiaDiff, cYCCorse);
      cYCFine:  CheckFineCount(cYCNDiaDiff, cYCFine);
      cSCCorse: CheckCoarseCount(cSCPDiaDiff, cSCCorse);
      cSCFine:  CheckFineCount(cSCNDiaDiff, cSCFine);
    end; // case
  end; // if

  // Hiermit wird das Tag-Item �berschrieben damit der Change mit einem g�ltigen Element ausgel�st wird
  case aTag of
    cYCCorse: aTag := cYCPDiaDiff;
    cYCFine:  aTag := cYCNDiaDiff;
    cSCCorse: aTag := cSCPDiaDiff;
    cSCFine:  aTag := cSCNDiaDiff;
  else
  end;

end;

//:---------------------------------------------------------------------------
procedure TYarnCountEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;

  // ----------- Yarn Count
  if XPSwitch[cYCPDiaDiff] = swOff then begin
    TextType[cYCPDiaDiff] := ttCmdDisabled;
    TextType[cYCNDiaDiff] := ttCmdDisabled;
    TextType[cYCCorse] := ttEmpty;
    TextType[cYCFine]  := ttEmpty; 
  end
  else begin
    TextType[cYCPDiaDiff] := ttFloat;
    Values[cYCPDiaDiff]   := XPValue[cYCPDiaDiff];

    TextType[cYCNDiaDiff] := ttFloat;
    Values[cYCNDiaDiff] := XPValue[cYCNDiaDiff];

    if TextType[cYCCount] <> ttEmpty then begin
      TextType[cYCCorse] := ttFloat;
      TextType[cYCFine]  := ttFloat;
      CalcCoarseFineYarnCount(cYCPDiaDiff, cYCCorse);
    end
    else begin
      TextType[cYCCorse] := ttEmpty;
      TextType[cYCFine]  := ttEmpty;
    end;
  end;

  // Repetition und Length immer abf�llen
  Values[cYCLength] := XPValue[cYCLength];

//  TextType[cYCRepetition] := ttFloat;
//  Values[cYCRepetition]   := XPValue[cYCRepetition];


  // ----------- Short Count
  if mHasShortCount then begin
    if XPSwitch[cSCPDiaDiff] = swOff then begin
      TextType[cSCPDiaDiff]   := ttCmdDisabled;
      TextType[cSCNDiaDiff]   := ttCmdDisabled;
      TextType[cSCCorse]      := ttEmpty;
      TextType[cSCFine]       := ttEmpty;
    end
    else begin
      TextType[cSCPDiaDiff] := ttFloat;
      Values[cSCPDiaDiff]   := XPValue[cSCPDiaDiff];

      TextType[cSCNDiaDiff] := ttFloat;
      Values[cSCNDiaDiff] := XPValue[cSCNDiaDiff];

      if TextType[cYCCount] <> ttEmpty then begin
        TextType[cSCCorse] := ttFloat;
        TextType[cSCFine]  := ttFloat;
        CalcCoarseFineYarnCount(cSCPDiaDiff, cSCCorse);
      end
      else begin
        TextType[cSCCorse] := ttEmpty;
        TextType[cSCFine]  := ttEmpty;
      end;
    end;

    // Repetition und Length immer abf�llen
    Values[cSCLength] := XPValue[cSCLength];

//    TextType[cSCRepetition] := ttFloat;
//    Values[cSCRepetition]   := XPValue[cSCRepetition];
  end; // if mHasShortCount
end;

//:---------------------------------------------------------------------------
//:--- Class: TSpliceEditBox
//:---------------------------------------------------------------------------
constructor TSpliceEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  CaptionWidth := 25;
  ValueWidth   := 31; // textsize 3, (3)
  MeasureWidth := 16;
  Caption      := rsSPCaption;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TSpliceEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cSpliceValuesDef) then
    SetLength(mComponentArr, Length(cSpliceValuesDef));

  inherited BuildBox;

  AddField(cSpN,    cSpliceValuesDef[cSpN],    cPTCHNepDiameter,    Printable);
  AddField(cSpDS,   cSpliceValuesDef[cSpDS],   cPTCHShortDiameter,  Printable);
  AddField(cSpLS,   cSpliceValuesDef[cSpLS],   cPTCHShortLength,    Printable);
  AddField(cSpDL,   cSpliceValuesDef[cSpDL],   cPTCHLongDiameter,   Printable);
  AddField(cSpLL,   cSpliceValuesDef[cSpLL],   cPTCHLongThinLength, Printable);
  AddField(cSpDT,   cSpliceValuesDef[cSpDT],   cPTCHThinDiameter,   Printable);
  AddField(cSpLT,   cSpliceValuesDef[cSpLT],   cPTCHLongThinLength, Printable);
  AddField(cSPDUpY, cSpliceValuesDef[cSPDUpY], cPTCHLongDiameter,   Printable);
end;

//:---------------------------------------------------------------------------
procedure TSpliceEditBox.CheckComponents;
begin
// TODO XML: Splice Status noch von FPattern ermitteln?
  Enabled   := True;
//    UpperYarn := xMachineAttributes.IsUpperYarn or xConfigCode.UpperYarnCheck;
// TODO XML: warum �ber Maschinen UND ConfigCode UpperYarn feststellen?
  UpperYarn := Boolean(mController.Model.FPHandler.Value[cXPFP_UpperYarnChannelItem]) or
               Boolean(mController.ValueDef[cXPFP_UpperYarnChannelItem, False]);
end;

//:---------------------------------------------------------------------------
procedure TSpliceEditBox.DoConfirm(aTag: Integer);
var
  xDia, xLen: Extended;
begin
  { F�r den Thin-Parameter wird folgendes noch gepr�ft:
    Thin length > 5cm: -6 ...-60,
    Thin length <= 5cm: -20 ...-60

    Wird der Diameter ver�ndert so wird die L�nge gepr�ft, wird die L�nge editiert
    so wird der Diameter angepasst.
  {}
  case aTag of
    cSpDT: begin
          xDia := 1.0 - Values[cSpDT];
          xLen := Values[cSpLT];
          if xDia >= cThinDiaMaxBelow then
            xLen := FitValue(xLen, cThinLenThreshold, cThinLenMax)
          else
            xLen := FitValue(xLen, cThinLenMin, cThinLenMax);

          XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);
          XPValue[cSpLT] := xLen;
          mController.Change(mComponentArr[aTag].XPValue, xDia);
      end;
    cSpLT: begin
          xDia := 1.0 - Values[cSpDT];
          xLen := Values[cSpLT];
          if xLen >= cThinLenThreshold then
            xDia := FitValue(xDia, cThinDiaMin, cThinDiaMaxAbove)
          else
            xDia := FitValue(xDia, cThinDiaMin, cThinDiaMaxBelow);

          XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);
          XPValue[cSpDT] := xDia;
          mController.Change(mComponentArr[aTag].XPValue, xLen);
      end;
  else
    XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);
    mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);
  end; // with

  inherited;
end;

//:---------------------------------------------------------------------------
procedure TSpliceEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TSpliceController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
  aController.ListenToKindSet     := [ckSplice];
end;

//:---------------------------------------------------------------------------
function TSpliceEditBox.GetUpperYarn: Boolean;
begin
  Result := EnabledField[cSPDUpY];
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TSpliceEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TSpliceEditBox.SetUpperYarn(const aValue: Boolean);
begin
// TODO wss: lokale Membervariable mUpperYarn umsetzen
  if EnabledField[cSPDUpY] <> aValue then begin
    EnabledField[cSPDUpY] := aValue;
//    if aValue then begin
//      TextType[cSPDUpY] := ParaSwitchToTextType();
//    end
//    else begin
//      TextType[cSPDUpY] := ttCmdDisabled;
//    end;
    if not aValue then
      TextType[cSPDUpY] := ttCmdDisabled;
  end;
{
  if EnabledField[cSPDUpY] <> aValue then
  begin
    EnabledField[cSPDUpY] := aValue;
    if aValue then
    begin
      Values[cSPDUpY] := mUpperYarn.dia / 100;
      TextType[cSPDUpY] := ParaSwitchToTextType(mUpperYarn.sw);
    end
    else
    begin
      mUpperYarn.dia := Round(Values[cSPDUpY] * 100);
      mUpperYarn.sw := TextTypToParaSwitch(TextType[cSPDUpY]);
      TextType[cSPDUpY] := ttCmdDisabled;
    end;
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TSpliceEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
var
  i: Integer;
begin
  {TODO wss: f�r XML aus FPattern lesen
    xConfigCode        := TConfigurationCode.Create(aSettings);
    xMachineAttributes := TMachineAttributes.Create(aSettings);

    UpperYarn := xMachineAttributes.IsUpperYarn or xConfigCode.UpperYarnCheck;

    xMachineAttributes.Free;
    xConfigCode.Free;
  {}

  CheckComponents;
  // Array High - 1, da UpperYarn separat behandelt wird
  for i:=0 to High(mComponentArr)-1 do begin
    if i = cSpDT then Values[i] := (1.0 - XPValue[i])
                 else Values[i] := XPValue[i];
    TextType[i] := ParaSwitchToTextType(XPSwitch[i]);
  end; // for

  if UpperYarn then begin
    Values[cSpDUpY]   := XPValue[cSpDUpY];
    TextType[cSpDUpY] := ParaSwitchToTextType(XPSwitch[cSpDUpY]);
  end;
end;


//:---------------------------------------------------------------------------
//:--- Class: TSFIEditBox
//:---------------------------------------------------------------------------
constructor TSFIEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  CaptionWidth := 75;
  MeasureWidth := 10;

  Caption      := rsSFICaption;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TSFIEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cSFIValuesDef) then
    SetLength(mComponentArr, Length(cSFIValuesDef));

  inherited BuildBox;

  ValueWidth   := 50; // damit der Text Floating mehr Platz hat
  AddField(cSFIReference,  cSFIValuesDef[cSFIReference],  cPTSFIReferenc, Printable);
  ValueWidth   := 35;
  AddField(cSFIPosLimit,   cSFIValuesDef[cSFIPosLimit],   cPTSFILimit, Printable);
  AddField(cSFINegLimit,   cSFIValuesDef[cSFINegLimit],   cPTSFILimit, Printable);
end;

//:---------------------------------------------------------------------------
procedure TSFIEditBox.CheckComponents;
begin
  Enabled := Boolean(mController.Model.FPHandler.Value[cXPFP_SFIItem]);
end;

//:---------------------------------------------------------------------------
procedure TSFIEditBox.DoConfirm(aTag: Integer);
var
  xValue: Double;
begin
  ValidatePara(aTag);
  case aTag of
    cSFIReference: begin
        if TextType[cSFIReference] = ttCmdDisabled then xValue := cSFIRefFloat
                                                   else xValue := Values[cSFIReference];
      end;
    cSFIPosLimit: begin
        XPSwitch[cSFIPosLimit] := TextTypToParaSwitch(TextType[cSFIPosLimit]);
        if TextType[cSFIPosLimit] = ttCmdDisabled then xValue := cSFILimitOff
                                                  else xValue := Values[cSFIPosLimit];
      end;
    cSFINegLimit: begin
        XPSwitch[cSFINegLimit] := TextTypToParaSwitch(TextType[cSFINegLimit]);
        if TextType[cSFINegLimit] = ttCmdDisabled then xValue := cSFILimitOff
                                                  else xValue := Values[cSFINegLimit];
      end;
  else //  cSFIReference, cSFIRepetition
    xValue := Values[aTag];
  end;
  mController.Change(mComponentArr[aTag].XPValue, xValue);

  inherited;
end;

//:---------------------------------------------------------------------------
procedure TSFIEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TSFIController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TSFIEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TSFIEditBox.ValidatePara(aTag: Integer);
begin
  // Wenn Off und dann bei einem Feld etwas eingegeben wird, dann das andere Feld angleichen
  // Wert wird gleich in DOM geschrieben, damit �ber die Benachrichtigung der richte Wert
  // wieder ausgelesen wird.
  case aTag of
    cSFIPosLimit: begin
        if TextType[cSFIPosLimit] = ttCmdDisabled then
          TextType[cSFINegLimit] := ttCmdDisabled
        else if TextType[cSFINegLimit] = ttCmdDisabled then begin
          TextType[cSFINegLimit] := ttFloat;
          XPValue[cSFINegLimit]  := Values[cSFIPosLimit];
        end;
      end;

    cSFINegLimit: begin
        if TextType[cSFINegLimit] = ttCmdDisabled then
          TextType[cSFIPosLimit] := ttCmdDisabled
        else if TextType[cSFIPosLimit] = ttCmdDisabled then begin
          TextType[cSFIPosLimit] := ttFloat;
          XPValue[cSFIPosLimit]  := Values[cSFINegLimit];
        end;
      end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TSFIEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;

  if XPValue[cSFIReference] = cSFIRefFloat then
    TextType[cSFIReference] := ttCmdDisabled
  else begin
    TextType[cSFIReference] := ttFloat;
    Values[cSFIReference]   := XPValue[cSFIReference];
  end;

  // Wenn einer von beiden der Wert 0 hat, dann sind beide auf Off
//  if (XPValue[cSFIPosLimit] = cSFILimitOff) or
//     (XPValue[cSFINegLimit] = cSFILimitOff) then begin
  if (XPValue[cSFIPosLimit] = cSFILimitOff) or
     (XPValue[cSFINegLimit] = cSFILimitOff) or
     (XPSwitch[cSFIPosLimit] = swOff) or          //Nue:7.5.08 Added
     (XPSwitch[cSFINegLimit] = swOff) then begin  //Nue:7.5.08 Added
    TextType[cSFIPosLimit] := ttCmdDisabled;
    TextType[cSFINegLimit] := ttCmdDisabled
  end
  else begin
    TextType[cSFIPosLimit] := ttFloat;
    Values[cSFIPosLimit]   := XPValue[cSFIPosLimit];

    TextType[cSFINegLimit] := ttFloat;
    Values[cSFINegLimit]   := XPValue[cSFINegLimit];
  end;

//  TextType[cSFIRepetition] := ttFloat;
//  Values[cSFIRepetition]   := XPValue[cSFIRepetition];
end;


//:---------------------------------------------------------------------------
//:--- Class: TFFClusterEditBox
//:---------------------------------------------------------------------------
constructor TFFClusterEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  ValueWidth           := 35; // textsize 3, (3)
  MeasureWidth         := 10;
  
  fSynchronizeFFMatrix := nil;
  
  Caption              := rsFClusterCaption;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TFFClusterEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cFClusterValuesDef) then
    SetLength(mComponentArr, Length(cFClusterValuesDef));

  inherited BuildBox;

  // Anlaufschnitt
//  CaptionWidth := 75;

//  CaptionWidth := 0;  // Label "Fehlerschwarm" Links ausgerichtet
//  inc(mTopPos, cGEBBoarder);
//  AddTitle(mClusterLabel, rsClusterCaption, taLeftJustify, True);

  // Dark Cluster
  CaptionWidth := 75;
//  mTopPos      := mClusterLabel.Top;
  AddField(cFCDarkLength,     cFClusterValuesDef[cFCDarkLength],     cPTClObsLength, Printable);
  AddField(cFCDarkFaults,     cFClusterValuesDef[cFCDarkFaults],     cPTClFaults, Printable);

{ F�r Bright-Cluster werden die gleichen Settings wie f�r Dark verwendet -> ergo nur 1 Parameter f�r beide
  // Bright Cluster
  CaptionWidth := 0;
  mLeftPos     := Width;
  mTopPos      := mClusterLabel.Top;
  AddTitle(mBrightLabel, rsFBrightCaption, True);
  AddField(cFCBrightLength,     cFClusterValuesDef[cFCBrightLength],     cPTClObsLength, Printable);
  AddField(cFCBrightFaults,     cFClusterValuesDef[cFCBrightFaults],     cPTClFaults, Printable);
  AddField(cFCBrightRepetition, cFClusterValuesDef[cFCBrightRepetition], cPTRepetition, Printable);
{}
end;

procedure TFFClusterEditBox.CheckComponents;
var
  xBool: Boolean;
  xSHClass: TSensingHeadClass;
  //...........................................................
  procedure SetEnabledField(aBaseIndex: Integer; aEnabled: Boolean);
  begin
    EnabledField[aBaseIndex]   := aEnabled; // Length
    EnabledField[aBaseIndex+1] := aEnabled; // Faults
//    EnabledField[aBaseIndex+2] := aEnabled; // Repetition
  end;
  //...........................................................
begin
//    Enabled := xMachineAttributes.IsFFCluster and xConfigCode.FFDetection and
//               not xConfigCode.FFBDDetection;

  // Grunds�tzlich mal ist es nur freigegeben wenn F-Reinigung vorhanden und aktiv ist
  Enabled := IsFSensingHead(mController.Model) and
             Boolean(mController.Model.ValueDef[cXPFSensorActiveItem, False]);
  if Enabled then begin
    // und nun noch die Indivuduellen FPatterns auswerten
    xSHClass := GetSensingHeadClass(mController.Model);
    // Dark Cluster
    xBool := Boolean(mController.Model.FPHandler.Value[cXPFP_FClusterDarkItem]) and (xSHClass <> shc9xBD);
//    mClusterLabel.Enabled := xBool;
    SetEnabledField(cFCDarkLength, xBool);
    // Bright Cluster
{ F�r Bright-Cluster werden die gleichen Settings wie f�r Dark verwendet -> ergo nur 1 Parameter f�r beide
    xBool := xBool and (xSHClass in [shcZenitF, shcZenitFP]);
    mBrightLabel.Enabled  := xBool;
    SetEnabledField(cFCBrightLength, xBool);
{}
  end
  else begin
//    mClusterLabel.Enabled := False;
{ F�r Bright-Cluster werden die gleichen Settings wie f�r Dark verwendet -> ergo nur 1 Parameter f�r beide
    mDarkLabel.Enabled    := False;
    mBrightLabel.Enabled  := False;
{}
  end;
end;

//:---------------------------------------------------------------------------
procedure TFFClusterEditBox.DoConfirm(aTag: Integer);
begin
  // ObsLen und Faults auf Switch pr�fen, f�r die anderen Werte gibt es keinen Off Zustand und die Werte werden immer angezeigt
  if (aTag = cFCDarkLength) {or (aTag = cFCDarkFaults)} then
    XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);

  mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);

  inherited;
end;

//:---------------------------------------------------------------------------
procedure TFFClusterEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TFClusterController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
  aController.ListenToKindSet := [];
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TFFClusterEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TFFClusterEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
  //........................................................
  procedure UpdateValues(aBaseIndex: Integer);
  begin
    if XPSwitch[aBaseIndex] = swOff then begin
      TextType[aBaseIndex]   := ttCmdDisabled; // Length
    end
    else begin
      Values[aBaseIndex]     := XPValue[aBaseIndex];
    end;
    Values[aBaseIndex+1]   := XPValue[aBaseIndex+1];  // Faults
  end;
  //........................................................
begin
  CheckComponents;

  UpdateValues(cFCDarkLength);
  // F�r Bright-Cluster werden die gleichen Settings wie f�r Dark verwendet -> ergo nur 1 Parameter f�r beide
  // UpdateValues(cFCBrightLength);

  // TODO XML: brauchts das noch?
  if Assigned(SynchronizeFFMatrix) then
    SynchronizeFFMatrix;
end;

//:---------------------------------------------------------------------------
//:--- Class: TPEditBox
//:---------------------------------------------------------------------------
constructor TPEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  CaptionWidth   := 75;
  ValueWidth     := 35; // textsize 3, (3)
  MeasureWidth   := 16;
  
  Caption := rsPCaption;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TPEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cPValuesDef) then
    SetLength(mComponentArr, Length(cPValuesDef));

  inherited BuildBox;

  AddField(cPLimit,             cPValuesDef[cPLimit],             cPTPLimit,     Printable);
  AddField(cPRefLength,         cPValuesDef[cPRefLength],         cPTPRefLength, Printable);
//  AddField(cPLowLimit,          cPValuesDef[cPLowLimit],          cPTPercent,    Printable);
//  AddField(cPLowBit,            cPValuesDef[cPLowBit],            cPTLowBit,     Printable);
//  AddField(cPReduction,         cPValuesDef[cPReduction],         cPTPercent,    Printable);
end;
//:---------------------------------------------------------------------------
procedure TPEditBox.CheckComponents;
var
  xVisible : Boolean;
begin
  // Grunds�tzlich mal ist es nur freigegeben wenn P-Reinigung vorhanden ist
  Enabled := Boolean(mController.Model.FPHandler.Value[cXPFP_PItem]);
  //P-Channel ist ein reines ZENIT-Feature ==> Hide if not ZENIT Nue:04.06.08
  xVisible := Boolean(mController.Model.FPHandler.Value[cXPFP_ZenitItem]);
  Visible := (xVisible) or (Boolean(mController.Model.FPHandler.Value[cXPFP_ZenitCItem]));
end;

//:---------------------------------------------------------------------------
procedure TPEditBox.DoConfirm(aTag: Integer);
begin
  if (aTag = cPLimit) {or (aTag = cPStartupRepetition) }then
    XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);

  mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);

  inherited;
end;

//:---------------------------------------------------------------------------
procedure TPEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TPController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
  aController.ListenToKindSet := [];
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TPEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TPEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;

  if XPSwitch[cPLimit] = swOff then begin
    TextType[cPLimit]             := ttCmdDisabled;
//    TextType[cPRefLength]         := ttCmdDisabled;
  end
  else begin
    Values[cPLimit]               := XPValue[cPLimit];
  end;
  Values[cPRefLength]           := XPValue[cPRefLength];

//  if XPSwitch[cPStartupRepetition] = swOff then begin
//    TextType[cPStartupRepetition] := ttCmdDisabled;
//    TextType[cPLowLimit]          := ttCmdDisabled;
//    TextType[cPLowBit]            := ttCmdDisabled;
//    TextType[cPReduction]         := ttCmdDisabled;
//  end
//  else begin
//    Values[cPStartupRepetition]   := XPValue[cPStartupRepetition];
//    Values[cPLowLimit]            := XPValue[cPLowLimit];
//    Values[cPLowBit]              := XPValue[cPLowBit];
//    Values[cPReduction]           := XPValue[cPReduction];
//  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TSFIEditBox
//:---------------------------------------------------------------------------
constructor THeadClassEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  BorderWidth := 2;

  fModel      := Nil;
  mController := THeadClassController.Create;
  mController.OnValueUpdate := ValueUpdate;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure THeadClassEditBox.BuildBox;
begin
  mHeadClassValue := TmmLabel.Create(Self);
  with mHeadClassValue do begin
    Caption   := '';
    Align     := alRight;
    Alignment := taLeftJustify;
    Layout    := tlCenter;
    Autosize  := False;
    Width     := 60;
    Parent    := Self;
  end;

  mHeadClassTitle := TmmLabel.Create(Self);
  with mHeadClassTitle do begin
    Caption   := Translate(rsHeadClassTitle);
    Align     := alClient;
    Alignment := taLeftJustify;
    Layout    := tlCenter;
    Autosize  := False;
    Parent    := Self;
  end;
end;

//:---------------------------------------------------------------------------
procedure THeadClassEditBox.CheckComponents;
begin
  Enabled := True;
end;

//:---------------------------------------------------------------------------
procedure THeadClassEditBox.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  if fModel <> aValue then begin
    // Wenn vorher schon ein Model dran war, erst die Observer unregistrieren
    if Assigned(fModel) then
      fModel.UnregisterObserver(mController);

    fModel := aValue;
    // Ein neues Model wird verlinkt -> die Observer registrieren lassen
    // Durch das Registrieren wird vom Model �ber den Observer ein ValueUpdate ausgel�st
    if Assigned(fModel) then
      fModel.RegisterObserver(mController);
  end;
end;

//:---------------------------------------------------------------------------
procedure THeadClassEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
var
  xSHC: TSensingHeadClass;
begin
  CheckComponents;

  xSHC := YMParaUtils.GetSensingHeadClass(mController.Model);
  mHeadClassValue.Caption := cGUISensingHeadClassNames[xSHC];
end;

//:---------------------------------------------------------------------------
//:--- Class: TFaultClusterEditBox
//:---------------------------------------------------------------------------
constructor TRepetitionEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  mClusterLabel    := Nil;
  mStartupCutLabel := Nil;
  mYarnCountLabel  := Nil;

  ValueWidth   := 35; // textsize 3, (3)
  MeasureWidth := 0;
  Caption      := rsRepetition;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TRepetitionEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cRepetitionValuesDef) then
    SetLength(mComponentArr, Length(cRepetitionValuesDef));

  inherited BuildBox;

  // NSLT
  CaptionWidth := 80;
  AddField(cRepNSLT,     cRepetitionValuesDef[cRepNSLT],     cPTRepetitionOff,    Printable);

  // Yarn Count
  CaptionWidth := 0;  // Label Links ausgerichtet
  inc(mTopPos, cGEBSpace);
  AddTitle(mYarnCountLabel, rsYCCaption, taLeftJustify, True);
  CaptionWidth := 80;
  AddField(cRepSCnt,     cRepetitionValuesDef[cRepSCnt],     cPTRepetition, Printable);
  AddField(cRepYCnt,     cRepetitionValuesDef[cRepYCnt],     cPTRepetition, Printable);

  // SFI
  AddField(cRepSFI,      cRepetitionValuesDef[cRepSFI],      cPTRepetition, Printable);

  // VCV
  AddField(cRepVCV,      cRepetitionValuesDef[cRepVCV],      cPTRepetition, Printable);

  // Startup Cut
  CaptionWidth := 0;  // Label Links ausgerichtet
  inc(mTopPos, cGEBSpace);
  AddTitle(mStartupCutLabel, rsStartupCutCaption, taLeftJustify, True);
  CaptionWidth := 80;
  AddField(cRepFStartup, cRepetitionValuesDef[cRepFStartup], cPTRepetition, Printable);
  AddField(cRepPStartup, cRepetitionValuesDef[cRepPStartup], cPTRepetitionOff, Printable);

  // Cluster
  CaptionWidth := 0;  // Label Links ausgerichtet
  inc(mTopPos, cGEBSpace);
  AddTitle(mClusterLabel, rsClusterCaption, taLeftJustify, True);
  CaptionWidth := 80;
  AddField(cRepClS,      cRepetitionValuesDef[cRepClS],      cPTRepetition, Printable);
  AddField(cRepClL,      cRepetitionValuesDef[cRepClL],      cPTRepetition, Printable);
  AddField(cRepClT,      cRepetitionValuesDef[cRepClT],      cPTRepetition, Printable);
  AddField(cRepFCluster, cRepetitionValuesDef[cRepFCluster], cPTRepetition, Printable);
end;
//:---------------------------------------------------------------------------
procedure TRepetitionEditBox.CheckComponents;
var
  xBool: Boolean;
  xSHClass: TSensingHeadClass;
begin
  Enabled := True;
  // NSLT
  EnabledField[cRepNSLT] := Boolean(mController.Model.FPHandler.Value[cXPFP_ZenitItem]);

  // Yarn/Short Count
  // ShortCount verf�gbar?
  mHasShortCount := Boolean(mController.Model.FPHandler.Value[cXPFP_ShortCountItem]);
  EnabledField[cRepSCnt] := mHasShortCount;
  if mHasShortCount then TextType[cRepSCnt] := ttFloat
                    else TextType[cRepSCnt] := ttEmpty;
  EnabledField[cRepYCnt] := True;

  // SFI
  EnabledField[cRepSFI] := Boolean(mController.Model.FPHandler.Value[cXPFP_SFIItem]);

  // VCV
  EnabledField[cRepVCV] := Boolean(mController.Model.FPHandler.Value[cXPFP_VCVItem]);

  // F Startup Cut und F Cluster
  // Grunds�tzlich mal ist es nur freigegeben wenn F-Reinigung vorhanden und aktiv ist
  xSHClass := GetSensingHeadClass(mController.Model);
  xBool := IsFSensingHead(mController.Model) and Boolean(mController.Model.ValueDef[cXPFSensorActiveItem, False]);
  EnabledField[cRepFStartup] := xBool;
  xBool := xBool and (Boolean(mController.Model.FPHandler.Value[cXPFP_FClusterDarkItem]) and (xSHClass <> shc9xBD));
  EnabledField[cRepFCluster] := xBool;

  // Grunds�tzlich mal ist es nur freigegeben wenn P-Reinigung vorhanden ist
  EnabledField[cRepPStartup] := Boolean(mController.Model.FPHandler.Value[cXPFP_PItem]);

  // Cluster
  mHasExtCluster := Boolean(mController.Model.FPHandler.Value[cXPFP_ExtendedClusterItem]);
  EnabledField[cRepClS]      := Boolean(mController.Model.FPHandler.Value[cXPFP_ClusterItem]);
  EnabledField[cRepClL]      := mHasExtCluster;
  EnabledField[cRepClT]      := mHasExtCluster;
end;

//:---------------------------------------------------------------------------
procedure TRepetitionEditBox.DoConfirm(aTag: Integer);
begin
  // ObsLen und Faults auf Switch pr�fen, f�r die anderen Werte gibt es keinen Off Zustand und die Werte werden immer angezeigt
  case aTag of
    cRepNSLT, cRepPStartup:
      XPSwitch[aTag] := TextTypToParaSwitch(TextType[aTag]);
  else
  end;

  mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);

  inherited DoConfirm(aTag);
end;

//:---------------------------------------------------------------------------
procedure TRepetitionEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TRepetitionController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
  aController.ListenToKindSet     := [];
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TRepetitionEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TRepetitionEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
  //........................................................
  procedure UpdateValues(aIndex: Integer);
  begin
    if XPSwitch[aIndex] = swOff then
      TextType[aIndex] := ttCmdDisabled
    else begin
      TextType[aIndex] := ttFloat;
      Values[aIndex]   := XPValue[aIndex];
    end;
  end;
  //........................................................
begin
  CheckComponents;

  UpdateValues(cRepNSLT);

  if mHasShortCount then begin
    TextType[cRepSCnt] := ttFloat;
    Values[cRepSCnt]   := XPValue[cRepSCnt];
  end;
  TextType[cRepYCnt]     := ttFloat;
  Values[cRepYCnt]       := XPValue[cRepYCnt];

  TextType[cRepSFI]      := ttFloat;
  Values[cRepSFI]        := XPValue[cRepSFI];

  TextType[cRepVCV]      := ttFloat;
  Values[cRepVCV]        := XPValue[cRepVCV];

  TextType[cRepFStartup] := ttFloat;
  Values[cRepFStartup]   := XPValue[cRepFStartup];

  UpdateValues(cRepPStartup);

  TextType[cRepClS]      := ttFloat;
  TextType[cRepClL]      := ttFloat;
  TextType[cRepClT]      := ttFloat;
  TextType[cRepFCluster] := ttFloat;
  Values[cRepClS]        := XPValue[cRepClS];
  Values[cRepClL]        := XPValue[cRepClL];
  Values[cRepClT]        := XPValue[cRepClT];
  Values[cRepFCluster]   := XPValue[cRepFCluster];
end;

//:---------------------------------------------------------------------------
//:--- Class: TFFClusterEditBox
//:---------------------------------------------------------------------------
constructor TPExtEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  CaptionWidth   := 75;
  ValueWidth     := 35; // textsize 3, (3)
  MeasureWidth   := 16;
  
  Caption := rsPExtCaption;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TPExtEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cPExtValuesDef) then
    SetLength(mComponentArr, Length(cPExtValuesDef));

  inherited BuildBox;

  AddField(cPExtLowLimit,  cPExtValuesDef[cPExtLowLimit],  cPTPercent, Printable);
  AddField(cPExtLowBit,    cPExtValuesDef[cPExtLowBit],    cPTLowBit,  Printable);
  AddField(cPExtReduction, cPExtValuesDef[cPExtReduction], cPTPercent, Printable);
end;

//:---------------------------------------------------------------------------
procedure TPExtEditBox.CheckComponents;
var
  xVisible : Boolean;
begin
  // Grunds�tzlich mal ist es nur freigegeben wenn P-Reinigung vorhanden ist
  Enabled := Boolean(mController.Model.FPHandler.Value[cXPFP_PItem]);
  //P-Channel ist ein reines ZENIT-Feature ==> Hide if not ZENIT Nue:04.06.08
  xVisible := Boolean(mController.Model.FPHandler.Value[cXPFP_ZenitItem]);
  Visible := (xVisible) or (Boolean(mController.Model.FPHandler.Value[cXPFP_ZenitCItem]));
end;

//:---------------------------------------------------------------------------
procedure TPExtEditBox.DoConfirm(aTag: Integer);
begin
  mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);
  inherited;
end;

//:---------------------------------------------------------------------------
procedure TPExtEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TPController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
  aController.ListenToKindSet     := [];
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TPExtEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TPExtEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;

  Values[cPExtLowLimit]  := XPValue[cPExtLowLimit];
  Values[cPExtLowBit]    := XPValue[cPExtLowBit];
  Values[cPExtReduction] := XPValue[cPExtReduction];

//  if XPSwitch[cPLimit] = swOff then begin
//    TextType[cPLimit]             := ttCmdDisabled;
//    TextType[cPRefLength]         := ttCmdDisabled;
//  end
//  else begin
//    Values[cPLimit]               := XPValue[cPLimit];
//    Values[cPRefLength]           := XPValue[cPRefLength];
//  end;

//  if XPSwitch[cPStartupRepetition] = swOff then begin
//    TextType[cPStartupRepetition] := ttCmdDisabled;
//    TextType[cPLowLimit]          := ttCmdDisabled;
//    TextType[cPLowBit]            := ttCmdDisabled;
//    TextType[cPReduction]         := ttCmdDisabled;
//  end
//  else begin
//    Values[cPStartupRepetition]   := XPValue[cPStartupRepetition];
//    Values[cPLowLimit]            := XPValue[cPLowLimit];
//    Values[cPLowBit]              := XPValue[cPLowBit];
//    Values[cPReduction]           := XPValue[cPReduction];
//  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TSFIEditBox
//:---------------------------------------------------------------------------
constructor TIPIEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  CaptionWidth := 75;
  MeasureWidth := 10;

  Caption      := rsIPICaption;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TIPIEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cIPIValuesDef) then
    SetLength(mComponentArr, Length(cIPIValuesDef));

  inherited BuildBox;

  ValueWidth   := 50; // damit der Text Floating mehr Platz hat
  AddField(cIPINeps,       cIPIValuesDef[cIPINeps],       cPTIPINeps, Printable);
  AddField(cIPISmallPlus,  cIPIValuesDef[cIPISmallPlus],  cPTIPIOthers, Printable);
  AddField(cIPISmallMinus, cIPIValuesDef[cIPISmallMinus], cPTIPIOthers, Printable);
  AddField(cIPIThick,      cIPIValuesDef[cIPIThick],      cPTIPIOthers, Printable);
  AddField(cIPIThin,       cIPIValuesDef[cIPIThin],       cPTIPIOthers, Printable);
end;

//:---------------------------------------------------------------------------
procedure TIPIEditBox.CheckComponents;
begin
  //Wss: auf der LZE werden diese Parameter pauschal �ber das Zenit Flag freigegeben
  Enabled := Boolean(mController.Model.FPHandler.Value[cXPFP_ZenitItem]);
end;

//:---------------------------------------------------------------------------
procedure TIPIEditBox.DoConfirm(aTag: Integer);
begin
  mController.Change(mComponentArr[aTag].XPValue, Values[aTag]);

  inherited;
end;

//:---------------------------------------------------------------------------
procedure TIPIEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TIPIController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TIPIEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TIPIEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;

  if Enabled then begin
    Values[cIPINeps]       := XPValue[cIPINeps];
    Values[cIPISmallPlus]  := XPValue[cIPISmallPlus];
    Values[cIPISmallMinus] := XPValue[cIPISmallMinus];
    Values[cIPIThick]      := XPValue[cIPIThick];
    Values[cIPIThin]       := XPValue[cIPIThin];
  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TVCVEditBox
//:---------------------------------------------------------------------------
constructor TVCVEditBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  CaptionWidth := 75;
  MeasureWidth := 10;

  Caption      := rsVCVCaption;

  BuildBox;
end;

//:---------------------------------------------------------------------------
procedure TVCVEditBox.BuildBox;
begin
  if Length(mComponentArr) <> Length(cVCVValuesDef) then
    SetLength(mComponentArr, Length(cVCVValuesDef));

  inherited BuildBox;

  ValueWidth   := 35;
  AddField(cVCVPosLimit,   cVCVValuesDef[cVCVPosLimit],   cPTVCVLimit, Printable);
  AddField(cVCVNegLimit,   cVCVValuesDef[cVCVNegLimit],   cPTVCVLimit, Printable);
//  ValueWidth   := 50; // damit der Text Floating mehr Platz hat
  AddField(cVCVLength,  cVCVValuesDef[cVCVLength],  cPTVCVLength, Printable);
end;

//:---------------------------------------------------------------------------
procedure TVCVEditBox.CheckComponents;
var
  xVisible : Boolean;
begin
  // Grunds�tzlich mal ist es nur freigegeben wenn VCV vorhanden ist
  Enabled := Boolean(mController.Model.FPHandler.Value[cXPFP_VCVItem]);
  //VCV ist ein reines ZENIT-Feature ==> Hide if not ZENIT Nue:04.06.08
  xVisible := Boolean(mController.Model.FPHandler.Value[cXPFP_ZenitItem]);
  Visible := (xVisible) or (Boolean(mController.Model.FPHandler.Value[cXPFP_ZenitCItem]));
end;

//:---------------------------------------------------------------------------
procedure TVCVEditBox.DoConfirm(aTag: Integer);
var
  xValue: Double;
begin
  ValidatePara(aTag);
  case aTag of
    cVCVPosLimit: begin
        XPSwitch[cVCVPosLimit] := TextTypToParaSwitch(TextType[cVCVPosLimit]);
        if TextType[cVCVPosLimit] = ttCmdDisabled then xValue := cVCVLimitOff
                                                  else xValue := Values[cVCVPosLimit];
      end;
    cVCVNegLimit: begin
        XPSwitch[cVCVNegLimit] := TextTypToParaSwitch(TextType[cVCVNegLimit]);
        if TextType[cVCVNegLimit] = ttCmdDisabled then xValue := cVCVLimitOff
                                                  else xValue := Values[cVCVNegLimit];
      end;
  else //  cVCVLength
    xValue := Values[aTag];
  end;
  mController.Change(mComponentArr[aTag].XPValue, xValue);

  inherited;
end;

//:---------------------------------------------------------------------------
procedure TVCVEditBox.GetCreateController(var aController: TBaseGUIController);
begin
  aController := TVCVController.Create;
  aController.OnValueUpdate       := ValueUpdate;
  aController.OnMachineInitialize := MachineInitialize;
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TVCVEditBox.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
end;

//:---------------------------------------------------------------------------
procedure TVCVEditBox.ValidatePara(aTag: Integer);
begin
  // Wenn Off und dann bei einem Feld etwas eingegeben wird, dann das andere Feld angleichen
  // Wert wird gleich in DOM geschrieben, damit �ber die Benachrichtigung der richte Wert
  // wieder ausgelesen wird.
  case aTag of
    cVCVPosLimit: begin
        if TextType[cVCVPosLimit] = ttCmdDisabled then
          TextType[cVCVNegLimit] := ttCmdDisabled
        else if TextType[cVCVNegLimit] = ttCmdDisabled then begin
          TextType[cVCVNegLimit] := ttFloat;
          XPValue[cVCVNegLimit]  := Values[cVCVPosLimit];
        end;
      end;

    cVCVNegLimit: begin
        if TextType[cVCVNegLimit] = ttCmdDisabled then
          TextType[cVCVPosLimit] := ttCmdDisabled
        else if TextType[cVCVPosLimit] = ttCmdDisabled then begin
          TextType[cVCVPosLimit] := ttFloat;
          XPValue[cVCVPosLimit]  := Values[cVCVNegLimit];
        end;
      end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TVCVEditBox.ValueUpdate(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;

  // Wenn einer von beiden der Wert 0 hat, dann sind beide auf Off
//  if (XPValue[cVCVPosLimit] = cVCVLimitOff) or
//     (XPValue[cVCVNegLimit] = cVCVLimitOff) then begin
  if (XPValue[cVCVPosLimit] = cVCVLimitOff) or
     (XPValue[cVCVNegLimit] = cVCVLimitOff) or
     (XPSwitch[cVCVPosLimit] = swOff) or          //Nue:7.5.08 Added
     (XPSwitch[cVCVNegLimit] = swOff) then begin  //Nue:7.5.08 Added
    TextType[cVCVPosLimit] := ttCmdDisabled;
    TextType[cVCVNegLimit] := ttCmdDisabled
  end
  else begin
    TextType[cVCVPosLimit] := ttFloat;
    Values[cVCVPosLimit]   := XPValue[cVCVPosLimit];

    TextType[cVCVNegLimit] := ttFloat;
    Values[cVCVNegLimit]   := XPValue[cVCVNegLimit];
  end;

  Values[cVCVLength]   := XPValue[cVCVLength];

end;




end.

