object GBBasicProdPara: TGBBasicProdPara
  Left = 0
  Top = 0
  Width = 936
  Height = 495
  TabOrder = 0
  object laPilotLabel: TmmLabel
    Left = 218
    Top = 269
    Width = 190
    Height = 13
    AutoSize = False
    Caption = '(28)Anzahl Pilotspulstellen:'
    FocusControl = laPilotCount
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laDFSSensitivity: TmmLabel
    Left = 218
    Top = 147
    Width = 192
    Height = 13
    AutoSize = False
    Caption = '(28)DFS Empfindlichkeit:'
    FocusControl = edDFSSensitivity
    Visible = True
    AutoLabel.Control = laTimes2
    AutoLabel.Distance = 22
    AutoLabel.LabelPosition = lpRight
  end
  object laReduction: TmmLabel
    Left = 218
    Top = 171
    Width = 190
    Height = 13
    AutoSize = False
    Caption = '(28)Reduktion bei Feinabgleich:'
    FocusControl = edReduction
    Visible = True
    AutoLabel.Control = laReductionUnit
    AutoLabel.Distance = 40
    AutoLabel.LabelPosition = lpRight
  end
  object laSpeedLabel: TmmLabel
    Left = 24
    Top = 340
    Width = 174
    Height = 13
    AutoSize = False
    Caption = '(28)Geschwindigkeit:'
    FocusControl = laSpeed
    Visible = True
    AutoLabel.Control = laSpeedUnit
    AutoLabel.Distance = 40
    AutoLabel.LabelPosition = lpRight
  end
  object laSpeedRampLabel: TmmLabel
    Left = 24
    Top = 364
    Width = 174
    Height = 13
    AutoSize = False
    Caption = '(28)Anlaufdauer:'
    FocusControl = laSpeedRamp
    Visible = True
    AutoLabel.Control = laStartupUnit
    AutoLabel.Distance = 40
    AutoLabel.LabelPosition = lpRight
  end
  object laPilotCount: TmmLabel
    Left = 410
    Top = 269
    Width = 25
    Height = 13
    AutoSize = False
    Caption = '4'
    Visible = True
    AutoLabel.Control = laPilotLabel
    AutoLabel.LabelPosition = lpLeft
  end
  object laTimes2: TmmLabel
    Left = 432
    Top = 147
    Width = 39
    Height = 13
    Caption = '(10)fach'
    FocusControl = laDFSSensitivity
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laTimes1: TmmLabel
    Left = 432
    Top = 123
    Width = 39
    Height = 13
    Caption = '(10)fach'
    FocusControl = laSFSSensitivity
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laSFSSensitivity: TmmLabel
    Left = 218
    Top = 123
    Width = 192
    Height = 13
    AutoSize = False
    Caption = '(28)SFS Empfindlichkeit:'
    FocusControl = edSFSSensitivity
    Visible = True
    AutoLabel.Control = laTimes1
    AutoLabel.Distance = 22
    AutoLabel.LabelPosition = lpRight
  end
  object laSpeedUnit: TmmLabel
    Left = 238
    Top = 340
    Width = 29
    Height = 13
    Caption = 'm/min'
    FocusControl = laSpeedLabel
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laStartupUnit: TmmLabel
    Left = 238
    Top = 364
    Width = 5
    Height = 13
    Caption = 's'
    FocusControl = laSpeedRampLabel
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laReductionUnit: TmmLabel
    Left = 448
    Top = 171
    Width = 8
    Height = 13
    Caption = '%'
    FocusControl = laReduction
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laSpeedRamp: TmmLabel
    Left = 200
    Top = 364
    Width = 33
    Height = 13
    AutoSize = False
    Caption = '4'
    Visible = True
    AutoLabel.Control = laSpeedRampLabel
    AutoLabel.LabelPosition = lpLeft
  end
  object laSpeed: TmmLabel
    Left = 200
    Top = 340
    Width = 33
    Height = 13
    AutoSize = False
    Caption = '900'
    Visible = True
    AutoLabel.Control = laSpeedLabel
    AutoLabel.LabelPosition = lpLeft
  end
  object laSensingHeadClass: TmmLabel
    Left = 24
    Top = 388
    Width = 174
    Height = 17
    AutoSize = False
    Caption = '(28)Tastkopf-Klasse'
    FocusControl = cobSensingHeadClass
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLineLabel1: TmmLineLabel
    Left = 12
    Top = 13
    Width = 912
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '(*)Garnspezifisch'
    Distance = 2
  end
  object mmLineLabel2: TmmLineLabel
    Left = 12
    Top = 318
    Width = 912
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '(*)Maschinenspezifisch'
    Distance = 2
  end
  object laAdjustMode: TmmLabel
    Left = 218
    Top = 195
    Width = 190
    Height = 13
    AutoSize = False
    Caption = '(20)Abgleichmodus:'
    FocusControl = cobAdjustMode
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbFDetection: TmmCheckBox
    Tag = 1
    Left = 218
    Top = 49
    Width = 250
    Height = 17
    Caption = '(28)FF Sensor aktiv'
    TabOrder = 0
    Visible = True
    OnClick = DoClickConfigCode
    AutoLabel.LabelPosition = lpLeft
  end
  object cbFAdjustOnOfflimit: TmmCheckBox
    Tag = 2
    Left = 218
    Top = 73
    Width = 250
    Height = 17
    Caption = '(28)FF Abgleich bei Offlimit'
    TabOrder = 1
    Visible = True
    OnClick = DoClickConfigCode
    AutoLabel.LabelPosition = lpLeft
  end
  object cbFAdjustAfterOfflimitAlarm: TmmCheckBox
    Tag = 3
    Left = 218
    Top = 97
    Width = 250
    Height = 17
    Caption = '(28)FF Abgleich nach Offlimit Alarm'
    TabOrder = 2
    Visible = True
    OnClick = DoClickConfigCode
    AutoLabel.LabelPosition = lpLeft
  end
  object cbSeparateSpliceSettings: TmmCheckBox
    Tag = 8
    Left = 354
    Top = 340
    Width = 250
    Height = 17
    Caption = '(28)Gesonderte Spleisspruef-Einstellung'
    TabOrder = 8
    Visible = True
    OnClick = DoClickConfigCode
    AutoLabel.LabelPosition = lpLeft
  end
  object cbFClearingOnSplice: TmmCheckBox
    Tag = 9
    Left = 354
    Top = 364
    Width = 250
    Height = 17
    Caption = '(28)FF Reinigung waehrend Spleisspruefung'
    TabOrder = 9
    Visible = True
    OnClick = DoClickConfigCode
    AutoLabel.LabelPosition = lpLeft
  end
  object cbBunchMonitor: TmmCheckBox
    Tag = 10
    Left = 648
    Top = 340
    Width = 250
    Height = 17
    Caption = '(28)Schlingenueberwachung'
    TabOrder = 11
    Visible = True
    OnClick = DoClickConfigCode
    AutoLabel.LabelPosition = lpLeft
  end
  object cbAdditionalCutOnBreak: TmmCheckBox
    Tag = 11
    Left = 648
    Top = 364
    Width = 250
    Height = 17
    Caption = '(28)Zusatzschnitt bei Fadenbruch'
    TabOrder = 12
    Visible = True
    OnClick = DoClickConfigCode
    AutoLabel.LabelPosition = lpLeft
  end
  object edSFSSensitivity: TKeySpinEdit
    Tag = 4
    Left = 412
    Top = 119
    Width = 15
    Height = 21
    Color = clWindow
    TabOrder = 3
    Text = '1.00'
    Visible = True
    AutoLabel.Control = laSFSSensitivity
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
    ValidateMode = [vmInput]
    OnConfirm = DoConfirm
  end
  object edDFSSensitivity: TKeySpinEdit
    Tag = 5
    Left = 412
    Top = 143
    Width = 15
    Height = 21
    Color = clWindow
    TabOrder = 4
    Text = '2.00'
    Visible = True
    AutoLabel.Control = laDFSSensitivity
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
    ValidateMode = [vmInput]
    OnConfirm = DoConfirm
  end
  object edReduction: TKeySpinEdit
    Tag = 6
    Left = 410
    Top = 167
    Width = 33
    Height = 21
    Color = clWindow
    TabOrder = 5
    Text = '0'
    Visible = True
    AutoLabel.Control = laReduction
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
    ValidateMode = [vmInput]
    OnConfirm = DoConfirm
  end
  object cobSensingHeadClass: TmmComboBox
    Tag = 7
    Left = 200
    Top = 386
    Width = 105
    Height = 21
    Style = csDropDownList
    Color = clWindow
    ItemHeight = 13
    TabOrder = 7
    Visible = True
    OnChange = OnCobChange
    AutoLabel.Control = laSensingHeadClass
    AutoLabel.LabelPosition = lpLeft
    Edit = False
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
  end
  object cobAdjustMode: TmmComboBox
    Tag = 13
    Left = 410
    Top = 191
    Width = 89
    Height = 21
    Style = csDropDownList
    Color = clWindow
    ItemHeight = 13
    TabOrder = 6
    Visible = True
    OnChange = OnCobChange
    AutoLabel.Control = laAdjustMode
    AutoLabel.LabelPosition = lpLeft
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
  end
  object pnConfigCode: TmmPanel
    Left = 12
    Top = 419
    Width = 912
    Height = 49
    Anchors = [akLeft, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 15
    Visible = False
    object laCfgCodeALabel: TmmLabel
      Left = 12
      Top = 24
      Width = 50
      Height = 13
      AutoSize = False
      Caption = '(28)A:'
      FocusControl = laCfgCodeA
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laCfgCodeA: TGBConfigCode
      Left = 64
      Top = 24
      Width = 30
      Height = 13
      AutoSize = False
      Caption = '42405'
      Visible = True
      AutoLabel.Control = laCfgCodeALabel
      AutoLabel.LabelPosition = lpLeft
      ConfigCodeType = ccA
    end
    object laCfgCodeB: TGBConfigCode
      Tag = 1
      Left = 226
      Top = 24
      Width = 30
      Height = 13
      AutoSize = False
      Caption = '42405'
      Visible = True
      AutoLabel.Control = laCfgCodeBLabel
      AutoLabel.LabelPosition = lpLeft
      ConfigCodeType = ccB
    end
    object laCfgCodeBLabel: TmmLabel
      Left = 174
      Top = 24
      Width = 50
      Height = 13
      AutoSize = False
      Caption = '(28)B:'
      FocusControl = laCfgCodeB
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laCfgCodeC: TGBConfigCode
      Tag = 2
      Left = 388
      Top = 24
      Width = 30
      Height = 13
      AutoSize = False
      Caption = '42405'
      Visible = True
      AutoLabel.Control = laCfgCodeCLabel
      AutoLabel.LabelPosition = lpLeft
      ConfigCodeType = ccC
    end
    object laCfgCodeCLabel: TmmLabel
      Left = 336
      Top = 24
      Width = 50
      Height = 13
      AutoSize = False
      Caption = '(28)C:'
      FocusControl = laCfgCodeC
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLineLabel4: TmmLineLabel
      Left = 0
      Top = 2
      Width = 912
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Caption = '(*)Konfigurations Code'
      Distance = 2
    end
  end
  object cbFModeBD: TmmCheckBox
    Tag = 15
    Left = 648
    Top = 388
    Width = 250
    Height = 17
    Caption = '(28)F Anzeige Hell && Dunkel'
    TabOrder = 13
    Visible = True
    OnClick = DoClickConfigCode
    AutoLabel.LabelPosition = lpLeft
  end
  object cbPClearingOnSplice: TmmCheckBox
    Tag = 14
    Left = 354
    Top = 388
    Width = 250
    Height = 17
    Caption = '(28)P Reinigung waehrend Spleisspruefung'
    TabOrder = 10
    Visible = True
    OnClick = DoClickConfigCode
    AutoLabel.LabelPosition = lpLeft
  end
  object mRepetitionEditBox: TRepetitionEditBox
    Left = 24
    Top = 32
    Width = 131
    Height = 281
    Caption = '(12)Wiederholung'
    TabOrder = 16
    CaptionSpace = True
    ReadOnly = False
  end
  object mPExtEditBox: TPExtEditBox
    Left = 528
    Top = 32
    Width = 142
    Height = 75
    Caption = '(15)P Konfiguration'
    TabOrder = 17
    CaptionSpace = True
    ReadOnly = False
  end
  object mbDefault: TmmBitBtn
    Left = 849
    Top = 465
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Default'
    TabOrder = 14
    Visible = False
    OnClick = mbDefaultClick
    AutoLabel.LabelPosition = lpLeft
  end
end
