inherited GBDialogConfigCode: TGBDialogConfigCode
  Left = 1417
  Top = 125
  Caption = 'ConfigCode'
  ClientHeight = 241
  ClientWidth = 191
  Position = poDesigned
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mclbConfigBits: TmmCheckListBox
    Left = 0
    Top = 0
    Width = 191
    Height = 213
    Align = alTop
    Enabled = False
    ItemHeight = 13
    Items.Strings = (
      'Bit 0'
      'Bit 1'
      'Bit 2'
      'Bit 3'
      'Bit 4'
      'Bit 5'
      'Bit 6'
      'Bit 7'
      'Bit 8'
      'Bit 9'
      'Bit 10'
      'Bit 11'
      'Bit 12'
      'Bit 13'
      'Bit 14'
      'Bit 15')
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mbClose: TmmButton
    Left = 5
    Top = 216
    Width = 181
    Height = 24
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Close'
    TabOrder = 1
    Visible = True
    OnClick = mbCloseClick
    AutoLabel.LabelPosition = lpLeft
  end
end
