(*==========================================================================================SP
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMParaDef.pas
| Projectpart...: MillMaster 
| Subpart.......: -
| Process(es)...: -
| Description...: Supports records and definitions for YarnMaster settings
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 31.10.1999  1.00  Kr  | Initial Release
| 03.05.2000  1.10  Kr  | TOptionRec changed. minCount and maxCount replaced by yarnUnit.
| 31.10.2000  1.11  Kr  | TMachineYMConfigRec included
| 30.11.2000  1.12  Kr  | Software option added
| 18.09.2001  1.13 NueKr| TText50 from BaseGlobal to YMParaDef; TYMExtensionRec added.
| 23.01.2002  1.14  Nue | Release 2.03.01 erstellt
|       2005  0.01  Wss | Umbau nach XML
| 26.07.2005  0.01  Wss | cLXSpdGroupLimit hinzugef�gt
| 28.09.2006  0.02  Nue | cMaxGroupMemoryLimit und cLXSpdMemoryLimit hinzugef�gt.
|=========================================================================================*)

//**************************************************
// Last change: Oktober 2004/Wss (V5.00.00)
//**************************************************

unit YMParaDef;

interface

uses
  XMLDef;

const
//------------------------------------------------------------------------------
// Global YM depending constants
//------------------------------------------------------------------------------
  //==== Handler Const; also used by MM ====
  cZESpdGroupLimit  = 12; //Max.12 ProdGrp. per Machine
  //ZE hat keine Memories
  cOldWSCSpdGroupLimit = 6;                   //Max.6 ProdGrp. per Machine
  cWSCSpdGroupLimit = 30;                     //Max.30 ProdGrp. per Machine
  cYMSwVersionWithOldGroupLimit = 'V 7.53';   //MM-20: YM Software v7.53 is a specific version that has only 6 groups 
  //WSC hat keine Memories
  cLXSpdGroupLimit  = 12; //Max.12 ProdGrp. per Machine
  cLXSpdMemoryLimit = 20; //Max.20 Memories per Machine 28.9.06

  //ACHTUNG: cMaxGroupMemoryLimit ist auch die Konstante f�r den Datenrecord, welcher innerhalb des MM
  // verwendet wird. Daher muss gew�hrleistet werden, dass diese Konstante immer den maximal Wert (pro Handler)
  // von Gruppen und Memories zusammen repr�sentieren MUSS.
  cMaxGroupMemoryLimit = cLXSpdGroupLimit+cLXSpdMemoryLimit; //Max.20 Memories per Machine 28.9.06

//------------------------------------------------------------------------------
// Constants for Parmeter Availability
//------------------------------------------------------------------------------
  cSpdlRngNotDefined = $FFFF; // Spindle Range

  //==== Assignement Type ====
  cAssignGroup         = 0; // 0 Assign settings and its prodGrpID to the Group
  cAssignMemory        = 1; // 1 Assign settings to a memory
  cAssignProdGrpIDOnly = 2; // 2 Assign only the prodGrpID to the Group
  //...........................................................................

  //==== SoftwareOption ====
  cSWOBasic   = 0; // Basic functions
  cSWOQuality = 1; // QualityPack
  cSWOIPI     = 2; // Imperfections/IPI
  cSWOLabor   = 3; // LabPack
  cSWOTrial   = 4; // LabPack Trial

type
  TText50 = array[1..50] of Char; // Fuer Informator V5.25  (Nue:18.9.01)
{ DONE -oNue -cXMLSettings : Ersetzten? call von XMLSettings }
//   fSwoEnd has always be the last enumator
  TFrontSwOption = (fSwOBasic, fSwOQuality, fSwOIPI, fSwOLabor, fSwOTrial, fSwOEnd);

const
{ DONE -oNue -cXMLSettings : Ersetzten? call von XMLSettings }
  cFrontSwOption: array[fSwOBasic..fSwOEnd] of string =
    ('Basic functions', 'OualityPack', 'Imperfections/IPI', 'LabPack',
    'LabPack Trial', 'Unknown option'); //ivlm
  //...........................................................................

  //==== Sensing Head ====
  // DONE XML: Zenit Typen Erg�nzen/Anpassen
// Neu in XML.Def
  cFirstTK = 0; // Note! MillMaster is depending on this figures
  cTK730   = cFirstTK; // if anything changes the MM DB must know it!!
  cTK740   = cFirstTK + 1;
  cTK750   = cFirstTK + 2;
  cTK770   = cFirstTK + 3;
  cTK780   = cFirstTK + 4;
  cTK930F  = cFirstTK + 5;
  cTK940BD = cFirstTK + 6;
  cTK930H  = cFirstTK + 7;
  cTK940F  = cFirstTK + 8;
  cTK930S  = cFirstTK + 9;
  cLastTK  = cTK930S;
  //...........................................................................

  //==== Winder Type ====
  // DONE XML: Typen Erg�nzen/Anpassen
// Neu in XML.Def
  cWTUnknownWinder   = 0;
  cWTAC238           = 1;
  cWTEspero          = 2;
  cWTMurIndInv       = 3;
  cWTAC238Sys        = 4;
  cWTAWESS           = 5;
  cWTWSCCConer       = 6;
  cWTAC338           = 7;
  cWTOrion           = 8;

  cWTAC338Spectra    = 9;
  cWTFirstSpectra    = cWTAC338Spectra;
  cWTOrionSpectra    = 10;
  cWTMurSpectra      = 11;
  cWTEsperoSpectra   = 12;
  cWTAWESSSpectra    = 13;
  cWTAC238Spectra    = 14;
  cWTMur21CSpectra   = 15;
  cWTLastSpectra     = cWTMur21CSpectra;

  cWTAC238SysSpectra = $FE;
  cWTDemoWinder      = $FF;

  //==== Speedramp ====
  cMinSpeedRamp = 1; // [s]
  cMaxSpeedRamp = 30; // [s]

  //==== inoperativePara[0] Mask ====
  cIP0MClassClearing     = $00000001; // !Basic Functios (!YM80), ClassClearing
                                        // is available
  cIP0MUpperYarnChannel  = $00000008; // Upper Yarn Channel is available

  cIP0MSpeed             = $00000020; // Speed setting is available
  cIP0MSpeedramp         = $00000040; // Speedramp setting is available

  cIP0MMMMachineConfig   = $00001000; // MillMaster MachineConfigRec is available
                                      // on the FrontZE (is estumated on AC338)

  cIP0MOffCountNegDiaDff = $00040000; // Plus functionality, independing -DiaDiff
                                      // Off Count Monitor is available
  cIP0MOffCountLength    = $00080000; // Plus functionality, Length for Off Count Monitor
  cIP0MCutRetries        = $00000080; // Plus functionality, Cut retries)

  cIP0MSFIMonitor        = $02000000; // SFI Settings is available
  cIP0MSFIRepetitons     = $04000000; // SFI repetitions is available
  cIP0MSFILock           = $08000000; // SFI block on SFI alarm setting is available

  // Default except SS Machines
  cIP0MDefault           = (cIP0MClassClearing or cIP0MUpperYarnChannel or
                            cIP0MOffCountNegDiaDff or cIP0MOffCountLength or cIP0MCutRetries or
                            cIP0MSFIMonitor or cIP0MSFIRepetitons or cIP0MSFILock);

  // Default for SS Machines
  cIP0MSSDefault         = (cIP0MDefault or cIP0MSpeed or cIP0MSpeedramp);
  cIP0MAll               = $FFFFFFFF;

  //==== inoperativePara[1] Mask ====
  cIP1MFFClearing      = $00000001; // FF Clearing is available
  cIP1MFFCluster       = $00000002; // FF Cluster Settings is available
  cIP1MFFSensorEnabled = $00000004; // FF Senor is active
  cIP1MFFSensingHeads  = $00000F00; // FF SensingHead Bit's

  // Default
  cIP1MDefault         = 0;
  // Default for FF TK
  cIP1MFFDefault       = (cIP1MDefault or cIP1MFFClearing or cIP1MFFCluster or cIP1MFFSensorEnabled);
  cIP1MAll             = $FFFFF0FF;
  //...........................................................................

//------------------------------------------------------------------------------
// Constants for Channel Settings
//------------------------------------------------------------------------------
  cChOn       = 2;
  cChOff      = 3;
  cChExtSp    = 6; // Channel Switch
// ein cChannelOff ist ebenfalls in YMParaUtils definiert aber als Record-Konstante
//wss  cChannelOff = $FFFF; // value for Diameter channels to switch them off

  cNepsDiaMin       = 1.50;
  cNepsDiaMax       = 7.00;

  cShortDiaMin      = 1.10; // Faktor
  cShortDiaMax      = 4.00; // Faktor
  cShortLenMin      = 1;    // [cm]
  cShortLenMax      = 10;   // [cm]
  cShortLenMaxMur   = 9.9;  // [cm], max Short Length for Murata Inside only

  cLongDiaMin       = 1.04; // Faktor
  cLongDiaMax       = 2.00; // Faktor
  cLongLenMin       = 6;    // [cm]
  cLongLenMax       = 200;  // [cm]

  cThinDiaMin       = 0.40; // Faktor
  cThinDiaMaxAbove  = 0.94; // Faktor
  cThinDiaMaxBelow  = 0.80; // Faktor
  cThinLenMin       = 2;    // [cm]
  cThinLenThreshold = 5;    // [cm]
  cThinLenMax       = 200;  // [cm]

  cDiaMin           = cThinDiaMin;
  cDiaMax           = cNepsDiaMax;
  cLenMin           = cShortLenMin;
  cLenMax           = cLongLenMax;

  cMinNepsDia_ = 150; // min Nep Diameter, [%]
  cMaxNepsDia_ = 700; // max Nep Diameter, [%]

  cMinShortDia_ = 110; // min Short Diameter, [%]
  cMaxShortDia_ = 400; // max Short Diameter, [%]
  cMinShortLen_ = 10; // min Short Length, [mm]
  cMaxShortLen_ = 100; // max Short Length, [mm]
  cMaxMurShortLen_ = 99; // max Short Length for Murata Inside only, [mm]

  cMinLongDia_ = 104; // min Long Diameter, [%]
  cMaxLongDia_ = 200; // max Long Diameter, [%]
  cMinLongLen_ = 60; // min Long Length, [mm]
  cMaxLongLen_ = 2000; // max Long Length, [mm]

  cMinThinDia_ = 40; // min Thin Diameter, [%]
  cMaxThinDiaAbove_ = 94; // max Thin Diameter above thin length treshold, [%]
  cMaxThinDiaBelow_ = 80; // max Thin Diameter below thin length treshold, [%]

  cMinThinLen_ = 20; // min Thin Length, [mm]
  cThinLenThreshold_ = 50; // Thin Length threshold, [mm]
  cMaxThinLen_ = 2000; // max Thin Length, [mm]

  cMinDia_ = cMinThinDia_;
  cMaxDia_ = cMaxNepsDia_;
  cMinLen_ = cMinShortLen_;
  cMaxLen_ = cMaxLongLen_;

  //==== Winding Speed ====
  cMinSpeed     = 20; // min speed   20m/Min
  cMaxSpeed     = 1600; // max speed 1600m/Min
  cDefaultSpeed = 900; // [m/Min]
  //.........................................................................

//------------------------------------------------------------------------------
// Constants for Splice Monitor
//------------------------------------------------------------------------------
  cUpperYarnMin            = cLongDiaMin;
  cUpperYarnMax            = cLongDiaMax;

  cSpliceLenMin            = 6;   // [cm]
  cSpliceLenMax            = 200; // [cm]
  cSpliceLenMaxMur         = 99;  // [cm]

  cSpliceCheckMin          = 150;  // [cm]
  cSpliceCheckMax          = 4000; //1200; // [cm]
  cSpliceCheckLongLength   = 127;  // [cm]
  cSpliceCheckModValue     = 32;
  cSpliceCheckOff          = 0;
  cSpliceCheckNotInit      = $FF;
  cSpliceCheckDefault      = 25; // [cm]
  cSpliceCheckDefaultAC338 = 35; // [cm]
  cSpliceCheckDefaultMur   = 50; // [cm]

  cMinUpperYarn_            = cMinLongDia_; // min UpperYarn Diameter, [%]
  cMaxUpperYarn_            = cMaxLongDia_; // max UpperYarn Diameter, [%]

  cMinSpliceLen_            = 60; // min Splice Length, [mm]
  cMaxSpliceLen_            = 2000; // max Splice Length, [mm]
  cMaxMurSpliceLen_         = 990; // max Splice Length for Murata Inside only, [mm]

  cMinSpliceCheck_          = 150; // min Splice Check length [cm]
  cMaxSpliceCheck_          = 1200; // max Splice Check length [cm]
  cSpliceCheckOff_          = 0; // Seperate Splice Check Off
  cSpliceCheckNotInit_      = $FF;
  cDefaultSpliceCheck_      = 25; // Default Splice Check length 25 [cm]
  cDefaultAC338SpliceCheck_ = 35; // Default Splice Check length for AC338 35 [cm]
  cDefaultMurSpliceCheck_   = 50; // Default Splice Check length for MURATA 50[cm]
  //...........................................................................

//------------------------------------------------------------------------------
// Constants for Additional Settings
//------------------------------------------------------------------------------
  //==== Config Code Bit Mask ====

  // Config Code A Bits
  cCCAFFSensorActiveBit                = Word($0001);
  cCCADFSSensitivity1Bit               = Word($0002);
  cCCASFSSensitivityBit                = Word($0004);
  cCCANoFFClearingOnSpliceBit          = Word($0008);
  cCCAZeroTestMonitorDisabledBit       = Word($0010);
  cCCANoBunchMonitorBit                = Word($0020);
  cCCAOneDrumPulsBit                   = Word($0080);
  cCCAKnifePowerHighBit                = Word($0100); // AC338 only
  cCCARemoveYarnAfterAdjustBit         = Word($0200); // AC338 and Orion only
  cCCAFFBlockDisabledBit               = Word($0400);
  cCCADriftCompensationOnConeChangeBit = Word($0800);

  cCCACutFailBlockDisabledBit          = Word($2000);
  cCCAClusterBlockDisabledBit          = Word($4000);
  cCCACountBlockDisabledBit            = Word($8000);

  // Config Code B Bits
  cCCBFFBDSensorActiveBit              = Word($0002);
  cCCBNoFFAdjAtOfflimitBit             = Word($0004);
  cCCBMurataExtendedItfBit             = Word($0008);
  cCCBFFAdjAfterAlarmBit               = Word($0010);
  cCCBCutBeforeAdjustBit               = Word($0020);

  // Config Code C Bits
  cCCBUpperYarnCheckBit                = Word($0001);
  cCCBDFSSensitivity2Bit               = Word($0040);
  cCCBCutOnYarnBreakBit                = Word($0080);
  cCCBConeChangeDetectionDisabledBit   = Word($0100);
  cCCBKnifeMonitorBit                  = Word($0200);
  cCCBHeadstockRightBit                = Word($8000);
  cCCCSFIBlockEnabledBit               = Word($0001);
  cCCCFFClusterBlockEnabledBit         = Word($0002);
  //...........................................................................

  //==== Config Code Mask AC 338 ====

  cCCA338Machine: Word    = $2F11; // Machine depended:     0010111100010001
  cCCA338Production: Word = $002F; // Production dependent: 0000000000101111
  cCCA338Diagnosis: Word  = $1040; // Diagnosis dependent:  0001000001000000
  cCCA338Unused: Word     = $C080; // Not used:             1100000010000000

  cCCB338Machine: Word    = $0023; // Machine depended:     0000000000100011
  cCCB338Production: Word = $00D6; // Production dependent: 0000000011010110
  cCCB338Diagnosis: Word  = $0000; // Diagnosis dependent:  0000000000000000
  cCCB338Unused: Word     = $FF08; // Not used:             1111111100001000

  cCCC338Machine: Word    = $0000; // Machine depended:     0000000000000000
  cCCC338Production: Word = $0000; // Production dependent: 0000000000000000
  cCCC338Diagnosis: Word  = $0000; // Diagnosis dependent:  0000000000000000
  cCCC338Unused: Word     = $FFFF; // Not used:             1111111111111111
  //...........................................................................

 //                       = == Config Code Mask All others machine ====

  cCCAMachine: Word       = $EC91; // Machine depended:     1110110010010001
  cCCAProduction: Word    = $002F; // Production dependent: 0000000000101111
  cCCADiagnosis: Word     = $1040; // Diagnosis dependent:  0001000001000000
  cCCAUnused: Word        = $0300; // Not used:             0000001100000000

  cCCBMachine: Word       = $832B; // Machine depended:     1000001100101011
  cCCBProduction: Word    = $00D6; // Production dependent: 0000000011010110
  cCCBDiagnosis: Word     = $0000; // Diagnosis dependent:  0000000000000000
  cCCBUnused: Word        = $7C00; // Not used:             0111110000000000

  cCCCMachine: Word       = $0003; // Machine depended:     0000000000000011
  cCCCProduction: Word    = $0000; // Production dependent: 0000000000000000
  cCCCDiagnosis: Word     = $0000; // Diagnosis dependent:  0000000000000000
  cCCCUnused: Word        = $FFFC; // Not used:             1111111111111100
  //...........................................................................

  //==== Config Code Mask Universal ====

  cCCAUniversalProduction: Word = $002F;
  cCCBUniversalProduction: Word = $00D6;
  cCCCUniversalProduction: Word = $0000;

  //...........................................................................

  //==== Config Code Mask Default ====
  cCCADefault = Word($001A);
  cCCBDefault = Word($0020);
  cCCCDefault = Word($0003);

  // Savio Inside Espero / Orion
  cCCASIDefault = Word($001A);
  cCCBSIDefault = Word($0020);
  cCCCSIDefault = Word($0000);

  // Murata inside
  cCCAMIDefault = Word($E41A);
  cCCBMIDefault = Word($0008);
  cCCCMIDefault = Word($0000);

  // AC338
  cCCAAC338Default = Word($201A);
  cCCBAC338Default = Word($0020);
  cCCCAC338Default = Word($0000);
  //...........................................................................

(*
 //==== Config Code Mask AC 338 ====
  cCCA338Machine = Word($0000);         // Machine depended:     0
  cCCA338Production = Word($0001);      // Production dependent: 0
  cCCA338Diagnosis = Word($0000);       // Diagnosis dependent:  0
  cCCA338Unused = Word($FFFF);          // Not used:             0

  cCCB338Machine = Word($0000);         // Machine depended:     1
  cCCB338Production = Word($0000);      // Production dependent: 0
  cCCB338Diagnosis = Word($0000);       // Diagnosis dependent:  0
  cCCB338Unused = Word($FFFF);          // Not used:             0

  cCCC338Machine = Word($0000);         // Machine depended:     1
  cCCC338Production = Word($0000);      // Production dependent: 0
  cCCC338Diagnosis = Word($0000);       // Diagnosis dependent:  0
  cCCC338Unused = Word($FFFF);          // Not used:             0

 //==== Config Code Mask All others machine ====
  cCCAMachine = Word($EC91);            // Machine depended:     1110110010010001
  cCCAProduction = Word($002F);         // Production dependent: 0000000000101111
  cCCADiagnosis = Word($1040);          // Diagnosis dependent:  0001000001000000
  cCCAUnused = Word($0300);             // Not used:             0000001100000000

  cCCBMachine = Word($832B);            // Machine depended:     1000001100101011
  cCCBProduction = Word($00D6);         // Production dependent: 0000000011010110
  cCCBDiagnosis = Word($0000);          // Diagnosis dependent:  0000000000000000
  cCCBUnused = Word($7C00);             // Not used:             0111110000000000

  cCCCMachine = Word($0000);            // Machine depended:     1
  cCCCProduction = Word($0000);         // Production dependent: 0
  cCCCDiagnosis = Word($0000);          // Diagnosis dependent:  0
  cCCCUnused = Word($FFFF);             // Not used:             0
*)

  //==== Yarn Count Monitor ====
  cDiaDiffMin = 3.0;  // [%]
  cDiaDiffMax = 44.0; // [%]
  cMinDiaDiff_ = 30;  // min Diameter Difference [%]
  cMaxDiaDiff_ = 440; // max Diameter Difference [%]

  // Yarncount neu als float
  cYarnCountMin    = 0.4;
  cYarnCountMax    = 3200.0;
  cYarnCountInit   = 100.0;
  cFineCountInit   = cYarnCountInit + 0.1;
  cCoarseCountInit = cYarnCountInit - 0.1;

  // Yarncount * 10 is stored
  cMinYarnCount_    = 20;
  cMaxYarnCount_    = 32000;
  cInitYarnCount_   = 1000;
  cInitFineCount_   = cInitYarnCount_ + 1;
  cInitCoarseCount_ = cInitYarnCount_ - 1;

// #define MIN_CNT_DIFF	(DIV_BY_10_RO((MIN_DIA_DIFF+100)*(MIN_DIA_DIFF+100)/10)-100)		// [%]
// #define MAX_CNT_DIFF	(DIV_BY_10_RO((MAX_DIA_DIFF+100)*(MAX_DIA_DIFF+100)/10)-100)	  // [%]

  cMaxYCCheckLength     = 5; // max Yarn count check length, MAX_YC_CHECK_LENGTH*10m
  cDefaultYCCheckLength = 1;

  cMinRepetitions        = 0;
  cMaxRepetitions        = 9; //5;
  cMaxSynRepetitions     = 9;
  cDefaultClusterRep     = 1; // Default Cluster Repetition = 0 (epetitions+1: 1..6)
  cDefaultSiroStartupRep = 6; // Default Siro Startup Repetition = 5 (Repetitions+1: 1..6)
  cDefaultRepetitions    = 3; // Default Repetition = 2 (Repetitions+1: 1..6)
  cDefaultCutRetries     = 1; // Default Cut Retries = 0 (Repetitions+1: 1..6)

  //==== Fault Cluster Monitor ====
//  cClusterDiaMin = 1.05; // min Cluster Diameter
//  cClusterDiaMax = 4.00; // max Cluster Diameter

  cClShortDiaMin      = 1.05; // Faktor
  cClShortDiaMax      = 4.00; // Faktor
  cClShortLenMin      = 1;    // [cm]
  cClShortLenMax      = 10;   // [cm]

  cClLongDiaMin       = 1.04; // Faktor
  cClLongDiaMax       = 2.00; // Faktor
  cClLongLenMin       = 6;    // [cm]
  cClLongLenMax       = 200;  // [cm]

  cClThinDiaMin       = 0.40; // Faktor
  cClThinDiaMaxAbove  = 0.94; // Faktor Thin Cluster Len >  5 cm
  cClThinDiaMaxBelow  = 0.80; // Faktor Thin Cluster Len <= 5 cm
  cClThinLenMin       = 2;    // [cm]
  cClThinLenThreshold = 5;    // [cm]
  cClThinLenMax       = 200;  // [cm]

  cClOffObsLength        = 0;    // Cluster Off (Observation Length)
  cClObsLengthMin        = 0.1;  // min Observation Length [m]
  cClObsLengthMax        = 80.0; // max Observation Length [m]
  cClObsLengthDeault     = 30.0; // Default Observation Length [m]

  cDefectsMin = 1;    // min Faults
  cDefectsMax = 9999; // max Faults

  //==== Fine Adjust, Cone Startup ====
  cInitFadjSetting    = 0;
  cDefaultFadjSetting = cInitFadjSetting;
  cMinFadjSettings    = 0;
  cMaxFadjSettings    = 250;
  cFineAdjSettingsOff    = $FF; // Adjust Offlimit
 //.........................................................................
  cMinDFSDelta = 0;
  cMaxDFSDelta = 250;
 //.........................................................................

//------------------------------------------------------------------------------
// Constants for Machine set
//------------------------------------------------------------------------------
  //==== Long Stop ====
  cMinLongStopDef  = 1; // Minimal long stop definition, [1/10Min.]
  cMaxLongStopDef  = 300; // Maximal long stop definition, [1/10Min.]
  cInitLongStopDef = 50; // Initial value of long stop definition, [1/10Min.]

  //==== Data Record Mode, lengthMode ====
  cDRMFirst = 6;
  cDRMLast  = 7;
  cDRMCone  = 8;

  //==== YM-Type declarations ====
  // DONE XML: Typen Erg�nzen/Anpassen
// Neu in XML.Def
  cYM80           = 0;
  cYM800          = cYM80 + 1;
  cYM900          = cYM80 + 2;
  cYM80I          = cYM80 + 3;
  cYM800I         = cYM80 + 4;
  cYM900I         = cYM80 + 5;
  cYMX00          = 20;
  cYMX00I         = cYMX00 + 1;
  cYM_NOT_DEFINED = cYMX00 + 2;
  cYM_UNKNOWN     = cYMX00 + 3;
  //...........................................................................

//----------------------------------------------------------------------------
// Constants for SFI Monitor
//----------------------------------------------------------------------------

  cSFIRefFloat       = 0;     // Float, Constant=0
  cSFIRefMin         = 5.0;   // Reference  5.0
  cSFIRefMax         = 25.0;  // Reference 25.0

  cSFILimitOff       = 0;     // +Limit, -Limit = OFF
  cSFIDiffUpMin      = 5; //0.05;  // +Limit 5   %
  cSFIDiffLwMin      = 5; //0.05;  // -Limit 5   %
  cSFIDiffUpMax      = 40; //0.40;  // +Limit 40   %
  cSFIDiffLwMax      = 40; //0.40;  // -Limit 40   %

  cSFIRepetitionsMax = cMaxRepetitions; //  SFI Repetitions (5)

  cFloatRefSFI_       = 0; // Float, Constant=0
  cMinSFIRef_         = 500; // Reference  5.0
  cMaxSFIRef_         = 2500; // Reference 25.0

  cSFILimitOff_       = 0; // +Limit, -Limit = OFF
  cMinSFIDiffUp_      = 50; // +Limit 5   %
  cMinSFIDiffLw_      = 50; // -Limit 5   %
  cMaxSFIDiffUp_      = 400; // +Limit 40   %
  cMaxSFIDiffLw_      = 400; // -Limit 40   %

  cMaxSFIRepetitions_ = cMaxRepetitions; //  SFI Repetitions (5)
  //...........................................................................

//----------------------------------------------------------------------------
// Constants for VCV Monitor
//----------------------------------------------------------------------------

  cVCVLimitOff       = 0;     // +Limit, -Limit = OFF
  cVCVDiffUpMin      = 5; //0.05;  // +Limit 5   %
  cVCVDiffLwMin      = 5; //0.05;  // -Limit 5   %
  cVCVDiffUpMax      = 100; //0.40;  // +Limit 40   %
  cVCVDiffLwMax      = 100; //0.40;  // -Limit 40   %

  cVCVOffObsLength        = 0;    // VCV Off (Observation Length)
  cVCVObsLengthMin        = 1;  // min Observation Length [m]
  cVCVObsLengthMax        = 50; // max Observation Length [m]
  cVCVObsLengthDefault    = 1; // Default Observation Length [m]

  cVCVRepetitionsMax = cMaxRepetitions; //  VCV Repetitions (9)

  cVCVLimitOff_       = 0; // +Limit, -Limit = OFF
  cMinVCVDiffUp_      = 50; // +Limit 5   %
  cMinVCVDiffLw_      = 50; // -Limit 5   %
  cMaxVCVDiffUp_      = 400; // +Limit 40   %
  cMaxVCVDiffLw_      = 400; // -Limit 40   %

  cMaxVCVRepetitions_ = cMaxRepetitions; //  VCV Repetitions (9)
  //...........................................................................

//----------------------------------------------------------------------------
// Constants for P Monitor
//----------------------------------------------------------------------------
  cPLimitMin     = 0.5;
  cPLimitMax     = 9.9;
  cPRefLengthMin = 0.5;
  cPRefLengthMax = 4;
  cPLowLimitMin  = 0;   // [%]
  cPLowLimitMax  = 100; // [%]
  cPLowBitMin    = 0;
  cPLowBitMax    = 4;
  cPReductionMin = 0;   // [%]
  cPRecuctionMax = 100; // [%]

type
//------------------------------------------------------------------------------
// Parmeter Availability records, 30 byte
//------------------------------------------------------------------------------
  //...........................................................................
  TSpdlRangeList = (Spec, Range);

  TSpindleRangeRec = packed record
    case TSpdlRangeList of
      Spec:  (contents: Word);
      Range: (start: Byte; // [1..MaxSpindleOfProfGrp]
              stop: Byte); // [start..MaxSpindleOfProfGrp]
  end;
  //...........................................................................

  TAvailabilityRec = packed record
    assignement: Byte;
    restart: Byte;
    swOption: Byte;
    group: Byte;
    prodGrpID: Integer;
    spdl: TSpindleRangeRec;
    pilotSpindles: Byte;
    sensingHead: Byte;
    machType: Byte;
    speedRamp: Byte;
    inoperativePara: array[0..1] of Integer;
    voidData: Integer;
    spare: Integer;
  end;
  
  //...........................................................................

//------------------------------------------------------------------------------
// Channel Settings records, 30 byte
//------------------------------------------------------------------------------
  //...........................................................................
  TDiameterRec = packed record
    dia: Single;
    sw: TSwitch;
//    sw: Word;
  end;
  
  TLengthRec = packed record
    len: Single;
    sw: Word;
  end;
  
  //...........................................................................

  TSettingsList = (Base, Splice);

  TChannelSettingsRec = packed record
    neps: TDiameterRec; // neps Diameter, (150..700/Off), [1.5..7.0]
    short: TDiameterRec; // short Diameter, (110..400/Off), [1.5..4.0]
    shortLen: Single; //short Length, [1.0..10.0 cm]
    long: TDiameterRec; // long Diameter, (110..200/Off), [1.1..2.0]
    longLen: Single; //long Length, [6.0..200.0 cm]
    thin: TDiameterRec; // thin Diameter, (40..90/Off), [-10..-60 %]
    thinLen: Single; //long Length, [6.0..200.0 cm]
    case TSettingsList of
      Base:   (splice: TLengthRec; // splice Length, (60..2000/LL), [6.0..200.0 cm]
               speed: Word; // winding speed, (500..1200), [500..1200 m/Min]
               spare: Word);
      Splice: (upperYarn: TDiameterRec; // upper yarn Diameter, (110..200/Off), [1.1..2.0]
               checkLen: Word; // splice check Length, (0..1200), [0..120 cm]
               spare_: Word);
  end; // 30 byte
//  TChannelSettingsRec = packed record
//    neps: TDiameterRec; // neps Diameter, (150..700/Off), [1.5..7.0]
//    short: TDiameterRec; // short Diameter, (110..400/Off), [1.5..4.0]
//    shortLen: Word; // short Length, (10..100), [1.0..10.0 cm]
//    long: TDiameterRec; // long Diameter, (110..200/Off), [1.1..2.0]
//    longLen: Word; // long Length, (60..2000), [6.0..200.0 cm]
//    thin: TDiameterRec; // thin Diameter, (40..90/Off), [-10..-60 %]
//    thinLen: Word; // long Length, (60..2000), [6.0..200.0 cm]
//    case TSettingsList of
//      Base:   (splice: TLengthRec; // splice Length, (60..2000/LL), [6.0..200.0 cm]
//               speed: Word; // winding speed, (500..1200), [500..1200 m/Min]
//               spare: Word);
//      Splice: (upperYarn: TDiameterRec; // upper yarn Diameter, (110..200/Off), [1.1..2.0]
//               checkLen: Word; // splice check Length, (0..1200), [0..120 cm]
//               spare_: Word);
//  end; // 30 byte

//------------------------------------------------------------------------------
// Class Clear Settings, 16 byte
//------------------------------------------------------------------------------
  TClassClearSettingsArr = packed array[0..15] of Byte;

//------------------------------------------------------------------------------
// FF Clear Settings, 8 byte
//------------------------------------------------------------------------------
  TSiroClearSettingsArr = packed array[0..7] of Byte;

  TColorClearSettingsArr = packed array[0..15] of Byte;

//------------------------------------------------------------------------------
// Additional Settings records, 46 byte
//------------------------------------------------------------------------------
  TOptionRec = packed record
    diaDiff: Word;
    yarnCnt: Word;
    yarnUnit: Word;
    threadCnt: Word;
    cntRep: Byte;
    siroStartupRep: Byte;
    clusterDia: Single;
    clusterLength: Single;
    clusterDefects: Word;
  end;
  
  //...........................................................................

  TExtendedOptionRec = packed record
    negDiaDiff: Word;
    countLength: Byte;
    clusterRep: Byte;
    cutRetries: Byte;
    fill: Byte;
    spare: array[0..1] of Integer;
  end;
  
  //...........................................................................
  TAdditionalRec = packed record
    option: TOptionRec;
    configA: Word;
    configB: Word;
    configC: Word;
    fAdjConeReduction: Byte;
    adjRequest: Byte;
    spare: array[0..1] of Integer;
    extOption: TExtendedOptionRec;
  end;
  
//------------------------------------------------------------------------------
// Machine Set record, 34 byte
//------------------------------------------------------------------------------
const
  cMaxMachNameStrSz = 10;
  cMaxVersiomStrSz  = 8;
  //...........................................................................

type
  TMachBezArr     = array[0..cMaxMachNameStrSz - 1] of Byte;
  TYMSWVersionArr = array[0..cMaxVersiomStrSz - 1] of Byte;
  //...........................................................................
  TMachineSetRec = packed record
    machBez: TMachBezArr;
    longStopDef: Word;
    lengthWindow: Word;
    lengthMode: Byte;
    yMType: Byte;
    spare: array[0..1] of Integer;
    reinVer: Byte;
    reinTech: Byte;
    yMSWVersion: TYMSWVersionArr;
  end;
  
//------------------------------------------------------------------------------
// SFI (Surface Index) Settings record, 8 byte
//------------------------------------------------------------------------------
  TSFIParaRec = packed record
    absRef: Word;
    upper: Single;
    lower: Single;
    rep: Byte;
    fill0: Byte;
  end;
  
//------------------------------------------------------------------------------
// FF Cluster Settings record, 14 byte
//------------------------------------------------------------------------------
  TFFClusterRec = packed record
    obsLength: Single;
    defects: Word;
    rep: Byte;
    fill0: Byte;
    siroClear: TSiroClearSettingsArr;
  end;
  
//------------------------------------------------------------------------------
// Spare Set record, 24 bytes
//------------------------------------------------------------------------------
  //...........................................................................

  TSpareSetRec = packed record
    fill: array[0..5] of Integer;
  end;
  
//------------------------------------------------------------------------------
// YM Settings (telegramsubID: SID_SETTINGS), 240 byte
//------------------------------------------------------------------------------
  TYMSettingsRecX = packed record
    Available: TAvailabilityRec;
    Channel: TChannelSettingsRec;
    Splice: TChannelSettingsRec;
    ClassClear: TClassClearSettingsArr;
    SiroClear: TSiroClearSettingsArr;
    Additional: TAdditionalRec;
    MachSet: TMachineSetRec;
    SFI: TSFIParaRec;
    FFCluster: TFFClusterRec;
    Spare: TSpareSetRec;
  end;
  PYMSettingsRec = ^TYMSettingsRecX;
//------------------------------------------------------------------------------
// YM Settings and extension fields
//------------------------------------------------------------------------------
  TYMExtensionRec = packed record
    YM_Set_Name: TText50;
    YMSettings: TYMSettingsRecX;
  end;
  
//------------------------------------------------------------------------------
// General definitions and default initialisation
//------------------------------------------------------------------------------
{
const
  cTopSixSettings: TChannelSettingsRec = (
    neps:     (dia: 4.0; sw: cChOn);
    short:    (dia: 2.2; sw: cChOn);
    shortLen: 1.3;
    long:     (dia: 1.2; sw: cChOn);
    longLen:  10.0;
    thin:     (dia: 0.9; sw: cChOn);
    thinLen:  45.0;
    splice:   (len: 20.0; sw: cChOn);
    checkLen: 25;
    spare:    0;
  );
{}

type
//------------------------------------------------------------------------------
// Byte buffer for YM Settings
//------------------------------------------------------------------------------
  TYMSettingsByteArr = array[1..400] of Byte; // Has to be bigger then size of
                                              // YMParaDef.TYMSettingsRec (actual 240B)
  PYMSettingsByteArr = ^TYMSettingsByteArr;

//------------------------------------------------------------------------------
// Pointer to YM Settings
//------------------------------------------------------------------------------
  PYMSettings = ^TYMSettingsRecX;

//------------------------------------------------------------------------------
// Data rules for not available data or data needs to process
//------------------------------------------------------------------------------
  TYMDataRulesRec = record
    voidData: Integer;
  end;
  
//------------------------------------------------------------------------------
// Void Data Items
//------------------------------------------------------------------------------
  TVoidDataItem = (vdCones, // Availability of Cones
                   vdCops, // Availability of Cops
                   vdIPOld, // Availability of IPOld
                   vdIPNew, // dito
                   vdIPPlus, // dito
                   vdSFIVar, // dito
                   vdSFICnt); // dito

//------------------------------------------------------------------------------
// Machine dependent parameter
// Old machine config record
//------------------------------------------------------------------------------
  TMachineYMParaRec = record
    sensingHead: Byte;
    machType: Byte;
    checkLen: Word;
    cutRetries: Byte;
    configA: Word;
    configB: Word;
    configC: Word;
    inoperativePara: array[0..1] of Integer;
    machBez: TMachBezArr;
    longStopDef: Word;
    yMType: Byte;
    yMSWVersion: TYMSWVersionArr;
  end;
  
//------------------------------------------------------------------------------
// Machine dependent parameter
// New machine config record
//------------------------------------------------------------------------------
  // DONE XML: Typen Erg�nzen/Anpassen
// Neu in XML.Def
  TAWEMachType = (amtUnknown, amtAC238, amtESPERO, amtMUR_IND_INV, amtAC238_SYS,
                  amtAWE_SS, amtWSC_C_CONER, amtAC338, amtOrion, amtMurata21C, amtDemoWinder);
  //...........................................................................
//wss XMLDef  TFrontType = (ftUnknown, ftZE80, ftZE80i, ftZE800, ftZE800i, ftSWSInformatorSBC5);
  //...........................................................................
  TAWEType = (atUnknown, atAWE800, atAWESpectra);
  //...........................................................................
  (* Note: When adding new sensing heads don't forget to check out the const:
  cFirstTK up to cLastTK and , the set: cOtherSensingHeadTypes, cFFSensingHeadFTypes
  and cFFSensingHeadTypes, the method: TYMSettingsUtils.ExtractMachineYMConfig.ExtractSensingHead
  and TYMMachineConfig.SetBuildMaxMachineConfig.
   *)
  // DONE XML: Zenit Typen Erg�nzen/Anpassen
// Neu in XML.Def
  TSensingHeadType = (htTK830, htTK840, htTK850, htTK870, htTK880, htTK930F, htTK940BD,
                      htTK930H, htTK940F, htTK930S);

  //...........................................................................
//  PTSensingHeadClass = ^TSensingHeadClass;
//  TSensingHeadClass  = (hcTK8xx, hcTK9xx, hcTK9xxH, hcTK9xxBD);
  //...........................................................................

const
//  cFirstSensingHead   = Ord(htTK830);
//  cLastSensingHead    = Ord(htTK930S);
//  cFirstFFSensingHead = Ord(htTK930F);

//  cOtherSensingHeadTypes: set of TSensingHeadType =
//    [htTK830, htTK840, htTK850, htTK870, htTK880];
  //...........................................................................
//  cFFSensingHeadTypes: set of TSensingHeadType =
//    [htTK930F, htTK940BD, htTK930H, htTK940F, htTK930S];
  //...........................................................................
//  cFFSensingHeadFTypes: set of TSensingHeadType =
//    [htTK930F, htTK940F, htTK930S];
  //...........................................................................
//  cFFSensingHeadBDTypes: set of TSensingHeadType =
//    [htTK940BD];
  //...........................................................................
  cAWEMachTypNames: array[amtUnknown..amtDemoWinder] of string =
    ('Unknown winder', 'AC238', 'ESPERO', 'MUR_IND_INV', 'AC238_SYS', 'AWE_SS',
     'WSC_C_CONER', 'AC338', 'Orion', 'Muarata 21C', 'DemoWinder'); //ivlm
  //...........................................................................
  //  NOTE: cWTAC238SysSpectra = $FE and cWTDemoWinder = $FF has to have a special handling !!
  cAWEMachTypXRefTbl: array[cWTUnknownWinder..cWTMur21CSpectra] of TAWEMachType =
    (amtUnknown, amtAC238, amtESPERO, amtMUR_IND_INV, amtAC238_SYS, amtAWE_SS,
     amtWSC_C_CONER, amtAC338, amtOrion, amtAC338, amtOrion, amtMUR_IND_INV,
     amtESPERO, amtAWE_SS, amtAC238, amtMurata21C);
  //...........................................................................
//wss  cFrontTypeNames: array[ftUnknown..ftSWSInformatorSBC5] of string =
//    ('Unknown front type', 'ZE80', 'ZE80inside', 'ZE800', 'ZE800inside',
//    'SWS Informator'); //ivlm
  //...........................................................................
//  cSensingHeadTypeNames: array[htTK830..htTK930S] of string =
//    ('TK830', 'TK840', 'TK850', 'TK870', 'TK880', 'TK930F', 'TK940BD',
//    'TK930H', 'TK940F', 'TK930S'); //ivlm
  //...........................................................................
//  cSensingHeadTypeOrder: array[htTK830..htTK930S] of Integer =
//    (6, 7, 8, 9, 10, 2, 5, 3, 4, 1);
  //...........................................................................
//wss  cSensingHeadClassNames: array[hcTK8xx..hcTK9xxBD] of string =
//    ('TK8xx', 'TK9xxF/S', 'TK9xxH', 'TK9xxBD'); //ivlm
  //...........................................................................
//  cAWETypeNames: array[atUnknown..atAWESpectra] of string =
//    ('Unknown AWE type', 'AWE800', 'AWESpectra'); //ivlm
  //...........................................................................
type
  TGroupSpecificMachConfigRec = record
    spindle: TSpindleRangeRec;
    aWEType: TAWEType;
    sensingHead: TSensingHead;
    configA: Word;
    configB: Word;
    configC: Word;
    inoperativePara: array[0..1] of Integer;
  end;
  
  //...........................................................................
//NUE1
//  TMachineYMConfigRec = record
//    spec: array[0..cZESpdGroupLimit-1] of TGroupSpecificMachConfigRec;
//    checkLen: Word;
//    cutRetries: Byte;
//    longStopDef: Word;
//    defaultSpeedRamp: Byte;
//    defaultSpeed: Word;
//    frontType: TFrontType;
//    frontSWOption: TFrontSwOption;
//    frontSWVersion: TYMSWVersionArr;
//    MachineType: TMachineType;
////NUE1    aWEMachType: TAWEMachType;
//    machBez: TMachBezArr;
//    inoperativePara: array[0..1] of Integer;
//  end;

//------------------------------------------------------------------------------
// Production group dependent parameter
//------------------------------------------------------------------------------
//NUETMP
//  TProdGrpYMParaRec = record
//    group: Byte; // neu: 0-basiert
////NUETMP    prodGrpID: Integer;
//    spdl: TSpindleRangeRec;
//    pilotSpindles: Byte;
//    speedRamp: Byte;
//    speed: Word;
//    yarnCnt: Word;
//    yarnCntUnit: Byte;
//    nrOfThreads: Byte;
//    lengthWindow: Word;
//    lengthMode: Byte;
//  end;
  
implementation
uses
  mmMBCS;

end.

