(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SelectionLayer.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr | Initial Release
|       2005  0.01  Wss | Umbau nach XML
| 21.03.2006  0.01  Wss | GetFieldStateTable(): bei Nicht-Zenit-F-Matrix nicht mehr pauschal
                          auf 128 Zeichen auff�llen, da das Array bereits 128 Zeichen enth�lt
| 04.06.2008  0.02  Nue | In SetMatrixSubType handling f�r mstSplice (f�r SpliceClassierung added.
|=========================================================================================*)
unit SelectionLayer;
                                                                                                   
interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, ExtCtrls, QualityMatrixDef,
  QualityMatrixBase, YMParaDef, VCLXMLSettingsModel;

type
  TFieldStateTbl = array of Char;
  //...........................................................................

  TSelectionLayer = class(TObject)
  private
    fActiveVisible: Boolean;
    mCutActiveColor: TBrushRec;
    mCutState: TFieldStateTbl;
    mFClusterActiveColor: TBrushRec;
    mFClusterState: TFieldStateTbl;
    mInactiveColor: TBrushRec;
    fMatrixSubType: TMatrixSubType;
    fMatrixType: TMatrixType;
    fMode: TSelectionMode;
    mSCActiveColor: TBrushRec;
    mSCMemberState: TFieldStateTbl;
    mQualityMatrixBase: TQualityMatrixBase;
    function GetCertainFieldState(aFieldID: Integer; aMode: TSelectionMode): Boolean;
    function GetCertainFieldStateColor(aID, aIndex: Integer; aMode: TSelectionMode): TBrushRec;
    function GetFieldState(aFieldID: Integer): Boolean;
    function GetFieldStateColor(aID, aIndex: Integer): TBrushRec;
    function GetStateColor(const aIndex: Integer): TColor;
    function GetStateStyle(const aIndex: Integer): TBrushStyle;
    procedure SetCertainFieldState(aFieldID: Integer; aState: Boolean; aMode: TSelectionMode);
    procedure SetFieldState(aFieldID: Integer; aState: Boolean);
    procedure SetMatrixSubType(const aValue: TMatrixSubType);
    procedure SetMode(const aValue: TSelectionMode);
    procedure SetStateColor(const aIndex: Integer; const aValue: TColor);
    procedure SetStateStyle(const aIndex: Integer; const aValue: TBrushStyle);
    procedure SetType(aType: TMatrixType);
  public
    constructor Create(aQualityMatrixBase: TQualityMatrixBase); reintroduce;
    destructor Destroy; override;
    procedure CheckArrayLength;
    procedure GetFieldStateTable(var aFClass, aFCluster: String); overload;
    procedure GetFieldStateTable(var aClassification: String); overload;
    procedure PaintField(aFieldID: Integer);
    procedure SetDefaultColor;
    procedure SetFieldStateTable(const aClassification: String); overload;
    procedure SetFieldStateTable(aFClass, aFCluster: String); overload;
    property ActiveColor: TColor index cCutActive read GetStateColor write SetStateColor;
    property ActiveFieldColor[aID: Integer]: TBrushRec index cCutActive read GetFieldStateColor;
    property ActiveStyle: TBrushStyle index cCutActive read GetStateStyle write SetStateStyle;
    property ActiveVisible: Boolean read fActiveVisible write fActiveVisible;
    property FieldState[aFieldID: Integer]: Boolean read GetFieldState write SetFieldState;
    property InactiveColor: TColor index cCutPassive read GetStateColor write SetStateColor;
    property InactiveFieldColor[aID: Integer]: TBrushRec index cCutPassive read GetFieldStateColor;
    property MatrixSubType: TMatrixSubType write SetMatrixSubType;
    property MatrixType: TMatrixType read fMatrixType write SetType;
    property SelectionMode: TSelectionMode read fMode write SetMode;
  end;

implementation
uses
  mmMBCS, mmCS;

const
  cCutMarker   = 'C';
  cUncutMarker = 'U';
  cCutUncutMarker: Array[Boolean] of Char = (cUncutMarker, cCutMarker);

//:---------------------------------------------------------------------------
//:--- Class: TSelectionLayer
//:---------------------------------------------------------------------------
constructor TSelectionLayer.Create(aQualityMatrixBase: TQualityMatrixBase);
begin
  inherited Create;

  mQualityMatrixBase := aQualityMatrixBase;
  //  SelectionMode := smSCMemberField;
  SelectionMode := smCutField;

  
  SetDefaultColor;
end;

//:---------------------------------------------------------------------------
destructor TSelectionLayer.Destroy;
begin
  Finalize(mCutState);
  Finalize(mFClusterState);
  Finalize(mSCMemberState);

  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TSelectionLayer.GetCertainFieldState(aFieldID: Integer; aMode: TSelectionMode): Boolean;
const
  Bit0 = $01;
var
  xState: TFieldStateTbl;
begin
  case aMode of
    smFFClusterField: xState := mFClusterState;
    smSCMemberField:  xState := mSCMemberState;
  else // smCutField
    xState := mCutState;
  end;

  Result := False;
  if (aFieldID < mQualityMatrixBase.TotFields) and
     (aFieldID < Length(xState)) then begin
    // U: Uncut = False, C: Cut = True
    Result := AnsiSameText(xState[aFieldID], cCutMarker);
  end;

//
//  case aMode of
//    smCutField:
//      xState := fCutState;
//
//    smFFClusterField:
//      xState := fFFClusterState;
//
//    smSCMemberField:
//      xState := fSCMemberState;
//  else
//    xState := fCutState;
//  end;
//
//  Result := False;
//  if aFieldID < mQualityMatrixBase.TotFields then
//  begin
//    if aFieldID < 64 then
//    begin
//      xArrayIndex := 0;
//    end
//    else
//    begin
//      xArrayIndex := 8;
//      aFieldID := aFieldID - 64;
//    end;
//    xArrayIndex := xArrayIndex + (aFieldID mod 8);
//    xBitIndex := aFieldID div 8;
//
//    if xArrayIndex < Length(xState) then
//    begin
//      xByte := xState[xArrayIndex] and (Bit0 shl xBitIndex);
//
//      if xByte <> 0 then
//        Result := True
//      else
//        Result := False;
//    end
//    else
//      Result := False;
//
//  end;
//
end;

//:---------------------------------------------------------------------------
function TSelectionLayer.GetCertainFieldStateColor(aID, aIndex: Integer; aMode: TSelectionMode): TBrushRec;
begin
  Result := cBackgroundUnassigned;
  Result.style := bsClear;

  with mQualityMatrixBase do
    case aIndex of
  
      cCutActive:
        case aMode of
  
          smCutField:
            begin
  
              if mCutActiveColor.color <> clNone then
              begin
                if mCutActiveColor.style = bsClear then
                  Result := CutColor[aID]
                else
                  Result.style := mCutActiveColor.style;
                Result.color := mCutActiveColor.color;
              end
              else
              begin
                Result := CutColor[aID];
                if mCutActiveColor.style <> bsClear then
                  Result.style := mCutActiveColor.style;
              end;
            end;
  
          smFFClusterField:
            begin
  
              if mFClusterActiveColor.color <> clNone then
              begin
                if mFClusterActiveColor.style = bsClear then
                  Result := FFClusterColor[aID]
                else
                  Result.style := mFClusterActiveColor.style;
                Result.color := mFClusterActiveColor.color;
              end
              else
              begin
                Result := FFClusterColor[aID];
                if mFClusterActiveColor.style <> bsClear then
                  Result.style := mFClusterActiveColor.style;
              end;
            end;
  
          smSCMemberField:
            begin
  
              if mSCActiveColor.color <> clNone then
              begin
                if mSCActiveColor.style = bsClear then
                  Result := SCMemberColor[aID]
                else
                  Result.style := mSCActiveColor.style;
                Result.color := mSCActiveColor.color;
              end
              else
              begin
                Result := SCMemberColor[aID];
                if mSCActiveColor.style <> bsClear then
                  Result.style := mSCActiveColor.style;
              end;
            end;
        end;
  
      cCutPassive:
        begin
          if mInactiveColor.color = clNone then
          begin
            case SelectionMode of
  
              smCutField:
                Result := NoCutColor[aID];
  
              smFFClusterField:
                Result := NoFFClusterColor[aID];
  
              smSCMemberField:
                Result := NoSCMemberColor[aID];
            end;
            if mInactiveColor.style <> bsClear then
              Result.style := mInactiveColor.style;
          end
          else
          begin
            if mInactiveColor.style = bsClear then
              case SelectionMode of
  
                smCutField:
                  Result := NoCutColor[aID];
  
                smFFClusterField:
                  Result := NoFFClusterColor[aID];
  
                smSCMemberField:
                  Result := NoSCMemberColor[aID];
              end
            else
              Result.style := mInactiveColor.style;
            Result.color := mInactiveColor.color;
          end;
        end;
    end;
end;

//:---------------------------------------------------------------------------
function TSelectionLayer.GetFieldState(aFieldID: Integer): Boolean;
begin
  
  Result := GetCertainFieldState(aFieldID, SelectionMode);
end;

//:---------------------------------------------------------------------------
function TSelectionLayer.GetFieldStateColor(aID, aIndex: Integer): TBrushRec;
begin
  
  Result := GetCertainFieldStateColor(aID, aIndex, SelectionMode);
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.GetFieldStateTable(var aFClass, aFCluster: String);
var
  xLen: Integer;
begin
  xLen := Length(mCutState);
  SetLength(aFClass, xLen);
  SetLength(aFCluster, xLen);

  Move(mCutState[0], PChar(aFClass)^, xLen);
  Move(mFClusterState[0], PChar(aFCluster)^, xLen);

//wss: auch F-Matrix hat immer 128 Felder. Diese werden f�r Dark und Bright ausserhalb
// in QualityMatrix getrennt ins XML gespeichert
//  if (fMatrixType = mtSiro) and (fMatrixSubType <> mstZenitF) then begin
//    // f�r NICHT Zenit Siro Matrix die R�ckgabestrings f�r Bright Klassierung noch mit 'U' auff�llen
//    // xLen entspricht schon dem Wert 64, daher kann ich diesen gleich auch verwenden
//    aFClass   := aFClass   + StringOfChar(cUncutMarker, xLen);
//    aFCluster := aFCluster + StringOfChar(cUncutMarker, xLen);
//  end;

{
  if SizeOf(aColorClass) = Length(fCutState) then
    for i := 0 to High(aColorClass) do begin

      aColorClass[i] := fCutState[i];

      fFFClusterState[i] := fFFClusterState[i] or fCutState[i];
      aColorCluster[i] := fFFClusterState[i];
    end;
{}
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.GetFieldStateTable(var aClassification: String);
var
  xLen: Integer;
begin
  xLen := Length(mCutState);
  SetLength(aClassification, xLen);
  Move(mCutState[0], PChar(aClassification)^, xLen);

//  aClassification := '';
//  for i:=0 to High(fCutState) do
//    aClassification := aClassification + fCutState[i];
end;

//:---------------------------------------------------------------------------
function TSelectionLayer.GetStateColor(const aIndex: Integer): TColor;
var
  xColor: TBrushRec;
begin
  Result := clNone;
  
  with mQualityMatrixBase do
    case aIndex of
  
      cCutActive:
        case SelectionMode of
  
          smCutField:
            if mCutActiveColor.color = clNone then
            begin
              xColor := CutColor[0];
              Result := xColor.color;
            end
            else
              Result := mCutActiveColor.color;

          smFFClusterField:
            if mFClusterActiveColor.color = clNone then
            begin
              xColor := FFClusterColor[0];
              Result := xColor.color;
            end
            else
              Result := mFClusterActiveColor.color;

          smSCMemberField:
            if mSCActiveColor.color = clNone then
            begin
              xColor := SCMemberColor[0];
              Result := xColor.color;
            end
            else
              Result := mSCActiveColor.color;
        end;
  
      cCutPassive:
        if mInactiveColor.color = clNone then
        begin
  
          case SelectionMode of
            smSCMemberField:
              xColor := NoSCMemberColor[0];
  
            smCutField:
              xColor := NoCutColor[0];
  
            smFFClusterField:
              xColor := NoFFClusterColor[0];
          end;
  
          Result := xColor.color;
        end
        else
          Result := mInactiveColor.color;
    end;
end;

//:---------------------------------------------------------------------------
function TSelectionLayer.GetStateStyle(const aIndex: Integer): TBrushStyle;
var
  xColor: TBrushRec;
begin
  
  Result := cBackgroundUnassigned.style;
  
  with mQualityMatrixBase do
    case aIndex of
  
      cCutActive:
        case SelectionMode of
  
          smCutField:
            if mCutActiveColor.style = bsClear then
            begin
              xColor := CutColor[0];
              Result := xColor.style;
            end
            else
              Result := mCutActiveColor.style;

          smFFClusterField:
            if mFClusterActiveColor.style = bsClear then
            begin
              xColor := FFClusterColor[0];
              Result := xColor.style;
            end
            else
              Result := mFClusterActiveColor.style;

          smSCMemberField:
            if mSCActiveColor.style = bsClear then
            begin
              xColor := SCMemberColor[0];
              Result := xColor.style;
            end
            else
              Result := mSCActiveColor.style;
        end;
  
  //    cCutPassive:
    end;
  
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.PaintField(aFieldID: Integer);
var
  xStr: string;
  xRect: TRect;
  xBoarder: Integer;
  xTBrush: TBrushRec;
begin
  with mQualityMatrixBase do begin
    CoordinateMode := cmInside;
    xRect          := Field[aFieldID];

    if ActiveVisible then begin
//      Canvas.TextOut(xRect.Left, xRect.Top, inttostr(aFieldid));
{}
      case SelectionMode of
        smCutField, smFFClusterField: begin
            if (LastCutMode = lcField) and (LastCutField = aFieldId) then begin
              xTBrush       := InactiveFieldColor[aFieldID];
              xTBrush.color := LastCutColor;
              FillField(xRect, xTBrush);
            end
            else begin
              if GetCertainFieldState(aFieldID, smCutField) then
                FillField(xRect, GetCertainFieldStateColor(aFieldID, cCutActive, smCutField))
              else if GetCertainFieldState(aFieldID, smFFClusterField) then begin
                FillField(xRect, InactiveFieldColor[aFieldID]);

                with xRect do begin
//                  InflateRect(xRect, -((Right - Left) div 4), -((Bottom - Top) div 4))
                  xBoarder := (Right - Left) div 4;
                  Left     := Left + xBoarder;
                  Right    := Right - xBoarder;
                  xBoarder := (Bottom - Top) div 4;
                  Top      := Top + xBoarder;
                  Bottom   := Bottom - xBoarder;
                end;

                FillField(xRect, GetCertainFieldStateColor(aFieldID, cCutActive, smFFClusterField));
              end else
                FillField(xRect, InactiveFieldColor[aFieldID]);
            end;
          end;

        smSCMemberField: begin
            if GetCertainFieldState(aFieldID, smSCMemberField) then
              FillField(xRect, GetCertainFieldStateColor(aFieldID, cCutActive, smSCMemberField))
            else
              FillField(xRect, InactiveFieldColor[aFieldID]);
          end;
      end; // case
{}
    end else // if ActiveVisible
      FillField(xRect, InactiveFieldColor[aFieldID]);
  end; // with
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.SetCertainFieldState(aFieldID: Integer; aState: Boolean; aMode: TSelectionMode);

//  function SetBit(aByte: Byte; aBitPosition: Integer; aValue: Boolean): Byte;
//  const
//    cBit0 = $01;
//  begin
//    if aValue then
//      Result := aByte or (cBit0 shl aBitPosition)
//    else
//      Result := aByte and not (cBit0 shl aBitPosition);
//  end;

begin
  if (aFieldID < mQualityMatrixBase.TotFields) and
     (aFieldID < Length(mCutState)) then begin

    case aMode of
      smCutField: begin
          mFClusterState[aFieldID] := cCutUncutMarker[aState];
          mCutState[aFieldID]      := cCutUncutMarker[aState];
        end;

      smFFClusterField:
          mFClusterState[aFieldID] := cCutUncutMarker[aState];

      smSCMemberField:
          mSCMemberState[aFieldID] := cCutUncutMarker[aState];
    end;
  end;

{
  if aFieldID < mQualityMatrixBase.TotFields then
  begin
    if aFieldID < 64 then
    begin
      xArrayIndex := 0;
    end
    else
    begin
      xArrayIndex := 8;
      aFieldID := aFieldID - 64;
    end;
    xArrayIndex := xArrayIndex + (aFieldID mod 8);
    xBitPosition := aFieldID div 8;

    case aMode of
      smCutField:
        if (xArrayIndex < Length(fCutState)) and
           (xArrayIndex < Length(fFFClusterState)) then
        begin
          fFFClusterState[xArrayIndex] := SetBit(fFFClusterState[xArrayIndex], xBitPosition, aState);
          fCutState[xArrayIndex]       := SetBit(fCutState[xArrayIndex], xBitPosition, aState);
        end;

      smFFClusterField:
        if (xArrayIndex < Length(fCutState)) and
           (xArrayIndex < Length(fFFClusterState)) then
        begin
          fFFClusterState[xArrayIndex] := SetBit(fFFClusterState[xArrayIndex], xBitPosition, aState) or
                                          fCutState[xArrayIndex];
        end;

      smSCMemberField:
        if (xArrayIndex < Length(fSCMemberState)) then begin
          fSCMemberState[xArrayIndex] := SetBit(fSCMemberState[xArrayIndex], xBitPosition, aState);
        end;
    end;
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.SetDefaultColor;
begin
  mCutActiveColor.color := clNone;
  mCutActiveColor.style := bsClear;

  mFClusterActiveColor.color := clNone;
  mFClusterActiveColor.style := bsClear;

  mInactiveColor.color := clNone;
  mInactiveColor.style := bsClear;

  mSCActiveColor.color := clNone;
  mSCActiveColor.style := bsClear;
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.SetFieldState(aFieldID: Integer; aState: Boolean);
begin
  SetCertainFieldState(aFieldID, aState, SelectionMode);
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.SetFieldStateTable(const aClassification: String);
var
  i: Integer;
begin
  i := Length(mCutState);
  if Length(aClassification) = i then
    Move(PChar(aClassification)^, mCutState[0], i);
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.SetFieldStateTable(aFClass, aFCluster: String);
var
  i: Integer;
begin
  // Erstmal beide Klassierparameter �bernehmen
  i := Length(aFClass);
  if i <= Length(mCutState) then
    Move(PChar(aFClass)^, mCutState[0], i);

  i := Length(aFCluster);
  if i <= Length(mFClusterState) then
    Move(PChar(aFCluster)^, mFClusterState[0], i);

  // Wieso auch immer, aber wenn per Klassierung das Feld auf 'Cut' steht
  // dann das Cluster Feld ebenfalls auf 'Cut' setzen
  for i:=0 to High(mFClusterState) do begin
    if AnsiSameText(mCutState[i], cCutMarker) then
      mFClusterState[i] := cCutMarker;
  end;
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.SetMatrixSubType(const aValue: TMatrixSubType);
begin
  if (fMatrixSubType <> aValue) or (aValue = mstSplice) then begin      //Nue:04.06.08  mstSplice added
    CheckArrayLength;
    //wss: SiroBD sieht optisch anderst aus als SiroF und SiroH
    //wss: Es wird nur beim Wechsel von Dark/Bright <-> F/H etwas gemacht
//    if (aValue = mstSiroBD) or (fMatrixSubType = mstSiroBD) then begin
    if (aValue in [mstSiroBD, mstZenitF, mstSplice]) or (fMatrixSubType in [mstSiroBD, mstZenitF]) then begin   //Nue:04.06.08  mstSplice added
      FillChar(mFClusterState[0], Length(mFClusterState), cUncutMarker);
      FillChar(mCutState[0], Length(mCutState), cUncutMarker);
    end;
    fMatrixSubType := aValue;
  end;
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.SetMode(const aValue: TSelectionMode);
begin
  fMode := aValue;
  ActiveVisible := True;
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.SetStateColor(const aIndex: Integer; const aValue: TColor);
begin
  case aIndex of
    cCutActive:
      case SelectionMode of
        smCutField:       mCutActiveColor.color := aValue;
        smFFClusterField: mFClusterActiveColor.color := aValue;
        smSCMemberField:  mSCActiveColor.color := aValue;
      end;

    cCutPassive:
      mInactiveColor.color := aValue;
  end;
end;

//:---------------------------------------------------------------------------
procedure TSelectionLayer.SetStateStyle(const aIndex: Integer; const aValue: TBrushStyle);
begin
  case aIndex of
    cCutActive:
      case SelectionMode of
        smCutField:       mCutActiveColor.style := aValue;
        smFFClusterField: mFClusterActiveColor.style := aValue;
        smSCMemberField:  mSCActiveColor.style := aValue;
      end;
  end;
end;

//:---------------------------------------------------------------------------
//1 Die Byte-Daten-Array werden auf die neue L�nge gesetzt und gel�scht
procedure TSelectionLayer.CheckArrayLength;
var
  xLength: Integer;
begin
  xLength := 128; //mQualityMatrixBase.TotFields;
  if xLength <> Length(mCutState) then begin
    SetLength(mCutState, xLength);
    FillChar(mCutState[0], xLength, cUncutMarker);

    SetLength(mFClusterState, xLength);
    FillChar(mFClusterState[0], xLength, cUncutMarker);

    SetLength(mSCMemberState, xLength);
    FillChar(mSCMemberState[0], xLength, cUncutMarker);
  end;
//  //wss: aType wird ja nirgends verwendet!!
//  with mQualityMatrixBase do
//  begin
//    xLength := TotFields;
//    if xLength <> Length(mCutState) then begin
//      SetLength(mCutState, xLength);
//      FillChar(mCutState[0], xLength, cUncutMarker);
//    end;
//
//    if xLength <> Length(mFClusterState) then begin
//      SetLength(mFClusterState, xLength);
//      FillChar(mFClusterState[0], xLength, cUncutMarker);
//    end;
//
//    if xLength <> Length(mSCMemberState) then begin
//      SetLength(mSCMemberState, xLength);
//      FillChar(mSCMemberState[0], xLength, cUncutMarker);
//    end;
//  end;
end;

procedure TSelectionLayer.SetType(aType: TMatrixType);
begin
  if aType <> fMatrixType then begin
    fMatrixType := aType;
    CheckArrayLength;
  end;
//  with mQualityMatrixBase do
//  begin
//    xLength := TotFields div 8;
//    if (TotFields mod 8) <> 0 then
//      inc(xLength);
//
//    if xLength <> Length(fCutState) then begin
//      SetLength(fCutState, xLength);
//      FillChar(fCutState[0], xLength, 0);
//    end;
//
//    if xLength <> Length(fFFClusterState) then begin
//      SetLength(fFFClusterState, xLength);
//      FillChar(fFFClusterState[0], xLength, 0);
//    end;
//
//    if xLength <> Length(fSCMemberState) then begin
//      SetLength(fSCMemberState, xLength);
//      FillChar(fSCMemberState[0], xLength, 0);
//    end;
//  end;
end;

end.
