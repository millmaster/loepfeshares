(*=========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMParaUtils.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Supports initialisation and other helper functions for YarnMaster settings
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.10.1999  1.00  Kr  | Initial Release, Definitive Yarn Count Units fehlt
| 03.11.1999  1.01  Mg  | TVoidDataItem and
| 16.11.1999  1.02NueKr | Swap's bei Get*-Methoden (Aufruf von Extract) rausgenommen.
| 12.04.2000  1.03  Nue | KorrekturHack temporaer!!!!!!!!!! NueWSCPatch added
| 08.05.2000  1.04  Kr  | GetProdGrpYMPara and PutProdGrpYMPara countUnit and threadCnt added
| 05.11.2001  1.05  Nue | "or (MachineConfig.aWEMachType = amtAC338)" added in GetAvailable.
| 20.11.2001  1.06  khp | Anpassung in ConvertSettingsFromAC338: Konvertieren von WSCGarnNr
|                       | in LoepfeGarnNr und WSCGarneinheit in MMStandarteinheit(Nm)
| 28.02.2002  1.07  khp | Zurueck bauen der Anpassung in ConvertSettingsFromAC338: Konvertieren von WSCGarnNr
|                       | in LoepfeGarnNr und WSCGarneinheit in MMStandarteinheit(Nm)
| 18.03.2002  1.08  Kr  | Diverse Aenderungen
| 06.06.2002  1.09  khp | ConvertSettingsFromAC338: WSCGarnNr=EinzelfadenNr, LoepfeGarnNr=GesamtfadenNr
|                       | somit LoepfeGarnNr = WSCGarnNr DIV fadenzahl
| 04.06.2004  1.11  Kr  | TConfigurationCode.ValidateSensingHeadDependency:
|                       | cCCBNoFFAdjAtOfflimitBit and cCCBFFAdjAfterAlarmBit wird nicht mehr default gesetzt
| 13.01.2005  1.11  Wss | SwapClassFields() Funktion hinzugef�gt
| 31.03.2005        Lok | Aufr�umen (Compiler Warnings und Hints)
| 20.02.2008  1.20  Nue | TK-ZenitC Handling added.
| 10.03.2008        Nue | TK-ZenitC Handling added.
| 10.06.2008  1.21  Nue | IsZenitOrHigher Funktion hinzugef�gt
|=========================================================================================*)
unit YMParaUtils;

interface

uses
  YMParaDef, BaseGlobal, MMUGlobal, LoepfeGlobal, Windows, sysutils, classes,
  XMLDef, XMLSettingsModel, xmlMaConfigClasses, MSXML2_TLB, FPatternClasses;

const
//------------------------------------------------------------------------------
// Test initialisation (more or less Memory C)
//------------------------------------------------------------------------------
{
 cYMSettingsDefault: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for Bueler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            550;
        sw:             cChOn;
//        sw:             cChOff;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         13;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            500;
        sw:             cChOn;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          150;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      upperYarn: (
        dia:            141;
        sw:             cChOn;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $80, $C0, $F0, $F8, $FC, $FE, $FF, $FF,
                  $F0, $C0, $80, $00, $00, $C0, $E0, $F8);
    siroClear: ( $00, $F0, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        180;
//       yarnCnt:        cInitYarnCount;
       // TODO wss: in record ist Word, YarnCount aber neu Float
       yarnCnt:        Round(cYarnCountInit * 10);
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     135;
        clusterLength:  1000;
        clusterDefects: 30;
      );
      configA:          cCCADefault or $01;
      configB:          cCCBDefault;
      configC:          cCCCDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     160;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultRepetitions;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            100;
      lower:            100;
      rep:              cDefaultRepetitions;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        1000;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
//      siroClear: ( $8F, $8F, $CF, $EF, $EF, $FF, $FF, $FF );
      siroClear: ( $00, $00, $00, $00, $00, $00, $00, $00 );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
{}
  //...........................................................................

  //...........................................................................

{
 cYMSettingsCD: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
     swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( 0, 0 );
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            550;
        sw:             cChOn;
//        sw:             cChOff;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         13;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            500;
        sw:             cChOn;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          150;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      upperYarn: (
        dia:            141;
        sw:             cChOn;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $80, $C0, $F0, $F8, $FC, $FE, $FF, $FF,
                  $F0, $C0, $80, $00, $00, $C0, $E0, $F8);
    siroClear: ( $00, $F0, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        180;
//       yarnCnt:        cInitYarnCount;
       // TODO wss: in record ist Word, YarnCount aber neu Float
       yarnCnt:        100; //Round(cYarnCountInit * 10);
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     135;
        clusterLength:  1000;
        clusterDefects: 30;
      );
      configA:          $001B;
      configB:          $0020;
      configC:          $0000;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     160;
       countLength:		1;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultRepetitions;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      10;
      lengthWindow:     11;
      lengthMode:       12;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            100;
      lower:            100;
      rep:              cDefaultRepetitions;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        1000;
      defects:          30;
      rep:              3;
      fill0:            0;
//      siroClear: ( $8F, $8F, $CF, $EF, $EF, $FF, $FF, $FF );
      siroClear: ( $00, $00, $00, $00, $00, $00, $00, $00 );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsTest: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
     swOption:            0;
      group:            1;
      prodGrpID:        1291;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      6;
     machType:         2;
      speedRamp:        5;
      inoperativePara: ( 0, 0 );
//      inoperativePara: ( (cIPMNoPlus or cIPMNoSpeedSim or cIPMUpperYarn or cIPMSHTypeF), 0 );
//      inoperativePara: ( (cIPMNoPlus or cIPMNoUpperYarn or cIPMNoSpeedSim), 0 );
//      inoperativePara: ( (cIPMNoPlus or cIPMNoUpperYarn), 0 );
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps:     (dia: cNepsDiaMin;      sw:  cChOn);
      short:    (dia: cShortDiaMin;     sw:  cChOn);
      shortLen: cShortLenMin;
      long:     (dia: cLongDiaMin;      sw:  cChOn);
      longLen:  cLongLenMin;
      thin:     (dia: cThinDiaMaxBelow; sw:  cChOn);
      thinLen:  cThinLenMin;
      splice:   (len: 150;              sw:  cChOn);
      speed:    600;
      spare:    0;
//      neps:     (dia: cMinNepsDia;      sw:  cChOn);
//      short:    (dia: cMinShortDia;     sw:  cChOn);
//      shortLen: cMinShortLen;
//      long:     (dia: cMinLongDia;      sw:  cChOn);
//      longLen:  cMinLongLen;
//      thin:     (dia: cMaxThinDiaBelow; sw:  cChOn);
//      thinLen:  cMinThinLen;
//      splice:   (len: 150;              sw:  cChOn);
//      speed:    600;
//      spare:    0;
    );

   splice: (
      neps:      (dia: cNepsDiaMin;      sw: cChOn);
      short:     (dia: cShortDiaMin;     sw: cChOn);
      shortLen:  cShortLenMin;
      long:      (dia: cLongDiaMin;      sw: cChOn);
      longLen:   cLongLenMin;
      thin:      (dia: cThinDiaMaxBelow; sw: cChOn);
      thinLen:   cThinLenMin;
      upperYarn: (dia: 141;              sw: cChOn);
      checkLen:  25;
      spare_:    0;
//      neps:      (dia: cMinNepsDia;      sw: cChOn);
//      short:     (dia: cMinShortDia;     sw: cChOn);
//      shortLen:  cMinShortLen;
//      long:      (dia: cMinLongDia;      sw: cChOn);
//      longLen:   cMinLongLen;
//      thin:      (dia: cMaxThinDiaBelow; sw: cChOn);
//      thinLen:   cMinThinLen;
//      upperYarn: (dia: 141;              sw: cChOn);
//      checkLen:  25;
//      spare_:    0;
    );

    classClear: ( $80, $C0, $F0, $F8, $FC, $FE, $FF, $FF,
                  $F0, $C0, $80, $00, $00, $C0, $E0, $F8);
    siroClear: ( $00, $F0, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        180;
//       yarnCnt:        cInitYarnCount;
       // TODO wss: in record ist Word, YarnCount aber neu Float
       yarnCnt:        Round(cYarnCountInit * 10);
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         3;
        siroStartupRep: 6;
        clusterDia:     135;
        clusterLength:  1000;
        clusterDefects: 30;
      );
      configA:          $001B;
      configB:          $0020;
      configC:          $0000;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     160;
       countLength:		1;
       clusterRep:     3;
        cutRetries:     3;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      10;
      lengthWindow:     11;
      lengthMode:       12;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            100;
      lower:            100;
      rep:              3;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        1000;
      defects:          30;
      rep:              3;
      fill0:            0;
//      siroClear: ( $8F, $8F, $CF, $EF, $EF, $FF, $FF, $FF );
      siroClear: ( $00, $00, $00, $00, $00, $00, $00, $00 );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
{}
  //...........................................................................

{
  cChannelOff: TChannelSettingsRec = (
    neps:     (dia: cNepsDiaMin;      sw: cChOff);
    short:    (dia: cShortDiaMin;     sw: cChOff);
    shortLen: cShortLenMin;
    long:     (dia: cLongDiaMin;      sw: cChOff);
    longLen:  cLongLenMin;
    thin:     (dia: cThinDiaMaxBelow; sw: cChOff);
    thinLen:  cThinLenMin;
    splice:   (len: 150;              sw: cChOff);
    speed:    600;
    spare:    0;
//    neps:     (dia: cMinNepsDia; sw: cChOff);
//    short:    (dia: cMinShortDia; sw: cChOff);
//    shortLen: cMinShortLen;
//    long:     (dia: cMinLongDia; sw: cChOff);
//    longLen:  cMinLongLen;
//    thin:     (dia: cMaxThinDiaBelow; sw: cChOff);
//    thinLen:  cMinThinLen;
//    splice:   (len: 150; sw: cChOff);
//    speed:    600;
//    spare:    0;
  );
{}

  //...........................................................................
// ZE Version V8.12, 9.12, 10.12: ConfigCodeC is not Swapped
  cFSWVConfigCodeCNotSwapped: array[0..2] of TYMSWVersionArr = (
    (Byte('V'),Byte('1'),Byte('0'),Byte('.'),Byte('1'),Byte('2'),0,0),
    (Byte('V'),Byte('9'),Byte('.'),Byte('1'),Byte('2'),0,0,0),
    (Byte('V'),Byte('8'),Byte('.'),Byte('1'),Byte('2'),0,0,0)
  );

  // ZE Version V8.12, 9.12, 10.12 and V8.14, 9.14, 10.14: FFCluster.defects &
  // FFCluster.obsLength not Swapped
  cFSWVFFClusterNotSwapped: array[0..5] of TYMSWVersionArr = (
    (Byte('V'),Byte('1'),Byte('0'),Byte('.'),Byte('1'),Byte('2'),0,0),
    (Byte('V'),Byte('9'),Byte('.'),Byte('1'),Byte('2'),0,0,0),
    (Byte('V'),Byte('8'),Byte('.'),Byte('1'),Byte('2'),0,0,0),
    (Byte('V'),Byte('1'),Byte('0'),Byte('.'),Byte('1'),Byte('4'),0,0),
    (Byte('V'),Byte('9'),Byte('.'),Byte('1'),Byte('4'),0,0,0),
    (Byte('V'),Byte('8'),Byte('.'),Byte('1'),Byte('4'),0,0,0)
  );

// Informator Version V5.10: inopSettings not available
  cFSWVInopSettingsNotAvailable: array[0..0] of TYMSWVersionArr = (
    (Byte('V'),Byte(' '),Byte('5'),Byte('.'),Byte('1'),Byte('0'),0,0)
  );

// Informator Version V5.25: inopSettings FFTK wrong
  cFSWVInopSettingsWrongFFTK: array[0..0] of TYMSWVersionArr = (
    (Byte('V'),Byte(' '),Byte('5'),Byte('.'),Byte('2'),Byte('5'),0,0)
  );

  // Informator Version V5.10, V5.25: SWOption not available
  cFSWVSWOptionNotAvailable: array[0..1] of TYMSWVersionArr = (
    (Byte('V'),Byte(' '),Byte('5'),Byte('.'),Byte('1'),Byte('0'),0,0),
    (Byte('V'),Byte(' '),Byte('5'),Byte('.'),Byte('2'),Byte('5'),0,0)
  );

//------------------------------------------------------------------------------
// Index constant
//------------------------------------------------------------------------------
// Block Bit Index
  cFFBlock                 = 0;
  cFFBlockAvailable        = 1;
  cCutFailBlock            = 2;
  cCutFailBlockAvailable   = 3;
  cClusterBlock            = 4;
  cCountBlock              = 5;
  cSFIBlock                = 6;
  cFFClusterBlock          = 7;
  cFFClusterBlockAvailable = 8;
  cBlockEnabled            = 9;
  //...........................................................................

// Other Config Bit Index
  cCfgAAdjustRemove                = 0;
  cCfgAAdjustRemoveAvailable       = 1;
  cCfgABunchMonitor                = 2;
  cCfgADriftConeChange             = 3;
  cCfgAFFClearingOnSplice          = 4;
  cCfgAFFClearingOnSpliceAvailable = 5;
  cCfgAFFDetection                 = 6;
  cCfgAFFDetectionAvailable        = 7;
  cCfgAKnifePowerHigh              = 8;
  cCfgAKnifePowerHighAvailable     = 9;
  cCfgAOneDrumPuls                 = 10;
  cCfgAOneDrumPulsAvailable        = 11;
  cCfgAZeroAdjust                  = 12;

  cCfgBFFBDDetection            = 13;
  cCfgBFFBDDetectionAvailable   = 14;
  cCfgBConeChangeCondition      = 15;
  cCfgBCutBeforeAdjust          = 16;
  cCfgBCutOnYarnBreak           = 17;
  cCfgBExtMurItf                = 18;
  cCfgBExtMurItfAvailable       = 19;
  cCfgBFFAdjAfterAlarm          = 20;
  cCfgBFFAdjAfterAlarmAvailable = 21;
  cCfgBFFAdjAtOfflimit          = 22;
  cCfgBFFAdjAtOfflimitAvailable = 23;
  cCfgBHeadstockRight           = 24;
  cCfgBKnifeMonitor             = 25;
  cCfgBUpperYarnCheck           = 26;
  //...........................................................................

// Other Sensitivity Bit Index
  cSFSSensitivity = 0;
  cDFSSensitivity = 1;
  //...........................................................................

// TMachineYMConfig: Propery index for Config Code
  cCfgA     = 0;
  cCfgB     = 1;
  cCfgC     = 2;
  cCfgMachA = 3;
  cCfgMachB = 4;
  cCfgMachC = 5;
  cCfgProdA = 6;
  cCfgProdB = 7;
  cCfgProdC = 8;
  //...........................................................................

 //TConfigurationCode: Propery index for spindle range
  cSRFrom = 1;
  cSRTo   = 2;
  //...........................................................................

  // TMachineYMConfig
  cCutRetries          = 1;
  cDefaultSpeedRamp    = 2;
  cDefaultSpeed        = 3;
  cDependendSpdleRange = 4;
  cSpeedSimulation     = 5;
  //...........................................................................

type
  // Listed group numbers
  TGroupListArr = array[0..cZESpdGroupLimit-1] of Integer;
const
  // TGroupListArr
  cNoGroup = High(TGroupListArr) + 1;
  //..............................................................................

type
  // TMachineYMConfig
  TSwitchState = (ssOn, ssOff);
  //...........................................................................

  // TConfigurationCode
  TCfgFilter = (cfNone, cfProduction, cfMachine, cfDiagnosis, cfUnused);

//------------------------------------------------------------------------------
// Parameter Source Indentifier
//------------------------------------------------------------------------------

  TParameterSource = (psUnknown,
    psChannel, // Channel Parameter Box Identifier
    psSplice, // Splice Parameter Box Identifier
    psCluster, // Cluster Parameter Box Identifier
    psYarnCount, // YarnCount Parameter Box Identifier
    psSFI, // SFI Parameter Box Identifier
    psNSLTClass, // NSLT Class Parameter Box Identifier
    psFFClass); // FF Class Parameter Box Identifier
  //...........................................................................

  TOnParameterChange = procedure (aParameterSource: TParameterSource) of object;
  //...........................................................................

  TSensingHeadClassList = class(TList)
  private
    function GetItems(Index: Integer): TSensingHeadClass;
    procedure SetItems(Index: Integer; aValue: TSensingHeadClass);
  public
    constructor Create;
    destructor Destroy; override;
    function Add(aSensingHeadClass: TSensingHeadClass): Integer;
    procedure Clear; override;
    procedure Sort;
    property Items[Index: Integer]: TSensingHeadClass read GetItems write SetItems;
  end;
  
  //...........................................................................

//------------------------------------------------------------------------------
// Free functions
//------------------------------------------------------------------------------
function CalculateCountFromDiaDiff(aMinMaxDia, aCount: Single; aByLength: Boolean): Single;
function GetAvailabilityOfDataItem(aVoidData: DWord; aDataItem: TVoidDataItem): Boolean;
function SensingHeadClassCompare(aItem1, aItem2: Pointer): Integer;
function SensingHeadTypeCompare(aItem1, aItem2: Pointer): Integer;
function ValidateSensingHeadType(aValue: TSensingHead): TSensingHead;

function CalculateDiaDiffFromCounts(aCorseFine, aCount: Single; aByLength: Boolean): Single;

function GetFPattern(aObserver: TBaseXMLSettingsObserver; aXPath: String): Boolean;

// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetAWEType(aMaConfigReader: TXMLMaConfigHelper; aGroupIndex: Integer): string;

function GetSensingHeadClass(aModel: TXMLSettingsModel): TSensingHeadClass;
    overload;

function IsFSensingHead(aModel: TXMLSettingsModel): Boolean;

procedure SetSensingHeadClass(aModel: TXMLSettingsModel; aSensingHeadClass: TSensingHeadClass);

procedure SwapClassFields(aNode: IXMLDOMNode);

// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetAWEClass(aMaConfigReader: TMaConfigReader; aGroupIndex: Integer):
    TAWEClass;

function GetSensingHeadClass(aSensingHead: TSensingHead): TSensingHeadClass; overload;

function IsZenit(aModel: TXMLSettingsModel): Boolean;

function IsZenitOrHigher(aModel: TXMLSettingsModel): Boolean;

function FitValue(aValue, aMin, aMax: Single): Single;

// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetSensingHeadClass(aMaConfigReader: TXMLMaConfigHelper; aGroupIndex: Integer): TSensingHeadClass; overload;

function GetSensingHeadClass(aFPattern: TBaseFPattern): TSensingHeadClass;
    overload;


  //...........................................................................

//------------------------------------------------------------------------------
// Free functions
//------------------------------------------------------------------------------
implementation
uses
  mmMBCS;
//------------------------------------------------------------------------------
const
  cBit0: Integer = 1;

//:-----------------------------------------------------------------------------
function IsFSensingHead(aModel: TXMLSettingsModel): Boolean;
begin
  Result := Boolean(aModel.FPHandler.Value[cXPFP_FSpectraNormalItem]) or
            Boolean(aModel.FPHandler.Value[cXPFP_FSpectraHighItem]) or
            Boolean(aModel.FPHandler.Value[cXPFP_FSpectraBDItem]) or
            Boolean(aModel.FPHandler.Value[cXPFP_FZenitDarkItem]) or
            Boolean(aModel.FPHandler.Value[cXPFP_FZenitBrightItem]);
end;
//:-----------------------------------------------------------------------------
// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetSensingHeadClass(aMaConfigReader: TXMLMaConfigHelper; aGroupIndex: Integer): TSensingHeadClass;
var
  xSensingHead: TSensingHead;
begin
  xSensingHead := GetSensingHeadValue(aMaConfigReader.GroupValueDef[aGroupIndex, cXPSensingHeadItem, cSensingHeadNames[shTK830]]);
  Result := GetSensingHeadClass(xSensingHead);
end;

//:-----------------------------------------------------------------------------
function GetSensingHeadClass(aFPattern: TBaseFPattern): TSensingHeadClass;
begin
  if Boolean(aFPattern.Value[cXPFP_FSpectraNormalItem]) then
    Result := shc9xFS
  else if Boolean(aFPattern.Value[cXPFP_FSpectraHighItem]) then
    Result := shc9xH
  else if Boolean(aFPattern.Value[cXPFP_FSpectraBDItem]) then
    Result := shc9xBD
  else if Boolean(aFPattern.Value[cXPFP_FZenitDarkItem]) or
          Boolean(aFPattern.Value[cXPFP_FZenitBrightItem]) then begin
//    if Boolean(aFPattern.Value[cXPFP_PItem]) then
//      Result := shcZenitFP
//    else
//      Result := shcZenitF;
//  end
//  else if Boolean(aFPattern.Value[cXPFP_ZenitItem]) then
//    Result := shcZenit
//  else
//    Result := shc8x;
{ DONE 1 -oNue -cRelease 5.04 : Neues FPattern Item einf�hren FP_ZenitC (Einf�gen der TKZenitC.. in den folgenden Statements }
//Nue: 10.03.08 Anpassungen wegen TKZenitC..
    if Boolean(aFPattern.Value[cXPFP_PItem]) then
      if Boolean(aFPattern.Value[cXPFP_ZenitCItem]) then
        Result := shcZenitCFP
      else
        Result := shcZenitFP
    else
      if Boolean(aFPattern.Value[cXPFP_ZenitCItem]) then
        Result := shcZenitCF
      else
        Result := shcZenitF;
  end
  else if Boolean(aFPattern.Value[cXPFP_ZenitItem]) then
      Result := shcZenit
    else if Boolean(aFPattern.Value[cXPFP_ZenitCItem]) then
        Result := shcZenitC
    else
      Result := shc8x;
end;

//:-----------------------------------------------------------------------------
function GetSensingHeadClass(aModel: TXMLSettingsModel): TSensingHeadClass;
begin
  //GetSensingHeadClass(aModel.FPHandler);
  Result := GetSensingHeadClass(aModel.FPHandler);    //Nue:28.9.05 Resultwert zuweisen!!!!!
end;

//:-----------------------------------------------------------------------------
function CalculateCountFromDiaDiff(aMinMaxDia, aCount: Single; aByLength: Boolean): Single;
begin
  try
    // aMinMaxData ist in Prozent, hier muss aber mit dem Faktor gerechnet werden 90% -> 0.9
    aMinMaxDia := aMinMaxDia / 100;
    if aByLength then Result := (aMinMaxDia * aMinMaxDia) * aCount
                 else Result := aCount / (aMinMaxDia * aMinMaxDia);

  except
    on e: Exception do
      raise Exception.Create('CalculateCountFromDiaDiff failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------

function CalculateCountByWeight(aMinMaxDia, aCount: Single): Single;
begin
  try
    // aMinMaxData ist in Prozent, hier muss aber mit dem Faktor gerechnet werden 90% -> 0.9
    aMinMaxDia := aMinMaxDia / 100;
    Result     := aCount / (aMinMaxDia * aMinMaxDia);
  except
    on e: Exception do
      raise Exception.Create('CalculateCountByWeight failed: ' + e.Message);
  end;

end;
//------------------------------------------------------------------------------

function CalculateDiaDiffByLength(aCorseFine, aCount: Single): Single;
begin
  try
    Result := Sqrt(aCorseFine / aCount);
    if Result < 1 then Result := 1 - Result
                  else Result := Result - 1;
    // Bei der Berechnung kommt ein Faktor z.B 0.09 heraus, ben�tigt wird aber Prozent 9%
    Result := Result * 100;
  except
    on e: Exception do
      raise Exception.Create('CalculateDiaDiffByLength failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------

function CalculateDiaDiffByWeight(aCorseFine, aCount: Single): Single;
begin
  try
    Result := Sqrt(aCount / aCorseFine);
    if Result < 1 then Result := 1 - Result
                  else Result := Result - 1;
    // Bei der Berechnung kommt ein Faktor z.B 0.09 heraus, ben�tigt wird aber Prozent 9%
    Result := Result * 100;
  except
    on e: Exception do
      raise Exception.Create('CalculateDiaDiffByWeight failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
function FitValue(aValue, aMin, aMax: Single): Single;
begin
  Result := aValue;
  if (aMax > aMin) then begin
    if aValue < aMin then
      Result := aMin
    else if aValue > aMax then
      Result := aMax;
  end;
end;
//------------------------------------------------------------------------------
function GetAvailabilityOfDataItem(aVoidData: DWord; aDataItem: TVoidDataItem): Boolean;
begin
  case aDataItem of
    vdCones: Result  := false;
    vdCops: Result   := false;
    vdIPOld: Result  := false;
    vdIPNew: Result  := true;
    vdIPPlus: Result := false;
    vdSFIVar: Result := true;
    vdSFICnt: Result := true;
  else
    Result := true;
  end;
end;
//------------------------------------------------------------------------------
function SensingHeadClassCompare(aItem1, aItem2: Pointer): Integer;
begin
    Result := 0;
{wss
  if TSensingHeadClass(aItem1^) < TSensingHeadClass(aItem2^) then
    Result := 1
  else if TSensingHeadClass(aItem1^) > TSensingHeadClass(aItem2^) then
    Result := -1
  else
    Result := 0;
{}
end;
//------------------------------------------------------------------------------
function SensingHeadTypeCompare(aItem1, aItem2: Pointer): Integer;
begin
    Result := 0;
{wss
  if cSensingHeadTypeOrder[TSensingHead(aItem1)] < cSensingHeadTypeOrder[TSensingHead(aItem2)] then
    Result := -1
  else if cSensingHeadTypeOrder[TSensingHead(aItem1)] > cSensingHeadTypeOrder[TSensingHead(aItem2)] then
    Result := 1
  else
    Result := 0;
{}
end;
//------------------------------------------------------------------------------
procedure SwapClassFields(aNode: IXMLDOMNode);
  //........................................................
  function SwapFields(aClassString: String): String;
  var
    xSrcIndex: Integer;
    xDstIndex: Integer;
    xBaseIndex: Integer;
    xSrcLength: Integer;
    xDestStrArr: Array of Char;
  begin
    xSrcLength := Length(aClassString);
    // Src String muss exakt durch 8 teilbar sein sonst ist es kein g�ltige Klassdefinition
    if (xSrcLength = 64) or (xSrcLength = 128) then begin
      SetLength(xDestStrArr, xSrcLength+1);
      xSrcIndex := 0;
      while xSrcIndex < xSrcLength do begin
        xBaseIndex := (xSrcIndex div 64 ) * 64;
        xDstIndex  := xBaseIndex + (xSrcIndex mod 8) * 8 + ((xSrcIndex-xBaseIndex) div 8);
        xDestStrArr[xDstIndex] := aClassString[xSrcIndex+1];
        inc(xSrcIndex);
      end;
      Result := String(xDestStrArr);
    end else
      Result := aClassString;
  end;
  //........................................................
begin
  if Assigned(aNode) then begin
    if aNode.childNodes.Length = 1 then  //Es darf nur einen Wert pro Element im MMXML haben
      if aNode.childNodes.item[0].nodeValue <> NULL then
        aNode.childNodes.item[0].nodeValue := SwapFields(aNode.childNodes.item[0].nodeValue);
  end;
end;
//------------------------------------------------------------------------------
function ValidateSensingHeadType(aValue: TSensingHead): TSensingHead;
begin
    Result := aValue;
{wss
  if aValue < TSensingHead(cFirstSensingHead) then
    Result := TSensingHead(cFirstSensingHead)
  else if aValue > TSensingHead(cLastSensingHead) then
      Result := TSensingHead(cLastSensingHead)
  else
    Result := aValue;
{}
end;
//------------------------------------------------------------------------------
function CalculateDiaDiffFromCounts(aCorseFine, aCount: Single; aByLength: Boolean): Single;
begin
  try
    // Bei der Berechnung kommt ein Faktor z.B 0.09 heraus, ben�tigt wird aber Prozent 9%
    // Daher die Multiplikation * 100
    if aByLength then Result := Sqrt(aCorseFine / aCount) * 100
                 else Result := Sqrt(aCount / aCorseFine) * 100;

    if Result < 100 then Result := 100 - Result
                    else Result := Result - 100;
  except
    on e: Exception do
      raise Exception.Create('CalculateDiaDiffFromCounts failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
function CalculateCountByLength(aMinMaxDia, aCount: Single): Single;
begin
  try
    // aMinMaxData ist in Prozent, hier muss aber mit dem Faktor gerechnet werden 90% -> 0.9
    aMinMaxDia := aMinMaxDia / 100;
    Result     := (aMinMaxDia * aMinMaxDia) * aCount;
  except
    on e: Exception do
      raise Exception.Create('CalculateCountByLength failed: ' + e.Message);
  end;
end;

function GetFPattern(aObserver: TBaseXMLSettingsObserver; aXPath: String): Boolean;
begin
  if aObserver.Model.MaConfigReader.Available then
    Result := aObserver.Model.MaConfigReader.FPHandler.Value[aXPath]
  else
    Result := aObserver.ValueDef[aXPath, False];
end;

//:-----------------------------------------------------------------------------
procedure SetSensingHeadClass(aModel: TXMLSettingsModel; aSensingHeadClass: TSensingHeadClass);
begin
  aModel.BeginUpdate;
  try
    aModel.FPHandler.Value[cXPFP_FSpectraNormalItem] := (aSensingHeadClass = shc9xFS);
    aModel.FPHandler.Value[cXPFP_FSpectraHighItem]   := (aSensingHeadClass = shc9xH);
    aModel.FPHandler.Value[cXPFP_FSpectraBDItem]     := (aSensingHeadClass = shc9xBD);
{ DONE 1 -oNue -cRelease 5.04 : Neues FPattern Item einf�hren FP_ZenitC (Einf�gen der TKZenitC.. in den folgenden Statements }
    // PPW 20/06/2017: issue MM-12: added missing aSensingHeadClasses
    aModel.FPHandler.Value[cXPFP_ZenitItem]          := (aSensingHeadClass in [shcZenit, shcZenitF, shcZenitFP, shcZenitCF, shcZenitCFP, shcZenitFP2, shcZenitCFP2]);
    aModel.FPHandler.Value[cXPFP_ZenitCItem]         := (aSensingHeadClass in [shcZenitC, shcZenitCF, shcZenitCFP]);    //Nue:10.3.08  TKZenitC.. added

    aModel.FPHandler.Value[cXPFP_FZenitDarkItem]     := (aSensingHeadClass in [shcZenitF, shcZenitFP, shcZenitCF, shcZenitCFP, shcZenitFP2, shcZenitCFP2]); //Nue:10.3.08  TKZenitC.. added
    aModel.FPHandler.Value[cXPFP_FZenitBrightItem]   := (aSensingHeadClass in [shcZenitF, shcZenitFP, shcZenitCF, shcZenitCFP, shcZenitFP2, shcZenitCFP2]); //Nue:10.3.08  TKZenitC.. added

    aModel.FPHandler.Value[cXPFP_FClusterDarkItem]   := (aSensingHeadClass in [shcZenitF, shcZenitFP, shcZenitCF, shcZenitCFP, shcZenitFP2, shcZenitCFP2, shc9xFS, shc9xH, shc9xBD]);
    aModel.FPHandler.Value[cXPFP_FClusterBrightItem] := (aSensingHeadClass in [shcZenitF, shcZenitFP, shcZenitCF, shcZenitCFP, shcZenitFP2, shcZenitCFP2]); //Nue:10.3.08  TKZenitC.. added
    aModel.FPHandler.Value[cXPFP_PItem]              := (aSensingHeadClass in [shcZenitFP, shcZenitCFP, shcZenitFP2, shcZenitCFP2]); //Nue:10.3.08  TKZenitC.. added
  finally
    aModel.EndUpdate;
  end;// try finally
end;

//:-----------------------------------------------------------------------------
// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetAWEClass(aMaConfigReader: TMaConfigReader; aGroupIndex: Integer):
    TAWEClass;
begin
{ TODO 1 -cLOK/Nue : Aus dem FPAttern die AWE-Klasse extrahieren! }
  Assert(false, 'Noch nicht implementiert');
  Result := aweSpectra;  //Tempor�rer Defaultwert
end;

//:-----------------------------------------------------------------------------
function GetSensingHeadClass(aSensingHead: TSensingHead): TSensingHeadClass;
begin
  case aSensingHead of
    shNone:                       Result := shcNone;
    shTK930F, shTK930S, shTK940F: Result := shc9xFS;
    shTK930H:                     Result := shc9xH;
    shTK940BD:                    Result := shc9xBD;
    shTKZenit:                    Result := shcZenit;
    shTKZenitF:                   Result := shcZenitF;
    shTKZenitFP:                  Result := shcZenitFP;
    shTKZenitFP2:                 Result := shcZenitFP2;
    shTKZenitC:                   Result := shcZenitC;     //Nue:19.02.08
    shTKZenitCF:                  Result := shcZenitCF;    //Nue:19.02.08
    shTKZenitCFP:                 Result := shcZenitCFP;   //Nue:19.02.08
    shTKZenitCFP2:                Result := shcZenitCFP2;
  else // shTK830, shTK840, shTK850, shTK870, shTK880
    Result := shc8x;
  end;
end;

//:-----------------------------------------------------------------------------
function IsZenit(aModel: TXMLSettingsModel): Boolean;
begin
  Result := Boolean(aModel.FPHandler.Value[cXPFP_ZenitItem]);
end;

//:-----------------------------------------------------------------------------
function IsZenitOrHigher(aModel: TXMLSettingsModel): Boolean;
begin
  Result := (Boolean(aModel.FPHandler.ValueDef[cXPFP_ZenitItem, False])) or (Boolean(aModel.FPHandler.ValueDef[cXPFP_ZenitCItem, False]));
end;

//:-----------------------------------------------------------------------------
// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetAWEType(aMaConfigReader: TXMLMaConfigHelper; aGroupIndex: Integer): string;
begin
  //TODO wss: hier noch verfeinern...
  if aMaConfigReader.GroupValueDef[aGroupIndex, cXPFP_ZenitItem, False] then
    Result := 'Zenit'
  else begin
    Result := 'Spectra';
    if aMaConfigReader.GroupValueDef[aGroupIndex, cXPFP_ExtendedClusterItem, False] then
      Result := Result + 'Plus';
  end;
  if aMaConfigReader.GroupValueDef[aGroupIndex, cXPFP_SpeedSimulationItem, False] then
    Result := Result + ' SS';


//  xSensingHead := GetSensingHeadValue(aMaConfigReader.GroupValueDef[aGroupIndex, cXPSensingHeadItem, cSensingHeadNames[shTK830]]);
//  Result := GetSensingHeadClass(xSensingHead);
//  if xSensingHead in [shTKZenit] then
//    Result := shcZenit
//  else if xSensingHead in [shTKZenitF] then
//    Result := shcZenitF
//  else if xSensingHead in [shTKZenitFP] then
//    Result := shcZenitFP
//  else if xSensingHead in [shTK930F, shTK940F, shTK930S] then
//    Result := shc9xFS
//  else if xSensingHead in [shTK930H] then
//    Result := shc9xH
//  else if xSensingHead in [shTK940BD] then
//    Result := shc9xBD
//  else
//    Result := shc8x;
end;





//:---------------------------------------------------------------------------
//:--- Class: TSensingHeadClassList
//:---------------------------------------------------------------------------
constructor TSensingHeadClassList.Create;
begin
  Capacity := cZESpdGroupLimit;
end;

//:---------------------------------------------------------------------------
destructor TSensingHeadClassList.Destroy;
begin
  Clear;
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TSensingHeadClassList.Add(aSensingHeadClass: TSensingHeadClass): Integer;
//var
//  xPSensingHeadClass: PTSensingHeadClass;
begin
  Result := inherited Add(Pointer(aSensingHeadClass));
{wss
  New(xPSensingHeadClass);
  xPSensingHeadClass^ := aSensingHeadClass;
  Result              := inherited Add(xPSensingHeadClass);
{}
end;

//:---------------------------------------------------------------------------
procedure TSensingHeadClassList.Clear;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    if Assigned(inherited Items[i]) then
      Dispose(inherited Items[i]);

  inherited Clear;
end;

//:---------------------------------------------------------------------------
function TSensingHeadClassList.GetItems(Index: Integer): TSensingHeadClass;
//var
//  xPSensingHeadClass: PTSensingHeadClass;
begin
  Result := TSensingHeadClass(Integer(inherited Items[Index]));
{wss
  xPSensingHeadClass := inherited Items[Index];
  Result             := xPSensingHeadClass^;
{}
end;

//:---------------------------------------------------------------------------
procedure TSensingHeadClassList.SetItems(Index: Integer; aValue: TSensingHeadClass);
begin
  inherited Items[Index] := Pointer(aValue);
end;

//:---------------------------------------------------------------------------
procedure TSensingHeadClassList.Sort;
begin
  inherited Sort(SensingHeadClassCompare);
end;

end.
