(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMParaEditBox.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...: EditBoxes for YarnMaster Basic Settings.
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.01.2000  0.00  Kr  | Initial Release
| 17.01.2002  0.00  Wss | ShowMode = Extended enabled to show ReadOnly mode
|       2005  0.01  Wss | Umbau nach XML
| 14.06.2005  0.01  Wss | Wiederholungen und Ext P-Params eigene Komponente
| 27.07.2006  0.01  Wss | Komponente f�r IPI Limits hinzugef�gt
| 06.09.2006  0.01  Wss | Komponente IPI Limits wieder entfernt
| 20.12.2007  1.00  Nue | TK ZENITC Behandlungen zugef�gt.
| 09.01.2008        Nue | Einbau VCV-Repetition.
| 12.06.2008        Nue | Anpassungen wegen Handlind Spleissklassierung in SaveSensingHead.
|=============================================================================*)
unit YMBasicProdParaBox;                  

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmGroupBox, Spin, mmLabel, ExtCtrls, mmBevel, ComCtrls,
  mmHeaderControl, mmEdit, mmPanel, mmCheckBox, Buttons, mmBitBtn, MMUGlobal,
  PresetValue, mmSpinEdit, mmStaticText, YMParaUtils, YMParaDef, KeySpinEdit,
  DialogConfigCode, mmLineLabel, mmComboBox, XMLDef, XMLSettingsModel,
  VCLXMLSettingsModel, xmlMaConfigClasses, YMParaEditBox, EditBox;

resourcestring
  rsSensingHeadClass     = '(28)Tastkopf-Klasse'; //ivlm
  rsSFSSensitifityHint   = '(*)SFS Empfindlichkeitsfaktor:'; //ivlm
  rsDFSSensitifityHint   = '(*)DFS Empfindlichkeitsfaktor:'; //ivlm
  rsReductionHint        = '(*)Reduktion der Numerabweichungs- und Fehlerschwarm-Empfindlichkeit waehrend den ersten 12km des Feinabgleichs:'; //ivlm
  rsSwOff                = '(3)&Aus'; //ivlm
  rsAdjustModeSingle     = '(*)Einzel'; // ivlm
  rsAdjustModeContinuous = '(*)Kontinuierlich'; // ivlm
//  rsYarnCountRepHint = '(*)Nummerabweichungsschnitt-Wiederholungen bis zur Alarmintervention:';
//  rsFaultClusterRepHint = '(*)Fehlerschwarmschnitt-Wiederholungen bis zur Alarmintervention:';
//  rsSFIRepHint = '(*)SFI Schnitt-Wiederholungen bis zur Alarmintervention:';
//  rsFFClusterRepHint = '(*)FF Schwarmschnitt-Wiederholungen bis zur Alarmintervention:';
//  rsFFStartupRepHint = '(*)FF Anlaufschnitt-Wiederholungen bis zur Alarmintervention:';
  //...........................................................................

const
  //==== Propery index for production group specific settings ====
  cPGYarnCount    = 0;
  cPGYarnUnit     = 1;
  cPGPilotSpindle = 2;
  cPGSpeedRamp    = 3;
  cPGSpeed        = 4;

  //==== Propery index for machine specific config code ====
  cMConfigA = 1;
  cMConfigB = 2;
  cMConfigC = 3;

  //==== Propery index for enabeling entry fields ====
  cBSSSWinder      = 0;
  cBSFFDetection   = 1;
  cBSCBFFDetection = 2;
  cBSFFAlarm       = 3;
  //...........................................................................

  cVSBSSFSSensitifity: TValueSpecRec = (
    MinValue: 1.0; MaxValue: 2.0;
    Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
    Caption: '';
    Hint: rsSFSSensitifityHint;
    XPValue: cXPSFSSensitivityItem;
    XPSwitch: '');

  cPTBSSFSSensitifity: array[0..1] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ));
  //...........................................................................

  cVSBSDFSSensitifity: TValueSpecRec = (
    MinValue: 1.0; MaxValue: 4.0;
    Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
    Caption: '';
    Hint: rsDFSSensitifityHint;
    XPValue: cXPDFSSensitivity1Item;
    XPSwitch: '');

  cPTBSDFSSensitifity: array[0..2] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '4'; ));
  //...........................................................................

  cVSBSReduction: TValueSpecRec = (
    MinValue: cMinFadjSettings; MaxValue: cMaxFadjSettings;
    Space: 0; Factor: 1;     Format: '%3.0f'; Measure: '';
    Caption: '';
    Hint: rsReductionHint;
    XPValue: cXPReductionItem;
    XPSwitch: '');

  cPTBSReduction: array[0..6] of TPresetTextRec = (
    (typ: ttFloat; text: '0'; ),
    (typ: ttFloat; text: '25'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttFloat; text: '75'; ),
    (typ: ttFloat; text: '100'; ),
    (typ: ttFloat; text: '150'; ),
    (typ: ttCmdDisabled; text: rsSwOff; ));
  //...........................................................................

//  cVSBSYarnCountRep: TValueSpecRec = (
//    MinValue: cMinRepetitions-1; MaxValue: cMaxRepetitions-1;
//    Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: '';
//    Hint: rsYarnCountRepHint;
//    XPValue: cXPYarnCountRepetitionItem;
//    XPSwitch: '');

//  cVSBSFaultClusterRep: TValueSpecRec = (
//    MinValue: cMinRepetitions-1; MaxValue: cMaxRepetitions-1;
//    Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: '';
//    Hint: rsFaultClusterRepHint;
//    XPValue: cXPSFS_EMPFItem;
//    XPSwitch: '');

//TODO XML: Repetitionen gruppiert mit den anderen Parametern?
//  cVSBSSFIRep: TValueSpecRec = (
//    MinValue: cMinRepetitions-1; MaxValue: cMaxRepetitions-1;
//    Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: '';
//    Hint: rsSFIRepHint;
//    XPValue: cXPYMSettingSFIRepetitionItem;
//    XPSwitch: '');


//  cVSBSFFClusterRep: TValueSpecRec = (
//    MinValue: cMinRepetitions-1;  MaxValue: cMaxRepetitions-1;
//    Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: '';
//    Hint: rsFFClusterRepHint;
//    XPValue: cXPDarkClusterRepetitionItem;
//    XPSwitch: '');

//  cVSBSFFStartupRep: TValueSpecRec = (
//    MinValue: cMinRepetitions-1; MaxValue: cMaxRepetitions-1;
//    Space: 0; Factor: 1; Format: '%1.0f'; Measure: '';
//    Caption: '';
//    Hint: rsFStartupRepHint;
//    XPValue: cXPFStartupRepetitionItem;
//    XPSwitch: '');

//  cPTBSRepetition: array[0..4] of TPresetTextRec = (
//    (typ: ttFloat; text: '1'; ),
//    (typ: ttFloat; text: '2'; ),
//    (typ: ttFloat; text: '3'; ),
//    (typ: ttFloat; text: '4'; ),
//    (typ: ttFloat; text: '5'; ));
  //...........................................................................

type
  TYMSettinsGUIMode = (gmTemplate, gmAssignment, gmTransparent);
 //...........................................................................

  TOnLoadYMSettings = procedure of object;

  TBasicProdSettingsRec = record
    cntRep: Byte;
    clusterRep: Byte;
    siroStartupRep: Byte;
    sFIRep: Byte;
    FFClusterRep: Byte;
    fAdjConeReduction: Byte;
    checkLen: Word;
    configA: Word;
    configB: Word;
    configC: Word;
  end;
  

  TGBBasicProdPara = class(TFrame)
    mbDefault: TmmBitBtn;
    cbAdditionalCutOnBreak: TmmCheckBox;
    cbBunchMonitor: TmmCheckBox;
    cbFAdjustAfterOfflimitAlarm: TmmCheckBox;
    cbFAdjustOnOfflimit: TmmCheckBox;
    cbFClearingOnSplice: TmmCheckBox;
    cbFDetection: TmmCheckBox;
    cbSeparateSpliceSettings: TmmCheckBox;
    cobSensingHeadClass: TmmComboBox;
    edDFSSensitivity: TKeySpinEdit;
    edReduction: TKeySpinEdit;
    edSFSSensitivity: TKeySpinEdit;
    laDFSSensitivity: TmmLabel;
    laPilotCount: TmmLabel;
    laPilotLabel: TmmLabel;
    laReduction: TmmLabel;
    laReductionUnit: TmmLabel;
    laSensingHeadClass: TmmLabel;
    laSFSSensitivity: TmmLabel;
    laSpeed: TmmLabel;
    laSpeedLabel: TmmLabel;
    laSpeedRamp: TmmLabel;
    laSpeedRampLabel: TmmLabel;
    laSpeedUnit: TmmLabel;
    laStartupUnit: TmmLabel;
    laTimes1: TmmLabel;
    laTimes2: TmmLabel;
    mmLineLabel1: TmmLineLabel;
    mmLineLabel2: TmmLineLabel;
    cobAdjustMode: TmmComboBox;
    laAdjustMode: TmmLabel;
    pnConfigCode: TmmPanel;
    laCfgCodeALabel: TmmLabel;
    laCfgCodeA: TGBConfigCode;
    laCfgCodeB: TGBConfigCode;
    laCfgCodeBLabel: TmmLabel;
    laCfgCodeC: TGBConfigCode;
    laCfgCodeCLabel: TmmLabel;
    mmLineLabel4: TmmLineLabel;
    cbFModeBD: TmmCheckBox;
    cbPClearingOnSplice: TmmCheckBox;
    mRepetitionEditBox: TRepetitionEditBox;
    mPExtEditBox: TPExtEditBox;
    procedure DoClickConfigCode(aSender: TObject);
    procedure DoConfirm(aTag: Integer);
    procedure mbDefaultClick(Sender: TObject);
    procedure OnCobChange(Sender: TObject);
  private
    fConfigCodeVisible: Boolean;
    fModel: TVCLXMLSettingsModel;
    fOnConfirm: TOnConfirm;
    fReadOnly: Boolean;
    fYMSettinsGUIMode: TYMSettinsGUIMode;
    mBasicController: TBasicController;
    mDoChangedEnabled: Boolean;
//    mMachineConfigCode: TConfigurationCode;
//    mMachineYMConfig: TYMMachineConfig;
//    mProductionConfigCode: TConfigurationCode;
    mRange: TSpindleRangeRec;
    procedure CheckComponents;
    procedure cobSynchronizeSensingHeadClass;
    function GetEnable(const aIndex: Integer): Boolean;
    procedure InitSensingHeadClass;
    procedure PrepareTemplateMode;
    procedure PutEnable(const aIndex: Integer; const aValue: Boolean);
    procedure PutProdGrpYMPara(const aIndex, aValue: Integer);
    procedure SaveSensingHead;
    procedure SetConfigCodeVisible(const aValue: Boolean);
    procedure SetModel(const aValue: TVCLXMLSettingsModel);
    procedure SetReadOnly(const aValue: Boolean);
    procedure SetYMSettinsGUIMode(const aValue: TYMSettinsGUIMode);
    procedure UpdateConfigCode;
    property EnableCBFFDetection: Boolean index cBSCBFFDetection read GetEnable write PutEnable;
    property EnableFFAlarm: Boolean index cBSFFAlarm read GetEnable write PutEnable;
    property EnableFFDetection: Boolean index cBSFFDetection read GetEnable write PutEnable;
    property EnableSSWinder: Boolean index cBSSSWinder read GetEnable write PutEnable;
  protected
    procedure MachineInitialize(Sender: TBaseXMLSettingsObserver);
    procedure Notification(aComponent: TComponent; Operation: TOperation); override;
    procedure ValueSave(aTag: Integer);
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetFabFPattern;
    property ConfigCodeVisible: Boolean read fConfigCodeVisible write SetConfigCodeVisible;
    property Mode: TYMSettinsGUIMode read fYMSettinsGUIMode write SetYMSettinsGUIMode;
    property OnConfirm: TOnConfirm read fOnConfirm write fOnConfirm;
    property ProdGrpPilot: Integer index cPGPilotSpindle write PutProdGrpYMPara;
    property ProdGrpSpeed: Integer index cPGSpeed write PutProdGrpYMPara;
    property ProdGrpSpeedRamp: Integer index cPGSpeedRamp write PutProdGrpYMPara;
    property ReadOnly: Boolean read fReadOnly write SetReadOnly;
  published
    property Model: TVCLXMLSettingsModel read fModel write SetModel;
  end;



const
  cDefaultProdSettings: TBasicProdSettingsRec = (
    cntRep: cDefaultRepetitions;
    clusterRep: cDefaultClusterRep;
    siroStartupRep: cDefaultSiroStartupRep;
    sFIRep: cDefaultRepetitions;
    FFClusterRep: cDefaultRepetitions;
    fAdjConeReduction: cDefaultFadjSetting;
    checkLen: cDefaultYCCheckLength;
    configA: cCCADefault;
    configB: cCCBDefault;
    configC: cCCCDefault;
    );

implementation 
uses
  mmMBCS, typinfo, BaseGlobal, mmcs, XMLGlobal;
{$R *.DFM}

//:---------------------------------------------------------------------------
//:--- Class: TGBBasicProdPara
//:---------------------------------------------------------------------------
constructor TGBBasicProdPara.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fOnConfirm         := nil;
  fConfigCodeVisible := False;

  mBasicController := TBasicController.Create;
  mBasicController.OnValueUpdate       := ValueUpdate;
  mBasicController.OnMachineInitialize := MachineInitialize;

  mDoChangedEnabled     := True;
  mRange.contents       := cSpdlRngNotDefined;
//  mMachineYMConfig      := TYMMachineConfig.Create;
//  mProductionConfigCode := TConfigurationCode.Create;
//  mMachineConfigCode    := TConfigurationCode.Create;

  edSFSSensitivity.LoadValueSpec(cVSBSSFSSensitifity, cPTBSSFSSensitifity);
  edSFSSensitivity.Value     := 1;
//  edSFSSensitivity.OnConfirm := DoConfirmConfig;

  edDFSSensitivity.LoadValueSpec(cVSBSDFSSensitifity, cPTBSDFSSensitifity);
  edDFSSensitivity.Value     := 2;
//  edDFSSensitivity.OnConfirm := DoConfirmConfig;

  cobAdjustMode.Items.Add(rsAdjustModeSingle);
  cobAdjustMode.Items.Add(rsAdjustModeContinuous);

  edReduction.LoadValueSpec(cVSBSReduction, cPTBSReduction);
  edReduction.Value     := 0;
  edReduction.TextType  := ttFloat;
  edReduction.OnConfirm := DoConfirm;

//  edYarnCountRep.LoadValueSpec(cVSBSYarnCountRep, cPTBSRepetition);
//wss  edFaultClusterRep.LoadValueSpec(cVSBSFaultClusterRep, cPTBSRepetition);
//  edSFIRep.LoadValueSpec(cVSBSSFIRep, cPTBSRepetition);
//  edFClusterRep.LoadValueSpec(cVSBSFFClusterRep, cPTBSRepetition);
//  edFStartupRep.LoadValueSpec(cVSBSFFStartupRep, cPTBSRepetition);
  
  laCfgCodeA.DialogCaption := 'ConfigCodeA';
  laCfgCodeB.DialogCaption := 'ConfigCodeB';
  laCfgCodeC.DialogCaption := 'ConfigCodeC';

//  EnableSSWinder       := False;
//  EnableFFDetection    := True;

  ReadOnly             := False;
  Mode                 := gmTransparent;
end;

//:---------------------------------------------------------------------------
destructor TGBBasicProdPara.Destroy;
begin

  mBasicController.Free;
//  mMachineYMConfig.Free;
//  mProductionConfigCode.Free;
//  mMachineConfigCode.Free;

  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.CheckComponents;
var
  xIsZenit: Boolean;
begin
  xIsZenit := IsZenit(mBasicController.Model);
  // TODO wss: was muss hier noch gemacht werden?
  EnableSSWinder := mBasicController.Model.FPHandler.Value[cXPFP_SpeedSimulationItem];

  //--- Garnspezifisch ---
  // Bedingung: Applikation setzt aussen die Sichtweise. Entweder gesamte Maschine oder Spindelbereich
//      Enabled := not ReadOnly and GetXPBoolean(cXPFItem); // ava �ber sensinghead
//      Enabled := not ReadOnly and FFDetectionAvailable; // ava �ber sensinghead
  cbFDetection.Enabled := (not ReadOnly) and IsFSensingHead(mBasicController.Model); // ava �ber sensinghead

//    cCfgBFFAdjAtOfflimitAvailable:
//      Result := GetBit(cCfgAFFDetectionAvailable) and GetBit(cCfgAFFDetection);
// available �ber SensingHead, FDet �ber CfgA
//      Enabled := not ReadOnly and FFAdjOnOfflimitAvailable;
  cbFAdjustOnOfflimit.Enabled := (not(ReadOnly or xIsZenit)) and cbFDetection.Enabled and cbFDetection.Checked;

//    cCfgBFFAdjAfterAlarmAvailable:
//      Result := GetBit(cCfgAFFDetectionAvailable) and GetBit(cCfgAFFDetection) and
//                not GetBit(cCfgBFFAdjAtOfflimit);
//    cCfgBFFAdjAtOfflimit:
//      Result := (fConfigB and cCCBNoFFAdjAtOfflimitBit) = 0;
//      Enabled := not ReadOnly and FFAdjAfterAlarmAvailable;
// TODO XML: CfgB.4 noch definieren
  cbFAdjustAfterOfflimitAlarm.Enabled := cbFAdjustOnOfflimit.Enabled and (not cbFAdjustOnOfflimit.Checked);
//  cbFAdjustAfterOfflimitAlarm.Enabled := (not(ReadOnly or xIsZenit)) and cbFDetection.Enabled and cbFDetection.Checked and (not cbFAdjustOnOfflimit.Checked);

  edSFSSensitivity.ReadOnly := Self.ReadOnly;
  edDFSSensitivity.ReadOnly := Self.ReadOnly;
  edReduction.ReadOnly      := fReadOnly;
  cobAdjustMode.Enabled     := mBasicController.Model.FPHandler.Value[cXPFP_ExtendedClusterItem];
  cobAdjustMode.ReadOnly    := fReadOnly;

  //--- Allgemein ---
//  if Mode = gmTemplate then cobSensingHeadClass.Enabled  := not fReadOnly
//                       else cobSensingHeadClass.ReadOnly := fReadOnly;
//  cobSensingHeadClass.Enabled  := (Mode = gmTemplate);
  cobSensingHeadClass.ReadOnly := fReadOnly or (Mode <> gmTemplate);

  cbSeparateSpliceSettings.Enabled := not fReadOnly;
//    cCfgAFFClearingOnSpliceAvailable:
//      Result := GetBit(cCfgAFFDetectionAvailable) and GetBit(cCfgAFFDetection);
//      Enabled := not ReadOnly and FFClearingOnSpliceAvailable;
  cbFClearingOnSplice.Enabled    := cbFDetection.Enabled and cbFDetection.Checked;
  cbPClearingOnSplice.Enabled    := (not fReadOnly) and mBasicController.Model.FPHandler.Value[cXPFP_PItem];
  cbBunchMonitor.Enabled         := not fReadOnly;
  cbAdditionalCutOnBreak.Enabled := not fReadOnly;
  // Nur Zenit: Hell UND Dunkel F-Matrix anzeigen und verwenden
  cbFModeBD.Enabled              := cbFDetection.Enabled and cbFDetection.Checked and xIsZenit;
  mRepetitionEditBox.ReadOnly    := fReadOnly;
  mPExtEditBox.ReadOnly          := fReadOnly;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.cobSynchronizeSensingHeadClass;
var
  xSHClass: TSensingHeadClass;
begin
  xSHClass := GetSensingHeadClass(mBasicController.Model);
  cobSensingHeadClass.ItemIndex := cobSensingHeadClass.Items.IndexOfObject(TObject(xSHClass));
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.DoClickConfigCode(aSender: TObject);
begin
  ValueSave(TControl(aSender).Tag);
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.DoConfirm(aTag: Integer);
begin
  ValueSave(aTag);
end;

//:---------------------------------------------------------------------------
function TGBBasicProdPara.GetEnable(const aIndex: Integer): Boolean;
begin
  Result := False;
  
  case aIndex of
    cBSSSWinder:
      begin
        Result := laSpeed.Enabled;
      end;

    cBSFFDetection:
      begin
        Result := cbFAdjustOnOfflimit.Enabled;
      end;
    cBSFFAlarm:
      Result := cbFAdjustAfterOfflimitAlarm.Enabled;
  end;
  
end;

procedure TGBBasicProdPara.InitSensingHeadClass;
var
  xSHClass: TSensingHeadClass;
begin
  if Mode in [gmAssignment, gmTransparent] then begin
    cobSensingHeadClass.Items.Clear;
    xSHClass := GetSensingHeadClass(mBasicController.Model);
    cobSensingHeadClass.Items.AddObject(cGUISensingHeadClassNames[xSHClass], TObject(xSHClass));
  end;
  cobSynchronizeSensingHeadClass;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.mbDefaultClick(Sender: TObject);
begin
//TODO wss: muss hier noch was gemacht werden?
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.OnCobChange(Sender: TObject);
begin
  ValueSave(TControl(Sender).Tag);
end;

//:---------------------------------------------------------------------------
//1 Anhand des Bauzustandes das Erscheinungsbild festlegen
procedure TGBBasicProdPara.MachineInitialize(Sender: TBaseXMLSettingsObserver);
begin
  CheckComponents;
  InitSensingHeadClass;
end;

//------------------------------------------------------------------------------
procedure TGBBasicProdPara.Notification(aComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);

  if (Operation = opRemove) and (aComponent = fModel) then begin
    Model := nil;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.PrepareTemplateMode;
var
  xSHClass: TSensingHeadClass;
  xSHSet: TSensingHeadClassSet;
begin
//  cobSensingHeadClass.Enabled := (fYMSettinsGUIMode = gmTemplate);
// TODO wss: Enabled mit MMSecurity kontrollieren -> ActionHandle
//  cobSensingHeadClass.Enabled := True;
  if cobSensingHeadClass.Enabled and Assigned(fModel) then begin
    xSHSet := fModel.AvailableFabSensingHeadClasses;
    cobSensingHeadClass.Items.Clear;
    //fFabMaConfig mit property FabMaConfig zuvor von aussen setzten
    for xSHClass:=Low(TSensingHeadClass) to High(TSensingHeadClass) do begin
      if xSHClass in xSHSet then
        cobSensingHeadClass.Items.AddObject(cGUISensingHeadClassNames[xSHClass], TObject(xSHClass));
    end; // for xSHClass
  end
  else begin  //Standardklasse einf�llen
    // TODO wss: Sinn und Zweck von einer Default Klasse wenn keine Fabrik vorhanden ist? -> abkl�ren
    cobSensingHeadClass.Items.AddObject(cGUISensingHeadClassNames[shc9xFS], TObject(shc9xFS));
  end; //if Assigned(fFabMaConfig)

  cobSynchronizeSensingHeadClass;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.PutEnable(const aIndex: Integer; const aValue: Boolean);
begin

  case aIndex of
    cBSSSWinder:
      begin
        laSpeed.Enabled := aValue;
        laSpeedRamp.Enabled := aValue;
      end;

    cBScbFFDetection:
      begin
        cbFAdjustOnOfflimit.Enabled := aValue;
        EnableFFAlarm               := aValue;
        cbFClearingOnSplice.Enabled := aValue;
      end;

    cBSFFDetection:
      begin
        EnableCBFFDetection := aValue;
      end;

    cBSFFAlarm:
      if cbFAdjustOnOfflimit.Enabled then
        cbFAdjustAfterOfflimitAlarm.Enabled := aValue
      else
        cbFAdjustAfterOfflimitAlarm.Enabled := False;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.PutProdGrpYMPara(const aIndex, aValue: Integer);
begin
  case aIndex of
    cPGPilotSpindle: begin
        laPilotCount.Enabled := (aValue <> 0);
        if laPilotCount.Enabled then laPilotCount.Value   := aValue
                                else laPilotCount.Caption := '';
      end;

    cPGSpeedRamp:
      if aValue <> 0 then
        laSpeedRamp.Caption := Format('%d', [aValue])
      else begin
        laSpeedRamp.Caption := '';
        EnableSSWinder := False;
      end;

    cPGSpeed:
      if aValue <> 0 then
        laSpeed.Caption := Format('%d', [aValue])
      else begin
        laSpeed.Caption := '';
        EnableSSWinder := False;
      end;
  end;

//  case aIndex of
//    cPGPilotSpindle:
//      if aValue <> 0 then begin
//        laPilotLabel.Enabled := True;
//        laPilotCount.Enabled := True;
//        laPilotCount.Value   := aValue;
//      end
//      else begin
//        laPilotLabel.Enabled := False;
//        laPilotCount.Enabled := False;
//        laPilotCount.Caption := '';
//      end;
//
//    cPGSpeedRamp:
//      if aValue <> 0 then
//        laSpeedRamp.Caption := Format('%d', [aValue])
//      else begin
//        laSpeedRamp.Caption := '';
//        EnableSSWinder := False;
//      end;
//
//    cPGSpeed:
//      if aValue <> 0 then
//        laSpeed.Caption := Format('%d', [aValue])
//      else begin
//        laSpeed.Caption := '';
//        EnableSSWinder := False;
//      end;
//  end;

end;

procedure TGBBasicProdPara.SaveSensingHead;
var
  xSHClass: TSensingHeadClass;
begin
  if cobSensingHeadClass.ItemIndex <> -1 then begin
    xSHClass := TSensingHeadClass(cobSensingHeadClass.Items.Objects[cobSensingHeadClass.ItemIndex]);
//    if xSHClass in [shc8x, shcZenit] then mBasicController.Value[cXPFSensorActiveItem] := False;
//    if xSHClass in [shc8x..shc9xH] then   mBasicController.Value[cXPFModeBDItem]       := 0; // FModeBD erwartet 0/1 und nicht False/True
//    if xSHClass in [shcZenit..shcZenitFP] then begin
// Anpassungen wegen Einbau TK ZENITC  (Nue:20.12.07)
    if xSHClass in [shc8x, shcZenit, shcZenitC] then mBasicController.Value[cXPFSensorActiveItem] := False;
    if xSHClass in [shc8x..shc9xH] then begin
      mBasicController.Value[cXPFModeBDItem]       := 0; // FModeBD erwartet 0/1 und nicht False/True
      mBasicController.Model.FPHandler.Value[cXPFP_ZenitItem] := False;    //Nue:10.06.08
      mBasicController.Model.FPHandler.Value[cXPFP_ZenitCItem] := False;   //Nue:10.06.08
    end;
    if xSHClass in [shcZenit..shcZenitCFP] then begin
      mBasicController.Value[cXPFAdjustAfterAlarmItem] := False;
      mBasicController.value[cXPFAdjustAtOfflimitItem] := False;
    end;

    SetSensingHeadClass(mBasicController.Model, xSHClass);
//    mBasicController.Model.Changed([ckBasic], mBasicController);   bis 10.06.08
      mBasicController.Model.Changed([ckBasic,ckSplice], mBasicController);  //Nue:10.06.08
  end;
{    //Nue:10.06.08
    if IsZenitOrHigher(mBasicController.Model) then
      mBasicController.Model.Changed([ckBasic,ckSplice], mBasicController)  //Nue:10.06.08
    else
      mBasicController.Model.Changed([ckBasic], mBasicController);
}
//  if cobSensingHeadClass.ItemIndex <> -1 then begin
//    xSHClass := TSensingHeadClass(cobSensingHeadClass.Items.Objects[cobSensingHeadClass.ItemIndex]);
//    if xSHClass in [shc8x, shcZenit] then mBasicController.Change(cXPFSensorActiveItem, False);
//    if xSHClass in [shc8x..shc9xH] then   mBasicController.Change(cXPFModeBDItem, 0); // FModeBD erwartet 0/1 und nicht False/True
//    if xSHClass in [shcZenit..shcZenitFP] then begin
//      mBasicController.Value[cXPFAdjustAfterAlarmItem] := False;
//      mBasicController.value[cXPFAdjustAtOfflimitItem] := False;
//    end;
//
//    SetSensingHeadClass(mBasicController.Model, xSHClass);
//    mBasicController.Model.Changed([], mBasicController);
//  end;
end;

procedure TGBBasicProdPara.SetConfigCodeVisible(const aValue: Boolean);
begin
  if fConfigCodeVisible <> aValue then begin
    fConfigCodeVisible := aValue;
    pnConfigCode.Visible := fConfigCodeVisible;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.SetFabFPattern;
var
  xSHC: TSensingHeadClass;
begin
  // Verwendung nur im Template Mode
  // Ausserhalb wurde ein neues XML ins Model "hinheingeschoben". Dadurch wird das Fabrik FPattern �berschrieben
  // Aaaaalso: Nun muss erst die aktuelle SensingHeadClass gemerkt werden...
  if cobSensingHeadClass.ItemIndex <> -1 then
    xSHC := TSensingHeadClass(cobSensingHeadClass.Items.Objects[cobSensingHeadClass.ItemIndex])
  else
    xSHC := shcNone;

  // Dann: ...das Fabrik FPattern wieder unterschieben....
  fModel.SetFabFPattern;
  // ...und Schlussendlich die gemerkte SensingHead Klasse wieder versuchen zu setzen
  cobSensingHeadClass.ItemIndex := cobSensingHeadClass.Items.IndexOfObject(TObject(xSHC));
  SaveSensingHead;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  if fModel <> aValue then begin
    if Assigned(fModel) then
      fModel.UnregisterObserver(mBasicController);

    fModel := aValue;

    // Ein neues Model wird verlinkt -> die Observer registrieren lassen
    // Durch das Registrieren wird vom Model �ber den Observer ein ValueUpdate ausgel�st
    if Assigned(fModel) then
      fModel.RegisterObserver(mBasicController);

    // Den ConfigCode Labels das Modell bekanntmachen
    laCfgCodeA.Model := fModel;
    laCfgCodeB.Model := fModel;
    laCfgCodeC.Model := fModel;

    mRepetitionEditBox.Model := fModel;
    mPExtEditBox.Model       := fModel;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.SetReadOnly(const aValue: Boolean);
begin
  if aValue <> fReadOnly then begin
    fReadOnly := aValue;

    mbDefault.Enabled := not fReadOnly;
    if Assigned(mBasicController.Model) then
      CheckComponents;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.SetYMSettinsGUIMode(const aValue: TYMSettinsGUIMode);
begin
  fYMSettinsGUIMode := aValue;
  if fYMSettinsGUIMode = gmTemplate then begin
    cobSensingHeadClass.ReadOnly := fReadOnly;
    PrepareTemplateMode;
  end else
    cobSensingHeadClass.ReadOnly := True;
//    cobSensingHeadClass.Enabled := False;

//  cobSensingHeadClass.Enabled := (fYMSettinsGUIMode = gmTemplate);
//  if cobSensingHeadClass.Enabled then begin
//    if mMachineYMConfig.BuildMaxMachineConfig then
//      qr;
//  end;
//
//  UpdateMachineConfigCode;

//  case fYMSettinsGUIMode of
//    gmTemplate: begin
//        cobSensingHeadClass.Enabled := True;
//
//        if mMachineYMConfig.BuildMaxMachineConfig then
//          LoadTemplateMachineConfig;
//      end;
//
//    gmAssignment:
//        cobSensingHeadClass.Enabled := False;
//
//    gmTransparent:
//        cobSensingHeadClass.Enabled := False;
//  end;
//
//  UpdateMachineConfigCode;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.UpdateConfigCode;
var
  xInt: Integer;
  //...........................................................
  function GetXPBoolean(aXPath: String): Boolean;
  begin
    Result := mBasicController.ValueDef[aXPath, False];
  end;
  //...........................................................
begin
  with cbFDetection do begin
    Checked := GetXPBoolean(cXPFSensorActiveItem); //FFDetection;
  end;

  with cbFAdjustOnOfflimit do begin
    Checked := GetXPBoolean(cXPFAdjustAtOfflimitItem); //FFAdjOnOfflimit;
  end;

  with cbFAdjustAfterOfflimitAlarm do begin
    Checked := GetXPBoolean(cXPFAdjustAfterAlarmItem); //FFAdjAfterAlarm;
  end;

  with cbFClearingOnSplice do begin
    Checked := GetXPBoolean(cXPFClearingDuringSpliceItem); //FFClearingOnSplice;
  end;

  with cbBunchMonitor do begin
    Checked := GetXPBoolean(cXPBunchMonitorItem); //BunchMonitor;
  end;

  with cbAdditionalCutOnBreak do begin
    Checked := GetXPBoolean(cXPCutAtYarnBreakItem); //CutOnYarnBreak;
  end;

  with edSFSSensitivity do begin
    if GetXPBoolean(cXPSFSSensitivityItem) then Value := 2   // True:  2-fach
                                           else Value := 1;  // False: 1-fach
  end;

  with edDFSSensitivity do begin
    xInt := 1; // Default ist 1-fach
    if GetXPBoolean(cXPDFSSensitivity1Item) then xInt := xInt * 2; // 1 -> 2 fach
    if GetXPBoolean(cXPDFSSensitivity2Item) then xInt := xInt * 2; // 2 -> 4 fach
    Value := xInt;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.ValueSave(aTag: Integer);
begin
  if not ReadOnly and mDoChangedEnabled then begin
    case aTag of
      1: mBasicController.Change(cXPFSensorActiveItem, cbFDetection.Checked);
      2: begin
          if cbFAdjustAfterOfflimitAlarm.Checked then
            mBasicController.Value[cXPFAdjustAfterAlarmItem] := False;
          mBasicController.Change(cXPFAdjustAtOfflimitItem, cbFAdjustOnOfflimit.Checked);
        end;
      3: mBasicController.Change(cXPFAdjustAfterAlarmItem, cbFAdjustAfterOfflimitAlarm.Checked);
      4: mBasicController.Change(cXPSFSSensitivityItem, (edSFSSensitivity.Value > 1)); //SFS Empfindlichkeit
      5: begin //DFS Empfindlichkeit
          // 1 = 00; 2 = 10; 3,4 = 11
          // 1. Wert "manuel" schreiben damit Change Event erst beim 2. Bit ausgel�stwird
          mBasicController.Value[cXPDFSSensitivity1Item] := (edDFSSensitivity.Value > 1);
          mBasicController.Change(cXPDFSSensitivity2Item, (edDFSSensitivity.Value > 2));
        end;
      6:  if edReduction.TextType = ttCmdDisabled then // Reduktion bei Feinabgleich
            mBasicController.Change(cXPReductionItem, cFineAdjSettingsOff)
          else
            mBasicController.Change(cXPReductionItem, edReduction.Value);
      7:  SaveSensingHead;
      // TODO: Wird hier der richtige XPath verwendet? -->> cXPCheckLengthItem
      // Anhand diesem Wert wird entschieden ob der Parameter CheckLength von der Sektion �bernommen wird oder 0 ist
      8:  mBasicController.Change(cXPSeparateSpliceCheckSettingsItem, cbSeparateSpliceSettings.Checked); // Gesonderte Spleichpr�feinstellung
      9:  mBasicController.Change(cXPFClearingDuringSpliceItem, cbFClearingOnSplice.Checked); // F Reinigung w�hrend Spleisspr�fung
      10: mBasicController.Change(cXPBunchMonitorItem, cbBunchMonitor.Checked); // Schlingen�berwachung
      11: mBasicController.Change(cXPCutAtYarnBreakItem, cbAdditionalCutOnBreak.Checked);  // Zusatzschnitt bei Fadenbruch
      13: mBasicController.Change(cXPModeItem, cobAdjustMode.ItemIndex);
      14: mBasicController.Change(cXPPClearingDuringSpliceItem, cbPClearingOnSplice.Checked); // P Reinigung w�hrend Spleisspr�fung
      15: if cbFModeBD.Checked then mBasicController.Change(cXPFModeBDItem, 1)
                               else mBasicController.Change(cXPFModeBDItem, 0);  // Erweiterte F Matrix (Hell)
    end;
    CheckComponents;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGBBasicProdPara.ValueUpdate(Sender: TBaseXMLSettingsObserver);
var
  i: Integer;
  xInt: Integer;
  xVar: Boolean;
  //...........................................................
  function GetXPBoolean(aXPath: String): Boolean;
  begin
    Result := mBasicController.ValueDef[aXPath, False];
  end;
  //...........................................................
begin
  mDoChangedEnabled := False;
  // Erst mal pr�fen, welche Komponenten verf�gbar sind...
  CheckComponents;
  //...dann die Werte auslesen

  //--- Garnspezifisch ---
//  laPilotCount.Value                  := mBasicController.Model.MaConfigReader.
  cbFDetection.Checked                := GetXPBoolean(cXPFSensorActiveItem);
  cbFAdjustOnOfflimit.Checked         := GetXPBoolean(cXPFAdjustAtOfflimitItem);
  cbFAdjustAfterOfflimitAlarm.Checked := GetXPBoolean(cXPFAdjustAfterAlarmItem);

  with edSFSSensitivity do begin
    if GetXPBoolean(cXPSFSSensitivityItem) then Value := 2   // True:  2-fach
                                           else Value := 1;  // False: 1-fach
  end;

  with edDFSSensitivity do begin
    xInt := 1; // Default ist 1-fach
    if GetXPBoolean(cXPDFSSensitivity1Item) then xInt := xInt * 2; // 1 -> 2 fach
    if GetXPBoolean(cXPDFSSensitivity2Item) then xInt := xInt * 2; // 2 -> 4 fach
    Value := xInt;
  end;

  i := mBasicController.ValueDef[cXPReductionItem, 0];
  if i = cFineAdjSettingsOff then
    edReduction.TextType := ttCmdDisabled
  else begin
    edReduction.TextType := ttFloat;
    edReduction.Value    := i;
  end;

  cobAdjustMode.ItemIndex := mBasicController.ValueDef[cXPModeItem, 0];


  //--- Allgemein ---

  // Wenn nicht im Template Modus dann die HeadClass vom aktuellen Model einf�llen
  // -> ComboBox ist in diesem Modus sowieso gesperrt und nicht editierbar
  InitSensingHeadClass;

  // Anhand diesem Wert wird entschieden ob der Parameter CheckLength von der Sektion �bernommen wird oder 0 ist
  try // tempor�r
    cbSeparateSpliceSettings.Checked := mBasicController.ValueDef[cXPSeparateSpliceCheckSettingsItem, False];
  except
  end;

  cbFClearingOnSplice.Checked    := GetXPBoolean(cXPFClearingDuringSpliceItem);
  cbPClearingOnSplice.Checked    := GetXPBoolean(cXPPClearingDuringSpliceItem);
  cbBunchMonitor.Checked         := GetXPBoolean(cXPBunchMonitorItem);
  cbAdditionalCutOnBreak.Checked := GetXPBoolean(cXPCutAtYarnBreakItem);

//  xVar := mBasicController.ValueDef[cXPFModeBDItem, 0];
//  if VarType(xVar) = vtBoolean then
//    cbFModeBD.Checked := xVar
//  else
//    cbFModeBD.Checked := (Integer(xVar) <> 0);
  cbFModeBD.Checked              := (mBasicController.ValueDef[cXPFModeBDItem, 0] <> 0);

  mDoChangedEnabled := True;
end;

end.

