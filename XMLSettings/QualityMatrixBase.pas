(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QualityMatrixBase.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Descriptio...:
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis. | Reason
|-------------------------------------------------------------------------------------------
| 23.03.1999  0.00  Kr   | Initial Release
|       2005  0.01  Wss  | Umbau nach XML
| 19.05.2005  0.01  Wss  | Konvertierungen von XML Floatzahlen korrigiert MMStrToFloat
| 26.10.2005  0.01  Wss  | Konstanten f�r Fixed QMatrix Width modifiziert f�r Floor
| 23.01.2008  0.02  Roda | In GetFieldID Initialisierung korrigiert (Result := TotFields-1 statt TotFields)
| 31.01.2008  0.03  Roda | Added function IsValidField().
|                        | Returns whether the coordinates are within a valid matrix field (true) or not (false)
| 28.04.2008  0.04  Nue  | R�ckg�ngig machen von Aenderungen in 0.02
|==========================================================================================*)
unit QualityMatrixBase;

interface

uses
  Windows, Classes, Dialogs, Graphics, ExtCtrls, SysUtils, QualityMatrixDef,
  QualityMatrixDesc, Controls;

const
  cHorizontal = 0;
  cVertical = 1;
  cField = 2;
  cXAxis = 3;
  cYAxis = 4;
  cCaption = 5;
  cSamples = 6;
  cGraphField = 7;
  cNSLGraphField = 8;
  cThinGraphField = 9;
  cSuperClass = 10;

  cCuts = 0;
  cDefects = 1;

  cCutActive = 0;
  cCutPassive = 1;
  cFFClusterActive = 2;
  cFFClusterPassive = 3;
  cSCMemberActive = 4;
  cSCMemberPassive = 5;

  //Relative borderwidth
  cBorderWidthRel = 3;

  cFixedWidthFMatrix = 400; //300;
  cFixedWidthQMatrix = 500; //560; //440;

type
  TPixelAid = class(TObject)
  public
    function AlignItem(aAlignPos, aItemLength: Integer; aAlign: TItemAlignement): TRangeRec;
    function PlaceItem(aBounds: TRangeRec; aItemLength: Integer; aAlign: TItemAlignement): TRangeRec;
  end;
  

  TQualityMatrixBase = class(TCustomControl)
  private
    fFixedSize: Boolean;
    fCoordinateMode: TCoordinateMode;
    fCutCurveSampleDivisor: Integer;
    fFModeBD: Boolean;
    fLastCutColor: TColor;
    fLastCutField: Integer;
    fLastCutMode: TLastCutMode;
    fMatrixSubType: TMatrixSubType;
    fMatrixType: TMatrixType;
    fNeighbour: TItemAlignement;
    fNumberOfSamples: Integer;
    fSubFieldX: Integer;
    fSubFieldY: Integer;
    mMatrix: TQualityMatrixDescRec;
    mTotCaptions: Integer;
    mTotFields: Integer;
    mTotHorLines: Integer;
    mTotSuperClasses: Integer;
    mTotVertLines: Integer;
    mTotXScales: Integer;
    mTotYScales: Integer;
    function AdjustBoundary(aToAdjustRect, aBoundaryRect: TRect; aCalcBase: TCalculationBase): TRect;
    procedure CalculateNumberOfSamples;
    function CalculatePixelToRaster(aPixelPos: TPoint): TPoint;
    function CalculateRasterToPixel(aLeft, aTop, aRight, aBottom: Word): TRect;
    function FindeLineIDs(aFieldID: Integer): TRect;
    function GetBackgroundColor(aID, aIndex: Integer): TBrushRec;
    function GetCoordinate(aID, aIndex: Integer): TRect;
    function GetGraphField(aIndex: Integer): TRect;
    function GetLastCutColor: TColor;
    function GetLastCutField: Integer;
    function GetLastCutMode: TLastCutMode;
    function GetLinePen(aID, aIndex: Integer): TPenRec;
    function GetNumberOfSamples(aID: Integer): Integer;
    function GetScale(aID, aIndex: Integer): TScaleRec;
    function GetSCField(aID: Integer): TRect;
    function GetSCMembers(aID: Integer): TSCMemberArray;
    function GetStateColor(aID, aIndex: Integer): TBrushRec;
    function GetText(aID, aIndex: Integer): TTextRec;
    function GetTextFormat(aID, aIndex: Integer): TFormatTextRec;
    function GetTotItems(aIndex: Integer): Integer;
    function GetVisibleNSLRange: TSingleRect;
    function GetVisibleTRange: TSingleRect;
    procedure SetFixedSize(const aValue: Boolean);
    procedure SetLastCutColor(const aValue: TColor);
    procedure SetLastCutField(const aValue: Integer);
    procedure SetLastCutMode(const aValue: TLastCutMode);
    procedure SetSubField(const aIndex, aValue: Integer);
    property NumberOfSamples[aID: Integer]: Integer read GetNumberOfSamples;
  protected
    procedure DefineSiroSubMatrix;
    procedure Resize; override;
    procedure SetFModeBD(const aValue: Boolean); virtual;
    procedure SetMatrixSubType(aValue: TMatrixSubType); virtual;
    procedure SetMatrixType(const aValue: TMatrixType); virtual;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure CalculateMatrixSize;
    procedure FillField(aFieldRect: TRect; aSelected: TBrushRec);
    function GetFieldID(aX, aY: Integer): Integer;
    function IsValidField(aX, aY: Integer): Boolean;
    function GetFieldWeight(aId: Integer): Real;
    function GetNeighbourFieldID(var aX, aY: Integer): Integer;
    property FixedSize: Boolean read fFixedSize write SetFixedSize;
    property BackgroundColor[aID: Integer]: TBrushRec index cField read GetBackgroundColor;
    property Caption[aID: Integer]: TTextRec index cCaption read GetText;
    property CoordinateMode: TCoordinateMode read fCoordinateMode write fCoordinateMode;
    property CutColor[aID: Integer]: TBrushRec index cCutActive read GetStateColor;
    property CutCurveSampleDivisor: Integer read fCutCurveSampleDivisor;
    property CutsFormat[aID: Integer]: TFormatTextRec index cCuts read GetTextFormat;
    property DefectsFormat[aID: Integer]: TFormatTextRec index cDefects read GetTextFormat;
    property FFClusterColor[aID: Integer]: TBrushRec index cFFClusterActive read GetStateColor;
    property Field[aID: Integer]: TRect index cField read GetCoordinate;
    property FModeBD: Boolean read fFModeBD write SetFModeBD;
    property GraphField: TRect index cGraphField read GetGraphField;
    property HorLine[aID: Integer]: TRect index cHorizontal read GetCoordinate;
    property HorLinePen[aID: Integer]: TPenRec index cHorizontal read GetLinePen;
    property NeighbourField: TItemAlignement read fNeighbour write fNeighbour;
    property NoCutColor[aID: Integer]: TBrushRec index cCutPassive read GetStateColor;
    property NoFFClusterColor[aID: Integer]: TBrushRec index cFFClusterPassive read GetStateColor;
    property NoSCMemberColor[aID: Integer]: TBrushRec index cSCMemberPassive read GetStateColor;
    property NSLGraphField: TRect index cNSLGraphField read GetGraphField;
    property SCField[aID: Integer]: TRect read GetSCField;
    property SCMemberColor[aID: Integer]: TBrushRec index cSCMemberActive read GetStateColor;
    property SCMembers[aID: Integer]: TSCMemberArray read GetSCMembers;
    property SubFieldX: Integer index cXAxis read fSubFieldX write SetSubField;
    property SubFieldY: Integer index cYAxis read fSubFieldY write SetSubField;
    property ThinGraphField: TRect index cThinGraphField read GetGraphField;
    property TotCaptions: Integer index cCaption read GetTotItems;
    property TotFields: Integer index cField read GetTotItems;
    property TotHorLines: Integer index cHorizontal read GetTotItems;
    property TotSamples: Integer index cSamples read GetTotItems;
    property TotSuperClasses: Integer index cSuperClass read GetTotItems;
    property TotVertLines: Integer index cVertical read GetTotItems;
    property TotXScales: Integer index cXAxis read GetTotItems;
    property TotYScales: Integer index cYAxis read GetTotItems;
    property VertLine[aID: Integer]: TRect index cVertical read GetCoordinate;
    property VertLinePen[aID: Integer]: TPenRec index cVertical read GetLinePen;
    property VisibleNSLRange: TSingleRect read GetVisibleNSLRange;
    property VisibleTRange: TSingleRect read GetVisibleTRange;
    property XScale[aID: Integer]: TScaleRec index cXAxis read GetScale;
    property XScaleText[aID: Integer]: TTextRec index cXAxis read GetText;
    property YScale[aID: Integer]: TScaleRec index cYAxis read GetScale;
    property YScaleText[aID: Integer]: TTextRec index cYAxis read GetText;
  published
    property Align;
    property Canvas;
    property Color;
    property Hint;
    property LastCutColor: TColor read GetLastCutColor write SetLastCutColor;
    property LastCutField: Integer read GetLastCutField write SetLastCutField;
    property LastCutMode: TLastCutMode read GetLastCutMode write SetLastCutMode;
    property MatrixSubType: TMatrixSubType read fMatrixSubType write SetMatrixSubType;
    property MatrixType: TMatrixType read fMatrixType write SetMatrixType;
    property ShowHint;
  end;
  

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMUGlobal, LoepfeGlobal, typinfo, mmCS;

//:---------------------------------------------------------------------------
//:--- Class: TQualityMatrixBase
//:---------------------------------------------------------------------------
constructor TQualityMatrixBase.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Anchors := [akLeft,akTop, akRight];

  fFixedSize := False;
  fFModeBD   := False;

  CoordinateMode := cmActual;
  NeighbourField := iaTop;

  // Set initial Properties
  MatrixType   := mtNone;
  LastCutMode  := lcNone;
  LastCutColor := clLime ;
  LastCutField := 0;

  SubFieldX := 0;
  SubFieldY := 0;
end;

//:---------------------------------------------------------------------------
destructor TQualityMatrixBase.Destroy;
begin
  //  Finalize(fCutState);
  //  Finalize(fCuts);
  //  Finalize(fDefects);
  
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.AdjustBoundary(aToAdjustRect, aBoundaryRect: TRect; aCalcBase: TCalculationBase): TRect;
begin
  Result := aToAdjustRect;
  case aCalcBase of
    cbAbsolute:
      begin
        if aToAdjustRect.Left < aBoundaryRect.Left then
        begin
  // Adjust Left Boundary
          Result.Left := aBoundaryRect.Left;
          if aToAdjustRect.Left = aToAdjustRect.Right then
  // Vertical Line
            Result.Right := aBoundaryRect.Left;
        end;
        if aToAdjustRect.Top < aBoundaryRect.Top then
        begin
  // Adjust Top Boundary
          Result.Top := aBoundaryRect.Top;
          if aToAdjustRect.Top = aToAdjustRect.Bottom then
  // Horizontal Line
            Result.Bottom := aBoundaryRect.Top;
        end;
        if aToAdjustRect.Right > aBoundaryRect.Right then
        begin
  // Adjust Right Boundary
          Result.Right := aBoundaryRect.Right;
          if aToAdjustRect.Right = aToAdjustRect.Left then
  // Vertical Line
            Result.Left := aBoundaryRect.Right;
        end;
        if aToAdjustRect.Bottom > aBoundaryRect.Bottom then
        begin
  // Adjust Bottom Boundary
          Result.Bottom := aBoundaryRect.Bottom;
          if aToAdjustRect.Bottom = aToAdjustRect.Top then
  // Horizontal Line
            Result.Top := aBoundaryRect.Bottom;
        end;
      end;
    cbRelative:
      begin
        Result.Left := aToAdjustRect.Left + aBoundaryRect.Left;
        Result.Top := aToAdjustRect.Top + aBoundaryRect.Top;
        Result.Right := aToAdjustRect.Right - aBoundaryRect.Right;
        Result.Bottom := aToAdjustRect.Bottom - aBoundaryRect.Bottom;
      end;
  end;
end;

//:---------------------------------------------------------------------------
{* Hat die Matrix eine Kurve so werden 2 Werte ermittelt: NumberOfSamples entspricht die Anzahl der Punkte/Feld f�r die Kurve und CutCurveSampleDivisor hat
etwas mit der virtuellen Reinigung zu tun.
*}
procedure TQualityMatrixBase.CalculateNumberOfSamples;
begin
  if Assigned(mMatrix.pCurveLayer) then begin
    with mMatrix.pCurveLayer^ do begin
      if fSubFieldX <> 0 then
        if SubFieldX < numberOfSamplePerScale then begin
          // Ist die Aufl�sung f�r die virtuelle Reinigung kleiner als von der Kurve selber,
          // dann wird der Divisor ermittelt
          // z.B. NrOfSample = 15, SubFieldX = 5 -> Divisor = 4
          fCutCurveSampleDivisor := (numberOfSamplePerScale div SubFieldX) + 1;
          fNumberOfSamples       := fCutCurveSampleDivisor * fSubFieldX;
        end
        else begin
          // Ist die virtuelle Aufl�sung Gr�sser oder Gleich dann wird Divisor = 1 und die
          // Feldaufl�sung der Kurve wird angepasst
          fCutCurveSampleDivisor := 1;
          fNumberOfSamples       := fSubFieldX
        end
      else begin
        // Ist keine virtuelle Aufl�sung gegeben, dann braucht es auch keinen Divisor
        fCutCurveSampleDivisor := 0;
        fNumberOfSamples       := numberOfSamplePerScale;
      end;
    end;
  end
  else begin
    // Matrix hat keine Kurve -> keine virtuelle noch Kurvenaufl�sung
    fCutCurveSampleDivisor := 0;
    fNumberOfSamples := 0;
  end;
end;

//:---------------------------------------------------------------------------
//1 Setzt die Mauskoordinaten in einen Index vom darunterliegenden Feld um 
function TQualityMatrixBase.CalculatePixelToRaster(aPixelPos: TPoint): TPoint;
var
  xRaster: Double;
begin
  with Result do begin
    if (ClientWidth <> 0) and (ClientHeight <> 0) then begin
      xRaster := aPixelPos.x * mMatrix.pRaster^.width / ClientWidth;
      x := aPixelPos.x * mMatrix.pRaster^.width div ClientWidth;
      if (xRaster - x) >= 0.95 then
        x := x + 1;
  (* Offset does not work because of TChannelCurve.GenerateCutCurve.IsInRect
    needs some more brain work!!
  
        xOffset := 0.01 * ClientHeight;
        if aPixelPos.y > xOffset then
          aPixelPos.y := aPixelPos.y - Round(xOffset);
  *)
  
      xRaster := aPixelPos.y * mMatrix.pRaster^.height / ClientHeight;
      y := (aPixelPos.y * mMatrix.pRaster^.height div ClientHeight);
      if (xRaster - y) >= 0.95 then
        y := y + 1;
    end
    else begin
      x := 0;
      y := 0;
    end;
  end;
end;

//:---------------------------------------------------------------------------
//1 Rechnet ein Feldindex in Bildschirmkoordinaten um (TRect)
function TQualityMatrixBase.CalculateRasterToPixel(aLeft, aTop, aRight, aBottom: Word): TRect;
begin
  with Result do
  begin
    if (mMatrix.pRaster^.width <> 0) and (mMatrix.pRaster^.height <> 0) then
    begin
      Left   := Round(aLeft * ClientWidth / mMatrix.pRaster^.width);
      Top    := Round(aTop * ClientHeight / mMatrix.pRaster^.height);
      Right  := Round(aRight * ClientWidth / mMatrix.pRaster^.width);
      Bottom := Round(aBottom * ClientHeight / mMatrix.pRaster^.height);

    end
    else
    begin
      Left := 0;
      Top := 0;
      Right := 0;
      Bottom := 0;
    end;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TQualityMatrixBase.CalculateMatrixSize;
var
  xSubType: TMatrixSubType;
  xMaxHeight: Integer;
  xMaxWidth: Integer;
  xHeight: Integer;
  xWidth: Integer;
  xFactor: Single;
begin
  if MatrixType = mtSiro then begin
    xSubType := fMatrixSubType;
    if (fMatrixSubType = mstZenitF) and (not fFModeBD) then
      xSubType := mstSiroH;

    if (xSubType = mstZenitF) and fFModeBD then xFactor := cH2WFactorFMatrix / 2
                                           else xFactor := cH2WFactorFMatrix;
  end else
    xFactor := cH2WFactorQMatrix;


  if Assigned(Parent) and (Parent is TControl) and (not fFixedSize) then begin
    xMaxHeight := Parent.Height;
    xMaxWidth  := Parent.Width;
  end
  else begin
    if MatrixType = mtSiro then
      xMaxWidth  := cFixedWidthFMatrix
    else
      xMaxWidth  := cFixedWidthQMatrix;
    xMaxHeight := Trunc(cFixedWidthQMatrix / xFactor);
  end;

  xHeight := Trunc(xMaxWidth / xFactor);
  if xHeight > xMaxHeight then begin
    xHeight := xMaxHeight;
    xWidth  := Trunc(xMaxHeight * xFactor);
  end else
    xWidth  := xMaxWidth;

  Self.Height := xHeight;
  Self.Width  := xWidth;

//  if Assigned(Parent) and (Parent is TControl) then begin
//    if MatrixType = mtSiro then begin
//      xSubType := fMatrixSubType;
//      if (fMatrixSubType = mstZenitF) and (not fFModeBD) then
//        xSubType := mstSiroH;
//
//      if (xSubType = mstZenitF) and fFModeBD then xFactor := cH2WFactorFMatrix / 2
//                                             else xFactor := cH2WFactorFMatrix;
//    end else
//      xFactor := cH2WFactorQMatrix;
//
//    xHeight := Trunc(Parent.Width / xFactor);
//    if xHeight > Parent.Height then begin
//      xHeight := Parent.Height;
//      xWidth  := Trunc(Parent.Height * xFactor);
//    end else
//      xWidth  := Parent.Width;
//
//    Height := xHeight;
//    Width  := xWidth;
//  end;
//

//  if MatrixType = mtSiro then begin
//    xSubType := fMatrixSubType;
//    if (fMatrixSubType = mstZenitF) and (not fFModeBD) then
//      xSubType := mstSiroH;
//
//// TODO wss: H�he/Breite bei ZenitF Matrix mit Faktor 1.8 berechnen...
//    if (xSubType = mstZenitF) and fFModeBD then Height := Round((Width / cH2WFactorFMatrix) * 2)
//                                           else Height := Round(Width / cH2WFactorFMatrix);
//  end
//  else begin
//    Height := Trunc(Width / cH2WFactorQMatrix);
//  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrixBase.DefineSiroSubMatrix;
var
  xSubType: TMatrixSubType;
begin
  if MatrixType = mtSiro then begin
    EnterMethod('DefineSiroSubMatrix');
    if (fMatrixSubType = mstZenitF) and (not fFModeBD) then
      xSubType := mstSiroH
    else
      xSubType := fMatrixSubType;

    CodeSite.SendString('Matrix is Siro', GetEnumName(TypeInfo(TMatrixSubType), ord(xSubType)));
    CodeSite.SendBoolean('FModeBD', fFModeBD);

// TODO wss: H�he/Breite bei ZenitF Matrix mit Faktor 1.8 berechnen...
    CalculateMatrixSize;
//    if (xSubType = mstZenitF) and fFModeBD then Height := Round((Width / cH2WFactorFMatrix) * 2)
//                                           else Height := Round(Width / cH2WFactorFMatrix);
//    if (fMatrixSubType <> mstZenitF) and (xSubType = mstZenitF) and fFModeBD then Height := Height * 2
//    else if (fMatrixSubType = mstZenitF) and (xSubType <> mstZenitF) and (not fFModeBD) then Height := Height div 2;
                  
    case xSubType of
      mstSiroH: begin
          mMatrix          := cSIROHMatrix;
          mTotCaptions     := 0;
          mTotSuperClasses := Length(cSIRORoughClasses);
          mTotFields       := Length(cSIROFields);
          mTotHorLines     := Length(cSIROHorLines);
          mTotVertLines    := Length(cSIROVertLines);
          mTotXScales      := Length(cSIROXScales);
          mTotYScales      := Length(cSIROHYScales);
        end;
      mstSiroBD: begin
          mMatrix          := cSIROBDMatrix;
          mTotCaptions     := 0;
          mTotSuperClasses := Length(cSLTRoughClasses);
          mTotSuperClasses := Length(cSIROBDRoughClasses);
          mTotFields       := Length(cSIROBDFields);
          mTotHorLines     := Length(cSIROBDHorLines);
          mTotVertLines    := Length(cSIROBDVertLines);
          mTotXScales      := Length(cSIROBDXScales);
          mTotYScales      := Length(cSIROBDYScales);
        end;
      mstZenitF: begin
          mMatrix          := cColorMatrix;
          mTotCaptions     := Length(cColorCaptions);
          mTotSuperClasses := Length(cColorRoughClasses);
          mTotFields       := Length(cColorFields);
          mTotHorLines     := Length(cColorHorLines);
          mTotVertLines    := Length(cColorVertLines);
          mTotXScales      := Length(cColorXScales);
          mTotYScales      := Length(cColorYScales);
        end;
    else
      mMatrix          := cSIROFMatrix;
      mTotCaptions     := 0;
      mTotSuperClasses := Length(cSIRORoughClasses);
      mTotFields       := Length(cSIROFields);
      mTotHorLines     := Length(cSIROHorLines);
      mTotVertLines    := Length(cSIROVertLines);
      mTotXScales      := Length(cSIROXScales);
      mTotYScales      := Length(cSIROFYScales);
    end; // case fSubType of
    CodeSite.SendInteger('Total Fields', mTotFields);
    Invalidate;
  end;
end;
//procedure TQualityMatrixBase.DefineSiroSubMatrix;
//begin
//  if MatrixType = mtSiro then begin
//    case fMatrixSubType of
//      mstSiroH: begin
//          mMatrix          := cSIROHMatrix;
//          mTotCaptions     := 0;
//          mTotSuperClasses := Length(cSIRORoughClasses);
//          mTotFields       := Length(cSIROFields);
//          mTotHorLines     := Length(cSIROHorLines);
//          mTotVertLines    := Length(cSIROVertLines);
//          mTotXScales      := Length(cSIROXScales);
//          mTotYScales      := Length(cSIROHYScales);
//        end;
//      mstSiroBD: begin
//          mMatrix          := cSIROBDMatrix;
//          mTotCaptions     := 0;
//          mTotSuperClasses := Length(cSLTRoughClasses);
//          mTotSuperClasses := Length(cSIROBDRoughClasses);
//          mTotFields       := Length(cSIROBDFields);
//          mTotHorLines     := Length(cSIROBDHorLines);
//          mTotVertLines    := Length(cSIROBDVertLines);
//          mTotXScales      := Length(cSIROBDXScales);
//          mTotYScales      := Length(cSIROBDYScales);
//        end;
//      mstZenitF: begin
//          mMatrix          := cColorMatrix;
//          mTotCaptions     := Length(cColorCaptions);
//          mTotSuperClasses := Length(cColorRoughClasses);
//          mTotFields       := Length(cColorFields);
//          mTotHorLines     := Length(cColorHorLines);
//          mTotVertLines    := Length(cColorVertLines);
//          mTotXScales      := Length(cColorXScales);
//          mTotYScales      := Length(cColorYScales);
//        end;
//    else
//      mMatrix          := cSIROFMatrix;
//      mTotCaptions     := 0;
//      mTotSuperClasses := Length(cSIRORoughClasses);
//      mTotFields       := Length(cSIROFields);
//      mTotHorLines     := Length(cSIROHorLines);
//      mTotVertLines    := Length(cSIROVertLines);
//      mTotXScales      := Length(cSIROXScales);
//      mTotYScales      := Length(cSIROFYScales);
//    end; // case fSubType of
//    Invalidate;
//  end;
//end;

//:---------------------------------------------------------------------------
procedure TQualityMatrixBase.FillField(aFieldRect: TRect; aSelected: TBrushRec);
begin
  Canvas.Brush.Color := aSelected.color;
  Canvas.Brush.Style := aSelected.style;
  
  Canvas.FillRect(aFieldRect);
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.FindeLineIDs(aFieldID: Integer): TRect;
var
  i: Integer;
  xFieldRaster: TRect;
begin
  Result.Left   := TotVertLines;
  Result.Right  := TotVertLines;
  Result.Top    := TotHorLines;
  Result.Bottom := TotHorLines;
  
  if aFieldID < TotFields then begin
    with mMatrix.pField^.table[aFieldID] do begin
      xFieldRaster.Left := left;
      xFieldRaster.Top  := top;
      if width = 0 then
        xFieldRaster.Right := left + mMatrix.pField^.fieldWidth
      else
        xFieldRaster.Right := left + width;
  
      if height = 0 then
        xFieldRaster.Bottom := top + mMatrix.pField^.fieldHeight
      else
        xFieldRaster.Bottom := top + height;
    end;
  
    xFieldRaster.Left := mMatrix.pField^.table[aFieldID].left;
    for i:=0 to TotVertLines-1 do begin
      with mMatrix.pScaleLayer^.vertLine.table[i], xFieldRaster do begin
        if (pos = Left) and (Top >= start) and (Bottom <= stop) then Result.Left := i;
        if (pos = Right) and (Top >= start) and (Bottom <= stop) then Result.Right := i;
      end;
    end;
  
    for i:=0 to TotHorLines-1 do begin
      with mMatrix.pScaleLayer^.horLine.table[i], xFieldRaster do begin
        if (pos = Top) and (Left >= start) and (Right <= stop) then Result.Top := i;
        if (pos = Bottom) and (Left >= start) and (Right <= stop) then Result.Bottom := i;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetBackgroundColor(aID, aIndex: Integer): TBrushRec;
begin
  case aIndex of
    cField:
      begin
        if aID < TotFields then
          with mMatrix.pField^.table[aID] do
          begin
            if pBackground = nil then
              Result := mMatrix.pField^.background
            else
              Result := pBackground^;
          end;
      end;
  else
    Result := cBackgroundUnassigned;
  end;
end;

//:---------------------------------------------------------------------------
{* Anhand von einer ID (Feld, Horizontale- oder Vertikale Linie) und dem Mode (aIndex) werden Pixelkoordinaten ermittelt. Bei einer Line entspricht dann Top =
Bottom. Die Umrechnung von Indexwerten (Parameter: start, stop, pos) nach Pixelwerten wird in CalculateRasterToPixel vorgenommen.
*}
function TQualityMatrixBase.GetCoordinate(aID, aIndex: Integer): TRect;
var
  xLine, xBoundary: TRect;
  xPen: TPenRec;
  xWidth, xHeight, xRemainder: Integer;
  xCoordinateMode: TCoordinateMode;
begin
  //���
  //���
  //��� Randbedingungeng checken div by zero, Breite, Hoehe overflow usw.
  //���
  // Default init
  Result.Left := 0;
  Result.Top := 0;
  Result.Right := ClientWidth;
  Result.Bottom := ClientHeight;
  
  xPen := GetLinePen(aID, aIndex);
  xRemainder := xPen.width mod 2;
  
  case aIndex of
    cHorizontal: begin
        if aID < TotHorLines then
          with mMatrix.pScaleLayer^.horLine.table[aID] do
            Result := CalculateRasterToPixel(start, pos, stop, pos);
  
        with xBoundary do begin
          Left   := 0;
          Top    := xPen.width div 2;
          Right  := ClientWidth;
          Bottom := ClientHeight - Top - xRemainder;
        end;
        Result := AdjustBoundary(Result, xBoundary, cbAbsolute);
  
        with Result do begin
          case CoordinateMode of
            cmTop: begin
                Top := Top - xBoundary.Top;
                Bottom := Top;
              end;
  
            cmBottom: begin
                Top := Top + xBoundary.Top + xRemainder;
                Bottom := Top;
              end;
          end;
        end;
      end;
  
    cVertical: begin
        if aID < TotVertLines then
          with mMatrix.pScaleLayer^.vertLine.table[aID] do
            Result := CalculateRasterToPixel(pos, start, pos, stop);
  
        with xBoundary do begin
          Left   := xPen.width div 2;
          Top    := 0;
          Right  := ClientWidth - Left - xRemainder;
          Bottom := ClientHeight;
        end;
        Result := AdjustBoundary(Result, xBoundary, cbAbsolute);
  
        with Result do begin
          case CoordinateMode of
            cmLeft: begin
                Left  := Left - xBoundary.Left;
                Right := Left;
              end;
            cmRight: begin
                Left  := Left + xBoundary.Left + xRemainder;
                Right := Left;
              end;
          end;
        end;
      end;
  
    cField: begin
        if aID < TotFields then begin
  //���
  //���
  //��� Randbedingungeng checken div by zero, Breite, Hoehe overflow usw.
  //���
          if mMatrix.pField^.table[aID].width = 0 then
            xWidth := mMatrix.pField^.fieldWidth
          else
            xWidth := mMatrix.pField^.table[aID].width;
  
          if mMatrix.pField^.table[aID].height = 0 then
            xHeight := mMatrix.pField^.fieldHeight
          else
            xHeight := mMatrix.pField^.table[aID].height;
  
          with mMatrix.pField^.table[aID] do
            Result := CalculateRasterToPixel(left, top, left + xWidth, top + xHeight);
  
          xCoordinateMode := CoordinateMode;
          if (xCoordinateMode = cmOutside) or (xCoordinateMode = cmInside) then begin
            xBoundary := FindeLineIDs(aID);
  
            with xBoundary do begin
              if Top < TotHorLines then begin
                case xCoordinateMode of
                  cmInside:  CoordinateMode := cmBottom;
                  cmOutside: CoordinateMode := cmTop;
                end;
                xLine := HorLine[Top];
                Top := xLine.Top;
              end
              else
                Top := Result.Top;
  
              if Bottom < TotHorLines then begin
                case xCoordinateMode of
                  cmInside:  CoordinateMode := cmTop;
                  cmOutside: CoordinateMode := cmBottom;
                end;
                xLine := HorLine[Bottom];
                Bottom := xLine.Top;
              end
              else
                Bottom := Result.Bottom;
  
              if Left < TotVertLines then begin
                case xCoordinateMode of
                  cmInside:  CoordinateMode := cmRight;
                  cmOutside: CoordinateMode := cmLeft;
                end;
                xLine := VertLine[Left];
                Left := xLine.Left;
              end
              else
                Left := Result.Left;
  
              if Right < TotVertLines then begin
                case xCoordinateMode of
                  cmInside:  CoordinateMode := cmLeft;
                  cmOutside: CoordinateMode := cmRight;
                end;
                xLine := VertLine[Right];
                Right := xLine.Left;
              end
              else
                Right := Result.Right;
            end;
            CoordinateMode := xCoordinateMode;
  
            Result := AdjustBoundary(Result, xBoundary, cbAbsolute);
          end;
        end;
      end;
  
    cGraphField: begin // Boundary of the Graph field (Channel Curve Graph)
        if (mMatrix.pScaleLayer <> nil) and (mMatrix.pScaleLayer^.XAxis <> nil) and
           (mMatrix.pScaleLayer^.yAxis <> nil) then begin
          with mMatrix.pScaleLayer^ do begin
            Result := CalculateRasterToPixel(XAxis[0].xPos, yAxis[0].yPos,
                                             XAxis[TotXScales - 1].xPos,
                                             yAxis[TotYScales - 1].yPos);
          end;
        end;
      end;
  
    cNSLGraphField: begin // Boundary of the Graph field (Channel Curve Graph)
        if (mMatrix.pScaleLayer <> nil) and (mMatrix.pScaleLayer^.XAxis <> nil) and
          (mMatrix.pScaleLayer^.yAxis <> nil) then begin
          with mMatrix.pScaleLayer^ do begin
            Result := CalculateRasterToPixel(XAxis[cNSLXLeftScale].xPos, yAxis[cNSLYTopScale].yPos,
                                             XAxis[cNSLXRightScale].xPos,
                                             yAxis[cNSLYBottomScale].yPos);
          end;
        end;
      end;
  
    cThinGraphField: begin // Boundary of the Graph field (Channel Curve Graph)
        if (mMatrix.pScaleLayer <> nil) and (mMatrix.pScaleLayer^.XAxis <> nil) and
          (mMatrix.pScaleLayer^.yAxis <> nil) then begin
          with mMatrix.pScaleLayer^ do begin
            Result := CalculateRasterToPixel(XAxis[cThinXLeftScale].xPos, yAxis[cThinYTopScale].yPos,
                                             XAxis[cThinXRightScale].xPos,
                                             yAxis[cThinYBottomScale].yPos);
          end;
        end;
      end;
  
  end;
end;

//:-------- Roda 31.01.2008 Returns whether the coordinates are within a valid matrix field (true) or not (false) -----
function TQualityMatrixBase.IsValidField(aX, aY: Integer): Boolean;
var
  i, xWidth, xHeight: Integer;
  xPixel, xRaster: TPoint;
begin
  xPixel.x := aX;
  xPixel.y := aY;
  xRaster := CalculatePixelToRaster(xPixel);
  Result := False;

  for i := 0 to TotFields - 1 do
  begin
    with mMatrix.pField^.table[i], xRaster do
    begin
      if width = 0 then
        xWidth := mMatrix.pField^.fieldWidth
      else
        xWidth := width;
      if height = 0 then
        xHeight := mMatrix.pField^.fieldHeight
      else
        xHeight := height;
      if (x >= left) and (x < (left + xWidth)) and (y >= top) and (y < (top + xHeight)) then
      begin
        Result := True;
        Break;
      end;
    end;
  end;
  
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetFieldID(aX, aY: Integer): Integer;
var
  i, xWidth, xHeight: Integer;
  xPixel, xRaster: TPoint;
begin
  xPixel.x := aX;
  xPixel.y := aY;
  xRaster := CalculatePixelToRaster(xPixel);
//  Result := TotFields-1;  //Roda:23.01.08
  Result := TotFields;  //Nue: 28.04.08 Zur�ck ge�ndert, wegen fehlerhaftem Verhalten

  for i := 0 to TotFields - 1 do
  begin
    with mMatrix.pField^.table[i], xRaster do
    begin
      if width = 0 then
        xWidth := mMatrix.pField^.fieldWidth
      else
        xWidth := width;
      if height = 0 then
        xHeight := mMatrix.pField^.fieldHeight
      else
        xHeight := height;
      if (x >= left) and (x < (left + xWidth)) and (y >= top) and (y < (top + xHeight)) then
      begin
        Result := i;
        Break;
      end;
    end;
  end;
  
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetFieldWeight(aId: Integer): Real;
begin
  if mMatrix.pField^.table[aID].width = 0 then
    Result := 1.0
  else
    Result := mMatrix.pField^.table[aID].width / mMatrix.pField^.Fieldwidth;
  
  if mMatrix.pField^.table[aID].height <> 0 then
    Result := Result * mMatrix.pField^.table[aID].height / mMatrix.pField^.fieldHeight;
  
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetGraphField(aIndex: Integer): TRect;
begin
  if (aIndex >= cGraphField) and (aIndex <= cThinGraphField) then
    Result := GetCoordinate(0, aIndex)
  else
  begin
    Result.Left := 0;
    Result.Top := 0;
    Result.Right := ClientWidth;
    Result.Bottom := ClientHeight;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetLastCutColor: TColor;
begin
  Result := fLastCutColor;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetLastCutField: Integer;
begin
  Result := fLastCutField;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetLastCutMode: TLastCutMode;
begin
  Result := fLastCutMode;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetLinePen(aID, aIndex: Integer): TPenRec;
begin
  Result := cLinePenUnassigned;
  case aIndex of
    cHorizontal:
      begin
        if aID < TotHorLines then
          with mMatrix.pScaleLayer^.horLine do
          begin
            if table[aID].pPen <> nil then
            begin
              Result := table[aID].pPen^;
            end
            else
              Result := pen;
          end;
      end;
  
    cVertical:
      begin
        if aID < TotVertLines then
          with mMatrix.pScaleLayer^.vertLine do
          begin
            if table[aID].pPen <> nil then
              Result := table[aID].pPen^
            else
              Result := pen;
          end;
      end;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetNeighbourFieldID(var aX, aY: Integer): Integer;
var
  xRecT: TRect;
  xFieldID: Integer;
  xCoordinateMode: TCoordinateMode;
  xGap: Integer;
begin
  xFieldID := GetFieldID(aX, aY);
  if xFieldID < TotFields then begin
    xCoordinateMode := CoordinateMode;
    CoordinateMode  := cmActual;
    xRect := Field[xFieldID];
  
    case fNeighbour of
      iaTop: begin
          xGap := (xRect.Bottom - xRect.Top) div 2;
          if (xRect.Top >= xGap) then begin
            aY     := xRect.Top - xGap;
            Result := GetFieldID(aX, aY);
          end
          else
            Result := TotFields;
        end;
      iaLeft: begin
          xGap := (xRect.Right - xRect.Left) div 4;
          if xRect.Left >= xGap then begin
            aX     := xRect.Left - xGap;
            Result := GetFieldID(aX, aY);
          end
          else
            Result := TotFields;
        end;
      iaBottom: begin
          aY     := xRect.Bottom + ((xRect.Bottom - xRect.Top) div 2);
          Result := GetFieldID(aX, aY);
        end;
      iaRight: begin
          aX     := xRect.Right + ((xRect.Right - xRect.Left) div 4);
          Result := GetFieldID(aX, aY);
        end;
    else
      Result := TotFields;
    end;
    CoordinateMode := xCoordinateMode;
  end
  else
    Result := TotFields;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetNumberOfSamples(aID: Integer): Integer;
begin
  if (mMatrix.pCurveLayer <> nil) and (aID < TotXScales - 1) then
    Result := fNumberOfSamples
  else
    Result := 0;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetScale(aID, aIndex: Integer): TScaleRec;
var
  xRect: TRect;
begin

  Result.value := 0;
  Result.pixcelToNextScale := 0;

  if (mMatrix.pCurveLayer <> nil) and (mMatrix.pScaleLayer <> nil) then begin
    case aIndex of
      cXAxis: begin
          if (mMatrix.pScaleLayer^.XAxis <> nil) and (aID < TotXScales) then begin
            with mMatrix.pCurveLayer^, mMatrix.pScaleLayer^, Result do begin
              if (aID + 1 < TotXScales) and (XAxis[aID].pFont <> nil) and (XAxis[aID + 1].pFont <> nil) then begin
                // Feldgr�sse anhand der ID ermitteln
                xRect             := CalculateRasterToPixel(XAxis[aID].xPos, XAxis[aID].yPos, XAxis[aID + 1].xPos, XAxis[aID + 1].yPos);
                // Pixeldifferenz ergibt Breite des Feldes in Pixel
                pixcelToNextScale := xRect.Right - xRect.Left;
                // Anzahl St�tzpunkte
                numberOfSamples   := self.NumberOfSamples[aID];
              end;

              try
                value := MMStrToFloat(XAxis[aID].text);
//                value := StrToFloat(StringReplace(XAxis[aID].text, '.', DecimalSeparator, [rfReplaceAll]));
              except
                on e: Exception do
                  raise Exception.Create('TQualityMatrixBase.GetScale (XAxis) failed.' + e.Message);
              end;
            end; // with
          end; // if
        end;

      cYAxis: begin
          if (mMatrix.pScaleLayer^.YAxis <> nil) and (aID < TotYScales) then begin
            with mMatrix.pScaleLayer^, Result do begin
              if (aID + 1 < TotYScales) and (yAxis[aID].pFont <> nil) and (yAxis[aID + 1].pFont <> nil) then begin
                // Feldgr�sse anhand der ID ermitteln
                xRect := CalculateRasterToPixel(yAxis[aID + 1].xPos, yAxis[aID + 1].yPos, yAxis[aID].xPos, yAxis[aID].yPos);
                // Pixeldifferenz ergibt H�he des Feldes in Pixel
                if xRect.Bottom >= xRect.Top then
                  pixcelToNextScale := xRect.Bottom - xRect.Top
                else
                  pixcelToNextScale := xRect.Top - xRect.Bottom;
                // Y-Positionen haben keine Anzahl St�tzpunkte
                numberOfSamples := 0;
              end;

              try
                value := MMStrToFloat(yAxis[aID].text);
//                value := StrToFloat(StringReplace(yAxis[aID].text, '.', DecimalSeparator, [rfReplaceAll]));
              except
                on e: Exception do
                  raise Exception.Create('TQualityMatrixBase.GetScale (yAxis) failed.' + e.Message);
              end;
            end; // with
          end; // if
        end;
    else
    end; // case
  end; // if
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetSCField(aID: Integer): TRect;
begin
  Result := Rect(0, 0, 0, 0);

  if (mMatrix.pDisplayLayer <> nil) and (mMatrix.pDisplayLayer^.roughClassTbl <> nil) then
    with mMatrix.pDisplayLayer^.roughClassTbl[aID].boarder do
    begin
      Result := CalculateRasterToPixel(left, top, left + width, top + height);
    end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetSCMembers(aID: Integer): TSCMemberArray;
begin
  Result := cNoMember;
  
  if (mMatrix.pDisplayLayer <> nil) and (mMatrix.pDisplayLayer^.roughClassTbl <> nil) then
    Result := mMatrix.pDisplayLayer^.roughClassTbl[aID].memberTbl;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetStateColor(aID, aIndex: Integer): TBrushRec;
begin
  Result       := cBackgroundUnassigned;
  Result.style := bsClear;

  case aIndex of
    cCutActive:
      with mMatrix.pSelectionLayer^.cutState do
        if pActiveColor <> nil then Result := pActiveColor^;

    cCutPassive:
      with mMatrix.pSelectionLayer^.cutState do
        if pPassiveColor <> nil then
          Result := pPassiveColor^
        else
          Result := BackgroundColor[aID];
  
    cFFClusterActive:
      with mMatrix.pSelectionLayer^.fFClusterState do
        if pActiveColor <> nil then Result := pActiveColor^;
  
    cFFClusterPassive:
      with mMatrix.pSelectionLayer^.fFClusterState do
        if pPassiveColor <> nil then
          Result := pPassiveColor^
        else
          Result := BackgroundColor[aID];
  
    cSCMemberActive:
      with mMatrix.pSelectionLayer^.sCMemberState do
        if pActiveColor <> nil then Result := pActiveColor^;
  
    cSCMemberPassive:
      with mMatrix.pSelectionLayer^.sCMemberState do
        if pPassiveColor <> nil then
          Result := pPassiveColor^
        else
          Result := BackgroundColor[aID];
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetText(aID, aIndex: Integer): TTextRec;
var
  xRect: TRect;
begin
  Result := cTextUnassigned;
  case aIndex of
    cCaption: begin
        if (mMatrix.pScaleLayer^.caption <> nil) and (aID < TotCaptions) then begin
          Result := mMatrix.pScaleLayer^.caption[aID];
          with Result do begin
            xRect := CalculateRasterToPixel(xPos, yPos, xPos, yPos);
            xPos  := xRect.Left;
            yPos  := xRect.Top;
          end;
        end;
      end;

    cXAxis: begin
        if (mMatrix.pScaleLayer^.XAxis <> nil) and (aID < TotXScales) then begin
          Result := mMatrix.pScaleLayer^.XAxis[aID];
          with Result do begin
            text  := StringReplace(text, '.', DecimalSeparator, [rfReplaceAll]);
            xRect := CalculateRasterToPixel(xPos, yPos, xPos, yPos);
            xPos  := xRect.Left;
            yPos  := xRect.Top;
          end;
        end;
      end;

    cYAxis: begin
        if (mMatrix.pScaleLayer^.yAxis <> nil) and (aID < TotYScales) then begin
          Result := mMatrix.pScaleLayer^.yAxis[aID];
          with Result do begin
            text  := StringReplace(text, '.', DecimalSeparator, [rfReplaceAll]);
            xRect := CalculateRasterToPixel(xPos, yPos, xPos, yPos);
            xPos  := xRect.Left;
            yPos  := xRect.Top;
          end;
        end;
      end;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetTextFormat(aID, aIndex: Integer): TFormatTextRec;
begin
  Result := cFormatTextUnassigned;
  case aIndex of
    cCuts: Result := mMatrix.pDisplayLayer^.number.cuts;
  
    cDefects: Result := mMatrix.pDisplayLayer^.number.defects;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetTotItems(aIndex: Integer): Integer;
var
  i, xTest: Integer;
begin
  Result := 0;
  case aIndex of
    cField:      Result := mTotFields;
    cSuperClass: Result := mTotSuperClasses;
    cHorizontal: Result := mTotHorLines;
    cVertical:   Result := mTotVertLines;
    cXAxis:      Result := mTotXScales;
    cYAxis:      Result := mTotYScales;
    cCaption:    Result := mTotCaptions;
    cSamples: begin
        if (mMatrix.pCurveLayer <> nil) then begin
          for i := 0 to TotXScales - 1 do begin
            xTest  := NumberOfSamples[i];
            Result := Result + xTest;
          end;
        end;
      end;
  end;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetVisibleNSLRange: TSingleRect;
begin
  if (mMatrix.pCurveLayer <> nil) then
    Result := mMatrix.pCurveLayer^.visibleNSLRange
  else
    Result := cVisibleNSLTRange;
end;

//:---------------------------------------------------------------------------
function TQualityMatrixBase.GetVisibleTRange: TSingleRect;
begin
  if (mMatrix.pCurveLayer <> nil) then
    Result := mMatrix.pCurveLayer^.visibleTRange
  else
    Result := cVisibleNSLTRange;
end;

//------------------------------------------------------------------------------
procedure TQualityMatrixBase.Resize;
begin
  inherited Resize;
  CalculateMatrixSize;
end;

procedure TQualityMatrixBase.SetFixedSize(const aValue: Boolean);
begin
  if fFixedSize <> aValue then begin
    fFixedSize := aValue;
    CalculateMatrixSize;
  end;
end;

procedure TQualityMatrixBase.SetFModeBD(const aValue: Boolean);
begin
  if fFModeBD <> aValue then begin
    fFModeBD := aValue;
    DefineSiroSubMatrix;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrixBase.SetLastCutColor(const aValue: TColor);
begin
  fLastCutColor := aValue;
  Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrixBase.SetLastCutField(const aValue: Integer);
begin
  fLastCutField := aValue;
  Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrixBase.SetLastCutMode(const aValue: TLastCutMode);
begin
  fLastCutMode := aValue;
  Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrixBase.SetMatrixSubType(aValue: TMatrixSubType);
begin
  if fMatrixType = mtSiro then begin
    // Wenn der Bright Mode nicht aktiviert ist, dann nur die Dark Matrix anzeigen lassen
//    if (aValue = mstZenitF) and (not fFModeBD) then
//      aValue := mstSiroH;

    if aValue <> fMatrixSubType then begin
      // 24.01.2005  mstSiroBD wird nicht mehr eingesetzt!! (Mj, Jm)
      if aValue in [mstNone, mstSiroBD] then
        aValue := mstSiroF;

//      if (fMatrixSubType <> mstZenitF) and (aValue = mstZenitF) then Height := Height * 2
//      else if (fMatrixSubType = mstZenitF) and (aValue <> mstZenitF) then Height := Height div 2;

      fMatrixSubType := aValue;
      DefineSiroSubMatrix;
    end;
  end else
    fMatrixSubType := mstNone;

  Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrixBase.SetMatrixType(const aValue: TMatrixType);
begin
//  if (aValue <> fMatrixType) or (aValue = mtSiro) then begin
  if aValue <> fMatrixType then begin
    fMatrixType := aValue;
    case fMatrixType of
      mtSplice: begin
          mMatrix          := cSpliceMatrix;
          mTotCaptions     := Length(cSLTCaptions);
          mTotSuperClasses := Length(cSpliceRoughClasses);
          mTotFields       := Length(cSLTFields);
          mTotHorLines     := Length(cSpliceHorLines);
          mTotVertLines    := Length(cSpliceVertLines);
          mTotXScales      := Length(cSLTXScales);
          mTotYScales      := Length(cSLTYScales);
        end;
      mtSiro: begin
          if fMatrixSubType = mstNone then
            fMatrixSubType := mstSiroF;
          DefineSiroSubMatrix;
        end;
    else // mtShortLongThin
      mMatrix          := cSLTMatrix;
      mTotCaptions     := Length(cSLTCaptions); ;
      mTotSuperClasses := Length(cSLTRoughClasses);
      mTotFields       := Length(cSLTFields);
      mTotHorLines     := Length(cSLTHorLines);
      mTotVertLines    := Length(cSLTVertLines);
      mTotXScales      := Length(cSLTXScales);
      mTotYScales      := Length(cSLTYScales);
    end; // case aType of

//    fMatrixType := aValue;
    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQualityMatrixBase.SetSubField(const aIndex, aValue: Integer);
begin
  case aIndex of
    cXAxis:
      begin
        fSubFieldX := aValue;
        CalculateNumberOfSamples;
      end;
  
    cYAxis:
      if fSubFieldY <> aValue then
      begin
        fSubFieldY := aValue;
  (* %%Caculate TotSamples
  *)
      end;
  end;
  
end;

//:---------------------------------------------------------------------------
//:--- Class: TPixelAid
//:---------------------------------------------------------------------------
function TPixelAid.AlignItem(aAlignPos, aItemLength: Integer; aAlign: TItemAlignement): TRangeRec;
var
  xScratch: Integer;
begin
  
  with Result do
  begin
    case aAlign of
  
      iaCenter:
        begin
          xScratch := aItemLength div 2;
          if aAlignPos > xScratch then
            start := aAlignPos - xScratch
          else
            start := 0;
          stop := start + aItemLength;
        end;
  
      iaLeft, iaTop:
        begin
          if aAlignPos > aItemLength then
            start := aAlignPos - aItemLength
          else
            start := 0;
          stop := aAlignPos;
        end;
  
      iaRight, iaBottom:
        begin
          start := aAlignPos;
          stop := aAlignPos + aItemLength;
        end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TPixelAid.PlaceItem(aBounds: TRangeRec; aItemLength: Integer; aAlign: TItemAlignement): TRangeRec;
var
  xBorder, xDistance: Integer;
begin
  with aBounds do
  begin
    Result.start := start;
    Result.stop := stop;
  
    if start <= stop then
    begin
      xDistance := stop - start;
      if aAlign = iaCenter then
        xBorder := 0
      else
        xBorder := xDistance * cBorderWidthRel div 100;
  
      if (aItemLength + xBorder) <= xDistance then
        case aAlign of
  
          iaCenter:
            begin
              Result.start := start + (stop - start - aItemLength) div 2;
              Result.stop := Result.start + aItemLength;
            end;
  
          iaBottom, iaRight:
            begin
              Result.start := stop - aItemLength - xBorder;
              Result.stop := stop - xBorder;
            end;
  
          iaLeft, iaTop:
            begin
              Result.start := start + xBorder;
              Result.stop := start + aItemLength + xBorder;
            end;
        end;
    end;
  end;
end;


end.
