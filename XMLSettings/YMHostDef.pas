(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMHostDef.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.03.2000  1.00  Kr  | Datei erstellt
| 13.07.2000  1.00  Kr  | Telegramm added
|=========================================================================================*)

unit YMHostDef;

interface

uses
  YMDataDef;

const
  //------------------------------------------------------------------------------
  // General Constants
  //------------------------------------------------------------------------------
    //..........................................................................
  cMaxOrionSections = 10;
  cMaxYMGroups      = 12;
  cMaxYMSpindles    = 72;
  //..........................................................................

//------------------------------------------------------------------------------
// Constants for TDeclarationRec
//------------------------------------------------------------------------------
  //..........................................................................

  // Modified : BITSET16
  cZERangeBit       = $0001;            // Spindle range  Bit 0: 1=modified, 0=nochange
  cZEParameterBit   = $0002;            // Parameter Bit 1: 1=modified, 0=nochange
  cZEAdjustBit      = $0004;            // Adjust Bit 1: 1=executed, 0=none
  //..........................................................................

type
  //------------------------------------------------------------------------------
  // MM NT Telegram contents if  telegram subID SID_NT_DECLARATIONS, 106 byte
  //------------------------------------------------------------------------------
    //..........................................................................

  //%% BITSET16 = set of 0..15;

  TDecEvent = (deNone, deInitialReset, deReset, deEntryLocked, deEntryUnLocked, deRange,
    deSettings, deAdjust,
    (*Additional declarations*) deAssignComplete);

  PTDeclarationRec = ^TDeclarationRec;
  TDeclarationRec = packed record
    Event: TDecEvent;
    fill: Byte;
    states: Word;
    spare: array[0..5] of Byte;
    Groups: ;
    Modified: Word;
    ProdId: LongWord;
    SpindleFirst: Byte;
    SpindleLast: Byte;
  end;
  
  //..........................................................................

  //------------------------------------------------------------------------------
  // MM Telegram contents if  telegram subID SID_DECLARATIONS, 82 byte
  //------------------------------------------------------------------------------
    //..........................................................................

  PTNoneNTDeclarationRec = ^TNoneNTDeclarationRec;
  TNoneNTDeclarationRec = packed record
    Event: TDecEvent;
    fill: Byte;
    states: Word;
    spare: array[0..5] of Byte;
    Groups: ;
    Modified: Word;
    ProdId: Word;
    SpindleFirst: Byte;
    SpindleLast: Byte;
  end;
  
  //..........................................................................

const
  //------------------------------------------------------------------------------
  // Constants for TScreenDataRec
  //------------------------------------------------------------------------------
    //..........................................................................

    // Total class fields
  cTotSLTClassFields = 23;
  cTotCompSpliceClassFields = 26;
  cTotCompFFCLassFields = 16;
  //..........................................................................

  // Update reasons
  cUPDRClusterCut   = 1;
  cUPDRYarnCntCut   = 2;
  cUPDRSFICut       = 3;
  cUPDRUpperYarnCut = 4;
  cUPDRDBunchCut    = 5;
  cUPDRSpliceCut    = 6;
  cUPDRChannelCut   = 7;
  cUPDRClassCut     = 8;
  cUPDRFFCut        = 9;
  cUPDRFFClusterCut = 10;
  cUPDRAdjustCut    = 11;
  cUPDRAdditionalCut = 12;
  cUPDRBunchCut     = 13;
  cUPDRSystemCut    = 14;
  cUPDRNoSpecCut    = 15;
  cUPDRNoCut        = 16;
  cUPDRUpdate       = 17;
  cUPDRUnknown      = 20;
  //..........................................................................

  // Update Details
  cUPDDNotSpec      = 0;
  cUPDDLongChan     = 1;
  cUPDDThinChan     = 2;
  cUPDDShortChan    = 3;
  cUPDDNepsChan     = 4;
  cUPDDFFSplice     = 5;
  cUPDDExtendedClassM = $10;
  cUPDDDelBunchM    = $20;
  cUPDDNotClassifiedM = $40;
  //..........................................................................

type
  //------------------------------------------------------------------------------
  // MM NT Telegram contents if  telegram subID is
  // SID_NT_SPDL_SCREENS or SID_NT_GRP_SCREENS, 616  byte
  //------------------------------------------------------------------------------
    //..........................................................................

    // Sort-long-thin class data of YARNMASTER screens, 184 byte
  TScreenSLTClassRec = packed record
    wound: array[0..cTotSLTClassFields - 1] of Integer;
    cut: array[0..cTotSLTClassFields - 1] of Integer;
  end;
  
  //..........................................................................

  // Splice class data of YARNMASTER screens, 52 byte
  TScreenSpliceClassRec = packed record
    wound: array[0..cTotCompSpliceClassFields - 1] of Byte;
    cut: array[0..cTotCompSpliceClassFields - 1] of Byte;
  end;
  
  //..........................................................................

  // Siro class data of YARNMASTER screens, 128 byte
  TScreenFFClassRec = packed record
    wound: array[0..cTotCompFFCLassFields - 1] of Integer;
    cut: array[0..cTotCompFFCLassFields - 1] of Integer;
  end;
  
  //..........................................................................

  // Layout of imperfection data, 32 byte
  TIPIDataRec = packed record
    nep: Integer;
    thick: Integer;
    small: Integer;
    thin: Integer;
    len2To4: Integer;
    len4To8: Integer;
    len8To20: Integer;
    len20To70: Integer;
  end;
  
  //..........................................................................

  // Layout of surface index data, 8 byte
  TSFIDataRec = packed record
    sFI_x1000: Integer;
    sFID_x100: Integer;
  end;
  
  //..........................................................................

  // Telegram contents if telegram subID's SID_NT_SPDL_SCREENS and SID_NT_GRP_SCREENS, 624 byte */
  TScreenDataToHostRec = packed record
    updReason: Byte;
    updDetail: Byte;
    classIndex: Byte;
    dataRecMode: Byte;
    length: Integer;
    base: array[0..(Ord(bdBaseDataSize) - 1)] of Integer;
    sLT: TScreenSLTClassRec;
    sP: TScreenSpliceClassRec;
    fF: TScreenFFClassRec;
    iPI: TIPIDataRec;
    sFI: TSFIDataRec;
  end;
  
  //..........................................................................

const
  //------------------------------------------------------------------------------
  // Constants for TSupportDataToHostRec
  //------------------------------------------------------------------------------
  //..........................................................................

  // Not available Data Values
  cSFINotAvailable  = $FFFF div 2;
  cClusterActualFaultsNotAvailable = $FFFFFFFF;
  //..........................................................................

type
  //------------------------------------------------------------------------------
  // MM NT Telegram contents if  telegram subID is
  // SID_NT_SPDL_SUPPORT_DATA or SID_NT_GRP_SUPPORT_DATA, 58 byte
  //------------------------------------------------------------------------------
    //..........................................................................

  TSFISupportDataRec = packed record
    diff_x10: SmallInt;
    floatRef_x100: Word;
  end;
  
  //..........................................................................

  TSupportDataToHostRec = packed record
    diaDiff: SmallInt;
    clusterActualFaults: Cardinal;
    sFI: TSFISupportDataRec;
    fFClusterActualFaults: Cardinal;
    diameterBase: Cardinal;
    spare: array[1..10] of Integer;
  end;
  
  //..........................................................................

const
  //------------------------------------------------------------------------------
  // Constants for TCommandFromHostRec
  //------------------------------------------------------------------------------
  //..........................................................................

  // CommandId's

  cHCmdStartAdjust  = 1;                // SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: no argument used
  // ID_ACKNOWLEGE_TELEGRAM: no argument used

  cHCmdStopAdjust   = 2;                // SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: no argument used
  // ID_ACKNOWLEGE_TELEGRAM: no argument used

  cHCmdDiameterBase = 3;                // SID_NT_GRP_COMMAND or SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: arg1 = Base Voltage DISPL-Format (4x)
  // ID_ACKNOWLEGE_TELEGRAM: no argument used

  cHCmdFineAdjust   = 4;                // SID_NT_GRP_COMMAND or SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: no argument used
  // ID_ACKNOWLEGE_TELEGRAM: no argument used

  cHCmdFFAdjust     = 5;                // SID_NT_GRP_COMMAND or SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: no argument used
  // ID_ACKNOWLEGE_TELEGRAM: no argument used

  cHCmdRestartMonitoring = 6;           // SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: no argument used
  // ID_ACKNOWLEGE_TELEGRAM: no argument used

  cHCmdSetSWOption  = 7;                // not dependet on SID_NT_SPDL_COMMAND or SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: arg4: Option code
  // ID_ACKNOWLEGE_TELEGRAM: arg1: SoftwareOption

  cHCmdGetSWOption  = 8;                // not dependet on  SID_NT_SPDL_COMMAND or SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: no argument used
  // ID_ACKNOWLEGE_TELEGRAM: arg1: SoftwareOption, arg4: Serial number

  cHCmdSetErrorSet  = 9;                // not dependet on SID_NT_SPDL_COMMAND or SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: arg1: error set id ,ERROR_SET
  // ID_ACKNOWLEGE_TELEGRAM: no argument used

  cHCmdGetErrorSet  = 10;               // not dependet on  SID_NT_SPDL_COMMAND or SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: no argument used
  // ID_ACKNOWLEGE_TELEGRAM: arg1: error set id ,ERROR_SET

  cHCmdAWECommand   = 11;               // SID_NT_SPDL_COMMAND or SID_NT_GRP_COMMAND
  // ID_MESSAGE_TELEGRAM: arg1: awe command id, 0..255
  // ID_ACKNOWLEGE_TELEGRAM: no argument used

  cHCmdSetZETime      = 12;             // SID_NT_SPDL_COMMAND or SID_NT_GRP_COMMAND don't care
  // ID_MESSAGE_TELEGRAM: time, Current time from Host
  // ID_ACKNOWLEGE_TELEGRAM: no argument used
//..........................................................................

//  ErrorSet
  cESAllErrors      = 0;                // All
  cESNoErrors       = 1;                // None
  cESErrorSet1      = 2;                // 1..17
  cESErrorSet2      = 3;                // > 18

type
  //------------------------------------------------------------------------------
  // MM NT Telegram contents if  telegram subID is
  // SID_NT_SPDL_COMMAND or SID_NT_GRP_COMMAND, 20 byte
  //------------------------------------------------------------------------------
    //..........................................................................

  TCommandArguments = (CAStandard, CATime);

  TCommandFromHostRec = packed record
    commandId: Word;                    // Command indentificaton

    case TCommandArguments of

      CAStandard: (
        arg1: Smallint;                 // Message argument 1
        arg2: Smallint;                 // Message argument 2
        arg3: Smallint;                 // Message argument 3
        arg4: Integer;                  // Message argument 4
        spare: array[1..2] of Integer);
      CATime: ( // the arguments will be swapped by the communication!!
        min: Byte;                      // minutes, [0..59]
        sec: Byte;                      // seconds, [0..59]
        wkday: Byte;                    // days, [1..7, (SUN, Mon, .., SAT)]
        hr: Byte;                       // houres, [0..23]
        mo: Byte;                       // month, [1..12, (JAN, FEB, ., DEC)]
        moday: Byte;                    // days, [1..31]
        fill1, fill2, fill3: Byte;
        yr: Byte);                       // year, [0..70]

  end;
  //..........................................................................

const
  //------------------------------------------------------------------------------
  // Constants for TMessageToHostRec
  //------------------------------------------------------------------------------
   //..........................................................................

    // Sender id's
  cSendIdFirstSpindle = 0;              // represents indentifier of the first spindle message
  cSendIdLastSpindle = (cSendIdFirstSpindle + cMaxYMSpindles - 1);  // represents indentifier of the last spindle message
  cSendIdFirstGroup = (cSendIdLastSpindle + 1); // represents indentifier of the first group message
  cSendIdLastGroup  = (cSendIdFirstGroup + cMaxYMGroups - 1);  // represents indentifier of the last group message
  cSendIdZESystem   = (cSendIdLastGroup + 1); // represents indentifier of a ZE system message
  cSendIdUnknown    = (cSendIdZESystem + 1);
  cSendIdOrionSystem = (cSendIdUnknown + 1); // represents indentifier of a Orion system message
  //..........................................................................

  // Message id's: AWE800 ALARMS
  cMsgIdFirst       = 0;
  cMsgIdFirstAweAlarm = cMsgIdFirst;
  cMsgIdAweNoCut    = cMsgIdFirstAweAlarm; // No Yarn Cut or Break (i0)
  cMsgIdAweNoSpeed  = (cMsgIdFirstAweAlarm + 1); // No Speedsignal
  cMsgIdAweIRCtrlOor = (cMsgIdFirstAweAlarm + 2); // Diameter Controller out of Range
  cMsgIdAweFFCtrlOor = (cMsgIdFirstAweAlarm + 3); // FF Controller out of Range
  cMsgIdAweSystem0Err = (cMsgIdFirstAweAlarm + 4); // System Error's
  cMsgIdAweSystem1Err = (cMsgIdFirstAweAlarm + 5); // System Error's
  cMsgIdAweSystem2Err = (cMsgIdFirstAweAlarm + 6); // System Error's
  cMsgIdAweSystem3Err = (cMsgIdFirstAweAlarm + 7); // System Error's (i7)
  cMsgIdLastAweAarm = cMsgIdAweSystem3Err;
  //..........................................................................

  // Message id's: AWE800 WARNINGS
  cMsgIdFirstAweWarning = (cMsgIdLastAweAarm + 1);
  cMsgIdFirstAweLock = cMsgIdFirstAweWarning;
  cMsgIdAweOffCount = cMsgIdFirstAweLock; // Yarn count offlimit detected (i8)
  cMsgIdAweFaultCluster = (cMsgIdFirstAweLock + 1); // Fault Cluster detected
  cMsgIdAweFFAlarm  = (cMsgIdFirstAweLock + 2); // FF Startup Cuts offlimit
  cMsgIdAweSFIAlarm = (cMsgIdFirstAweLock + 3); // SFI Startup Cuts offlimit
  cMsgIdAweFFCluster = (cMsgIdFirstAweLock + 4); // SFI Startup Cuts offlimit
  cMsgIdAweSpare1   = (cMsgIdFirstAweLock + 5); // Spare1
  cMsgIdAweSpare2   = (cMsgIdFirstAweLock + 6); // Spare2
  cMsgIdAweSpare3   = (cMsgIdFirstAweLock + 7); // Spare3
  cMsgIdAweSpare4   = (cMsgIdFirstAweLock + 8); // Spare4
  cMsgIdAweLocked   = (cMsgIdFirstAweLock + 9); // AWE lock
  cMsgIdLastAweLock = cMsgIdAweLocked;
  cMsgIdAweIRAdjustErr = (cMsgIdLastAweLock + 1); // IR Adjust Error
  cMsgIdAweFFAdjustErr = (cMsgIdLastAweLock + 2); // FF Adjust Error (i19)
  cMsgIdLastAweWarning = cMsgIdAweFFAdjustErr;
  //..........................................................................

  // Message id's: AWE800 ZE ALARMS
  cMsgIdFirstZEAlarm = (cMsgIdLastAweWarning + 1);
  cMsgIdZEOfflineSpindle = cMsgIdFirstZEAlarm; // Offline spindle (i20)
  cMsgIdZELevelDetection = (cMsgIdFirstZEAlarm + 1); // Level detection locked
  cMsgIdZEPowerFailure = (cMsgIdFirstZEAlarm + 2); // Failure of power supply, not yet used
  cMsgIdZETemperaturAlarm = (cMsgIdFirstZEAlarm + 3); // Temperature offlimit (i23)
  cMsgIdLastZEAlarm = cMsgIdZETemperaturAlarm;
  //..........................................................................

  // Message id's: AWE800 ZE WARNINGS
  cMsgIdFirstZEWarning = (cMsgIdLastZEAlarm + 1);
  cMsgIdZEPowerOff  = cMsgIdFirstZEWarning; // Power off warning (i24)
  cMsgIdZEMachineTypeErr = (cMsgIdFirstZEWarning + 1); // AWE SW Versions supporting different
  // machine types, not yet used
  cMsgIdZENotAssignedSpindle = (cMsgIdFirstZEWarning + 2); // Spindle is not assigned to a group
  cMsgIdZEFineAdjustOfflimit = (cMsgIdFirstZEWarning + 3); // Fine Adjust Offlimit
  cMsgIdZESettingsOffRange = (cMsgIdFirstZEWarning + 4); // Channel Settings off Range, not yet used
  cMsgIdZEAdjustComplete = (cMsgIdFirstZEWarning + 5); // Adjust Complete
  cMsgIdZEJournalOverflow = (cMsgIdFirstZEWarning + 6); // Message Journal Overflow (i30)
  cMsgIdLastZEWarning = cMsgIdZEJournalOverflow;
  //..........................................................................

  // Message id's: Unknown
  cMsgIdUnknown     = (cMsgIdLastZEWarning + 1); // Unknown Message Id (i31)
  //..........................................................................

  // Message id's: AWE800 Orion ALARMS
  cMsgIdFirstOrionAlarm = (cMsgIdUnknown + 1);
  cMsgIdOrionOfflineZE = cMsgIdFirstOrionAlarm;
  cMsgIdLastOrionAlarm = cMsgIdOrionOfflineZE;
  //..........................................................................

  // Message id's: AWE800 Orion Warnings
  cMsgIdFirstOrionWarning = (cMsgIdLastOrionAlarm + 1);
  cMsgIdCheckSettings = cMsgIdFirstOrionWarning;
  cMsgIdLastOrionWarning = cMsgIdCheckSettings;
  //..........................................................................

  // Message id's: Last
  cMsgIdLast        = cMsgIdLastOrionWarning;
  //..........................................................................

type
  //------------------------------------------------------------------------------
  // MM NT Telegram contents if  telegram subID is
  // SID_NT_MESSAGE, 20 byte
  //------------------------------------------------------------------------------
    //..........................................................................
  TMMTimeRec = packed record
  end;
  
  //..........................................................................

  PTMessageToHostRec = ^TMessageToHostRec;
  TMessageToHostRec = packed record
    senderId: Word;
    messageId: Word;
    elapsedTime: Word;
    arg1: SmallInt;
    arg2: SmallInt;
    arg3: SmallInt;
    spare: array[1..2] of Integer;
  end;
  
  //..........................................................................

//------------------------------------------------------------------------------
// MM NT Telegram contents if  telegram subID is
// SID_NT_MESSAGE_JOURNAL,  2..662 byte
//------------------------------------------------------------------------------
  //..........................................................................

  TMessageJournalToHostRec = packed record
    messageCount: Word;
    messages: array[1..33] of TMessageToHostRec;
  end;
  
  //..........................................................................

//------------------------------------------------------------------------------
// MM NT Telegram contents if  telegram subID is
// SID_NT_SYSTEM_EVENTS, 2..514 byte
//------------------------------------------------------------------------------
  //..........................................................................

  TSystemEventToHostRec = packed record
    eventCount: Byte;
    fill: Byte;
    eventStrings: array[1..4, 1..128] of Byte;
  end;
  
  //..........................................................................

//------------------------------------------------------------------------------
// MM NT Telegram contents if  telegram subID is
// SID_NT_AWE_STATUS, 54 byte
//------------------------------------------------------------------------------
  //..........................................................................

  TAWEStatusToHostRec = packed record
    blockNumber: Byte;
    fill1: Byte;
    updReason: Byte;
    updDetail: Byte;
    fineAdjustFact: Word;
    error: Byte;
    errorCode: Byte;
    miso: Byte;
    bereich: Byte;
    pwm: Byte;
    cutzust: Byte;
    infoby: Byte;
    sirmsig: Byte;
    ffgegreg: Byte;
    ffaufreg: Byte;
    reg: Word;
    nrmmw: Word;
    zeregmmw: Word;
    clearCut: array[0..1] of Byte;
    fFCut: array[0..1] of Byte;
    tagStd: array[0..1] of Byte;
    jahrMon: array[0..1] of Byte;
    spare: array[1..6] of Integer;
  end;
  
  //..........................................................................

implementation
uses
  mmMBCS;
end.

