unit ClearCurve;
interface

uses
  Classes, Controls, ExtCtrls, Graphics, SysUtils, Windows,
  KGridDef;

type
  TClearCurve = class(TPaintBox)
  private
    FClearerColor: TColor;
    FSpliceColor: TColor;
    FClusterColor: TColor;
    FCurveType: sCurveType;
    FCurveEdit: eClearCurve;
    FEditColor: TColor;
//    FFieldWidth: Integer;
    m_YValue: Array[ctClearer..ctCluster, ccNep..ccThin, 1..cMaxCurvePoints] of Single;
    m_Points: Array[ctClearer..ctCluster, 1..2, 1..cMaxCurvePoints] of TPoint;
    m_EditPoints: Array[1..cMaxCurvePoints] of TPoint;
    m_EditPara: rClearerParameter;
    m_EditImage: TImage;
    m_XStart,
    m_YStart: Integer;
    m_MouseDown: Boolean;
    FCurvePara: Array[ctClearer..ctCluster] of rClearerParameter;
    procedure CalculateCurves;
    procedure CalculateDrawEditCurve;
    procedure SetClearerPara(Index: Integer; Value: rClearerParameter);
    function GetClearerPara(Index: Integer): rClearerParameter;
    procedure SetCurveType(Value: sCurveType);
    procedure SetCurveEdit(Value: eClearCurve);
  protected
    function YPosToDiameter(aY: Integer; aMin, aMax: Single): Single;
    function XPosToLength(aX: Integer; aMin, aMax: Single): Single;
    function DiameterToYPos(aDiameter: Single): Integer;
    function LengthToXPos(aLength: Single): Integer;
    function GetYInPixel(const aValue, aFHeight: Single): Integer;
    procedure GenerateCurve(var aPosPoints, aNegPoints: Array of TPoint; aTyp: eCurveType);
    procedure CalculateCurve(var aValues: Array of Single; aCurve: eClearCurve; Dicke, Long: Single);
    procedure DrawCurve(Points: Array of TPoint; aNegCurve: Boolean);
    function GetAxFactor(var aF: Single; aCurve: eClearCurve; Dicke, Long: Single):Single;
    function LengthAtThickness(aCurve: eClearCurve; aD: Single; aPara: rClearerParameter): Single;
  public
//    property FieldWidth: Integer read FFieldWidth write FFieldWidth;
    property ClearerColor: TColor read FClearerColor write FClearerColor default clRed;
    property SpliceColor: TColor read FSpliceColor write FSpliceColor default clBlue;
    property ClusterColor: TColor read FClusterColor write FClusterColor default clGreen;
    property CurveType: sCurveType read FCurveType write SetCurveType default [ctClearer, ctSplice, ctCluster];
    property CurveEdit: eClearCurve read FCurveEdit write SetCurveEdit default ccNep;
    property EditColor: TColor read FEditColor write FEditColor default clPurple;

    property ClearerPara: rClearerParameter index 0 read GetClearerPara write SetClearerPara;
    property SplicePara: rClearerParameter index 1 read GetClearerPara write SetClearerPara;

    constructor Create(aOwner: TComponent); override;
    function GetLengthAtThickness(const aValue: Single): Single;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure Refresh;
  end;

implementation
//------------------------------------------------------------------------
constructor TClearCurve.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Align    := alNone;
  Enabled  := False;
  Left     := 0;
  ShowHint := False;
  Top      := 0;

  with FCurvePara[ctClearer] do begin
    EnabledCurve := [ccNep, ccShort, ccLong, ccThin];
    NDiam := 6.0;
    SDiam := 2.1;
    SLong := 2.2;
    LDiam := 1.26;
    LLong := 40;
    TDiam := 0.75;     // expect Thin-Parameter as factor, not as procent
    TLong := 20;
    LowCurve := 0.90;  // expect Cluster-Parameter as factor, not as procent
  end;
  // copies the default settings
  FCurvePara[ctSplice] := FCurvePara[ctClearer];
  FCurvePara[ctCluster] := FCurvePara[ctClearer];
  FCurvePara[ctCluster].EnabledCurve := [ccShort];

  FClearerColor := clRed;
  FSpliceColor  := clBlue;
  FClusterColor := clGreen;
  FCurveType    := [ctClearer, ctSplice, ctCluster];
  FCurveEdit    := ccNone;
  FEditColor    := clPurple;

//  FFieldWidth  := 10;

  m_MouseDown := False;
  CalculateCurves;
end;
//------------------------------------------------------------------------
function TClearCurve.GetAxFactor(var aF: Single; aCurve: eClearCurve; Dicke, Long: Single):Single;
begin
  case aCurve of
    ccShort: begin
      Result := (cKs * (cFs*Dicke - cBAS) + cBAS - cFs*Dicke) * Long * cGs;
      aF := cFs;
    end;
    ccLong: begin
      Result := (cKl * (cFl*Dicke - cBAS) + cBAS - cFl*Dicke) * Long * cGl;
      aF := cFl;
    end;
    ccThin: begin
      Result := -((-cKt * (cFt*Dicke - cBAS) - cBAS + cFt*Dicke) * Long);
      aF := cFt;
    end;
  else // ccNep
    Result := 1;
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.CalculateCurve(var aValues: Array of Single; aCurve: eClearCurve; Dicke, Long: Single);
var
  xA,xd,xF: Single;
  xLen, xStep: Single;
  i,j: Integer;
  PointIndex: Integer;
begin
  xA := GetAxFactor(xF, aCurve, Dicke, Long);
  PointIndex := 0;          // Parameter Array begins with 0
  for i:=1 to cMaxKlassFieldX do begin
    xLen := cCurveValuesX[i];
    xStep := (cCurveValuesX[i+1] - cCurveValuesX[i]) / cCurvePointsPerField;
    for j:=1 to cCurvePointsPerField do begin
      xLen := xLen + xStep;
      if aCurve = ccNep then
        xd :=  Dicke
      else
        xd := (xA / xLen) + (xF * Dicke);
      aValues[PointIndex] := xd;
      inc(PointIndex);
    end;
  end;
end;
//------------------------------------------------------------------------
function TClearCurve.LengthAtThickness(aCurve: eClearCurve; aD: Single; aPara: rClearerParameter): Single;
var
  xA, xF,
  xDiam, xLong: Single;
begin
  case aCurve of
    ccShort: begin
      xDiam := aPara.SDiam; xLong := aPara.SLong;
    end;
    ccLong: begin
      xDiam := aPara.LDiam; xLong := aPara.LLong;
    end;
    ccThin: begin
      xDiam := aPara.TDiam; xLong := aPara.TLong;
    end;
  end;
  xA := GetAxFactor(xF, aCurve, xDiam, xLong);
  Result := xA / (aD - (xF*xDiam));
end;
//------------------------------------------------------------------------
procedure TClearCurve.CalculateDrawEditCurve;
var
  i: Integer;
  x1,x2,
  y1,y2: Integer;
  xPixelStep,
  xFieldHeight: Single;
  xValues: Array[1..cMaxCurvePoints] of Single;
  xOldPoints: Array[1..cMaxCurvePoints] of TPoint;
  xRect, xDestRect: TRect;
  xx,xy: Integer;
  xR1, xR2: TRect;

  function LowerInt(const V1, V2: Integer): Integer;
  begin
    if V1 < V2 then Result := V1
    else            Result := V2;
  end;
  function HigherInt(const V1, V2: Integer): Integer;
  begin
    if V1 > V2 then Result := V1
    else            Result := V2;
  end;

begin
  Move(m_EditPoints, xOldPoints, sizeof(xOldPoints));
  if FCurveEdit <> ccNone then begin
    with m_EditPara do
      case FCurveEdit of
        ccNep:
          CalculateCurve(xValues, ccNep, NDiam, 0);
        ccShort:
          CalculateCurve(xValues, ccShort, SDiam, SLong);
        ccLong:
          CalculateCurve(xValues, ccLong, LDiam, LLong);
        ccThin:
          CalculateCurve(xValues, ccThin, TDiam, TLong);
      end;
    xPixelStep := Width / cMaxCurvePoints;
    xFieldHeight := Height / cMaxKlassFieldY;
    FillChar(m_EditPoints, sizeof(m_EditPoints), 0);
    for i:=1 to cMaxCurvePoints do begin
      m_EditPoints[i].X := Round(i*xPixelStep);
      m_EditPoints[i].Y := GetYInPixel(xValues[i], xFieldHeight);
    end;
    Paint;
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.CalculateCurves;
var
  i: Integer;
begin
  // calculate Clearer Curve
  with FCurvePara[ctClearer] do begin
    CalculateCurve(m_YValue[ctClearer, ccNep], ccNep, NDiam, 0);
    CalculateCurve(m_YValue[ctClearer, ccShort], ccShort, SDiam, SLong);
    CalculateCurve(m_YValue[ctClearer, ccLong], ccLong, LDiam, LLong);
    CalculateCurve(m_YValue[ctClearer, ccThin], ccThin, TDiam, TLong);
  end;
  // calculate Splice Curve
  with FCurvePara[ctSplice] do begin
    CalculateCurve(m_YValue[ctSplice, ccNep], ccNep, NDiam, 0);
    CalculateCurve(m_YValue[ctSplice, ccShort], ccShort, SDiam, SLong);
    CalculateCurve(m_YValue[ctSplice, ccLong], ccLong, LDiam, LLong);
    CalculateCurve(m_YValue[ctSplice, ccThin], ccThin, TDiam, TLong);
  end;
  // calculate Cluster Curve
  for i:=1 to cMaxCurvePoints do
    m_YValue[ctCluster, ccShort, i] :=
      m_YValue[ctClearer, ccShort, i] * FCurvePara[ctClearer].LowCurve;
end;
//------------------------------------------------------------------------
//------------------------------------------------------------------------
function TClearCurve.GetYInPixel(const aValue, aFHeight: Single): Integer;
var
  i: Integer;
  xBase: Single;
  xDelta: Single;
  xRelPos: Single;
begin
  if (aValue >= cCurveValuesY[1]) or (aValue <= cCurveValuesY[High(cCurveValuesY)]) then
    Result := 0
  else begin
    i := 1;
    while aValue < cCurveValuesY[i+1] do
      inc(i);
    xBase := cCurveValuesY[i+1];
    xDelta := aValue - xBase;
    // evaluates the pos inner of a Field
    xRelPos := 1 - (xDelta / (cCurveValuesY[i]-xBase));
    Result := Round((i-1)*aFHeight + (xRelPos * aFHeight));
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.GenerateCurve(var aPosPoints, aNegPoints: Array of TPoint; aTyp: eCurveType);
var
  i: Integer;
  xPixelStep: Single;
  xYVal: Single;
  xFieldHeight: Single;

begin
  xPixelStep := Width / cMaxCurvePoints;
  xFieldHeight := Height / cMaxKlassFieldY;
  FillChar(aPosPoints, sizeof(aPosPoints), 0);
  // define Pixel for Pos-Curve
  for i:=1 to cMaxCurvePoints do begin
    xYVal := 10.0;
    with FCurvePara[aTyp] do begin
      // evaluate Y-Pos for enabled Curves only
      if (ccNep in EnabledCurve) and (m_YValue[aTyp, ccNep, i] < xYVal) then
        xYVal := m_YValue[aTyp, ccNep, i];
      if (ccShort in EnabledCurve) and (m_YValue[aTyp, ccShort, i] < xYVal) then
        xYVal := m_YValue[aTyp, ccShort, i];
      if (ccLong in EnabledCurve) and (m_YValue[aTyp, ccLong, i] < xYVal) then
        xYVal := m_YValue[aTyp, ccLong, i];
    end;
    // -1 : Parameter Array begins with 0
    aPosPoints[i-1].X := Round(i*xPixelStep);
    aPosPoints[i-1].Y := GetYInPixel(xYVal, xFieldHeight);
  end;

  FillChar(aNegPoints, sizeof(aNegPoints), 0);
  // define Pixel for Neg-Curve
  for i:=1 to cMaxCurvePoints do begin
    xYVal := 0.55;
    with FCurvePara[aTyp] do begin
      if (ccThin in EnabledCurve) and (m_YValue[aTyp, ccThin, i] > xYVal) then
        xYVal := m_YValue[aTyp, ccThin, i];
    end;
    // -1 : Parameter Array begins with 0
    aNegPoints[i-1].X := Round(i*xPixelStep);
    aNegPoints[i-1].Y := GetYInPixel(xYVal, xFieldHeight);
  end;
end;
//------------------------------------------------------------------------
//------------------------------------------------------------------------
procedure TClearCurve.DrawCurve(Points: Array of TPoint; aNegCurve: Boolean);
var
  i,j,k: Integer;
  xFirst: Boolean;
begin
  // Attention!! Array as Parameter begins with 0
    for k:=0 to 1 do begin              // Thickness
      xFirst := True;
      with Canvas do begin
        MoveTo(Points[0].X, Points[0].Y + k);
        for i:=1 to cMaxCurvePoints-1 do
          if Points[i].Y = 0 then
            MoveTo(Points[i].X, Points[i].Y + k)
          else if aNegCurve and xFirst then begin
            MoveTo(Points[i].X, Points[i].Y + k);
            xFirst := False;
          end else
            LineTo(Points[i].X, Points[i].Y + k);
      end;
    end;
end;
//------------------------------------------------------------------------
function TClearCurve.YPosToDiameter(aY: Integer; aMin, aMax: Single): Single;
var
  xFHeight: Integer;
  xIndex, xRest: Integer;
  xPos, xValue: Single;
  xDelta: Single;
begin
  xFHeight := Height div cMaxKlassFieldY;
  xIndex := (aY div xFHeight)+1;
  xRest  := aY mod xFHeight;
  xDelta := cCurveValuesY[xIndex]-cCurveValuesY[xIndex+1];
  xPos   := (xDelta / xFHeight) * xRest;
  Result := cCurveValuesY[xIndex] - xPos;
  if Result < aMin then      Result := aMin
  else if Result > aMax then Result := aMax;
end;
//------------------------------------------------------------------------
function TClearCurve.XPosToLength(aX: Integer; aMin, aMax: Single): Single;
var
  xFWidth: Integer;
  xIndex, xRest: Integer;
  xPos, xValue: Single;
  xDelta: Single;
  s: String;
begin
  xFWidth := Width div cMaxKlassFieldX;
  xIndex := (aX div xFWidth)+1;
  xRest  := aX mod xFWidth;
  xDelta := cCurveValuesX[xIndex]-cCurveValuesX[xIndex+1];
  xPos   := (xDelta / xFWidth) * xRest;
  Result := cCurveValuesX[xIndex] - xPos;
  if Result < aMin then      Result := aMin
  else if Result > aMax then Result := aMax;
end;
//------------------------------------------------------------------------
function TClearCurve.LengthToXPos(aLength: Single): Integer;
var
  xFWidth: Integer;
  i: Integer;
  xBase, xDelta: Single;
begin
  if (aLength <= cCurveValuesX[1]) or (aLength >= cCurveValuesX[High(cCurveValuesX)]) then
    Result := 0
  else begin
    xFWidth := Width div cMaxKlassFieldX;
    i := 1;
    while aLength > cCurveValuesX[i+1] do
      inc(i);
    xBase := cCurveValuesX[i];
    xDelta := aLength - xBase;
    // evaluates the pos inner of a Field
    Result  := Round(((i-1)*xFWidth) + (xFWidth / (cCurveValuesX[i+1]-xBase)) * xDelta);
  end;
end;
//------------------------------------------------------------------------
function TClearCurve.DiameterToYPos(aDiameter: Single): Integer;
var
  i: Integer;
  xFHeight: Integer;
  xBase: Single;
  xDelta: Single;
  xRelPos: Single;
begin
  if (aDiameter >= cCurveValuesY[1]) or (aDiameter <= cCurveValuesY[High(cCurveValuesY)]) then
    Result := 0
  else begin
    xFHeight := Height div cMaxKlassFieldY;
    i := 1;
    while aDiameter < cCurveValuesY[i+1] do
      inc(i);
    xBase := cCurveValuesY[i+1];
    xDelta := aDiameter - xBase;
    // evaluates the pos inner of a Field
    xRelPos := 1 - (xDelta / (cCurveValuesY[i]-xBase));
    Result := Round((i-1)*xFHeight + (xRelPos * xFHeight));
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  s: String;
  xx, xy: Integer;
begin
  inherited MouseDown(Button, Shift, X, Y);

  if (Button = mbLeft) and (Canvas.Pixels[X, Y] = FEditColor) then begin
    m_XStart := X;
    m_YStart := Y;
    m_MouseDown := True;
{
    s := Format('D=%.5g   L=%.5g', [YPosToDiameter(Y),XPosToLength(X)]);
    Canvas.TextOut(10, (Height div 2) + 10, s);
    s := Format('X=%d:%d  Y=%d:%d', [X,LengthToXPos(XPosToLength(X)), Y, DiameterToYPos(YPosToDiameter(Y))]);
    Canvas.TextOut(10, (Height div 2) + 20, s);
}
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  i: Integer;
  xX, xY: Integer;
  xPos: Single;
begin
  inherited MouseUp(Button, Shift, X, Y);
  m_MouseDown := False;
  if FCurveEdit <> ccNone then begin
    for i:=1 to cMaxKlassFieldY do begin
      with m_EditPara do begin
        xPos := LengthAtThickness(FCurveEdit, cCurveValuesY[i], m_EditPara);
        xX := LengthToXPos(xPos);
        xY := DiameterToYPos(cCurveValuesY[i]);
        with Canvas do begin
          Brush.Color := clYellow;
          Ellipse(xX-3, xY-3, xX+3, xY+3);
        end;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------
function TClearCurve.GetLengthAtThickness(const aValue: Single): Single;
begin
  Result := LengthAtThickness(FCurveEdit, aValue, m_EditPara);
end;

//------------------------------------------------------------------------
procedure TClearCurve.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  xRect: TRect;
begin
  inherited MouseMove(Shift, X, Y);
  if m_MouseDown then begin
    // forces the repaint for the drag-circle
{
    if FCurveEdit = ccNep then
      xRect := Rect(20-11, m_YStart-11,20+11, m_YStart+11)
    else
      xRect := Rect(m_XStart-12, m_YStart-12, m_XStart+12, m_YStart+12);
    InvalidateRect(Parent.Handle, @xRect, False);
}

    m_XStart := X;
    m_YStart := Y;
    with m_EditPara do
      case FCurveEdit of
        ccNep:
          NDiam := YPosToDiameter(Y, 1.8, 10.0);
        ccShort: begin
            SLong := XPosToLength(X, 0.0, 200.0);
            SDiam := YPosToDiameter(Y, 1.2, 10.0);
          end;
        ccLong: begin
            LLong := XPosToLength(X, 2.0, 200.0);
            LDiam := YPosToDiameter(Y, 1.2, 10.0);
          end;
        ccThin: begin
            TLong := XPosToLength(X, 2.0, 200.0);
            TDiam := YPosToDiameter(Y, 0.55, 0.83);
          end;
      end;
    CalculateDrawEditCurve;
  end else if Canvas.Pixels[X, Y] = FEditColor then
    Cursor := crHandPoint
  else
    Cursor := crDefault;

end;
//------------------------------------------------------------------------
procedure TClearCurve.Paint;
var
  xX,xY: Integer;
  xR1,xR2: TRect;
begin
  inherited Paint;
  with Canvas do
    if FCurveEdit = ccNone then begin
      Pen.Width := 1;
      if ctClearer in FCurveType then begin
        Pen.Color := FClearerColor;
        Pen.Style := psSolid;
        DrawCurve(m_Points[ctClearer, 1], False);     // Pos curve
        DrawCurve(m_Points[ctClearer, 2], True);      // Neg curve
      end;
      if ctSplice in FCurveType then begin
        Pen.Color := FSpliceColor;
        Pen.Style := psDash;
        DrawCurve(m_Points[ctSplice, 1], False);
        DrawCurve(m_Points[ctSplice, 2], True);
      end;
      if ctCluster in FCurveType then begin
        Pen.Color := FClusterColor;
        Pen.Style := psDot;
        DrawCurve(m_Points[ctCluster, 1], False);
        DrawCurve(m_Points[ctCluster, 2], True);
      end;
    end else begin
      // copy saved basic graphic to canvas
      xR1 := Self.ClientRect;
      xR2 := m_EditImage.ClientRect;
      Self.Canvas.CopyRect(xR1, m_EditImage.Canvas, xR2);
      // draw the Edit curve
      Pen.Color := clGreen;
      Pen.Style := psSolid;
      DrawCurve(m_EditPoints, (FCurveEdit = ccThin));
      // get position of ball point
      with m_EditPara do
        case FCurveEdit of
          ccNep: begin
            xX := 20;
            xY := DiameterToYPos(NDiam);
          end;
          ccShort: begin
            xX := LengthToXPos(SLong);
            xY := DiameterToYPos(SDiam);
          end;
          ccLong: begin
            xX := LengthToXPos(LLong);
            xY := DiameterToYPos(LDiam);
          end;
          ccThin: begin
            xX := LengthToXPos(TLong);
            xY := DiameterToYPos(TDiam);
          end;
        end;
      // draw ball point
      Pen.Color := FEditColor;
      Brush.Color := FEditColor;
      Ellipse(xX-10, xY-10, xX+10, xY+10);
    end;
end;
//------------------------------------------------------------------------
//------------------------------------------------------------------------
procedure TClearCurve.Refresh;
begin
  GenerateCurve(m_Points[ctClearer, 1], m_Points[ctClearer, 2], ctClearer);
  GenerateCurve(m_Points[ctSplice, 1], m_Points[ctSplice, 2], ctSplice);
  GenerateCurve(m_Points[ctCluster, 1], m_Points[ctCluster, 2], ctCluster);
  inherited Refresh;
end;
//------------------------------------------------------------------------
function TClearCurve.GetClearerPara(Index: Integer): rClearerParameter;
begin
  case index of
    0: Result := FCurvePara[ctClearer];
    1: Result := FCurvePara[ctSplice];
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.SetClearerPara(Index: Integer; Value: rClearerParameter);
begin
  case index of
    0: FCurvePara[ctClearer] := Value;
    1: FCurvePara[ctSplice]  := Value;
  end;
  with FCurvePara[ctClearer] do begin
    case SpliceMode of
      smValue, smLL: begin
          FCurvePara[ctSplice] := FCurvePara[ctClearer];
          FCurvePara[ctSplice].EnabledCurve :=
            FCurvePara[ctSplice].EnabledCurve - [ccNep]; // fix: no NepCurve
          FCurvePara[ctSplice].SLong     := 4.0;         // fix: ShortLength
          if SpliceMode = smValue then
            FCurvePara[ctSplice].LLong     := SpliceValue
          else
            FCurvePara[ctSplice].LLong     := LLong;  // value from ClearerPara
        end;
      smExt:;  // individual parameters, no dependence
    end;
  end;
  CalculateCurves;
  Refresh;
end;
//------------------------------------------------------------------------
procedure TClearCurve.SetCurveType(Value: sCurveType);
begin
  if Value <> FCurveType then begin
    FCurveType := Value;
    Visible := (FCurveType <> []);
  end;
  Refresh;
end;
//------------------------------------------------------------------------
procedure TClearCurve.SetCurveEdit(Value: eClearCurve);
begin
  if Value <> FCurveEdit then begin
    CurveType := [ctClearer];
    // Save current graphic in a Image
    if FCurveEdit = ccNone then begin
      m_EditImage := TImage.Create(Self);
      m_EditImage.Width := Width;
      m_EditImage.Height := Height;
      m_EditImage.Canvas.CopyRect(m_EditImage.ClientRect, Canvas, ClientRect);
    end;
    FCurveEdit := Value;
    if FCurveEdit <> ccNone then begin
      // copy start parameter
      m_EditPara := FCurvePara[ctClearer];
      Enabled   := True;                    // enable mouse events
      // calculate the selected curve (nep, short, long, thin)
      CalculateDrawEditCurve;
    end else begin
      // EditCurve finished: release saved graphic
      m_EditImage.Free;
      Enabled := False;                     // desable mouse events
    end;
  end;
end;
//------------------------------------------------------------------------

end.
