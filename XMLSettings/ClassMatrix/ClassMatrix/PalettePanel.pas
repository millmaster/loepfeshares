unit PalettePanel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, KlassField, KGridDef;

type
  TPalettePanel = class(TPanel)
  private
    FFieldCount: Integer;
    FValueMode: eValueMode;
    m_Fields: TList;
    procedure SetFieldCount(Value: Integer);
    procedure SetValueMode(Value: eValueMode);
  public
    property FieldCount: Integer read FFieldCount write SetFieldCount;
    property ValueMode: eValueMode read FValueMode write SetValueMode;
    procedure SetValue(Index: Integer; Mode: eValueMode; Value: DWord; Range: Single);
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
    procedure Resize; override;
  end;
//************************************************************************
implementation

constructor TPalettePanel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  // Set visual properties
  BevelInner := bvNone;
  BevelOuter := bvNone;
  ParentColor := True;

  FFieldCount := 0;
  FValueMode  := vmDots;

  m_Fields := TList.Create;
end;
//------------------------------------------------------------------------
destructor TPalettePanel.Destroy;
var
  i: Integer;
  xpField: PKlassField;
begin
  if m_Fields.Count > 0 then
    for i:=0 to m_Fields.Count-1 do begin
      xpField := PKlassField(m_Fields.Items[i]);
      xpField^.Free;
      dispose(xpField);
    end;
  m_Fields.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------
{
function SingleToString(n: Single): String;
var
  s: String;
  r: Single;
  e: Integer; // Exponent
begin
  if n<100000 then
    s := Format('%.5g', [n])
  else
  begin
    r := n; e := 0;
    while r>1000 do
    begin
      r := r/1000;
      inc(e,3);
    end;
    if r>99 then
      str(r:1:0,s)
    else
      str(r:1:1,s);
    s := s + 'E' + IntToStr(e);
  end;
  Result := s;
end;
}
//------------------------------------------------------------------------
procedure TPalettePanel.Paint;
var
  i,x,y: Integer;
  s: String;
begin
  inherited Paint;
  for i:=0 to m_Fields.Count-1 do begin
    x := TKlassField(m_Fields.Items[i]^).Width + 3;
    y := i * TKlassField(m_Fields.Items[i]^).Height + 1;
    s := '-> ' + Format('%.5g', [TKlassField(m_Fields.Items[i]^).UnCut]);
    Canvas.TextOut(x, y, s);
  end;
end;
//------------------------------------------------------------------------
procedure TPalettePanel.Resize;
var
  i: Integer;
  xField: TKlassField;
  xHeight,
  xWidth: Integer;
begin
  inherited Resize;
  if FFieldCount > 0 then begin
    xHeight := (Height div FFieldCount);
    xWidth := Width div 3;
    if m_Fields.Count > 0 then
      for i:=0 to m_Fields.Count-1 do begin
        xField := TKlassField(m_Fields.Items[i]^);
        with xField do begin
          Top := i*xHeight;
          SetSize(xHeight, xWidth);
        end;
      end;
  end;
end;
//------------------------------------------------------------------------
procedure TPalettePanel.SetValueMode(Value: eValueMode);
var
  i: Integer;
begin
  if (Value in [vmDots, vmColor]) then begin
    Visible := True;
    if (Value <> FValueMode) then begin
      FValueMode := Value;
      for i:=0 to m_Fields.Count-1 do
        TKlassField(m_Fields.Items[i]^).ValueMode := FValueMode;
    end;
  end else
    Visible := False;
end;
//------------------------------------------------------------------------
procedure TPalettePanel.SetFieldCount(Value: Integer);
  procedure UpdateFields;
  var
    xpField: PKlassField;
  begin
    if FFieldCount > m_Fields.Count then
      // Add Fields
      while m_Fields.Count < FFieldCount do begin
        new(xpField);
        xpField^ := TKlassField.Create(Self, m_Fields.Count, 0, 1, 0);
        xpField^.Parent := Self;
        if m_Fields.Count = 0 then
          xpField^.Border := $0F
        else
          xpField^.Border := $07;
        xpField^.ValueMode := FValueMode;
        m_Fields.Add(xpField);
      end
    else
      // Remove Fields
      with m_Fields do
        while Count > FFieldCount do begin
          // deletes the highest entry in List
          xpField := Items[m_Fields.Count-1];
          xpField^.Free;
          dispose(xpField);
          Delete(Count-1);
        end;
  end;
begin
  if Value <> FFieldCount then begin
    FFieldCount := Value;
    // Add or remove some Field in the palette
    UpdateFields;
    Resize;
  end;
end;
//------------------------------------------------------------------------
//------------------------------------------------------------------------
procedure TPalettePanel.SetValue(Index: Integer; Mode: eValueMode; Value: DWord; Range: Single);
var
  xField: TKlassField;
begin
//  dec(Index);
  if (Index >= 1) and (Index <= m_Fields.Count) and (Mode in [vmDots, vmColor]) then begin
    xField := TKlassField(m_Fields.Items[m_Fields.Count-Index]^);
    xField.UnCut := Range;
    if Mode = vmDots then
      xField.DotCount := Value
    else
      xField.BackColor := TColor(Value);
  end;
end;
//------------------------------------------------------------------------

end.
