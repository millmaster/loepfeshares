program ClassMatrix;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  KlassierTable in 'KlassierTable.pas',
  KlassierGrid in 'KlassierGrid.pas',
  KlassField in 'KlassField.pas',
  KGridDef in 'KGridDef.pas',
  KGridProperty in 'KGridProperty.pas' {KGProperty},
  DescPanel in 'DescPanel.pas',
  ClearCurve in 'ClearCurve.pas',
  PalettePanel in 'PalettePanel.pas',
  ClearParameter in '..\Parameter\ClearParameter.pas',
  NumComBox in '..\Diverses\NumComBox.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
