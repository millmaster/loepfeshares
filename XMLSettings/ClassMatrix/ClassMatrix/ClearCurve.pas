unit ClearCurve;
interface

uses
  Classes, Controls, ExtCtrls, Graphics, Math, SysUtils, Windows,
  KlassField, KGridDef, Tools;

type
  rCurvePoints = record
    // 1 = Pos-Curve, 2 = Neg-Curve
    Points: Array[1..2, 1..cMaxCurvePoints] of TPoint;
    Values: Array[1..2, 1..cMaxCurvePoints] of Single;
    Color: TColor;
    Style: TPenStyle;
  end;

  TClearCurve = class(TPaintBox)
  private
    FCurveType: sCurveType;
    FCurveEdit: eClearCurve;
    FEditColor: TColor;
    m_Curve: Array[ctClearer..ctCluster] of rCurvePoints;
    m_EditPoints: Array[1..cMaxCurvePoints] of TPoint;
    m_EditValues: Array[1..cMaxCurvePoints] of Single;
    m_EditPara: rClearerParameter;
    m_BackPara: rClearerParameter;
    m_TmpImage: TImage;
    m_Neighbor: Array[1..cVirtualKlassFields] of rNeighbors;
    m_MouseDown: Boolean;
    m_ClassFields: sClassFields;
    FCurvePara: Array[ctClearer..ctCluster] of rClearerParameter;
    procedure CalculateAllCurves;
    procedure CalculateDrawEditCurve;
    procedure SetClearerPara(Index: Integer; Value: rClearerParameter);
    function GetClearerPara(Index: Integer): rClearerParameter;
    procedure SetCurveType(Value: sCurveType);
    procedure SetCurveEdit(Value: eClearCurve);
    procedure SetCurveColor(aIndex: Integer; Value: TColor);
    function GetCurveColor(aIndex: Integer): TColor;
  protected
    function YPosToDiameter(aY: Integer; aMin, aMax: Single): Single;
    function XPosToLength(aX: Integer; aMin, aMax: Single): Single;
    function DiameterToYPos(aDiameter: Single): Integer;
    function LengthToXPos(aLength: Single): Integer;
    procedure ConvertToScreen(var aPoints: rCurvePoints; aTyp: eCurveType);
    procedure CalculateCurve(var aValues: Array of Single; aCurve: eClearCurve; Dicke, Long: Single; aOffset: Boolean; aCnt: Integer);
    procedure DrawCurve(Points: Array of TPoint; aNegCurve: Boolean);
    function GetAxFactor(var aF: Single; aCurve: eClearCurve; Dicke, Long: Single):Single;
    function LengthAtThickness(aCurve: eClearCurve; aD: Single; aPara: rClearerParameter): Single;
    function DiamAtLength(var aLength: Single; aCurve: eClearCurve; aDicke, aLong: Single): Single;
    procedure GetUncutAndCut(aValues: Array of Single; aCurve: eClearCurve; var aUnCutted, aCutted: Single);
  public
    property ClearerColor: TColor index 0 read GetCurveColor write SetCurveColor default clRed;
    property SpliceColor: TColor index 1 read GetCurveColor write SetCurveColor default clBlue;
    property ClusterColor: TColor index 2 read GetCurveColor write SetCurveColor default clGreen;
    property CurveType: sCurveType read FCurveType write SetCurveType default [ctClearer, ctSplice, ctCluster];
    property CurveEdit: eClearCurve read FCurveEdit write SetCurveEdit default ccNep;
    property EditColor: TColor read FEditColor write FEditColor default clPurple;

    property ClearerPara: rClearerParameter index 0 read GetClearerPara write SetClearerPara;
    property SplicePara: rClearerParameter index 1 read GetClearerPara write SetClearerPara;

    constructor Create(aOwner: TComponent); override;
    function GetLengthAtThickness(const aValue: Single): Single;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure Refresh;
    procedure Resize;
    procedure SetFieldValues(aValue: Array of Single);
    procedure SetFieldClass(aSet: sClassFields);
    procedure GetCutInfo(var aUncutted, aCutted, aRefUc, aRefC: Single);
  end;

implementation
//------------------------------------------------------------------------
constructor TClearCurve.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Align    := alNone;
  Enabled  := False;
  Left     := 0;
  ShowHint := False;
  Top      := 0;
  Visible  := False;

  with FCurvePara[ctClearer] do begin
    EnabledCurve := [{ccNep, }ccShort, ccLong, ccThin];
    Diam[ccNep]   := 6.0;
    Diam[ccShort] := 2.2;
    Long[ccShort] := 2.4;
    Diam[ccLong]  := 1.26;
    Long[ccLong]  := 40;
    Diam[ccThin]  := 0.70;     // expect Thin-Parameter as factor, not as percentage
    Long[ccThin]  := 90;
    LowCurve      := 0.90;     // expect Cluster-Parameter as factor, not as percentage
    SpliceMode    := smValue;
    SpliceValue   := 20;
  end;
  // copies the default settings
  FCurvePara[ctSplice] := FCurvePara[ctClearer];
  FCurvePara[ctSplice].CheckLength := 15;
  FCurvePara[ctCluster] := FCurvePara[ctClearer];
  FCurvePara[ctCluster].EnabledCurve := [ccShort];

  FCurveType  := [{ctClearer, ctSplice, ctCluster}];
  FCurveEdit  := ccNone;
  FEditColor  := clFuchsia; //clPurple;

  m_Curve[ctClearer].Color := clRed;
  m_Curve[ctClearer].Style := psSolid;
  m_Curve[ctSplice].Color  := clBlue;
  m_Curve[ctSplice].Style  := psDash;
  m_Curve[ctCluster].Color := clGreen;
  m_Curve[ctCluster].Style := psDot;
  m_TmpImage := Nil;
  m_MouseDown := False;
  m_ClassFields := [];

  CalculateAllCurves;
end;
//------------------------------------------------------------------------
procedure TClearCurve.CalculateAllCurves;
var
  i: Integer;
  xCType: eCurveType;
  xCurve: eClearCurve;
  xFirst: Boolean;
begin
  for xCType:=ctClearer to ctCluster do begin
    // clear y-data array curve: set to high value
    for i:=1 to cMaxCurvePoints do begin
      m_Curve[xCType].Values[1,i] := cCurveValuesY[1];
      m_Curve[xCType].Values[2,i] := cCurveValuesY[1];
    end;
    // calculate curve
    with FCurvePara[xCType] do
      for xCurve:=ccNep to ccThin do
        if xCurve in EnabledCurve then
          if xCurve = ccThin then
            CalculateCurve(m_Curve[xCType].Values[2], xCurve, Diam[xCurve], Long[xCurve], False, cMaxCurvePoints)
          else
            CalculateCurve(m_Curve[xCType].Values[1], xCurve, Diam[xCurve], Long[xCurve], False, cMaxCurvePoints);
  end;

  // calculate ClusterCurve with parameter from ClearerCurve
  for i:=1 to cMaxCurvePoints do
    with m_Curve[ctCluster] do
      if Values[1,i] < 11.0 then
        Values[1,i] := Values[1,i] * FCurvePara[ctCluster].LowCurve;

  xFirst := True;
  // evaluate cross points with clearer curve
  for i:=1 to cMaxCurvePoints do
    with m_Curve[ctCluster] do begin
      if xFirst and (Values[1,i+1] < m_Curve[ctClearer].Values[1,i+1]) and
         (i<cMaxCurvePoints) then begin
        xFirst := False;
        Values[1,i] := m_Curve[ctClearer].Values[1,i];
      end else if Values[1,i] > m_Curve[ctClearer].Values[1,i] then
        Values[1,i] := 11.0
    end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.CalculateCurve(var aValues: Array of Single; aCurve: eClearCurve; Dicke, Long: Single; aOffset: Boolean; aCnt: Integer);
var
  xLen, xStep: Single;
//  xValues: Array[1..cMaxCurvePoints] of Single;
  xValue: Single;
  xPointsPerField: Integer;

  i,j: Integer;
  xPointIndex: Integer;

  procedure CopyLowerValues(aSrc: Array of Single; var aDst: Array of Single);
  var i: Integer;
  begin
    for i:=0 to aCnt-1 do
      if aSrc[i] < aDst[i] then aDst[i] := aSrc[i];
  end;

begin
  xPointIndex := 0;
  xPointsPerField := aCnt div cMaxKlassFieldX;
  for i:=1 to cMaxKlassFieldX do begin
    xLen := cCurveValuesX[i];  // get start lenght
    xStep := (cCurveValuesX[i+1] - cCurveValuesX[i]) / xPointsPerField;
    // calculate at middle of the one third area (Offset = True) or at the right side
    if aOffset then
      xLen := xLen -(xStep/2);

    for j:=1 to xPointsPerField do begin
      xLen := xLen + xStep;
      xValue := DiamAtLength(xLen, aCurve, Dicke, Long);
      if xValue < aValues[xPointIndex] then
        aValues[xPointIndex] := xValue;
      inc(xPointIndex);
    end;
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.CalculateDrawEditCurve;
var
  i: Integer;
  xPixelStep: Single;
begin
  if FCurveEdit <> ccNone then begin
    // set default values to highes value
    for i:=1 to cMaxCurvePoints do
      m_EditValues[i] := cCurveValuesY[1];

    with m_EditPara do
      CalculateCurve(m_EditValues, FCurveEdit, Diam[FCurveEdit], Long[FCurveEdit], False, cMaxCurvePoints);

    xPixelStep := Width / cMaxCurvePoints;
    for i:=1 to cMaxCurvePoints do begin
      m_EditPoints[i].X := Round(i*xPixelStep);
      m_EditPoints[i].Y := DiameterToYPos(m_EditValues[i]);
    end;
    Paint;
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.ConvertToScreen(var aPoints: rCurvePoints; aTyp: eCurveType);
var
  i: Integer;
  xPixelStep: Single;
begin
  xPixelStep := Width / cMaxCurvePoints;
  // define Pixel for Pos-Curve
  for i:=1 to cMaxCurvePoints do begin
    aPoints.Points[1,i].X := Round((i)*xPixelStep);
    aPoints.Points[1,i].Y := DiameterToYPos(aPoints.Values[1,i]);
  end;
  // define Pixel for Neg-Curve
  for i:=1 to cMaxCurvePoints do begin
    aPoints.Points[2,i].X := Round((i)*xPixelStep);
    aPoints.Points[2,i].Y := DiameterToYPos(aPoints.Values[2,i]);
  end;
end;
//------------------------------------------------------------------------
function TClearCurve.DiamAtLength(var aLength: Single; aCurve: eClearCurve; aDicke, aLong: Single): Single;
var
  xA,xF: Single;
begin
  xA := GetAxFactor(xF, aCurve, aDicke, aLong);
  if aCurve = ccNep then
    Result := aDicke
  else
    Result :=(xA / aLength) + (xF * aDicke);
end;
//------------------------------------------------------------------------
function TClearCurve.DiameterToYPos(aDiameter: Single): Integer;
var
  i: Integer;
  xFHeight: Integer;
  xDelta: Single;
  xRelPos: Single;
begin
  if (aDiameter >= cCurveValuesY[1]) or (aDiameter <= cCurveValuesY[High(cCurveValuesY)]) then
    Result := 0
  else begin
    i := 1;
    while aDiameter < cCurveValuesY[i] do
      inc(i);
    xDelta   := aDiameter - cCurveValuesY[i];
    xFHeight := Height div cMaxKlassFieldY;
    // evaluates the position in the Field from the top border
    xRelPos := 1 - (xDelta / (cCurveValuesY[i-1] - cCurveValuesY[i]));
    Result := Round((i-2)*xFHeight + (xRelPos * xFHeight));
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.DrawCurve(Points: Array of TPoint; aNegCurve: Boolean);
var
  i,k: Integer;
  xFirst: Boolean;
begin
  // Attention!! Array as Parameter begins with 0
    for k:=0 to 1 do begin              // Thickness
      xFirst := True;
      with Canvas do begin
        MoveTo(Points[0].X, Points[0].Y + k);
        for i:=1 to cMaxCurvePoints-1 do begin
          if Points[i].Y = 0 then
            MoveTo(Points[i].X, Points[i].Y + k)
          else if aNegCurve and xFirst then begin
            MoveTo(Points[i].X, Points[i].Y + k);
            xFirst := False;
          end else
            LineTo(Points[i].X, Points[i].Y + k);
        end;
      end;
    end;
end;
//------------------------------------------------------------------------
function TClearCurve.GetAxFactor(var aF: Single; aCurve: eClearCurve; Dicke, Long: Single):Single;
begin
  case aCurve of
    ccShort: begin
      aF := cFs;
      Result := (cKs * (cFs*Dicke - cBAS) + cBAS - cFs*Dicke) * Long * cGs;
    end;
    ccLong: begin
      aF := cFl;
      Result := (cKl * (cFl*Dicke - cBAS) + cBAS - cFl*Dicke) * Long * cGl;
    end;
    ccThin: begin
      aF := cFt;
      Result := -((-cKt * (cFt*Dicke - cBAS) - cBAS + cFt*Dicke) * Long);
    end;
  else // ccNep
    Result := 1;
  end;
end;
//------------------------------------------------------------------------
function TClearCurve.GetClearerPara(Index: Integer): rClearerParameter;
begin
  case index of
    0: Result := FCurvePara[ctClearer];
    1: Result := FCurvePara[ctSplice];
  end;
end;
//------------------------------------------------------------------------
function TClearCurve.GetCurveColor(aIndex: Integer): TColor;
begin
  case aIndex of
    0: Result := m_Curve[ctClearer].Color;
    1: Result := m_Curve[ctSplice].Color;
  else
    Result := m_Curve[ctCluster].Color;
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.GetCutInfo(var aUncutted, aCutted, aRefUc, aRefC: Single);
const
  cxPointsPerField = 3;
  cxMaxCurvePoints = cxPointsPerField * cMaxKlassFieldX;
var
  i: Integer;
  xCurve: eClearCurve;
  xValues: Array[1..2, 1..cxMaxCurvePoints] of Single;
  xUnCutted, xCutted: Single;
begin
  // set the referenced Cut parameters
  aRefUC := SumSingle(cValues[1]);
  aRefC  := SumSingle(cValues[2]);

  // clear y-data array curve: set to high value
  for i:=1 to cxMaxCurvePoints do begin
    xValues[1,i] := cCurveValuesY[1];
    xValues[2,i] := cCurveValuesY[1];
  end;

  // Calculate points in X-middle of the Fields
  with m_EditPara do begin
    CalculateCurve(xValues[2], ccThin, Diam[ccThin], Long[ccThin], True, cxMaxCurvePoints);
    for xCurve:=ccNep to ccLong do
      if xCurve in EnabledCurve then
        CalculateCurve(xValues[1], xCurve, Diam[xCurve], Long[xCurve], True, cxMaxCurvePoints);
  end;

  // get the Uncutted / Cutted values
  aUncutted := 0; aCutted := 0;
  for xCurve := ccShort to ccThin do begin
    if xCurve = ccThin then
      GetUncutAndCut(xValues[2], ccThin, xUncutted, xCutted)
    else
      GetUncutAndCut(xValues[1], xCurve, xUncutted, xCutted);
    aUncutted := aUncutted + xUncutted;
    aCutted  := aCutted + xCutted;
  end;
end;
//------------------------------------------------------------------------
function TClearCurve.GetLengthAtThickness(const aValue: Single): Single;
begin
  Result := LengthAtThickness(FCurveEdit, aValue, m_EditPara);
end;
//------------------------------------------------------------------------
procedure TClearCurve.GetUncutAndCut(aValues: Array of Single; aCurve: eClearCurve; var aUnCutted, aCutted: Single);
var
  xPixelStep: Single;
  xBase,xStep,xValue: Single;
  xPointIndex: Integer;
  xSubField: Integer;
  xFieldCnt: Integer;
  xFieldX,xFieldY,xSubX,xSubY: Integer;
  xFieldNr,xOffset,xPointOffset,xYIndex,xStart, xEnd: Integer;
  xClassNr: Integer;
begin
  case aCurve of
    ccLong: begin
        xFieldCnt := 9;
        xOffset := 104;
        xPointOffset := 12;
        xYIndex := 18;
        xStart := 5;
        xEnd   := 8;
      end;
    ccThin: begin
        xFieldCnt := 9;
        xOffset := 104;
        xPointOffset := 12;
        xYIndex := 19;
        xStart := 1;
        xEnd   := 4;
      end;
  else  // ccShort
    xFieldCnt := 13;
    xOffset := 0;
    xPointOffset := 0;
    xYIndex := 10;
    xStart := 1;
    xEnd   := 8;
  end;

  aUnCutted := 0; aCutted := 0;
  xPixelStep := Width / (High(aValues)+1);
  for xFieldX:=0 to xFieldCnt-1 do begin
    for xFieldY:=xStart to xEnd do begin
      xBase := cCurveValuesY[xYIndex-xFieldY];                 // value at lower border as Base
      xStep := (cCurveValuesY[xYIndex-1-xFieldY] - xBase) / 3; // middle to middle
      xBase := xBase - (xStep/2);                   // offset to lower border of field
      xFieldNr   := xOffset + (xFieldX*8) + xFieldY;           // number of virtual field
      // get the belonging number of the ClassField to check if ClassCleaning is enabled
      case xFieldNr of
        57..72:   xClassNr := xFieldNr - 8;
        73..80:   xClassNr := xFieldNr - 16;
        81..88:   xClassNr := xFieldNr - 24;
        89..96:   xClassNr := xFieldNr - 32;
        97..112:  xClassNr := xFieldNr - 40;
        113..176: xClassNr := xFieldNr - 48;
      else  // 1..56
        xClassNr := xFieldNr;
      end;
      // if ClassCleaning is enabled, add the sum of all calc to Cutted value
      if xClassNr in m_ClassFields then
        aCutted := aCutted + SumSingle(m_Neighbor[xFieldNr].calc)
      else
        // else check the 9 points in the virtual field
        for xSubX:=0 to 2 do begin              // count SubFields in x direction
          // Index to compare with the value from calculated curve
          xPointIndex := xPointOffset + (xFieldX*3) + xSubX;
          for xSubY:=1 to 3 do begin            // count SubFields in y direction
            xSubField := (xSubX*3) + xSubY;     // SubField (1-9) in the virtual Field
            xValue := xBase + (xSubY*xStep);    // this is the reference value
            with m_Neighbor[xFieldNr] do
              if ((aCurve = ccThin)  and (xValue >= aValues[xPointIndex])) or
                 ((aCurve <> ccThin) and (xValue <= aValues[xPointIndex])) then begin
                aUnCutted := aUnCutted + calc[xSubField];

  {
                X2 := Round(((xPointIndex+1)*xPixelStep) - (xPixelStep/2));
                Y2 := DiameterToYPos(xValue);
                with Canvas do begin
                  Brush.Color := clYellow;
                  Ellipse(X2-2, Y2-2, X2+2, Y2+2);
                end;
  {}
              end else
                aCutted := aCutted + calc[xSubField];

          end;
        end;
    end;
  end;
end;
//------------------------------------------------------------------------
function TClearCurve.LengthAtThickness(aCurve: eClearCurve; aD: Single; aPara: rClearerParameter): Single;
var
  xA, xF: Single;
begin
  if aCurve in [ccShort, ccLong, ccThin] then begin
    xA := GetAxFactor(xF, aCurve, aPara.Diam[aCurve], aPara.Long[aCurve]);
    Result := xA / (aD - (xF * aPara.Diam[aCurve]));
  end else
    Result := 0;
end;
//------------------------------------------------------------------------
function TClearCurve.LengthToXPos(aLength: Single): Integer;
var
  xFWidth: Integer;
  i: Integer;
  xDelta: Single;
  xRelPos: Single;
begin
  if (aLength <= cCurveValuesX[1]) or (aLength >= cCurveValuesX[High(cCurveValuesX)]) then
    Result := 0
  else begin
    i := 1;
    while aLength > cCurveValuesX[i+1] do
      inc(i);
    xDelta  := aLength - cCurveValuesX[i];
    xFWidth := Width div cMaxKlassFieldX;
    // evaluates the position in the Field from the left border
    xRelPos := xDelta / (cCurveValuesX[i+1] - cCurveValuesX[i]);
    Result  := Round((i-1)*xFWidth + (xRelPos * xFWidth));
  end;
end;                 
//------------------------------------------------------------------------
procedure TClearCurve.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseDown(Button, Shift, X, Y);
  if (Button = mbLeft) and (Canvas.Pixels[X, Y] = FEditColor) then
    m_MouseDown := True;
end;
//------------------------------------------------------------------------
procedure TClearCurve.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  c: TColor;
begin
  inherited MouseMove(Shift, X, Y);
  if m_MouseDown then begin
    if (X>=0) and (X<=Width-1) and (Y>=0) and (Y<=Height-1) then begin
      with m_EditPara do
        case FCurveEdit of
          ccNep:
            Diam[ccNep] := YPosToDiameter(Y, 3.0, 11.0);
          ccShort: begin
              Long[ccShort] := XPosToLength(X, 1.0, 10.0);
              Diam[ccShort] := YPosToDiameter(Y, 1.5, 4.0);
            end;
          ccLong: begin
              Long[ccLong] := XPosToLength(X, 6.0, 200.0);
              Diam[ccLong] := YPosToDiameter(Y, 1.10, 2.0);
            end;
          ccThin: begin
              Long[ccThin] := XPosToLength(X, 6.0, 200.0);
              Diam[ccThin] := YPosToDiameter(Y, 0.55, 0.9);
            end;
        end;
      CalculateDrawEditCurve;
    end;
  end else if Canvas.Pixels[X, Y] = FEditColor then
    Cursor := crHandPoint
  else
    Cursor := crDefault;
end;
//------------------------------------------------------------------------
procedure TClearCurve.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xOldCurve: eClearCurve;
  xSumUC,xSumC, xUnCutted, xCutted: Single;
  xS: String;
begin
  inherited MouseUp(Button, Shift, X, Y);
  if m_MouseDown then begin
    m_MouseDown := False;
    // copy parameters and calculate the curves new
    FCurvePara[ctClearer] := m_EditPara;
    CalculateAllCurves;

    // paint curve with new parameters without edit courve and ...
    xOldCurve := FCurveEdit;
    FCurveEdit := ccNone;
    Refresh;
    // ... copy new image in temporary Canvas
    m_TmpImage.Canvas.CopyRect(m_TmpImage.ClientRect, Canvas, ClientRect);
    // calculate the edit curve again
    FCurveEdit := xOldCurve;
    Paint;
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.Paint;
var
  xX,xY: Integer;
  xR1,xR2: TRect;
  xCurve: eCurveType;
begin
  inherited Paint;
  with Canvas do
    if FCurveEdit = ccNone then begin
      for xCurve:=ctClearer to ctCluster do
        if xCurve in FCurveType then
          with m_Curve[xCurve] do begin
            Pen.Color := Color;
            Pen.Style := Style;
            DrawCurve(Points[1], (xCurve=ctCluster));     // Pos curve
            DrawCurve(Points[2], True);      // Neg curve
          end;
    end else begin
      // copy saved basic graphic to canvas
      xR1 := Self.ClientRect;
      xR2 := m_TmpImage.ClientRect;
      Canvas.CopyRect(ClientRect, m_TmpImage.Canvas, m_TmpImage.ClientRect);
      // draw the Edit curve
      Pen.Color := clGreen;
      Pen.Style := psSolid;
      DrawCurve(m_EditPoints, (FCurveEdit = ccThin));
      // get position of ball point
      with m_EditPara do begin
        xY := DiameterToYPos(Diam[FCurveEdit]);
        if FCurveEdit = ccNep then
          xX := 20
        else
          xX := LengthToXPos(Long[FCurveEdit]);
      end;
      // draw ball point
      Pen.Color := FEditColor;
      Brush.Color := FEditColor;
      Ellipse(xX-10, xY-10, xX+10, xY+10);
    end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.Refresh;
begin
  ConvertToScreen(m_Curve[ctClearer], ctClearer);
  ConvertToScreen(m_Curve[ctSplice ], ctSplice);
  ConvertToScreen(m_Curve[ctCluster], ctCluster);
  inherited Refresh;
end;
//------------------------------------------------------------------------
procedure TClearCurve.Resize;
var
  xOldCurve: eClearCurve;
begin
  if FCurveEdit <> ccNone then begin
    xOldCurve := FCurveEdit;
    FCurveEdit := ccNone;
    Refresh;

    // Recreate the TmpImage and copy new image in temporary Canvas
    m_TmpImage.Free;
    m_TmpImage := TImage.Create(Self);
    m_TmpImage.Width  := Width;
    m_TmpImage.Height := Height;
    m_TmpImage.Canvas.CopyRect(m_TmpImage.ClientRect, Canvas, ClientRect);

    FCurveEdit := xOldCurve;
    CalculateDrawEditCurve;
  end else
    Refresh;
end;
//------------------------------------------------------------------------
procedure TClearCurve.SetClearerPara(Index: Integer; Value: rClearerParameter);
begin
  case index of
    0: begin
         FCurvePara[ctClearer] := Value;
         FCurvePara[ctCluster] := Value;
         with FCurvePara[ctCluster] do
           if (ccShort in EnabledCurve) and (LowCurve >= 0.5) then
             EnabledCurve := [ccShort]
           else
             EnabledCurve := [];
       end;
    1: FCurvePara[ctSplice]  := Value;
  end;
  with FCurvePara[ctClearer] do begin
    case SpliceMode of
      smValue, smLL: begin
          FCurvePara[ctSplice] := FCurvePara[ctClearer];
          FCurvePara[ctSplice].EnabledCurve :=
            FCurvePara[ctSplice].EnabledCurve - [ccNep]; // fix: no NepCurve
          FCurvePara[ctSplice].Long[ccShort] := 4.0;     // fix: ShortLength
          if SpliceMode = smValue then
            FCurvePara[ctSplice].Long[ccLong]:= SpliceValue
          else
            FCurvePara[ctSplice].Long[ccLong]:= Long[ccLong];// value from ClearerPara
        end;
      smExt:;  // individual parameters, no dependences
    end;
  end;
  CalculateAllCurves;
  if FCurveType <> [] then
    Refresh;
end;
//------------------------------------------------------------------------
procedure TClearCurve.SetCurveColor(aIndex: Integer; Value: TColor);
begin
  case aIndex of
    1: m_Curve[ctSplice].Color := Value;
    2: m_Curve[ctCluster].Color := Value;
  else // 0
    m_Curve[ctClearer].Color := Value;
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.SetCurveEdit(Value: eClearCurve);
begin
  if Value <> FCurveEdit then begin
    if Value <> ccNone then begin
      FCurveEdit := ccNone;
      CurveType := [ctClearer];
      if not Assigned(m_TmpImage) then begin
        // Create a temporary Canvas
        m_TmpImage := TImage.Create(Self);
        m_TmpImage.Width := Width;
        m_TmpImage.Height := Height;
        // save current original parameters
        m_BackPara := FCurvePara[ctClearer];
      end;
      // Save current Image in temporary Canvas
      m_TmpImage.Canvas.CopyRect(m_TmpImage.ClientRect, Canvas, ClientRect);
      // begin with original parameter of ClearerCurve
      m_EditPara := FCurvePara[ctClearer];
      // calculate the selected curve (nep, short, long, thin)
      FCurveEdit := Value;
      CalculateDrawEditCurve;
      // enable mouse events to move curves
      Enabled   := True;
    end else begin
      Enabled := False;                     // disable mouse events
      // EditCurveMode finished: release saved graphic
      m_TmpImage.Free;
      m_TmpImage := Nil;
      FCurveEdit := ccNone;
      // restore original parameters
      FCurvePara[ctClearer] := m_BackPara;
      CalculateAllCurves;
      Refresh;
    end;
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.SetCurveType(Value: sCurveType);
begin
  if (Value <> FCurveType) and (FCurveEdit = ccNone) then begin
    FCurveType := Value;
    Visible := (FCurveType <> []);
  end;
  Refresh;
end;
//------------------------------------------------------------------------
procedure TClearCurve.SetFieldValues(aValue: Array of Single);
var
  i: Integer;

  procedure SetNeighborValue(aIndex: Integer; var aNeighbor: rNeighbors; aValues: Array of Single);
  var
    i: Integer;
    xFieldNr: Integer;
    {  Neighbors im Pos-Bereich:
       | 1  |  2 |
       |----|----|
       | 0  |  3 |
       |----|----|
      *** ACHTUNG!!!! Beim Array als Parameter beginnt der indexierte Zugriff bei 0}
  begin
    xFieldNr := aIndex;
    case xFieldNr of
      57..72:   aIndex := xFieldNr - 8;
      73..80:   aIndex := xFieldNr - 16;
      81..88:   aIndex := xFieldNr - 24;
      89..96:   aIndex := xFieldNr - 32;
      97..112:  aIndex := xFieldNr - 40;
      113..176: aIndex := xFieldNr - 48;
    end;
    dec(aIndex);  // Parameter array begins with 0
    for i:=0 to 3 do begin
      case i of
        // bottom left
        0: case xFieldNr of
             49..64,109..112,117..120:
               aNeighbor.values[i] := aValues[aIndex]/2;
             65..104:
               aNeighbor.values[i] := aValues[aIndex]/5;
{
             57..64,117..120:
               aNeighbor.values[i] := (aValues[aIndex]+aValues[aIndex+8])/2;
             73..80:
               aNeighbor.values[i] := aValues[aIndex]*4/5;
             81..88:
               aNeighbor.values[i] := aValues[aIndex]*3/5;
             89..96:
               aNeighbor.values[i] := aValues[aIndex]*2/5;
             97..104:
               aNeighbor.values[i] := aValues[aIndex]*1/5;
}
             // Thin range
             106..108,114..116:
               aNeighbor.values[i] := aValues[aIndex-1]/2;
             122..124,130..132,138..140,146..148,154..156,162..164,170..172:
               aNeighbor.values[i] := aValues[aIndex-1];
{
             106..108,122..124,130..132,138..140,146..148,154..156,162..164,170..172:
               aNeighbor.values[i] := aValues[aIndex-1];
             114..116:
               aNeighbor.values[i] := (aValues[aIndex-1]+aValues[aIndex+7])/2;
}
             // lower border of Thin range
             105,113,121,129,137,145,153,161,169:
               aNeighbor.values[i] := 0;
           else
             aNeighbor.values[0] := aValues[aIndex];
           end;
        // top left
        1: case xFieldNr of
             49..55,57..63:
               aNeighbor.values[i] := aValues[aIndex+1]/2;
             65..71,73..79,81..87,89..95,97..103:
               aNeighbor.values[i] := aValues[aIndex+1]/5;
{
             57..63:
               aNeighbor.values[i] := (aValues[aIndex+1]+aValues[aIndex+9])/2;
             73..79:
               aNeighbor.values[i] := aValues[aIndex+1]*4/5;
             81..87:
               aNeighbor.values[i] := aValues[aIndex+1]*3/5;
             89..95:
               aNeighbor.values[i] := aValues[aIndex+1]*2/5;
             97..103:
               aNeighbor.values[i] := aValues[aIndex+1]*1/5;
}
             112: aNeighbor.values[i] := aValues[32];   // 72: Field 33
             120: aNeighbor.values[i] := aValues[40];   //     Field 41
             128: aNeighbor.values[i] := aValues[48];   // 80: Field 49
             136: aNeighbor.values[i] := (aValues[48]+aValues[56])/2;
             144: aNeighbor.values[i] := aValues[56];   // 96: Field 57
             152,160,168,176:
               aNeighbor.values[i] := aValues[56]/5;
             // Thin range
             121..124,129..132,137..140,145..148,153..156,161..164,169..172:
               aNeighbor.values[i] := aValues[aIndex];
             105..108,113..116:
               aNeighbor.values[i] := aValues[aIndex]/2;
{
             105..108,121..124,129..132,137..140,145..148,153..156,161..164,169..172:
               aNeighbor.values[i] := aValues[aIndex];
             113..116:
               aNeighbor.values[i] := (aValues[aIndex]+aValues[aIndex+8])/2;
}
             // Fields at top border
             8,16,24,32,40,48,56,64,72,80,88,96,104:
               aNeighbor.values[i] := 0;
           else
             aNeighbor.values[i] := aValues[aIndex+1];
           end;
        // top right
        2: case xFieldNr of
             49..55,57..63,109..111,117..119:
               aNeighbor.values[i] := aValues[aIndex+1]/2;
             65..71,73..79,81..87,89..95:
               aNeighbor.values[i] := aValues[aIndex+1]/5;
{
             49..55, 109..111:
               aNeighbor.values[i] := (aValues[aIndex+1]+aValues[aIndex+9])/2;
             65..71:
               aNeighbor.values[i] := aValues[aIndex+1]*4/5;
             73..79:
               aNeighbor.values[i] := aValues[aIndex+1]*3/5;
             81..87:
               aNeighbor.values[i] := aValues[aIndex+1]*2/5;
             89..95:
               aNeighbor.values[i] := aValues[aIndex+1]*1/5;
}
             112: aNeighbor.values[i] := aValues[40];  // 72: Field 41
             120: aNeighbor.values[i] := aValues[48];  //   : Field 49
             128: aNeighbor.values[i] := aValues[48]/2;// 80
             136: aNeighbor.values[i] := aValues[56];  // 88: Field 57
             144,152,160,168:
               aNeighbor.values[i] := aValues[56]/5;
             // Thin range
             105..108:
               aNeighbor.values[i] := aValues[aIndex]/2;
             113..116,121..124,129..132,137..140,145..148,153..156,161..164:
               aNeighbor.values[i] := aValues[aIndex+8];
{
             105..108:
               aNeighbor.values[i] := (aValues[aIndex]+aValues[aIndex+8])/2;
             113..116,121..124,129..132,137..140,145..148,153..156,161..164:
               aNeighbor.values[i] := aValues[aIndex+8];
}
             // Fields at top right border
             8,16,24,32,40,48,56,64,72,80,88,96,97..104,169..176:
               aNeighbor.values[i] := 0;
           else
             aNeighbor.values[i] := aValues[aIndex+9];
           end;
        // bottom right
        3: case xFieldNr of
             49..56,109..112:
               aNeighbor.values[i] := aValues[aIndex]/2;
             57..64:
               aNeighbor.values[i] := aValues[aIndex+8]/5;
             65..96:
               aNeighbor.values[i] := aValues[aIndex]/5;
{
             49..56,109..112:
               aNeighbor.values[i] := (aValues[aIndex]+aValues[aIndex+8])/2;
             57..64:
               aNeighbor.values[i] := aValues[aIndex+8];
             65..72:
               aNeighbor.values[i] := aValues[aIndex]*4/5;
             73..80:
               aNeighbor.values[i] := aValues[aIndex]*3/5;
             81..88:
               aNeighbor.values[i] := aValues[aIndex]*2/5;
             89..96:
               aNeighbor.values[i] := aValues[aIndex]*1/5;
}
             // Thin range
             114..116,122..124,130..132,138..140,146..148,154..156,162..164:
               aNeighbor.values[i] := aValues[aIndex+7];
             106..108:
               aNeighbor.values[i] := aValues[aIndex-1]/2;
{
             114..116,122..124,130..132,138..140,146..148,154..156,162..164:
               aNeighbor.values[i] := aValues[aIndex+7];
             106..108:
               aNeighbor.values[i] := (aValues[aIndex-1]+aValues[aIndex+7])/2;
}
             // Thin range and Fields at right border
             105,113,121,129,137,145,153,161,
             97..104,169..176:
               aNeighbor.values[i] := 0;
           else
             aNeighbor.values[i] := aValues[aIndex+8];
           end;
      end;
    end;
  end;

  procedure InterpolateField(var aField: rNeighbors; aIndex: Integer);
  var
    i: Integer;
  begin
    with aField do
      for i:=1 to 9 do
        if aIndex in [105..108,113..116,121..124,129..132,137..140,145..148,153..156,161..164,169..172] then
          calc[i] := values[1] / 9
        else
          calc[i] := values[0] / 9;
{
    for i:=0 to 8 do
      with aField do begin
        // Thin range
//        if aIndex in [65..68,73..76,81..84,89..92,97..100,105..108,113..116,121..124] then
          case i of
            0: calc[i] := Sum(values)/6;
            1: calc[i] := values[0];
            2: calc[i] := values[0] - ((values[0]-values[1])/3);
            3: calc[i] := values[1] + ((values[0]-values[1])/3);
            4: calc[i] := values[0] - ((values[0]-values[3])/3);
            5: calc[i] := values[1] - ((values[1]-values[2])/3);
            6: calc[i] := values[3] + ((values[0]-values[3])/3);
            7: calc[i] := values[3] - ((values[3]-values[2])/3);
            8: calc[i] := values[2] + ((values[3]-values[2])/3);
          end;

        if calc[i] > 0 then begin
          calc[i] := ln(calc[i]);
          if calc[i] < xOffset then
            xOffset := calc[i];
        end;

      end;
}
  end;
begin
  for i:=1 to cVirtualKlassFields do begin
    SetNeighborValue(i, m_Neighbor[i], aValue);
    InterpolateField(m_Neighbor[i], i);
  end;
end;
//------------------------------------------------------------------------
procedure TClearCurve.SetFieldClass(aSet: sClassFields);
begin
  m_ClassFields := aSet;
end;
//------------------------------------------------------------------------
function TClearCurve.XPosToLength(aX: Integer; aMin, aMax: Single): Single;
var
  xFWidth: Integer;
  xIndex, xRest: Integer;
  xPos: Single;
  xDelta: Single;
begin
  xFWidth := Width div cMaxKlassFieldX;
  xIndex := (aX div xFWidth)+1;
  xRest  := aX mod xFWidth;
  xDelta := cCurveValuesX[xIndex] - cCurveValuesX[xIndex+1];
  xPos   := (xDelta / xFWidth) * xRest;
  Result := cCurveValuesX[xIndex] - xPos;
  if Result < aMin then      Result := aMin
  else if Result > aMax then Result := aMax;
end;
//------------------------------------------------------------------------
function TClearCurve.YPosToDiameter(aY: Integer; aMin, aMax: Single): Single;
var
  xFHeight: Integer;
  xIndex, xRest: Integer;
  xPos: Single;
  xDelta: Single;
begin
  xFHeight := Height div cMaxKlassFieldY;
  xIndex := (aY div xFHeight)+1;
  xRest  := aY mod xFHeight;
  xDelta := cCurveValuesY[xIndex] - cCurveValuesY[xIndex+1];
  xPos   := (xDelta / xFHeight) * xRest;
  Result := cCurveValuesY[xIndex] - xPos;
  if Result < aMin then      Result := aMin
  else if Result > aMax then Result := aMax;
end;
//------------------------------------------------------------------------
//------------------------------------------------------------------------

end.

