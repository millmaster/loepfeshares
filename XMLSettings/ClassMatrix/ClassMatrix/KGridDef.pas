unit KGridDef;

interface

uses
  Classes, Graphics, SysUtils;
//************************************************************************
type
  // defenition of constant array for Field location
  rFieldPos = record
    tPos,
    lPos,
    size: Byte;
  end;
  // data type for interpolated values
  rNeighbors = record
    values: Array[0..3] of Single;
    calc: Array[1..9] of Single;
  end;

  // set of numbers from 1 to 128 for working with Group
  sClassFields = set of 1..128;

  // some enumerations and set's to define the behavior of the Fields
  eFlowColor  = (fcRed, fcGreen, fcBlue);
  eGridMode   = (gmNormal, gmZE, gmUser);
  eValueMode  = (vmNone, vmValue, vmSummary, vmDots, vmColor);
  eDataMode   = (dmAbsolute, dm100km, dm1000km, dm100ky, dm1000ky);
  eEditMode   = (emNone, emGroup, emCurve, emClass);
  eBorderMode = (bmNormal, bmBold);
  eUpdateState = (usBorder, usValue, usColor, usDots);
  sBorderMode = set of eBorderMode;
  sUpdateState = set of eUpdateState;


  // Types for ClearerCurve
  eClearCurve = (ccNone, ccNep, ccShort, ccLong, ccThin);
  sClearCurve = set of eClearCurve;
  eCurveType  = (ctClearer, ctSplice, ctCluster);
  sCurveType  = set of eCurveType;
  eSpliceMode = (smValue, smLL, smExt);

  // Parameter record for the definition of the courves
  rClearerParameter = record
    EnabledCurve: sClearCurve;
    Diam: Array[ccNep..ccThin] of Single;
    Long: Array[ccNep..ccThin] of Single;
    LowCurve: Single;         // defines ClusterCurve, not used for SpliceCurve
    SpliceMode: eSpliceMode;  // defines the behavior of SpliceParameters
    SpliceValue: Single;      // needed in smValue and smLL mode
    CheckLength: Single;
  end;

//************************************************************************
const
  // defenitions of the borders
  cBLeft   = 1;
  cBBottom = 2;
  cBRight  = 4;
  cBTop    = 8;
  cBNoChange = $FF;

  // defenitions for the KlassierGrid
  cFieldCount = 128;
  cMaxKlassFieldX = 13;
  cMaxKlassFieldY = 17;
  cVirtualKlassFields = cMaxKlassFieldX * cMaxKlassFieldY;
  cDefaultClassFields: sClassFields = [14..16,21..24,29..32,36..40,44..48,49..64,96,103..104,110..112,118..120,126..128,97,105,113,121];

  // defenitions for the DescriptionPanels
  cGridValueX: Array[1..cMaxKlassFieldX] of Single =
    (0.0, 0.5, 1.0,1.5,2.0,3.0,4.0,6.0,8.0,12.0,20.0,32.0,70.0);
  cGridValueY: Array[1..cMaxKlassFieldY] of Single =
    (7.00,5.00,3.90,3.20,2.70,2.30,2.10,1.80,1.60,1.45,1.30,1.20,-1.0,0.83,0.80,0.75,0.65);


  // almost the same defenitions for ClearerCurve calculations
  cCurveValuesX: Array[1..cMaxKlassFieldX+1] of Single =
    (0.0, 0.5, 1.0,1.5,2.0,3.0,4.0,6.0,8.0,12.0,20.0,32.0,70.0, 200.0);
  cCurveValuesY: Array[1..cMaxKlassFieldY+1] of Single =
    (11.0,7.00,5.00,3.90,3.20,2.70,2.30,2.10,1.80,1.60,1.45,1.30,1.20,{1.0,}0.83,0.80,0.75,0.65,0.550);

  cCurvePointsPerField = 10;
  cMaxCurvePoints = (cCurvePointsPerField * cMaxKlassFieldX);
  cFs = 0.92;  cKs = 3.5;  cGs = 0.4;
  cFl = 0.98;  cKl = 3.0;  cGl = 0.3;
  cFt = 1.02;  cKt = 1.7;
  cBAS = 1.0;

  cFieldPos: array[1..cFieldCount] of rFieldPos = (
{1} (tPos:  7; lPos:  0; size: 1), (tPos:  6; lPos:  0; size: 1), (tPos:  5; lPos:  0; size: 1), (tPos:  4; lPos:  0; size: 1),
    (tPos:  3; lPos:  0; size: 1), (tPos:  2; lPos:  0; size: 1), (tPos:  1; lPos:  0; size: 1), (tPos:  0; lPos:  0; size: 1),
{2} (tPos:  7; lPos:  1; size: 1), (tPos:  6; lPos:  1; size: 1), (tPos:  5; lPos:  1; size: 1), (tPos:  4; lPos:  1; size: 1),
    (tPos:  3; lPos:  1; size: 1), (tPos:  2; lPos:  1; size: 1), (tPos:  1; lPos:  1; size: 1), (tPos:  0; lPos:  1; size: 1),
{3} (tPos:  7; lPos:  2; size: 1), (tPos:  6; lPos:  2; size: 1), (tPos:  5; lPos:  2; size: 1), (tPos:  4; lPos:  2; size: 1),
    (tPos:  3; lPos:  2; size: 1), (tPos:  2; lPos:  2; size: 1), (tPos:  1; lPos:  2; size: 1), (tPos:  0; lPos:  2; size: 1),
{4} (tPos:  7; lPos:  3; size: 1), (tPos:  6; lPos:  3; size: 1), (tPos:  5; lPos:  3; size: 1), (tPos:  4; lPos:  3; size: 1),
    (tPos:  3; lPos:  3; size: 1), (tPos:  2; lPos:  3; size: 1), (tPos:  1; lPos:  3; size: 1), (tPos:  0; lPos:  3; size: 1),
{5} (tPos:  7; lPos:  4; size: 1), (tPos:  6; lPos:  4; size: 1), (tPos:  5; lPos:  4; size: 1), (tPos:  4; lPos:  4; size: 1),
    (tPos:  3; lPos:  4; size: 1), (tPos:  2; lPos:  4; size: 1), (tPos:  1; lPos:  4; size: 1), (tPos:  0; lPos:  4; size: 1),
{6} (tPos:  7; lPos:  5; size: 1), (tPos:  6; lPos:  5; size: 1), (tPos:  5; lPos:  5; size: 1), (tPos:  4; lPos:  5; size: 1),
    (tPos:  3; lPos:  5; size: 1), (tPos:  2; lPos:  5; size: 1), (tPos:  1; lPos:  5; size: 1), (tPos:  0; lPos:  5; size: 1),
{7} (tPos:  7; lPos:  6; size: 2), (tPos:  6; lPos:  6; size: 2), (tPos:  5; lPos:  6; size: 2), (tPos:  4; lPos:  6; size: 2),
    (tPos:  3; lPos:  6; size: 2), (tPos:  2; lPos:  6; size: 2), (tPos:  1; lPos:  6; size: 2), (tPos:  0; lPos:  6; size: 2),
{8} (tPos:  7; lPos:  8; size: 5), (tPos:  6; lPos:  8; size: 5), (tPos:  5; lPos:  8; size: 5), (tPos:  4; lPos:  8; size: 5),
    (tPos:  3; lPos:  8; size: 5), (tPos:  2; lPos:  8; size: 5), (tPos:  1; lPos:  8; size: 5), (tPos:  0; lPos:  8; size: 5),
{9} (tPos: 16; lPos:  4; size: 2), (tPos: 15; lPos:  4; size: 2), (tPos: 14; lPos:  4; size: 2), (tPos: 13; lPos:  4; size: 2),
    (tPos: 11; lPos:  4; size: 2), (tPos: 10; lPos:  4; size: 2), (tPos:  9; lPos:  4; size: 2), (tPos:  8; lPos:  4; size: 2),
{10}(tPos: 16; lPos:  6; size: 1), (tPos: 15; lPos:  6; size: 1), (tPos: 14; lPos:  6; size: 1), (tPos: 13; lPos:  6; size: 1),
    (tPos: 11; lPos:  6; size: 1), (tPos: 10; lPos:  6; size: 1), (tPos:  9; lPos:  6; size: 1), (tPos:  8; lPos:  6; size: 1),
{11}(tPos: 16; lPos:  7; size: 1), (tPos: 15; lPos:  7; size: 1), (tPos: 14; lPos:  7; size: 1), (tPos: 13; lPos:  7; size: 1),
    (tPos: 11; lPos:  7; size: 1), (tPos: 10; lPos:  7; size: 1), (tPos:  9; lPos:  7; size: 1), (tPos:  8; lPos:  7; size: 1),
{12}(tPos: 16; lPos:  8; size: 1), (tPos: 15; lPos:  8; size: 1), (tPos: 14; lPos:  8; size: 1), (tPos: 13; lPos:  8; size: 1),
    (tPos: 11; lPos:  8; size: 1), (tPos: 10; lPos:  8; size: 1), (tPos:  9; lPos:  8; size: 1), (tPos:  8; lPos:  8; size: 1),
{13}(tPos: 16; lPos:  9; size: 1), (tPos: 15; lPos:  9; size: 1), (tPos: 14; lPos:  9; size: 1), (tPos: 13; lPos:  9; size: 1),
    (tPos: 11; lPos:  9; size: 1), (tPos: 10; lPos:  9; size: 1), (tPos:  9; lPos:  9; size: 1), (tPos:  8; lPos:  9; size: 1),
{14}(tPos: 16; lPos: 10; size: 1), (tPos: 15; lPos: 10; size: 1), (tPos: 14; lPos: 10; size: 1), (tPos: 13; lPos: 10; size: 1),
    (tPos: 11; lPos: 10; size: 1), (tPos: 10; lPos: 10; size: 1), (tPos:  9; lPos: 10; size: 1), (tPos:  8; lPos: 10; size: 1),
{15}(tPos: 16; lPos: 11; size: 1), (tPos: 15; lPos: 11; size: 1), (tPos: 14; lPos: 11; size: 1), (tPos: 13; lPos: 11; size: 1),
    (tPos: 11; lPos: 11; size: 1), (tPos: 10; lPos: 11; size: 1), (tPos:  9; lPos: 11; size: 1), (tPos:  8; lPos: 11; size: 1),
{16}(tPos: 16; lPos: 12; size: 1), (tPos: 15; lPos: 12; size: 1), (tPos: 14; lPos: 12; size: 1), (tPos: 13; lPos: 12; size: 1),
    (tPos: 11; lPos: 12; size: 1), (tPos: 10; lPos: 12; size: 1), (tPos:  9; lPos: 12; size: 1), (tPos:  8; lPos: 12; size: 1)
  );

  cValues: Array[1..2,1..cFieldCount] of Single = (
   (2.5e6,1.5e6,1.3e6,399.9e3,121.2e3,33.7e3,6882,393,          113.4e3,34.7e3,30.6e3,10.9e3,3963,0,0,0,
    10.5e3,7366,9161,4022,0,0,0,0,                              2068,2581,4306,785,0,0,0,0,
    669,1156,1020,0,0,0,0,0,                                    92,187,118,0,0,0,0,0,
    0,0,0,0,0,0,0,0,                                            0,0,0,0,0,0,0,0,
    854.4e3,18.9e6,81.1e6,268.2e6,137.9e6,17.1e6,314.9e3,3755,  14.1e3,1.2e6,9.2e6,15.6e6,2.3e6,311.0e3,360,1,
    2297,360.7e3,3.3e6,4.4e6,404.5e3,54.0e3,32,0,               526,138.5e3,1.6e6,1.6e6,87.4e3,12.8e3,27,0,
    0,12.8e3,201.7e3,155.4e3,3533,990,0,0,                      0,345,2689,1279,197,0,0,0,
    0,146,181,97,87,0,0,0,                                      0,22,26,12,3,0,0,0),

   (0,1,0,0,0,158,1505,915,                                     1,1,3,2,159,1251,345,15,
    2,0,1,249,1610,764,133,1,                                   1,2,2,1418,1066,453,81,1,
    1,1,583,1656,925,501,65,1,                                  0,2,339,416,327,194,31,0,
    30,89,210,184,188,160,35,0,                                 10,12,46,100,76,49,19,0,
    0,0,0,0,1,3,3,0,                                            0,0,0,0,1,8,7,1,
    0,0,0,0,0,4,9,0,                                            0,0,0,0,0,13,6,1,
    61,0,0,0,2,27,21,0,                                         24,0,0,0,2,242,15,3,
    18,0,0,0,4,296,29,4,                                        9,0,0,0,8,6,1,0)
  );

{
   (1135,490,345,84.6,24.5,7.43,1.36,0.11, 53.9,17.9,15.5,5.45,0,0,0,0,
    7.05,4.83,5.13,0.8,0,0,0,0,            1.79,1.8,1.95,0,0,0,0,0,
    0.59,0.93,0,0,0,0,0,0,                 0.08,0.1,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,                       0,0,0,0,0,0,0,0,
    131,8077,62.4e3,315.2e3,102.2e3,7522,93.9,1.4, 0,312,5304,16.9e3,845,55,0.07,0,
    0, 72.7,1676,4555,97,5.51,0,0,         0,25,786,1778,14.9,0.96,0,0,
    0,3.14,123,202,0.68,0.2,0,0,           0,0.15,4.25,3.63,0.07,0,0,0,
    0,0.02,0.12,0.02,0.02,0,0,0,           0,0,0,0.010,0,0,0,0),

   (0,0,0,0,0,0.25,1.43,0.82,              0,0,0,0,1.7,0.87,0.39,0.02,
    0,0,0,1.09,0.8,0.45,0.11,0,            0,0,0.59,1.11,0.5,0.27,0.06,0,
    0,0,1.53,0.9,0.49,0.27,0.06,0,         0,0.05,0.29,0.26,0.18,0.13,0.03,0,
    0.06,0.13,0.21,0.13,0.14,0.14,0.05,0,  0,0.03,0.12,0.17,0.08,0.07,0.04,0,
    0,0,0,0,0,0,0,0,                       1.12,0,0,0,0,0,0,0,
    0.18,0,0,0,0,0,0,0,                    0.08,0,0,0,0,0,0,0,
    0.02,0,0,0,0,0.01,0,0,                 0.01,0,0,0,0,0.14,0,0,
    0.01,0,0,0,0.01,0.2,0,0,               0,0,0,0,0,0,0,0)
  );
}
  cValuesTest: Array[1..2,1..cFieldCount] of Single = (
   (1000,500,100,20,24.5,7.43,1.36,0.11,   600,400,50,10,0,0,0,0,
    300,100,30,5,0,0,0,0,                  150,100,20,10,0,0,0,0,
    0.59,0.93,0,0,0,0,0,0,                 0.08,0.1,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,                       0,0,0,0,0,0,0,0,
    131,8077,62.4e3,315.2e3,102.2e3,7522,93.9,1.4, 0,312,5304,16.9e3,845,55,0.07,0,
    0, 72.7,1676,4555,97,5.51,0,0,         0,25,786,1778,14.9,0.96,0,0,
    0,3.14,123,202,0.68,0.2,0,0,           0,0.15,4.25,3.63,0.07,0,0,0,
    0,0.02,0.12,0.02,0.02,0,0,0,           0,0,0,0.010,0,0,0,0),

   (0,0,0,0,0,0.25,1.43,0.82,              0,0,0,0,1.7,0.87,0.39,0.02,
    0,0,0,1.09,0.8,0.45,0.11,0,            0,0,0.59,1.11,0.5,0.27,0.06,0,
    0,0,1.53,0.9,0.49,0.27,0.06,0,         0,0.05,0.29,0.26,0.18,0.13,0.03,0,
    0.06,0.13,0.21,0.13,0.14,0.14,0.05,0,  0,0.03,0.12,0.17,0.08,0.07,0.04,0,
    0,0,0,0,0,0,0,0,                       1.12,0,0,0,0,0,0,0,
    0.18,0,0,0,0,0,0,0,                    0.08,0,0,0,0,0,0,0,
    0.02,0,0,0,0,0.01,0,0,                 0.01,0,0,0,0,0.14,0,0,
    0.01,0,0,0,0.01,0.2,0,0,               0,0,0,0,0,0,0,0)
  );

  cBorderDef: Array[gmNormal..gmUser,1..cFieldCount] of Byte = (
{gmNormal}
  { 1}($03,$03,$03,$03,$03,$03,$03,$03, $03,$03,$03,$03,$03,$03,$03,$03,
  { 3} $03,$03,$03,$03,$03,$03,$03,$03, $03,$03,$03,$03,$03,$03,$03,$03,
  { 5} $03,$03,$03,$03,$03,$03,$03,$03, $03,$03,$03,$03,$03,$03,$03,$03,
  { 7} $03,$03,$03,$03,$03,$03,$03,$03, $03,$03,$03,$03,$03,$03,$03,$03,
  { 9} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$03,$03,$03,
  {11} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$03,$03,$03,
  {13} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$03,$03,$03,
  {15} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$03,$03,$03),
{gmZE}
  { 1}($03,$03,$03,$03,$03,$03,$03,$03, $33,$13,$33,$13,$33,$13,$33,$13,
  { 3} $33,$13,$33,$13,$33,$13,$33,$13, $23,$03,$23,$03,$23,$03,$23,$03,
  { 5} $33,$13,$33,$13,$33,$13,$33,$13, $23,$03,$23,$03,$23,$03,$23,$03,
  { 7} $33,$13,$33,$13,$33,$13,$33,$13, $33,$13,$13,$13,$13,$13,$13,$13,
  { 9} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$03,$03,$03,
  {11} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$33,$13,$13,
  {13} $11,$33,$13,$2B,$03,$23,$03,$03, $01,$23,$03,$2B,$03,$23,$03,$03,
  {15} $11,$33,$13,$2B,$03,$33,$13,$13, $01,$23,$03,$2B,$03,$23,$03,$03),
{gmUser}
  { 1}($03,$03,$03,$03,$03,$03,$03,$03, $03,$03,$03,$03,$03,$03,$03,$03,
  { 3} $03,$03,$03,$03,$03,$03,$03,$03, $03,$03,$03,$03,$03,$03,$03,$03,
  { 5} $03,$03,$03,$03,$03,$03,$03,$03, $03,$03,$03,$03,$03,$03,$03,$03,
  { 7} $03,$03,$03,$03,$03,$03,$03,$03, $03,$03,$03,$03,$03,$03,$03,$03,
  { 9} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$03,$03,$03,
  {11} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$03,$03,$03,
  {13} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$03,$03,$03,
  {15} $01,$03,$03,$0B,$03,$03,$03,$03, $01,$03,$03,$0B,$03,$03,$03,$03)
  );


implementation
//------------------------------------------------------------------------
end.
