unit KlassField;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, KGridDef;

type
  PKlassField = ^TKlassField;
  TKlassField = class(TPaintBox)
  private
    FBackColor: TColor;
    FBorder: Byte;
    FBorderMode: sBorderMode;
    FCut: Single;
    FCutColor: TColor;
    FDotCount: Integer;
    FGridColor: TColor;
    FHighLight: Boolean;
    FSelected: Boolean;
    FSelectedColor: TColor;
    FUnCut: Single;
    FUnCutColor: TColor;
    FValueMode: eValueMode;
    m_FieldWidth:  Integer;
    m_LeftPos:   Integer;
    m_TopPos:    Integer;
    m_WidthFactor: Integer;
    procedure DrawDots(aValue: Integer; aColor: TColor);
    procedure DrawGrid(aGrid: Byte);
    procedure DrawLine(aGrid: Byte; aAlign: TAlign);
    procedure DrawValue(aValue: Double; aAlign: TAlign; aColor: TColor);
    procedure SetHighLight(aValue: Boolean);
    procedure SetSelected(aValue: Boolean);
    procedure SetValue(Index: Integer; Value: Single);
  protected
    procedure MakeLine(aAlign: TAlign; aBold: Boolean);
  public
    // public properties
    property BackColor: TColor read FBackColor write FBackColor;
    property Border: Byte read FBorder write FBorder;
    property BorderMode: sBorderMode read FBorderMode write FBorderMode;
    property Cut: Single index 1 read FCut write SetValue;
    property CutColor: TColor read FCutColor write FCutColor default clRed;
    property DotCount: Integer write FDotCount;
    property GridColor: TColor Write FGridColor;
    property HighLight: Boolean read FHighLight write SetHighLight;
    property Selected: Boolean read FSelected write SetSelected;
    property SelectedColor: TColor read FSelectedColor write FSelectedColor;
    property UnCut: Single index 0 read FUnCut write SetValue;
    property UnCutColor: TColor read FUnCutColor write FUnCutColor default clBlack;
    property ValueMode: eValueMode read FValueMode write FValueMode;
    // public methodes
    constructor Create(AOwner: TComponent; aTop, aLeft, aFactor: Integer; aIndex: Byte);
    procedure Paint; override;
    procedure SetSize(aHeight, aWidth: Integer);
  end;
//------------------------------------------------------------------------
implementation

constructor TKlassField.Create(AOwner: TComponent; aTop, aLeft, aFactor: Integer; aIndex: Byte);
begin
  inherited Create(AOwner);
  Enabled     := False;
  Font.Name := 'Sans Serif';
  ParentColor := True;
  ShowHint    := True;
  Tag         := aIndex;

  FBackColor  := clGreen;
  FBorderMode := [bmNormal, bmBold];
  FCut        := 0;
  FCutColor   := clRed;
  FDotCount   := 1;
  FHighLight  := False;
  FSelected   := False;
  FSelectedColor := clAqua;
  FUnCut      := 0;
  FUnCutColor := clBlack;
  FValueMode  := vmValue;

  m_FieldWidth   := 0;
  m_LeftPos    := aLeft;
  m_TopPos     := aTop;
  m_WidthFactor := aFactor;
end;
//------------------------------------------------------------------------
procedure TKlassField.DrawDots(aValue: Integer; aColor: TColor);
var
  i,x,y: Integer;
begin
  with Canvas do begin
    for i:=1 to aValue do begin
      x := Random(Width-1);
      y := Random(Height-1);
      Pixels[x,y] := aColor;
    end;
  end;
end;
//------------------------------------------------------------------------
procedure TKlassField.DrawGrid(aGrid: Byte);
var
  xGrid: Byte;
begin
  xGrid := 0;
  // define bit mask for border drawing
  if bmBold in FBorderMode then
    if (aGrid and $F0) > 0 then  // if bmBold selected but no BoldBorder there
      xGrid := aGrid and $F0
    else
      xGrid := aGrid;           // draw NormalBorder instead

  if bmNormal in FBorderMode then
    xGrid := xGrid or (aGrid and $0F);

  with Canvas do begin
    Pen.Color := FGridColor;
    DrawLine(xGrid, alLeft);
    DrawLine(xGrid shr 1, alBottom);
    DrawLine(xGrid shr 2, alRight);
    DrawLine(xGrid shr 3, alTop);
  end;
end;
//------------------------------------------------------------------------
procedure TKlassField.DrawLine(aGrid: Byte; aAlign: TAlign);
begin
 with Canvas do
   if Boolean(aGrid and $11) then
     case aAlign of
       alTop:    MakeLine(aAlign, False);  // only single line for bold effect
       alLeft:   MakeLine(aAlign, Boolean(aGrid and $10));
       alBottom: MakeLine(aAlign, Boolean(aGrid and $10));
       alRight:  MakeLine(aAlign, False);  // only single line for bold effect
     end;
end;
//------------------------------------------------------------------------
procedure TKlassField.DrawValue(aValue: Double; aAlign: TAlign; aColor: TColor);
var
  s: string;
  x,y: Integer;
begin
  ParentColor := True;
  if aValue < 10 then s := Format('%3.2g', [aValue])
  else               s := Format('%5.5g', [aValue]);
  with Canvas do begin
    Font.Height := (Height div 2);
    Font.Color := aColor;
    if aAlign = alRight then begin
      x := m_FieldWidth - TextWidth(s) - 3;
      y := 0;
    end else begin
      x := 2;
      y := Height - TextHeight(s) - 1;
    end;
    TextOut(x, y, s);
  end;
end;
//------------------------------------------------------------------------
procedure TKlassField.MakeLine(aAlign: TAlign; aBold: Boolean);
var
  xy: Integer;
begin
  with Canvas do begin
    if aBold and (aAlign in [alLeft, alBottom]) then
      Pen.Width := 2
    else
      Pen.Width := 1;

    case aAlign of
      alTop, alLeft:   xy := 0+Pen.Width-1;
      alBottom:        xy := Height-1;
    else // alRight
      xy := Width -1;
    end;

    if aAlign in [alTop, alBottom] then begin
      MoveTo(0, xy);
      LineTo(Width, xy);
    end else begin
      MoveTo(xy, 0);
      LineTo(xy, Height);
    end;
  end;
end;
//------------------------------------------------------------------------
procedure TKlassField.Paint;
begin
  inherited Paint;
  // defines the background color
  if FHighLight then
    Canvas.Brush.Color := clInactiveCaption
  else if FSelected then
    Canvas.Brush.Color := FSelectedColor
  else
    Canvas.Brush.Color := Color;

  // paints the rectangle if needed
  if FHighLight or FSelected then begin
    Canvas.Pen.Color := Canvas.Brush.Color;
    Canvas.Rectangle(0, 0, Width, Height);
  end;

  case FValueMode of
    // shows the values as text
    vmValue, vmSummary: begin
        if FUnCut > 0 then DrawValue(FUnCut, alLeft, FUnCutColor);
        if FCut > 0 then
          DrawValue(FCut, alRight, FCutColor);
      end;
    // shows the Value1 as Dots, Value2 as Text
    vmDots: begin
        DrawDots(m_WidthFactor*FDotCount, FUnCutColor);
        if FCut > 0 then DrawValue(FCut, alRight, FCutColor);
      end;
    // shows the Value1 as Color, Value2 as Text
    vmColor: begin
        if (FUnCut > 0) and not FHighLight then begin
          Canvas.Brush.Color := FBackColor;
          Canvas.Pen.Color   := Canvas.Brush.Color;
          Canvas.Rectangle(0, 0, Width+1, Height+1);
        end;
        Canvas.Brush.Color := Color;
        if FCut > 0 then DrawValue(FCut, alRight, FCutColor);
      end;
  end;

  // draws the border if necessary
  if (FBorderMode * [bmNormal,bmBold]) <> [] then
    DrawGrid(Border);
end;
//------------------------------------------------------------------------
procedure TKlassField.SetHighLight(aValue: Boolean);
begin
  FHighLight := aValue;
  Invalidate;
end;
//------------------------------------------------------------------------
procedure TKlassField.SetSelected(aValue: Boolean);
begin
  FSelected := aValue;
  Invalidate;
end;
//------------------------------------------------------------------------
procedure TKlassField.SetSize(aHeight, aWidth: Integer);
begin
  m_FieldWidth := aWidth;
  Left   := 1 + (m_LeftPos * m_FieldWidth);
  Top    := 1 + (m_TopPos * aHeight);
  Width  := m_WidthFactor * m_FieldWidth;
  Height := aHeight;
end;
//------------------------------------------------------------------------
procedure TKlassField.SetValue(Index: Integer; Value: Single);
begin
  if Index = 0 then FUnCut := Value
  else              FCut   := Value;
  Hint := Format('%.5g, %.5g', [FUnCut, FCut]);
end;
//------------------------------------------------------------------------
end.
