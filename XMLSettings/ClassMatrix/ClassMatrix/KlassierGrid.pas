unit KlassierGrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Consts, Math, KGridDef, KlassField, ClearCurve, PalettePanel,
  YarnUnits;

type
  // data type for holding values
  rValue = record
    MaxValue: Double;
    MinValue: Double;
    UnCut: Array[1..cFieldCount] of Single;
    Cut:   Array[1..cFieldCount] of Single;
  end;

  PrGroupData = ^rGroupData;
  rGroupData = record
    Sum1,
    Sum2: Single;
    SummaryField: SmallInt;
    FieldList: TList;
  end;

  rFieldData = record
    Border: Byte;
    BorderMode: sBorderMode;
    Index: Integer;
  end;

  TKlassierGrid = class(TPanel)
  private
    FClearParaGroup: TGroupBox;
    FSpliceParaGroup: TGroupBox;
    FDataMode: eDataMode;
    FDeleteGroup: Boolean;
    FEditMode: eEditMode;
    FFieldHeight: Integer;
    FFieldWidth: Integer;
    FFlowColor: TColor;
    FGridColor: TColor;
    FGridMode: eGridMode;
    FMaxDots: Integer;
    FShowClass: Boolean;
    FStepCount: Integer;
    FValueMode: eValueMode;
    m_ClassFields: sClassFields;
    m_ClearCurve: TClearCurve;
    m_CurrentGroupIndex: Integer;
    m_Field: Array[1..cFieldCount] of TKlassField;
    m_FieldData: Array[gmNormal..gmUser,1..cFieldCount] of rFieldData;
    m_FieldNotInGroup: sClassFields;
    m_GroupList: TList;
    m_PalettePanel: TPalettePanel;
    m_RT: Single;
    m_SpooledLength: Single;
    m_SummaryFields: sClassFields;
    m_Value: rValue;
    m_WT: Single;
    function GetFlowColor: eFlowColor;
    function GetGridFont: TFont;
    procedure SetColor(Index: Integer; Value: TColor);
    procedure SetDataMode(Value: eDataMode);
    procedure SetDeleteGroup(Value: Boolean);
    procedure SetEditMode(Value: eEditMode);
    procedure SetFlowColor(Value: eFlowColor);
    procedure SetGridFont(Value: TFont);
    procedure SetGridMode(Value: eGridMode);
    procedure SetMaxDots(Value: Integer);
    procedure SetShowClass(Value: Boolean);
    procedure SetStepCount(Value: Integer);
    procedure SetValueMode(Value: eValueMode);
  protected
    procedure BuildNewGroup;
    procedure DefineBorderMode;
    procedure DefineColorAndDotsValue;
    procedure DefineColors;
    procedure DefineValueProperties;
    procedure EvaluateGroupSummary(aGroup: PrGroupData);
    procedure HighLightGroup(GroupIndex: Integer; State: Boolean);
    procedure UpdateFieldProperties(aUpdate: sUpdateState);
  public
    property ClearParaGroup: TGroupBox read FClearParaGroup write FClearParaGroup;
    property SpliceParaGroup: TGroupBox read FSpliceParaGroup write FSpliceParaGroup;
    property DeleteGroup: Boolean read FDeleteGroup write SetDeleteGroup default False;
    property EditMode: eEditMode read FEditMode write SetEditMode default emNone;
    property GridColor: TColor index 0 read FGridColor write SetColor default clBlack;
    property GridFont: TFont read GetGridFont write SetGridFont;
    property GridMode: eGridMode read FGridMode write SetGridMode default gmZe;
    property FieldHeight: Integer read FFieldHeight;
    property FieldWidth: Integer read FFieldWidth;
    property FlowColor: eFlowColor read GetFlowColor write SetFlowColor default fcRed;
    property MaxDots: Integer read FMaxDots write SetMaxDots default 250;
    property ShowClass: Boolean read FShowClass write SetShowClass default False;
    property StepCount: Integer read FStepCount write SetStepCount default 5;
    property ValueMode: eValueMode read FValueMode write SetValueMode default vmValue;    property DataMode: eDataMode read FDataMode write SetDataMode default dmAbsolute;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BuildGroup;
    function CheckNeighbor(aNr: SmallInt; aAlign: TAlign): Boolean;
    procedure DeleteSelectedGroup;
    function GetClearerPara(Index: Integer): rClearerParameter;
    function GetCurveEdit: eClearCurve;
    function GetCurveType: sCurveType;
    procedure GetCutInfo(var Uncutted, Cutted, RefUc, RefC: Single);
    function GetValueColor(aIndex: Integer): TColor;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure Resize; override;
    procedure SelectNextGroup;
    procedure SetClearerPara(Index: Integer; Value: rClearerParameter);
    procedure SetCurveEdit(Value: eClearCurve);
    procedure SetCurveType(Value: sCurveType);
    procedure SetProductionData(aLength, aRT, aWT: Single);
    procedure SetValueArray(aArr: Array of Single; aNumber: Byte);
    procedure SetValueColor(aIndex: Integer; aColor: TColor);
    procedure Update; override;
  published
    { Published declarations }
  end;


//------------------------------------------------------------------------
implementation
//------------------------------------------------------------------------
//************************************************************************
constructor TKlassierGrid.Create(AOwner: TComponent);
var
  i: Integer;
  j: eGridMode;
begin
  inherited Create(AOwner);
  Align  := alClient;
  BevelInner := bvLowered;
  BevelOuter := bvNone;
  Enabled := True;
  ParentColor := True;

  FClearParaGroup := Nil;
  FDeleteGroup := False;
  FFieldHeight := 1;            // value changes in Resize
  FFieldWidth := 1;             // value changes in Resize
  FFlowColor := clRed;
  FGridColor := clBlack;
  FMaxDots   := 250;
  FSpliceParaGroup := Nil;
  FStepCount := 5;

  FGridMode := gmZE;
  FValueMode := vmValue;
  FDataMode  := dmAbsolute;
  FEditMode  := emNone;
  FShowClass := False;

  m_FieldNotInGroup := [1..128];
  m_SummaryFields := [];
  m_SpooledLength := 0;
  m_RT            := 0;
  m_WT            := 0;
  m_ClassFields := cDefaultClassFields;
  m_GroupList := TList.Create;
  // creates object with palette info
  m_PalettePanel := TPalettePanel.Create(Self);
  m_PalettePanel.Parent := Self;
  m_PalettePanel.Visible := False;
  for i:=1 to cFieldCount do begin
    // initialize FieldDataRecord
    for j:=gmNormal to gmUser do
      with m_FieldData[j][i] do begin
        Border := cBorderDef[j][i];
        BorderMode := [bmNormal, bmBold];
        Index := i;
      end;
    // clear Values
    FillChar(m_Value, sizeof(m_Value), 0);

    // Create TKlassField object ...
    m_Field[i] := TKlassField.Create(Self, cFieldPos[i].tPos, cFieldPos[i].lPos,
                                           cFieldPos[i].size, i);
    m_Field[i].Parent := Self;
  end;
  // creates object with Clearer Curves
  m_ClearCurve := TClearCurve.Create(Self);
  m_ClearCurve.Parent := Self;
  m_ClearCurve.SetFieldClass(m_ClassFields);

  // Set startup properties
  UpdateFieldProperties([usBorder]);
end;
//------------------------------------------------------------------------
destructor TKlassierGrid.Destroy;
begin
  m_GroupList.Free;
  m_ClearCurve.Free;
  m_PalettePanel.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------
function TKlassierGrid.CheckNeighbor(aNr: SmallInt; aAlign: TAlign): Boolean;
begin
  Result := False;
  case aAlign of
    alLeft:
      if (aNr in [1..8]) or (aNr in [65..72]) then
        Result := True
      else
        // check the Field on the left side (Nr - 8)
        Result := not m_Field[aNr-8].HighLight;
    alBottom:
      if aNr in [1,9,17,25,33,41,49,57,  69,77,85,93,101,109,117,125] then
        Result := True
      else if not (aNr in [65,73,81,89,97,105,113,121]) then
        // check the Field one below (Nr - 1)
        Result := not m_Field[aNr-1].HighLight;
    alRight:
      if aNR in [57..64, 121..128] then
        Result := False
      else
        // check the Field on the right side (Nr + 8)
        Result := not m_Field[aNr+8].HighLight;
    alTop:
        if aNr in [8,16,24,32,40,48,56,64] then
          Result := False
        else if aNr in [68,76,84,92,100,108,116,124] then
          Result := True
        else
          // check the Field on top (Nr + 1)
          Result := not m_Field[aNr+1].HighLight;
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.BuildGroup;
begin
    if GridMode = gmUser then
      BuildNewGroup
    else begin
      MessageBeep(MB_ICONEXCLAMATION);
      MessageDlg('You have to be in UserGrid mode!', mtInformation, [mbOK], 0);
    end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.BuildNewGroup;
var
  i: Integer;
  xIndex: Integer;
  xBBold: Byte;
  xPGroup: PrGroupData;
  xFirstFind: Boolean;
begin
  xFirstFind := True;
  xPGroup := Nil;
  // create new List and add the selected Fields to it
  for i:=1 to cFieldCount do
    if m_Field[i].HighLight then begin
      if xFirstFind then begin   // first found: create list
        xFirstFind := False;
        new(xPGroup);
        xPGroup.FieldList := TList.Create;
        xPGroup.SummaryField := i;
        m_GroupList.Add(xPGroup);         // add UserFieldList to GroupList
      end;
      xPGroup.FieldList.Add(@m_FieldData[gmUser, i]);
      Exclude(m_FieldNotInGroup, i);
    end;

  if xPGroup <> Nil then
    with xPGroup^ do begin
      // first Field in List shows the summary if SummaryMode is selected
      Include(m_SummaryFields, SummaryField);
      // checkout the position in the grid and define border
      with FieldList do begin                     // Count all FieldData in Group
        for i:=0 to Count-1 do begin
          xBBold := 0;
          xIndex := rFieldData(Items[i]^).Index;  // get Index of linked Field in the grid
          if CheckNeighbor(xIndex, alLeft) then
            inc(xBBold, cBLeft);
          if CheckNeighbor(xIndex, alBottom) then
            inc(xBBold, cBBottom);
          if CheckNeighbor(xIndex, alRight) then
            inc(xBBold, cBRight);
          if CheckNeighbor(xIndex, alTop) then
            inc(xBBold, cBTop);
          xBBold := xBBold shl 4;  // the higher 4 bit defines bold lines

          // replace previous bold definition with new
          rFieldData(Items[i]^).Border := (rFieldData(Items[i]^).Border and $0F) + xBBold;
          // Field without a BoldBorder shows no line
          if xBBold = 0 then
            rFieldData(Items[i]^).BorderMode := []
          else
            rFieldData(Items[i]^).BorderMode := [bmBold]
        end;
        // reset HighLight flag direct in Field
        for i:=0 to Count-1 do
          m_Field[rFieldData(Items[i]^).Index].HighLight := False;
      end;
      // evaluate the summary of the current group
      EvaluateGroupSummary(xPGroup);
      UpdateFieldProperties([usValue, usBorder]);
    end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.DefineBorderMode;
var
  i: Integer;
begin
  for i:=1 to cFieldCount do
    with m_Field[i] do begin
      Border := m_FieldData[FGridMode, i].Border;
      if (FGridMode = gmUser) and (FValueMode = vmSummary) then
        BorderMode := m_FieldData[FGridMode, i].BorderMode
      else
        BorderMode := [bmNormal, bmBold];
    end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.DefineColorAndDotsValue;
var
  i: Integer;
  xOffset,
  xDotOffset: Double;
  xStep,
  xDotStep: Double;
  xSingle: Single;

  function GetStepFactor(aValue: Double): Integer;
  var
    xI: Integer;
  begin
    Result := 0;
    if aValue > 0.0 then begin
      xI := 0;
      repeat
        inc(xI);
      until exp(xOffset + xI*xStep) >= aValue;
      if xI > FStepCount then Result := FStepCount
      else                    Result := xI;
    end;
  end;

  function GetDotCount(aFactor:Integer): Integer;
  begin
    if aFactor > 0 then
      Result := Round(exp(xDotOffset + aFactor*xDotStep))
    else
      Result := 0;
  end;

  function GetColorValue(aFactor: Integer): DWord;
  var
    xMiddle: Single;
    xC1, xC2, xC3: Byte;
  begin
    xMiddle := (FStepCount +1) /2;
    if aFactor = 0 then begin                  // White:               no value
      xC1 := 255; xC2 := 255; xC3 := 255;
    end else if aFactor <= xMiddle then begin  // Light -> BaseColor:  low value
      xC1 := 240 - Round((240 / Int(xMiddle)) * (aFactor-1));
      xC2 := xC1; xC3 := 254;
    end else begin                             // BaseColor -> Dark:    middle value
      xC1 := 0; xC2 := 0;
      xC3 := Round((254 / Int(xMiddle)) * (FStepCount-aFactor));
    end;
                                              // else 0,0,0 for Black: high value
    case FFlowColor of
      clRed:   Result := RGB(xC3, xC1, xC2);
      clGreen: Result := RGB(xC1, xC3, xC2);
      clBlue:  Result := RGB(xC1, xC2, xC3);
    else
      Result := RGB(255,255,255);  // else White
    end;
  end;

begin
  if (m_Value.MinValue > 0) and (m_Value.MaxValue > 0) then begin
    xOffset := ln(m_Value.MinValue);
    xStep   := (ln(m_Value.MaxValue) - xOffset) / FStepCount;
    xDotOffset := ln(FMaxDots/100);
    xDotStep   := (ln(FMaxDots) - xDotOffset) / FStepCount;

    for i:=1 to cFieldCount do begin
      with m_Field[i] do begin
        DotCount  := GetDotCount(GetStepFactor(m_Value.UnCut[i]));
        BackColor := GetColorValue(GetStepFactor(m_Value.UnCut[i]));
      end;
    end;

    // Set properties in the Palette object
    for i:=1 to FStepCount do begin
      xSingle := exp(xOffset + i*xStep);
      m_PalettePanel.SetValue(i, vmDots, GetDotCount(i), xSingle);
      m_PalettePanel.SetValue(i, vmColor, GetColorValue(i), xSingle);
    end;
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.DefineColors;
var
  i: Integer;
begin
  // First, set the normal value in Field
  for i:=1 to cFieldCount do
    with m_Field[i] do begin
      GridColor  := FGridColor;
    end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.DefineValueProperties;
var
  i,j: Integer;

  function Convert(aValue: Single): Single;
  begin
    if m_SpooledLength > 0 then begin
      case FDataMode of
        dm100km:  Result := (100000 * aValue)  / m_SpooledLength;
        dm1000km: Result := (1000000 * aValue) / m_SpooledLength;
        dm100ky:  Result := (100000 * aValue)  / (m_SpooledLength * cMToYd);
        dm1000ky: Result := (1000000 * aValue) / (m_SpooledLength * cMToYd);
      else  // dmAbsolute
        Result := aValue;
      end
    end else
      Result := aValue;
  end;

begin
  // First, set the normal values in Field
  for i:=1 to cFieldCount do
    with m_Field[i] do begin
      // set the Value properties
      if (FGridMode = gmZE) or (FValueMode <> vmSummary) or
         (i in m_FieldNotInGroup) then begin
        ValueMode := FValueMode;
        UnCut     := Convert(m_Value.UnCut[i]);
        Cut       := Convert(m_Value.Cut[i]);
      end else begin  // (FValueMode = vmSummary) and (FGridMode = gmUser)
        // Field is in a Group, Value will be set below
        if (i in m_FieldNotInGroup) then
          ValueMode := vmValue
        else if (i in m_SummaryFields) then
          ValueMode := vmSummary
        else
          ValueMode := vmNone;
      end;
    end;

  // if ValueMode is summary and there are some Groups, set Value-Property with Sum
  if (FValueMode = vmSummary) and (FGridMode = gmUser) and (m_GroupList.Count > 0) then
    for i:=0 to m_GroupList.Count-1 do                 // count through all Groups
      with rGroupData(m_GroupList.Items[i]^) do        // count through all Items for FieldData
        // every Field in Group get the Sum for showing in Hint
        for j:=0 to FieldList.Count-1 do begin
          m_Field[rFieldData(FieldList.Items[j]^).Index].UnCut := Convert(Sum1);
          m_Field[rFieldData(FieldList.Items[j]^).Index].Cut   := Convert(Sum2);
        end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.DeleteSelectedGroup;
begin
  if (FEditMode = emGroup) and FDeleteGroup then begin
    if m_CurrentGroupIndex >= 0 then begin
      // no more highlighted
      HighLightGroup(m_CurrentGroupIndex, False);
      with m_GroupList do begin
        // in selected GroupList ...
        with rGroupData(Items[m_CurrentGroupIndex]^) do begin
          // ... variable FieldList ...
          with FieldList do begin
            // ... count all Items and remove entry
            while Count > 0 do begin
              with rFieldData(Items[0]^) do begin
                Border := Border and $0F;  // delete Bold leave Normal Border
                BorderMode := [bmNormal];
                // add entry in set
                Include(m_FieldNotInGroup, Index);
              end;
              // delete link to m_FieldList[]
              Delete(0);
            end;
          end;
          // remove Index from SummarySet
          Exclude(m_SummaryFields, SummaryField);
          FieldList.Free;
        end;
        Delete(m_CurrentGroupIndex);      // delete Group entry from GroupList
        Pack;                             // removes the NIL-hole in List
        if m_CurrentGroupIndex >= Count then
          m_CurrentGroupIndex := Count-1;
      end;
      UpdateFieldProperties([usValue, usBorder]);;
    end;
    // HighLight next Group
    HighLightGroup(m_CurrentGroupIndex, True);
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.EvaluateGroupSummary(aGroup: PrGroupData);
var
  i,j: Integer;
begin
  // build summary from all values for selected group
  with aGroup^ do begin
    Sum1 := 0; Sum2 := 0;
    with FieldList do
      for i:=0 to Count-1 do begin         // count all Fields in Group
        j := rFieldData(Items[i]^).Index;  // access with Index member of FieldData
        Sum1 := Sum1 + m_Value.UnCut[j];
        Sum2 := Sum2 + m_Value.Cut[j];
      end;
  end;
end;
//------------------------------------------------------------------------
function TKlassierGrid.GetClearerPara(Index: Integer): rClearerParameter;
begin
  case Index of
    0: Result := m_ClearCurve.ClearerPara;
    1: Result := m_ClearCurve.SplicePara;
  end;
end;
//------------------------------------------------------------------------
function TKlassierGrid.GetCurveEdit: eClearCurve;
begin
  Result := m_ClearCurve.CurveEdit;
end;
//------------------------------------------------------------------------
function TKlassierGrid.GetCurveType: sCurveType;
begin
  Result := m_ClearCurve.CurveType;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.GetCutInfo(var Uncutted, Cutted, RefUc, RefC: Single);
begin
  m_ClearCurve.GetCutInfo(Uncutted, Cutted, RefUc, RefC);
end;
//------------------------------------------------------------------------
function TKlassierGrid.GetFlowColor: eFlowColor;
begin
  case FFlowColor of
    clGreen: Result := fcGreen;
    clBlue:  Result := fcBlue;
  else  // clRed
    Result := fcRed;
  end;
end;
//------------------------------------------------------------------------
function TKlassierGrid.GetGridFont: TFont;
begin
  Result := Font;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.HighLightGroup(GroupIndex: Integer; State: Boolean);
var
  i: Integer;
begin
  with m_GroupList do
    if (GroupIndex >= 0) and (GroupIndex < Count) then
      with rGroupData(Items[GroupIndex]^).FieldList do
        for i:=0 to Count-1 do
          m_Field[rFieldData(Items[i]^).Index].HighLight := State;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xPoint: TPoint;
  xField: TKlassField;
  xX, xY: Integer;
  xP: TControl;
begin
  inherited MouseDown(Button, Shift, X, Y);
  if (Button = mbLeft) then
    if FEditMode in [emGroup, emClass] then begin
      xPoint.x := X; xPoint.y := Y;
      xP := ControlAtPos(xPoint, True);
      if (xP <> Nil) then begin
        xField := xP as TKlassField;
        case FEditMode of
          emGroup: begin
              if (xField.Tag in m_FieldNotInGroup) then
                xField.HighLight := not xField.HighLight;
            end;
          emClass: begin
              if xField.Tag in m_ClassFields then
                Exclude(m_ClassFields, xField.Tag)
              else
                Include(m_ClassFields, xField.Tag);
              xField.Selected := not xField.Selected;
              xField.Invalidate;
              // Update FieldClassSet in ClearCurve for evaluate cutted/uncutted
              m_ClearCurve.SetFieldClass(m_ClassFields);
            end;
          emCurve:; // handled in ClearCurve-Class
        end;
      end;
    end else begin
{
      xX := 20; xY := (Height div 2) + 10;
      with Canvas do
        TextOut(xX,xY, Format('Groups: %d',[m_GroupList.Count]));
}
    end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.Paint;
begin
  inherited Paint;
  m_PalettePanel.Refresh;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.Resize;
var
  xInnerWidth,
  xInnerHeight: Integer;
  xPaletteHeight: Integer;
  i: Integer;
begin
  inherited Resize;
  xInnerHeight := Height - 4;   // without border width
  xInnerWidth := Width - 4;     // without border width
  FFieldHeight := xInnerHeight div cMaxKlassFieldY;
  FFieldWidth  := xInnerWidth div cMaxKlassFieldX;

  // define the size of each Field
  for i:=1 to cFieldCount do
      m_Field[i].SetSize(FFieldHeight, FFieldWidth);

  // define the size and pos of ColorPalette
  xPaletteHeight := (xInnerHeight div 2) div (FStepCount+1);
  m_PalettePanel.Top    := (xInnerHeight div 2) + (xPaletteHeight div 3);
  m_PalettePanel.Left   := FFieldWidth div 2;
  m_PalettePanel.Height := xPaletteHeight * FStepCount + 1;
  m_PalettePanel.Width  := 3*FFieldWidth;

  // define the size and pos of the courve panel
  m_ClearCurve.Height := FFieldHeight * cMaxKlassFieldY;
  m_ClearCurve.Width  := FFieldWidth  * cMaxKlassFieldX;
  m_ClearCurve.Resize;
end;
//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
procedure TKlassierGrid.SelectNextGroup;
begin
  if (FEditMode = emGroup) and FDeleteGroup then begin
    HighLightGroup(m_CurrentGroupIndex, False);
    if m_CurrentGroupIndex < m_GroupList.Count-1 then
      inc(m_CurrentGroupIndex)
    else
      m_CurrentGroupIndex := 0;
    HighLightGroup(m_CurrentGroupIndex, True);
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetClearerPara(Index: Integer; Value: rClearerParameter);
begin
  case Index of
    0: m_ClearCurve.ClearerPara := Value;
    1: m_ClearCurve.SplicePara  := Value;
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetColor(Index: Integer; Value: TColor);
begin
  case Index of
    0: FGridColor := Value;
  end;
  UpdateFieldProperties([usColor]);
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetCurveEdit(Value: eClearCurve);
begin
  m_ClearCurve.CurveEdit := Value;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetCurveType(Value: sCurveType);
begin
  m_ClearCurve.CurveType := Value;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetDataMode(Value: eDataMode);
begin
  if FDataMode <> Value then begin
    FDataMode := Value;
    UpdateFieldProperties([usValue]);
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetDeleteGroup(Value: Boolean);
begin
  if FEditMode = emGroup then begin
    FDeleteGroup := Value;
    if FDeleteGroup then begin
      if m_GroupList.Count > 0 then begin
        m_CurrentGroupIndex := 0;         // select first Group
        HighLightGroup(m_CurrentGroupIndex, True);
      end else begin
        FDeleteGroup := False;           // no Groups available
        MessageBeep(MB_ICONEXCLAMATION);
        MessageDlg('No User grids defined.', mtInformation, [mbOK], 0);
      end;
    end else
      HighLightGroup(m_CurrentGroupIndex, False);
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetEditMode(Value: eEditMode);
var
  i: Integer;
  xValue: Array[1..cFieldCount] of Single;
begin
  if Value <> FEditMode then begin
    FEditMode := Value;
    DeleteGroup := False;
    // Enable or Disable the Groupbox for Parameter settings
    if Assigned(FClearParaGroup) then
      FClearParaGroup.Enabled := (FEditMode = emNone);
    if Assigned(FSpliceParaGroup) then
      FSpliceParaGroup.Enabled := (FEditMode = emNone);

    case FEditMode of
      emGroup: begin
          ShowClass := False;
          // in Group- or Class-Edit mode -> no curves
          m_ClearCurve.CurveEdit := ccNone;
          m_ClearCurve.CurveType := [];
        end;
      emClass: begin
          // in Group- or Class-Edit mode -> no curves
          m_ClearCurve.CurveEdit := ccNone;
          m_ClearCurve.CurveType := [];
          ShowClass := True;
        end;
      emCurve: begin
          m_ClearCurve.CurveEdit := ccNep;
        end;
    else  // emNone
      m_ClearCurve.CurveEdit := ccNone;
    end;
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetFlowColor(Value: eFlowColor);
begin
  case Value of
    fcGreen: FFlowColor := clGreen;
    fcBlue:  FFlowColor := clBlue;
  else  // fcRed
    FFlowColor := clRed;
  end;
  UpdateFieldProperties([usDots]);
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetGridFont(Value: TFont);
begin
  Font := Value;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetGridMode(Value: eGridMode);
begin
  if FGridMode <> Value then begin
    FGridMode := Value;
    UpdateFieldProperties([usBorder, usValue]);
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetMaxDots(Value: Integer);
begin
  if (Value >= 100) and (Value <= 1000) then begin
    FMaxDots:= Value;
    if FValueMode in [vmDots, vmColor] then
      UpdateFieldProperties([usDots]);
  end else
    raise EComponentError.CreateFmt('%s. MaxDots should be in 100..1000!',[SInvalidPropertyValue]);
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetProductionData(aLength, aRT, aWT: Single);
begin
  m_SpooledLength := aLength;
  m_RT            := aRT;
  m_WT            := aWT;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetStepCount(Value: Integer);
begin
  if (Value >= 5) and (Value <= 20) then begin
    FStepCount:= Value;
    m_PalettePanel.FieldCount := FStepCount;
    if FValueMode in [vmDots, vmColor] then
      UpdateFieldProperties([usDots]);
  end else
    raise EComponentError.CreateFmt('%s. StepCount should be in 5..20',[SInvalidPropertyValue]);
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetShowClass(Value: Boolean);
var
  i: Integer;
begin
  if Value <> FShowClass then begin
    FShowClass := Value;
    for i:=1 to cFieldCount do
      with m_Field[i] do
        if (i in m_ClassFields) then begin
          Selected := FShowClass;
          Invalidate;
        end;
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetValueArray(aArr: Array of Single; aNumber: Byte);
var
  i: Integer;
  xValue: Array[1..cFieldCount] of Single;
begin
  if aNumber in [1,2] then begin
    // copy Values to local location and get the Min/Max level
    if aNumber = 1 then
      Move(aArr, m_Value.UnCut, SizeOf(m_Value.UnCut))
    else
      Move(aArr, m_Value.Cut, SizeOf(m_Value.Cut));
    // set start values
    m_Value.MaxValue := m_Value.UnCut[1];
    m_Value.MinValue := m_Value.UnCut[1];
    xValue[1]        := m_Value.UnCut[1] + m_Value.Cut[1];

    // evaluate the Min and Max value
    for i:=2 to cFieldCount do begin
      // Max
      if m_Value.UnCut[i] > m_Value.MaxValue then
        m_Value.MaxValue := m_Value.UnCut[i];
      // Min
      if (m_Value.UnCut[i] > 0) and
         (m_Value.UnCut[i] < m_Value.MinValue) then
        m_Value.MinValue := m_Value.UnCut[i];
      // build summary of both
      xValue[i] := m_Value.UnCut[i] + m_Value.Cut[i];
    end;
    // calculate interpolated fields
    m_ClearCurve.SetFieldValues(xValue);

    // evaluate new summaries for each Group
    with m_GroupList do
      for i:=0 to Count-1 do
        EvaluateGroupSummary(Items[i]);
    // update properties of Fields
    UpdateFieldProperties([usValue, usDots]);
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetValueColor(aIndex: Integer; aColor: TColor);
var
  i: Integer;
begin
  for i:=1 to cFieldCount do
    case aIndex of
      0: m_Field[i].UnCutColor := aColor;
      1: m_Field[i].CutColor   := aColor;
    end;
end;
//------------------------------------------------------------------------
function TKlassierGrid.GetValueColor(aIndex: Integer): TColor;
begin
  case aIndex of
    1: Result := m_Field[1].CutColor;
  else
    Result := m_Field[1].UnCutColor;
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.SetValueMode(Value: eValueMode);
begin
  if FValueMode <> Value then begin
    FValueMode := Value;
    m_PalettePanel.ValueMode := FValueMode;
    UpdateFieldProperties([usBorder, usValue]);
  end;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.Update;
begin
  inherited Update;
  if Assigned(FClearParaGroup) then
    FClearParaGroup.Update;
  if Assigned(FSpliceParaGroup) then
    FSpliceParaGroup.Update;
end;
//------------------------------------------------------------------------
procedure TKlassierGrid.UpdateFieldProperties(aUpdate: sUpdateState);
begin
  if usBorder in aUpdate then
    DefineBorderMode;

  if usColor in aUpdate then
    DefineColors;

  if usDots in aUpdate then
    DefineColorAndDotsValue;

  if usValue in aUpdate then
    DefineValueProperties;

  Invalidate;
end;
//------------------------------------------------------------------------

end.

