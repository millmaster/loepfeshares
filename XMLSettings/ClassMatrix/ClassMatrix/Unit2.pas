unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  quickrpt, Qrctrls, ExtCtrls, StdCtrls;

type
  TForm2 = class(TForm)
    QuickRep1: TQuickRep;
    Button1: TButton;
    QRImage1: TQRImage;
    procedure Button1Click(Sender: TObject);
    procedure QuickRep1BeforePrint(Sender: TQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses Unit1;


{$R *.DFM}

procedure TForm2.Button1Click(Sender: TObject);
begin
  QuickRep1.Preview;
end;

procedure TForm2.QuickRep1BeforePrint(Sender: TQuickRep;
  var PrintReport: Boolean);
begin
{
  with QRImage1.Canvas do begin
    CopyRect(ClipRect, Form1.KlassierTable1.Canvas, KlassierTable1.ClipRect);
  end;
}
  Form1.KlassierTable1.PaintTo(QRImage1.Handle, 0, 0);
  SysUtils.Beep;
end;



end.
