unit DescPanel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

type
  TDescPanel = class(TPanel)
  private
    FOffset: Integer;
    FStep: Integer;
    m_Number: Array[1..20] of Single;
    m_Count: Byte;
    m_Text: String;
    procedure WriteHorizontal;
    procedure WriteVertical;
  public
    property Offset: Integer read FOffset write FOffset;
    property Step: Integer read FStep write FStep;
    constructor Create(AOwner: TComponent; aAlign: TAlign; aSize: Integer;
                       aNumber: Array of Single; aTxt: String);
    procedure Paint; override;
    procedure Resize; override;
  end;
//************************************************************************
implementation

constructor TDescPanel.Create(AOwner: TComponent; aAlign: TAlign; aSize: Integer;
                              aNumber: Array of Single; aTxt: String);
begin
  inherited Create(AOwner);
  // Set visual properties
  Align := aAlign;
  BevelInner := bvNone;
  BevelOuter := bvNone;
  if Align in [alTop, alBottom] then
    Height := aSize
  else if Align in [alLeft, alRight] then
    Width  := aSize;
  FOffset := 1;
  FStep := 5;
  // Set visual parameters
  Move(aNumber, m_Number, SizeOf(Single)*(High(aNumber)+1));
  m_Count  := High(aNumber);
  m_Text   := aTxt;
end;
//------------------------------------------------------------------------
procedure TDescPanel.Paint;
begin
  inherited Paint;
  if Align in [alTop, alBottom] then
    WriteHorizontal
  else if Align in [alLeft, alRight] then
    WriteVertical;
end;
//------------------------------------------------------------------------
procedure TDescPanel.Resize;
begin
  inherited Resize;
end;
//------------------------------------------------------------------------
procedure TDescPanel.WriteHorizontal;
var
  i,y: Integer;
begin
  y := (Height - canvas.TextHeight('A')) div 2;
  for i:=0 to m_Count do
    canvas.TextOut(FOffset + (i*FStep), y, Format('%.1n', [m_Number[i+1]]));
  canvas.TextOut(Width - canvas.TextWidth(m_Text) - 3, y, m_Text);
end;
//------------------------------------------------------------------------
procedure TDescPanel.WriteVertical;
var
  i: Integer;
  xNegOffset: Integer;
begin
  xNegOffset := 0;
  for i:=0 to m_Count do
    if m_Number[i+1] < 0 then
      xNegOffset := -FStep + Canvas.Font.Size
    else
      canvas.TextOut(3, FOffset + xNegOffset + (i*FStep), Format('%.2n', [m_Number[i+1]]));
  canvas.TextOut(3, Height-15, m_Text);
end;
//------------------------------------------------------------------------

end.
