unit KGridProperty;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ComCtrls, ExtCtrls, Dialogs, KGridDef;

type
  TKGProperty = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Grid: TTabSheet;
    Field: TTabSheet;
    OKBtn: TButton;
    CancelBtn: TButton;
    HelpBtn: TButton;
    ColorDialog1: TColorDialog;
    GridSettings: TGroupBox;
    FontDialog1: TFontDialog;
    SpeedButton1: TSpeedButton;
    LGridFont: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SValue1: TShape;
    SValue2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    SNormalGrid: TShape;
    SBoldGrid: TShape;
    procedure SValue1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SValue2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SNormalGridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SBoldGridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
//    FGridOptions: rGridOptions;
//    procedure GetGridOptions: TGridOptions;
//    procedure SetGridOptions(aValue: rGridOptions);
    procedure UpdateValues;
  public
  published
    { Public declarations }
//    property GridOptions: rGridOptions read FGridOptions write SetGridOptions;
  end;

var
  KGProperty: TKGProperty;

implementation


{$R *.DFM}
//------------------------------------------------------------------------

//------------------------------------------------------------------------
procedure TKGProperty.UpdateValues;
begin
{
  with GridOptions do begin
    SValue1.Brush.Color := CValue1;
    SValue2.Brush.Color := CValue2;
    SNormalGrid.Brush.Color := CNormalGrid;
    SBoldGrid.Brush.Color := CBoldGrid;
//    LGridFont.Font := GridFont;
  end;
}
end;
//------------------------------------------------------------------------
procedure TKGProperty.SValue1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ColorDialog1.Execute then begin
//    FGridOptions.CValue1 := ColorDialog1.Color;
    UpdateValues;
  end;
end;
//------------------------------------------------------------------------
procedure TKGProperty.SValue2MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ColorDialog1.Execute then begin
//    FGridOptions.CValue2 := ColorDialog1.Color;
    UpdateValues;
  end;
end;
//------------------------------------------------------------------------
procedure TKGProperty.SNormalGridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ColorDialog1.Execute then begin
//    FGridOptions.CNormalGrid := ColorDialog1.Color;
    UpdateValues;
  end;
end;
//------------------------------------------------------------------------
procedure TKGProperty.SBoldGridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ColorDialog1.Execute then begin
//    FGridOptions.CBoldGrid := ColorDialog1.Color;
    UpdateValues;
  end;
end;
//------------------------------------------------------------------------
procedure TKGProperty.SpeedButton1Click(Sender: TObject);
begin
{
  FontDialog1.Font := GridOptions.GridFont;
  if FontDialog1.Execute then begin
    LGridFont.Font := FontDialog1.Font;
    UpdateValues;
  end;
}
end;
//------------------------------------------------------------------------
{
procedure TKGProperty.GetGridOptions: TGridOptions;
begin
end;
}
//------------------------------------------------------------------------
{
procedure TKGProperty.SetGridOptions(aValue: rGridOptions);
begin
//  FGridOptions.Assign(aValue);
//  FGridOptions := aValue;
  UpdateValues;
end;
}
//------------------------------------------------------------------------
end.

