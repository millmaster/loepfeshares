unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, KlassierTable, StdCtrls, KGridDef, Menus, Printers, Math,
  ToolWin, ComCtrls, Spin, ClearParameter;

type
  TForm1 = class(TForm)
    PopupMenu1: TPopupMenu;
    MakeGroup1: TMenuItem;
    N1: TMenuItem;
    ZEGrid1: TMenuItem;
    UserGrid1: TMenuItem;
    PrintDialog1: TPrintDialog;
    DataMode1: TMenuItem;
    km100: TMenuItem;
    km1000: TMenuItem;
    ky100: TMenuItem;
    ky1000: TMenuItem;
    Absolute1: TMenuItem;
    Panel1: TPanel;
    KlassierTable1: TKlassierTable;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    PaletteStep: TLabel;
    GroupBox6: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    GroupBox7: TGroupBox;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    Button1: TButton;
    SpinEdit1: TSpinEdit;
    FlowColorGroup: TGroupBox;
    FColorRed: TRadioButton;
    FColorGreen: TRadioButton;
    FColorBlue: TRadioButton;
    ClassONOFF: TButton;
    TabSheet6: TTabSheet;
    GroupBox10: TGroupBox;
    RadioButton10: TRadioButton;
    RadioButton11: TRadioButton;
    RadioButton12: TRadioButton;
    RadioButton13: TRadioButton;
    GroupDelete: TCheckBox;
    NextGroup: TButton;
    DeleteGroup: TButton;
    gbCurveEdit: TGroupBox;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    RadioButton9: TRadioButton;
    RadioButton14: TRadioButton;
    TabSheet2: TTabSheet;
    GroupBox9: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    TabSheet1: TTabSheet;
    lUncutted: TLabel;
    Label17: TLabel;
    lCutted: TLabel;
    Label2: TLabel;
    lCutPercent: TLabel;
    lCuts: TLabel;
    lEff: TLabel;
    lOrgEff: TLabel;
    Label19: TLabel;
    lNewEff: TLabel;
    SaveFields: TButton;
    Cutt_Info: TButton;
    nCutTime: TSpinEdit;
    ClearParameter1: TClearParameter;
    ClusterParameter1: TClusterParameter;
    SpliceParameter1: TSpliceParameter;
    procedure FormCreate(Sender: TObject);
    procedure ZEGridClick(Sender: TObject);
    procedure UserGridClick(Sender: TObject);
    procedure MakeGroup1Click(Sender: TObject);
    procedure ZEGrid1Click(Sender: TObject);
    procedure UserGrid1Click(Sender: TObject);
    procedure GroupDeleteClick(Sender: TObject);
    procedure NextGroupClick(Sender: TObject);
    procedure DeleteGroupClick(Sender: TObject);
    procedure ValueModeClick(Sender: TObject);
    procedure PrintScreenClick(Sender: TObject);
    procedure CurveClick(Sender: TObject);
    procedure EditModeClick(Sender: TObject);
    procedure ShowClassClick(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure FlowColorClick(Sender: TObject);
    procedure ClassONOFFClick(Sender: TObject);
    procedure CurveEditClick(Sender: TObject);
    procedure SaveFieldsClick(Sender: TObject);
    procedure Cutt_InfoClick(Sender: TObject);
    procedure DataModeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  KlassierTable1.SetValue(cValues[1], 1);
  KlassierTable1.SetValue(cValues[2], 2);
  KlassierTable1.SetProductionData(67922664, 0, 0);
end;

procedure TForm1.ZEGridClick(Sender: TObject);
begin
  KlassierTable1.GridMode := gmZE;
end;

procedure TForm1.UserGridClick(Sender: TObject);
begin
  KlassierTable1.GridMode := gmUser;
end;

procedure TForm1.MakeGroup1Click(Sender: TObject);
begin
  KlassierTable1.MakeNewGroup;
end;

procedure TForm1.ZEGrid1Click(Sender: TObject);
begin
  (Sender as TMenuItem).Checked := True;
  KlassierTable1.GridMode := gmZE;
end;

procedure TForm1.UserGrid1Click(Sender: TObject);
begin
  (Sender as TMenuItem).Checked := True;
  KlassierTable1.GridMode := gmUser;
end;

procedure TForm1.GroupDeleteClick(Sender: TObject);
begin
  KlassierTable1.DeleteGroup := (Sender as TCheckBox).Checked;
end;

procedure TForm1.NextGroupClick(Sender: TObject);
begin
  KlassierTable1.SelectNextGroup;
end;

procedure TForm1.DeleteGroupClick(Sender: TObject);
begin
  KlassierTable1.DeleteSelectedGroup;
end;

procedure TForm1.ValueModeClick(Sender: TObject);
begin
  with Sender as TRadioButton do begin
    case TabOrder of
      0: KlassierTable1.ValueMode := vmValue;
      1: KlassierTable1.ValueMode := vmSummary;
      2: KlassierTable1.ValueMode := vmDots;
      3: KlassierTable1.ValueMode := vmColor;
    end;
    FlowColorGroup.Enabled := (TabOrder = 3);
  end;
end;

procedure TForm1.PrintScreenClick(Sender: TObject);
var
  xRect: TRect;
  xW, xH: Integer;
begin
{}
  PageControl2.Visible := False;
  Self.Print;
  PageControl2.Visible := True;
{}
//  xPrint := TPrinter.Create;
{
  with Printer do begin
    Title := 'Test';
    xRect := Canvas.ClipRect;
    BeginDoc;
    xW := KlassierTable1.Width;
    xH := KlassierTable1.Height;
    KlassierTable1.Width := xRect.Right;
    KlassierTable1.Height := xRect.Bottom;
    KlassierTable1.PaintTo(Handle, 0, 0);
    EndDoc;
    KlassierTable1.Width := xW;
    KlassierTable1.Height := xH;
  end;
}
end;


procedure TForm1.CurveClick(Sender: TObject);
begin
  with (Sender as TCheckBox) do
    case TabOrder of
      0: KlassierTable1.ClearerCurve := Checked;
      1: KlassierTable1.SpliceCurve  := Checked;
      2: KlassierTable1.ClusterCurve := Checked;
    end;
end;

procedure TForm1.EditModeClick(Sender: TObject);
begin
  with (Sender as TRadioButton) do begin
    gbCurveEdit.Visible := (TabOrder = 2);
    case TabOrder of
      0: KlassierTable1.EditMode := emNone;
      1: KlassierTable1.EditMode := emGroup;
      2: KlassierTable1.EditMode := emCurve;
      3: KlassierTable1.EditMode := emClass;
    end;
  end;
end;

procedure TForm1.ShowClassClick(Sender: TObject);
begin
  KlassierTable1.ShowClass := (Sender as TCheckBox).Checked;
end;



procedure TForm1.SpinEdit1Change(Sender: TObject);
begin
  KlassierTable1.StepCount := SpinEdit1.Value;
end;

procedure TForm1.FlowColorClick(Sender: TObject);
begin
  with (Sender as TRadioButton) do
    case TabOrder of
      0: KlassierTable1.FlowColor := fcRed;
      1: KlassierTable1.FlowColor := fcGreen;
      2: KlassierTable1.FlowColor := fcBlue;
    end;
end;



procedure TForm1.ClassONOFFClick(Sender: TObject);
begin
  KlassierTable1.ShowClass := not KlassierTable1.ShowClass;
end;

procedure TForm1.CurveEditClick(Sender: TObject);
begin
  with (Sender as TRadioButton) do
    case TabOrder of
      0: KlassierTable1.CurveEdit := ccNep;
      1: KlassierTable1.CurveEdit := ccShort;
      2: KlassierTable1.CurveEdit := ccLong;
      3: KlassierTable1.CurveEdit := ccThin;
    end;
end;

procedure TForm1.SaveFieldsClick(Sender: TObject);
var
  i,j,k, xBase: Integer;
  xIndex: Integer;
  xEnd: Integer;
  s, sStart: String;
  xF: Array[1..176] of rNeighbors;
  xValues: Array[1..128] of Single;
  xFile: System.Text;
  xOffset: Double;

  procedure GetNeighborValue(aIndex: Integer; var aNeighbor: rNeighbors; aValues: Array of Single);
  var
    i: Integer;
    xFieldNr: Integer;
    {  Neighbors im Pos-Bereich:
       | 1  |  2 |
       |----|----|
       | 0  |  3 |
       |----|----|
      *** ACHTUNG!!!! Beim Array als Parameter beginnt der indexierte Zugriff bei 0}
  begin
    xFieldNr := aIndex;
    case xFieldNr of
      57..72:   aIndex := xFieldNr - 8;
      73..80:   aIndex := xFieldNr - 16;
      81..88:   aIndex := xFieldNr - 24;
      89..96:   aIndex := xFieldNr - 32;
      97..112:  aIndex := xFieldNr - 40;
      113..176: aIndex := xFieldNr - 48;
    end;
    dec(aIndex);  // Parameter array begins with 0
    for i:=0 to 3 do begin
      case i of
        // bottom left
        0: case xFieldNr of
             49..64,109..112,117..120:
               aNeighbor.values[i] := aValues[aIndex]/2;
             65..104:
               aNeighbor.values[i] := aValues[aIndex]/5;
{
             57..64,117..120:
               aNeighbor.values[i] := (aValues[aIndex]+aValues[aIndex+8])/2;
             73..80:
               aNeighbor.values[i] := aValues[aIndex]*4/5;
             81..88:
               aNeighbor.values[i] := aValues[aIndex]*3/5;
             89..96:
               aNeighbor.values[i] := aValues[aIndex]*2/5;
             97..104:
               aNeighbor.values[i] := aValues[aIndex]*1/5;
}
             // Thin range
             106..108,114..116:
               aNeighbor.values[i] := aValues[aIndex-1]/2;
             122..124,130..132,138..140,146..148,154..156,162..164,170..172:
               aNeighbor.values[i] := aValues[aIndex-1];
{
             106..108,122..124,130..132,138..140,146..148,154..156,162..164,170..172:
               aNeighbor.values[i] := aValues[aIndex-1];
             114..116:
               aNeighbor.values[i] := (aValues[aIndex-1]+aValues[aIndex+7])/2;
}
             // lower border of Thin range
             105,113,121,129,137,145,153,161,169:
               aNeighbor.values[i] := 0;
           else
             aNeighbor.values[0] := aValues[aIndex];
           end;
        // top left
        1: case xFieldNr of
             49..55,57..63:
               aNeighbor.values[i] := aValues[aIndex+1]/2;
             65..71,73..79,81..87,89..95,97..103:
               aNeighbor.values[i] := aValues[aIndex+1]/5;
{
             57..63:
               aNeighbor.values[i] := (aValues[aIndex+1]+aValues[aIndex+9])/2;
             73..79:
               aNeighbor.values[i] := aValues[aIndex+1]*4/5;
             81..87:
               aNeighbor.values[i] := aValues[aIndex+1]*3/5;
             89..95:
               aNeighbor.values[i] := aValues[aIndex+1]*2/5;
             97..103:
               aNeighbor.values[i] := aValues[aIndex+1]*1/5;
}
             112: aNeighbor.values[i] := aValues[32];   // 72: Field 33
             120: aNeighbor.values[i] := aValues[40];   //     Field 41
             128: aNeighbor.values[i] := aValues[48];   // 80: Field 49
             136: aNeighbor.values[i] := (aValues[48]+aValues[56])/2;
             144: aNeighbor.values[i] := aValues[56];   // 96: Field 57
             152,160,168,176:
               aNeighbor.values[i] := aValues[56]/5;
             // Thin range
             121..124,129..132,137..140,145..148,153..156,161..164,169..172:
               aNeighbor.values[i] := aValues[aIndex];
             108..108,113..116:
               aNeighbor.values[i] := aValues[aIndex]/2;
{
             105..108,121..124,129..132,137..140,145..148,153..156,161..164,169..172:
               aNeighbor.values[i] := aValues[aIndex];
             113..116:
               aNeighbor.values[i] := (aValues[aIndex]+aValues[aIndex+8])/2;
}
             // Fields at top border
             8,16,24,32,40,48,56,64,72,80,88,96,104:
               aNeighbor.values[i] := 0;
           else
             aNeighbor.values[i] := aValues[aIndex+1];
           end;
        // top right
        2: case xFieldNr of
             49..55,57..63,109..111,117..119:
               aNeighbor.values[i] := aValues[aIndex+1]/2;
             65..71,73..79,81..87,89..95:
               aNeighbor.values[i] := aValues[aIndex+1]/5;
{
             49..55, 109..111:
               aNeighbor.values[i] := (aValues[aIndex+1]+aValues[aIndex+9])/2;
             65..71:
               aNeighbor.values[i] := aValues[aIndex+1]*4/5;
             73..79:
               aNeighbor.values[i] := aValues[aIndex+1]*3/5;
             81..87:
               aNeighbor.values[i] := aValues[aIndex+1]*2/5;
             89..95:
               aNeighbor.values[i] := aValues[aIndex+1]*1/5;
}
             112: aNeighbor.values[i] := aValues[40];  // 72: Field 41
             120: aNeighbor.values[i] := aValues[48];  //   : Field 49
             128: aNeighbor.values[i] := aValues[48]/2;// 80
             136: aNeighbor.values[i] := aValues[56];  // 88: Field 57
             144,152,160,168:
               aNeighbor.values[i] := aValues[56]/5;
             // Thin range
             105..108:
               aNeighbor.values[i] := aValues[aIndex]/2;
             113..116,121..124,129..132,137..140,145..148,153..156,161..164:
               aNeighbor.values[i] := aValues[aIndex+8];
{
             105..108:
               aNeighbor.values[i] := (aValues[aIndex]+aValues[aIndex+8])/2;
             113..116,121..124,129..132,137..140,145..148,153..156,161..164:
               aNeighbor.values[i] := aValues[aIndex+8];
}
             // Fields at top right border
             8,16,24,32,40,48,56,64,72,80,88,96,97..104,169..176:
               aNeighbor.values[i] := 0;
           else
             aNeighbor.values[i] := aValues[aIndex+9];
           end;
        // bottom right
        3: case xFieldNr of
             49..56,109..112:
               aNeighbor.values[i] := aValues[aIndex]/2;
             57..64:
               aNeighbor.values[i] := aValues[aIndex+8]/5;
             65..96:
               aNeighbor.values[i] := aValues[aIndex]/5;
{
             49..56,109..112:
               aNeighbor.values[i] := (aValues[aIndex]+aValues[aIndex+8])/2;
             57..64:
               aNeighbor.values[i] := aValues[aIndex+8];
             65..72:
               aNeighbor.values[i] := aValues[aIndex]*4/5;
             73..80:
               aNeighbor.values[i] := aValues[aIndex]*3/5;
             81..88:
               aNeighbor.values[i] := aValues[aIndex]*2/5;
             89..96:
               aNeighbor.values[i] := aValues[aIndex]*1/5;
}
             // Thin range
             114..116,122..124,130..132,138..140,146..148,154..156,162..164:
               aNeighbor.values[i] := aValues[aIndex+7];
             106..108:
               aNeighbor.values[i] := aValues[aIndex-1]/2;
{
             114..116,122..124,130..132,138..140,146..148,154..156,162..164:
               aNeighbor.values[i] := aValues[aIndex+7];
             106..108:
               aNeighbor.values[i] := (aValues[aIndex-1]+aValues[aIndex+7])/2;
}
             // Thin range and Fields at right border
             105,113,121,129,137,145,153,161,
             97..104,169..176:
               aNeighbor.values[i] := 0;
           else
             aNeighbor.values[i] := aValues[aIndex+8];
           end;
      end;
    end;
  end;

  procedure InterpolateField(var aField: rNeighbors; aIndex: Integer);
  var
    i: Integer;
  begin
    with aField do
      for i:=1 to 9 do begin
        if aIndex in [105..108,113..116,121..124,129..132,137..140,145..148,153..156,161..164,169..172] then
          calc[i] := values[1] / 9
        else
          calc[i] := values[0] / 9;

        if calc[i] > 0 then begin
          calc[i] := ln(calc[i]);
          if calc[i] < xOffset then
            xOffset := calc[i];
        end;
      end;
{
    for i:=0 to 8 do
      with aField do begin
        // Thin range
//        if aIndex in [65..68,73..76,81..84,89..92,97..100,105..108,113..116,121..124] then
          case i of
            0: calc[i] := Sum(values)/6;
            1: calc[i] := values[0];
            2: calc[i] := values[0] - ((values[0]-values[1])/3);
            3: calc[i] := values[1] + ((values[0]-values[1])/3);
            4: calc[i] := values[0] - ((values[0]-values[3])/3);
            5: calc[i] := values[1] - ((values[1]-values[2])/3);
            6: calc[i] := values[3] + ((values[0]-values[3])/3);
            7: calc[i] := values[3] - ((values[3]-values[2])/3);
            8: calc[i] := values[2] + ((values[3]-values[2])/3);
          end;

        if calc[i] > 0 then begin
          calc[i] := ln(calc[i]);
          if calc[i] < xOffset then
            xOffset := calc[i];
        end;

      end;
}
  end;

  procedure AddOffset(var aField: rNeighbors);
  var
    i: Integer;
  begin
    for i:=1 to 9 do
      with aField do
        if abs(calc[i]) > 0 then
          calc[i] := calc[i] - xOffset;
  end;

begin
  xOffset := cValues[1,1];
  for i:=1 to 128 do
    xValues[i] := cValues[1,i] + cValues[2,i];

  for i:=1 to 176 do begin
    GetNeighborValue(i, xF[i], xValues);
  end;
  for i:=1 to 176 do begin
    InterpolateField(xF[i], i);
  end;
{}
  for i:=1 to 176 do begin
    AddOffset(xF[i]);
  end;
{}

{}
  xEnd := 12;
  // write interpolated fields
  System.Assign(xFile, 'c:\temp\klass.txt');
  System.Rewrite(xFile);
  xBase := 0;
  sStart := '';
  for k:=1 to 2 do begin
    for i:=8 downto 1 do begin
      s := sStart;
      for j:=0 to xEnd do begin
        xIndex := xBase + (j*8)+i;
        with xF[xIndex] do begin
          s := s + Format('%.5g;%.5g;%.5g;',[calc[3],calc[6],calc[9]]);
        end;
      end;
      WriteLn(xFile, s);

      s := sStart;
      for j:=0 to xEnd do begin
        xIndex := xBase + (j*8)+i;
        with xF[xIndex] do begin
          s := s + Format('%.5g;%.5g;%.5g;',[calc[2],calc[5],calc[8]]);
        end;
      end;
      WriteLn(xFile, s);

      s := sStart;
      for j:=0 to xEnd do begin
        xIndex := xBase + (j*8)+i;
        with xF[xIndex] do begin
          s := s + Format('%.5g;%.5g;%.5g;',[calc[1],calc[4],calc[7]]);
        end;
      end;
      WriteLn(xFile, s);
      if (k=2) and (i=5) then begin
        WriteLn(xFile, '');
        WriteLn(xFile, '');
      end;
    end;
    WriteLn(xFile, '');
    xBase := 104;
    xEnd := 8;
    sStart := '0;0;0;0;0;0;0;0;0;0;0;0;';
  end;
  System.Close(xFile);
{}

  // write neighbor values
  System.Assign(xFile, 'c:\temp\neighbor.txt');
  System.Rewrite(xFile);
  xBase := 0;
  xEnd := 12;
  sStart := '';
  for i:=8 downto 1 do begin
    s := sStart;
    for j:=0 to xEnd do begin
      xIndex := xBase + (j*8)+i;
      with xF[xIndex] do begin
        s := s + Format('%.5g;%.5g;',[values[1],values[2]]);
      end;
    end;
    WriteLn(xFile, s);

    s := sStart;
    for j:=0 to xEnd do begin
      xIndex := xBase + (j*8)+i;
      with xF[xIndex] do begin
        s := s + Format('%.5g;%.5g;',[values[0],values[3]]);
      end;
    end;
    WriteLn(xFile, s);
  end;
//  WriteLn(xFile, '');
  // neighbor f�r D�nnbereich
  xBase := 104;
  xEnd := 8;
  sStart := '0;0;0;0;0;0;0;0;';
  for i:=8 downto 1 do begin
    s := sStart;
    for j:=0 to xEnd do begin
      xIndex := xBase + (j*8)+i;
      with xF[xIndex] do begin
        s := s + Format('%.5g;%.5g;',[values[1],values[2]]);
      end;
    end;
    WriteLn(xFile, s);

    s := sStart;
    for j:=0 to xEnd do begin
      xIndex := xBase + (j*8)+i;
      with xF[xIndex] do begin
        s := s + Format('%.5g;%.5g;',[values[0],values[3]]);
      end;
    end;
    WriteLn(xFile, s);
    if (i=5) then begin
      WriteLn(xFile, '');
      WriteLn(xFile, '');
    end;
  end;
  WriteLn(xFile, '');
  System.Close(xFile);
end;

procedure TForm1.Cutt_InfoClick(Sender: TObject);
const
  cEff = 0.85;
var
  xUC, xC, xRefUc, xRefC: Single;
  xSingle: Single;
  xWT,xN: Single;
begin
  KlassierTable1.GetCutInfo(xUC, xC, xRefUC, xRefC);
  lUncutted.Caption := Format('%.5g', [xUC]);
  lCutted.Caption := Format('%.5g', [xC]);

  xWT := (xRefC * nCutTime.Value) / (1-cEff);
  xN  := (xWT - (xC * nCutTime.Value)) / xWT;
  xSingle := (xC / xRefC * 100)-100;

  lCutPercent.Caption := Format('%.5g', [xSingle]);
  lOrgEff.Caption := Format('%.5g', [cEff]);
  lNewEff.Caption := Format('%.5g', [xN]);
end;

procedure TForm1.DataModeClick(Sender: TObject);
begin
  with Sender as TMenuItem do begin
    Checked := True;
    case Tag of
      0: KlassierTable1.DataMode := dmAbsolute;
      1: KlassierTable1.DataMode := dm100km;
      2: KlassierTable1.DataMode := dm1000km;
      3: KlassierTable1.DataMode := dm100ky;
      4: KlassierTable1.DataMode := dm1000ky;
    end;
  end;
end;

end.
