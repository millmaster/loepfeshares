unit KlassierTable;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, KlassierGrid, DescPanel, KGridDef, KGridProperty;

type
  TKlassierTable = class(TPanel)
    m_KGrid:     TKlassierGrid;
    m_LeftPanel: TDescPanel;
    m_TopPanel:  TDescPanel;
  protected
    function GetClearerPara(Index: Integer): rClearerParameter;
    function GetCurveEdit: eClearCurve;
    function GetCurveType(Index: Integer): Boolean;
    function GetDataMode: eDataMode;
    function GetEditMode: eEditMode;
    function GetFlowColor: eFlowColor;
    function GetGridColor: TColor;
    function GetGridFont: TFont;
    function GetGridMode: eGridMode;
    function GetMaxDots: Integer;
    function GetParaGroup(Index: Integer): TGroupBox;
    function GetShowClass: Boolean;
    function GetStepCount: Integer;
    function GetValueColor(aIndex: Integer): TColor;
    function GetValueMode: eValueMode;
    procedure SetClearerPara(Index: Integer; Value: rClearerParameter);
    procedure SetCurveEdit(Value: eClearCurve);
    procedure SetCurveType(Index: Integer; Value: Boolean);
    procedure SetDataMode(Value: eDataMode);
    procedure SetDeleteGroup(Value: Boolean);
    procedure SetEditMode(Value: eEditMode);
    procedure SetGridColor(Value: TColor);
    procedure SetGridFont(Value: TFont);
    procedure SetGridMode(Value: eGridMode);
    procedure SetFlowColor(Value: eFlowColor);
    procedure SetMaxDots(Value: Integer);
    procedure SetParaGroup(Index: Integer; Value: TGroupBox);
    procedure SetShowClass(Value: Boolean);
    procedure SetStepCount(Value: Integer);
    procedure SetValueColor(aIndex: Integer; Value: TColor);
    procedure SetValueMode(Value: eValueMode);
  public
    { Public declarations }
    property ClearerPara: rClearerParameter index 0 read GetClearerPara write SetClearerPara;
    property ClearParaGroup: TGroupBox index 0 read GetParaGroup write SetParaGroup;
    property CurveEdit: eClearCurve read GetCurveEdit write SetCurveEdit;
    property DeleteGroup: Boolean write SetDeleteGroup;
    property EditMode: eEditMode read GetEditMode write SetEditMode;
    property SplicePara: rClearerParameter index 1 read GetClearerPara write SetClearerPara;
    property SpliceParaGroup: TGroupBox index 1 read GetParaGroup write SetParaGroup;

    constructor Create(AOwner: TComponent); override;
    procedure DeleteSelectedGroup;
    procedure GetCutInfo(var Uncutted, Cutted, RefUc, RefC: Single);
    procedure MakeNewGroup;
    procedure Resize; override;
    procedure SelectNextGroup;
    procedure SetProductionData(aLength, aRT, aWT: Single);
    procedure SetValue(aArr: Array of Single; aNumber: Byte);
    procedure UpdateClearPara;
  published
    property ClearerCurve: Boolean index 0 read GetCurveType write SetCurveType;
    property ClusterCurve: Boolean index 2 read GetCurveType write SetCurveType;
    property CutColor: TColor index 1 read GetValueColor write SetValueColor;
    property DataMode: eDataMode read GetDataMode write SetDataMode;
    property FlowColor: eFlowColor read GetFlowColor write SetFlowColor;
    property GridColor: TColor read GetGridColor write SetGridColor;
    property GridFont: TFont read GetGridFont write SetGridFont;
    property GridMode: eGridMode read GetGridMode write SetGridMode;
    property MaxDots: Integer read GetMaxDots write SetMaxDots;
    property ShowClass: Boolean read GetShowClass write SetShowClass;
    property SpliceCurve: Boolean index 1 read GetCurveType write SetCurveType;
    property StepCount: Integer read GetStepCount write SetStepCount;
    property UnCutColor: TColor index 0 read GetValueColor write SetValueColor;
    property ValueMode: eValueMode read GetValueMode write SetValueMode;
  end;

procedure Register;

//------------------------------------------------------------------------
implementation

procedure Register;
begin
  RegisterComponents('MMaster', [TKlassierTable]);
end;
//************************************************************************
constructor TKlassierTable.Create(AOwner: TComponent);
begin
  Randomize;
  inherited Create(AOwner);
  Color     := clWhite;

  m_KGrid     := TKlassierGrid.Create(Self);
  m_TopPanel  := TDescPanel.Create(Self, alBottom, 20, cGridValueX, '[cm]');
  m_LeftPanel := TDescPanel.Create(Self, alLeft, 30, cGridValueY, '[o]');
  m_KGrid.Parent := Self;
  m_TopPanel.Parent := Self;
  m_LeftPanel.Parent := Self;

  m_KGrid.Enabled := True;
  m_TopPanel.Enabled := False;           // no mouse, keyboard events on DescPanel
  m_TopPanel.Offset := 30;
  m_LeftPanel.Enabled := False;
end;
//------------------------------------------------------------------------
procedure TKlassierTable.DeleteSelectedGroup;
begin m_KGrid.DeleteSelectedGroup; end;
//------------------------------------------------------------------------
function TKlassierTable.GetClearerPara(Index: Integer): rClearerParameter;
begin Result := m_KGrid.GetClearerPara(Index); end;
//------------------------------------------------------------------------
procedure TKlassierTable.GetCutInfo(var Uncutted, Cutted, RefUc, RefC: Single);
begin m_KGrid.GetCutInfo(Uncutted, Cutted, RefUc, RefC); end;
//------------------------------------------------------------------------
function TKlassierTable.GetDataMode: eDataMode;
begin Result := m_KGrid.DataMode; end;
//------------------------------------------------------------------------
function TKlassierTable.GetCurveEdit: eClearCurve;
begin Result := m_KGrid.GetCurveEdit; end;
//------------------------------------------------------------------------
function TKlassierTable.GetCurveType(Index: Integer): Boolean;
begin
  case Index of
    1: Result := ctSplice  in m_KGrid.GetCurveType;
    2: Result := ctCluster in m_KGrid.GetCurveType;
  else
    Result := ctClearer in m_KGrid.GetCurveType;
  end;
end;
//------------------------------------------------------------------------
function TKlassierTable.GetEditMode: eEditMode;
begin Result := m_KGrid.EditMode; end;
//------------------------------------------------------------------------
function TKlassierTable.GetFlowColor: eFlowColor;
begin Result := m_KGrid.FlowColor; end;
//------------------------------------------------------------------------
function TKlassierTable.GetGridColor: TColor;
begin Result := m_KGrid.GridColor; end;
//------------------------------------------------------------------------
function TKlassierTable.GetGridFont: TFont;
begin Result := m_KGrid.Font; end;
//------------------------------------------------------------------------
function TKlassierTable.GetGridMode: eGridMode;
begin Result := m_KGrid.GridMode; end;
//------------------------------------------------------------------------
function TKlassierTable.GetMaxDots: Integer;
begin Result := m_KGrid.MaxDots; end;
//------------------------------------------------------------------------
function TKlassierTable.GetParaGroup(Index: Integer): TGroupBox;
begin
  if Index = 0 then Result := m_KGrid.ClearParaGroup
  else              Result := m_KGrid.SpliceParaGroup;
end;
//------------------------------------------------------------------------
function TKlassierTable.GetShowClass: Boolean;
begin Result := m_KGrid.ShowClass; end;
//------------------------------------------------------------------------
function TKlassierTable.GetStepCount: Integer;
begin Result := m_KGrid.StepCount; end;
//------------------------------------------------------------------------
function TKlassierTable.GetValueColor(aIndex: Integer): TColor;
begin Result := m_KGrid.GetValueColor(aIndex); end;
//------------------------------------------------------------------------
function TKlassierTable.GetValueMode: eValueMode;
begin Result := m_KGrid.ValueMode; end;
//------------------------------------------------------------------------
procedure TKlassierTable.MakeNewGroup;
begin m_KGrid.BuildGroup; end;
//------------------------------------------------------------------------
procedure TKlassierTable.Resize;
begin
  inherited Resize;
  m_TopPanel.Step := m_KGrid.FieldWidth;
  m_LeftPanel.Offset := m_KGrid.FieldHeight + Font.Height;
  m_LeftPanel.Step   := m_KGrid.FieldHeight;
end;
//------------------------------------------------------------------------
procedure TKlassierTable.SelectNextGroup;
begin m_KGrid.SelectNextGroup; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetClearerPara(Index: Integer; Value: rClearerParameter);
begin m_KGrid.SetClearerPara(Index, Value); end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetCurveEdit(Value: eClearCurve);
begin m_KGrid.SetCurveEdit(Value); end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetCurveType(Index: Integer; Value: Boolean);
var
  xCurve: eCurveType;
  xSet: sCurveType;
begin
  xSet := m_KGrid.GetCurveType;
  case Index of
    1: xCurve := ctSplice;
    2: xCurve := ctCluster;
  else // 0
    xCurve := ctClearer;
  end;
  if Value then Include(xSet, xCurve)
  else          Exclude(xSet, xCurve);
  m_KGrid.SetCurveType(xSet);
end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetDataMode(Value: eDataMode);
begin m_KGrid.DataMode := Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetDeleteGroup(Value: Boolean);
begin m_KGrid.DeleteGroup := Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetEditMode(Value: eEditMode);
begin m_KGrid.EditMode := Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetFlowColor(Value: eFlowColor);
begin m_KGrid.FlowColor := Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetGridColor(Value: TColor);
begin m_KGrid.GridColor := Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetGridFont(Value: TFont);
begin m_KGrid.GridFont := Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetGridMode(Value: eGridMode);
begin m_KGrid.GridMode := Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetMaxDots(Value: Integer);
begin m_KGrid.MaxDots:= Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetParaGroup(Index: Integer; Value: TGroupBox);
begin
  if Index = 0 then m_KGrid.ClearParaGroup  := Value
  else              m_KGrid.SpliceParaGroup := Value;
end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetShowClass(Value: Boolean);
begin m_KGrid.ShowClass := Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetStepCount(Value: Integer);
begin m_KGrid.StepCount:= Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetValueColor(aIndex: Integer; Value: TColor);
begin m_KGrid.SetValueColor(aIndex, Value); end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetProductionData(aLength, aRT, aWT: Single);
begin m_KGrid.SetProductionData(aLength, aRT, aWT); end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetValue(aArr: Array of Single; aNumber: Byte);
begin m_KGrid.SetValueArray(aArr, aNumber); end;
//------------------------------------------------------------------------
procedure TKlassierTable.SetValueMode(Value: eValueMode);
begin m_KGrid.ValueMode := Value; end;
//------------------------------------------------------------------------
procedure TKlassierTable.UpdateClearPara;
begin
  m_KGrid.Update;
end;
//------------------------------------------------------------------------

end.

