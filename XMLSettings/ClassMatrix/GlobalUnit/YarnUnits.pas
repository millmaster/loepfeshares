unit YarnUnits;

interface
{
uses
  MMDef;
}

//---------------------------- article constants -----------------------------
const
  cMaxArtPers = 20;
  // Garnnummer Umrechnungsfaktoren von .. nach..
  // z.B: Nm=1.69336*Ne
  cNmNe:  Double  = 1.69336;
  cNmTex = 1000.0;
  //cTexNm = 1000.0;  cTexNe = 0.00169336;
  cNmNeL: Double  = 0.604772;
  cNmNeK: Double  = 1.12891;
  cNmNeW: Double  = 0.516072;
  cNmTd:  Double  = 9000.0;
  cNmTs:  Double  = 29.0291;
  cNmNcC: Double  = 1.7671;
  cNeNm:  Double  = 0.590541;
  cNeTex: Double  = 590.541;
  cNeNeL: Double  = 0.357143;
  cNeNeK: Double  = 0.666667;
  cNeNeW: Double  = 0.304762;
  cNeTd:  Double  = 5314.87;
  cNeTs:  Double  = 17.1429;
  cNeNcC: Double  = 1.04347;

  cKmToMile: Double = 0.62136995;
  cMToYd:    Double = 3937.0/3600.0; // 1.30 ca.1.094
  cKgToLb:   Double = 2.20462262185;

  // Garnnummereinheiten
  cGarnNr1 = 'Nm ';   // Nummer Metrisch
  cGarnNr2 = 'NeC';   // English Cotton Count (Deutsch:NeB)
  cGarnNr3 = 'tex';   // Fineness Tex-System
  cGarnNr4 = 'Td ';   // Internat. denier count
  cGarnNr5 = 'NeL';   // English linen count
  cGarnNr6 = 'NeW';   // English worsted count (Deutsch:NeK)
  cGarnNr7 = 'Ny ';   // English woolen count yorkshire (Deutsch:NeW)
  cGarnNr8 = 'Ts ';   // Scottish count
  cGarnNr9 = 'NcC';   // Cotton Catalonian
  cGarnNrAnz = 9;

  // Lšngeneinheiten
  cLength1 = ' m ';
  cLength2 = 'km ';
  cLength3 = 'yd ';   // 1.31 "->yd
  cLengthAnz = 3;

  // Gewichteinheiten
  cWeight1 = ' g ';
  cWeight2 = 'kg ';
  cWeight3 = ' t ';
  cWeight4 = 'lb ';
  cWeightAnz = 4;

  // Bestelleinheiten
  cOrder1 = 'kg      ';
  cOrder2 = 'Cones   ';
  cOrder3 = ' t      ';
  cOrder4 = 'lb      ';
  cOrderAnz = 4;

type
  // Artikel
{
  TeGarnNr = (Pad1, Nm, NeB, tex, Td, NeL, NeK, NeW, Ts, NcC);
  TsGarnNr = SET OF TeGarnNr;
  TaUnit = ARRAY[0..3] OF CHAR;  //1.24
}

  TeWeight = (Pad2, eWg, eWkg, eWt, eWlb);
  TaWeight = ARRAY[0..3] OF CHAR;  //1.31

  TeLength = (Pad3, eLm, eLkm, eLyd);
  TaLength = ARRAY[0..3] OF CHAR;  //1.24

{
//-------------------------------------------------------------------------
function MeterToWeightUnit(WeightUnit: TeWeight; UnitNr: TeGarnNr; Value: UCIF_LONG; GarnNr: UCIF_FLOAT): UCIF_DOUBLE;
function WeightUnitToMeter(WeightUnit: TeWeight; UnitNr: TeGarnNr; Value: UCIF_FLOAT; GarnNr: UCIF_FLOAT): UCIF_DOUBLE;
function LengthToMeter(LengthUnit: TeLength; Length: UCIF_FLOAT): UCIF_FLOAT;
function MeterToLength(LengthUnit: TeLength; Length: UCIF_FLOAT): UCIF_FLOAT;
//-------------------------------------------------------------------------
}
implementation
{
// Unit Check Routinen ----------------------------------------------------
function  GarnNrConvert(InTyp, OutTyp : TeGarnNr; ValIn : UCIF_FLOAT): UCIF_FLOAT;
begin
  case InTyp of
    Nm  : case OutTyp of
            NeB : Result := ValIn/cNmNe;
            tex : Result := cNmTex/ValIn;
            Td  : Result := cNmTd/ValIn;
            NeL : Result := ValIn/cNmNeL;
            NeK : Result := ValIn/cNmNeK;
            NeW : Result := ValIn/cNmNeW;
            Ts  : Result := cNmTs/ValIn;
            NcC : Result := ValIn/cNmNcC;
          else
            Result := ValIn;
          end;
    NeB : case OutTyp of
            Nm : Result := ValIn*cNmNe;
          else
            Result := ValIn;
          end;
    tex : case OutTyp of
            Nm : Result := cNmTex/ValIn;
          else Result := ValIn;
          end;
    Td :  case OutTyp of
            Nm : Result := cNmTd/ValIn;
          else
            Result := ValIn;
          end;
    NeL : case OutTyp of
            Nm : Result := ValIn*cNmNeL;
          else
            Result := ValIn;
          end;
    NeK : case OutTyp of
            Nm : Result := ValIn*cNmNeK;
          else
            Result := ValIn;
          end;
    NeW : case OutTyp of
            Nm : Result := ValIn*cNmNeW;
          else
            Result := ValIn;
          end;
    Ts :  case OutTyp of
            Nm : Result := cNmTs/ValIn;
          else
            Result := ValIn;
          end;
    NcC : case OutTyp of
            Nm : Result := ValIn*cNmNcC;
          else
            Result := ValIn;
          end;
  else
    Result := ValIn;
  end;
end;
//-------------------------------------------------------------------------
function MeterToWeightUnit(WeightUnit: TeWeight; UnitNr: TeGarnNr; Value: UCIF_LONG; GarnNr: UCIF_FLOAT): UCIF_DOUBLE;
begin
  case WeightUnit of
    eWg:
      Result := Value/GarnNrConvert(UnitNr, Nm, GarnNr);
    eWkg:
      Result := Value/GarnNrConvert(UnitNr, Nm, GarnNr)/1000.0;
  else
    Result := Value/GarnNrConvert(UnitNr, Nm, GarnNr)*cKgToLb/1000.0;
  end;
end;
//-------------------------------------------------------------------------
function WeightUnitToMeter(WeightUnit: TeWeight; UnitNr: TeGarnNr; Value: UCIF_FLOAT; GarnNr: UCIF_FLOAT): UCIF_DOUBLE;
begin
  case WeightUnit of
    eWg:
      Result := Value*GarnNrConvert(UnitNr, Nm, GarnNr);
  else
    Result := Value*GarnNrConvert(UnitNr, Nm, GarnNr)/cKgToLb*1000.0;
  end;
end;
//-------------------------------------------------------------------------
function LengthToMeter(LengthUnit: TeLength; Length: UCIF_FLOAT): UCIF_FLOAT;
begin
  case LengthUnit of
    eLkm : Result := Length*1000.0;
    eLyd : Result := Length/cMToYd;
  else
    Result := Length;
  end;
end;
//-------------------------------------------------------------------------
function MeterToLength(LengthUnit: TeLength; Length: UCIF_FLOAT): UCIF_FLOAT;
begin
  case LengthUnit of
    eLkm : Result := Length/1000.0;
    eLyd : Result := Length*cMToYd;
  else
    Result := Length;
  end;
end;
//-------------------------------------------------------------------------
}
end.


