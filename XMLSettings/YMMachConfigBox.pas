(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMSettingsGUI.pas
| Projectpart...: MillMaster Spulerei
| Subpart.......: Clearer Settings
| Process(es)...: -
| Description...: Graphical User Interface for YarnMaster Mach Config Settings
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 15.01.01 | 1.00 | Kr  | Initial release
| 25.01.01 | 1.01 | Kr  | Interface INavChange implemented
| 22.03.02 | 1.01 | Wss | Strings missed at scanning files with Multilizer -> marked with // ivlm
|       2005  0.01  Wss | Umbau nach XML
| 17.06.05 | 1.01 | Wss | Splice CheckLenght wird bei �berl�nge mit mod 32 behandelt
| 26.10.05 |         Lok  | Software Optioenn neu aus dem FPattern generieren (vorher Element im XML)
| 06.02.06 |         Nue  | Property NetType added. (Und Folgesachen)
| 02.05.06 | 1.02 | Nue | LZE mit Spectra richtig behandeln.
| 20.02.08 | 1.10 | Nue | TK-ZenitC Handling added.
| 21.02.08 |      | Nue | Anpassungen wegen neuer Maschinentypen. (mtAC5, mtSavioEsperoNuovo, mtSavioExcello, mtSavioPolar)
| 05.03.08 |      | Nue | Anpassungen das auf AC5 nur ZENIT-Reiniger m�glich sind.
| 11.08.08 |      | Nue | Beim einf�gen von Texten aus Konstanten-Arrays wurde nicht �bersetzt -> IvDictio mit Translate eingef.
|=============================================================================*)
unit YMMachConfigBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmLineLabel, mmCheckBox, mmComboBox, mmLabel, KeySpinEdit, PresetValue,
  mmEdit, YMParaUtils, YMParaDef, MachNodeComp, BaseGlobal, DialogConfigCode, XMLDef, xmlMaConfigClasses,
  ExtCtrls, mmPanel;
  //...........................................................................

resourcestring
  //...........................................................................
  rsCutRetriesHint         = '(*)Schnittwiederholungen bei Schnittfehler bis zur Alarmintervention:'; //ivlm
  rsSPCheckLengthHint      = '(*)Anlauflaenge waehrend dem die Spleisspruef-Einstellungen aktiv sind:'; //ivlm
  rsLongStopDefHint        = '(*)Dauer bis eine stehende Spullstelle in den Lang-Stop-Zustand uebertritt:'; //ivlm
  rsDefaultSpeedHint       = '(*)Standardgeschwindigkeit fuer Maschinen ohne Geschwindigkeitsinformation:'; //ivlm
  rsDefaultSpeedRampHint   = '(*)Standardanlaufdauer fuer Maschinen ohne Geschwindigkeitsinformation:'; //ivlm
  rsDFSDeltaHint           = '(*)Schwelle des dynamischen Fadenwaechters Bezogen auf das "Fadenrauschen":'; //ivlm
  rsHeadstockPositionRight = '(6)rechts'; //ivlm
  rsHeadstockPositionLeft  = '(6)links'; //ivlm
  rsKnifePowerHigh         = '(6)hoch'; //ivlm
  rsKnifePowerRegular      = '(6)normal'; //ivlm

  //...........................................................................

const
  //...........................................................................
{(*}
  cVSBSCutRetries: TValueSpecRec = (
    minValue: cMinRepetitions-1; maxValue: cMaxRepetitions-1;
    format: '%1.0f'; measure: '';
    caption: '';
    hint: rsCutRetriesHint;
    );

  cPTBSRepetition: array[0..4] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '3'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '5'; ));
  //...........................................................................

  cVSBSSPCheckLength: TValueSpecRec = (
    minValue: 0;
    maxValue: cSpliceCheckMax; // [cm]  // /10; // [dm]
    format: '%4.0f'; measure: '';
    caption: '';
    hint: rsSPCheckLengthHint;
    );

  cPTBSSPCheckLength: array[0..13] of TPresetTextRec = (
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '15'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '25'; ),
    (typ: ttFloat; text: '30'; ),
    (typ: ttFloat; text: '35'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '50'; ),
    (typ: ttFloat; text: '60'; ),
    (typ: ttFloat; text: '70'; ),
    (typ: ttFloat; text: '40'; ),
    (typ: ttFloat; text: '80'; ),
    (typ: ttFloat; text: '100'; ),
    (typ: ttFloat; text: '120'; ));
  //...........................................................................

  cVSBSLongStopDef: TValueSpecRec = (
    minValue: cMinLongStopDef/10;
    maxValue: cMaxLongStopDef/10;
    format: '%2.1f'; measure: '';
    caption: '';
    hint: rsLongStopDefHint;
    );

  cPTBSLongStopDef: array[0..18] of TPresetTextRec = (
    (typ: ttFloat; text: '0.2'; ),
    (typ: ttFloat; text: '0.4'; ),
    (typ: ttFloat; text: '0.8'; ),
    (typ: ttFloat; text: '1.0'; ),
    (typ: ttFloat; text: '1.5'; ),
    (typ: ttFloat; text: '2.0'; ),
    (typ: ttFloat; text: '2.5'; ),
    (typ: ttFloat; text: '3.0'; ),
    (typ: ttFloat; text: '3.5'; ),
    (typ: ttFloat; text: '4.0'; ),
    (typ: ttFloat; text: '6.0'; ),
    (typ: ttFloat; text: '8.0'; ),
    (typ: ttFloat; text: '10.0'; ),
    (typ: ttFloat; text: '12.0'; ),
    (typ: ttFloat; text: '14.0'; ),
    (typ: ttFloat; text: '16.0'; ),
    (typ: ttFloat; text: '18.0'; ),
    (typ: ttFloat; text: '20.0'; ),
    (typ: ttFloat; text: '25.0'; ) );
  //...........................................................................

  cVSBSDefaultSpeed: TValueSpecRec = (
    minValue: cMinSpeed;
    maxValue: cMaxSpeed;
    format: '%4.0f'; measure: '';
    caption: '';
    hint: rsDefaultSpeedHint;
    );

  cPTBSDefaultSpeed: array[0..18] of TPresetTextRec = (
    (typ: ttFloat; text: '500'; ),
    (typ: ttFloat; text: '550'; ),
    (typ: ttFloat; text: '600'; ),
    (typ: ttFloat; text: '650'; ),
    (typ: ttFloat; text: '700'; ),
    (typ: ttFloat; text: '750'; ),
    (typ: ttFloat; text: '800'; ),
    (typ: ttFloat; text: '850'; ),
    (typ: ttFloat; text: '900'; ),
    (typ: ttFloat; text: '950'; ),
    (typ: ttFloat; text: '1000'; ),
    (typ: ttFloat; text: '1050'; ),
    (typ: ttFloat; text: '1100'; ),
    (typ: ttFloat; text: '1150'; ),
    (typ: ttFloat; text: '1200'; ),
    (typ: ttFloat; text: '1300'; ),
    (typ: ttFloat; text: '1400'; ),
    (typ: ttFloat; text: '1500'; ),
    (typ: ttFloat; text: '1600'; ));
  //...........................................................................

  cVSBSDefaultSpeedRamp: TValueSpecRec = (
    minValue: cMinSpeedRamp;
    maxValue: cMaxSpeedRamp;
    format: '%2.0f'; measure: '';
    caption: '';
    hint: rsDefaultSpeedRampHint;
    );

  cPTBSDefaultSpeedRamp: array[0..16] of TPresetTextRec = (
    (typ: ttFloat; text: '1'; ),
    (typ: ttFloat; text: '2'; ),
    (typ: ttFloat; text: '3'; ),
    (typ: ttFloat; text: '4'; ),
    (typ: ttFloat; text: '5'; ),
    (typ: ttFloat; text: '6'; ),
    (typ: ttFloat; text: '7'; ),
    (typ: ttFloat; text: '8'; ),
    (typ: ttFloat; text: '9'; ),
    (typ: ttFloat; text: '10'; ),
    (typ: ttFloat; text: '12'; ),
    (typ: ttFloat; text: '14'; ),
    (typ: ttFloat; text: '16'; ),
    (typ: ttFloat; text: '18'; ),
    (typ: ttFloat; text: '20'; ),
    (typ: ttFloat; text: '25'; ),
    (typ: ttFloat; text: '30' ));
{*)}
  //...........................................................................
  cVSBSDFSDelta: TValueSpecRec = (
    MinValue: cMinDFSDelta; MaxValue: cMaxDFSDelta;
    Space: 0; Factor: 1;     Format: '%3.0f'; Measure: '%';
    Caption: '';
    Hint: rsDFSDeltaHint;
    XPValue: cXPDeltaItem;
    XPSwitch: '');

  cPTBSDFSDelta: array[0..18] of TPresetTextRec = (
    (typ: ttFloat; text: '0'),
    (typ: ttFloat; text: '10'),
    (typ: ttFloat; text: '20'),
    (typ: ttFloat; text: '30'),
    (typ: ttFloat; text: '40'),
    (typ: ttFloat; text: '50'),
    (typ: ttFloat; text: '60'),
    (typ: ttFloat; text: '70'),
    (typ: ttFloat; text: '80'),
    (typ: ttFloat; text: '90'),
    (typ: ttFloat; text: '100'),
    (typ: ttFloat; text: '120'),
    (typ: ttFloat; text: '140'),
    (typ: ttFloat; text: '160'),
    (typ: ttFloat; text: '180'),
    (typ: ttFloat; text: '200'),
    (typ: ttFloat; text: '220'),
    (typ: ttFloat; text: '240'),
    (typ: ttFloat; text: '250'));
  //...........................................................................

const
  cHeadstockPosition: array[swOff..swOn] of string =
    (rsHeadstockPositionLeft, rsHeadstockPositionRight);
//  cHeadstockPosition: array[ssOn..ssOff] of string =
//    (rsHeadstockPositionRight, rsHeadstockPositionLeft);

  cKnifePower: array[swOff..swOn] of string =
    (rsKnifePowerRegular, rsKnifePowerHigh);

type
  TGBMachConfigBox = class(TFrame, INavChange)
    cbAdjustCut: TmmCheckBox;
    cbAdjustRemove: TmmCheckBox;
    cbConeChangeCondition: TmmCheckBox;
    cbDriftConeChange: TmmCheckBox;
    cbExtMurItf: TmmCheckBox;
    cbKnifeMonitor: TmmCheckBox;
    cbOneDrumPuls: TmmCheckBox;
    cbUpperYarnCheck: TmmCheckBox;
    cbZeroAdjust: TmmCheckBox;
    cobHeadstock: TmmComboBox;
    cobKnifePower: TmmComboBox;
    cobSensingHead: TmmComboBox;
    edCutRetries: TKeySpinEdit;
    edDefaultSpeed: TKeySpinEdit;
    edDefaultSpeedRamp: TKeySpinEdit;
    edLongStopDef: TKeySpinEdit;
    edSPCheckLength: TKeySpinEdit;
    laAWEType: TmmLabel;
    laAWETypeStr: TmmLabel;
    laSensingHead: TmmLabel;
    laFrontmodelCaption: TmmLabel;
    laFrontmodel: TmmLabel;
    laCutRetries: TmmLabel;
    laDefaultSpeed: TmmLabel;
    laDefaultSpeedRamp: TmmLabel;
    laHeadstock: TmmLabel;
    laKnifePower: TmmLabel;
    laLongStopDef: TmmLabel;
    laSPCheckLength: TmmLabel;
    laZESwOptionlb: TmmLabel;
    laZESwOptionStr: TmmLabel;
    laZESwVersionlb: TmmLabel;
    laZESwVersionStr: TmmLabel;
    laFacility: TmmLineLabel;
    laGarnreiniger: TmmLineLabel;
    laDefaultSpeedUnit: TmmLabel;
    mplbDefaultSpeedRamp: TmmLabel;
    laLongStopDefUnit: TmmLabel;
    laSPCheckLengthUnit: TmmLabel;
    mmLineLabel1: TmmLineLabel;
    pnBlocks: TmmPanel;
    pnBlocksZenit: TmmPanel;
    pnBlocksSpectra: TmmPanel;
    pnBlocksBoth: TmmPanel;
    cbBlockAtAlarm: TmmCheckBox;
    cbBlockAtCutFailureAlarm: TmmCheckBox;
    cbBlockAtYarnCountAlarm: TmmCheckBox;
    cbBlockAtClusterAlarm: TmmCheckBox;
    cbBlockAtSFIAlarm: TmmCheckBox;
    cbBlockAtFAlarm: TmmCheckBox;
    cbBlockAtFClusterAlarm: TmmCheckBox;
    edDFSDelta: TKeySpinEdit;
    laDFSDeltaUnit: TmmLabel;
    mmLabel1: TmmLabel;
    cbBlockAtShortCountAlarm: TmmCheckBox;
    cbShortSFID: TmmCheckBox;
    cbSHZeroCut: TmmCheckBox;
    cbSuckOffLastCutOnly: TmmCheckBox;
    procedure DoConfirm(aTag: Integer);
    procedure OnValueChange(aSender: TObject);
  private
    fActiveGroup: Integer;
    fMachineType: TMachineType;
    fMaConfig: TXMLMaConfigHelper;
    FNetType: TNetTyp;
    fOnMachNodeChanged: TNotifyEvent;
    mDoChangedEnabled: Boolean;
    mTKSpectra: Boolean;
    procedure CheckComponents;
    function GetGroupValueDef(aXPath: String; aDefault: Variant): Variant;
    function GetMachValueDef(aXPath: String; aDefault: Variant): Variant;
    function GetSpeedSimulation: Boolean;
  //...........................................................
    function GetXPBoolean(aXPath: String): Boolean;
    procedure InitComboAndEditBoxes;
    procedure SetActiveGroup(const aValue: Integer);
    procedure SetMaConfig(const Value: TXMLMaConfigHelper);
    procedure ValueUpdate;
    procedure DoMachNodeChanged;
    procedure FillSensingHeadList;
    procedure SetMachineType(const aValue: TMachineType);
    procedure SetNetType(const Value: TNetTyp);
    procedure UpdateMachConfigBox;
    procedure ValueSave(aTag: Integer);
  protected
    function CheckUserValue: Boolean;
    procedure Notification(aComponent: TComponent; Operation: TOperation); override;
    procedure UpdateComponent;
    property GroupValueDef[aXPath: String; aDefault: Variant]: Variant read GetGroupValueDef;
    property MachValueDef[aXPath: String; aDefault: Variant]: Variant read GetMachValueDef;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Init;
    property ActiveGroup: Integer read fActiveGroup write SetActiveGroup;
    property MachineType: TMachineType read fMachineType write SetMachineType;
    property MaConfig: TXMLMaConfigHelper read FMaConfig write SetMaConfig;
    property NetType: TNetTyp read FNetType write SetNetType;
  published
    property OnMachNodeChanged: TNotifyEvent read fOnMachNodeChanged write fOnMachNodeChanged;
    property SpeedSimulation: Boolean read GetSpeedSimulation;
  end;
  

implementation
uses
  mmMBCS, MMUGlobal, FPatternClasses, IvDictio;  //Nue: 11.08.08 IvDictio eingef�gt wegen den Calls mit Translate

{$R *.DFM}

//:---------------------------------------------------------------------------
//:--- Class: TGBMachConfigBox
//:---------------------------------------------------------------------------
constructor TGBMachConfigBox.Create(aOwner: TComponent);
begin
  inherited;

  //  if aOwner is TForm then
  //    (aOwner as TForm).FormStyle := fsMDIForm;

  fActiveGroup       := -1;
  fMachineType       := mtGeneric;
//  fAdminMachNodeXX     := nil;
  fOnMachNodeChanged := nil;
  
//  mMachineYMConfig := TYMMachineConfig.Create;
//  mYMSettingsUtils := TYMSettingsUtils.Create;

//  mMachineYMConfig.MachineConfig := mYMSettingsUtils.InitMachineYMConfigRec;
  
  mDoChangedEnabled := True;
  
{  laCfgCodeA.DialogCaption := 'ConfigCodeA';
  laCfgCodeB.DialogCaption := 'ConfigCodeB';
  laCfgCodeC.DialogCaption := 'ConfigCodeC';}

//  if not(csDesigning in ComponentState) then
//    InitComboAndEditBoxes;

//  ActiveGroup := Low(MachineConfig.spec);
end;

//:---------------------------------------------------------------------------
destructor TGBMachConfigBox.Destroy;
begin
//  mMachineYMConfig.Free;
//  mYMSettingsUtils.Free;

  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.CheckComponents;
var
  xSHC: TSensingHeadClass;
  xFSensor: Boolean;
begin
  xSHC     := GetSensingHeadClass(GetSensingHeadValue(GroupValueDef[cXPSensingHeadItem, cSensingHeadNames[shTK830]]));
//  xFSensor := xSHC in [shc9xFS, shc9xH, shc9xBD, shcZenitF, shcZenitFP];
  xFSensor := xSHC in [shc9xFS, shc9xH, shc9xBD, shcZenitF, shcZenitFP, shcZenitCF, shcZenitCFP];   //Nue:20.02.08

  // --- Garnreiniger ---
  cbAdjustCut.Enabled    := xSHC in [shc8x, shc9xFS, shc9xH, shc9xBD];
  cbAdjustRemove.Enabled := fMachineType in [mtGeneric, mtAC338, mtSavioOrion, mtMurata21C, mtAC5, mtSavioPolar];   //Nue:21.02.08 mtAC5, mtSavioPolar added.

  // wss: Kommentar in XML Schema: Maschinenabh�ngig: Messer Schnitt wiederholungen (Sinnvollerweise immer 0)
  //  cCutRetries:
  //    Result := KnifeMonitor;  mMachineYMConfig.CutRetriesAvailable; CfgB.9
  // TODO wss: KnifeMonitor: MMI Doc: ON (nur SS und SAVIO, nicht f�r MURATA mit SS!)
  //cbKnifeMonitor.Enabled := GroupValueDef[cXPFP_SpeedSimulationItem, False] or (fMachineType in [mtGeneric, mtSavioEspero, mtSavioOrion]);
  //Nue/Sch: Gem�ss Sch ist die Logik f�r dieses Bit so komplex, dass es einfacher (und absolut vertretbar) ist, diese
  edCutRetries.Enabled   := cbKnifeMonitor.Enabled and cbKnifeMonitor.Checked;

  // --- Anlage ---
  edDefaultSpeed.Enabled := GroupValueDef[cXPFP_SpeedSimulationItem, False] or (fMachineType = mtGeneric);

  edDefaultSpeedRamp.Enabled := GroupValueDef[cXPFP_SpeedSimulationItem, False] or (fMachineType = mtGeneric);
  // TODO wss: DFSDelta ist nur bei WSC (und LZE?) verf�bar. Wie zus�tzlich pr�fen?
  // IsZenit() ??
//  edDFSDelta.Enabled      := xSHC in [shcZenitF, shcZenitFP]; //GroupValueDef[cXPFP_ZenitItem, False];
  edDFSDelta.Enabled      := xSHC in [shcZenitF, shcZenitFP, shcZenitCF, shcZenitCFP]; //GroupValueDef[cXPFP_ZenitItem, False];  //Nue:20.02.08

//  pnBlocksZenit.Visible   := xSHC in [shcZenit, shcZenitF, shcZenitFP];
//  pnBlocksSpectra.Visible := xSHC in [shc8x, shc9xFS, shc9xH, shc9xBD];
  mTKSpectra              := xSHC in [shc8x, shc9xFS, shc9xH, shc9xBD];
//  pnBlocksZenit.Visible   := (xSHC in [shcZenit, shcZenitF, shcZenitFP]) or (FNetType in [ntLX]); //Nue:2.5.06
  pnBlocksZenit.Visible   := (xSHC in [shcZenit, shcZenitF, shcZenitFP, shcZenitC, shcZenitCF, shcZenitCFP]) or (FNetType in [ntLX]); //Nue:2.5.06  //Nue:20.02.08
  pnBlocksSpectra.Visible := (xSHC in [shc8x, shc9xFS, shc9xH, shc9xBD]) and (not(FNetType in [ntLX])); //Nue:2.5.06
  if pnBlocksZenit.Visible then pnBlocksZenit.Top := 0;
  if pnBlocksSpectra.Visible then pnBlocksSpectra.Top := 0;

  cbBlockAtShortCountAlarm.Enabled    := GroupValueDef[cXPFP_ShortCountItem, False];
  cbBlockAtFAlarm.Enabled             := xFSensor;
  cbBlockAtFClusterAlarm.Enabled      := xFSensor;
  cbBlockAtCutFailureAlarm.Enabled    := cbKnifeMonitor.Checked;

  cbSHZeroCut.Enabled                 := GroupValueDef[cXPFP_SpeedSimulationItem, False];
//  cbSuckOffLastCutOnly.Enabled        := xSHC in [shcZenit, shcZenitF, shcZenitFP];
  cbSuckOffLastCutOnly.Visible        := xSHC in [shcZenit, shcZenitF, shcZenitFP, shcZenitC, shcZenitCF, shcZenitCFP];   //Nue:20.02.08

  // --- Maschine ---
  // TODO: cCfgAKnifePowerHighAvailable:
  //    Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtAC338);
  cobKnifePower.Enabled := fMachineType in [mtGeneric, mtAC338, mtAC5];  //Nue:21.02.08 mtAC5 added.

//  cAWEMachTypNames: array[amtUnknown..amtDemoWinder] of string =
//    ('Unknown winder', 'AC238', 'ESPERO', 'MUR_IND_INV', 'AC238_SYS', 'AWE_SS',
//     'WSC_C_CONER', 'AC338', 'Orion', 'Muarata 21C', 'DemoWinder'); //ivlm
// TODO: cCfgBExtMurItfAvailable:
//    Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtMUR_IND_INV);
  cbExtMurItf.Enabled := fMachineType in [mtGeneric, mtMurata7_2, mtMurata7_5];

  // TODO wss: MMI Doc = Kreuzspulenwechsel Zustand sperren (nur AC138 AWE SS Vers. > 83)
  //cbConeChangeCondition.Enabled := True;
  
// TODO: OneDrumPuls     Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtAWE_SS);
  cbOneDrumPuls.Enabled := GroupValueDef[cXPFP_SpeedSimulationItem, False] or (fMachineType = mtGeneric);
end;

//:-----------------------------------------------------------------------------
function TGBMachConfigBox.CheckUserValue: Boolean;
begin
  // vorl�ufig noch ben�tigt da im Interface INavChange so deklariert
end;

//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.DoConfirm(aTag: Integer);
begin
  ValueSave(aTag);
end;

//:-----------------------------------------------------------------------------
function TGBMachConfigBox.GetGroupValueDef(aXPath: String; aDefault: Variant): Variant;
begin
  Result := aDefault;
  if Assigned(fMaConfig) then
    Result := fMaConfig.GroupValueDef[fActiveGroup, aXPath, aDefault];
end;

//:-----------------------------------------------------------------------------
function TGBMachConfigBox.GetMachValueDef(aXPath: String; aDefault: Variant): Variant;
begin
  Result := aDefault;
  if Assigned(fMaConfig) then
    Result := fMaConfig.MachValueDef[aXPath, aDefault];
end;

//:---------------------------------------------------------------------------
function TGBMachConfigBox.GetSpeedSimulation: Boolean;
begin
  Result := cbOneDrumPuls.Enabled;
end;

  //...........................................................
function TGBMachConfigBox.GetXPBoolean(aXPath: String): Boolean;
begin
  Result := GroupValueDef[aXPath, False];
end;

//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.InitComboAndEditBoxes;
var
  xSs: TSwitch;
begin
  // Sensing Head
  FillSensingHeadList;
  // Cut Retries
  edCutRetries.LoadValueSpec(cVSBSCutRetries, cPTBSRepetition);

  // DefaultSpeed
  edDefaultSpeed.LoadValueSpec(cVSBSDefaultSpeed, cPTBSDefaultSpeed);
  // DefaultSpeedRamp
  edDefaultSpeedRamp.LoadValueSpec(cVSBSDefaultSpeedRamp, cPTBSDefaultSpeedRamp);
  // DFS Delta
  edDFSDelta.LoadValueSpec(cVSBSDFSDelta, cPTBSDFSDelta);

  // Headstock
  for xSs:=swOff to swOn do
    cobHeadstock.Items.Add(Translate(cHeadstockPosition[xSs]));   //Nue: 11.08.08 IvDictio eingef�gt wegen den Calls mit Translate
  cobHeadstock.DropDownCount := cobHeadstock.Items.Count;
  // KnifePower
  for xSs:=swOff to swOn do
    cobKnifePower.Items.Add(Translate(cKnifePower[xSs]));        //Nue: 11.08.08 IvDictio eingef�gt wegen den Calls mit Translate
  cobKnifePower.DropDownCount := cobKnifePower.Items.Count;
  // SpliceCheckLength
  edSPCheckLength.LoadValueSpec(cVSBSSPCheckLength, cPTBSSPCheckLength);
  // LongStopDef
  edLongStopDef.LoadValueSpec(cVSBSLongStopDef, cPTBSLongStopDef);

  //  cobSensingHead.ItemIndex := TMachineYMConfig.SensigHeadTypeToIndex(mMachineYMConfig.GroupSpecificConfig.SensingHead);
//  cobHeadstock.DropDownCount := Length(cHeadstockPosition[xSs]);
//  for xSs := Low(cHeadstockPosition) to High(cHeadstockPosition) do
//    cobHeadstock.Items.Insert(TYMMachineConfig.SwichStateToIndex(xSs), cHeadstockPosition[xSs]);
//  cobHeadstock.DropDownCount := TYMMachineConfig.SwichStateToIndex(High(cHeadstockPosition)) + 1;
  (*
    if mMachineYMConfig.HeadstockRight then
      cobHeadstock.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOn)
    else
      cobHeadstock.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOff);
  *)

//  cobKnifePower.DropDownCount := Length(cKnifePower[xSs]);
//  for xSs := Low(cKnifePower) to High(cKnifePower) do
//    cobKnifePower.Items.Insert(TYMMachineConfig.SwichStateToIndex(xSs), cKnifePower[xSs]);
//  cobKnifePower.DropDownCount := TYMMachineConfig.SwichStateToIndex(High(cKnifePower)) + 1;
  (*
    if mMachineYMConfig.KnifePowerHigh then
      cobKnifePower.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOn)
    else
      cobKnifePower.ItemIndex := TYMMachineConfig.SwichStateToIndex(ssOff);
  *)
end;

//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.SetActiveGroup(const aValue: Integer);
begin
  {if aValue <> fActiveGroup then {}begin
    fActiveGroup := aValue;
    UpdateMachConfigBox;
  end;
end;

procedure TGBMachConfigBox.SetMaConfig(const Value: TXMLMaConfigHelper);
begin
  fMaConfig := Value;
end;

//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.OnValueChange(aSender: TObject);
begin
  ValueSave(TControl(aSender).Tag);
end;

//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.ValueUpdate;
var
  xSH: TSensingHead;
  xInt: Integer;
begin

  if Assigned(fMaConfig) and fMaConfig.Available then begin
    // --- Garnreiniger ---
    // --------------------
    // Tastkopf
    xSH := GetSensingHeadValue(GroupValueDef[cXPSensingHeadItem, cSensingHeadNames[shTK830]]);
    cobSensingHead.ItemIndex := cobSensingHead.Items.IndexOfObject(TObject(xSH));
    laAWETypeStr.Caption     := GetAWEType(fMaConfig, fActiveGroup);

    // Schnitt vor Abgleich
    cbAdjustCut.Checked             := GetXPBoolean(cXPCutBeforeAdjustItem);
    // Garn nach Abgleich absaugen
    cbAdjustRemove.Checked          := GetXPBoolean(cXPRemoveYarnAfterAdjustItem);
  //      Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtAC338) or
  //                (fAWEMachType = amtOrion);
  //  cbAdjustRemove.Enabled        := AdjustRemoveAvailable;

    // Null Abgleich �berwachung CfgA.4
    cbZeroAdjust.Checked          := GetXPBoolean(cXPZeroTestItem); //ZeroAdjust;

    // Driftkompensation nach Kreuzspulwechsel CfgA.11
    cbDriftConeChange.Checked     := GetXPBoolean(cXPDriftCompensationItem); //DriftConeChange;

    // Schnitt�berwachung CfgB.9
    cbKnifeMonitor.Checked        := GetXPBoolean(cXPKnifeMonitorItem); //KnifeMonitor;

    // Schnittwiederholungen
    // wss: Kommentar in XML Schema: Maschinenabh�ngig: Messer Schnitt wiederholungen (Sinnvollerweise immer 0)
    edCutRetries.AsInteger        := GroupValueDef[cXPCutRetriesItem, 0];

    // --- Anlage ---
    // --------------
    // Geschwindigkeit (Enabled wird durch FPattern bestimmt)
    edDefaultSpeed.Value             := GroupValueDef[cXPSpeedItem, 1];
    // Anlaufdauer
    edDefaultSpeedRamp.Value         := GroupValueDef[cXPSpeedRampItem, 1];
    // DFS Delta
    edDFSDelta.Value                 := GroupValueDef[cXPDeltaItem, 0];
    // Alarme
    cbBlockAtAlarm.Checked           := GetXPBoolean(cXPBlockAtAlarmItem);
    cbBlockAtYarnCountAlarm.Checked  := GetXPBoolean(cXPBlockAtYarnCountAlarmItem);
    cbBlockAtShortCountAlarm.Checked := GetXPBoolean(cXPBlockAtShortCountAlarmItem);
    cbBlockAtClusterAlarm.Checked    := GetXPBoolean(cXPBlockAtClusterAlarmItem);
    cbBlockAtSFIAlarm.Checked        := GetXPBoolean(cXPBlockAtSFIAlarmItem);
    cbBlockAtFAlarm.Checked          := GetXPBoolean(cXPBlockAtAlarmItem);
    cbBlockAtFClusterAlarm.Checked   := GetXPBoolean(cXPBlockAtFClusterAlarmItem);
    cbBlockAtCutFailureAlarm.Checked := GetXPBoolean(cXPBlockAtCutFailureItem);
  //  cbBlockAtAlarm.Checked           := cbBlockAtFAlarm.Checked or cbBlockAtCutFailureAlarm.Checked or cbBlockAtClusterAlarm.Checked or
  //                                   cbBlockAtYarnCountAlarm.Checked or cbBlockAtSFIAlarm.Checked or cbBlockAtFClusterAlarm.Checked;
    cbShortSFID.Checked              := GetXPBoolean(cXPShortSFIDItem);
    cbSHZeroCut.Checked              := GetXPBoolean(cXPSHZeroCutItem);

    cbSuckOffLastCutOnly.Checked     := GetXPBoolean(cXPSuckOffLastCutOnlyItem);

    // --- Maschine ---
    // ----------------
    // Maschinenkopf
    if MachValueDef[cXPHeadstockRightItem, False] then cobHeadstock.ItemIndex := 1
                                                  else cobHeadstock.ItemIndex := 0;
    // Messertrennleistung
    if MachValueDef[cXPKnifeCutOffPowerHighItem, False] then cobKnifePower.ItemIndex := 1
                                                        else cobKnifePower.ItemIndex := 0;
    // Splice Check Length / Spleisspr�fl�nge
    case fMachineType of
      mtMurata7_2, mtMurata7_5, mtMurata21C: xInt := 50;
      mtAC338, mtAC5:                        xInt := 35;     //Nue:21.02.08 AC5 added
    else
      xInt := 25;
    end;
    edSPCheckLength.Value         := MachValueDef[cXPCheckLengthItem, xInt];
    // LongSTopDef
    edLongStopDef.Value           := MachValueDef[cXPLongStopDefItem, 1];

    // Oberfadenpr�fung
    cbUpperYarnCheck.Checked      := MachValueDef[cXPUpperYarnCheckItem, False];
    // Erweiterte Murata Schnittstelle
    cbExtMurItf.Checked           := MachValueDef[cXPExtendedInterfaceMurataItem, False];
    cbConeChangeCondition.Checked := MachValueDef[cXPConeChangeConditionActiveItem, False];
    // Speedsimulation (Enabled wird durch FPattern bestimmt)
    cbOneDrumPuls.Checked         := MachValueDef[cXPSpeedSimulationItem, False];

    laFrontmodel.Caption          := cGUIFrontTypeNames[GetFrontTypeValue(MachValueDef[cXPYMTypeItem, cFrontTypeNames[ftNone]])];
    laZESwVersionStr.Caption      := MachValueDef[cXPYMSwVersionItem, ''];
    // Software Option wird neu aus dem FPattern gebildet
    if Assigned(fMaConfig) then
      laZESwOptionStr.Caption     := GetSWOptionSetNames(fMaConfig.GroupNode[fActiveGroup, cXPFPatternNode])
    else
      laZESwOptionStr.Caption     := cGUISWOptionNames[swoNone];

    // Am Schluss noch Abh�ngigkeiten der Komponenten visualisieren
    CheckComponents;
  end;  
end;

//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.DoMachNodeChanged;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;
//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.FillSensingHeadList;
var
  xSh: TSensingHead;
begin
  cobSensingHead.Clear;
  for xSh:=shTK830 to High(TSensingHead) do begin
    if (xSH <> shTK940BD) AND
//       ( (fMachineType in [mtGeneric, mtAC338]) or
       ( ((fMachineType = mtGeneric) or (FNetType in [ntWSC, ntLX])) or //Nue:6.2.06
         not(xSH in [shTKZenit, shTKZenitF, shTKZenitFP,shTKZenitC, shTKZenitCF, shTKZenitCFP]) ) then
      if not ((fMachineType = mtAC5) and (xSH<shTKZenit)) then //Nue:05.03.08  Keine SpectraTastk�pfe an AC5 m�glich
      cobSensingHead.Items.AddObject(cGUISensingHeadNames[xSh], Pointer(xSh));
  end;
  cobSensingHead.DropDownCount := cobSensingHead.Items.Count;

end;

//------------------------------------------------------------------------------
procedure TGBMachConfigBox.Init;
begin
  if not(csDesigning in ComponentState) then
    InitComboAndEditBoxes;
end;

//------------------------------------------------------------------------------
procedure TGBMachConfigBox.Notification(aComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
end;

procedure TGBMachConfigBox.SetMachineType(const aValue: TMachineType);
begin
  if fMachineType <> aValue then begin
    fMachineType := aValue;
    FillSensingHeadList;
    ValueUpdate;
//    CheckComponents;
  end;
end;

procedure TGBMachConfigBox.SetNetType(const Value: TNetTyp);
begin
  //Nue:6.2.06
  FNetType := Value;
  if Value=ntLX then
    cobSensingHead.Enabled := False;
end;

//:-----------------------------------------------------------------------------
procedure TGBMachConfigBox.UpdateComponent;
begin
  // vorl�ufig noch ben�tigt da im Interface INavChange so deklariert
end;

//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.UpdateMachConfigBox;
begin
  if Assigned(fMaConfig) and fMaConfig.Available then begin
    mDoChangedEnabled := False;

    ValueUpdate;

    mDoChangedEnabled := True;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGBMachConfigBox.ValueSave(aTag: Integer);
  //.........................................................
  procedure ClearZenitSpectraBits;
//  var
//    xSHC: TSensingHeadClass;
  begin
//    xSHC := GetSensingHeadClass(GetSensingHeadValue(GroupValueDef[cXPSensingHeadItem, cSensingHeadNames[shTK830]]));
    if pnBlocksZenit.Visible then begin
//    if xSHC in [shcZenit..shcZenitFP] then begin
      // Wenn Zenit, dann SpectraPlus Bits l�schen
      fMaConfig.GroupValue[fActiveGroup, cXPCutBeforeAdjustItem]         := False;

      fMaConfig.GroupValue[fActiveGroup, cXPBlockAtYarnCountAlarmItem]   := False;
      fMaConfig.GroupValue[fActiveGroup, cXPBlockAtShortCountAlarmItem]  := False;
      fMaConfig.GroupValue[fActiveGroup, cXPBlockAtClusterAlarmItem]     := False;
      fMaConfig.GroupValue[fActiveGroup, cXPBlockAtSFIAlarmItem]         := False;
      fMaConfig.GroupValue[fActiveGroup, cXPBlockAtAlarmItem]            := False;
      fMaConfig.GroupValue[fActiveGroup, cXPBlockAtFClusterAlarmItem]    := False;

      fMaConfig.GroupValue[fActiveGroup, cXPExtendedInterfaceMurataItem] := False;

      if mTKSpectra then begin
      //Felder der LZE, welche aber bei Spectra ausgeschaltet werden m�ssen. Nue
        fMaConfig.GroupValue[fActiveGroup, cXPSuckOffLastCutOnlyItem]    := False;
      end; //if
    end
    else begin
      // Wenn SpectraPlus, dann Zenit Bits l�schen
      fMaConfig.GroupValue[fActiveGroup, cXPBlockAtAlarmItem] := False;
    end;
  end;
  //.........................................................
begin
  if mDoChangedEnabled then begin
    case aTag of
      // Garnreiniger
      //TODO wss: Beim Umschalten Zenit <> nonZenit entsprechend doppelt belegte ConfigBits gegenseitig l�schen
      1: begin
          fMaConfig.GroupValue[fActiveGroup, cXPSensingHeadItem] := cSensingHeadNames[TSensingHead(cobSensingHead.Items.Objects[cobSensingHead.ItemIndex])];
          ClearZenitSpectraBits;
        end;
      2:  fMaConfig.GroupValue[fActiveGroup, cXPCutBeforeAdjustItem]        := cbAdjustCut.Checked;
      3:  fMaConfig.GroupValue[fActiveGroup, cXPRemoveYarnAfterAdjustItem]  := cbAdjustRemove.Checked;
      4:  fMaConfig.GroupValue[fActiveGroup, cXPZeroTestItem]               := cbZeroAdjust.Checked;
      5:  fMaConfig.GroupValue[fActiveGroup, cXPDriftCompensationItem]      := cbDriftConeChange.Checked;
      6:  fMaConfig.GroupValue[fActiveGroup, cXPKnifeMonitorItem]           := cbKnifeMonitor.Checked;
      7:  fMaConfig.GroupValue[fActiveGroup, cXPCutRetriesItem]             := edCutRetries.AsInteger; // CutRetries: Default immer 0
      // Anlage
      8:  fMaConfig.MachValue[cXPHeadstockRightItem]                        := (cobHeadstock.ItemIndex > 0);
      9: begin
          // wenn L�nge gr�sser ist als 127cm wird im Handler die Umsetzung mit dem LongSpliceCheckLength ConfigBit gemacht
          // 0-127cm: False und Wert wird so �bernommen
          // >127cm:  True und Wert wird mit 32 dividiert.
          // Hier wird sichergestellt, dass nur ganze 32er L�ngen gespeichert werden, wenn die L�nge > 127cm betr�gt 
          if edSPCheckLength.Value > cSpliceCheckLongLength then
            fMaConfig.MachValue[cXPCheckLengthItem] := (edSPCheckLength.AsInteger div cSpliceCheckModValue) * cSpliceCheckModValue
          else
            fMaConfig.MachValue[cXPCheckLengthItem] := edSPCheckLength.AsInteger;
        end;
      10: fMaConfig.GroupValue[fActiveGroup, cXPSpeedItem]                  := edDefaultSpeed.Value;
      11: fMaConfig.GroupValue[fActiveGroup, cXPSpeedRampItem]              := edDefaultSpeedRamp.Value;
      25: fMaConfig.GroupValue[fActiveGroup, cXPDeltaItem]                  := edDFSDelta.Value;
      // Alarme
      12: fMaConfig.GroupValue[fActiveGroup, cXPBlockAtAlarmItem]           := cbBlockAtAlarm.Checked;
      13: fMaConfig.GroupValue[fActiveGroup, cXPBlockAtYarnCountAlarmItem]  := cbBlockAtYarnCountAlarm.Checked;
      26: fMaConfig.GroupValue[fActiveGroup, cXPBlockAtShortCountAlarmItem] := cbBlockAtShortCountAlarm.Checked;
      14: fMaConfig.GroupValue[fActiveGroup, cXPBlockAtClusterAlarmItem]    := cbBlockAtClusterAlarm.Checked;
      15: fMaConfig.GroupValue[fActiveGroup, cXPBlockAtSFIAlarmItem]        := cbBlockAtSFIAlarm.Checked;
      16: fMaConfig.GroupValue[fActiveGroup, cXPBlockAtAlarmItem]           := cbBlockAtFAlarm.Checked;
      17: fMaConfig.GroupValue[fActiveGroup, cXPBlockAtFClusterAlarmItem]   := cbBlockAtFClusterAlarm.Checked;
      18: fMaConfig.GroupValue[fActiveGroup, cXPBlockAtCutFailureItem]      := cbBlockAtCutFailureAlarm.Checked;

      27: fMaConfig.GroupValue[fActiveGroup, cXPShortSFIDItem]              := cbShortSFID.Checked;
      28: fMaConfig.GroupValue[fActiveGroup, cXPSHZeroCutItem]              := cbSHZeroCut.Checked;
      30: fMaConfig.GroupValue[fActiveGroup, cXPSuckOffLastCutOnlyItem]     := cbSuckOffLastCutOnly.Checked;

      // Maschine
      19: fMaConfig.MachValue[cXPUpperYarnCheckItem]                        := cbUpperYarnCheck.Checked;
      20: fMaConfig.MachValue[cXPExtendedInterfaceMurataItem]               := cbExtMurItf.Checked;
      21: fMaConfig.MachValue[cXPSpeedSimulationItem]                       := cbOneDrumPuls.Checked;
      22: fMaConfig.MachValue[cXPConeChangeConditionActiveItem]             := cbConeChangeCondition.Checked;
      23: fMaConfig.MachValue[cXPLongStopDefItem]                           := edLongStopDef.Value;
      24: fMaConfig.MachValue[cXPKnifeCutOffPowerHighItem]                  := (cobKnifePower.ItemIndex > 0);
    else
    end;
    // nun noch eventuelle Abh�ngigkeiten wieder neu visualisieren
    ValueUpdate;
//    CheckComponents;
    // Noch den Event nach Aussen benachrichtigen
    DoMachNodeChanged;
  end;
end;

end.
