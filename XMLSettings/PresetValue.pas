(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: PresetValue.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: PresetValue supports a Value with additional preset table.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 13.03.2000  0.00  Kr  | Initial Release
| 19.05.2005  0.01  Wss | Gewisse Properties Datentypen zu Extended gewechselt
|=============================================================================*)
unit PresetValue;

interface

uses Classes, SysUtils, IvDictio;

type
  //...........................................................................
  TTextType = (ttText, ttCmdDisabled, ttFloat, ttEmpty);

  TPresetTextRec = record
    Typ: TTextType;
    Text: string;
  end;
  
  IPresetValue = interface(IUnknown)
    ['{5E05DEF1-C9F2-4FAD-99D1-4377DFF2077E}']
    function GetAsFloat: Double;
    function GetHintText: string;
    function GetTextType: TTextType;
    function GetValueText: string;
    procedure SetAsFloat(const Value: Double);
    procedure SetHintText(const aValue: string);
    procedure SetTextType(const Value: TTextType);
    procedure SetValueText(const aValue: string);
    property AsFloat: Double read GetAsFloat write SetAsFloat;
    property HintText: string read GetHintText write SetHintText;
    property TextType: TTextType read GetTextType write SetTextType;
    property ValueText: string read GetValueText write SetValueText;
  end;
  

  PValueSpecRec = ^TValueSpecRec;
  TValueSpecRec = record
    minValue: Double;
    maxValue: Double;
    Space: Integer;
    Factor: Single;
    Format: string;
    Measure: string;
    Caption: string;
    Hint: string;
    XPValue: string;
    XPSwitch: string;
  end;
  
  PValueSpecArray = ^TValueSpecRec;
  TValueSpecArray = Array of TValueSpecRec;

  TPresetValue = class(TObject)
  private
    fFactor: Extended;
    fMaxValue: Extended;
    fMinValue: Extended;
    fTextType: TTextType;
    fXPath: string;
    mInterface: IPresetValue;
    mPresetTbl: array of TPresetTextRec;
    mValue: Double;
    mValueFormat: string;
    function GetHint: string;
    function GetHintText: string;
    function GetText: string;
    function GetValue: Extended;
    function GetValueText: string;
    procedure SetHint(const aValue: string);
    procedure SetHintText(const aValue: string);
    procedure SetText(const aValue: string);
    procedure SetTextType(const aValue: TTextType);
    procedure SetValue(const aNewValue: Extended);
    procedure SetValueText(const aValue: string);
    property Hint: string read GetHint write SetHint;
    property Text: string read GetText write SetText;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AttachInterface(aInterface: IPresetValue);
    function CheckValue(aNewValue: Double): Double;
    function FindHotkeyText(aKey: Char): Boolean;
    procedure LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec);
    procedure ReadValueFromText;
    procedure SelectNext;
    procedure SelectPrevious;
    property Factor: Extended read fFactor write fFactor;
    property MaxValue: Extended read fMaxValue write fMaxValue;
    property MinValue: Extended read fMinValue write fMinValue;
    property TextType: TTextType read fTextType write SetTextType;
    property Value: Extended read GetValue write SetValue;
    property XPath: string read fXPath write fXPath;
  end;
  

//------------------------------------------------------------------------------
//  Functions and procedures
//------------------------------------------------------------------------------
procedure CircularIncrement(var aIndex: Integer; aMaxIndex: Integer);
procedure CircularDecrement(var aIndex: Integer; aMaxIndex: Integer);

implementation
uses
  mmMBCS, LoepfeGlobal;

//------------------------------------------------------------------------------

procedure CircularIncrement(var aIndex: Integer; aMaxIndex: Integer);
begin
  if aIndex < aMaxIndex then Inc(aIndex)
  else aIndex := 0;
end;
//------------------------------------------------------------------------------

procedure CircularDecrement(var aIndex: Integer; aMaxIndex: Integer);
begin
  if aIndex > 0 then Dec(aIndex)
  else aIndex := aMaxIndex;
end;

{*
Verwaltet die Grenzbereiche, die Preset-Values wenn Up/Down gedr�ckt wird und den Formatstring.
Damit diese Verwaltung nur 1x implementiert werden muss, wird per Interface kommuniziert, da im Printable Modus nicht die KeyEditField sondern nur
TPrintableValue Komponenten verwendet wird, welche die gleichen Informationen ebenfalls ben�tigen.
}
{*
Verwaltet die Grenzbereiche, die Preset-Values wenn Up/Down gedr�ckt wird und den Formatstring.
Damit diese Verwaltung nur 1x implementiert werden muss, wird per Interface kommuniziert, da im Printable Modus nicht die KeyEditField sondern nur
TPrintableValue Komponenten verwendet wird, welche die gleichen Informationen ebenfalls ben�tigen.
}
//:---------------------------------------------------------------------------
//:--- Class: TPresetValue
//:---------------------------------------------------------------------------
{* Verwaltet die Grenzbereiche, die Preset-Values wenn Up/Down gedr�ckt wird und den Formatstring.
   Damit diese Verwaltung nur 1x implementiert werden muss, wird per Interface kommuniziert, da im Printable Modus nicht die KeyEditField sondern nur
   TPrintableValue Komponenten verwendet wird, welche die gleichen Informationen ebenfalls ben�tigen.
*}
constructor TPresetValue.Create;
begin
  inherited Create;
  
  mInterface   := nil;
  mValueFormat := '%4.2f';
  
  MinValue     := 0;
  MaxValue     := 0;
  fTextType    := ttFloat;
  Value        := 0;
  //  TextType := ttFloat;
  //  Text := '0';
  //  Value := 0;
  
end;

//:---------------------------------------------------------------------------
destructor TPresetValue.Destroy;
begin
  
  Finalize(mPresetTbl);
  inherited;
end;

//:---------------------------------------------------------------------------
procedure TPresetValue.AttachInterface(aInterface: IPresetValue);
begin
  mInterface := aInterface;
end;

//:---------------------------------------------------------------------------
{* Pr�ft die Grenzwerte anhand der Min/Max Werte vom �bergebenen Konstantenarray
*}
function TPresetValue.CheckValue(aNewValue: Double): Double;
begin
  Result := aNewValue;
  if (fMaxValue > fMinValue) then begin
    if aNewValue < fMinValue then
      Result := fMinValue
    else if aNewValue > fMaxValue then
      Result := fMaxValue;
  end;
end;

//:---------------------------------------------------------------------------
function TPresetValue.FindHotkeyText(aKey: Char): Boolean;
var
  i: Integer;
  xStr: string;
  
  function FindAcceleratorChar(aText: string): char;
  var
    xPos: Integer;
  begin
    xPos := Pos('&', aText);
    if xPos <> 0 then begin
      if (xPos + 1) <= Length(aText) then
        Result := aText[xPos + 1]
      else
        Result := #0;
    end
    else
      Result := #0;
  end;
  
begin
  Result := False;
  
  for i:=0 to High(mPresetTbl) do begin
    if (mPresetTbl[i].Typ <> ttFloat) then begin
      xStr := Translate(mPresetTbl[i].Text); // '()&Aus' -> '&Off'
      if UpCase(aKey) = UpCase(FindAcceleratorChar(xStr)) then begin
        Text      := StringReplace(xStr, '&', '', [rfReplaceAll]);
        fTextType := mPresetTbl[i].typ;
        Result    := True;
        Break;
      end; // if
    end; //if
  end; // for
end;

//:---------------------------------------------------------------------------
function TPresetValue.GetHint: string;
begin
  if Assigned(mInterface) then
    Result := mInterface.HintText;
end;

//:---------------------------------------------------------------------------
function TPresetValue.GetHintText: string;
begin
  Result := Hint;
end;

//:---------------------------------------------------------------------------
function TPresetValue.GetText: string;
begin
  if Assigned(mInterface) then
    Result := mInterface.GetValueText;
end;

//:---------------------------------------------------------------------------
function TPresetValue.GetValue: Extended;
begin
  if TextType = ttFloat then begin
    if Text <> '' then begin
      try
        Text := StringReplace(Text, ' ', '', [rfReplaceAll]);
        Result := MMStrToFloat(Text);
    //        Result := StrToFloat(Text) / fFactor;
      except
        Result := fMinValue;
      end;

    end else
      Result := FMinValue;
  end
  else
    Result := mValue;
  Result := CheckValue(Result)
end;

//:---------------------------------------------------------------------------
function TPresetValue.GetValueText: string;
begin
  Result := Text;
end;

//:---------------------------------------------------------------------------
procedure TPresetValue.LoadValueSpec(aValueSpec: TValueSpecRec; aPresetTbl: array of TPresetTextRec);
var
  i: Integer;
begin
    // Value Specifications
  fMinValue := aValueSpec.MinValue;
  fMaxValue := aValueSpec.MaxValue;
  fFactor   := aValueSpec.Factor;
  mValueFormat := aValueSpec.Format;
  if aValueSpec.Hint <> '' then begin
    Hint := Format(Translate(aValueSpec.Hint) + mValueFormat + '..' + mValueFormat + ' %s',
                   [MinValue, MaxValue, aValueSpec.Measure]);
  end;
  
  
  // Preset Table
  SetLength(mPresetTbl, Length(aPresetTbl));
  for i := 0 to High(mPresetTbl) do begin
    mPresetTbl[i] := aPresetTbl[i];
      // Am Schluss noch den Hotkey Text anh�ngen
    if mPresetTbl[i].Typ = ttCmdDisabled then
      Hint := Hint + '; ' + StringReplace(Translate(mPresetTbl[i].Text), '&', '', [rfReplaceAll]);
  end;
  
  with mPresetTbl[0] do begin
    if Typ = ttFloat then
      Text := StringReplace(Text, '.', DecimalSeparator, [rfReplaceAll])
    else
      Text := Text;
    TextType := Typ;
  end;
end;

//:---------------------------------------------------------------------------
procedure TPresetValue.ReadValueFromText;
begin
  if fTextType = ttFloat then
    mValue := MMStrToFloat(mInterface.ValueText);
end;

//:---------------------------------------------------------------------------
{* Sucht und selektiert nach dem n�chst h�heren Vorgabewert aus der Preset Liste
*}
procedure TPresetValue.SelectNext;
var
  i: Integer;
  xValue1, xValue2: Integer;
begin
  // Es muss mit Integer gepr�ft werden, da bei Floatzahlen die Genauigkeit einen
  // Strich durch die Rechnung macht, weil z.B. der Wert 4.5 intern als 4.4999999
  // ist und beim pr�fen ich wieder den Vorgabewert 4.5 nehmen w�rde 
  if Assigned(mPresetTbl) then begin
    i       := 0;
    xValue1 := Round(mValue * 100);
    while i <= High(mPresetTbl) do begin
      with mPresetTbl[i] do begin
        if Typ = ttFloat then begin
          xValue2 := Round(MMStrToFloat(Text) * 100);
            // wenn aktueller Wert keine Zahl ist (Off), dann nimm einfach den ersten Indexwert
  //          if (xSingle > mValue) or (fTextType <> ttFloat) then begin
          if (xValue2 > xValue1) or (fTextType <> ttFloat) then begin
            Value := xValue2 / 100;
            Break;
          end;
        end
        else begin
          fTextType := Typ;
          Self.Text := StringReplace(Translate(Text), '&', '', [rfReplaceAll]);
          Break;
        end;
      end; // with
      inc(i);
    end; // while i >= 0
  
        // wir sind am oberen Ende der Liste angelangt -> Daten vom kleinsten Index setzen
    if i > High(mPresetTbl) then begin
      with mPresetTbl[0] do begin
        fTextType := Typ;
        if Typ = ttFloat then
           Value := MMStrToFloat(Text)
        else
          Self.Text := StringReplace(Translate(Text), '&', '', [rfReplaceAll]);
      end; // with
    end; // if i
  end; // if Assigned
  
    //  if Assigned(mPresetTbl) then begin
    //    i := 0;
    //    if fTextType = ttFloat then begin
    //      // suche nach dem n�chst h�heren Wert in der Vorgabeliste
    //      while (i <= High(mPresetTbl)) do begin
    //        with mPresetTbl[i] do begin
    //          if Typ = ttFloat then begin
    //            xSingle := MMStrToFloat(Text);
    //            if xSingle > mValue then begin
    //              Value := xSingle;
    //              Break;
    //            end;
    //          end
    //          else begin
    //            // am Oberen Ende angekommen
    //            fTextType := Typ;
    //            if fTextType = ttFloat then
    //               Value := MMStrToFloat(Text)
    //            else
    //              Self.Text := StringReplace(Translate(Text), '&', '', [rfReplaceAll]);
    //            Break;
    //          end;
    //        end;
    //        inc(i);
    //      end;
    //    end
    //    else begin
    //      // Wert war keine Zahl -> erster Wert setzen
    //      mValue   := MMStrToFloat(mPresetTbl[i].Text);
    //      TextType := ttFloat;
    //    end;
    //  end;
end;

//:---------------------------------------------------------------------------
{* Sucht und selektiert nach dem n�chst kleineren Vorgabewert aus der Preset Liste
*}
procedure TPresetValue.SelectPrevious;
var
  i: Integer;
  xValue1, xValue2: Integer;
begin
  if Assigned(mPresetTbl) then begin
    i       := High(mPresetTbl);
    xValue1 := Round(mValue * 100);
    while i >= 0 do begin
      with mPresetTbl[i] do begin
        if Typ = ttFloat then begin
  //          xSingle := MMStrToFloat(Text);
  //          if (xSingle < mValue) or (fTextType <> ttFloat) then begin
  //            Value := xSingle;
          xValue2 := Round(MMStrToFloat(Text) * 100);
          if (xValue2 < xValue1) or (fTextType <> ttFloat) then begin
            Value := xValue2 / 100;
            Break;
          end;
        end;
      end; // with
      dec(i);
    end; // while i >= 0
  
      // wir sind am unteren Ende der Liste angelangt -> Daten vom h�chsten Index setzen
    if i < 0 then begin
      with mPresetTbl[High(mPresetTbl)] do begin
        fTextType := Typ;
        if Typ = ttFloat then
           Value := MMStrToFloat(Text)
        else
          Self.Text := StringReplace(Translate(Text), '&', '', [rfReplaceAll]);
      end; // with
    end; // if i
  end; // if Assigned
end;

//:---------------------------------------------------------------------------
procedure TPresetValue.SetHint(const aValue: string);
begin
  if Assigned(mInterface) then
    mInterface.HintText := aValue;
  //    mInterface.SetValueHint(aValue);
end;

//:---------------------------------------------------------------------------
procedure TPresetValue.SetHintText(const aValue: string);
begin
  Hint := aValue;
end;

//:---------------------------------------------------------------------------
procedure TPresetValue.SetText(const aValue: string);
begin
  if Assigned(mInterface) then
    mInterface.SetValueText(aValue);
end;

//:---------------------------------------------------------------------------
procedure TPresetValue.SetTextType(const aValue: TTextType);
var
  i: Integer;
begin
  if aValue = ttFloat then begin
    if fTextType <> aValue then begin
      fTextType := aValue;
  //      mValue    := CheckValue(mValue);
      Text      := Format(mValueFormat, [mValue]);
    end;
  end
  else begin
    // find a possibly preset text
    if Assigned(mPresetTbl) then
      for i := 0 to High(mPresetTbl) do
        if mPresetTbl[i].Typ = aValue then begin
          fTextType := mPresetTbl[i].Typ;
          Text      := StringReplace(Translate(mPresetTbl[i].Text), '&', '', [rfReplaceAll]);
        end;
  end;

//  if fTextType <> aValue then begin
//    if aValue = ttFloat then begin
//      fTextType := aValue;
//  //      mValue    := CheckValue(mValue);
//      Text      := Format(mValueFormat, [mValue]);
//    end
//    else begin
//        // find a possibly preset text
//      if Assigned(mPresetTbl) then
//        for i := 0 to High(mPresetTbl) do
//          if mPresetTbl[i].Typ = aValue then begin
//            Text := StringReplace(Translate(mPresetTbl[i].Text), '&', '', [rfReplaceAll]);
//            fTextType := mPresetTbl[i].Typ;
//          end;
//    end;
//  end;
end;

//:---------------------------------------------------------------------------
procedure TPresetValue.SetValue(const aNewValue: Extended);
begin
  if TextType <> ttEmpty then begin
    if TextType <> ttFloat then
      TextType := ttFloat;
    Text := Format(mValueFormat, [aNewValue]);
  //    Text := Format(mValueFormat, [CheckValue(aNewValue) * fFactor]);
  end;
  mValue := aNewValue;
end;

//:---------------------------------------------------------------------------
procedure TPresetValue.SetValueText(const aValue: string);
begin
  Text := aValue;
end;

end.
