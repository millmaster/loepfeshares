(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: CurveLayer.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.05.1999  0.00  Kr  | Initial Release
| 19.12.2003  0.01  Kr  | resourcestring modified (Umlaute entfernt)
|       2005  0.01  Wss | Umbau nach XML
|=========================================================================================*)
unit CurveLayer;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, ExtCtrls, QualityMatrixBase,
  QualityMatrixDef, YMParaDef, QualityMatrixDesc, YMParaUtils, Syncobjs, XMLDef,
  XMLSettingsModel, VCLXMLSettingsModel;

const
  cMaxSamples = 1;

  cChannelCurve = 0;
  cSpliceCurve = 1;
  cClusterCurve = 2;
  cMouseTargetThreshold = 4;
  //...........................................................................

resourcestring
  rsQMChannelHint = '(*)Kanal:'; // ivlm
  rsQMClusterHint = '(*)Cluster:'; // ivlm
  rsQMSpliceHint = '(*)Spleiss:'; // ivlm

  rsNepsHint = '(*)Noppen'; //ivlm
  rsShortHint = '(*)Kurzfehler';  //ivlm
  rsLongHint = '(*)Langfehler'; //ivlm
  rsThinHint = '(*)Duennstellen'; //ivlm


  //...........................................................................

type
  //...........................................................................

  TCurveTypes = (cvtNSL, cvtThin, cvtNSLSplice, cvtThinSplice,
    cvtNSLCluster, cvtThinCluster);
  TChannelTypes = (ctShort, ctLong, ctThin, ctNeps, ctUnknown);
  TChannels = set of TChannelTypes;

  TFactorsRec = record
    AProduct: Single;
    FxDia: Single;
  end;
  
  TXPRec = record
    NSw: string;
    NDia: string;
    SSw: string;
    SDia: string;
    SLen: string;
    LSw: string;
    LDia: string;
    LLen: string;
    TSw: string;
    TDia: string;
    TLen: string;
  end;
  
  TOnTouchChannel = procedure (aChannelType: TChannelTypes) of object;
  TChannelFormula = class(TObject)
  private
    fChannelSettings: TChannelSettingsRec;
    mFactors: array[ctShort..ctThin] of TFactorsRec;
    function ChannelParaChanged(aPara: TChannelSettingsRec): Boolean;
    function GetChannel(aIndex: TChannelTypes): Boolean;
    function GetChannelPara: TChannelSettingsRec;
    procedure PutChannelPara(aSettings: TChannelSettingsRec);
  protected
    procedure ComputeFactors;
    property LongChannel: Boolean index ctLong read GetChannel;
    property NepsChannel: Boolean index ctNeps read GetChannel;
    property ShortChannel: Boolean index ctShort read GetChannel;
    property ThinChannel: Boolean index ctThin read GetChannel;
  public
    constructor Create;
    function ComputeDiameter(aChannel: TChannelTypes; aLength: Single): Single;
    property ChannelSettings: TChannelSettingsRec read GetChannelPara write PutChannelPara;
  end;
  

  TCurveControlRec = record
    TotNSLSamples: Integer;
    TotThinSamples: Integer;
    NepRange: TRangeRec;
    ShortRange: TRangeRec;
    LongRange: TRangeRec;
    ThinRange: TRangeRec;
    XSectI: Integer;
    YSectI: Integer;
    SectHome: TPoint;
    XSection: TScaleRec;
    YSection: TScaleRec;
    VisibleRange: TSingleRect;
    NSLSamples: array of TPoint;
    ThinSamples: array of TPoint;
    Color: TColor;
  end;
  

  TClassFieldCurveRec = record
    Field: Integer;
    SubFields: string;
  end;
  
  PTClassFieldCurveRec = ^TClassFieldCurveRec;

  TCutCurveList = class(TList)
  private
    fSubFieldX: Integer;
    fSubFieldY: Integer;
    function GetItems(Index: Integer): TClassFieldCurveRec;
    procedure SetItems(Index: Integer; aValue: TClassFieldCurveRec);
  public
    destructor Destroy; override;
    function Add(aClassFieldCurve: TClassFieldCurveRec): Integer;
    procedure Clear; override;
    property Items[Index: Integer]: TClassFieldCurveRec read GetItems write SetItems;
    property SubFieldX: Integer read fSubFieldX;
    property SubFieldY: Integer read fSubFieldY;
  end;
  

  TChannelCurve = class(TChannelFormula)
  private
    fController: TBaseGUIController;
    fCurveColor: TColor;
    fCurveStyle: TPenStyle;
    fCurveVisible: Boolean;
    fCurveWidth: Integer;
    fEnabledChannels: TChannels;
    fHitChannel: TChannelTypes;
    fOnParameterChange: TOnParameterChange;
    fOnReleaseChannel: TOnTouchChannel;
    fParameterSource: TParameterSource;
    fSelectableChannels: TChannels;
    fSelectedChannel: TChannelTypes;
    mCalculationActive: Boolean;
    mControl: TCurveControlRec;
    mCriticalSection: TCriticalSection;
    mCutCurve: TCutCurveList;
    mDragRoot: TPoint;
    mDragRootSettings: TChannelSettingsRec;
    mHasExtCluster: Boolean;
    mQualityMatrixBase: TQualityMatrixBase;
    mXP: TXPRec;
    procedure AdjustChannel(const aDragTarget: TPoint);
    procedure CheckComponents;
    function CheckHit(aX, aY: Integer; aRange: TRangeRec; aSamples: array of TPoint): Boolean;
    procedure ComputeNSLCurve;
    procedure ComputeThinCurve;
    procedure GenerateCutCurve(const aSamples: array of TPoint; const aTotSamples: Integer; const aReverse: Boolean);
    function GetChannelPara: TChannelSettingsRec;
    function HitCurve(aX, aY: Integer): TChannelTypes;
    procedure PaintCurve(aSamples: array of TPoint; aTotSamples: Integer; aRange: TRangeRec; aHit: Boolean); overload;
    procedure PutChannelPara(aSettings: TChannelSettingsRec);
    procedure PutHitChannel(const aValue: TChannelTypes);
    procedure PutSelectableChannels(const aValue: TChannels);
    procedure PutSelectedChannel(const aValue: TChannelTypes);
    procedure SetChannelPara(aSettings: TChannelSettingsRec);
    procedure SetController(aValue: TBaseGUIController);
    procedure SetParameterSource(aValue: TParameterSource);
    procedure InitSampleBuffer(var aArr: array of TPoint);
  protected
    function EnterCalculation: Boolean;
    procedure LeaveCalculation;
    procedure ValueUpdate(Sender: TBaseXMLSettingsObserver);
  public
    constructor Create(aQualityMatrixBase: TQualityMatrixBase); reintroduce;
    destructor Destroy; override;
    procedure CustomMouseDown(aShift: TShiftState; aX, aY: Integer);
    procedure CustomMouseMove(aShift: TShiftState; aX, aY: Integer);
    procedure CustomMouseUp(aShift: TShiftState; aX, aY: Integer);
    procedure DoValueUpdate;
    function GetCutCurve(const aCurveType: TCurveTypes): TCutCurveList;
    procedure PaintCurve; overload;
    procedure SetSampleBufferLength;
    property Controller: TBaseGUIController read fController write SetController;
    property HitChannel: TChannelTypes read fHitChannel write PutHitChannel;
    property OnReleaseChannel: TOnTouchChannel read fOnReleaseChannel write fOnReleaseChannel;
    property SelectedChannel: TChannelTypes read fSelectedChannel write PutSelectedChannel;
  published
    property ChannelParameter: TChannelSettingsRec read GetChannelPara write PutChannelPara;
    property CurveColor: TColor read fCurveColor write fCurveColor;
    property CurveStyle: TPenStyle read fCurveStyle write fCurveStyle;
    property CurveVisible: Boolean read fCurveVisible write fCurveVisible;
    property CurveWidth: Integer read fCurveWidth write fCurveWidth;
    property EnabledChannels: TChannels read fEnabledChannels write fEnabledChannels;
    property OnParameterChange: TOnParameterChange read fOnParameterChange write fOnParameterChange;
    property ParameterSource: TParameterSource read fParameterSource write SetParameterSource;
    property SelectableChannels: TChannels read fSelectableChannels write PutSelectableChannels;
  end;
  

  TCurveLayer = class(TObject)
  private
    fImageStored: Boolean;
    fModel: TVCLXMLSettingsModel;
    fOnParameterChange: TOnParameterChange;
    fSelectCurve: Boolean;
    mChannel: TChannelCurve;
    mCluster: TChannelCurve;
    mImage: TImage;
    mQualityMatrixBase: TQualityMatrixBase;
    mSplice: TChannelCurve;
    procedure DoParameterChange(aSource: TParameterSource);
    procedure DoReleaseChannel(aChannelType: TChannelTypes);
    function GetCurveColor(const aIndex: Integer): TColor;
    function GetCurveStyle(const aIndex: Integer): TPenStyle;
    function GetCurveVisible(const aIndex: Integer): Boolean;
    procedure PutCurveColor(const aIndex: Integer; const aValue: TColor);
    procedure PutCurveStyle(const aIndex: Integer; const aValue: TPenStyle);
    procedure PutCurveVisible(const aIndex: Integer; const aValue: Boolean);
    procedure SetModel(const aValue: TVCLXMLSettingsModel);
    procedure SetSelectCurve(const aValue: Boolean);
  public
    constructor Create(aQualityMatrixBase: TQualityMatrixBase); reintroduce;
    destructor Destroy; override;
    procedure CustomMouseDown(aShift: TShiftState; aX, aY: Integer);
    procedure CustomMouseMove(aShift: TShiftState; aX, aY: Integer);
    procedure CustomMouseUp(aShift: TShiftState; aX, aY: Integer);
    procedure CustomPaint;
    procedure CustomResize;
    procedure DoValueUpdate;
    function GetChannelPara: TChannelSettingsRec;
    function GetClusterDiameter: Single;
    function GetCutCurve(const aCurveType: TCurveTypes): TCutCurveList;
    function GetSplicePara: TChannelSettingsRec;
    procedure PaintImage;
    procedure SetDefaultColor;
    procedure SetSampleBufferLength;
    procedure StoreImage;
  published
    property ChannelColor: TColor index cChannelCurve read GetCurveColor write PutCurveColor;
    property ChannelStyle: TPenStyle index cChannelCurve read GetCurveStyle write PutCurveStyle;
    property ChannelVisible: Boolean index cChannelCurve read GetCurveVisible write PutCurveVisible;
    property ClusterColor: TColor index cClusterCurve read GetCurveColor write PutCurveColor;
    property ClusterStyle: TPenStyle index cClusterCurve read GetCurveStyle write PutCurveStyle;
    property ClusterVisible: Boolean index cClusterCurve read GetCurveVisible write PutCurveVisible;
    property ImageStored: Boolean read fImageStored write fImageStored;
    property Model: TVCLXMLSettingsModel read fModel write SetModel;
    property OnParameterChange: TOnParameterChange read fOnParameterChange write fOnParameterChange;
    property SelectCurve: Boolean read fSelectCurve write SetSelectCurve;
    property SpliceColor: TColor index cSpliceCurve read GetCurveColor write PutCurveColor;
    property SpliceStyle: TPenStyle index cSpliceCurve read GetCurveStyle write PutCurveStyle;
    property SpliceVisible: Boolean index cSpliceCurve read GetCurveVisible write PutCurveVisible;
  end;
  

//------------------------------------------------------------------------------
// Channel Formula factors and definitions
//------------------------------------------------------------------------------
  //...........................................................................

const
// Diameter offlimit
  cDiaOfflimit = 10.01; // visibleNSLTRange.Top*100+1;
// Short factors
  cKS = 3.5; //350;
  cFS = 0.92; //92;
  cGS = 0.4; //40;
// Long factors
  cKL = 3.0; //300;
  cFL = 0.98; //98;
  cGL = 0.3; //30;
// Thin factors
  cKT = 1.7; //170;
  cFT = 1.02; //102;
//wss  cGT = 100;
// Invisible Pixcel
  cInvisiblePixcel = High(Integer);


  cChannelXPDef: TXPRec = (
    NSw:  cXPChannelNepsSwitchItem;
    NDia: cXPChannelNepsDiaItem;
    SSw:  cXPChannelShortSwitchItem;
    SDia: cXPChannelShortDiaItem;
    SLen: cXPChannelShortLengthItem;
    LSw:  cXPChannelLongSwitchItem;
    LDia: cXPChannelLongDiaItem;
    LLen: cXPChannelLongLengthItem;
    TSw:  cXPChannelThinSwitchItem;
    TDia: cXPChannelThinDiaItem;
    TLen: cXPChannelThinLengthItem;
  );

  cClusterXPDefSpectra: TXPRec = (
    NSw:  cXPChannelNepsSwitchItem;   // ev. Leerstring da f�r Neps kein Cluster vorhanden
    NDia: cXPChannelNepsDiaItem;
    SSw:  cXPClusterShortSwitchItem;
    SDia: cXPClusterShortDiaItem;
    SLen: cXPChannelShortLengthItem;
    LSw:  cXPChannelLongSwitchItem;
    LDia: cXPChannelLongDiaItem;
    LLen: cXPChannelLongLengthItem;
    TSw:  cXPChannelThinSwitchItem;
    TDia: cXPChannelThinDiaItem;
    TLen: cXPChannelThinLengthItem;
  );

  cClusterXPDefSpectraPlus: TXPRec = (
    NSw:  cXPChannelNepsSwitchItem;   // ev. Leerstring da f�r Neps kein Cluster vorhanden
    NDia: cXPChannelNepsDiaItem;
    SSw:  cXPClusterShortSwitchItem;
    SDia: cXPClusterShortDiaItem;
    SLen: cXPClusterShortLengthItem;
    LSw:  cXPClusterLongSwitchItem;
    LDia: cXPClusterLongDiaItem;
    LLen: cXPClusterLongLengthItem;
    TSw:  cXPClusterThinSwitchItem;
    TDia: cXPClusterThinDiaItem;
    TLen: cXPClusterThinLengthItem;
  );

  cSpliceXPDef: TXPRec = (
    NSw:  cXPSpliceNepsSwitchItem;
    NDia: cXPSpliceNepsDiaItem;
    SSw:  cXPSpliceShortSwitchItem;
    SDia: cXPSpliceShortDiaItem;
    SLen: cXPSpliceShortLengthItem;
    LSw:  cXPSpliceLongSwitchItem;
    LDia: cXPSpliceLongDiaItem;
    LLen: cXPSpliceLongLengthItem;
    TSw:  cXPSpliceThinSwitchItem;
    TDia: cXPSpliceThinDiaItem;
    TLen: cXPSpliceThinLengthItem;
  );

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmcs, typinfo;
//------------------------------------------------------------------------------
{
const
  cTestSettings: TChannelSettingsRec = (
    neps: (dia: 600; sw: cChOn);
    short: (dia: 280; sw: cChOn);
    shortLen: 12;
    long: (dia: 115; sw: cChOn);
    longLen: 270;
    thin: (dia: 80; sw: cChOn);
    thinLen: 38;
    splice: (len: 200; sw: cChOn);
    checkLen: 250;
    spare: 0
    );
{}
//------------------------------------------------------------------------------

//:---------------------------------------------------------------------------
//:--- Class: TChannelFormula
//:---------------------------------------------------------------------------
constructor TChannelFormula.Create;
begin
  inherited create;
  //%%
//  ChannelSettings := cTopSixSettings;
  //  ChannelSettings := cTestSettings;
end;

//:---------------------------------------------------------------------------
//1 Pr�ft ob die Parameter im Argument gegen�ber der lokalen ge�ndert wurden. 
{* -
*}
function TChannelFormula.ChannelParaChanged(aPara: TChannelSettingsRec): Boolean;
begin
  with fChannelSettings do
  begin
    Result := (neps.sw <> aPara.neps.sw)   or (neps.dia <> aPara.neps.dia) or
  
              (short.sw <> aPara.short.sw) or (short.dia <> aPara.short.dia) or
              (shortLen <> aPara.shortLen) or
  
              (long.sw <> aPara.long.sw)   or (long.dia <> aPara.long.dia) or
              (longLen <> aPara.longLen)   or
  
              (thin.sw <> aPara.thin.sw)   or (thin.dia <> aPara.thin.dia) or
              (thinLen <> aPara.thinLen);
  end;
end;

//:---------------------------------------------------------------------------
function TChannelFormula.ComputeDiameter(aChannel: TChannelTypes; aLength: Single): Single;
begin
  case aChannel of
    ctNeps: begin
        with fChannelSettings do
          if neps.sw = swOn then
            Result := neps.dia
          else
            Result := cDiaOfflimit;
      end;
    ctShort, ctLong, ctThin: begin
        with mFactors[aChannel] do
          if aLength <> 0 then
            // Hier ist die Formel calcDia = (A / calcLen) + (F * Dia)
            // wobei df = FxDia = (F*Dia) ist und ap = A = AProduct = Wert von ComputeFactors
            Result := (AProduct / aLength) + FxDia
          else if mFactors[ctLong].FxDia <> 0 then
            Result := cDiaOfflimit
          else
            Result := cDiaOfflimit + 1;
  //       with mFactors[aChannel] do
  //         if aLength <> 0 then
  //           // Hier ist die Formel calcDia = (A / calcLen) + (F * Dia)
  //           // wobei df = (F*Dia) = FxDia ist und ap = A = AProduct = Wert von ComputeFactors
  //           Result := df + ap div aLength
  //         else if xDf <> 0 then
  //           Result := cDiaOfflimit
  //         else
  //           Result := cDiaOfflimit + 1;
      end;
  else
    Result := cDiaOfflimit;
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelFormula.ComputeFactors;
begin
  with fChannelSettings do begin
    // Short curve factor set
    with mFactors[ctShort] do
      if short.sw = swOn then begin
        FxDia    := cFS * short.dia;
        AProduct := (cKS * (FxDia-1) + 1 - FxDia) * shortLen * cGS;
      end
      else begin
        FxDia    := cDiaOfflimit;
        AProduct := 0;
      end;
  
    // Long curve factor set
    with mFactors[ctLong] do
      if long.sw = swOn then begin
        FxDia    := cFL * long.dia;
        AProduct := (cKL * (FxDia-1) + 1 - FxDia) * longLen * cGL;
      end
      else begin
        FxDia    := cDiaOfflimit;
        AProduct := 0;
      end;
  
    // Thin curve factor set
    with mFactors[ctThin] do
      if thin.sw = swOn then begin
        FxDia    := cFT * thin.dia;
        AProduct := -((-cKT * (FxDia-1) - 1 + FxDia) * thinLen);
      end
      else begin
        FxDia    := cDiaOfflimit;
        AProduct := 0;
      end;
  end;
end;

//:---------------------------------------------------------------------------
function TChannelFormula.GetChannel(aIndex: TChannelTypes): Boolean;
begin
  with fChannelSettings do
    case aIndex of
      ctNeps:  Result := (neps.sw  = swOn);
      ctShort: Result := (short.sw = swOn);
      ctLong:  Result := (long.sw  = swOn);
      ctThin:  Result := (thin.sw  = swOn);
    else
      Result := False;
    end;
end;

//:---------------------------------------------------------------------------
function TChannelFormula.GetChannelPara: TChannelSettingsRec;
begin
  Result := fChannelSettings;
end;

//:---------------------------------------------------------------------------
procedure TChannelFormula.PutChannelPara(aSettings: TChannelSettingsRec);
begin
  //  TYMSettingsUtils.ValidateChannelSettings(aSettings);
  
  fChannelSettings := aSettings;
  ComputeFactors;
end;

//:---------------------------------------------------------------------------
//:--- Class: TCurveLayer
//:---------------------------------------------------------------------------
//1 Pr�ft ob die Parameter im Argument gegen�ber der lokalen ge�ndert wurden. 
{* -
*}
constructor TCurveLayer.Create(aQualityMatrixBase: TQualityMatrixBase);
begin
  inherited Create;
  
  fOnParameterChange := nil;
  
  mQualityMatrixBase := aQualityMatrixBase;

  mImage := TImage.Create(mQualityMatrixBase);
  ImageStored := False;

  SelectCurve := True;

  mCluster := TChannelCurve.Create(mQualityMatrixBase);
  mChannel := TChannelCurve.Create(mQualityMatrixBase);
  mSplice  := TChannelCurve.Create(mQualityMatrixBase);

  SetDefaultColor;

  mChannel.Controller                 := TChannelController.Create;
  mChannel.Controller.ChangeKinds     := [ckChannel];
  mChannel.Controller.ListenToKindSet := [ckChannel, ckCluster];
  mChannel.ParameterSource            := psChannel;
  mChannel.OnParameterChange          := DoParameterChange;
  mChannel.OnReleaseChannel           := DoReleaseChannel;
  mChannel.CurveWidth                 := cChannelCurvePen.width;
  ChannelStyle                        := cChannelCurvePen.style;
  ChannelVisible                      := True;

  mCluster.Controller                 := TClusterController.Create;
  mCluster.Controller.ChangeKinds     := [ckCluster];
  mCluster.Controller.ListenToKindSet := [ckChannel, ckCluster];
  mCluster.ParameterSource            := psCluster;
  mCluster.OnParameterChange          := DoParameterChange;
  mCluster.OnReleaseChannel           := DoReleaseChannel;
  mCluster.CurveWidth                 := cClusterCurvePen.width;
  ClusterStyle                        := cClusterCurvePen.style;
  mCluster.EnabledChannels            := [ctNeps, ctShort, ctLong, ctThin];
  ClusterVisible                      := True;

  mSplice.Controller                  := TSpliceController.Create;
  mSplice.Controller.ChangeKinds      := [ckSplice];
  mSplice.Controller.ListenToKindSet  := [ckSplice];
  mSplice.ParameterSource             := psSplice;
  mSplice.OnParameterChange           := DoParameterChange;
  mSplice.OnReleaseChannel            := DoReleaseChannel;
  mSplice.CurveWidth                  := cSpliceCurvePen.width;
  SpliceStyle                         := cSpliceCurvePen.style;
  SpliceVisible                       := True;
end;

//:---------------------------------------------------------------------------
destructor TCurveLayer.Destroy;
begin
  FreeAndNil(mCluster);
  FreeAndNil(mChannel);
  FreeAndNil(mSplice);
  FreeAndNil(mImage);
  
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.CustomMouseDown(aShift: TShiftState; aX, aY: Integer);
begin
  
  if SelectCurve then
  begin
  
    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
      mChannel.CustomMouseDown(aShift, aX, aY);
  
    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) then
      mCluster.CustomMouseDown(aShift, aX, aY);
  
    if (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
      mSplice.CustomMouseDown(aShift, aX, aY);
  end;
  
end;

//:---------------------------------------------------------------------------
{* F�r mChannel wird gepr�ft, ob die mSplice und mCluster Kurve NICHT getroffen oder selektiert sind. Wenn dies der Fall ist, dann wird der MouseEvent an
   mChannel weitergereicht.
   
   Pendant dazu bei mCluster (mit mSplice, mChannel) und mSplice (mit mChannel, mCluster)
*}
procedure TCurveLayer.CustomMouseMove(aShift: TShiftState; aX, aY: Integer);
begin
  
  if SelectCurve then
  begin
  
    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
    begin
      if mChannel.HitChannel = ctUnknown then
        mQualityMatrixBase.Hint := rsQMChannelHint;
      mChannel.CustomMouseMove(aShift, aX, aY);
    end;
  
    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) then
    begin
      if mCluster.HitChannel = ctUnknown then
        mQualityMatrixBase.Hint := rsQMClusterHint;
      mCluster.CustomMouseMove(aShift, aX, aY);
    end;
  
    if (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
    begin
      if mSplice.HitChannel = ctUnknown then
        mQualityMatrixBase.Hint := rsQmSpliceHint;
      mSplice.CustomMouseMove(aShift, aX, aY);
    end;
  end;
  
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.CustomMouseUp(aShift: TShiftState; aX, aY: Integer);
begin
  
  if SelectCurve then
  begin
  
    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
      mChannel.CustomMouseUp(aShift, aX, aY);
  
    if (mSplice.HitChannel = ctUnknown) and (mSplice.SelectedChannel = ctUnknown) and
      (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) then
      mCluster.CustomMouseUp(aShift, aX, aY);
  
    if (mChannel.HitChannel = ctUnknown) and (mChannel.SelectedChannel = ctUnknown) and
      (mCluster.HitChannel = ctUnknown) and (mCluster.SelectedChannel = ctUnknown) then
      mSplice.CustomMouseUp(aShift, aX, aY);
  end;
  
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.CustomPaint;
begin
  
  if mChannel.HitChannel <> ctUnknown then
  begin
  
    if not ImageStored then
    begin
  
      mSplice.PaintCurve;
      mCluster.PaintCurve;
  
      StoreImage;
      ImageStored := True;
    end;
  
    mChannel.PaintCurve;
  end
  else if mCluster.HitChannel <> ctUnknown then
  begin
  
    if not ImageStored then
    begin
  
      mSplice.PaintCurve;
      mChannel.PaintCurve;
  
      StoreImage;
      ImageStored := True;
    end;
  
    mCluster.PaintCurve;
  
  end
  else if mSplice.HitChannel <> ctUnknown then
  begin
  
    if not ImageStored then
    begin
  
      mCluster.PaintCurve;
      mChannel.PaintCurve;
  
      StoreImage;
      ImageStored := True;
    end;
  
    mSplice.PaintCurve;
  
  end
  else
  begin
  
    mSplice.PaintCurve;
    mCluster.PaintCurve;
    mChannel.PaintCurve;
  end;
  
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.CustomResize;
begin
  mChannel.ComputeThinCurve;
  mChannel.ComputeNSLCurve;
  mCluster.ComputeThinCurve;
  mCluster.ComputeNSLCurve;
  mSplice.ComputeThinCurve;
  mSplice.ComputeNSLCurve;
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.DoParameterChange(aSource: TParameterSource);
begin
  if Assigned(fOnParameterChange) then
    fOnParameterChange(aSource);
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.DoReleaseChannel(aChannelType: TChannelTypes);
begin
  
  if ImageStored then
    ImageStored := False;
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.DoValueUpdate;
begin
//  mChannel.DoValueUpdate;
//  mCluster.DoValueUpdate;
//  mSplice.DoValueUpdate;
end;

//:---------------------------------------------------------------------------
function TCurveLayer.GetChannelPara: TChannelSettingsRec;
begin
  Result := mChannel.ChannelParameter;
end;

//:---------------------------------------------------------------------------
function TCurveLayer.GetClusterDiameter: Single;
begin
  Result := mCluster.ChannelParameter.short.dia;
end;

//:---------------------------------------------------------------------------
function TCurveLayer.GetCurveColor(const aIndex: Integer): TColor;
begin
  Result := clSilver;
  case aIndex of
  
    cChannelCurve:
      Result := mChannel.CurveColor;
  
    cClusterCurve:
      Result := mCluster.CurveColor;
  
    cSpliceCurve:
      Result := mSplice.CurveColor;
  end;
end;

//:---------------------------------------------------------------------------
function TCurveLayer.GetCurveStyle(const aIndex: Integer): TPenStyle;
begin
  Result := psSolid;
  case aIndex of
  
    cChannelCurve:
      Result := mChannel.CurveStyle;
  
    cClusterCurve:
      Result := mCluster.CurveStyle;
  
    cSpliceCurve:
      Result := mSplice.CurveStyle;
  end;
  
end;

//:---------------------------------------------------------------------------
function TCurveLayer.GetCurveVisible(const aIndex: Integer): Boolean;
begin
  Result := False;
  case aIndex of
  
    cChannelCurve:
      Result := mChannel.CurveVisible;
  
    cClusterCurve:
      Result := mCluster.CurveVisible;
  
    cSpliceCurve:
      Result := mSplice.CurveVisible;
  end;
  
end;

//:---------------------------------------------------------------------------
function TCurveLayer.GetCutCurve(const aCurveType: TCurveTypes): TCutCurveList;
begin
  Result := nil;
  
  case aCurveType of
  
    cvtNSL, cvtThin:
      Result := mChannel.GetCutCurve(aCurveType);
  
    cvtNSLSplice:
      Result := mSplice.GetCutCurve(cvtNSL);
  
    cvtThinSplice:
      Result := mSplice.GetCutCurve(cvtThin);
  
    cvtNSLCluster:
      Result := mCluster.GetCutCurve(cvtNSL);
  
    cvtThinCluster:
      Result := mCluster.GetCutCurve(cvtThin);
  end;
end;

//:---------------------------------------------------------------------------
function TCurveLayer.GetSplicePara: TChannelSettingsRec;
begin
  Result := mSplice.ChannelParameter;
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.PaintImage;
var
  xClientRect: TRect;
begin
  with mQualityMatrixBase do
  begin
    xClientRect := Rect(0, 0, ClientWidth, ClientHeight);
  // Temporary for Release because Curve draging is not finished yet
  //    Canvas.CopyRect(xClientRect, mImage.Canvas, xClientRect);
  
    Canvas.CopyRect(xClientRect, mImage.Canvas, xClientRect);
  end;
  
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.PutCurveColor(const aIndex: Integer; const aValue: TColor);
begin
  case aIndex of
  
    cChannelCurve:
      mChannel.CurveColor := aValue;
  
    cClusterCurve:
      mCluster.CurveColor := aValue;
  
    cSpliceCurve:
      mSplice.CurveColor := aValue;
  end;
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.PutCurveStyle(const aIndex: Integer; const aValue: TPenStyle);
begin
  case aIndex of
  
    cChannelCurve:
      mChannel.CurveStyle := aValue;
  
    cClusterCurve:
      mCluster.CurveStyle := aValue;
  
    cSpliceCurve:
      mSplice.CurveStyle := aValue;
  end;
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.PutCurveVisible(const aIndex: Integer; const aValue: Boolean);
begin
  case aIndex of
  
    cChannelCurve:
      mChannel.CurveVisible := aValue;
  
    cClusterCurve:
      mCluster.CurveVisible := aValue;
  
    cSpliceCurve:
      mSplice.CurveVisible := aValue;
  end;
  
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.SetDefaultColor;
begin
  
  ChannelColor := cChannelCurvePen.color;
  ClusterColor := cClusterCurvePen.color;
  SpliceColor := cSpliceCurvePen.color;
  
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  if fModel <> aValue then begin
    // Wenn vorher schon ein Model dran war, erst die Observer unregistrieren
    if Assigned(fModel) then begin
      fModel.UnregisterObserver(mChannel.Controller);
      fModel.UnregisterObserver(mSplice.Controller);
      fModel.UnregisterObserver(mCluster.Controller);
    end;

    fModel := aValue;

    // Ein neues Model wird verlinkt -> die Observer registrieren lassen
    // Durch das Registrieren wird vom Model �ber den Observer ein ValueUpdate ausgel�st
    if Assigned(fModel) then begin
      fModel.RegisterObserver(mChannel.Controller);
      fModel.RegisterObserver(mSplice.Controller);
      fModel.RegisterObserver(mCluster.Controller);
      mCluster.ParameterSource := psCluster;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.SetSampleBufferLength;
begin
  mChannel.SetSampleBufferLength;
  mCluster.SetSampleBufferLength;
  mSplice.SetSampleBufferLength;
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.SetSelectCurve(const aValue: Boolean);
begin
  fSelectCurve := aValue;
  mQualityMatrixBase.DoubleBuffered := aValue;
end;

//:---------------------------------------------------------------------------
procedure TCurveLayer.StoreImage;
var
  xClientRect: TRect;
begin
  with mQualityMatrixBase do
  begin
    xClientRect := Rect(0, 0, ClientWidth, ClientHeight);
  
    mImage.Width := Width;
    mImage.Height := Height;
  
    mImage.Width := ClientWidth;
    mImage.Height := ClientHeight;
    mImage.Canvas.CopyRect(xClientRect, Canvas, xClientRect);
  end;
  
end;

//:---------------------------------------------------------------------------
//:--- Class: TChannelCurve
//:---------------------------------------------------------------------------
{* F�r mChannel wird gepr�ft, ob die mSplice und mCluster Kurve NICHT getroffen oder selektiert sind. Wenn dies der Fall ist, dann wird der MouseEvent an
   mChannel weitergereicht.
   
   Pendant dazu bei mCluster (mit mSplice, mChannel) und mSplice (mit mChannel, mCluster)
*}
constructor TChannelCurve.Create(aQualityMatrixBase: TQualityMatrixBase);
begin
  inherited Create;
  
  fController        := Nil;
  fHitChannel        := ctUnknown;
  fOnParameterChange := nil;
  fOnReleaseChannel  := nil;
  fParameterSource   := psUnknown;
  fSelectedChannel   := ctUnknown;
  
  mCalculationActive := False;
  mDragRoot.x        := 0;
  mDragRoot.y        := 0;
  
  
  mQualityMatrixBase := aQualityMatrixBase;
  
  mQualityMatrixBase.SubFieldX := 0;
  mQualityMatrixBase.SubFieldY := 0;

  mControl.nepRange.start   := mQualityMatrixBase.TotSamples + 1;
  mControl.nepRange.stop    := mQualityMatrixBase.TotSamples + 1;
  mControl.shortRange.start := mQualityMatrixBase.TotSamples + 1;
  mControl.shortRange.stop  := mQualityMatrixBase.TotSamples + 1;
  mControl.longRange.start  := mQualityMatrixBase.TotSamples + 1;
  mControl.longRange.stop   := mQualityMatrixBase.TotSamples + 1;
  mControl.thinRange.start  := mQualityMatrixBase.TotSamples + 1;
  mControl.thinRange.stop   := mQualityMatrixBase.TotSamples + 1;

  EnabledChannels := [ctShort, ctLong, ctThin, ctNeps];
  SelectableChannels := EnabledChannels;

  //  SetSampleBufferLength;

  SetLength(mControl.NSLSamples, mQualityMatrixBase.TotSamples + 1);
  SetLength(mControl.thinSamples, mQualityMatrixBase.TotSamples + 1);

  mCutCurve := TCutCurveList.Create;

  mCriticalSection := TCriticalSection.Create;
  //:...................................................................
  //:...................................................................
end;

//:---------------------------------------------------------------------------
destructor TChannelCurve.Destroy;
begin
  FreeAndNil(fController);
  //:...................................................................
  Finalize(mControl.NSLSamples);
  Finalize(mControl.thinSamples);
  
  mCutCurve.Free;
  
  mCriticalSection.Free;
  
  inherited;
end;

//:---------------------------------------------------------------------------
{* Pixelbewegungen von der Maus werden in Dia und Len Parameter umgerechnet.
   mDragRoot Koordinaten und mDragRootSettings wird abgef�llt beim MouseDown. Mausbewegungen werden immer relativ von den MouseDown Koordinaten verrechnet.
   Zugleich wird jedoch noch die Grenzwerte gepr�ft, ob sich die Maus innerhalb des sichtbaren Bereichs befindet.
*}
procedure TChannelCurve.AdjustChannel(const aDragTarget: TPoint);
var
  xLen, xDia: Single;
  
  // Sichert die Einhaltung von Grenzbereichen falls die Maus ausserhalb des sichtbaren Breichs f�hrt
  procedure CalculateChannelPara(aGraphField: TRect; var aDia, aLen: Single);
  var
    xBorder: Integer;
  begin
    with aGraphField do begin
      // Berechnung des Durchmesser
      if aDragTarget.y < Top then
        // Mauskoordinate oberhalb vom sichtbaren Bereich?
        aDia := cDiaMax
      else if aDragTarget.y > Bottom then
        // Mauskoordinate unterhalb vom sichtbaren Bereich?
        aDia := cDiaMin
      else begin
        // Thin Kurve anderst pr�fen
        if SelectedChannel = ctThin then begin
          if mDragRoot.y <= Top then mDragRoot.y := Top + 1;
          xBorder := Top;
        end else begin
          if mDragRoot.y >= Bottom then mDragRoot.y := Bottom - 1;
          xBorder := Bottom;
        end;
        // Aus dem Delta der Mausbewegung zum Klickpunkt wird der neue Parameter ermittelt
        aDia := 1.00 + ( ( ((mDragRoot.y - aDragTarget.y)/(xBorder - mDragRoot.y)) + 1.0) * (aDia - 1.0) );
      end;
  
      // Berechnung der L�nge
      if aDragTarget.x < Left then
        aLen := cLenMin
      else if aDragTarget.x > Right then
        aLen := cLenMax
      else begin
        if mDragRoot.x <= Left then
          mDragRoot.x := Left + 1;
        // Aus dem Delta der Mausbewegung zum Klickpunkt wird der neue Parameter ermittelt
        aLen := (((aDragTarget.x - mDragRoot.x)/(mDragRoot.x - Left)) + 1.0) * aLen;
      end;
    end;
  end;
  
begin
  case SelectedChannel of
    ctNeps: begin
        xDia := mDragRootSettings.neps.dia;
          // Pr�fe und korrigiere die Parameter im Grenzbereich
        CalculateChannelPara(mQualityMatrixBase.NSLGraphField, xDia, xLen);
          // eventuell korrigierte Parameter �bernehmen, ansonsten sind es die gleichen
        xDia := FitValue(xDia, cNepsDiaMin, cNepsDiaMax);
        fController.Change(mXP.NDia, xDia);
      end;

    ctShort: begin
        xDia := mDragRootSettings.short.dia;
        xLen := mDragRootSettings.shortLen;
          // Pr�fe und korrigiere die Parameter im Grenzbereich
        CalculateChannelPara(mQualityMatrixBase.NSLGraphField, xDia, xLen);
          // eventuell korrigierte Parameter �bernehmen, ansonsten sind es die gleichen
        xDia := FitValue(xDia, cShortDiaMin, cShortDiaMax);
        xLen := FitValue(xLen, cShortLenMin, cShortLenMax);
        if mHasExtCluster or (fParameterSource <> psCluster) then
          fController.Value[mXP.SLen] := xLen;
        fController.Change(mXP.SDia, xDia);
      end;

    ctLong: begin
        xDia := mDragRootSettings.long.dia;
        xLen := mDragRootSettings.longLen;
          // Pr�fe und korrigiere die Parameter im Grenzbereich
        CalculateChannelPara(mQualityMatrixBase.NSLGraphField, xDia, xLen);
          // eventuell korrigierte Parameter �bernehmen, ansonsten sind es die gleichen
        xDia := FitValue(xDia, cLongDiaMin, cLongDiaMax);
        xLen := FitValue(xLen, cLongLenMin, cLongLenMax);
        fController.Value[mXP.LLen] := xLen;
        fController.Change(mXP.LDia, xDia);
      end;

    ctThin: begin
        xDia := mDragRootSettings.thin.dia;
        xLen := mDragRootSettings.thinLen;
          // Pr�fe und korrigiere die Parameter im Grenzbereich
        CalculateChannelPara(mQualityMatrixBase.ThinGraphField, xDia, xLen);
          // eventuell korrigierte Parameter �bernehmen, ansonsten sind es die gleichen
        xLen := FitValue(xLen, cThinLenMin, cThinLenMax);

        if xLen >= cThinLenThreshold then xDia := FitValue(xDia, cThinDiaMin, cThinDiaMaxAbove)
                                     else xDia := FitValue(xDia, cThinDiaMin, cThinDiaMaxBelow);

        fController.Value[mXP.TLen] := xLen;
        fController.Change(mXP.TDia, xDia);
      end;
  else
  end;

{
  // Haben die Parameter ge�ndert?
  if ChannelParaChanged(xSettings) then begin
      // Jupp -> �bernehmen...
    SetChannelPara(xSettings);
      //...und Event ausl�sen
  //    if Assigned(fOnParameterChange) then
  //      fOnParameterChange(ParameterSource);
  end;
{}
end;

//:-----------------------------------------------------------------------------
procedure TChannelCurve.CheckComponents;
begin
  if (fParameterSource = psCluster) then begin
    if Assigned(fController.Model) then
      mHasExtCluster := fController.Model.FPHandler.Value[cXPFP_ExtendedClusterItem]
    else
      mHasExtCluster := False;

    if mHasExtCluster then begin
      mXP := cClusterXPDefSpectraPlus;
      SelectableChannels := [ctShort, ctLong, ctThin];
    end
    else begin
      mXP := cClusterXPDefSpectra;
      SelectableChannels := [ctShort];
    end;
  end; // if fParameterSource = psCluster
end;

//:---------------------------------------------------------------------------
function TChannelCurve.CheckHit(aX, aY: Integer; aRange: TRangeRec; aSamples: array of TPoint): Boolean;
var
  i: Integer;
  xLow, xHigh: TPoint;
  
  procedure EvaluateLowHigh(aValue1, aValue2: Integer; var aLow, aHigh: Integer);
  begin
    if aValue1 < aValue2 then begin
      aLow  := aValue1;
      aHigh := aValue2;
    end
    else begin
      aLow  := aValue2;
      aHigh := aValue1;
    end;
  end;
  
begin
  Result := False;
  with mControl, aRange do begin
  
    if start < mQualityMatrixBase.TotSamples then begin
      EvaluateLowHigh(aSamples[start].x, aSamples[stop].x, xLow.x, xHigh.x);
      EvaluateLowHigh(aSamples[start].y, aSamples[stop].y, xLow.y, xHigh.y);
  
      if (aX >= xLow.x) and (aX <= xHigh.x) and
         (aY >= xLow.y - cMouseTargetThreshold) and (aY <= xHigh.y + cMouseTargetThreshold) then
      begin
        i := Start;
        while i < Stop do begin
          EvaluateLowHigh(aSamples[i].x, aSamples[i + 1].x, xLow.x, xHigh.x);
          EvaluateLowHigh(aSamples[i].y, aSamples[i + 1].y, xLow.y, xHigh.y);
  
          if (aX >= xLow.x) and (aX <= xHigh.x) and
             (aY >= xLow.y - cMouseTargetThreshold) and (aY <= xHigh.y + cMouseTargetThreshold) then
          begin
            Result := True;
            i      := stop;
          end else
            Inc(i);
        end;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.ComputeNSLCurve;
var
  xActXScale, xNextScale, xDeltaIncr: Single;
  i, xStepIndex: Integer;
  xLengthCM, xDia, xLowerDia: Single;
  xPixcelFactX, xPixcelFactY: Single;
  xNextXSection, xNextYSection: TScaleRec;
  xActiveChannel: TChannelTypes;
  xBreakFlag: Boolean;
  xGraphField: TRect;
begin
  if not EnterCalculation then
    Exit; // <============= mControl in use

  i := 0;
  InitSampleBuffer(mControl.NSLSamples);
  with mControl, mQualityMatrixBase do begin
    xActiveChannel   := ctUnknown;
    TotNSLSamples    := 0;
    NepRange.start   := TotSamples + 1;
    NepRange.stop    := TotSamples + 1;
    ShortRange.start := TotSamples + 1;
    ShortRange.stop  := TotSamples + 1;
    LongRange.start  := TotSamples + 1;
    LongRange.stop   := TotSamples + 1;

//    if ((ctNeps in EnabledChannels) and NepsChannel) OR
    if   ((ctShort in EnabledChannels) and ShortChannel) OR
       ((ctLong in EnabledChannels) and LongChannel) then
    begin
      XSectI        := 0;
      YSectI        := 0;
      // in Var kopieren da GraphField eine Funktion ist wo etwas berechnet wird
      xGraphField   := GraphField;
      SectHome.x    := xGraphField.Left;
      SectHome.y    := xGraphField.Top;
      XSection      := XScale[XSectI];
      YSection      := YScale[YSectI];
      xNextXSection := XScale[XSectI + 1];
      xNextYSection := YScale[YSectI + 1];
      VisibleRange  := VisibleNSLRange;

      xLowerDia := cDiaOfflimit;

      xBreakFlag := False;
      // MAINLOOP: Feld f�r Feld wird durchgegangen
      while (XSection.pixcelToNextScale <> 0) and not xBreakFlag do begin
        with mControl.XSection do begin
          // in value ist der X-Achsen Wert der St�tzpunkte abgelegt 0.5, 1.0, 1.5, 2.0, etc
          xActXScale := value;
          xNextScale := xNextXSection.value;
          // Length-Aufl�sung anhand der St�tzpunkte
          if xNextXSection.pixcelToNextScale <> 0 then
            xDeltaIncr := (xNextScale - xActXScale) / NumberOfSamples
          else begin
            xDeltaIncr := (xNextScale - xActXScale) / (NumberOfSamples - 1);
            // das Ende der X-Achse wird anhand vom letzten g�ltigen X-Wert 70.0 - 200.0 berechnet
            xNextScale := (NumberOfSamples * xDeltaIncr) + xActXScale + 0.5;
          end;
          // Anzahl Bildpunkte in diesem Feld zur totalen Arraygr�sse hinzuz�hlen
          TotNSLSamples := TotNSLSamples + NumberOfSamples;

          // precalculat x and y  pixcel factors
          if NumberOfSamples <> 0 then begin
            // Faktor f�r die relativen Pixelpositionen
            if xNextXSection.pixcelToNextScale <> 0 then
              xPixcelFactX := pixcelToNextScale / NumberOfSamples
            else
              xPixcelFactX := pixcelToNextScale / (NumberOfSamples - 1);
          end
          else
            xPixcelFactX := 1;

          // Pixelfaktor anhand der float Werten f�r die Y-Aufl�sung
          if YSection.pixcelToNextScale <> 0 then
            xPixcelFactY := (YSection.value - xNextYSection.value) / YSection.pixcelToNextScale
          else
            xPixcelFactY := 1;

          xStepIndex := 0;
          // Secondary loop: das einzelne Feld wird Anzahl der NumberOfSamples durchgegangen
          // Es wird Step by Step im faktorbehafteten X-Wert durchgegangen
          while xActXScale <= xNextScale - xDeltaIncr do begin
            // in xLengthCM wird jeweils das berechnete Delta mit NumberOfSamples inkrementiert
            xLengthCM := xActXScale;

            case xActiveChannel of
              ctUnknown: begin
                  // Nep curve
                  if ctNeps in EnabledChannels then begin
                    xDia := ComputeDiameter(ctNeps, xLengthCM);
                    if xDia < xLowerDia then begin
                      xLowerDia               := xDia;
                      xActiveChannel          := ctNeps;
                      mControl.NepRange.start := i;
                    end;
                  end;

                  // Short curve
                  if ctShort in EnabledChannels then begin
                    xDia := ComputeDiameter(ctShort, xLengthCM);
                    if xDia < xLowerDia then begin
                      xLowerDia                 := xDia;
                      xActiveChannel            := ctShort;
                      NepRange.start            := TotSamples + 1;
                      mControl.shortRange.start := i;
                    end;
                  end;
  
                  // Long curve
                  if ctLong in EnabledChannels then begin
                    xDia := ComputeDiameter(ctLong, xLengthCM);
                    if xDia < xLowerDia then begin
                      xLowerDia                 := xDia;
                      xActiveChannel            := ctLong;
                      mControl.NepRange.start   := TotSamples + 1;
                      mControl.shortRange.start := TotSamples + 1;
                      mControl.longRange.start  := i;
                    end;
                  end;
                end; // ctUnknown
  
              ctNeps: begin
                  // Nep curve
                  xLowerDia := ComputeDiameter(ctNeps, xLengthCM);
  
                  // Short curve
                  if ctShort in EnabledChannels then begin
                    xDia := ComputeDiameter(ctShort, xLengthCM);
                    if xDia < xLowerDia then begin
                      xLowerDia                 := xDia;
                      xActiveChannel            := ctShort;
                      mControl.shortRange.start := i;
                      mControl.NepRange.stop    := i;
                    end;
                  end;
  
                  // Long curve
                  if ctLong in EnabledChannels then begin
                    xDia := ComputeDiameter(ctLong, xLengthCM);
                    if xDia < xLowerDia then begin
                      xLowerDia                 := xDia;
                      xActiveChannel            := ctLong;
                      mControl.shortRange.start := TotSamples + 1;
                      mControl.longRange.start  := i;
                      mControl.NepRange.stop    := i;
                    end;
                  end;
                end; // ctNeps
  
              ctShort: begin
                  // Short curve
                  xLowerDia := ComputeDiameter(ctShort, xLengthCM);
  
                  // Long curve
                  if ctLong in EnabledChannels then begin
                    xDia := ComputeDiameter(ctLong, xLengthCM);
                    if xDia < xLowerDia then begin
                      xLowerDia       := xDia;
                      xActiveChannel  := ctLong;
                      longRange.start := i;
                      shortRange.stop := i;
                    end;
                  end;
                end; // ctShort
  
              ctLong:
                // Long curve
                xLowerDia := ComputeDiameter(ctLong, xLengthCM);
            else
            end; // case xActiveChannel
  
            //...................................................
            // Sodele, hier nun noch die xLengthCM und xLowerDia Werte in Pixel umrechnen
            //...................................................
            // X-Pixel = Linker Feldrand + (aktuellerSample * PixelFaktor)
            mControl.NSLSamples[i].x := SectHome.x + Round(xStepIndex * xPixcelFactX);

            if xLowerDia > YSection.value then
              NSLSamples[i].y := 0
            else if xLowerDia > xNextYSection.value then
              // Berechneter Dia-Wert ist
              NSLSamples[i].y := SectHome.y + Round((YSection.value - xLowerDia) / xPixcelFactY)
            else begin
              // Ann�herung von oben an das betroffene Feld in welchem der Dia vorhanden ist
              while (YSection.pixcelToNextScale <> 0) and
                    (xLowerDia <= xNextYSection.value) do
              begin
                SectHome.y := SectHome.y + YSection.pixcelToNextScale;
                Inc(YSectI);
                YSection   := YScale[YSectI];
  
                if YSection.pixcelToNextScale <> 0 then begin
                  xNextYSection := YScale[YSectI + 1];
                  xPixcelFactY  := (YSection.value - xNextYSection.value) / YSection.pixcelToNextScale;
                end;
              end; // while
            end; // if
  
            if (YSection.pixcelToNextScale = 0) or
               (xLowerDia <= VisibleRange.Bottom) or
               (xLengthCM > VisibleRange.Right) then
            begin
              // Kurve l�uft aus dem Rand aus (unten oder rechts)
              TotNSLSamples := i;
              xBreakFlag    := True; // Break main loop too!
              break;
            end
            else begin
              if ((xLowerDia <= YSection.value) and (xLowerDia > VisibleRange.Top)) OR
                 (xLengthCM < VisibleRange.Left) then
//                NSLSamples[i].y := cInvisiblePixcel
              else begin
                NSLSamples[i].y := SectHome.y + Round((YSection.value - xLowerDia) / xPixcelFactY);
//                NSLSamples[i + 1].y := cInvisiblePixcel;
              end;
            end;
  
            // inkrementieren des X-Wertes
            xActXScale := xActXScale + xDeltaIncr;
            Inc(i);
            Inc(xStepIndex);
          end; // SECONDLOOP: while xActXScale <= xNextScale - xDeltaIncr

          SectHome.x := SectHome.x + pixcelToNextScale;
        end; // with mControl.XSection do

        Inc(XSectI);
        XSection      := XScale[XSectI];
        xNextXSection := XScale[XSectI + 1];
      end; // MAINLOOP: while (XSection.pixcelToNextScale <> 0) and not xBreakFlag do begin

      // Letztes Inc wieder R�ckg�ngig damit es die richtige Anzahl der Samples entspricht
      if i > 0 then dec(i);
    end
    else
      TotNSLSamples := 0;

    case xActiveChannel of
      ctNeps:  mControl.NepRange.stop   := i;
      ctShort: mControl.shortRange.stop := i;
      ctLong:  mControl.longRange.stop  := i;
//      ctThin:  mControl.thinRange.stop  := i;
    end;
//    case xActiveChannel of
//      ctNeps:  mControl.NepRange.stop   := TotNSLSamples - 1;
//      ctShort: mControl.shortRange.stop := TotNSLSamples - 1;
//      ctLong:  mControl.longRange.stop  := TotNSLSamples - 1;
//      ctThin:  mControl.thinRange.stop  := TotNSLSamples - 1;
//    end;
  end;
  
  LeaveCalculation; // <================ mControl free
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.ComputeThinCurve;
var
  xActXScale, xPreviousScale, xDeltaIncr: Single;
  i, xStepIndex: Integer;
  xLengthCM, xLowerDia: Single;
  xPixcelFactX, xPixcelFactY: Single;
  previousXSection, xNextYSection: TScaleRec;
  xBreakFlag: Boolean;
  xGraphField: TRect;
  xActiveChannel: TChannelTypes;
begin

  if not EnterCalculation then
    Exit; // <============= mControl in use

  i := 0;
  InitSampleBuffer(mControl.ThinSamples);
  with mControl, mQualityMatrixBase do begin
    totThinSamples  := 0;
    thinRange.start := TotSamples + 1;
    thinRange.stop  := TotSamples + 1;
    xActiveChannel  := ctUnknown;
  
    if (ctThin in EnabledChannels) and ThinChannel then
    begin
  
      XSectI           := TotXScales - 1;
      YSectI           := 0;
        // in Var kopieren da GraphField eine Funktion ist wo etwas berechnet wird
      xGraphField      := GraphField;
      sectHome.x       := xGraphField.Right;
      sectHome.y       := xGraphField.Top;
      YSection         := YScale[YSectI];
      XSection         := XScale[XSectI];
      previousXSection := XScale[XSectI - 1];
      xNextYSection    := YScale[YSectI + 1];
      visibleRange     := VisibleTRange;
  
      xBreakFlag := False;
      // MAINLOOP: Feld f�r Feld wird durchgegangen
      while (XSectI > 0) and not xBreakFlag do
      try
        with mControl.XSection do begin
          xActXScale     := value;
          xPreviousScale := previousXSection.value;

          if XSectI <= 1 then
            xDeltaIncr := (xActXScale - xPreviousScale) / previousXSection.numberOfSamples
          else begin
            xDeltaIncr := (xActXScale - xPreviousScale) / (previousXSection.numberOfSamples - 1);
            xPreviousScale := xActXScale - (previousXSection.numberOfSamples * xDeltaIncr);
          end;
          totThinSamples := totThinSamples + previousXSection.numberOfSamples;

              // precalculat x and y  pixcel factors
          if previousXSection.numberOfSamples <> 0 then begin
            if previousXSection.pixcelToNextScale <> 0 then
              xPixcelFactX := previousXSection.pixcelToNextScale / previousXSection.numberOfSamples
            else
              xPixcelFactX := previousXSection.pixcelToNextScale / (previousXSection.numberOfSamples - 1);
          end
          else
            xPixcelFactX := 1;

          if YSection.pixcelToNextScale <> 0 then
            xPixcelFactY := (YSection.value - xNextYSection.value) / YSection.pixcelToNextScale
          else
            xPixcelFactY := 1;

          xStepIndex := 0;
          // Secondary loop: das einzelne Feld wird Anzahl der NumberOfSamples durchgegangen
          // Es wird Step by Step im faktorbehafteten X-Wert durchgegangen
          while xActXScale > xPreviousScale + xDeltaIncr do begin
            xLengthCM := xActXScale;

            case xActiveChannel of
              ctUnknown: begin
                      // Thin curve
                  xLowerDia := ComputeDiameter(ctThin, xLengthCM);
                  if xLowerDia <= YSection.value then begin
                    xActiveChannel           := ctThin;
                    i                        := 0;
                    mControl.thinRange.start := i;
                  end;
                end;
            else
                  // Thin curve
              xLowerDia := ComputeDiameter(ctThin, xLengthCM);
            end;
  
                // Convert to x and y pixcel
            mControl.thinSamples[i].x := sectHome.x - Round(xStepIndex * xPixcelFactX);
  
            if xLowerDia > YSection.value then
              thinSamples[i].y := 0
            else if xLowerDia > xNextYSection.value then
              thinSamples[i].y := sectHome.y + Round((YSection.value - xLowerDia) / xPixcelFactY)
            else begin
              while (YSection.pixcelToNextScale <> 0) and
                    (xLowerDia <= xNextYSection.value) do
              begin
                sectHome.y := sectHome.y + YSection.pixcelToNextScale;
                Inc(YSectI);
                YSection   := YScale[YSectI];
  
                if YSection.pixcelToNextScale <> 0 then begin
                  xNextYSection := YScale[YSectI + 1];
                  xPixcelFactY  := (YSection.value - xNextYSection.value) / YSection.pixcelToNextScale;
                end;
              end;
            end;
  
            if (YSection.pixcelToNextScale = 0) or
               (xLowerDia <= visibleRange.Bottom) or
               (xLengthCM < visibleRange.Left) then
            begin
              totThinSamples := i;
              xBreakFlag     := True; // Break main loop also!
              break;
            end
            else begin

              if ((xLowerDia <= YSection.value) and (xLowerDia > visibleRange.Top)) OR
                 (xLengthCM > visibleRange.Right) then
                thinSamples[i].y := 0 //cInvisiblePixcel
              else begin
                thinSamples[i].y     := sectHome.y + Round((YSection.value - xLowerDia) / xPixcelFactY);
//                thinSamples[i + 1].y := 0;
  //                thinSamples[i + 1].y := cInvisiblePixcel;
              end;
            end;

            xActXScale := xActXScale - xDeltaIncr;
            Inc(i);
            Inc(xStepIndex);
  //            if thinSamples[0].y = cInvisiblePixcel then
            if thinSamples[0].y = 0 then
              xActiveChannel := ctUnknown;
          end; // Second Loop
          sectHome.x := sectHome.x - previousXSection.pixcelToNextScale;
        end;
            // wir fiengen von rechts an, also ein Feld nach link den Index verkleinern
        Dec(XSectI);
        XSection := XScale[xSectI];
        if XSectI <> 0 then
          previousXSection := XScale[XSectI - 1];

      except
        on e:Exception do
          CodeSite.SendError('ComputeThinCurve: ' + e.Message);
      end; // Mainloop

      // Letztes Inc wieder R�ckg�ngig damit es die richtige Anzahl der Samples entspricht
      if i > 0 then dec(i);
    end else
      totThinSamples := 0;

    if xActiveChannel = ctThin then
      mControl.thinRange.stop := i;
//      mControl.thinRange.stop := totThinSamples - 1;

  end;

  LeaveCalculation; // <================ mControl free

  
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.CustomMouseDown(aShift: TShiftState; aX, aY: Integer);
begin
  if CurveVisible then begin
    if ([ssRight] <= aShift) and (HitChannel <> ctUnknown) and
       (SelectedChannel = ctUnknown) then
    begin
      SelectedChannel := HitChannel;
      mDragRoot.x := aX;
      mDragRoot.y := aY;
      mDragRootSettings := ChannelSettings;
    end
  end
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.CustomMouseMove(aShift: TShiftState; aX, aY: Integer);
var
  xHitChannel: TChannelTypes;
  xDragTarget: TPoint;
begin
  if CurveVisible then
  begin
    // Maustaste noch nicht gedr�ckt
    if SelectedChannel = ctUnknown then
    begin
      xHitChannel := HitCurve(aX, aY);
      if (xHitChannel <> ctUnknown) then
      begin
        if (HitChannel = ctUnknown) then
        begin
          HitChannel := xHitChannel;
          case xHitChannel of
            ctNeps:
              mQualityMatrixBase.Hint := mQualityMatrixBase.Hint + ' ' + rsNepsHint;
            ctShort:
              mQualityMatrixBase.Hint := mQualityMatrixBase.Hint + ' ' + rsShortHint;
            ctLong:
              mQualityMatrixBase.Hint := mQualityMatrixBase.Hint + ' ' + rsLongHint;
            ctThin:
              mQualityMatrixBase.Hint := mQualityMatrixBase.Hint + ' ' + rsThinHint;
          end;
          mQualityMatrixBase.ShowHint := True;
          mQualityMatrixBase.Invalidate;
        end
      end
      else begin
        if (HitChannel <> ctUnknown) then
        begin
          if Assigned(fOnReleaseChannel) then
            fOnReleaseChannel(HitChannel);

          HitChannel := xHitChannel;
          mQualityMatrixBase.ShowHint := False;
          mQualityMatrixBase.Invalidate;
        end
      end;

      if (aX > mQualityMatrixBase.ClientWidth - 4) or
         (aY > mQualityMatrixBase.ClientHeight - 4) then
      begin
        if Assigned(fOnReleaseChannel) then
          fOnReleaseChannel(HitChannel);

        HitChannel := ctUnknown;
        mQualityMatrixBase.ShowHint := False;
        mQualityMatrixBase.Invalidate;
      end
    end
    // Maustaste gedr�ckt
    else begin
      xDragTarget.x := aX;
      xDragTarget.y := aY;
      // anhand der Koordinaten die Parameter Dia und Len ermitteln
      AdjustChannel(xDragTarget);
  
      mQualityMatrixBase.Invalidate;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.CustomMouseUp(aShift: TShiftState; aX, aY: Integer);
begin
  //    if ([ssLeft] <= aShift) and
  if CurveVisible then
  begin
    if (SelectedChannel <> ctUnknown) then
    begin
  
      if Assigned(fOnReleaseChannel) then
        fOnReleaseChannel(SelectedChannel);
  
      SelectedChannel := ctUnknown;
      mDragRoot.x := 0;
      mDragRoot.y := 0;
      mQualityMatrixBase.Invalidate;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.DoValueUpdate;
begin
EnterMethod('DoValueUpdate');
end;

//:---------------------------------------------------------------------------
//1 Ein internes Flag wird gesetzt um zu zeigen, dass die Klasse gerade in einer Berechnung ist 
function TChannelCurve.EnterCalculation: Boolean;
begin
  mCriticalSection.Enter; // <======== Critical section
  try
    Result := not mCalculationActive;
  //  if not mCalculationActive then
    mCalculationActive := True;
  finally
    mCriticalSection.Leave;    // <======== Critical section
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.GenerateCutCurve(const aSamples: array of TPoint; const aTotSamples: Integer; const aReverse: Boolean);
var
  i, xSubFieldX, xSubFieldY: Integer;
  xFieldCurve: TClassFieldCurveRec;
  xFieldRect: TRect;
  xCoordinateMode: TCoordinateMode;
  xStrings: TStrings;
  
  // const aRange: TRangeRec);
    //----------------------------------------------------------------------------
  
  procedure CalculateSubField(const aX, aY: Integer; const aRect: TRect);
  begin
  
    with aRect, mCutCurve do
      if (mQualityMatrixBase.SubFieldX > 0) and
        (mQualityMatrixBase.SubFieldY > 0) then
      begin
  
        if mQualityMatrixBase.SubFieldX > (aRect.Right - aRect.Left) then
          fSubFieldX := (aRect.Right - aRect.Left);
  
        if (aRect.Right - aRect.Left) > 0 then
          xSubFieldX := Round((aX - aRect.Left) * fSubFieldX /
            (aRect.Right - aRect.Left)) + 1
        else
          xSubFieldX := 0;
  
        if mQualityMatrixBase.SubFieldY > (aRect.Bottom - aRect.Top) then
          fSubFieldY := (aRect.Bottom - aRect.Top);
  
        if (aRect.Bottom - aRect.Top) > 0 then
          xSubFieldY := Round((aRect.Bottom - aY) * fSubFieldY /
            (aRect.Bottom - aRect.Top)) + 1
        else
          xSubFieldY := 0;
      end
      else
      begin
  
        xSubFieldX := 0;
        xSubFieldY := 0;
      end;
  end;
    //----------------------------------------------------------------------------
  
  procedure InsertSubField;
  begin
  
    with mQualityMatrixBase do
    begin
      if (xSubFieldX > 0) and (xSubFieldY > 0) then
      begin
        if xSubFieldX > SubFieldX then xSubFieldX := SubFieldX;
        if xSubFieldY > SubFieldY then xSubFieldY := SubFieldY;
  (*
          xStrings.Delete(xSubFieldX - 1);
          if aReverse then
            xStrings.Insert(xSubFieldX - 1, IntToStr(SubFieldY - xSubFieldY + 1))
          else
            xStrings.Insert(xSubFieldX - 1, IntToStr(xSubFieldY));
  *)
        if aReverse then
          xStrings.Strings[xSubFieldX - 1] := IntToStr(SubFieldY - xSubFieldY + 1)
        else
          xStrings.Strings[xSubFieldX - 1] := IntToStr(xSubFieldY);
      end
  
    end;
  
  end;
    //----------------------------------------------------------------------------
  
  procedure NewField;
  var
    j: Integer;
  begin
  
    with mQualityMatrixBase do
    begin
      xStrings.Clear;
      for j := 0 to SubFieldX - 1 do
        xStrings.Add('-1');
  
      xFieldCurve.Field := GetFieldID(aSamples[i].x, aSamples[i].y);
      xFieldRect := Field[xFieldCurve.Field];
  
      CalculateSubField(aSamples[i].x, aSamples[i].y, xFieldRect);
      InsertSubField;
    end;
  end;
  
    //----------------------------------------------------------------------------
  
  function IsInRect(const aX, aY: Integer; const aRect: TRect): Boolean;
  begin
    with aRect do

      if (aX >= Left) and (aX < Right) and (aY >= Top) and (aY < Bottom) then
      begin
        Result := True;
  
      end
      else
        Result := False;
  end;
    //----------------------------------------------------------------------------
  
  procedure PadOutSubFieldString;
  var
    j: Integer;
  begin
  
    with mQualityMatrixBase do
    begin
      j := SubFieldX - 1;
  
      while (j >= 0) and (xStrings.Strings[j] = '-1') do
      begin
  (*
          xStrings.Delete(j);
          xStrings.Insert(j, '0');
  *)
        xStrings.Strings[j] := '0';
        Dec(j);
      end
    end;
  
  end;
    //----------------------------------------------------------------------------
  
begin
  with mQualityMatrixBase do
  begin
  
    xStrings := TStringList.Create;
  
    xFieldCurve.Field := TotFields; // %% !! TotFields might be a problem in Rough mode
  
    xCoordinateMode := CoordinateMode;
    CoordinateMode := cmActual;
  
    mCutCurve.fSubFieldX := mQualityMatrixBase.SubFieldX;
    mCutCurve.fSubFieldY := mQualityMatrixBase.SubFieldY;
  
  
    if aReverse then
    begin
      i := aTotSamples;
      while aSamples[i].y = cInvisiblePixcel do
        Dec(i);
  
      while (i >= 0) do
      begin
        if not ((aSamples[i].x = 0) and (aSamples[i].y = 0)) then
        begin
          if xFieldCurve.Field < TotFields then
          begin
  
            if IsInRect(aSamples[i].x, aSamples[i].y, xFieldRect) then
            begin
  
              CalculateSubField(aSamples[i].x, aSamples[i].y, xFieldRect);
              InsertSubField;
            end
            else
            begin
              PadOutSubFieldString;
  
              XFieldCurve.SubFields := xStrings.CommaText;
              mCutCurve.Add(xFieldCurve);
  
              NewField;

            end;
  
          end
          else
          begin
            NewField;
  
          end;
  
  //          Canvas.LineTo(aSamples[i].x, aSamples[i].y);
        end;
  
        Dec(i, CutCurveSampleDivisor);
      end;
    end
    else
    begin
      i := 0;
  
      while aSamples[i + 1].y = 0 do
        Inc(i);
      while aSamples[i].y = cInvisiblePixcel do
        Inc(i);
  
      while (i < aTotSamples) do
      begin
        if not ((aSamples[i].x = 0) and (aSamples[i].y = 0)) then
        begin
          if xFieldCurve.Field < TotFields then
          begin
  
            if IsInRect(aSamples[i].x, aSamples[i].y, xFieldRect) then
            begin
              CalculateSubField(aSamples[i].x, aSamples[i].y, xFieldRect);
              InsertSubField;
            end
            else
            begin
              PadOutSubFieldString;
  
              XFieldCurve.SubFields := xStrings.CommaText;
              mCutCurve.Add(xFieldCurve);
  
              NewField;
  
            end;
  
          end
          else
          begin
            NewField;
  
          end;
  
  //          Canvas.LineTo(aSamples[i].x, aSamples[i].y);
        end;
  
        Inc(i, CutCurveSampleDivisor);
      end;
    end;
  
    CoordinateMode := xCoordinateMode;
  
    if TotSamples > 0 then
    begin
      PadOutSubFieldString;

      XFieldCurve.SubFields := xStrings.CommaText;
      mCutCurve.Add(xFieldCurve);
    end;
  
    xStrings.Free;
  end;
end;

//:---------------------------------------------------------------------------
function TChannelCurve.GetChannelPara: TChannelSettingsRec;
begin
  Result := inherited GetChannelPara;
  
end;

//:---------------------------------------------------------------------------
function TChannelCurve.GetCutCurve(const aCurveType: TCurveTypes): TCutCurveList;
begin
  mCutCurve.Clear;

  if aCurveType = cvtNSL then begin
    with mControl, mQualityMatrixBase do
      if totNSLSamples > 0 then begin
        mCutCurve.Capacity := (totNSLSamples div CutCurveSampleDivisor) + 1;
        GenerateCutCurve(NSLSamples, totNSLSamples, False);
      end
  end
  else if aCurveType = cvtThin then begin
    with mControl, mQualityMatrixBase do
      if totThinSamples > 0 then begin
        mCutCurve.Capacity := (totThinSamples div CutCurveSampleDivisor) + 1;
        GenerateCutCurve(thinSamples, totThinSamples, True);
      end;
  end;

  Result := mCutCurve;
end;

//:---------------------------------------------------------------------------
{* Ermittelt, welche Kurve sich unter den gegebenen Mauskoordinaten befindet (ctNeps, ctShort, ctLong, ctThin)
*}
function TChannelCurve.HitCurve(aX, aY: Integer): TChannelTypes;
begin
  Result := ctUnknown;
  
  with mControl do
  begin
    if (ctNeps in SelectableChannels) and CheckHit(aX, aY, nepRange, NSLSamples) then
      Result := ctNeps
    else if (ctShort in SelectableChannels) and CheckHit(aX, aY, shortRange, NSLSamples) then
      Result := ctShort
    else if (ctLong in SelectableChannels) and CheckHit(aX, aY, longRange, NSLSamples) then
      Result := ctLong
    else if (ctThin in SelectableChannels) and CheckHit(aX, aY, thinRange, thinSamples) then
      Result := ctThin;
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.LeaveCalculation;
begin
  
  mCalculationActive := False;
end;

//:---------------------------------------------------------------------------
{* Checkt erst mal den "sichtbaren" Bereich und entscheidet dann, welche Kurven gezeichnet werden.
*}
procedure TChannelCurve.PaintCurve;
var
  xStart: Integer;
  
  //.....................................................
  procedure SetupCanvas;
  begin
    with mQualityMatrixBase do begin
      Canvas.Brush.Style := bsClear;
      Canvas.Pen.Mode    := pmCopy;
      Canvas.Pen.Color   := CurveColor;
      Canvas.Pen.Style   := CurveStyle;
  
      if CurveStyle <> psSolid then
        Canvas.Pen.Width := 1
      else
        Canvas.Pen.Width := CurveWidth;
    end;
  end;
  // //.....................................................
  // procedure SetHitCurveCanvasStyle;
  // begin
  //   with mQualityMatrixBase do begin
  //     Canvas.Pen.Mode  := pmMergePenNot;
  //     Canvas.Pen.Style := psDot;
  //     Canvas.Pen.Width := 1;
  //   end
  // end;
  // //.....................................................
  // procedure SetRegularCurveCanvasStyle;
  // begin
  //   with mQualityMatrixBase do begin
  //     Canvas.Pen.Mode  := pmCopy;
  //     Canvas.Pen.Style := CurveStyle;
  //     Canvas.Pen.Width := CurveWidth;
  //   end
  // end;
  // //.....................................................
  
begin
  if CurveVisible then begin
    with mControl, mQualityMatrixBase do begin
      SetupCanvas;
  
      // suche den Startpunkt, welcher als erster innerhalb der Komponente liegt
      // es bringt ja nichts Koordinaten abzufahren welche nicht sichtbar sind
      // z.B. wenn die Short Kurve oben herausf�hrt
      xStart := 0;
      while NSLSamples[xStart + 1].y = 0 do
        Inc(xStart);
      while NSLSamples[xStart].y = cInvisiblePixcel do
        Inc(xStart);
  
      if nepRange.start < TotSamples then begin
        if (xStart + 1) = nepRange.start then
          nepRange.start := xStart;
      end
      else if shortRange.start < TotSamples then begin
        if (xStart + 1) = shortRange.start then
          shortRange.start := xStart;
      end
      else if longRange.start < TotSamples then begin
        if (xStart + 1) = longRange.start then
          longRange.start := xStart;
      end;
  
      if ctNeps in EnabledChannels then
        PaintCurve(NSLSamples, totNSLSamples, nepRange, (HitChannel = ctNeps));

      if ctShort in EnabledChannels then
        PaintCurve(NSLSamples, totNSLSamples, shortRange, (HitChannel = ctShort));
  
      if ctLong in EnabledChannels then
          PaintCurve(NSLSamples, totNSLSamples, longRange, (HitChannel = ctLong));
  
      if ctThin in EnabledChannels then begin
        xStart := 0;
        while mControl.thinSamples[xStart + 1].y = 0 do
          Inc(xStart);
        while (mControl.thinSamples[xStart].y = cInvisiblePixcel) do
          Inc(xStart);
  
        if thinRange.start < TotSamples then begin
          if (xStart + 1) = thinRange.start then
            thinRange.start := xStart;
        end;
  
        PaintCurve(thinSamples, totThinSamples, thinRange, (HitChannel = ctThin));
      end;
    end;
  end; // if CurveVisible
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.PaintCurve(aSamples: array of TPoint; aTotSamples: Integer; aRange: TRangeRec; aHit: Boolean);
var
  i: Integer;
  
  //.....................................................
  procedure SetHitCurveCanvasStyle;
  begin
    with mQualityMatrixBase do begin
      Canvas.Pen.Mode  := pmMergePenNot;
      Canvas.Pen.Style := psDot;
      Canvas.Pen.Width := 1;
    end
  end;
  //.....................................................
  procedure SetRegularCurveCanvasStyle;
  begin
    with mQualityMatrixBase do begin
      Canvas.Pen.Mode  := pmCopy;
      Canvas.Pen.Style := CurveStyle;
      Canvas.Pen.Width := CurveWidth;
    end
  end;
  //.....................................................
  
begin
  if aHit then
    SetHitCurveCanvasStyle;
  
  with aRange, mQualityMatrixBase do begin
  //    if (start < aTotSamples) and (stop < aTotSamples) then begin
  //      for i := start to stop do begin
  //        with aSamples[i] do begin
  //          if (x <> 0) or (y <> 0) then
  //            Canvas.Ellipse(x-1, y-1, x, y);
  //        end;
  //      end;
  //    end;
     if (start < aTotSamples) and (stop < aTotSamples) then begin
       Canvas.MoveTo(aSamples[start].x, aSamples[start].y);
  
       for i := start to stop do begin
         if not ((aSamples[i].x = 0) and (aSamples[i].y = 0)) then
  //         if aSamples[i].x > 0 then
           Canvas.LineTo(aSamples[i].x, aSamples[i].y);
       end;
     end;
  end;
  
  if aHit then
    SetRegularCurveCanvasStyle;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.PutChannelPara(aSettings: TChannelSettingsRec);
begin
    {wss: test f�r cluster kurve}
  {
    if ParameterSource = psCluster then begin
      with aSettings do begin
        neps.sw  := 2;
        neps.dia := neps.dia * 0.8;

    //    short.sw  := 2;
    //    short.dia := Round(short.dia * 0.9);
    //    shortLen  := 18;
  
    //     long.sw  := 2;
         long.dia := long.dia * 0.95;
    //     longLen  := 600;
    //
         thin.sw  := 2;
         thin.dia := thin.dia * 1.05;
      //    thinLen  := 18;
      end;
    end;
    {}

  if ChannelParaChanged(aSettings) then
    SetChannelPara(aSettings);
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.PutHitChannel(const aValue: TChannelTypes);
begin
  if aValue <> fHitChannel then
  begin
    fHitChannel := aValue;
  end;
  
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.PutSelectableChannels(const aValue: TChannels);
begin
  fSelectableChannels := EnabledChannels * aValue;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.PutSelectedChannel(const aValue: TChannelTypes);
begin
  if aValue <> fSelectedChannel then
  begin
    fSelectedChannel := aValue;
    if aValue = ctUnknown then
    begin
      HitChannel := ctUnknown;
    end
    else
    begin
  
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.SetChannelPara(aSettings: TChannelSettingsRec);
begin
  inherited PutChannelPara(aSettings);
  ComputeNSLCurve;
  ComputeThinCurve;
  mQualityMatrixBase.Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.SetController(aValue: TBaseGUIController);
begin
  if fController <> aValue then
  begin
  //:...................................................................
    fController := aValue;
  //:...................................................................
  if Assigned(fController) then
    fController.OnValueUpdate := ValueUpdate;
  //:...................................................................
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.SetParameterSource(aValue: TParameterSource);
begin
  fParameterSource := aValue;
  //:...................................................................
  case fParameterSource of
    psChannel: mXP := cChannelXPDef;
    psSplice:  mXP := cSpliceXPDef;
//    psCluster: begin
//        mXP := cClusterXPDefSpectra;
//        SelectableChannels := [ctShort];
//        if Assigned(fController.Model) then begin
//          mHasExtCluster := Boolean(fController.Model.FPReader.Value[cXPFP_ExtendedClusterItem]);
//          if mHasExtCluster then begin
//            mXP := cClusterXPDefSpectraPlus;
//            SelectableChannels := [ctShort, ctLong, ctThin];
//          end;
//        end;
//      end;
  else
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.SetSampleBufferLength;
begin
  SetLength(mControl.NSLSamples, mQualityMatrixBase.TotSamples + 1);
  SetLength(mControl.thinSamples, mQualityMatrixBase.TotSamples + 1);

  ComputeNSLCurve;
  ComputeThinCurve;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.InitSampleBuffer(var aArr: array of TPoint);
var
 i: Integer;
begin
  for i:=0 to High(aArr) do begin
    aArr[i].x := 0; //cInvisiblePixcel;
    aArr[i].y := 0; //cInvisiblePixcel;
  end;
end;

//:---------------------------------------------------------------------------
procedure TChannelCurve.ValueUpdate(Sender: TBaseXMLSettingsObserver);
var
  xSettings: TChannelSettingsRec;
begin
  CheckComponents;

  with xSettings do begin
    Neps.Sw   := fController.SwitchValueDef[mXP.NSw, swOff];
    Short.Sw  := fController.SwitchValueDef[mXP.SSw, swOff];
    Long.Sw   := fController.SwitchValueDef[mXP.LSw, swOff];
    Thin.Sw   := fController.SwitchValueDef[mXP.TSw, swOff];

    // Spezialbehandlung f�r Kluster:
    // Wenn ein Kluster-Kanal ausgeschalten ist, dann wird dieser �bersteuert (swOn)
    // und die Parameter vom Channel genommen. Somit wird gew�hrleistet, dass die
    // normale Berechnung weiterhin funktioniert. Da die Klusterkurve UNTER der Kanalkurve
    // gezeichnet wird, sieht es f�r den Benutzer optisch wie ausgeschalten aus.
    // !! Lediglich f�rs GUI wird diese �bersteuerung gemacht. Die wirklichen Klusterparameter
    // !! sind weiterhin korrekt vorhanden
    if (fParameterSource = psCluster) then begin
      if Short.Sw = swOff then begin
        Short.Sw := swOn;
        mXP.SDia := cChannelXPDef.SDia;
        mXP.SLen := cClusterXPDefSpectra.SLen;
      end;
      if mHasExtCluster then begin
        if Long.Sw = swOff then begin
          Long.Sw := swOn;
          mXP.LDia := cClusterXPDefSpectra.LDia;
          mXP.LLen := cClusterXPDefSpectra.LLen;
        end;
        if Thin.Sw = swOff then begin
          Thin.Sw := swOn;
          mXP.TDia := cClusterXPDefSpectra.TDia;
          mXP.TLen := cClusterXPDefSpectra.TLen;
        end;
      end;
    end; // if fParameterSource = psCluster

    Neps.Dia  := fController.ValueDef[mXP.NDia, cNepsDiaMin];
    Short.Dia := fController.ValueDef[mXP.SDia, cShortDiaMin];
    ShortLen  := fController.ValueDef[mXP.SLen, cShortLenMin];
    Long.Dia  := fController.ValueDef[mXP.LDia, cLongDiaMin];
    LongLen   := fController.ValueDef[mXP.LLen, cLongLenMin];
    Thin.Dia  := fController.ValueDef[mXP.TDia, cThinDiaMaxBelow];
    ThinLen   := fController.ValueDef[mXP.TLen, cThinLenMin];
  end;

  SetChannelPara(xSettings);
end;


//:---------------------------------------------------------------------------
//:--- Class: TCutCurveList
//:---------------------------------------------------------------------------
{* Ermittelt, welche Kurve sich unter den gegebenen Mauskoordinaten befindet (ctNeps, ctShort, ctLong, ctThin)
*}
destructor TCutCurveList.Destroy;
begin
  Clear;
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TCutCurveList.Add(aClassFieldCurve: TClassFieldCurveRec): Integer;
var
  xPClassFieldCurve: PTClassFieldCurveRec;
begin
  New(xPClassFieldCurve);
  
  xPClassFieldCurve^ := aClassFieldCurve;

  Result := inherited Add(xPClassFieldCurve);
end;

//:---------------------------------------------------------------------------
procedure TCutCurveList.Clear;
var
  i: Integer;
begin
  
  for i := 0 to Count - 1 do
    if Assigned(inherited Items[i]) then
      Dispose(inherited Items[i]);
  inherited Clear;
end;

//:---------------------------------------------------------------------------
function TCutCurveList.GetItems(Index: Integer): TClassFieldCurveRec;
var
  xPClassFieldCurve: PTClassFieldCurveRec;
begin
  xPClassFieldCurve := inherited Items[Index];
  Result := xPClassFieldCurve^;
end;

//:---------------------------------------------------------------------------
procedure TCutCurveList.SetItems(Index: Integer; aValue: TClassFieldCurveRec);
var
  xPClassFieldCurve: PTClassFieldCurveRec;
begin
  if Assigned(inherited Items[Index]) then begin
    xPClassFieldCurve := inherited Items[Index];
    xPClassFieldCurve^ := aValue;
  end
  else begin
    New(xPClassFieldCurve);
    xPClassFieldCurve^ := aValue;
    inherited Items[Index] := xPClassFieldCurve;
  end;
end;

end.
