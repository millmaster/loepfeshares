(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: YMSettingsGUI.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: Clearer Settings
| Process(es)...: -
| Description...: Graphical User Interface for YarnMaster Settings
| Info..........: -
| Develop.system: Compaq Deskpro Pentium Pro 200, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 29.11.99 | 1.00 | Kr | Initial release
| 07.01.00 | 1.00 | Kr | Window sizes defined:
|          |      |    | TGUIYMSettings: 595x1010, Inside tabs: 565x1002
| 28.11.00 | 1.01 | Kr | NEW properties FromSpindle, ToSpindle, MachineConfig
|          |      |    | defined (interface definition only)
| 18.09.01 | 1.02 |NueKr| Handling of YM_set_name added.
| 3.10.01  | 1.03 |Kr  | GetSettingsfromScreen with "Changed" result extended
| 13.12.01 | 1.04 |Nue | GetActSensingHeadClass added.
| 20.09.02         Nue | Umbau ADO
| 08.12.04 | 2.00 |Nue | Umbau auf XML Version 5.0
| 07.06.05 | 2.00 |Wss | Caption von F SettingsBox korrigiert
| 11.01.08 | 2.01 |Nue | Einbau VCV.
| 16.01.08 | 2.01 |Nue | Einbau Spleissklassierung (zus�tzliche Matrix-Komponente auf Form!!).
| 15.09.08 | 2.02 |Nue | Beim dr�cken auf mFFClusterButtonClick ging es in den Edit-Mode => behoben.
|=============================================================================*)
unit YMSettingsGUI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, EditBox, YMParaEditBox, ExtCtrls, QualityMatrixBase,
  QualityMatrix, ComCtrls, mmTabControl, mmPageControl, mmGroupBox,
  YMBasicProdParaBox, mmButton, mmCheckBox, mmPanel,
  {YMParaDBAccessForGUI,} YMParaDef, YMParaUtils, QualityMatrixDef,
  MMUGlobal, mmQuery, mmListBox, adoDBAccess,
  XMLDef, VCLXMLSettingsModel, MMQualityMatrixClass,
  XMLSettingsAccess, xmlMaConfigClasses, SizingPanel;

const
  //==== Index for production group specific settings ====
//NUE1  cPGYarnCount = 0;
  cPGYarnUnit = 1;
  cPGPilotSpindle = 2;
  cPGSpeedRamp = 3;
  cPGSpeed = 4;

  //==== Propery index for machine specific config code ====
  cMConfigA = 1;
  cMConfigB = 2;
  cMConfigC = 3;

resourcestring
  //==== FF-Cluster strings ====
  rsFFSettings = '(16)FF-Einstellung'; // ivlm
  rsFFCluster = '(11)FF-Cluster';      // ivlm 

type
  TOnChange = procedure (aOwner: TComponent) of object;
  TGUIYMSettings = class(TFrame)
    mFFClusterButton: TmmButton;
    mFFClusterEditBox: TFFClusterEditBox;
    mFFGroupBox: TmmGroupBox;
    mGBBasicProdPara: TGBBasicProdPara;
    mPageControl: TmmPageControl;
    mQuality: TTabSheet;
    mSetup: TTabSheet;
    mStructureGroupBox: TmmGroupBox;
    mpnSensingHeadClass: THeadClassEditBox;
    mmPanel2: TmmPanel;
    mChannelEditBox: TChannelEditBox;
    mSpliceEditBox: TSpliceEditBox;
    mYarnCountEditBox: TYarnCountEditBox;
    mmPanel1: TmmPanel;
    mmPanel3: TmmPanel;
    mFaultClusterEditBox: TFaultClusterEditBox;
    mmPanel4: TmmPanel;
    mClassMatrix: TQualityMatrix;
    mCurveSelection: TmmPanel;
    mcbChannelCurve: TmmCheckBox;
    mcbSpliceCurve: TmmCheckBox;
    mcbClusterCurve: TmmCheckBox;
    mmPanel5: TmmPanel;
    mFFMatrix: TQualityMatrix;
    mPEditBox: TPEditBox;
    mSFIEditBox: TSFIEditBox;
    mVCVEditBox: TVCVEditBox;
    mSpliceMatrix: TQualityMatrix;
    procedure mcbChannelCurveClick(Sender: TObject);
    procedure mcbClusterCurveClick(Sender: TObject);
    procedure mcbSpliceCurveClick(Sender: TObject);
    procedure mFFClusterButtonClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure mSFIEditBoxClick(Sender: TObject);
  private
    fChannelSelection: Boolean;
    fConfigCodeVisible: Boolean;
//    FxFactoryMaConfig: TFabMaConfigHelper;
    FModel: TVCLXMLSettingsModel;
    fOnChange: TOnChange;
    fReadOnly: Boolean;
    mDoChangedEnabled: Boolean;
//NUE1    mSettings: TYMSettingsRec;
    mYMSettings: TXMLSettingsAccess;
    procedure BasicCreate;
//NUE1    procedure DoChanged(aTag: Integer);
//NUE1    procedure DoConfirm(aTag: Integer);
//NUE1    procedure DoParameterChange(aSource: TParameterSource);
//NUE1    procedure DoValidate;
    function GetActSensingHeadClass: TSensingHeadClass;
//NUE1    function GetBuildMaxMachineConfig: Boolean;
    function GetChannelSelection: Boolean;
    function GetProdGrpYarnCount: Single;
    function GetProdGrpYMPara(const aIndex: Integer): Integer;
    function GetYMSettinsGUIMode: TYMSettinsGUIMode;
    procedure PutChannelSelection(const aValue: Boolean);
//NUE1    procedure PutMachineConfig(const aValue: TMachineYMConfigRec);
//NUE1    procedure PutProdGrpYMPara(const aIndex: Integer; const aValue: Integer);
    procedure PutProdGrpYMPara(const aIndex: Integer; const aValue: Integer);
    procedure SetConfigCodeVisible(const aValue: Boolean);
//NUE1    procedure PutSpindleRange(const aIndex: Integer; const aValue: Byte);
//NUE1    procedure SetBuildMaxMachineConfig(const aValue: Boolean);
//NUE1    procedure SetCollectMaxMachineConfig(const aValue: TMachineYMConfigRec);
    procedure SetModel(const aModel: TVCLXMLSettingsModel);
    procedure SetProdGrpYarnCount(const Value: Single);
    procedure SetReadOnly(const aValue: Boolean);
    procedure SetYMSettinsGUIMode(const aValue: TYMSettinsGUIMode);
//NUE1    procedure xSynchFFBoxes;
//NUE1    procedure SynchronizeMachineInopSettings;
//NUE1    procedure xUpdateBasicProdParaBoxDependency;
//NUE1    procedure xUpdateMachineInopSettings;
//NUE1    procedure UpdateSensingHeadClass;
  public
    constructor Create(aOwner: TComponent); override;
//NUE1    constructor Create(aOwner: TComponent; aQuery: TNativeAdoQuery); reintroduce; overload;
//NUE1    constructor CreateStandAlone(aOwner: TComponent);
    destructor Destroy; override;
    procedure Delete(aSetID: Integer);
    function EqualSensingHeadClassInRange(const aFirstSpindle, aLastSpindle: Byte): Boolean; overload;
    function EqualSensingHeadClassInRange(const aFirstSpindle, aLastSpindle: Byte; const aSensingHeadClass: TSensingHeadClass): Boolean; overload;
//NUE1    function GetFromScreen(var aSettings: TYMSettingsByteArr; var aSize: Word): Boolean;
    procedure LoadFromDB(aSetID: Integer; var aYMSetName: TText50);
    function NewOnDB: Integer; overload;
    function NewOnDB(aName: string): Integer; overload;
    function NewTemplate(aName: string): Integer;
    procedure PutDefaultToScreen;
//NUE1    procedure PutToScreen(var aSettings: TYMSettingsByteArr);
//NUE1    procedure SetMaxSensingHeadClass;
    procedure UpdateOnDB(aSetID: Integer);
    property ActSensingHeadClass: TSensingHeadClass read GetActSensingHeadClass;
    property ConfigCodeVisible: Boolean read fConfigCodeVisible write SetConfigCodeVisible; //30.3.05
  published
//NUE1    property BuildMaxMachineConfig: Boolean read GetBuildMaxMachineConfig write SetBuildMaxMachineConfig;
//NUE1    property CollectMaxMachineConfig: TMachineYMConfigRec write SetCollectMaxMachineConfig;
    property CurveSelection: Boolean read GetChannelSelection write PutChannelSelection;
//NUE1    property FromSpindle: Byte index cSRFrom write PutSpindleRange;
//NUE1    property MachineConfig: TMachineYMConfigRec write PutMachineConfig;
    property GUIMode: TYMSettinsGUIMode read GetYMSettinsGUIMode write
        SetYMSettinsGUIMode;
    property Model: TVCLXMLSettingsModel read FModel write SetModel;
    property OnChange: TOnChange read fOnChange write fOnChange;
    property ProdGrpPilot: Integer index cPGPilotSpindle write PutProdGrpYMPara;
    property ProdGrpSpeed: Integer index cPGSpeed write PutProdGrpYMPara;
    property ProdGrpSpeedRamp: Integer index cPGSpeedRamp write PutProdGrpYMPara;
//NUE1    property ProdGrpYarnCount: Integer index cPGYarnCount read GetProdGrpYMPara write PutProdGrpYMPara;
    property ProdGrpYarnCount: Single read GetProdGrpYarnCount write
        SetProdGrpYarnCount;
    property ProdGrpYarnUnit: Integer index cPGYarnUnit read GetProdGrpYMPara write PutProdGrpYMPara;
    property ReadOnly: Boolean read fReadOnly write SetReadOnly;
//NUE1    property ToSpindle: Byte index cSRTo write PutSpindleRange;
  end;


implementation
uses
  mmMBCS, MMXMLConverter, BaseGlobal, mmcs, SettingsReader;

{$R *.DFM}
//------------------------------------------------------------------------------

//:---------------------------------------------------------------------------
//:--- Class: TGUIYMSettings
//:---------------------------------------------------------------------------
constructor TGUIYMSettings.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  mYMSettings := TXMLSettingsAccess.Create;

  //  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@??Nue23.12.04
  BasicCreate;
end;

//:---------------------------------------------------------------------------
destructor TGUIYMSettings.Destroy;
begin
  mYMSettings.Free;
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.BasicCreate;
begin
  mDoChangedEnabled := False;
  fReadOnly := False;

//NUE1  with mYMSettings do
//  begin

  //    OnValidate := DoValidate;

//NUE1    ChannelEditBox := mChannelEditBox;
//NUE1    mChannelEditBox.OnConfirm := DoConfirm;
    mChannelEditBox.Printable := False;

//NUE1    ClassMatrixEditBox := mClassMatrix;
  //    mClassMatrix.OnConfirm := DoChanged;
//NUE1    mClassMatrix.OnConfirm := DoConfirm;
///????    mClassMatrix.OnParameterChange := DoParameterChange;

//NUE1    FFMatrixEditBox := mFFMatrix;
    mFFMatrix.MatrixMode := mmSelectCutFields;
//NUE1    mFFMatrix.OnConfirm := DoChanged;

//NUE1    FFClusterEditBox := mFFClusterEditBox;
//NUE1    mFFClusterEditBox.OnConfirm := DoChanged;
    mFFClusterEditBox.Printable := False;
//NUE1    mFFClusterEditBox.SynchronizeFFMatrix := SynchFFBoxes;

//NUE1    FaultClusterEditBox := mFaultClusterEditBox;
//NUE1    mFaultClusterEditBox.OnConfirm := DoConfirm;
  //    mFaultClusterEditBox.OnConfirm := DoConfirmFaultCluster;
    mFaultClusterEditBox.Printable := False;

//NUE1    SpliceEditBox := mSpliceEditBox;
//NUE1    mSpliceEditBox.OnConfirm := DoConfirm;
    mSpliceEditBox.Printable := False;
  //    mSpliceEditBox.UpperYarn := False;

//NUE1    YarnCountEditBox := mYarnCountEditBox;
  //    mYarnCountEditBox.OnConfirm := DoConfirmYarnCount;
//NUE1    mYarnCountEditBox.OnConfirm := DoConfirm;
    mYarnCountEditBox.Printable := False;

   //    YarnCountEditBox.Plus := False;

//NUE1    SFIEditBox := mSFIEditBox;
//NUE1    mSFIEditBox.OnConfirm := DoChanged;
    mSFIEditBox.Printable := False;

    mVCVEditBox.Printable := False;

//NUE1    mPEditBox.OnConfirm := DoConfirm;
    mPEditBox.Printable := False;

//    mpnSensingHeadClass.OnConfirm := DoConfirm;
//    mpnSensingHeadClass.Printable := False;

//NUE1    GBBasicProdPara := mGBBasicProdPara;
  //    mGBBasicProdPara.OnConfirm := DoConfirmGBBasicProdPara;
//    mGBBasicProdPara.OnLoadYMSettings := UpdateBasicProdParaBoxDependency;
//NUE1    mGBBasicProdPara.OnConfirm := DoConfirm;

    PutDefaultToScreen;
//  end;

  fOnChange := nil;
  CurveSelection := False;

  mcbChannelCurve.Checked := True;
  mClassMatrix.ChannelColor := mcbChannelCurve.Font.Color;
  mClassMatrix.ChannelVisible := True;

  mcbClusterCurve.Checked := False;
  mClassMatrix.ClusterColor := mcbClusterCurve.Font.Color;
  mClassMatrix.ClusterVisible := False;

  mcbSpliceCurve.Checked := False;
//  mClassMatrix.SpliceColor := mcbSpliceCurve.Font.Color;
//  mClassMatrix.SpliceVisible := False;
  mSpliceMatrix.Visible := False;
  mSpliceMatrix.SpliceColor := mcbSpliceCurve.Font.Color;     //Nue:16.01.08
  mSpliceMatrix.SpliceVisible := True;                       //Nue:16.01.08

  mpnSensingHeadClass.Caption := rsSensingHeadClass + ':';
  
  ProdGRpYarnUnit   := TMMSettingsReader.Instance.Value [cYarnCntUnit]; //Nue:7.3.06
//Org  ProdGRpYarnUnit := Integer(yUNm);

  ReadOnly := fReadOnly;
  //ReadOnly := True;

//NUE1  mYMSettings.GetFromScreen(mSettings);

  mDoChangedEnabled := True;
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.Delete(aSetID: Integer);
begin
  mYMSettings.DeleteSetting(aSetID)
end;

//:---------------------------------------------------------------------------
function TGUIYMSettings.EqualSensingHeadClassInRange(const aFirstSpindle, aLastSpindle: Byte): Boolean;
begin

  Result := FModel.MaConfigReader.IsSameSection(aFirstSpindle, aLastSpindle);
end;

//:---------------------------------------------------------------------------
function TGUIYMSettings.EqualSensingHeadClassInRange(const aFirstSpindle, aLastSpindle: Byte; const aSensingHeadClass: TSensingHeadClass): Boolean;
var
  xGroup: Integer;

begin
  Result := False;
  if FModel.MaConfigReader.GroupIndexFromRange(aFirstSpindle, aLastSpindle, xGroup) then
    Result := (GetSensingHeadClass(FModel.MaConfigReader, xGroup)= aSensingHeadClass);
end;

//:---------------------------------------------------------------------------
function TGUIYMSettings.GetActSensingHeadClass: TSensingHeadClass;
begin
  Result := GetSensingHeadClass(FModel);
//NUE1  Result := TYMSettingsUtils.GetSensingHeadClass(mSettings);
end;

//:---------------------------------------------------------------------------
function TGUIYMSettings.GetChannelSelection: Boolean;
begin
  Result := fChannelSelection;
end;

function TGUIYMSettings.GetProdGrpYarnCount: Single;
begin
  Result := mYarnCountEditBox.YarnCount;
end;

//:---------------------------------------------------------------------------
function TGUIYMSettings.GetProdGrpYMPara(const aIndex: Integer): Integer;
begin
  Result := 0;
  case aIndex of
    cPGYarnUnit:
        Result := Integer(mYarnCountEditBox.YarnUnit);

    cPGPilotSpindle:;
  //      Result := mGBBasicProdPara.ProdGrpPilot;

    cPGSpeedRamp:;
  //      Result := mGBBasicProdPara.ProdGrpSpeedRamp;

    cPGSpeed:;
  //      Result := mGBBasicProdPara.ProdGrpSpeed;
  end;
end;

//:---------------------------------------------------------------------------
function TGUIYMSettings.GetYMSettinsGUIMode: TYMSettinsGUIMode;
begin
  
  Result := mGBBasicProdPara.Mode;
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.LoadFromDB(aSetID: Integer; var aYMSetName: TText50);
  var
    xXMLData, xYMSetName : String;
begin

  if mYMSettings.ReadSetting(aSetID, itXMLSetID, xXMLData, xYMSetName) then begin
    FModel.xmlAsString := xXMLData;
    StrLCopy(@aYMSetName, PChar(xYMSetName), sizeOf(aYMSetName));
    // Dem GUI sagen, dass das Fabrik FPattern wieder neu zugewiesen werden muss
    if GUIMode = gmTemplate then
      mGBBasicProdPara.SetFabFPattern;
  end; //if
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.mcbChannelCurveClick(Sender: TObject);
begin

  mClassMatrix.Visible := mcbChannelCurve.Checked; //Nue:2.7.07
  mSpliceMatrix.Visible := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbSpliceCurve.Checked := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbClusterCurve.Enabled := mcbChannelCurve.Checked; //Nue:2.7.07

end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.mcbClusterCurveClick(Sender: TObject);
begin

  mClassMatrix.ClusterVisible := mcbClusterCurve.Checked;

end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.mcbSpliceCurveClick(Sender: TObject);
begin
  mSpliceMatrix.Visible :=  mcbSpliceCurve.Checked;
  mClassMatrix.Visible := not(mcbSpliceCurve.Checked); //Nue:2.7.07
  mcbChannelCurve.Checked := not(mcbSpliceCurve.Checked); //Nue:2.7.07
  mcbClusterCurve.Enabled := not(mcbSpliceCurve.Checked);
  mcbClusterCurve.Enabled := not(mcbSpliceCurve.Checked); //Nue:2.7.07

end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.mFFClusterButtonClick(Sender: TObject);
begin
  if mFFMatrix.MatrixMode = mmDisplayData  then begin   //Abhandeln des DisplayModes Nue:15.09.08
    ;
  end
  else if mFFMatrix.MatrixMode = mmSelectFFClusterFields then begin
    mFFMatrix.MatrixMode := mmSelectCutFields;
    mFFClusterButton.Caption := rsFFSettings;
  end
  else begin
    mFFMatrix.MatrixMode := mmSelectFFClusterFields;
    mFFClusterButton.Caption := rsFFCluster;
  end;
end;

//:---------------------------------------------------------------------------
function TGUIYMSettings.NewOnDB: Integer;
begin
  //HashCodes werden innerhalb von NewTemplateSetting generiert
  Result := mYMSettings.NewSetting(fModel);
end;

//:---------------------------------------------------------------------------
function TGUIYMSettings.NewOnDB(aName: string): Integer;
begin
  //HashCodes werden innerhalb von NewTemplateSetting generiert
  Result := mYMSettings.NewSetting(fModel, aName);
end;

//:---------------------------------------------------------------------------
function TGUIYMSettings.NewTemplate(aName: string): Integer;
begin
  //HashCodes werden innerhalb von NewTemplateSetting generiert
  Result := mYMSettings.NewTemplateSetting(fModel, aName);
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.PutChannelSelection(const aValue: Boolean);
begin
  fChannelSelection := aValue;

  if not ReadOnly then
  begin
  
    if aValue then begin
      mClassMatrix.MatrixMode := mmSelectSettings;
      mSpliceMatrix.MatrixMode := mmSelectSettings;    //Nue:16.01.08
    end
    else begin
      mClassMatrix.MatrixMode := mmSelectCutFields;
      mSpliceMatrix.MatrixMode := mmSelectCutFields //Nue:16.01.08
    end;

  end
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.PutDefaultToScreen;
begin
  if not(csDesigning in ComponentState) and Assigned(fModel) then
    fModel.xmlAsString := mYMSettings.DefaultXMLYMSetting;
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.PutProdGrpYMPara(const aIndex: Integer; const aValue:
    Integer);
begin
  case aIndex of
    cPGYarnUnit:
        mYarnCountEditBox.YarnUnit := TYarnUnit(aValue);

    cPGPilotSpindle:
        mGBBasicProdPara.ProdGrpPilot := aValue;

    cPGSpeedRamp:
        mGBBasicProdPara.ProdGrpSpeedRamp := aValue;

    cPGSpeed:
        mGBBasicProdPara.ProdGrpSpeed := aValue;
  end;
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.SetConfigCodeVisible(const aValue: Boolean);
begin
  mGBBasicProdPara.ConfigCodeVisible := aValue;
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.SetModel(const aModel: TVCLXMLSettingsModel);
begin
  FModel := aModel;
  //Setzen des Models in allen Komponenten
  if Assigned(FModel) then begin
//    codesite.SendNumAsHexEx(csmCheckPoint, 'Modell TGUIYMSettings', cardinal(Pointer(FModel)));
    mClassMatrix.Model         := aModel;
    mSpliceMatrix.Model        := aModel;   //Nue:16.01.08
    mFFMatrix.Model            := aModel;
    mChannelEditBox.Model      := aModel;
    mSpliceEditBox.Model       := aModel;
    mYarnCountEditBox.Model    := aModel;
    mFaultClusterEditBox.Model := aModel;
    mSFIEditBox.Model          := aModel;
    mVCVEditBox.Model          := aModel;
    mFFClusterEditBox.Model    := aModel;
    mGBBasicProdPara.Model     := aModel;
    mPEditBox.Model            := aModel;
    mpnSensingHeadClass.Model  := aModel;
  end; //if
end;
//:---------------------------------------------------------------------------
procedure TGUIYMSettings.SetProdGrpYarnCount(const Value: Single);
begin
  mYarnCountEditBox.YarnCount := Value;
end;
//:---------------------------------------------------------------------------
procedure TGUIYMSettings.SetReadOnly(const aValue: Boolean);
begin
  mDoChangedEnabled := False;

  fReadOnly := aValue;

  if aValue then begin
    mClassMatrix.MatrixMode := mmDisplayData;
    mSpliceMatrix.MatrixMode := mmDisplayData;  //Nue:16.01.08
    mFFMatrix.MatrixMode    := mmDisplayData;
    mFFClusterButton.Caption := rsFFSettings;   //Damit FFMatrix und Button synchron sind! Nue:15.09.08 
  end
  else begin
    if CurveSelection then begin
      mClassMatrix.MatrixMode := mmSelectSettings;
      mSpliceMatrix.MatrixMode := mmSelectSettings   //Nue:16.01.08
    end
    else begin
      mClassMatrix.MatrixMode := mmSelectCutFields;
      mSpliceMatrix.MatrixMode := mmSelectCutFields //Nue:16.01.08
    end;

    if mFFMatrix.MatrixMode = mmDisplayData then
      mFFMatrix.MatrixMode := mmSelectSettings;
  end;

  mChannelEditBox.ReadOnly      := fReadOnly;
  mSpliceEditBox.ReadOnly       := fReadOnly;
  mYarnCountEditBox.ReadOnly    := fReadOnly;
  mFaultClusterEditBox.ReadOnly := fReadOnly;
  mSFIEditBox.ReadOnly          := fReadOnly;
  mVCVEditBox.ReadOnly          := fReadOnly;
  mFFClusterEditBox.ReadOnly    := fReadOnly;
  mPEditBox.ReadOnly            := fReadOnly;
  mGBBasicProdPara.ReadOnly     := fReadOnly;

  mDoChangedEnabled := True;
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.SetYMSettinsGUIMode(const aValue: TYMSettinsGUIMode);
begin
  mDoChangedEnabled := False;

//  mGBBasicProdPara.FabMaConfig := FactoryMaConfig; //Zuweisen des FactoryMaConfig das dieses im Context von mGBBasicProdPara bekannt ist
  mGBBasicProdPara.Mode := aValue;
//  UpdateMachineInopSettings;
  
  mDoChangedEnabled := True;
  
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.UpdateOnDB(aSetID: Integer);
// var
//   xHash1, xHash2: Integer;
begin
//  TMMXMLConverter.GenerateHashCode(fModel.xmlAsDOM, xHash1, xHash2);
  mYMSettings.UpdateSetting(aSetID, fModel.xmlAsString, GetSensingHeadClass(fModel){, xHash1, xHash2});
//NUE1  mYMSettings.UpdateOnDB(aSetID);
end;

//:---------------------------------------------------------------------------
procedure TGUIYMSettings.FrameResize(Sender: TObject);
begin
  mCurveSelection.Left := mClassMatrix.Width - mCurveSelection.Width;
  mCurveSelection.Left := mSpliceMatrix.Width - mCurveSelection.Width;
end;

procedure TGUIYMSettings.mSFIEditBoxClick(Sender: TObject);
begin

end;

//:---------------------------------------------------------------------------

end.

