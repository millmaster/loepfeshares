object fcTreeViewItemsEditor: TfcTreeViewItemsEditor
  Left = 211
  Top = 215
  Width = 502
  Height = 325
  ActiveControl = TextEdit
  Caption = 'fcTreeView Items Editor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 261
    Top = 0
    Width = 233
    Height = 263
    Align = alRight
    Caption = 'Item Properties'
    TabOrder = 1
    object Label1: TLabel
      Left = 12
      Top = 16
      Width = 21
      Height = 13
      Caption = '&Text'
      FocusControl = TextEdit
    end
    object Label2: TLabel
      Left = 12
      Top = 42
      Width = 58
      Height = 13
      Caption = 'I&mage Index'
      FocusControl = ImageIndexEdit
    end
    object Label3: TLabel
      Left = 12
      Top = 94
      Width = 54
      Height = 13
      Caption = 'State Inde&x'
      FocusControl = StateIndexEdit
    end
    object Label4: TLabel
      Left = 12
      Top = 68
      Width = 71
      Height = 13
      Caption = '&Selected Index'
      FocusControl = SelectedIndexEdit
    end
    object Label5: TLabel
      Left = 12
      Top = 120
      Width = 53
      Height = 13
      Caption = 'String D&ata'
      FocusControl = StringData1Edit
    end
    object Label6: TLabel
      Left = 12
      Top = 144
      Width = 62
      Height = 13
      Caption = 'St&ring Data 2'
      FocusControl = StringData2Edit
    end
    object TextEdit: TEdit
      Left = 96
      Top = 14
      Width = 129
      Height = 21
      TabOrder = 0
      OnChange = TextEditChange
      OnKeyDown = StateIndexEditKeyDown
    end
    object ImageIndexEdit: TEdit
      Left = 96
      Top = 40
      Width = 49
      Height = 21
      TabOrder = 1
      OnExit = ImageIndexEditExit
      OnKeyDown = StateIndexEditKeyDown
    end
    object SelectedIndexEdit: TEdit
      Left = 96
      Top = 66
      Width = 49
      Height = 21
      TabOrder = 2
      OnExit = SelectedIndexEditExit
      OnKeyDown = StateIndexEditKeyDown
    end
    object StateIndexEdit: TEdit
      Left = 96
      Top = 92
      Width = 49
      Height = 21
      TabOrder = 3
      OnExit = StateIndexEditExit
      OnKeyDown = StateIndexEditKeyDown
    end
    object CheckListBox: TCheckListBox
      Left = 8
      Top = 232
      Width = 217
      Height = 26
      TabStop = False
      OnClickCheck = CheckListBoxClickCheck
      BorderStyle = bsNone
      Color = clBtnFace
      Enabled = False
      IntegralHeight = True
      ItemHeight = 13
      Style = lbOwnerDrawFixed
      TabOrder = 7
      OnClick = CheckListBoxClick
      OnDrawItem = CheckListBoxDrawItem
    end
    object StringData1Edit: TEdit
      Left = 96
      Top = 118
      Width = 129
      Height = 21
      TabOrder = 4
      OnExit = StringData1EditExit
    end
    object StringData2Edit: TEdit
      Left = 96
      Top = 142
      Width = 129
      Height = 21
      TabOrder = 5
      OnExit = StringData2EditExit
    end
    object CheckboxRadioGroup: TGroupBox
      Left = 8
      Top = 171
      Width = 217
      Height = 57
      TabOrder = 8
      object CheckboxButton: TRadioButton
        Left = 8
        Top = 19
        Width = 105
        Height = 17
        Caption = 'Checkbox'
        Checked = True
        Enabled = False
        TabOrder = 0
        TabStop = True
        OnClick = CheckboxButtonClick
      end
      object RadioButton: TRadioButton
        Left = 8
        Top = 35
        Width = 97
        Height = 17
        Caption = 'Radio Button'
        Enabled = False
        TabOrder = 1
        OnClick = RadioButtonClick
      end
      object GrayedCheckbox: TCheckBox
        Left = 112
        Top = 19
        Width = 73
        Height = 17
        Caption = 'Grayed'
        Enabled = False
        TabOrder = 2
        OnClick = GrayedCheckboxClick
      end
    end
    object ShowCheckBox: TCheckBox
      Left = 16
      Top = 169
      Width = 145
      Height = 17
      Caption = 'Show Chec&kbox Using...'
      TabOrder = 6
      OnClick = ShowCheckBoxClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 0
    Width = 261
    Height = 263
    Align = alClient
    Caption = 'Items'
    TabOrder = 0
    object TreeView: TfcTreeView
      Left = 2
      Top = 15
      Width = 160
      Height = 246
      Align = alClient
      Indent = 19
      Options = [tvoExpandOnDblClk, tvoShowButtons, tvoShowLines, tvoShowRoot, tvoToolTips, tvoEditText]
      Items.StreamVersion = 1
      Items.Data = {00000000}
      TabOrder = 0
      OnChange = TreeViewChange
      OnChanging = TreeViewChanging
      OnDragDrop = TreeViewDragDrop
      OnDragOver = TreeViewDragOver
      OnMouseDown = TreeViewMouseDown
      OnToggleCheckbox = TreeViewToggleCheckbox
      OnCalcNodeAttributes = TreeViewCalcNodeAttributes
    end
    object Panel2: TPanel
      Left = 162
      Top = 15
      Width = 97
      Height = 246
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object NewItemButton: TSpeedButton
        Left = 9
        Top = 17
        Width = 80
        Height = 25
        Caption = '&New Item'
        ParentShowHint = False
        ShowHint = False
        OnClick = NewItemButtonClick
      end
      object NewSubitemButton: TSpeedButton
        Left = 9
        Top = 42
        Width = 80
        Height = 25
        Caption = 'N&ew Subitem'
        OnClick = NewSubitemButtonClick
      end
      object DeleteButton: TSpeedButton
        Left = 9
        Top = 67
        Width = 80
        Height = 25
        Caption = '&Delete'
        OnClick = DeleteButtonClick
      end
      object MoveUpButton: TSpeedButton
        Tag = 1
        Left = 9
        Top = 108
        Width = 80
        Height = 25
        Caption = 'Move &Up'
        OnClick = MoveButtonClick
      end
      object MoveDownButton: TSpeedButton
        Tag = -1
        Left = 9
        Top = 133
        Width = 80
        Height = 25
        Caption = 'Move Do&wn'
        OnClick = MoveButtonClick
      end
      object LoadButton: TSpeedButton
        Left = 9
        Top = 176
        Width = 80
        Height = 25
        Caption = '&Load from File'
        OnClick = LoadButtonClick
      end
      object SpeedButton1: TSpeedButton
        Left = 9
        Top = 201
        Width = 80
        Height = 25
        Caption = 'Save to &File'
        OnClick = SpeedButton1Click
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 263
    Width = 494
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = '&OK'
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 104
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object Button4: TButton
      Left = 192
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Help'
      TabOrder = 2
      OnClick = Button4Click
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'tvw'
    Filter = 'Tree View Files (*.tvw)|*.tvw|All Files|*.*'
    Left = 301
    Top = 269
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Tree View Files (*.TVW)|*.TVW|All Files (*.*)|*.*'
    Left = 343
    Top = 269
  end
end
