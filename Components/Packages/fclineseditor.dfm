object LinesEditorForm: TLinesEditorForm
  Left = 182
  Top = 148
  BorderStyle = bsDialog
  Caption = 'Multi-Line Text'
  ClientHeight = 277
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object OKButton: TButton
    Left = 264
    Top = 246
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelButton: TButton
    Left = 348
    Top = 246
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 4
    Width = 413
    Height = 233
    TabOrder = 2
    object LinesLabel: TLabel
      Left = 8
      Top = 12
      Width = 28
      Height = 13
      Caption = 'Lines:'
    end
    object LinesMemo: TMemo
      Left = 8
      Top = 28
      Width = 397
      Height = 197
      ScrollBars = ssBoth
      TabOrder = 0
      OnChange = LinesMemoChange
    end
  end
end
