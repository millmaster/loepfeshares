object fcAbout1stForm: TfcAbout1stForm
  Left = 569
  Top = 282
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'About 1stClass'
  ClientHeight = 191
  ClientWidth = 223
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 13
    Top = 120
    Width = 196
    Height = 2
  end
  object Version: TLabel
    Left = 16
    Top = 80
    Width = 161
    Height = 13
    AutoSize = False
    Caption = 'Version 1.0'
    WordWrap = True
  end
  object Label4: TLabel
    Left = 16
    Top = 96
    Width = 185
    Height = 13
    Caption = 'Copyright (c) 2000, Woll2Woll Software'
    WordWrap = True
  end
  object Registration: TLabel
    Left = 16
    Top = 136
    Width = 185
    Height = 13
    AutoSize = False
    Caption = 'Registration No.:'
    WordWrap = True
  end
  object fcLabel1: TfcLabel
    Left = 13
    Top = 37
    Width = 130
    Height = 42
    Caption = '1stClass'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -37
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TextOptions.Alignment = taLeftJustify
    TextOptions.Style = fclsRaised
    TextOptions.VAlignment = vaTop
  end
  object fcLabel2: TfcLabel
    Left = 13
    Top = 18
    Width = 181
    Height = 22
    Caption = 'Woll2Woll Software'#39's'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TextOptions.Alignment = taLeftJustify
    TextOptions.Style = fclsRaised
    TextOptions.VAlignment = vaTop
  end
  object Button1: TButton
    Left = 74
    Top = 162
    Width = 72
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
end
