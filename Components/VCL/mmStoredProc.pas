(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmStoredProc.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmStoredProc;

interface

uses
  Classes, DBTables, mmDatabase;

type
  TmmStoredProc = class(TStoredProc)
  private
    procedure ParseSchemaName;
    { Private declarations }
  protected
    procedure Loaded; override;
    function SetDBFlag(Flag: Integer; Value: Boolean): Boolean; override;
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//procedure Register;
//begin
//  RegisterComponents('Data Access', [TmmStoredProc]);
//end;

procedure TmmStoredProc.Loaded;
begin
  inherited;
  ParseSchemaName;
end;

function TmmStoredProc.SetDBFlag(Flag: Integer; Value: Boolean): Boolean;
begin
  if Value then ParseSchemaName;
  Result := inherited SetDBFlag(Flag, Value);
end;

procedure TmmStoredProc.ParseSchemaName;
var
  LocDatabase: TDatabase;
begin
  if (csDesigning in ComponentState) then Exit;
  if (Pos('.',StoredProcName) = 0) then
  begin
    LocDatabase := Session.FindDatabase(DatabaseName);
    if (LocDataBase <> nil) then
      with LocDatabase as TmmDatabase do
      begin
        if (SchemaName <> '') then
          StoredProcName := SchemaName + '.' + StoredProcName;
      end;
  end;
end;

end.
