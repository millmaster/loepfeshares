(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmRenameDlg.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmRenameDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, DBTables, StdCtrls;

type
  TmmRenameDlg = class(TComponent)
  private
    FCurrentKey: String;
    FReplaceKey: String;
    FCharCase: TEditCharCase;
    FEditMask: String;
  protected
    { Protected declarations }
  public
    function Execute: Boolean;
    property CurrentKey: String read FCurrentKey write FCurrentKey;
    property ReplaceKey: String read FReplaceKey write FReplaceKey;
    property EditMask: String read FEditMask write FEditMask;
  published
    property CharCase: TEditCharCase read FCharCase write FCharCase;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  DlgRename;

//procedure Register;
//begin
//  RegisterComponents('Samples', [TmmRenameDlg]);
//end;

function TmmRenameDlg.Execute: Boolean;
begin
  DlgRename_Data := TDlgRename_Data.Create(Application);
  try
    DlgRename_Data.CurrentKey := CurrentKey;
    DlgRename_Data.CharCase   := CharCase;
    DlgRename_Data.EditMask   := EditMask;
    DlgRename_Data.PrepareData;
    Result := (DlgRename_Data.ShowModal = IDOK);
    ReplaceKey := DlgRename_Data.ReplaceKey;
  finally
    DlgRename_Data.Free;
  end;
end;

end.

