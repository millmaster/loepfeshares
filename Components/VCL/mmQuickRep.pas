(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmQuickRep.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  1.00  Wss | Initial Release
| 24.01.2001  1.00  Wss | ClearQRTempFiles added because QuickReport does not cleanup in temp dir
| 02.03.2001  1.00  Wss | Property PageCount added. Filled in if Prepare function is called
| 16.03.2001  1.00  Wss | QRPrintable components can be edited at runtime by selecting components by mouse
                          OnSelectPrintable is called on mouse down
                            - Return True in CanSelect if the comp can be selected
                            - Return True in CanResize if the size of the comp can be changed by mouse
                          OnBandResize is called to get permission if the parent QRBand has to be
                          adapt to the pos and size of the bottom most component
| 13.07.2001  1.00  Wss | CheckMinBandHeight method added
| 06.11.2001  1.00  Wss | UsePrinterIndex methode added for using properties of paper size, etc.
                          In Loaded it is called UsePrinterIndex(-1) to use properties of default printer
| 14.02.2003  1.00  Wss | In CheckMinBandHeight wird nun gepr�ft, ob QRPrintable �berhaupt
                          Visible and Enabled ist
|=========================================================================================*)
unit mmQuickRep;

interface

uses
  Classes, Controls, Quickrpt, Windows, mmQRDBText;

type
  TStretchMode = (smNone, smMove, smHorizL, smHorizR, smVertT, smVertB);
  TResizeRequest = procedure (Sender: TObject; var CanResize: Boolean) of object;
  TSelectQREvent = procedure (Sender: TObject; var CanSelect, CanResize: Boolean) of object;

  TmmQuickRep = class(TQuickRep)
  private
    fColumnDistance: Integer;
    fOnBandResize: TResizeRequest;
    fOnSelectPrintable: TSelectQREvent;
    fPageCount: Integer;
    fQRControl: TQRPrintable;
    mCanResize: Boolean;
    mDrawTop,
    mDrawBottom,
    mDrawLeft,
    mDrawRight: Boolean;
    mLastPoint: TPoint;
    mStretchMode: TStretchMode;
    fOnAfterBandResize: TNotifyEvent;
    procedure SetColumnDistance(Value: Integer);
    procedure LocalOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure LocalOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure LocalOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  public
    property PageCount: Integer read fPageCount;
    property QRControl: TQRPrintable read fQRControl;
    procedure CheckMinBandHeight(aBand: TQRBand);
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure Prepare; virtual;
    procedure UnselectQRControl;
    procedure UsePrinterIndex(aIndex: Integer);
  published
    property ColumnDistance: Integer read fColumnDistance write SetColumnDistance default 0;
    property OnAfterBandResize: TNotifyEvent read fOnAfterBandResize write fOnAfterBandResize;
    property OnBandResize: TResizeRequest read fOnBandResize write fOnBandResize;
    property OnMouseDown;
    property OnSelectPrintable: TSelectQREvent read fOnSelectPrintable write fOnSelectPrintable;
  end;

procedure ClearQRTempFiles(aWnd: Integer);

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  QRPrntr, Printers,
  Forms, ShellAPI, SysUtils;

//------------------------------------------------------------------------------
function GetPaperSize: TQRPaperSize;
var
  xDevice: Array[0..255] of Char;
  xDriver: Array[0..255] of Char;
  xPort: Array[0..255] of Char;
  xDeviceMode: THandle;
  xDevMode: PDevMode;
  //.................................................................
  procedure GPaperSize;
  var
    xPS: TQRPaperSize;
  begin
    Result := Default;
    if (xDevMode^.dmFields and dm_papersize) = dm_papersize then begin
      for xPS:=Default to Custom do begin
        if xDevMode^.dmPaperSize = cQRPaperTranslate[xPS] then begin
          Result := xPS;
          Exit;
        end
      end
    end
  end;
  //.................................................................
begin
  Result := Default;
  Printer.GetPrinter(xDevice, xDriver, xPort, xDeviceMode);
  if xDeviceMode = 0 then
    Printer.GetPrinter(xDevice, xDriver, xPort, xDeviceMode);

  if xDeviceMode <> 0 then
  try
    xDevMode := GlobalLock(xDeviceMode);
    GPaperSize;
  finally
    GlobalUnlock(xDeviceMode);
  end;
end;
//------------------------------------------------------------------------------
{-------------------------------------------------------------------------------
 If an QRImage component is placed to QuickReport a tmp file is generated
 in the windows temp directory. QuickReport doesn't cleanup this after closing
 application !!
-------------------------------------------------------------------------------}
procedure ClearQRTempFiles(aWnd: Integer);
var
  xFOS: TSHfileOpStruct;
begin
  ZeroMemory(@xFOS, SizeOf(xFOS));
  with xFOS do begin
    Wnd := aWnd;
    wFunc := FO_DELETE;
    pFrom := PChar('C:\Temp\QRP*.tmp');
    fFlags := FOF_SILENT or FOF_FILESONLY or FOF_NOCONFIRMATION;
  end;
  SHFileOperation(xFOS);
end;

//------------------------------------------------------------------------------
// TmmQuickRep
//------------------------------------------------------------------------------
type
  THackControl = class(TControl);
  THackQRPrintable = class(TQRPrintable);

constructor TmmQuickRep.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fColumnDistance := 0;
  fonAfterBandResize := Nil;
  fOnBandResize      := Nil;
  fOnSelectPrintable := Nil;
  fPageCount      := -1;  // PageCount isn't evaluated if value = -1
  fQRControl      := Nil;

  mCanResize   := False;
  mStretchMode := smNone;

  OnMouseMove := LocalOnMouseMove;
end;
//------------------------------------------------------------------------------
destructor TmmQuickRep.Destroy;
begin

  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.CheckMinBandHeight(aBand: TQRBand);
var
  i: Integer;
  xMaxHeight: Integer;
begin
  // check for the selected QRPrintable if it does go over the bottom line
  xMaxHeight := 0;
  for i:=0 to Owner.ComponentCount-1 do begin
    if Owner.Components[i] is TQRPrintable then
      with TQRPrintable(Owner.Components[i]) do begin
        if (Parent = aBand) and Visible and Enabled then begin
          if (Top + Height) > xMaxHeight then
            xMaxHeight := (Top + Height);
        end;
    end;
  end;
  aBand.Height := xMaxHeight + 5;
  if Assigned(fOnAfterBandResize) then
    fOnAfterBandResize(aBand);
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.LocalOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xBand: TQRBand;
  xDoResize: Boolean;
begin
  // check for the selected QRPrintable if it does go over the bottom line
  if Sender = fQRControl then begin
    xBand := TQRBand(fQRControl.Parent);
    if Assigned(xBand) then begin
      if Assigned(fOnBandResize) then begin
        xDoResize := False;
        fOnBandResize(xBand, xDoResize);

        if xDoResize then
          CheckMinBandHeight(xBand);
      end; // if xDoResize
    end; // if Assigned
  end;
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.LocalOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
const
  cScreenCursor: Array[TStretchMode] of TCursor = (crDefault, crHandPoint, crHSplit, crHSplit, crVSplit, crVSplit);
  cSnapCursor = 4;
var
  xNewPoint: TPoint;
  xDeltaX: Integer;
  xDeltaY: Integer;
begin
  if Sender = fQRControl then begin
    if ssLeft in Shift then begin
      // if this controls has to be aligned to band -> reset property
      THackQRPrintable(fQRControl).AlignToBand := False;

      xNewPoint := fQRControl.ClientToScreen(Point(X, Y));
      xDeltaX   := xNewPoint.X - mLastPoint.X;
      xDeltaY   := xNewPoint.Y - mLastPoint.Y;
      with fQRControl do begin
        case mStretchMode of
          smHorizL: begin
              Width := Width - xDeltaX;
              Left  := Left  + xDeltaX;
            end;
          smHorizR: begin
              Width := Width + xDeltaX;
            end;
          smVertT: begin
              Height := Height - xDeltaY;
              Top    := Top    + xDeltaY;
            end;
          smVertB: begin
              Height := Height + xDeltaY;
            end;
        else // smMove
          Left := Left + xDeltaX;
          Top  := Top  + xDeltaY;
        end;
      end; // with
    end else begin
      // use Move as default
      mStretchMode := smMove;
      if mCanResize then
        with fQRControl do begin
          if X < cSnapCursor            then mStretchMode := smHorizL else
          if X > (Width - cSnapCursor)  then mStretchMode := smHorizR else
          if Y < cSnapCursor            then mStretchMode := smVertT else
          if Y > (Height - cSnapCursor) then mStretchMode := smVertB;
        end;
    end;
    mLastPoint := xNewPoint;
  end else
    mStretchMode := smNone;

  Screen.Cursor := cScreenCursor[mStretchMode];
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.UnselectQRControl;
begin
  // unselect previous selection
  if Assigned(fQRControl) then begin
    with fQRControl.Frame do begin
      DrawTop    := mDrawTop;
      DrawBottom := mDrawBottom;
      DrawLeft   := mDrawLeft;
      DrawRight  := mDrawRight;
    end;
    fQRControl := Nil;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.LocalOnMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xCanSelect: Boolean;
begin
  if (Sender <> fQRControl) then begin
    UnselectQRControl;
    // first check if the current printable is enabled to be selected
    xCanSelect := False;
    mCanResize := False;
    if Assigned(fOnSelectPrintable) then
      fOnSelectPrintable(Sender, xCanSelect, mCanResize);

    if xCanSelect then begin
      fQRControl := TQRPrintable(Sender);
      // save frame properties from selected component
      with fQRControl.Frame do begin
        mDrawTop    := DrawTop;
        mDrawBottom := DrawBottom;
        mDrawLeft   := DrawLeft;
        mDrawRight  := DrawRight;
      end;

      // set frame properties to draw complete frame
      with fQRControl.Frame do begin
        DrawTop    := True;
        DrawBottom := True;
        DrawLeft   := True;
        DrawRight  := True;
      end;
    end;
  end;
  if Assigned(fQRControl) then
    mLastPoint := fQRControl.ClientToScreen(Point(X, Y));

  if Assigned(OnMouseDown) then
    OnMouseDown(Sender, Button, Shift, X, Y);
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opInsert) then begin

    if (AComponent is TQRPrintable) or (AComponent is TQRBand) or
       (AComponent is TQRChildBand) then begin
      THackControl(AComponent).OnMouseMove := LocalOnMouseMove;
    end;

    if (AComponent is TQRPrintable) then begin
      THackControl(AComponent).OnMouseDown := LocalOnMouseDown;
      THackControl(AComponent).OnMouseUp   := LocalOnMouseUp;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.Prepare;
begin
  // first remove selection marks if selected
  UnselectQRControl;
  inherited Prepare;

  if (fPageCount = -1) and Assigned(QRPrinter) then
  try
    fPageCount := QRPrinter.PageCount;
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.SetColumnDistance(Value: Integer);
var
  I,NewPos,NewTop: Integer;
begin
  if fColumnDistance <> Value then
  begin
    NewPos := -1;
    NewTop := 0;
    for I := 0 to Owner.ComponentCount - 1 do
      if (Owner.Components[I] is TmmQRDBText) then //and (Components[I].Owner = Self) then
        with Owner.Components[I] as TmmQRDBText do
        begin
          if (NewPos = -1) or (Tag = -1) then
          begin
            NewPos := Left + Width + Value;
            NewTop := Top;
          end
          else
          begin
            Left := NewPos;
            Top  := NewTop;
            NewPos := Left + Width + Value;
          end;
          UpdateReportLabel;
        end;
    fColumnDistance := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.UsePrinterIndex(aIndex: Integer);
begin
  PrinterSettings.PrinterIndex := aIndex;
  Page.Orientation := Printers.Printer.Orientation;
  Page.PaperSize   := GetPaperSize;
end;
//------------------------------------------------------------------------------
procedure TmmQuickRep.Loaded;
begin
  inherited Loaded;
  // set properties from default printer
  UsePrinterIndex(-1);
end;

end.

