(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: regQRT.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components for 'QReport'
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit regQRT;

interface

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  Classes, mmQRBand, mmQRGroup, mmQRLabel, mmQRMemo,
  mmQRShape, mmQRSysData, mmQRChildBand, mmQRCompositeReport,
  mmQRDBRichText, mmQRExpr, mmQRImage, mmQRRichText,
  mmQRSubDetail, mmQuickRep, mmQRDBImage, mmQRDBText, mmQRStringsBand,
  mmQRExprMemo, mmQRTextFilter, mmQRCSVFilter,
  mmQRHTMLFilter;

procedure Register;
begin
  RegisterComponents('QReport',[
                          TmmQRBand,
                          TmmQRGroup,
                          TmmQRLabel,
                          TmmQRMemo,
                          TmmQRShape,
                          TmmQRSysData,
                          TmmQRChildBand,
                          TmmQRCompositeReport,
                          TmmQRDBRichText,
                          TmmQRExpr,
                          TmmQRImage,
                          TmmQRRichText,
                          TmmQRSubDetail,
                          TmmQuickRep,
                          TmmQRDBImage,
                          TmmQRDBText,
                          TmmQRStringsBand,
                          TmmQRExprMemo,
                          TmmQRTextFilter,
                          TmmQRCSVFilter,
                          TmmQRHTMLFilter
                                ]);
end;

end.
