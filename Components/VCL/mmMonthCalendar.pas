(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmMonthCalendar.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
|=========================================================================================*)
unit mmMonthCalendar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  AutoLabelClass, ComCtrls, DBCtrls, DB, CommCtrl;

type
  TmmMonthCalendar = class(TMonthCalendar)
  private
    fAutoLabel: TAutoLabel;
    procedure SetVisible(Value: Boolean); virtual;
    function GetVisible: Boolean;
    procedure CNNotify(var Message: TWMNotify); message CN_NOTIFY;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//procedure Register;
//begin
//  RegisterComponents('Win32', [TmmMonthCalendar]);
//end;

function IsBlankSysTime(const ST: TSystemTime): Boolean;
begin
  with ST do
    Result := (wYear = 0) and (wMonth = 0) and (wDayOfWeek = 0) and
      (wDay = 0) and (wHour = 0) and (wMinute = 0) and (wSecond = 0) and
      (wMilliseconds = 0);
end;
//------------------------------------------------------------------------------
procedure TmmMonthCalendar.CNNotify(var Message: TWMNotify);
var
  ST: PSystemTime;
begin
  inherited;
  with Message, NMHdr^ do
  begin
    case code of
      MCN_SELCHANGE:
        begin
          ST := @PNMSelChange(NMHdr).stSelStart;
          if not IsBlankSysTime(ST^) then
            DateTime := SystemTimeToDateTime(ST^);
          if MultiSelect then
          begin
            ST := @PNMSelChange(NMHdr).stSelEnd;
            if not IsBlankSysTime(ST^) then
              EndDate := SystemTimeToDateTime(ST^);
          end;
        end;
    end;
  end;
end;
//------------------------------------------------------------------------------
constructor TmmMonthCalendar.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAutoLabel := TAutoLabel.Create(Self);
end;
//------------------------------------------------------------------------------
destructor TmmMonthCalendar.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TmmMonthCalendar.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmMonthCalendar.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmMonthCalendar.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmMonthCalendar.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TmmMonthCalendar.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

end.
