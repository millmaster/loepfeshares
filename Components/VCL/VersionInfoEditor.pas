{$I DFS.INC}  { Standard defines for all Delphi Free Stuff components }

{-----------------------------------------------------------------------------}
{ DFSAbout unit v1.01                                                         }
{-----------------------------------------------------------------------------}
{ This unit provides a property editor that I use for the version property in }
{ all of my components.                                                       }
{ Copyright 1998, Brad Stowers.  All Rights Reserved.                         }
{ This unit can be freely used and distributed in commercial and private      }
{ environments, provied this notice is not modified in any way and there is   }
{ no charge for it other than nomial handling fees.  Contact me directly for  }
{ modifications to this agreement.                                            }
{-----------------------------------------------------------------------------}
{ Feel free to contact me if you have any questions, comments or suggestions  }
{ at bstowers@pobox.com.                                                      }
{ The lateset version of my components are always available on the web at:    }
{   http://www.pobox.com/~bstowers/delphi/                                    }
{-----------------------------------------------------------------------------}
{ Date last modified:  November 25, 1998                                      }
{-----------------------------------------------------------------------------}

{$IFDEF DFS_COMPILER_3_UP}
{$WEAKPACKAGEUNIT ON} { Allow unit to exist in multiple packages }
{$ENDIF}

unit VersionInfoEditor;

interface

uses
  DsgnIntf;

{$IFDEF DFS_WIN32}
  {$R VERSINFO.RES}
{$ELSE}
  {$R VERSINFO.R16}
{$ENDIF}

type
  TDFSVersion = {$IFNDEF DFS_DELPHI_1} type {$ENDIF} string;

  TDFSVersionProperty = class(TStringProperty)
  public
    procedure Edit; override;
    function GetValue: string; override;
    function GetAttributes: TPropertyAttributes; override;
  end;

{--- Version editor -----------------------------------------------------------}
{ double click opens the filename edit dialog }
{ right click gives two options - show the resources in a grid now }
{ and edit the filename }
  TVersionEditor = class(TDefaultEditor)
    procedure Edit; override;
    procedure EditProp (PropertyEditor : TPropertyEditor);
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  private
    procedure ShowVersInfoForm(const Filename: string);
  end;

{--- filename property editor .. fileopen dialog box --------------------------}
  TVersionFilenameProperty = class (TStringProperty)
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  Classes, ComCtrls, Controls, Dialogs, Forms, StdCtrls, SysUtils, TypInfo, VersionInfo;

//******************************************************************************
//* TDFSVersionProperty
//******************************************************************************
procedure TDFSVersionProperty.Edit;
const
  ABOUT_TEXT = '%s'#13#13 +
     'Copyright 1998, Brad Stowers, All Rights Reserved.'#13 +
     'This component is distributed as freeware.'#13#13 +
     'The latest version of this component can be found on'#13 +
     'my web site, Delphi Free Stuff, at:'#13 +
     '  http://www.pobox.com/~bstowers/delphi/'#13;
begin
  MessageDlg(Format(ABOUT_TEXT, [GetStrValue]), mtInformation, [mbOK], 0);
end;
//------------------------------------------------------------------------------
function TDFSVersionProperty.GetValue: string;
var
  i: integer;
begin
  i := Pos(' v', GetStrValue);
  Result := Copy(GetStrValue, i + 2, Length(GetStrValue)-i);
end;
//------------------------------------------------------------------------------
function TDFSVersionProperty.GetAttributes: TPropertyAttributes;
begin
  Result := inherited GetAttributes + [paDialog, paReadOnly];
end;
//******************************************************************************
//* TVersionFilenameProperty
//******************************************************************************
procedure TVersionFilenameProperty.Edit;
begin
  with TOpenDialog.Create(Application) do
  begin
    Filename := GetValue;
    Filter := 'Executables (*.exe)|*.exe|' +
              'Libraries (*.dll)|*.dll|' +
              'Packages (*.dpl)|*.dpl|' +
              'Drivers (*.drv,*.386,*.vxd)|*.drv;*.386;*.vxd|' +
              'Any file (*.*)|*.*';
    Options := Options + [ofPathMustExist, ofFileMustExist, ofHideReadOnly];
    try
      if Execute then
        SetValue(Filename)
    finally
      Free
    end
  end
end;
//------------------------------------------------------------------------------
function TVersionFilenameProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paDialog {$IFDEF DFS_WIN32}, paRevertable {$ENDIF}]
end;
//******************************************************************************
//* TVersionEditor
//******************************************************************************
procedure TVersionEditor.Edit;
var
  Components : TComponentList;
begin
  Components := TComponentList.Create;
  try
    Components.Add (Component);
    GetComponentProperties (Components, tkAny, Designer, EditProp)
  finally
    Components.Free
  end
end;
//------------------------------------------------------------------------------
procedure TVersionEditor.EditProp (PropertyEditor : TPropertyEditor);
begin
  if PropertyEditor is TVersionFilenameProperty then
  begin
    TVersionFilenameProperty (PropertyEditor).Edit;
    Designer.Modified
  end
end;
//------------------------------------------------------------------------------
procedure TVersionEditor.ShowVersInfoForm(const Filename: string);
var
  Frm: TForm;
  btnClose: TButton;
  VerInfo: TVersionInfoResource;
  VersionDisplay: {$IFDEF DFS_WIN32} TListView {$ELSE} TStringGrid {$ENDIF};
begin
  Frm := TForm.Create(Application);
  try
    Frm.BorderStyle := bsDialog;
    Frm.Caption := 'Version Info';
    Frm.Position := poScreenCenter;
    Frm.SetBounds(0, 0, 384, 238);
    btnClose := TButton.Create(frm);
    btnClose.Parent := Frm;
    btnClose.SetBounds(147, 180, 80, 25);
    btnClose.Cancel := TRUE;
    btnClose.Caption := '&Close';
    btnClose.Default := True;
    btnClose.ModalResult := mrOK;
    VerInfo := TVersionInfoResource.Create(Frm);
    VerInfo.Filename := Filename;

    {$IFDEF DFS_WIN32}
    VersionDisplay := TListView.Create(Frm);
    with VersionDisplay do
    begin
      Parent := Frm;
      Left := 8;
      Top := 8;
      Width := 358;
      Height := 164;
      ColumnClick := FALSE;
      with Columns.Add do
      begin
        Caption := 'Resource';
        Width := 85;
      end;
      with Columns.Add do
      begin
        Caption := 'Value';
        Width := 265;
      end;
      ReadOnly := True;
      TabOrder := 0;
      ViewStyle := vsReport;
    end;
    VerInfo.VersionListView := VersionDisplay;
    {$ELSE}
    VersionDisplay := TStringGrid.Create(Frm);
    with VersionDisplay do
    begin
      Parent := Frm;
      Left := 8;
      Top := 8;
      Width := 358;
      Height := 164;
      ColCount := 2;
      FixedCols := 0;
      FixedRows := 0;
      Options := [goDrawFocusSelected, goColSizing, goRowSelect];
      TabOrder := 0;
      ColWidths[0] := 85;
      ColWidths[1] := 265;
    end;
    VerInfo.VersionGrid := VersionDisplay;
    {$ENDIF}

    Frm.ShowModal;
  finally
    { Everything created above is owned by Frm, so it will free them. }
    Frm.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TVersionEditor.ExecuteVerb(Index: Integer);
begin
  case Index of
    0 : ShowVersInfoForm(TVersionInfoResource(Component).Filename);
    1 : Edit;
  end
end;
//------------------------------------------------------------------------------
function TVersionEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0 : Result := 'Show Version Info';
    1 : Result := 'Set Filename';
  end
end;
//------------------------------------------------------------------------------
function TVersionEditor.GetVerbCount: Integer;
begin
  Result := 2
end;
//------------------------------------------------------------------------------
end.
