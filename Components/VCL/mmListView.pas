(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmListView.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
| 09.02.2006        Wss | Wenn Caption editiert wird, dann wird bei Esc der Abbruch nie mitgeteilt
                          -> Eventproperty OnEndEdit hinzugefügt
|=========================================================================================*)
unit mmListView;

interface

uses
  AutoLabelClass, Classes, ComCtrls, Controls, Commctrl, Messages, SysUtils, Windows, mmRegistry;

type
  TLVItemEvent = TLVDeletedEvent;
  
  TmmListView = class(TListView)
  private
    fAutoLabel: TAutoLabel;
    fOnEndEdit: TLVItemEvent;
    procedure CNNotify(var Message: TWMNotify); message CN_NOTIFY;
    procedure SetVisible(Value: Boolean); virtual;
    function GetVisible: Boolean;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure SaveColumnsLayout(RegistryKey: String);
    procedure RestoreColumnsLayout(RegistryKey: String);
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property OnEndEdit: TLVItemEvent read fOnEndEdit write fOnEndEdit;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

constructor TmmListView.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAutoLabel := TAutoLabel.Create(Self);
  fOnEndEdit := nil;
end;
//------------------------------------------------------------------------------
destructor TmmListView.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TmmListView.CNNotify(var Message: TWMNotify);
var
  xItem: TLVItem;
begin
   inherited;

   if Message.NMHdr.code = LVN_ENDLABELEDIT then begin
     if Assigned(fOnEndEdit) then begin
       xItem := PLVDispInfo(Message.NMHdr)^.item;
       fOnEndEdit(Self, TListItem(xItem.lParam));
     end;
   end;
end;
//------------------------------------------------------------------------------
function TmmListView.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmListView.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
procedure TmmListView.RestoreColumnsLayout(RegistryKey: String);
var
  i: Integer;
  Str: String;
begin
  with TmmRegistry.Create do
  begin
    RootKey := HKEY_CURRENT_USER;
    if OpenKeyRel(RegistryKey,False) then
    begin
      for i:=0 to Columns.Count-1 do
      begin
        if ValueExists(Format('%s.%d',[Name,Columns.Items[i].Index])) then
          Str := ReadString(Format('%s.%d',[Name,Columns.Items[i].Index]))
        else
          Str := '';
        if (Str <> '') then
          Columns.Items[i].Width := StrToIntDef(Str,Columns.Items[i].Width);
      end;
    end;
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmListView.SaveColumnsLayout(RegistryKey: String);
var
  i: Integer;
begin
  with TmmRegistry.Create do
  begin
    RootKey := HKEY_CURRENT_USER;
    OpenKeyRel(RegistryKey,True);
    for i:=0 to Columns.Count-1 do
      WriteString(Format('%s.%d',[Name,Columns.Items[i].Index]),
                  Format('%d',[Columns.Items[i].Width]));
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmListView.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmListView.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;

//------------------------------------------------------------------------------
procedure TmmListView.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

end.
