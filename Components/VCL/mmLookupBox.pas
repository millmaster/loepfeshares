(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmLookupBox.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
|=========================================================================================*)
unit mmLookupBox;

interface

uses
  AutoLabelClass, Classes, StdCtrls, DB, DBTables, mmMemTable, Messages, dbclient;

type
  TmmLookupBox = class(TComboBox)
  private
    fAutoLabel: TAutoLabel;
    FLookupSource : TDataSource;  { Datasource from which to select }
    FLookupField : string;        { internal field used for further processing }
    FLookupDisplay : string;      { field displayed during selection }
    FDisplayValue : string;       { selected value of LookupDisplay }
    FValue : Variant;             { corresponding value for LookupField }
    FFilter: String;
    FFiltered: Boolean;
    FNoSelection: String;
    {SetNoSelection: Boolean;}
    function GetLookupSource : TDataSource;
    procedure SetLookupSource( Value : TDataSource);
    procedure SetLookupFieldName (const Value : string);
    procedure SetLookupDisplayName (const Value : string);
    procedure SetVisible(Value: Boolean); virtual;
    function GetVisible: Boolean;
    procedure OpenLookup;
  protected
    Procedure Change; override;
    procedure DefaultHandler(var Message); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
  public
    property  DisplayValue : string read FDisplayValue;
    property  Value : Variant read FValue;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DropDown; override;
    procedure ForceItemIndex(Value: integer);
    function  LookupValid: Boolean;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property LookupSource : TDataSource read GetLookupSource write SetLookupSource;
    property LookupField : string read FLookupField write SetLookupFieldName;
    property LookupDisplay : string read FLookupDisplay write SetLookupDisplayName;
    property Filter: String read FFilter write FFilter;
    property Filtered: Boolean read FFiltered write FFiltered;
    property NoSelection: String read FNoSelection write FNoSelection;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, sysutils;

//procedure Register;
//begin
//  RegisterComponents('Standard', [TmmLookupBox]);
//end;

constructor TmmLookupBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fAutoLabel := TAutoLabel.Create(Self);
  FNoSelection := '';
  {SetNoSelection := False;}
end;

function TmmLookupBox.GetLookupSource : TDataSource;
begin
  Result := FLookupSource;
end;

procedure TmmLookupBox.SetLookupSource (Value : TDataSource);
begin
  FLookupSource := Value;
  if Value <> nil then Value.FreeNotification(Self);
end;

procedure TmmLookupBox.SetLookupFieldName (const Value : string);
begin
  FLookupField := Value;
end;

procedure TmmLookupBox.SetLookupDisplayName (const Value : string);
begin
  FLookupDisplay := Value;
end;

function TmmLookupBox.LookupValid: Boolean;
begin
  OpenLookup;
  if (items.count > 0) then
    Result := True
  else
    Result := False;
end;

procedure TmmLookupBox.DropDown;
begin
  if LookupValid then inherited;
end;

procedure TmmLookupBox.OpenLookup;
var
  SourceActive : Boolean;
  LFilter: String;
  LFiltered: Boolean;
begin
// if already filtered -> copy filter in local var.
// set new filter
  LFilter   := '';
  LFiltered := false;
  if (FLookupSource <> nil) then
    if (FLookupSource.Dataset is TTable) or
       (FLookupSource.Dataset is TClientDataSet) or
       (FLookupSource.Dataset is TQuery) then
      with FLookupSource.Dataset as TDataSet do
      begin
        if (Filter <> '') then
        begin
          LFilter   := Filter;
          LFiltered := Filtered;
        end;
        Filter   := FFilter;
        Filtered := FFiltered;
      end;
// Dropdown list
  Items.Clear;
  if (FLookupSource <> nil) then
    if (FLookupSource.Dataset is TTable) or
       (FLookupSource.Dataset is TClientDataSet) or
       (FLookupSource.Dataset is TmmMemTable) then
      with FLookupSource.Dataset do
        begin
          SourceActive := Active;
          if Active then First else Open;
          Items.BeginUpdate;
          while not EOF do
            begin
              Items.Add( FieldByName(FLookupDisplay).AsString);
              Next;
            end;
          Items.EndUpdate;
          if not SourceActive then Close
        end
    else if (FLookupSource.Dataset is TQuery) then
      with FLookupSource.Dataset as TQuery do
        begin
          SourceActive := Active;
          if Active then First else Open;
          while not EOF do
            begin
              Items.Add( FieldByName(FLookupDisplay).AsString);
              Next;
            end;
          if not SourceActive then Close;
        end;
//////////  Inherited;
// restore filter from local var.
  if (FLookupSource <> nil) then
    if (FLookupSource.Dataset is TTable) or
       (FLookupSource.Dataset is TClientDataSet) then
      with FLookupSource.Dataset as TDataSet do
      begin
        Filter   := LFilter;
        Filtered := LFiltered;
      end;
end;

procedure TmmLookupBox.ForceItemIndex(Value: integer);
begin
  ItemIndex := Value;
  Change;
end;

procedure TmmLookupBox.Change ;
var
  SourceActive : Boolean;
begin
  FValue := Null;
  if (FLookupSource <> nil) then
    if (FLookupSource.Dataset is TTable) or
       (FLookupSource.Dataset is TClientDataSet) or
       (FLookupSource.Dataset is TmmMemTable) then
      with FLookupSource.Dataset do
        begin
          SourceActive := Active;
          if Active then First else Open;
          while (not EOF) and
                (FieldByName(FLookupDisplay).AsString<>Items[Itemindex]) do
              Next;
          if not EOF then
            case FieldByName(FLookupField).DataType of
              ftString : FValue := FieldByName(FLookupField).AsString;
              ftSmallint, ftInteger, ftWord:
                         FValue := FieldByName(FLookupField).AsInteger;
              ftBoolean: FValue := FieldByName(FLookupField).AsBoolean;
              ftFloat  : FValue := FieldByName(FLookupField).AsFloat;
              ftDate, ftTime, ftDateTime :
                         FValue := FieldByName(FLookupField).AsDateTime;
            end;
          if not SourceActive then Close;
        end
    else if (FLookupSource.Dataset is TQuery) then
      with FLookupSource.Dataset as TQuery do
        begin
          SourceActive := Active;
          if Active then First else Open;
          while (not EOF) and
                (FieldByName(FLookupDisplay).AsString<>Items[Itemindex]) do
              Next;
          if not EOF then
            case FieldByName(FLookupField).DataType of
              ftString : FValue := FieldByName(FLookupField).AsString;
              ftSmallint, ftInteger, ftWord:
                         FValue := FieldByName(FLookupField).AsInteger;
              ftBoolean: FValue := FieldByName(FLookupField).AsBoolean;
              ftFloat  : FValue := FieldByName(FLookupField).AsFloat;
              ftDate, ftTime, ftDateTime :
                         FValue := FieldByName(FLookupField).AsDateTime;
            end;
          if not SourceActive then Close;
        end;
  Inherited;
end;

procedure TmmLookupBox.DefaultHandler(var Message);
begin
  if (WindowHandle <> 0) then
  begin
    if {(not SetNoSelection) and}
       ((TMessage(Message).Msg = WM_PAINT) or
        (TMessage(Message).Msg = WM_SETFOCUS) or
//        (TMessage(Message).Msg = WM_COMMAND) or
//        (TMessage(Message).Msg = WM_NEXTDLGCTL) or
        (TMessage(Message).Msg = WM_KEYDOWN) or
        (TMessage(Message).Msg = WM_KILLFOCUS)) then
    begin
      {SetNoSelection := True;}
      if (ItemIndex = -1) and (Text = '') then
        Text := FNoSelection;
      {SetNoSelection := False;}
    end;
  end;
  inherited;
end;
//------------------------------------------------------------------------------
destructor TmmLookupBox.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TmmLookupBox.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmLookupBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmLookupBox.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmLookupBox.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TmmLookupBox.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

end.
