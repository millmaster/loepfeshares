(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: RegDSS.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components for 'Decision Cube'
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit regDSS;

interface

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  Classes, mmDecisionCube, mmDecisionQuery, mmDecisionSource,
  mmDecisionPivot, mmDecisionGrid, mmDecisionGraph;

procedure Register;
begin
  RegisterComponents('Decision Cube',[
                          TmmDecisionCube,
                          TmmDecisionQuery,
                          TmmDecisionSource,
                          TmmDecisionPivot,
                          TmmDecisionGrid,
                          TmmDecisionGraph
                                ]);
end;

end.
