(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDBGrid.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 23.02.1998  0.01  Wss | Methode Loaded: problems with lookup fields in ShiftCalendar
| 25.06.1999  1.00  Wss | Barco code remarked
| 02.10.2002  1.00  LOK | Barco code removed
|=========================================================================================*)
unit mmDBGrid;

interface

uses
  DBGrids;

type
  TmmDBGrid = class(TDBGrid)
  private
  protected
  public
  published
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.
