(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmStringTable.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmStringTable;

interface

uses
  Classes;

type
  TmmCustomStringTable = class(TComponent)
  private
    FItems: TStrings;
  protected
    procedure SetItems(Value: TStrings);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Items: TStrings read FItems write SetItems;
  end;

  TmmStringTable = class(TmmCustomStringTable)
  published
    property Items;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;


//procedure Register;
//begin
//  RegisterComponents('Samples', [TmmStringTable]);
//end;

{ TmmCustomStringTable}

constructor TmmCustomStringTable.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FItems := TStringList.Create;
end;

destructor TmmCustomStringTable.Destroy;
begin
  FItems.Free;
  inherited Destroy;
end;

procedure TmmCustomStringTable.SetItems(Value: TStrings);
begin
  FItems.Assign(Value);
end;

end.
