(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: mmCheckListEdit.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 01.11.2002  1.00  SDo | Initial Release
|=============================================================================*)
unit mmCheckListEdit;

interface

uses
  AutoLabelClass, Classes, ComCtrls, Sysutils, clisted;

type
  TMMCheckListEdit = class(TCheckListEdit)
  private
    fAutoLabel: TAutoLabel;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
  end;

//procedure Register;

implementation
uses
  mmMBCS;


{ TMMCheckListEdit }
//------------------------------------------------------------------------------
constructor TMMCheckListEdit.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAutoLabel := TAutoLabel.Create(Self);
end;
//------------------------------------------------------------------------------
destructor TMMCheckListEdit.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TMMCheckListEdit.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------

end.
