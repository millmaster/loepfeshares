(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: regDB.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components for 'Data Access, Data Controls'
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit regADO;

interface

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  Classes,
  mmADOConnection,
  mmADOCommand,
  mmADODataSet,
  mmADOTable,
  mmADOQuery,
  mmADOStoredProc,
  mmRDSConnection;

procedure Register;
begin
  RegisterComponents('ADO',[
                          TmmADOConnection,
                          TmmADOCommand,
                          TmmADODataSet,
                          TmmADOTable,
                          TmmADOQuery,
                          TmmADOStoredProc,
                          TmmRDSConnection
                                ]);
end;

end.
