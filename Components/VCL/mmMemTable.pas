(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmMemTable.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmMemTable;

interface

uses
  DB, Classes, mmClasses;

const
  Precision = 1E-400; { This is an arbitrary limit }

type

{ TRecInfo }

{   This structure is used to access additional information stored in
  each record buffer which follows the actual record data.

    Buffer: PChar;
   ||
   \/
    --------------------------------------------
    |  Record Data  | Bookmark | Bookmark Flag |
    --------------------------------------------
                    ^-- PRecInfo = Buffer + FRecInfoOfs

  Keep in mind that this is just an example of how the record buffer
  can be used to store additional information besides the actual record
  data.  There is no requirement that TDataSet implementations do it this
  way.

  For the purposes of this demo, the bookmark format used is just an integer
  value.  For an actual implementation the bookmark would most likely be
  a native bookmark type (as with BDE), or a fabricated bookmark for
  data providers which do not natively support bookmarks (this might be
  a variant array of key values for instance).

  The BookmarkFlag is used to determine if the record buffer contains a
  valid bookmark and has special values for when the dataset is positioned
  on the "cracks" at BOF and EOF. }

  PRecInfo = ^TRecInfo;
  TRecInfo = packed record
    Bookmark: Integer;
    BookmarkFlag: TBookmarkFlag;
  end;

{ *** TmmMemTable *** }

  TmmDataOrigin = (doInternal, doExternal);

  TMemoryRows = class;
  TmmMemTable = class(TDataSet)
  private
    FData : TMemoryRows;
    FDataLocation: PChar;
    FDataCount : Integer;
    FDAtaOrigin: TmmDataOrigin;
    FReadOnly: Boolean;
    FRecBufSize: Integer;
    FRecInfoOfs: Integer;
    FCurRec: Integer;
    FSaveChanges: Boolean;
    FUserAccess: TmmUserAccess;
    FPKFields: TStringList;
//    property MemoryRows: TMemoryRows read FData;
    procedure SetReadOnly(Value: Boolean);
    procedure SetUserAccess(Value: TmmUserAccess);
    function GetPKFields : TStringList;
  protected
    { Overriden abstract methods (required) }
    function AllocRecordBuffer: PChar; override;
    procedure FreeRecordBuffer(var Buffer: PChar); override;
    procedure GetBookmarkData(Buffer: PChar; Data: Pointer); override;
    function GetBookmarkFlag(Buffer: PChar): TBookmarkFlag; override;
    function GetFieldData(Field: TField; Buffer: Pointer): Boolean; override;
    function GetRecord(Buffer: PChar; GetMode: TGetMode; DoCheck: Boolean): TGetResult; override;
    function GetRecordSize: Word; override;
    procedure InternalAddRecord(Buffer: Pointer; Append: Boolean); override;
    procedure InternalClose; override;
    procedure InternalDelete; override;
    procedure InternalFirst; override;
    procedure InternalGotoBookmark(Bookmark: Pointer); override;
    procedure InternalHandleException; override;
    procedure InternalInitFieldDefs; override;
    procedure InternalInitRecord(Buffer: PChar); override;
    procedure InternalLast; override;
    procedure InternalOpen; override;
    procedure InternalPost; override;
    procedure InternalSetToRecord(Buffer: PChar); override;
    function IsCursorOpen: Boolean; override;
    procedure SetBookmarkFlag(Buffer: PChar; Value: TBookmarkFlag); override;
    procedure SetBookmarkData(Buffer: PChar; Data: Pointer); override;
    procedure SetFieldData(Field: TField; Buffer: Pointer); override;

  protected
    { Additional overrides (optional) }
    function GetCanModify: Boolean; override;
    function GetRecordCount: Integer; override;
    function GetRecNo: Integer; override;
    procedure SetRecNo(Value: Integer); override;
    procedure SetDataLocation(Value: PChar);
    procedure SetDataCount(Value : Integer);
    procedure SetDataOrigin(Value: TmmDataOrigin);
    function GetDataOrigin: TmmDataOrigin;
// >> Filter
//    function FindRecord(Restart, GoForward: Boolean): Boolean; override;
//    function FilterOK: Boolean;
//    function FilterOK2: Boolean;
// << Filter
    function Locate(const KeyFields: string; const KeyValues: Variant;
      Options: TLocateOptions): Boolean; override;
    function Lookup(const KeyFields: string; const KeyValues: Variant;
               const ResultFields: string): Variant; override;
    function GetCurrentRecord(Buffer: PChar): Boolean; override;
    function CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer; override;
    function BookmarkValid(Bookmark: TBookmark): Boolean; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Sort(FieldCol: Integer; Ascend: Boolean);
    procedure DoBeforeInsert; override;
    procedure DoBeforeDelete; override;
    procedure DoBeforePost; override;
    procedure DoBeforeOpen; override;
    procedure DoAfterPost; override;
    procedure DoAfterCancel; override;
    procedure ProtectPKFields;
    procedure UnprotectPKFields;
    property DataLocation: PChar read FDataLocation write SetDataLocation;
    property DataCount: Integer read FDataCount write SetDataCount;
    property PKFields: TStringList read GetPKFields;
  published
    property Active;
    property Tag;
    property ReadOnly: Boolean read FReadOnly write SetReadOnly;
//    property Filtered; // >> Filter <<
    property DataOrigin: TmmDataOrigin read GetDataOrigin write SetDataOrigin;
    property BeforeOpen;
    property AfterOpen;
    property BeforeClose;
    property AfterClose;
    property BeforeInsert;
    property AfterInsert;
    property BeforeEdit;
    property AfterEdit;
    property BeforePost;
    property AfterPost;
    property BeforeCancel;
    property AfterCancel;
    property BeforeDelete;
    property AfterDelete;
    property BeforeScroll;
    property AfterScroll;
    property OnCalcFields;
    property OnDeleteError;
    property OnEditError;
//    property OnFilterRecord;  >> Filter <<
    property OnNewRecord;
    property OnPostError;
    property UserAccess: TmmUserAccess read FUserAccess write SetUserAccess default uaFull;
  end;

{ *** TMemoryRow ***}

  TMemoryRow = class(TPersistent)
  private
    FMemoryRows : TMemoryRows;
    FID: Integer;
    Data: Variant;
    DataPtr: PChar;
    function GetIndex: Integer;
    function Get(Index: Integer): Variant;
    procedure Put(Index:Integer; Item: Variant);
    property RecData[Index: Integer]: Variant read Get write Put;
  protected
    procedure changed(AllItems: Boolean);
    procedure SetIndex(Value: Integer); virtual;
  public
    constructor Create(AMemoryRows: TMemoryRows; Index: Integer);
    destructor Destroy; override;
    property ID: integer read FID;
    property Index: Integer read GetIndex write SetIndex;
    procedure Assign(Source: TPersistent); override;
  end;


{ *** TMemoryRows ***}

  TListSortCompare = function (FieldNo: Integer; Item1, Item2: Pointer; Ascend: Boolean; Owner: TmmMemTable): Integer;
  TMemoryRows = class(TList)
  private
    FOwner : TmmMemTable;
    FUpdateCount: Integer;
    FNextID: Integer;
    procedure QuickSort(FieldCol:Integer; L, R: Integer; SCompare: TListSortCompare;
                        Ascend: Boolean);
    procedure SortOnColumn(FieldCol: Integer; Compare: TListSortCompare; Ascend: Boolean);
    procedure InsertItem(Ind: Integer; Item: TMemoryRow);
    procedure RemoveItem(Item: TMemoryRow);
  protected
    procedure Changed;
    procedure Update(Item: TMemoryRow); virtual;

  public
    constructor Create(AOwner: TmmMemTable);
    destructor  Destroy; override;
    procedure BeginUpdate;
    procedure EndUpdate;
    function FindItemID(ID: Integer): TMemoryRow;
    property Owner: TmmMemTable read FOwner;
    procedure InsertRow(Index: Integer; Value: Variant);
    procedure RemoveRow(Index: Integer);
  end;


{ *** Register ***}

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 Windows, SysUtils, Forms;

{ TmmMemTable }

{ This method is called by TDataSet.Open and also when FieldDefs need to
  be updated (usually by the DataSet designer).  Everything which is
  allocated or initialized in this method should also be freed or
  uninitialized in the InternalClose method. }

constructor TmmMemTable.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FUserAccess   := uaFull;
  FPKFields     := TStringList.Create;
end;

destructor TmmMemTable.Destroy;
begin
  FPKFields.Free;
  inherited Destroy;
end;

procedure TmmMemTable.InternalOpen;
var
  i: integer;
begin
  FData := TMemoryRows.Create(self);
  { Initialize the FieldDefs }
  InternalInitFieldDefs;
  { Initialize our internal position.
    We use -1 to indicate the "crack" before the first record. }
  FCurRec := -1;
  { Tell TDataSet how big our Bookmarks are (REQUIRED) }
  BookmarkSize := SizeOf(Integer);
  { Create TField components when no persistent fields have been created }
  if DefaultFields then CreateFields;
  { Bind the TField components to the physical fields }
  BindFields(True);
  { Initialize an offset value to find the TRecInfo in each buffer }
  case FDataOrigin of
    doInternal:
      FRecInfoOfs := SizeOf(Variant);
    doExternal:
      begin
        FRecInfoOfs := 0;
        for i:= 0 to FieldDefs.Count - 1 do
            FRecInfoOfs := FRecInfoOfs + Fields[i].DataSize ;
      end;
  end;
  { Calculate the size of the record buffers.
    Note: This is NOT the same as the RecordSize property which
    only gets the size of the data in the record buffer }
  FRecBufSize := FRecInfoOfs + SizeOf(TRecInfo);
  if DataOrigin = doExternal then
    for i := 0 to (DataCount-1) do
      begin
        TMemoryRow.Create(FData,i);
        TMemoryRow(FData[i]).DataPtr:= PChar(DataLocation + i*RecordSize);
      end;
end;

procedure TmmMemTable.InternalClose;
begin
  { Write any edits to disk and free the managing string list }
  FData.Free;
  FData := nil;
  { Destroy the TField components if no persistent fields }
  if DefaultFields then DestroyFields;
  { Reset these internal flags }
  FCurRec := -1;
end;

{ This property is used while opening the dataset.
  It indicates if data is available even though the
  current state is still dsInActive. }

function TmmMemTable.IsCursorOpen: Boolean;
begin
  Result := Assigned(FData);
end;

{ For this simple example we just create one FieldDef, but a more complete
  TDataSet implementation would create multiple FieldDefs based on the
  actual data. }

procedure TmmMemTable.InternalInitFieldDefs;
var
  i: integer;
begin
  if not DefaultFields then
    for i := 0 to (FieldCount-1) do
      FieldDefs.Add(Fields[i].FieldName, Fields[i].DataType, Fields[i].Size, Fields[i].Required);
end;

{ This is the exception handler which is called if an exception is raised
  while the component is being stream in or streamed out.  In most cases this
  should be implemented useing the application exception handler as follows. }

procedure TmmMemTable.InternalHandleException;
begin
  Application.HandleException(Self);
end;

{ Bookmarks }
{ ========= }

{ In this sample the bookmarks are stored in the Object property of the
  TStringList holding the data.  Positioning to a bookmark just requires
  finding the offset of the bookmark in the TStrings.Objects and using that
  value as the new current record pointer. }

procedure TmmMemTable.InternalGotoBookmark(Bookmark: Pointer);
var
  IndexItem: Pointer;
begin
  IndexItem := FData.FindItemID(PInteger(Bookmark)^);
  if IndexItem <> nil then
    FCurRec := FData.IndexOf(IndexItem)
  else
    DatabaseError('Bookmark not found');
end;

{ This function does the same thing as InternalGotoBookmark, but it takes
  a record buffer as a parameter instead }

procedure TmmMemTable.InternalSetToRecord(Buffer: PChar);
begin
  InternalGotoBookmark(@PRecInfo(Buffer + FRecInfoOfs).Bookmark);
end;

{ Bookmark flags are used to indicate if a particular record is the first
  or last record in the dataset.  This is necessary for "crack" handling.
  If the bookmark flag is bfBOF or bfEOF then the bookmark is not actually
  used; InternalFirst, or InternalLast are called instead by TDataSet. }

function TmmMemTable.GetBookmarkFlag(Buffer: PChar): TBookmarkFlag;
begin
  Result := PRecInfo(Buffer + FRecInfoOfs).BookmarkFlag;
end;

procedure TmmMemTable.SetBookmarkFlag(Buffer: PChar; Value: TBookmarkFlag);
begin
  PRecInfo(Buffer + FRecInfoOfs).BookmarkFlag := Value;
end;

{ These methods provide a way to read and write bookmark data into the
  record buffer without actually repositioning the current record }

procedure TmmMemTable.GetBookmarkData(Buffer: PChar; Data: Pointer);
begin
  PInteger(Data)^ := PRecInfo(Buffer + FRecInfoOfs).Bookmark;
end;

procedure TmmMemTable.SetBookmarkData(Buffer: PChar; Data: Pointer);
begin
  PRecInfo(Buffer + FRecInfoOfs).Bookmark := PInteger(Data)^;
end;

{ Record / Field Access }
{ ===================== }

{ This method returns the size of just the data in the record buffer.
  Do not confuse this with RecBufSize which also includes any additonal
  structures stored in the record buffer (such as TRecInfo). }

function TmmMemTable.GetRecordSize: Word;
begin
  Result := FRecInfoOfs;
end;

{ TDataSet calls this method to allocate the record buffer.  Here we use
  FRecBufSize which is equal to the size of the data plus the size of the
  TRecInfo structure. }

function TmmMemTable.AllocRecordBuffer: PChar;
begin
  GetMem(Result, FRecBufSize);
end;

{ Again, TDataSet calls this method to free the record buffer.
  Note: Make sure the value of FRecBufSize does not change before all
  allocated buffers are freed. }

procedure TmmMemTable.FreeRecordBuffer(var Buffer: PChar);
begin
  FreeMem(Buffer, FRecBufSize);
end;

{ This multi-purpose function does 3 jobs.  It retrieves data for either
  the current, the prior, or the next record.  It must return the status
  (TGetResult), and raise an exception if DoCheck is True. }

function TmmMemTable.GetRecord(Buffer: PChar; GetMode: TGetMode;
  DoCheck: Boolean): TGetResult;
begin
  if RecordCount < 1 then
    Result := grEOF
  else
    begin //2
      Result := grOK;
      case GetMode of //3
        gmNext:
          if FCurRec >= (RecordCount - 1)  then
            Result := grEOF
          else
            Inc(FCurRec);
        gmPrior:
          if FCurRec <= 0 then
            Result := grBOF
          else
            Dec(FCurRec);
        gmCurrent:
          if (FCurRec < 0) or (FCurRec >= RecordCount) then
            Result := grError;
      end; //-3
      if Result = grOK then
      begin //3
        case FDataOrigin of
          doInternal: PVariant(Buffer)^ := TMemoryRow(FData[FCurRec]).Data;
          doExternal: Move((TMemoryRow(FData[FCurRec]).DataPtr)^,Buffer^, RecordSize);
        end;
        with PRecInfo(Buffer + FRecInfoOfs)^ do
          begin //4
            BookmarkFlag := bfCurrent;
            Bookmark := Integer(TMemoryRow(FData[FCurRec]).ID);
          end; //-4
// >> Filter
{        if not(FilterOK) then
          begin
            case GetMode of
              gmNext : Result := GetRecord(Buffer, gmNext, DoCheck);
              gmPrior: Result := GetRecord(Buffer, gmPrior, DoCheck);
              gmCurrent: Result := grError;
            end;
          end;}
{        else
          begin
            case FDataOrigin of
              doInternal: PVariant(Buffer)^ := TMemoryRow(FData[FCurRec]).Data;
              doExternal: Move((TMemoryRow(FData[FCurRec]).DataPtr)^,Buffer^, RecordSize);
            end;
            with PRecInfo(Buffer + FRecInfoOfs)^ do
              begin //4
                BookmarkFlag := bfCurrent;
                Bookmark := Integer(TMemoryRow(FData[FCurRec]).ID);
              end; //-4
          end;}
//<<Filter
      end
      else   //-3
        if (Result = grError) and DoCheck then DatabaseError('No Records');
    end; // -2
end;

{ This routine is called to initialize a record buffer.  In this sample,
  we fill the buffer with zero values, but we might have code to initialize
  default values or do other things as well. }

procedure TmmMemTable.InternalInitRecord(Buffer: PChar);
var
  Dum: Variant;
  i  : integer;
begin
  case FDataOrigin of
    doInternal:
      begin
        Dum := VarArrayCreate([0,FieldDefs.Count - 1],varVariant);
        for i := 0 to (FieldDefs.Count-1) do
          case Fields[i].DataType of
            ftInteger   :
              begin
                Dum[Fields[i].FieldNo-1] := VarAsType(Dum[Fields[i].FieldNo-1],varInteger);
                if Fields[I].DefaultExpression <> '' then
                   Dum[Fields[i].FieldNo-1] := Fields[I].DefaultExpression
                else
                   Dum[Fields[i].FieldNo-1] := null;
              end;
            ftSmallInt  :
              begin
                Dum[Fields[i].FieldNo-1] := VarAsType(Dum[Fields[i].FieldNo-1],varSmallInt);
                if Fields[I].DefaultExpression <> '' then
                   Dum[Fields[i].FieldNo-1] := Fields[I].DefaultExpression
                else
                   Dum[Fields[i].FieldNo-1] := null;
              end;
            ftFloat     :
              begin
                Dum[Fields[i].FieldNo-1] := VarAsType(Dum[Fields[i].FieldNo-1],varDouble);
                if Fields[I].DefaultExpression <> '' then
                   Dum[Fields[i].FieldNo-1] := Fields[I].DefaultExpression
                else
                   Dum[Fields[i].FieldNo-1] := null;
              end;
            ftCurrency  :
              begin
                Dum[Fields[i].FieldNo-1] := VarAsType(Dum[Fields[i].FieldNo-1],varCurrency);
                if Fields[I].DefaultExpression <> '' then
                   Dum[Fields[i].FieldNo-1] := StrToCurr(Fields[I].DefaultExpression)
                else
                   Dum[Fields[i].FieldNo-1] := null;
              end;
            ftDate, ftTime, ftDateTime:
              begin
                Dum[Fields[i].FieldNo-1] := VarAsType(Dum[Fields[i].FieldNo-1],varDate);
                if Fields[I].DefaultExpression <> '' then
                   Dum[Fields[i].FieldNo-1] := TDateTime(StrToDateTime(Fields[I].DefaultExpression))
                else
                   Dum[Fields[i].FieldNo-1] := null;
              end;
            ftBoolean   :
              begin
                Dum[Fields[i].FieldNo-1] := VarAsType(Dum[Fields[i].FieldNo-1],varBoolean);
                if Fields[I].DefaultExpression <> '' then
                   Dum[Fields[i].FieldNo-1] := Fields[I].DefaultExpression
                else
                   Dum[Fields[i].FieldNo-1] := null;
              end;
            ftString    :
              begin
                Dum[Fields[i].FieldNo-1] := VarAsType(Dum[Fields[i].FieldNo-1],varString);
                if Fields[I].DefaultExpression <> '' then
                   Dum[Fields[i].FieldNo-1] := Fields[I].DefaultExpression
                else
                   Dum[Fields[i].FieldNo-1] := null;
              end;
          end;
        PVariant(Buffer)^ := Dum;
      end;
    doExternal:
      begin
        FillChar(Buffer^, RecordSize, 0);     // NIY
      end;
    end;
end;

{ Here we copy the data from the record buffer into a field's buffer.
  This function, and SetFieldData, are more complex when supporting
  calculated fields, filters, and other more advanced features.
  See TBDEDataSet for a more complete example. }

function TmmMemTable.GetFieldData(Field: TField; Buffer: Pointer): Boolean;
var
  Dum : Variant;
  Fldofs, i : integer;
  Intgr: Longint;
  Dble : Double;
  Str  : ShortString;
  Bool : LongBool;
  DtTm : Comp;
  Curr : Currency;
begin
  Result := True;
  if (((FCurRec > -1) and (FCurRec < RecordCount)) or (state=dsInsert)) then
  begin
    case FDataOrigin of
      doInternal:
        begin
          Dum := PVariant(ActiveBuffer)^;
          if Buffer <> nil then
          begin
            if VarIsEmpty(Dum[Field.FieldNo - 1]) or VarisNull(Dum[Field.FieldNo - 1]) then
             begin
               //>> hver (17/10/97)
               Result := True;
               case Field.DataType of
                ftDateTime,ftFloat,ftCurrency: Double(Buffer^) := 0.0;
                ftDate,ftTime,ftSmallInt,ftInteger: Integer(Buffer^) := 0;
                ftString: PChar(Buffer)^ := #0;
                ftBoolean: Boolean(Buffer^) := False;
                else Variant(buffer^) := 0;
               end;
               //<< hver (17/10/97)
             end
            else
            case Field.DataType of
              ftDateTime :
                  Double(Buffer^) := TimeStampToMSecs(DateTimeToTimeStamp(Dum[Field.FieldNo - 1]));
              ftDate     :
                  Integer(Buffer^) := DateTimeToTimeStamp(Dum[Field.FieldNo - 1]).Date;
              ftTime     :
                  Integer(Buffer^) := DateTimeToTimeStamp(Dum[Field.FieldNo - 1]).Time;
              ftSmallInt, ftInteger:
                  Integer(Buffer^) := Dum[Field.FieldNo - 1];
              ftFloat:
                  Double(Buffer^) := Dum[Field.FieldNo - 1];
              ftCurrency:
                  Double(Buffer^) := Dum[Field.FieldNo - 1];
              ftString:
                begin
                  if String(Dum[Field.FieldNo - 1]) <> '' then
                     StrLCopy((Buffer),Pointer(String(Dum[Field.FieldNo - 1])),Field.Size)
                  else
                     PChar(Buffer)^ := #0;
                end;
              ftBoolean:
                  Boolean(Buffer^) := Dum[Field.FieldNo - 1];
              else
                  Variant(buffer^) := Dum[Field.FieldNo - 1];
            end;
          end;
        end;
      doExternal:
        begin
          intgr := 0;
          bool  := False;
          dble  := 0.0;
          Str   := '';
          fldofs := 0;
          DtTm  := 0;
          Curr  := 0.0;
          for i:= 0 to (FieldDefs.Count-1) do
             if Fields[i].FieldNo < Field.FieldNo then
                  fldofs := fldofs + Fields[i].DataSize;
          if Buffer <> nil then
          case Field.DataType of
            ftInteger, ftSmallInt, ftWord:
              begin
                Move((ActiveBuffer+fldofs)^,Intgr, Field.DataSize);
                Integer(Buffer^) := Intgr;
              end;
            ftFloat  :
              begin
                Move((ActiveBuffer+fldofs)^,dble, Field.DataSize);
                Double(Buffer^) := Dble;
              end;
            ftString :
              begin
                Move((ActiveBuffer+fldofs+1)^,Buffer^, Field.DataSize-1);
              end;
            ftBoolean:
              begin
                Move((ActiveBuffer+fldofs)^,Bool, Field.DataSize);
                Boolean(Buffer^) := Bool;
              end;
            ftCurrency:
              begin
                Move((ActiveBuffer+fldofs)^,Curr, Field.DataSize);
                Double(Buffer^) := Curr;
              end;
            ftTime:
              begin
                Move((ActiveBuffer+fldofs)^,Intgr, Field.DataSize);
                Integer(Buffer^) := (Intgr);
              end;
            ftDate:
              begin
                Move((ActiveBuffer+fldofs)^,Intgr, Field.DataSize);
                Integer(Buffer^) := (Intgr);
              end;
            ftDateTime:
              begin
                Move((ActiveBuffer+fldofs)^,DtTm, Field.DataSize);
                Double(Buffer^) := DtTm;
              end;
          else
            Move((ActiveBuffer+fldofs)^,Buffer^,Field.DataSize);
          end;
        end;
    end;
  end
  else
    begin
     Result := False;
     if (FCurRec = -1) and (RecordCount > 0) then Result := True
    end;
end;

procedure TmmMemTable.SetFieldData(Field: TField; Buffer: Pointer);
var
  Dum: Variant;
  DumTimeStamp: TTimeStamp;
begin
  case DataOrigin of
    doInternal:
      begin
        Dum := PVariant(ActiveBuffer)^;
        if Buffer <> nil then
          case Field.Datatype of
            ftSmallInt, ftInteger:
              Dum[Field.FieldNo - 1] := Integer(Buffer^);
            ftFloat:
              Dum[Field.FieldNo - 1] := Double(Buffer^);
            ftCurrency:
              Dum[Field.FieldNo - 1] := Double(Buffer^);
            ftString:
              Dum[Field.FieldNo - 1] := Copy(String(Buffer), 1, Field.Size);
            ftBoolean:
              Dum[Field.FieldNo - 1]:= Boolean(Buffer^);
            ftDateTime:
              Dum[Field.FieldNo - 1]:= TimeStampToDateTime(MSecsToTimeStamp(Double(Buffer^)));
            ftDate:
              begin
                DumTimeStamp.Date := Integer(Buffer^);
                DumTimeStamp.Time := 0;
                Dum[Field.FieldNo - 1]:= TimeStampToDateTime(DumTimeStamp);
                VarAsType(Dum[Field.FieldNo - 1], VarDate);
              end;
            ftTime:
              begin
                DumTimeStamp.Time := Integer(Buffer^);
                DumTimeStamp.Date := 693594;
                Dum[Field.FieldNo - 1]:= TimeStampToDateTime(DumTimeStamp);
                VarAsType(Dum[Field.FieldNo - 1], VarDate);
              end;
          end
          else
            Dum[Field.FieldNo - 1] := Unassigned;
        PVariant(ActiveBuffer)^ := Variant(Dum);
        DataEvent(deFieldChange, Longint(Field));
      end;
    doExternal: ; //NIY
  end;
end;

{ Record Navigation / Editing }
{ =========================== }

{ This method is called by TDataSet.First.  Crack behavior is required.
  That is we must position to a special place *before* the first record.
  Otherwise, we will actually end up on the second record after Resync
  is called. }

procedure TmmMemTable.InternalFirst;
begin
  FCurRec := -1;
end;

{ Again, we position to the crack *after* the last record here. }

procedure TmmMemTable.InternalLast;
begin
  FCurRec := RecordCount;
end;

{ This method is called by TDataSet.Post.  Most implmentations would write
  the changes directly to the associated datasource, but here we simply set
  a flag to write the changes when we close the dateset. }

procedure TmmMemTable.InternalPost;
begin
  FSaveChanges := True;
  { For inserts, just update the data in the string list }
  if FCurRec = -1 then Inc(FCurRec);
  if State = dsEdit then
    TMemoryRow(FData[FCurRec]).Data := PVariant(ActiveBuffer)^
  else
  begin
    { If inserting (or appending), increment the bookmark counter and
      store the data }
    FData.InsertRow(FCurRec, PVariant(ActiveBuffer)^);
  end;
end;

{ This method is similar to InternalPost above, but the operation is always
  an insert or append and takes a pointer to a record buffer as well. }

procedure TmmMemTable.InternalAddRecord(Buffer: Pointer; Append: Boolean);
begin
  FSaveChanges := True;
  if Append then InternalLast;
  FData.InsertRow(FCurRec, PVariant(Buffer)^);
end;

{ This method is called by TDataSet.Delete to delete the current record }

procedure TmmMemTable.InternalDelete;
begin
  FSaveChanges := True;
  FData.RemoveRow(FCurRec);
  if FCurRec >= RecordCount then Dec(FCurRec);
end;

{ Optional Methods }
{ ================ }

{ The following methods are optional.  When provided they will allow the
  DBGrid and other data aware controls to track the current cursor postion
  relative to the number of records in the dataset.  Because we are dealing
  with a small, static data store (a stringlist), these are very easy to
  implement.  However, for many data sources (SQL servers), the concept of
  record numbers and record counts do not really apply. }

function TmmMemTable.GetRecordCount: Longint;
begin
  case FDataOrigin of
    doInternal: Result := FData.Count;
    doExternal: Result := DataCount;
  else
    Result := 0;
  end;
end;

function TmmMemTable.GetRecNo: Longint;
begin
  UpdateCursorPos;
  if (FCurRec = -1) and (RecordCount > 0) then
    Result := 1
  else
    Result := FCurRec + 1;
end;

procedure TmmMemTable.SetRecNo(Value: Integer);
begin
  if (Value >= 0) and (Value < RecordCount) then
  begin
    FCurRec := Value - 1;
    Resync([]);
  end;
end;

{ Zet de private parameter FReadOnly op Value - gecopieerd uit DBTables.pas}

procedure TmmMemTable.SetReadOnly(Value: Boolean);
begin
  FReadOnly := Value;
  if DataOrigin = doExternal then
    FReadOnly := True;
end;

function TmmMemTable.GetCanModify: Boolean;
begin
  if ReadOnly then
    Result := False
  else
    Result:= inherited GetCanModify;
end;

procedure TmmMemTable.SetDataLocation(Value: PChar);
begin
  FDataLocation := Value;
end;

procedure TmmMemTable.SetDataCount(Value: Integer);
begin
  FDataCount := Value;
end;

procedure TmmMemTable.SetDataOrigin(Value: TmmDataOrigin);
begin
  FDataOrigin := Value;
  if Value = doExternal then ReadOnly := True;
end;

function TmmMemTable.GetDataOrigin: TmmDataOrigin;
begin
  Result := FDataOrigin;
end;

function Compare(FieldCol: Integer; Item1, Item2: Pointer; Ascend: Boolean; Owner: TmmMemTable): Integer;
var
  res: integer;
  aid: extended;
  ItemVl1, ItemVl2: Variant;
begin
  if (Item1=nil) or (Item2=nil) then
    Result:=0
  else
  begin
    ItemVl1:= TMemoryRow(Item1).RecData[FieldCol];
    ItemVl2:= TMemoryRow(Item2).RecData[FieldCol];
    if (VarIsEmpty(ItemVl1) or VarIsNull(ItemVl1)) then
     begin
      if (VarIsEmpty(ItemVl2) or VarIsNull(ItemVl2)) then
        Res := 0
      else
        Res := +1;
     end
    else if (VarIsEmpty(ItemVl2) or VarIsNull(ItemVl2)) then
      Res := -1
    else
    begin
      case Owner.Fields[FieldCol].DataType of
       ftInteger, ftSmallInt, ftWord:
          begin
            if ItemVl1 < ItemVl2 then
              res := -1
            else if ItemVl1 > ItemVl2 then
              res := 1
            else
              res := 0;
          end;
       ftDateTime, ftDate, ftTime, ftFloat, ftCurrency:
          begin
            aid := ItemVl1 - ItemVl2;
            if abs(aid) < Precision then
               res := 0
            else
             begin
              if Extended(aid) < Precision then
                res := -1
              else
                res := 1;
             end;
          end;
       ftString:
          res := CompareStr(ItemVl1, ItemVl2);
       ftBoolean:
          begin
            if ItemVl1 = ItemVl2 then
              res:= 0
            else if ItemVl1 then
              res:= 1
            else if ItemVl2 then
              res := -1
            else
              res := 0;
          end;
      else
        res := 0;
      end;
    end;
    if Ascend <> True then res := -res;
    Result := res;
  end;
end;

procedure TmmMemTable.Sort(FieldCol: Integer; Ascend: Boolean);
begin
  if (FieldCol >= 0) and (FieldCol <= (FieldDefs.Count-1)) then
    begin
      FData.SortOnColumn(FieldCol, Compare, Ascend);
      Refresh;
    end
  else
    DataBaseError('Invalid FieldCol');
end;

// >> Filter
{function TmmMemTable.FilterOK: Boolean;
begin
  OnFilterRecord(Self,Result)
end;

function TmmMemTable.FilterOK2: Boolean;
var
  HelpVar: Variant;
  HelpBuf: PChar;
begin
   if Filtered then
    begin
      case DataOrigin of
        doInternal:
          begin
            HelpVar := PVariant(ActiveBuffer)^;
            PVariant(ActiveBuffer)^ := TMemoryRow(FData[FCurRec]).Data;
            OnFilterRecord(Self, Result);
            PVariant(ActiveBuffer)^ := HelpVar;
          end;
        doExternal:
          begin
            HelpBuf := '';
            if ActiveBuffer <> nil then
              begin
                Move(ActiveBuffer^,HelpBuf^, FRecInfoOfs);
                Move((TMemoryRow(FData[FCurRec]).DataPtr)^, ActiveBuffer^, FRecInfoOfs);
                OnFilterRecord(Self, Result);
                Move(HelpBuf^, ActiveBuffer^, FRecInfoOfs);
              end
            else Result := True;
          end
      end;
   end else Result := True;
end;

function TmmMemTable.FindRecord(Restart, GoForward: Boolean): Boolean;
var
  FOldCurRec: Integer;
begin
  if (RecordCount > 0) then
    begin
      FOldCurRec:= RecNo;
      if Restart then
           if GoForward then First
                  else Last;
      if GoForward then
         begin
           if not Restart then Next;
           while not(EOF or FilterOK) do Next;
         end else
         begin
           if not Restart then Prior;
           while not(BOF or FilterOK) do Prior;
         end;
      if (FilterOK) then Result:=True
        else
          begin
            Result := False;
            SetRecNo(FOldCurRec);
          end;
    end
  else Result := False;
  SetFound(Result);
end; }
// << Filter;

function TmmMemTable.Locate(const KeyFields: string; const KeyValues: Variant;
      Options: TLocateOptions): Boolean;
var
  FOldCurRec, i: Integer;
  HEOF, HFound, HIntFound: Boolean;
  KValues: Variant;
  SData, SValues: String;
  LFields: TList;
begin
  HEOF := False;
  FOldCurRec := RecNo;
  if Pos(';', KeyFields) = 0 then
    KValues := VarArrayOf([KeyValues])
  else
    KValues := KeyValues;
  LFields := TList.Create;
  try
    GetFieldList(LFields, KeyFields);
    repeat
      HIntFound := True;
      for I := 0 to (LFields.Count - 1) do
      begin
        if ((TField(LFields[I]).DataType = ftString) and (Options <> [])) then
           begin
             if (VarIsNull(TField(LFields[I]).Value) or
                 VarIsNull(TField(LFields[I]).Value)) then
                SData := ''
             else
                SData := TField(LFields[I]).Value;
             SValues := KValues[I];
             if loCaseInsensitive in Options then
               begin
                 SData   := UpperCase(SData);
                 SValues := UpperCase(SValues);
               end;
             if loPartialKey in Options then
                HIntFound := (HIntFound and (Pos(SValues,SData) = 1))
             else
                HIntFound := (HIntFound and (SData = SValues));
           end
        else
            HIntFound := (HIntFound and (TField(LFields[I]).Value = KValues[i]));
      end;
      HFound := HIntFound;
      if not(HFound) then Next;
      if (EOF and not(HFound)) then
        begin
          First;
          HEOF := True;
        end;
    until ((HFound) or ((RecNo >= FOldCurRec) and HEOF));
    Result := HFound;
  finally
    LFields.Free;
  end;
  if not(Result) then SetRecNo(FOldCurRec);
end;

function TmmMemTable.Lookup(const KeyFields: string; const KeyValues: Variant;
  const ResultFields: string): Variant;
var
  FOldCurRec : Integer;
begin
  FOldCurRec := RecNo;
  if Locate(KeyFields, KeyValues, []) then
    Result := FieldValues[ResultFields]
  else
    Result := False;
  if FOldCurRec >= RecordCount then
    Last
  else
    SetRecNo(FOldCurRec);
end;

function TmmMemTable.GetCurrentRecord(Buffer: PChar): Boolean;
begin
  if not IsEmpty and (GetBookmarkFlag(ActiveBuffer) = bfCurrent) then
  begin
    UpdateCursorPos;
    Result := (GetRecord(Buffer, gmCurrent, False) = grOK);
  end
  else
    Result := False;
end;

function TmmMemTable.BookmarkValid(Bookmark: TBookmark): Boolean;
begin
  Result := FData.FindItemID(PInteger(Bookmark)^) <> nil;
end;

function TmmMemTable.CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer;
begin
  if Bookmark1 = nil then
   begin
    if Bookmark2 = nil then
      Result := 0
    else
      Result := 1;
   end
  else
   begin
    if Bookmark2 = nil then
      Result := 1
    else
     begin
      if (PInteger(Bookmark1)^) = (PInteger(Bookmark2)^) then
        Result := 0
      else
       Result := 1;
     end;
   end;
end;

procedure TmmMemTable.SetUserAccess(Value: TmmUserAccess);
begin
  if FUserAccess <> Value then
  begin
    FUserAccess := Value;
    if Value = uaReadOnly then
      ReadOnly := True
    else
      ReadOnly := False;
  end;
end;

procedure TmmMemTable.ProtectPKFields;
var
  I:integer;
begin
  for I := 0 to FieldCount-1 do
  begin
    if (Fields[I].Tag and 1) = 1 then
      Fields[I].ReadOnly := TRUE;
  end;
end;

procedure TmmMemTable.UnprotectPKFields;
var
  I:integer;
begin
  for I := 0 to FieldCount-1 do
  begin
    if (Fields[I].Tag and 1) = 1 then
      Fields[I].ReadOnly := FALSE;
  end;
end;

procedure TmmMemTable.DoBeforeOpen;
begin
  inherited;
  ProtectPKFields;
end;

procedure TmmMemTable.DoAfterPost;
begin
  inherited;
  ProtectPKFields;
end;

procedure TmmMemTable.DoAfterCancel;
begin
  inherited;
  ProtectPKFields;
end;

procedure TmmMemTable.DoBeforeInsert;
begin
  if (UserAccess <> uaFull) then Abort;
  UnprotectPKFields;
  inherited;
end;

procedure TmmMemTable.DoBeforeDelete;
begin
  if (UserAccess <> uaFull) then Abort;
  inherited;
end;

procedure TmmMemTable.DoBeforePost;
var
  I:integer;
begin
 for I := 0 to FieldCount-1 do
  begin
    case Fields[I].DataType of
     ftstring:
       if ((Fields[I].Value = '') and ((Fields[I].Tag and 2) = 2)) then
       begin
         Fields[I].Value := Null;
       end;
     ftSmallInt,ftInteger,ftWord,ftFloat,ftDate,ftTime,ftDateTime:
       if ((Fields[I].Value = 0) and ((Fields[I].Tag and 2) = 2)) then
       begin
         Fields[I].Value := null;
       end;
    end;
  end;
  inherited;
end;

function TmmMemTable.GetPKFields : TStringList;
var
  I: Integer;
begin
  {Fill in PKFields using loop activesource.dataset.fields[n].tag = 1}
  if FPKFields.Count = 0 then
  begin
    for I := 0 to FieldCount - 1 do
      if ((Fields[I].Tag and 1) = 1) then
          FPKFields.Add(Fields[I].FieldName);
  end;
  Result := FPKFields;
end;

{-----------------------------------------------------------------------------}
{ TMemoryRows }

{ Create, GetItem, SetItem, Add: gecopieerd van ComCtrls.pas}

constructor TMemoryRows.Create(AOwner: TmmMemTable);
begin
  inherited Create;
  FOwner := AOwner;
  FNextID:= 0;
end;

destructor TMemoryRows.Destroy;
begin
  while Count > 0 do
    TMemoryRow(Items[Count-1]).Free;
  FUpdateCount := 1;
  FOwner := nil;
  inherited;
end;

procedure TMemoryRows.InsertRow(Index: Integer; Value: Variant);
begin
  TMemoryRow.Create(Self, Index);
  TMemoryRow(Items[Index]).Data := Value;
end;

procedure TMemoryRows.RemoveRow(Index: Integer);
begin
  if (Index < 0) or (Index >= Count) then DatabaseError('Invalid Index: '+IntToStr(Index));
  TMemoryRow(Items[Index]).Free;
end;

function TMemoryRows.FindItemID(ID: Integer): TMemoryRow;
var
  I: Integer;
begin
  for I := 0 to Count-1 do
  begin
    Result := TMemoryRow(Items[I]);
    if Result.ID = ID then Exit;
  end;
  Result := nil;
end;

procedure TMemoryRows.InsertItem(Ind: Integer; Item: TMemoryRow);
begin
  Insert(Ind, Item);
  Item.FMemoryRows := Self;
  Item.FID := FNextID;
  Inc(FNextID);
  Changed;
end;

procedure TMemoryRows.RemoveItem(Item: TMemoryRow);
begin
  Remove(Item);
  Item.FMemoryRows := nil;
  Changed;
end;

procedure TMemoryRows.BeginUpdate;
begin
  Inc(FUpdateCount);
end;

procedure TMemoryRows.Changed;
begin
  if FUpdateCount = 0 then Update(nil);
end;

procedure TMemoryRows.EndUpdate;
begin
  Dec(FUpdateCount);
  Changed;
end;

procedure TMemoryRows.Update(Item: TMemoryRow);
begin
end;

procedure TMemoryRows.SortOnColumn(FieldCol:Integer; Compare: TListSortCompare; Ascend: Boolean);
begin
  if (List <> nil) and (Owner.RecordCount > 0) then
     QuickSort(FieldCol, 0, Owner.RecordCount - 1, Compare,Ascend);
end;

procedure TMemoryRows.QuickSort(FieldCol:Integer; L, R: Integer;
  SCompare: TListSortCompare; Ascend: Boolean);
var
  I, J: Integer;
  P: Pointer;
begin
  repeat
    I := L;
    J := R;
    P := List^[(L + R) shr 1];
    repeat
      while SCompare(FieldCol, (List^[I]), P,Ascend,FOwner) < 0 do Inc(I);
      while SCompare(FieldCol, (List^[J]), P,Ascend,FOwner) > 0 do Dec(J);
      if I <= J then
      begin
        if (SCompare(FieldCol, List^[I], List^[J],Ascend, FOwner) <> 0) then
              Exchange(I,J);
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then QuickSort(FieldCol, L, J, SCompare,Ascend);
    L := I;
  until I >= R;
end;


{-----------------------------------------------------------------------------}
{ TMemoryRow }

{Create, Destroy en Assign zijn +/- copies van die functies uit ComCtrls.pas}

constructor TMemoryRow.Create(AMemoryRows: TMemoryRows; Index: Integer);
var
  i: integer;
begin
  inherited Create;
  AMemoryRows.InsertItem(Index, Self);
  if AMemoryRows.FOwner.DataOrigin = doInternal then
    begin
     Data := VarArrayCreate([0,AMemoryRows.Owner.FieldDefs.Count - 1],varVariant);
     with AMemoryRows.Owner do
       for i := 0 to FieldDefs.Count-1 do
         case FieldDefs[i].DataType of
           ftInteger   : Data[I] := VarAsType(Data[I],varInteger);
           ftSmallInt  : Data[I] := VarAsType(Data[I],varSmallInt);
           ftFloat     : Data[I] := VarAsType(Data[I],varDouble);
           ftCurrency  : Data[I] := VarAsType(Data[I],varCurrency);
           ftDate, ftDateTime, ftTime:
                         Data[I] := VarAsType(Data[I],varDate);
           ftBoolean   : Data[I] := VarAsType(Data[I],varBoolean);
           ftString    : Data[I] := VarAsType(Data[I],varString);
         end;
    end
  else
    Data := 0;
end;

destructor TMemoryRow.Destroy;
begin
  if FMemoryRows.Owner.FDataOrigin = doInternal then
    VarArrayRedim(Data,0)
  else
    DataPtr := nil;
  FMemoryRows.RemoveItem(self);
  inherited;
end;

function TMemoryRow.Get(Index: Integer): Variant;
var
  i, FldOfs : Integer;
  intgr: Longint;
  dble: Double;
  bool: Longbool;
  Str : Shortstring;
  Curr: Currency;
  Dttm: Comp;
  TmStmp: TTimeStamp;
  Ind: integer;
begin
  Ind := Index;
  case FMemoryRows.Owner.FDataOrigin of
    doInternal:
       Result := Data[(FMemoryRows.Owner.Fields[Ind].FieldNo - 1)];
    doExternal:
       begin
         intgr := 0;
         bool  := False;
         dble  := 0.0;
         Str   := '';
         Curr  := 0.0;
         fldofs := 0;
         with FMemoryRows.Owner do
         begin
           for i:= 0 to (FieldDefs.Count-1) do
              if Fields[i].FieldNo < Fields[Index].FieldNo then
                 fldofs := fldofs + Fields[i].DataSize;
           case Fields[Index].DataType of
             ftSmallInt, ftInteger, ftWord:
               begin
                 Move((DataPtr+fldofs)^,Intgr, Fields[Index].DataSize);
                 Result := Intgr;
               end;
             ftFloat  :
               begin
                 Move((DataPtr+fldofs)^,dble, Fields[Index].DataSize);
                 Result := Dble;
               end;
             ftString :
               begin
                 Move((DataPtr+Fldofs)^,Str, Fields[Index].DataSize);
                 Result := Str;
               end;
             ftBoolean:
               begin
                 Move((DataPtr+fldofs)^,Bool, Fields[Index].DataSize);
                 Result := Bool;
               end;
             ftCurrency:
               begin
                 Move((DataPtr+fldofs)^,Curr, Fields[Index].DataSize);
                 Result := Curr;
               end;
             ftDateTime:
               begin
                 Move((DataPtr+fldofs)^,DtTm, Fields[Index].DataSize);
                 Result := TimeStampToDateTime(MSecsToTimeStamp(Comp(DtTm)));
               end;
             ftDate:
               begin
                 Move((DataPtr+fldofs)^,Intgr, Fields[Index].DataSize);
                 TmStmp.Date := Intgr;
                 Result := TimeStampToDateTime(TmStmp);
               end;
             ftTime:
               begin
                 Move((DataPtr+fldofs)^,Intgr, Fields[Index].DataSize);
                 TmStmp.Time := Intgr;
                 Result := TimeStampToDateTime(TmStmp);
               end;
           end;
         end;
       end;
  end;
end;

procedure TMemoryRow.Put(Index: Integer; Item: Variant);
{ Writing Data to the MemTable is disabled if the DataOrigin <> doInternal}
begin
  case FMemoryRows.Owner.DataOrigin of
    doInternal: Data[Index] := Item;
    doExternal: ;       // NIY
  end;
end;

procedure TMemoryRow.Assign(Source: TPersistent);
begin
  if Source is TMemoryRow then
    begin
      Data      := TMemoryRow(Source).Data;
      DataPtr   := TMemoryRow(Source).DataPtr;
    end
  else
    inherited Assign(Source);
end;

function TMemoryRow.GetIndex: Integer;
begin
  if FMemoryRows <> nil then
    Result := FMemoryRows.IndexOf(Self)
  else
    Result := -1;
end;

procedure TMemoryRow.Changed(AllItems: Boolean);
var
  Item: TMemoryRow;
begin
  if (FMemoryRows <> nil) and (FMemoryRows.FUpdateCount = 0) then
  begin
    if AllItems then
      Item := nil
    else
      Item := Self;
    FMemoryRows.Update(Item);
  end;
end;

procedure TMemoryRow.SetIndex(Value: Integer);
var
  CurIndex: Integer;
begin
  CurIndex := GetIndex;
  if (CurIndex >= 0) and (CurIndex <> Value) then
  begin
    FMemoryRows.Move(CurIndex, Value);
    Changed(True);
  end;
end;



{-----------------------------------------------------------------------------}
{ This procedure is used to register this component on the component palette }

//procedure Register;
//begin
//  RegisterComponents('Data Access', [TmmMemTable]);
//end;

end.
