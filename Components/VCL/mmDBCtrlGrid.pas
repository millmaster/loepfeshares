(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDBCtrlGrid.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmDBCtrlGrid;

interface

uses
  DBCtrls, DBCGrids, Classes, DBTables, mmMemTable;

type
  TmmDBCtrlGrid = class(TDBCtrlGrid)
  private
    FDataSourceActive: Boolean;
    { Private declarations }
  protected
    procedure Loaded; override;
    property DataSourceActive: Boolean read FDataSourceActive;
    procedure SetDataSourceActive; virtual;
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    { Public declarations }
  published
    { Published declarations }
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//procedure Register;
//begin
//  RegisterComponents('Data Controls', [TmmDBCtrlGrid]);
//end;

constructor TmmDBCtrlGrid.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDataSourceActive := False;
end;

procedure TmmDBCtrlGrid.Loaded;
begin
  inherited Loaded;
  // automatically open lookup source...
  if (not (csDesigning in ComponentState)) and
     (not DataSourceActive) then SetDataSourceActive;
end;

procedure TmmDBCtrlGrid.SetDataSourceActive;
//var
//  I: integer;
begin
  if (DataSource = nil) then Exit;
  if (DataSource.Enabled = False) then Exit;
//  if (DataSource.DataSet is TTable) then
//    with DataSource.DataSet as TTable do
//    begin
//     for I := 0 to FieldCount - 1 do  // activate lookup fields
//     begin
//       if Fields[I].Lookup then
//       begin
//         if (not TTable(Fields[I].LookupDataSet).Active) then
//           TTable(Fields[I].LookupDataSet).Open;
//       end;
//     end;
//    end;
///
///
  if not FDataSourceActive then    // activate datasource (e.g. detail table)
  begin
    if (DataSource.DataSet is TTable) or
       (DataSource.DataSet is TmmMemTable) then
      with DataSource.DataSet do
      begin
        if not Active then Open;
        FDataSourceActive := True;
      end;
{    else if (DataSource.DataSet is TQuery) then
      with DataSource.DataSet as TQuery do
      begin
        if not Active then Open;
        FDataSourceActive := True;
      end}
  end;
end;

end.
