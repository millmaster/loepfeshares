(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: regTEE.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the chart components for 'Addition, DataControls, QReport'
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit regTEE;

interface

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  Classes, mmChart, mmDBChart, mmQRChart;

procedure Register;
begin
  RegisterComponents('Additional',[
                          TmmChart
                                ]);
  RegisterComponents('Data Controls',[
                          TmmDBChart
                                ]);
  RegisterComponents('QReport',[
                          TmmQRChart
                                ]);
end;

end.
