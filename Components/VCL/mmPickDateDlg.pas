(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmPickDateDlg.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmPickDateDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TmmPickDateDlg = class(TComponent)
  private
    FDate: TDateTime;
    FFieldName: string;
  protected
    property FieldName: string read FFieldName write FFieldName;
  public
    property DateValue: TDateTime read FDate write FDate;
    function Execute: Boolean; virtual;
  published
    { Published declarations }
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  DlgPickDate;

//procedure Register;
//begin
//  RegisterComponents('Samples', [TmmPickDateDlg]);
//end;

function TmmPickDateDlg.Execute: Boolean;
begin
  DlgPick_Date := TDlgPick_Date.Create(Application);
  try
    DlgPick_Date.DateValue := DateValue;
    DlgPick_Date.Caption   := FieldName;
    Result := (DlgPick_Date.ShowModal = IDOK);
    if Result then DateValue := DlgPick_Date.DateValue;
  finally
    DlgPick_Date.Free;
  end;
end;

end.
