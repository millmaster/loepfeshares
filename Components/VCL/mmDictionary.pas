(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDictionary.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 18.02.1999  1.00  Wss | Initial Release
| 03.03.1999  1.01  Wss | AutoConfig property implemented
|                       | Force the MDIChild to redraw its system menu after changing the language
| 11.03.1999  1.02  Wss | AutoConfig changed to: acNone, acLoepfe, acFloor, acBoth
| 07.04.1999  1.03  Wss | Registry constants has changed to access language settings
| 08.04.1999  1.04  Wss | AutoConfig changed again: get from registry which company runs with MM
| 17.05.1999  1.05  Wss | if FileName doesn't exists check for local path or set to '' and native
| 04.06.1999  1.06  Wss | On language change recreating of maximized childform now in TIvDictionary
| 16.12.1999  1.07  Wss | with Binding = None language changes goes through Language property:
|                       | Property PrimaryLanguage overriden to perform changes in AutoMode
| 27.01.2000  1.08  Wss | After AutoMode set to True call Open
| 01.02.2000  1.09  Wss | In ReadLanguageConfig: if file doesn't exist set FileName := ''
| 02.02.2000  1.10  Wss | Property LoepfeIndex provides an index for Loepfe Languages documents
|                       | for TMMHtmlHelp component: -> see definition in Intranet / Standards
| 14.04.2000  1.10  Wss | Dependencies of Floor locale settings implemented: fFloorCheck
| 29.06.2000  1.10  Wss | OnAfterLangChange property implemented for changing help file
| 10.07.2002  1.07  Wss | Erweiterungen/Anpassungen fuer Chinesisch
                          - Property IsChineseSimpl und IsChineseTrad eingefuegt
                          - Fuer Sprachumschaltung eine neue Enumeration definiert, wo die
                            Chinesische Sprache unterscheidet. Nur mit Primary geht es nicht.
| 16.07.2002  1.07  Wss | Es wird nun im AutoConfig Mode f�r alle Applikationen die gleiche
                          Dictionary Dateo MMGlossary.mld verwendet -> einfachern zu warten
| 23.07.2002  1.07  Wss | Mit MasterLanguage kann nun bei fehlenden �bersetzungen eine 2. Sprache
                          angegeben werden. Es wird default auf Englisch gesetzt und ist vorerst
                          pber die Registry ..\Debug\MasterLanguage = n steuerbar (default = 2)
| 26.09.2002  1.07  Wss | Vergleichen der Chinesischen Spracheninformationen war nicht korrekt
                          Statt des Properties Languages[i].LangId muss Languages[i].Primary
                          verglichen werden, um die richtige Sprache aus dem Glossary zu verwenden.
                          Bis auf Traditional Chinesisch sind LangId und Primary identisch.
| 17.09.2003  1.07  Wss | T�rkisch hinzugef�gt
| 14.10.2003  1.07  Wss | T�rkisch: Kennung f�r Floor hinzugef�gt
| 07.02.2006  1.07  Wss | Bezeichnungen f�r jede Sprache hinzugef�gt
|=========================================================================================*)
unit mmDictionary;

interface

uses
  Classes, Windows, IvBinDic;

type
  TAutoConfig = (acNone, acLoepfe, acFloor, acBoth);
  TGlossaryLanguage = (glNative, glGerman, glEnglish, glSpanish, glPortuguese,
                        glItalian, glFrench, glChineseTrad, glChineseSimpl, glTurkish);

  TLanguageIdentifier = record
    Name: String;
    LangID: Integer;
    SubID: Integer;
    LoepfeIndex: Integer;
    BarcoLangId: String;
  end;
  //..........................................................
  TmmDictionary = class (TIvBinaryDictionary)
  private
    FAutoConfig: Boolean;
    FFloorCheck: Boolean;
    FGlossaryLanguage: TGlossaryLanguage;
    FLoepfeIndex: Integer;
    FOnAfterLangChange: TNotifyEvent;
    function GetIsChineseSimpl: Boolean;
    function GetIsChineseTrad: Boolean;
    procedure SetAutoConfig(const Value: Boolean);
    procedure SetGlossaryLanguage(const Value: TGlossaryLanguage);
  protected
    procedure LanguageChanged(languageChanged, localeChanged: Boolean); override;
  public
    constructor Create(aOwner: TComponent); override;
    function LoepfeSystemLanguage: TGlossaryLanguage;
    procedure Init;
    procedure Loaded; override;
    procedure Open; override;
    property FloorCheck: Boolean read FFloorCheck write FFloorCheck default False;
    property GlossaryLanguage: TGlossaryLanguage read FGlossaryLanguage write SetGlossaryLanguage;
    property IsChineseSimpl: Boolean read GetIsChineseSimpl;
    property IsChineseTrad: Boolean read GetIsChineseTrad;
    property LoepfeIndex: Integer read FLoepfeIndex;
  published
    property AutoConfig: Boolean read FAutoConfig write SetAutoConfig default False;
    property OnAfterLangChange: TNotifyEvent read FOnAfterLangChange write FOnAfterLangChange;
  end;
  //..........................................................

const
  cLoepfeLanguageID: Array[glGerman..glTurkish] of TLanguageIdentifier = (
    (Name: '(*)Deutsch'; // ivlm
     LangID: LANG_GERMAN;     SubID: SUBLANG_DEFAULT;            LoepfeIndex: 1;  BarcoLangID: 'T0407'),
    (Name: '(*)Englisch'; // ivlm
     LangID: LANG_ENGLISH;    SubID: SUBLANG_DEFAULT;            LoepfeIndex: 3;  BarcoLangID: 'T0809'),
    (Name: '(*)Spanisch'; // ivlm
     LangID: LANG_SPANISH;    SubID: SUBLANG_DEFAULT;            LoepfeIndex: 4;  BarcoLangID: 'T040A'),
    (Name: '(*)Portugiesisch'; // ivlm
     LangID: LANG_PORTUGUESE; SubID: SUBLANG_DEFAULT;            LoepfeIndex: 6;  BarcoLangID: 'T0816'),
    (Name: '(*)Italienisch'; // ivlm
     LangID: LANG_ITALIAN;    SubID: SUBLANG_DEFAULT;            LoepfeIndex: 5;  BarcoLangID: 'T0410'),
    (Name: '(*)Franzoesisch'; // ivlm
     LangID: LANG_FRENCH;     SubID: SUBLANG_DEFAULT;            LoepfeIndex: 2;  BarcoLangID: 'T040C'),
    (Name: '(*)Chinesisch Trad.'; // ivlm
     LangID: LANG_CHINESE;    SubID: SUBLANG_DEFAULT;            LoepfeIndex: 10; BarcoLangID: 'T0404'),
    (Name: '(*)Chinesisch Simpl.'; // ivlm
     LangID: LANG_CHINESE;    SubID: SUBLANG_CHINESE_SIMPLIFIED; LoepfeIndex: 10; BarcoLangID: 'T0804'),
    (Name: '(*)Tuerkisch'; // ivlm
     LangID: LANG_TURKISH;    SubID: SUBLANG_DEFAULT;            LoepfeIndex: 7;  BarcoLangID: 'T041F')
//    (WindowsLangID: LANG_RUSSIAN;    LoepfeIndex: 8;  BarcoLangID: ''),  // Russisch
//    (WindowsLangID: LANG_JAPANESE;   LoepfeIndex: 9;  BarcoLangID: ''), // Japanisch
  );
  //..........................................................

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  Consts, Controls, Dialogs, Forms, Messages, SysUtils, LoepfeGlobal, IvDictio,
  mmRegistry;

const
  cRegGlossaryIndex = 'GlossaryIndex'; // Number
  cMMGlossaryFile   = 'MMGlossary.mld';

  // Traditional		SubL	WinCode	Binaer
  // Taiwan	    0404	1	4    	00000100
  // Hong Kong	    0c04	3	4    	00001100
  //
  // Simplified
  // Singapore	    1004	4	4    	00010000
  // China PRC	    0804	2	4    	00001000
  cChineseTaiwan    = $0404;
  cChineseHongKong  = $0c04;
  cChineseSingapore = $1004;
  cChinesePRC       = $0804;

//------------------------------------------------------------------------------
//--- TmmDictionary
//------------------------------------------------------------------------------
constructor TmmDictionary.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fAutoConfig       := False;
  fFloorCheck       := False;
  fGlossaryLanguage := glNative;
  fLoepfeIndex      := 3;
end;

//------------------------------------------------------------------------------
function TmmDictionary.GetIsChineseSimpl: Boolean;
var
  xLangID: Word;
begin
  xLangID := GetSystemDefaultLangID;
  Result := (xLangID = cChineseSingapore) or (xLangID = cChinesePRC);
end;

//------------------------------------------------------------------------------
function TmmDictionary.GetIsChineseTrad: Boolean;
var
  xLangID: Word;
begin
  xLangID := GetSystemDefaultLangID;
  Result := (xLangID = cChineseTaiwan) or (xLangID = cChineseHongKong);
end;

//------------------------------------------------------------------------------
procedure TmmDictionary.Init;
var
  xStr: string;
  i, xGL: TGlossaryLanguage;
begin
//  EnterMethod('Init');
  // set defaults
  xGL := glEnglish;
  if not(csDesigning in ComponentState) and fAutoConfig then begin
    // in AutoConfig mode we use always the system settings of locale information
    if FileName <> '' then begin
      // Language is translated usually always -> at missing translations use English
      MasterLanguage :=  GetRegInteger(cRegLM, cRegMMDebug, 'MasterLanguage', 2);
      // read language identifier from Floor
      if fFloorCheck then begin
        // read Floor locale registry. UK english as default
        xStr := GetRegString(cRegLM, cRegBarcoLocalePath, cRegBarcoLanguage, 'T0809');
        for i:=glGerman to High(TGlossaryLanguage) do begin
          if CompareText(xStr, cLoepfeLanguageID[i].BarcoLangId) = 0 then begin
            xGL := i;
            Break;
          end;
        end; // for
      end else begin
        // readout default language settings
        xGL := TGlossaryLanguage(GetRegInteger(cRegLM, cRegMMCommonPath, cRegGlossaryIndex, ord(fGlossaryLanguage)));
      end;
      GlossaryLanguage := xGL;
    end;
  end;
end;

//------------------------------------------------------------------------------
procedure TmmDictionary.LanguageChanged(languageChanged, localeChanged: Boolean);
var
  i: TGlossaryLanguage;
begin
//  EnterMethod('LanguageChanged');
  // Language has changed -> write values into registry
  inherited LanguageChanged(languageChanged, localeChanged);

  if not(csDesigning in ComponentState) then begin
    // LOEPFE has it's own language ID's. Provide the index in a property for TMMHtmlHelp
    if languageChanged then begin
      // retrieve LoepfeIndex from current language
      fLoepfeIndex := 3;  // default English
      for i:=glGerman to High(TGlossaryLanguage) do begin
        if (LanguageData.Primary    = cLoepfeLanguageID[i].LangID) and
           (LanguageData.DefaultSub = cLoepfeLanguageID[i].SubID) then begin
//          CodeSite.SendInteger('fLoepfeIndex', fLoepfeIndex);
          fLoepfeIndex := cLoepfeLanguageID[i].LoepfeIndex;
          // write language identifier for Floor
          if fAutoConfig then begin
            if fFloorCheck then begin
//              CodeSite.SendString('fFloorCheck', cLoepfeLanguageID[i].BarcoLangId);
              SetRegString(cRegLM, cRegBarcoLocalePath, cRegBarcoLanguage, cLoepfeLanguageID[i].BarcoLangId);
            end;
            // write GlossaryIndex to LOEPFE path
//            CodeSite.SendInteger('fGlossaryLanguage', ord(fGlossaryLanguage));
            SetRegInteger(cRegLM, cRegMMCommonPath, cRegGlossaryIndex, ord(fGlossaryLanguage));
          end;
          Break;
        end; // if
      end; // for
    end; // if languageChanged

    if Assigned(fOnAfterLangChange) then
      fOnAfterLangChange(Self);
  end; // if not
end;

//------------------------------------------------------------------------------
procedure TmmDictionary.Loaded;
begin
//  EnterMethod('Loaded');
  inherited Loaded;
  Init;
end;

//------------------------------------------------------------------------------
function TmmDictionary.LoepfeSystemLanguage: TGlossaryLanguage;
var
  xLangID: Word;
begin
  xLangID := GetSystemDefaultLangID;
  if (xLangID = cChineseSingapore) or (xLangID = cChinesePRC) then Result := glChineseSimpl
  else if (xLangID = cChineseTaiwan) or (xLangID = cChineseHongKong) then Result := glChineseTrad
  else if (xLangID and LANG_TURKISH) = LANG_TURKISH then Result := glTurkish
  else
    Result := glEnglish;
end;

procedure TmmDictionary.Open;
begin
//  EnterMethod('Open');
  if not IsOpen then
    Init;
  
  inherited Open;
end;

//------------------------------------------------------------------------------
procedure TmmDictionary.SetAutoConfig(const Value: Boolean);
var
//  xFilePath: string;
  xFileName: string;
begin
  if FAutoConfig <> Value then
  begin
    FAutoConfig := Value;
    if not(csDesigning in ComponentState) and fAutoConfig then begin

      // Check Filename and extend with full path
//      CodeSite.SendString('mmDictionary.FileName', FileName);
      if FileName = '' then begin
        // name of dictionary is the value of global variable of application name with other extension
        xFileName := GetRegString(cRegLM, cRegMMCommonPath, cRegHelpFilePath, '');
        if xFileName <> '' then
          xFileName := IncludeTrailingBackslash(xFileName);
      end else begin
        // extract file name from given property value
        xFileName := ExtractFilePath(Application.ExeName);
      end;
      // dictionary file has to be in the same directory as application

      xFileName := xFileName + cMMGlossaryFile;
//      CodeSite.SendString('mmDictionary.xFileName', xFileName);
      if FileExists(xFileName) then begin
        // in AutoConfig mode we use always the system settings of locale information
        Binding    := ivbiNone;
        CheckLevel := ivclSystem;
        FileName := xFileName
      end else begin
//        CodeSite.SendMsg('Dictionary file does not exist!');
        FileName := '';
      end;
    end;
  end;
end;

//------------------------------------------------------------------------------
procedure TmmDictionary.SetGlossaryLanguage(const Value: TGlossaryLanguage);
var
  i: Integer;
begin
//  EnterMethod('SetGlossaryLanguage');
  if fGlossaryLanguage <> Value then begin
//    CodeSite.SendInteger('Value', ord(Value));
//    CodeSite.SendInteger('LangID', cLoepfeLanguageID[Value].LangID);
//    CodeSite.SendInteger('SubID', cLoepfeLanguageID[Value].SubID);
//    CodeSite.SendInteger('LanguageCount', LanguageCount);
    for i:=1 to LanguageCount-1 do begin
//      if (Languages[i].LangId     = cLoepfeLanguageID[Value].LangID) and
//    CodeSite.EnterMethod('Language: ' + Languages[i].EnglishName);
//    CodeSite.SendInteger('Primary', Languages[i].Primary);
//    CodeSite.SendInteger('LangId', Languages[i].LangId);
//    CodeSite.SendInteger('Locale', Languages[i].Locale);
//    CodeSite.SendInteger('DefaultLocale', Languages[i].DefaultLocale);
//    CodeSite.SendInteger('ActiveLocale', Languages[i].ActiveLocale);
//    CodeSite.SendInteger('CodePage', Languages[i].CodePage);
//    CodeSite.SendInteger('Sub', Languages[i].Sub);
//    CodeSite.SendInteger('DefaultSub', Languages[i].DefaultSub);
//    CodeSite.SendInteger('ActiveSub', Languages[i].ActiveSub);
//    CodeSite.SendInteger('', Languages[i].);
//    CodeSite.ExitMethod('');
      if (Languages[i].Primary    = cLoepfeLanguageID[Value].LangID) and
         (Languages[i].DefaultSub = cLoepfeLanguageID[Value].SubID) then begin
//        CodeSite.SendInteger('Gefunden: LanguageIndex', i);
        fGlossaryLanguage := Value;
        Language := i;
        Break;
      end;
    end;
  end;
end;


end.

