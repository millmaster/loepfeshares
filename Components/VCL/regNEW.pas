(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: regNEW.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components in different pages
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit regNEW;

interface

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  Classes,
  mmDictionary, mmTranslator,
  mmRenameDlg, {mmFindDlg, }mmAboutDlg, mmStringTable, mmColorGrid;

procedure Register;
begin
  RegisterComponents('LOEPFE',[
                          TmmDictionary,
                          TmmTranslator
                          ]);

  RegisterComponents('Samples',[
                          TmmRenameDlg,
//                          TmmFindDlg,
                          TmmAboutDlg,
                          TmmStringTable,
                          TmmColorGrid
                                ]);
end;

end.
