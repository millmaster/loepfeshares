(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmAniamte.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 29.05.1999  0.00  Wss | Initial Release
| 10.06.1999  1.00  Wss | Error handling implemented in SetControl() methode
|=========================================================================================*)
unit AutoLabelClass;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TLabelPosition = (lpLeft, lpRight, lpTop, lpBottom);

  TAutoLabel = class(TPersistent)
  private
    fControl: TLabel;
    fDistance: Integer;
    fLabelPosition: TLabelPosition;
    mOwner: TControl;
    procedure SetControl(const Value: TLabel);
    procedure SetLabelPosition(const Value: TLabelPosition);
    procedure SetDistance(const Value: Integer);
    procedure SetEnabled(const Value: Boolean);
    procedure SetVisible(const Value: Boolean);
    function GetEnabled: Boolean;
    function GetVisible: Boolean;
  protected
  public
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property Visible: Boolean read GetVisible write SetVisible;

    procedure Assign(Source: TPersistent); override;
    constructor Create(aOwner: TComponent); virtual;
    destructor Destroy; override;
    procedure SetLabelPos;
  published
    property Control: TLabel read fControl write SetControl;
    property Distance: Integer read fDistance write SetDistance default 2;
    property LabelPosition: TLabelPosition read fLabelPosition write SetLabelPosition;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmLabel;

//******************************************************************************
// TAutoLabel
//******************************************************************************
procedure TAutoLabel.Assign(Source: TPersistent);
begin
  if Source is TAutoLabel then begin
    fDistance      := TAutoLabel(Source).Distance;
    fLabelPosition := TAutoLabel(Source).LabelPosition;
    Control        := TAutoLabel(Source).Control;
    exit;
  end;
  inherited Assign(Source);
end;
//------------------------------------------------------------------------------
constructor TAutoLabel.Create(aOwner: TComponent);
begin
  inherited Create;

  fControl  := Nil;
  fDistance := 2;
  mOwner := TControl(aOwner);
end;
//------------------------------------------------------------------------------
destructor TAutoLabel.Destroy;
begin
  if Assigned(fControl) then
    fControl.FocusControl := Nil;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TAutoLabel.GetEnabled: Boolean;
begin
  if Assigned(fControl) then
    Result := fControl.Enabled
  else
    Result := False;
end;
//------------------------------------------------------------------------------
function TAutoLabel.GetVisible: Boolean;
begin
  if Assigned(fControl) then
    Result := fControl.Visible
  else
    Result := False;
end;
//------------------------------------------------------------------------------
procedure TAutoLabel.SetLabelPos;
begin
  if Assigned(fControl) then
    with fControl do begin
      case LabelPosition of
        lpRight: begin
            Left := mOwner.Left + mOwner.Width + fDistance;
            Top  := mOwner.Top + (mOwner.Height - Height) div 2;
          end;
        lpTop: begin
            Left := mOwner.Left;
            Top  := mOwner.Top - Height - fDistance;
          end;
        lpBottom: begin
            Left := mOwner.Left;
            Top  := mOwner.Top + mOwner.Height + fDistance;
          end;
      else // lpLeft
        Left := mOwner.Left - Width - fDistance;
        Top  := mOwner.Top + (mOwner.Height - Height) div 2;
      end;
    end;
end;
//------------------------------------------------------------------------------
procedure TAutoLabel.SetControl(const Value: TLabel);
begin
  // if fControl contains already a TLabel reset FocusControl link to this component
  if (Value <> fControl) and (Value <> mOwner) then begin
    if Assigned(fControl)  then begin
      fControl.FocusControl := Nil;
      fControl := Nil;
    end;

    if Assigned(Value) then begin
{
      if Value is TmmLabel then
        if ((Value as TmmLabel).AutoLabel.Control = mOwner) then begin
          raise EComponentError.CreateFmt('Circular error: %s is owned from %s', [mOwner.Name, Value.Name]);
        end;
{}
      fControl := Value;
      fControl.Enabled := mOwner.Enabled;
      fControl.Visible := mOwner.Visible;
      fControl.FocusControl := TWinControl(mOwner);
      SetLabelPos;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TAutoLabel.SetDistance(const Value: Integer);
begin
  if Value <> fDistance then begin
    FDistance := Value;
    SetLabelPos;
  end;
end;
//------------------------------------------------------------------------------
procedure TAutoLabel.SetEnabled(const Value: Boolean);
begin
  if Assigned(fControl) then
    fControl.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TAutoLabel.SetLabelPosition(const Value: TLabelPosition);
begin
  if Value <> fLabelPosition then begin
    fLabelPosition := Value;
    SetLabelPos;
  end;
end;
//------------------------------------------------------------------------------
procedure TAutoLabel.SetVisible(const Value: Boolean);
begin
  if Assigned(fControl) then
    fControl.Visible := Value;
end;
//------------------------------------------------------------------------------
end.

