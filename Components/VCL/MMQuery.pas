(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmQuery.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 20.11.1998  0.00  Mg  | Assign methode eingefuegt und Barco code in kommentar
| 27.11.1998  0.01  Mg  | InsertSQL methode eingefuegt. ***ACHTUNG** muss noch vervollstaendigt werden.
| 10.02.1999  1.02  Mg  | InsertSQL methode abgeschlossen.
| 24.06.1999  1.03  Mg  | Anpassung fuer SQL-Server 7 : Tablockx in update statement
| 29.09.1999  1.04  Mg  | Fehlerbehandlungen erweitert
|=========================================================================================*)

unit mmQuery;

interface

uses
  SysUtils, Classes, DB, DBTables,
  Dialogs, mmDatabase, Windows;

type
  TmmQuery = class(TQuery)
  private
 {   FFormatFields: Boolean;
    procedure SetFormatFields(Value: Boolean);
    procedure ReplaceSchema;
    procedure ParseSchemaName(SchemaString: string);
 } protected
//    procedure Loaded; override;
//    procedure DoBeforeOpen; override;
//    procedure PrepareCursor; override;
//    procedure OpenCursor; override;
//    procedure InitFieldDefs; override;
    { Protected declarations }
  public
    procedure Assign(aSource: TPersistent); override;
    function InsertSQL ( aTable, aIDField : String; aMaxIDVal, aMinIDVal : integer ) : integer;
    // use this methode to insert a record into a table with an unique id
    // the method will return the id
    // if an error occures it will raise an exception
  {  constructor Create(AOwner: TComponent); override;
    procedure ReplaceSchema2;
 } published
  //  property FormatFields: Boolean read FFormatFields write SetFormatFields default False;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure TMMQuery.Assign(aSource: TPersistent);
begin
  if aSource is TQuery then begin
    DatabaseName := ( aSource as TQuery ).DatabaseName;
    SessionName := ( aSource as TQuery ).SessionName;
//    Name := ( aSource as TQuery ).SessionName;
  end else
    inherited Assign(aSource);
end;

function TMMQuery.InsertSQL ( aTable, aIDField : String; aMaxIDVal, aMinIDVal : integer ) : integer;
var  xID      : integer;
     xQuery   : TMMQuery;
     //xFound   : boolean;

   function getUniqueID : integer;
     function SearchID ( aCheck, aMax, aMin : integer ):integer;
     var   xCount1, xCount2, xNewBorder : integer;
     begin
       // Check if aCheck id is already in use
       xQuery.SQL.Text := Format ( 'select %s from %s where %s = %d', [aIDField,aTable,aIDField,aCheck] );
       xQuery.Open;
       if xQuery.EOF then begin // a whole is found
         Result := aCheck;
         xQuery.Close;
       end else begin
         // maybe the table is full
         if aMax = aMin then raise EDBEngineError.CreateFmt ( 'No space available in table ' + aTable , [] );
         xNewBorder := Trunc ( ( aMax + aMin ) / 2.0 );
         // Count the top half of records
         xQuery.close;
         xQuery.SQL.Text := Format ( 'select count(*) Num from %s where %s <= %d and %s >= %d',
                                     [aTable,aIDField,aMax,aIDField,xNewBorder]);
         xQuery.Open;
         xCount1 := xQuery.FieldByName ( 'Num' ).AsInteger;
         xQuery.Close;
         // Count the bottom half of records
         xQuery.SQL.Text := Format ( 'select count(*) Num from %s where %s <= %d and %s >= %d',
                                     [aTable,aIDField,xNewBorder,aIDField, aMin] );
         xQuery.Open;
         xCount2 := xQuery.FieldByName ( 'Num' ).AsInteger;
         xQuery.Close;
         if xCount1 = xCount2 then  // the border will be moved up because the trunc function
           Result := SearchID ( xNewBorder, aMax, xNewBorder )
         else
           if xCount1 < xCount2 then
             Result := SearchID ( xNewBorder, aMax, xNewBorder )
           else
             Result := SearchID ( xNewBorder, xNewBorder, aMin );
           end;
     end;

   begin
     Result := 0;
     // get the max value of the id field
     xQuery.close;
     xQuery.SQL.Text := Format ( 'select max ( %s ) %s from %s where %s between (%d) and (%d)',
      [aIDField, aIDField, aTable, aIDField, aMinIDVal, aMaxIDVal] );
     try
       xQuery.open;
       Result := xQuery.FieldByName ( aIDField ).AsInteger;
       xQuery.close;
     except
       raise;
     end;
     if Result = 0 then begin // no record in table
       Result := aMinIDVal;
       Exit;
     end;
     if Result < aMaxIDVal then begin // the max value is not reached
       inc ( Result );
       Exit;
     end;
     if Result > aMaxIDVal then begin
       Result := 0;
       raise EDBEngineError.CreateFmt ( 'Overflow of ID : ' + aIDField , [] );
     end;
     // search an available value in the field id of the spezified table
     Result := SearchID ( aMinIDVal, aMaxIDVal, aMinIDVal );
   end;

begin
  Result := 0;
  // cretate a query and assign it with the self object
  xQuery := TMMQuery.Create ( Nil );
  try
    xQuery.Assign ( self );
  except
    xQuery.Free;
    raise EDBEngineError.CreateFmt ( 'Assign of MMQuery failed', [] );
  end;
  try
    // start a transaction for the following steps
    xQuery.SQL.Text := 'begin transaction';
    xQuery.ExecSQL;
  except
    Result := 0;
    xQuery.Free;
    raise;
  end;
  try
    // lock the table with an update
    xQuery.Close;
    xQuery.SQL.Text := Format ( 'update %s with ( tablockx holdlock ) set %s = 1 where %s = 1', [ aTable, aIDField, aIDField ] );
    xQuery.ExecSQL;
    // get a unique ID from the spezified table
    xID := getUniqueID;
    // completet the query parameter with the ID and insert the data
    Params.ParamByName ( aIDField ).AsInteger := xID;
    ExecSQL;
    Result := xID;
    // commit the transaction
    xQuery.Close;
    xQuery.SQL.Text := 'commit transaction';
    xQuery.ExecSQL;
    xQuery.Free;
  except
    on e:Exception do begin
      Result := 0;
      try
        xQuery.Close;
        xQuery.SQL.Text := 'rollback transaction';
        xQuery.ExecSQL;
        xQuery.Free;
      except end;
      raise Exception.Create ( e.Message );
    end;
  end;
end;
//procedure Register;
//begin
//  RegisterComponents('Data Access', [TmmQuery]);
//end;
{
constructor TmmQuery.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFormatFields := False;
end;

procedure TmmQuery.Loaded;
begin
  inherited;
  ReplaceSchema;
end;


procedure TmmQuery.SetFormatFields(Value: Boolean);
var
  I,NewPos,LastPos: integer;
  FieldName,DLabel: string;
begin
  if FFormatFields <> Value then
  begin
    if (Value) then
    begin
      for I := 0 to FieldCount-1 do
      begin
        FieldName := Fields[I].FieldName;
        if (FieldName = 'C_CREATOR') or
           (FieldName = 'C_CREATE_DATE') or
           (FieldName = 'C_MODIFIER') or
           (FieldName = 'C_MODIFY_DATE') or
           (FieldName = 'C_DELETER') or
           (FieldName = 'C_DELETE_DATE') then
         begin
           Fields[I].ReadOnly := True;
           Fields[I].Visible  := False;
         end
        else if (FieldName = 'C_DELETE') then
         begin
           Fields[I].ReadOnly := False;
           Fields[I].Visible  := False;
         end;
        DLabel := Copy(FieldName,3,99);
        if (Pos('_ID',DLabel) <> 0) then
          DLabel := Copy(DLabel,1,Pos('_ID',DLabel)-1);
        LastPos := 1;
        While (Pos('_',DLabel) <> 0) do
        begin
          NewPos := Pos('_',DLabel);
          DLabel := Copy(DLabel,1,LastPos)
                    + LowerCase(Copy(DLabel,LastPos+1,NewPos-LastPos-1))
                    + ' '
                    + Copy(DLabel,NewPos+1,99);
          LastPos := NewPos;
        end;
        if (LastPos = 1) then LastPos := 0;
        DLabel := Copy(DLabel,1,LastPos+1)
                  + LowerCase(Copy(DLabel,LastPos+2,99));
        Fields[I].DisplayLabel := DLabel;
      end;
    end;
    FFormatFields := Value;
  end;
end;

procedure TmmQuery.ReplaceSchema;
begin
  ParseSchemaName('<SCHEMA.>');
end;

procedure TmmQuery.ReplaceSchema2;
begin
  ParseSchemaName('<SCHEMA2.>');
end;

procedure TmmQuery.ParseSchemaName(SchemaString: string);
var
  LocDatabase: TDatabase;
  Str,LocSchema: String;
  I: Integer;
begin
  if (csDesigning in ComponentState) then Exit;
  LocDatabase := nil;
  Str := SQL.Text;
  I   := Pos(SchemaString,Str);
  while (I <> 0) do
  begin
    SQL.Clear;
    LocSchema := '';
    if (LocDatabase =  nil) then LocDatabase := Session.FindDatabase(DatabaseName);
    if (LocDatabase <> nil) then
      if (TmmDatabase(LocDatabase).SchemaName <> '') then
        LocSchema := TmmDatabase(LocDatabase).SchemaName + '.';
    Str := Copy(Str,1,I-1) + LocSchema + Copy(Str,I+Length(SchemaString),Length(Str));
    SQL.Add(Str);
    Str := SQL.Text;
    I   := Pos(SchemaString,Str);
  end;
end;
}
end.
