(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDBEdit.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
  Funktion wurde so Erweitert, dass der Zustand Edit/Read Only visualisiert
  wird.

  ShowMode = smNormal
    Komponente verhaelt sich gleich wie TDBEdit

  ShowMode = smExtended
  Komponente ist im erweiterten Modus. In diesem Mode ist das inherited Enabled
  immer auf True, da die Darstellung bei False "geisterhaft" erscheint.

  Enabled = False
    Die Farbe wird auf clMMReadOnlyColor gesetzt und darunter ReadOnly = True
  Enabled = True
      Die Farbe wird auf die Originalfarbe gesetzt und darunter ReadOnly = False
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.01  Wss | AutoLabel implemented
| 10.04.2001  1.02  Wss | Show component state Edit/ReadOnly implemented
| 19.12.2001  1.03  Wss | Extended ShowMode changed to use ReadOnly state
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
|=========================================================================================*)
unit mmDBEdit;

interface

uses
  AutoLabelClass, Classes, DB, DBCtrls, Graphics,
  Windows, stdCtrls, SysUtils, dialogs, LoepfeGlobal;

type
  {:----------------------------------------------------------------------------
   }
  TmmDBEdit = class (TDBEdit)
  private
    fAutoLabel: TAutoLabel;
    fColor: TColor;
    fReadOnlyColor: TColor;
    fShowMode: TShowMode;
    function GetColor: TColor;
    function GetVisible: Boolean;
    procedure SetColor(Value: TColor);
    procedure SetReadOnlyColor(Value: TColor);
    procedure SetShowMode(Value: TShowMode);
    procedure SetVisible(Value: Boolean); virtual;
    procedure UpdateProperties;
    function GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
  protected
//    function GetEnabled: Boolean; reintroduce; virtual;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Color: TColor read GetColor write SetColor;
//    property Enabled: Boolean read GetEnabled write SetEnabled;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property ReadOnlyColor: TColor read fReadOnlyColor write SetReadOnlyColor;
    property ShowMode: TShowMode read fShowMode write SetShowMode;
    property Visible: Boolean read GetVisible write SetVisible;
  end;


//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure Register;
begin
end;

{:------------------------------------------------------------------------------
 TmmDBEdit}
{:-----------------------------------------------------------------------------}
constructor TmmDBEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fAutoLabel     := TAutoLabel.Create(Self);
  fColor         := inherited Color;
  fReadOnlyColor := clMMReadOnlyColor;
  fShowMode      := smNormal;
end;

{:-----------------------------------------------------------------------------}
destructor TmmDBEdit.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;

{:-----------------------------------------------------------------------------}
function TmmDBEdit.GetColor: TColor;
begin
  Result := fColor;
end;

{:-----------------------------------------------------------------------------}
{
function TmmDBEdit.GetEnabled: Boolean;
begin
  Result := inherited Enabled;
end;
{}

{:-----------------------------------------------------------------------------}
function TmmDBEdit.GetReadOnly: Boolean;
begin
  Result := inherited ReadOnly;
end;

{:-----------------------------------------------------------------------------}
function TmmDBEdit.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.Loaded;
begin
  inherited Loaded;
  UpdateProperties;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.SetColor(Value: TColor);
begin
  if fColor <> Value then begin
    fColor := Value;
    UpdateProperties;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
//  inherited Enabled := Value;
  UpdateProperties;
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.SetReadOnly(const Value: Boolean);
begin
  inherited ReadOnly := Value;
  UpdateProperties;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.SetReadOnlyColor(Value: TColor);
begin
  if fReadOnlyColor <> Value then begin
    fReadOnlyColor := Value;
    UpdateProperties;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.SetShowMode(Value: TShowMode);
begin
  if fShowMode <> Value then begin
    fShowMode := Value;
    UpdateProperties;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBEdit.UpdateProperties;
begin
  if (ComponentState - [csFreeNotification]) = [] then begin
    if (fShowMode = smExtended) and ReadOnly then
      inherited Color := fReadOnlyColor
    else
      inherited Color := fColor;
  end;
end;


end.

