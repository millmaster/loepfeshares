(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDBMonthCalendar.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmDBMonthCalendar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, mmMonthCalendar, DBCtrls, DB, CommCtrl;

type
  TmmDBMonthCalendar = class(TmmMonthCalendar)
  private
    FDataLink: TFieldDataLink;
    FRestoreNullDate: Boolean;
    function GetDataField: string;
    function GetDataSource: TDataSource;
    procedure SetDataField(const Value: string);
    procedure SetDataSource(Value: TDataSource);
    procedure DataChange(Sender: TObject);
    procedure UpdateData(Sender: TObject);
    procedure CNNotify(var Message: TWMNotify); message CN_NOTIFY;
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{procedure Register;
begin
  RegisterComponents('Data Controls', [TmmDBMonthCalendar]);
end;}

constructor TmmDBMonthCalendar.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDataLink := TFieldDataLink.Create;
  FDataLink.Control := Self;
  FDataLink.OnDataChange := DataChange;
  FDataLink.OnUpdateData := UpdateData;
end;

destructor TmmDBMonthCalendar.Destroy;
begin
  FDataLink.OnUpdateData := nil;
  FDataLink.OnDataChange := nil;
  FDataLink.Free;
  FDataLink := nil;
  inherited Destroy;
end;

function TmmDBMonthCalendar.GetDataField: string;
begin
  Result := FDataLink.FieldName;
end;

function TmmDBMonthCalendar.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

procedure TmmDBMonthCalendar.SetDataSource(Value: TDataSource);
begin
  FDataLink.DataSource := Value;
  if Value <> nil then Value.FreeNotification(Self);
end;

procedure TmmDBMonthCalendar.SetDataField(const Value: string);
begin
  FDataLink.FieldName := Value;
end;

procedure TmmDBMonthCalendar.DataChange(Sender: TObject);
begin
  if (FDataLink.Field = nil) then
    Date := 0
  else
  begin
    if (FDataLink.Field.IsNull) or (Trunc(FDataLink.Field.AsDateTime) = 0.0) then
    begin
      FRestoreNullDate := True;
      CalColors.TitleBackColor := clInactiveCaptionText;
      CalColors.TextColor      := clInactiveCAption;
      Date := SysUtils.Date;
    end
    else
    begin
      FRestoreNullDate := False;
      CalColors.TitleBackColor := clActiveCaption;
      CalColors.TextColor      := clWindowText;
      Date := FDataLink.Field.AsDateTime
    end;
  end;
end;

procedure TmmDBMonthCalendar.UpdateData(Sender: TObject);
begin
  if FRestoreNullDate then
    FDataLink.Field.Clear
  else
    FDataLink.Field.AsDateTime := Trunc(Date) + Frac(FDataLink.Field.AsDateTime);
end;

procedure TmmDBMonthCalendar.CNNotify(var Message: TWMNotify);
begin
  with Message, NMHdr^ do
  begin
    case code of
      MCN_SELECT,MCN_SELCHANGE:
        begin
          if (not FDataLink.Editing) then FDataLink.Edit;
        end;
    end;
  end;
  inherited;
  with Message, NMHdr^ do
  begin
    case code of
      MCN_SELECT,MCN_SELCHANGE:
        begin
          FRestoreNullDate := False;
          CalColors.TitleBackColor := clActiveCaption;
          CalColors.TextColor      := clWindowText;
          FDataLink.Modified;
          try
            FDataLink.UpdateRecord;
          except
            on Exception do SetFocus;
          end;
        end;
    end;
  end;
end;

end.

