(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmLabel.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 18.01.1999  1.01  Wss | Property Alignment to taLeftJustify changed
|                       | property value Autosize to True changed
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 15.05.2000  1.10  Wss | fAutolabel with FreeAndNil removed -> problems with Kr at Notification()
|=========================================================================================*)
unit mmLabel;

interface

uses
  AutoLabelClass, StdCtrls, Classes;

type
  TmmLabel = class(TLabel)
  private
    fAutoLabel: TAutoLabel;
    procedure SetVisible(Value: Boolean); virtual;
    function GetVisible: Boolean;
    function GetValue: Variant;
    procedure SetValue(const Value: Variant);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
  public
    property Value: Variant read GetValue write SetValue;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  SysUtils;
//procedure Register;
//begin
//  RegisterComponents('Standard', [TmmLabel]);
//end;

constructor TmmLabel.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAutoLabel := TAutoLabel.Create(Self);
end;
//------------------------------------------------------------------------------
destructor TmmLabel.Destroy;
begin
//  fAutoLabel.Free;
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TmmLabel.GetValue: Variant;
begin
  Result := Caption;
end;
//------------------------------------------------------------------------------
function TmmLabel.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmLabel.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmLabel.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmLabel.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TmmLabel.SetValue(const Value: Variant);
begin
  try
    Caption := Value;
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TmmLabel.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

end.
