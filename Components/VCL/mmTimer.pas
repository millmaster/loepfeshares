(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmTimer.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 04.12.2000  0.00  Wss | procedure Restart added
|=========================================================================================*)
unit mmTimer;

interface

uses
  Classes, ExtCtrls;

type
  TmmTimer = class(TTimer)
  private
  protected
  public
    procedure Restart;
  published
  end;


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------
procedure TmmTimer.Restart;
begin
  Enabled := False;
  Enabled := True;
end;
//------------------------------------------------------------------------------
end.
