(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmTreeView.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
| 09.02.2006        Wss | Wenn Caption editiert wird, dann wird bei Esc der Abbruch nie mitgeteilt
                          -> Eventproperty OnEndEdit hinzugefügt
|=========================================================================================*)
unit mmTreeView;

interface

uses
  AutoLabelClass, Classes, ComCtrls, Controls, Commctrl, Messages;

type
  TmmTreeView = class(TTreeView)
  private
    fAutoLabel: TAutoLabel;
    fOnEndEdit: TTVExpandedEvent;
    procedure CNNotify(var Message: TWMNotify); message CN_NOTIFY;
    procedure SetVisible(Value: Boolean); virtual;
    function GetVisible: Boolean;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property OnEndEdit: TTVExpandedEvent read fOnEndEdit write fOnEndEdit;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, sysutils;

constructor TmmTreeView.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAutoLabel := TAutoLabel.Create(Self);
  fOnEndEdit := nil;
end;
//------------------------------------------------------------------------------
destructor TmmTreeView.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;

//------------------------------------------------------------------------------
procedure TmmTreeView.CNNotify(var Message: TWMNotify);
var
  xItem: TTVItem;
begin
   inherited;

   if Message.NMHdr.code = TVN_ENDLABELEDIT then begin
     if Assigned(fOnEndEdit) then begin
       xItem := PTVDispInfo(Message.NMHdr)^.item;
       fOnEndEdit(Self, TTreeNode(xItem.lParam));
     end;
   end;
end;

//------------------------------------------------------------------------------
function TmmTreeView.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmTreeView.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
    	  fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmTreeView.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmTreeView.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TmmTreeView.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

end.
