(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmScrollBox.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 28.08.2006  0.00  Wss | Events f�r scrollen hinzugef�gt
|=========================================================================================*)
unit mmScrollBox;

interface

uses
  Classes, Forms, Messages;

type
  TmmScrollBox = class(TScrollBox)
  private
    FOnHorzScrollEvent: TNotifyEvent;
    FOnVertScrollEvent: TNotifyEvent;
    procedure WMVScroll(var Msg: TMessage); message WM_VSCROLL;
    procedure WMHScroll(var Msg: TMessage); message WM_HSCROLL;
  protected
  public
  published
    property OnHorzScrollEvent: TNotifyEvent read FOnHorzScrollEvent write FOnHorzScrollEvent;
    property OnVertScrollEvent: TNotifyEvent read FOnVertScrollEvent write FOnVertScrollEvent;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
//:-----------------------------------------------------------------------------
procedure TmmScrollBox.WMHScroll(var Msg: TMessage);
begin
  inherited;
  if Assigned(FOnHorzScrollEvent) then
    FOnHorzScrollEvent(Self);
end;
//:-----------------------------------------------------------------------------
procedure TmmScrollBox.WMVScroll(var Msg: TMessage);
begin
  inherited;
  if Assigned(FOnVertScrollEvent) then
    FOnVertScrollEvent(Self);
end;
//:-----------------------------------------------------------------------------

end.
