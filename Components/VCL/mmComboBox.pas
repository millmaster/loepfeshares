(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmComboBox.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 10.09.2001  1.01  Wss | Properties Edit, Alignment added
                          - Alignment: specify the orientation of containing text in dropdown list
                          - Edit: False = no key editing will be allowed
| 17.01.2002  1.02  Wss | Extended ShowMode behavior added and removed from inherited components
| 25.10.2002  1.02  Wss | Funktion IndexOfId hinzugefügt
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
|=========================================================================================*)
unit mmComboBox;

interface

uses
  AutoLabelClass, Classes, Controls, Graphics, StdCtrls, Windows,
  LoepfeGlobal;

type
  TmmComboBox = class(TComboBox)
  private
    fAlignment: TAlignment;
    fAutoLabel: TAutoLabel;
    fColor: TColor;
    fEdit: Boolean;
    fReadOnly: Boolean;
    fReadOnlyColor: TColor;
    fShowMode: TShowMode;
    function GetColor: TColor;
    function GetVisible: Boolean;
    procedure SetAlignment(const Value: TAlignment);
    procedure SetColor(Value: TColor);
    procedure SetReadOnlyColor(const Value: TColor);
    procedure SetShowMode(const Value: TShowMode);
    procedure SetVisible(Value: Boolean); virtual;
    procedure UpdateProperties;
  protected
    procedure DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState); override;
    function GetReadOnly: Boolean; virtual;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
    procedure SetReadOnly(const Value: Boolean); virtual;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function IndexOfId(aId: Integer): Integer;
    procedure Loaded; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property Alignment: TAlignment read fAlignment write SetAlignment default taLeftJustify;
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Color: TColor read GetColor write SetColor;
    property Edit: Boolean read fEdit write fEdit;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property ReadOnlyColor: TColor read fReadOnlyColor write SetReadOnlyColor;
    property ShowMode: TShowMode read fShowMode write SetShowMode;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  SysUtils;

//------------------------------------------------------------------------------
constructor TmmComboBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAutoLabel     := TAutoLabel.Create(Self);
  fAlignment     := taLeftJustify;
  fColor         := inherited Color;
  fEdit          := True;
  fReadOnly      := False;
  fReadOnlyColor := clMMReadOnlyColor;
  fShowMode      := smNormal;
end;
//------------------------------------------------------------------------------
destructor TmmComboBox.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState);
const
  Alignments: array[TAlignment] of Word = (dt_Left, dt_Right, dt_Center);
var
  xOption: Integer;
begin
  if (fAlignment = taLeftJustify) or Assigned(OnDrawItem) then
    inherited DrawItem(Index, Rect, State)
  else begin
    Canvas.Font  := Font;
    Canvas.Brush := Brush;

    if (Index >= 0) and (odSelected in State) then
    begin
      Canvas.Brush.Color := clHighlight;
      Canvas.Font.Color  := clHighlightText
    end;

    Canvas.FillRect(Rect);
    if Index >= 0 then begin
      xOption := Alignments[fAlignment] or DT_SINGLELINE or DT_VCENTER;
      Rect.Right := Rect.Right - 2;
      DrawText(Canvas.Handle, PChar(Items[Index]), -1, Rect, xOption);
    end;

    if odFocused in State then
      DrawFocusRect(Canvas.Handle, Rect);
  end;
end;
//------------------------------------------------------------------------------
function TmmComboBox.GetColor: TColor;
begin
  Result := fColor;
end;
//------------------------------------------------------------------------------
function TmmComboBox.GetReadOnly: Boolean;
begin
  Result := fReadOnly;
end;
//------------------------------------------------------------------------------
function TmmComboBox.IndexOfId(aId: Integer): Integer;
begin
  for Result:=0 to Items.Count-1 do
    if Integer(Items.Objects[Result]) = aId then Exit;
  Result := -1;
end;
//------------------------------------------------------------------------------
function TmmComboBox.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if (not fEdit and not((Key = VK_DOWN) or (Key = VK_UP) or (Key = VK_NEXT) or (Key = VK_PRIOR))) OR
      fReadOnly then
    Abort;

  inherited KeyDown(Key, Shift);
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.KeyPress(var Key: Char);
begin
  if not fEdit or fReadOnly then
    Key := #0;
  inherited KeyPress(Key);
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.Loaded;
begin
  inherited Loaded;
  UpdateProperties;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ReadOnly then
    Abort
  else
    inherited;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.SetAlignment(const Value: TAlignment);
begin
  if fAlignment <> Value then begin
    fAlignment := Value;
    if fAlignment <> taLeftJustify then
      Style := csOwnerDrawFixed;
    Invalidate;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.SetColor(Value: TColor);
begin
  if fColor <> Value then begin
    fColor := Value;
    UpdateProperties;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;

  UpdateProperties;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.SetReadOnly(const Value: Boolean);
begin
  fReadOnly := Value;
  UpdateProperties;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.SetReadOnlyColor(const Value: TColor);
begin
  fReadOnlyColor := Value;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.SetShowMode(const Value: TShowMode);
begin
  if fShowMode <> Value then begin
    fShowMode := Value;
    UpdateProperties;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;
//------------------------------------------------------------------------------
procedure TmmComboBox.UpdateProperties;
begin
  if (ComponentState - [csFreeNotification]) = [] then begin
    if (fShowMode = smExtended) and ReadOnly then
      inherited Color := fReadOnlyColor
    else
      inherited Color := fColor;
  end;
end;
//------------------------------------------------------------------------------
end.

