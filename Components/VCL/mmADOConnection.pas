(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmADOConnection.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.04.2002  1.00  Wss | Initial Release
| 18.07.2002  1.01  LOK | BuildConnectionString und AutoConfig eingef�gt
|=========================================================================================*)
unit mmADOConnection;

interface

uses
  ADODb, LoepfeGlobal, classes, sysutils;

type
  TAutoConfig = (acNone, acManual, acAuto);

  TmmADOConnection = class(TADOConnection)
  private
    FAutoConfig: TAutoConfig;
    FDBName: string;
    FUserName: string;
    FSQLServerName: string;
    FPassword: string;
    procedure SetDBName(const Value: string);
    procedure SetPassword(const Value: string);
    procedure SetSQLServerName(const Value: string);
    procedure SetUserName(const Value: string);
    procedure SetAutoConfig(const Value: TAutoConfig);
  protected
    procedure BuildConnectionString;
  public
    constructor Create(AOwner: TComponent); override;
//    function GetDefaultConnectionString:string;
  published
    property AutoConfig:TAutoConfig read FAutoConfig write SetAutoConfig default acNone;
    property DBName:string read FDBName write SetDBName;
    property SQLServerName:string read FSQLServerName write SetSQLServerName;
    property UserName:string read FUserName write SetUserName;
    property Password:string read FPassword write SetPassword;
  end;


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{ TmmADOConnection }

//const
//  cConnectionString = 'Provider=SQLOLEDB.1;Data Source=%s;Initial Catalog=%s;Password=%s;User ID=%s';

procedure TmmADOConnection.BuildConnectionString;
begin
  case FAutoConfig of
    acAuto: begin
      FSQLServerName := '';
      FDBName        := '';
      FPassword      := '';
      FUserName      := '';
      ConnectionString := GetDefaultConnectionString;
    end;// acAuto: begin
    acManual:
      ConnectionString := Format(cDefaultConnectionString,[FSQLServerName,FDBName,FPassword,FUserName]);
  end;// case FAutoConfig of
end;// procedure TmmADOConnection.BuildConnectionString;

constructor TmmADOConnection.Create(AOwner: TComponent);
begin
  inherited;
  FAutoConfig    := acNone;
  LoginPrompt    := false;
  FDBName        := gDBName;
  FSQLServerName := gSQLServerName;
  FUserName      := gDBUsername;
  FPassword      := gDBPassword;
end;// constructor TmmADOConnection.Create(AOwner: TComponent);

(* Gibt den automatischen Connectioon String zur�ck.
   Damit kann der Standard ConnectionString erg�nzt werden.
   Bsp.: xCon := mmAdoConnection.GetDefaultConnectionString;
         xCon := xCon + ';Auto Translate=True;Packet Size=4096'
         mmAdoConnection.ConnectionString := xCon;
  function TmmADOConnection.GetDefaultConnectionString: string;
  begin
    result := Format(cConnectionString,[gSQLServerName,gDBName,gDBPassword,gDBUserName]);
  end;// function TmmADOConnection.GetDefaultConnectionString: string;*)
procedure TmmADOConnection.SetAutoConfig(const Value: TAutoConfig);
begin
  if FAutoConfig <> Value then begin
    FAutoConfig := Value;
    case FAutoConfig of
      acAuto: begin
        FDBName        := '';
        FUserName      := '';
        FSQLServerName := '';
        FPassword      := '';
        BuildConnectionString;
      end;// acAuto: begin
      acNone: begin
        FDBName        := '';
        FUserName      := '';
        FSQLServerName := '';
        FPassword      := '';
        ConnectionString := '';
      end;// acNone: begin
      acManual: begin
        FDBName        := gDBName;
        FUserName      := gDBUserName;
        FSQLServerName := gSQLServerName;
        FPassword      := gDBPassword;
        BuildConnectionString;
      end;// acNone: begin
    end;// case FAutoConfig of
  end;// if FAutoConfig <> Value then begin
end;// procedure TmmADOConnection.SetAutoConfig(const Value: boolean);

procedure TmmADOConnection.SetDBName(const Value: string);
begin
  FDBName := Value;
  if not(csLoading in ComponentState) then
    SetAutoConfig(acManual);
  BuildConnectionString;
end;// procedure TmmADOConnection.SetDBName(const Value: string);

procedure TmmADOConnection.SetPassword(const Value: string);
begin
  FPassword := Value;
  if not(csLoading in ComponentState) then
    SetAutoConfig(acManual);
  BuildConnectionString;
end;// procedure TmmADOConnection.SetPassword(const Value: string);

procedure TmmADOConnection.SetSQLServerName(const Value: string);
begin
  FSQLServerName := Value;
  if not(csLoading in ComponentState) then
    SetAutoConfig(acManual);
  BuildConnectionString;
end;// procedure TmmADOConnection.SetSQLServerName(const Value: string);

procedure TmmADOConnection.SetUserName(const Value: string);
begin
  FUserName := Value;
  if not(csLoading in ComponentState) then
    SetAutoConfig(acManual);
  BuildConnectionString;
end;// procedure TmmADOConnection.SetUserName(const Value: string);

end.

