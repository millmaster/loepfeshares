(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmStringGrid.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: Attention:  TAdvStringGrid Vers. 1.90 has an error.
                               - 'DB init error' with the comp. TMMSetupModul and
                               - TAdvStringGrid.ColCount / RowCount <> Default-Value

| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.08.1998  1.00  Wss | Before compare 2 string in StringCompare() test if they aren't empty
| 24.09.1998  1.01  Wss | call of OnDrawCell property in DrawCell()
| 02.12.1998  1.02  Wss | DeleteRow, DeleteColumn changed to override
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 31.08.1999  1.03  SDo | Change TmmStringGrid.Parentclass form TStingGrid to TAdvStringGrid
| 05.10.1999  1.03  Wss | Cleanup unneeded code from previous TSortGrid
| 21.09.2000  1.04  SDo | Neue Proc. SelectCol (selektiert eine ganze Spalte)
| 22.09.2000  1.04  SDo | Propery ColSelect
| 04.01.2001  1.04  SDo | Error observerd in TAdvStringGrid Vers. 1.90 (take a look to Info)
| 12.01.2001  1.04  Wss | TmmExtStringGrid created for advanced features
| 15.01.2002  1.05  Nue | DateesTimes methodes and codes added
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
|=========================================================================================*)
unit mmExtStringGrid;

interface

uses
  Classes, AdvGrid, AutoLabelClass, Grids, Messages, Windows,
  sysutils;

type
  TmmExtStringGrid = class(TAdvStringGrid)
  private
    fAutoLabel: TAutoLabel;
    fColumnSelect : Boolean;

    function GetColSelect: Boolean;
    function GetOptions: TGridOptions;
    procedure SetColumnSelect(const Value: Boolean);
    procedure SetOptions(const Value: TGridOptions);
    function GetDatesTimes(ACol, ARow: Integer): tdatetime;
    procedure SetDatesTimes(ACol,ARow:Integer;const Value:tDatetime);
  protected
    function GetVisible: Boolean;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
    procedure SetVisible(Value: Boolean); virtual;
    procedure WMKeyDown(var Msg:TWMKeyDown); message wm_keyDown;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure SelectCol(aColIndex: LongInt);
    procedure Click; override;
    procedure ColumnMoved(FromIndex, ToIndex: longint); override;
    property DatesTimes[ACol,ARow:Integer]:tdatetime read GetDatesTimes write SetDatesTimes;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Visible: Boolean read GetVisible write SetVisible;
    property ColumnSelect: Boolean read GetColSelect write SetColumnSelect Default FALSE;
    property Options: TGridOptions read GetOptions write SetOptions;

{
  private
    fAutoLabel: TAutoLabel;

    function GetVisible: Boolean;
    procedure SetAlignmentHorz(Value: TAlignment);
    procedure SetAlignmentVert(Value: TVertAlignment);
    procedure SetBevelStyle(Value: TCellBevelStyle);
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
    procedure SetProportionalScrollBars(Value: Boolean);
    procedure SetVisible(Value: Boolean); virtual;
  protected
    procedure ListQuickSort(const ACol: Longint; const SortOptions: TSortOptions); virtual;
    function DetermineSortStyle(const ACol: Longint): TSortStyle; virtual;
    procedure InitializeFormatOptions(var FmtOpts: TFormatOptions);
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure ColWidthsChanged; override;
    procedure RowHeightsChanged; override;
    procedure SizeChanged(OldColCount, OldRowCount: Longint); override;
    procedure UpdateScrollPage; virtual;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure SetEditText(ACol, ARow: Longint; const Value: string); override;
    procedure Click; override;
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure InitValidate; virtual;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
  public
    property Sorting: Boolean read fSorting default False;
    property Modified: Boolean read fModified write fModified default False;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure MoveTo(ACol, ARow: Longint); virtual;
    procedure Clear; virtual;
    procedure InsertRow(ARow: Longint); virtual;
    procedure InsertColumn(ACol: Longint); virtual;
    procedure DeleteRow(ARow: Longint); override;
    procedure DeleteColumn(ACol: Longint); override;
    procedure MoveRow(FromIndex, ToIndex: Longint); virtual;
    procedure MoveColumn(FromIndex, ToIndex: Longint); virtual;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure SwapRows(ARow1, ARow2: Longint); virtual;
    procedure SwapColumns(ACol1, ACol2: Longint); virtual;

    procedure AutoSizeCol(const ACol: LongInt); virtual;
    procedure AutoSizeColumns(const DoFixedCols: Boolean; const Padding: Integer); virtual;
    procedure SortByColumn(const ACol: LongInt; SortOptions: TSortOptions); virtual;
    function IsCell(const Value: String; var ACol, ARow: Longint): Boolean; virtual;
    procedure LoadFromFile(const FileName: String; const Delimiter: Char); virtual;
    procedure SaveToFile(const FileName: String; const Delimiter: Char); virtual;
    function CanUndoSort: Boolean; virtual;
    procedure UndoSort; virtual;
    function GetCellDrawState(const ACol, ARow: Longint): TGridDrawState;
    function SelectCell(ACol, ARow: Longint): Boolean; override;
    procedure ValidateCell; virtual;
  published
    property AlignmentHorz: TAlignment read fAlignmentHorz write SetAlignmentHorz default taLeftJustify;
    property AlignmentVert: TVertAlignment read fAlignmentVert write SetAlignmentVert default taTopJustify;
    property BevelStyle: TCellBevelStyle read fBevelStyle write SetBevelStyle default cbNone;
    property ClickSorting: Boolean read fClickSorting write fClickSorting default False;
    property ExtendedKeys: Boolean read fExtendedKeys write fExtendedKeys default False;
    property ProportionalScrollBars: Boolean read fProportionalScrollBars write SetProportionalScrollBars default True;
    property Visible: Boolean read GetVisible write SetVisible;

    property OnGetCellFormat: TFormatDrawCellEvent read fOnGetCellFormat write fOnGetCellFormat;
    property OnClickSort: TClickSortEvent read fOnClickSort write fOnClickSort;
    property OnRowInsert: TUpdateGridEvent read fOnRowInsert write fOnRowInsert;
    property OnRowDelete: TUpdateGridEvent read fOnRowDelete write fOnRowDelete;
    property OnColumnInsert: TUpdateGridEvent read fOnColumnInsert write fOnColumnInsert;
    property OnColumnDelete: TUpdateGridEvent read fOnColumnDelete write fOnColumnDelete;
    property OnColumnWidthsChanged: TNotifyEvent read fOnColumnWidthsChanged write fOnColumnWidthsChanged;
    property OnRowHeightsChanged: TNotifyEvent read fOnRowHeightsChanged write fOnRowHeightsChanged;
    property OnSizeChanged: TSizeChangedEvent read fOnSizeChanged write fOnSizeChanged;
    property OnBeginSort: TBeginSortEvent read fOnBeginSort write fOnBeginSort;
    property OnEndSort: TEndSortEvent read fOnEndSort write fOnEndSort;
    property OnCellValidate: TCellValidateEvent read fOnCellValidate write fOnCellValidate;
{}
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{******************************************************************************}
{** Public Members for TmmExtStringGrid                                             **}
{******************************************************************************}
procedure TmmExtStringGrid.Click;
begin
  inherited;
  if fColumnSelect then
    SelectCol(GetRealCol);
end;
//------------------------------------------------------------------------------
procedure TmmExtStringGrid.ColumnMoved(FromIndex, ToIndex: longint);
begin
  inherited;

  if fColumnSelect then
     SelectCol(ToIndex);
end;

//------------------------------------------------------------------------------
constructor TmmExtStringGrid.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fAutoLabel := TAutoLabel.Create(Self);
end;
//------------------------------------------------------------------------------
destructor TmmExtStringGrid.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TmmExtStringGrid.GetColSelect: Boolean;
begin
 Result := fColumnSelect;
end;
//------------------------------------------------------------------------------
function TmmExtStringGrid.GetDatesTimes(ACol, ARow: Integer): tdatetime;
begin
  GetDatesTimes:=StrToDateTime(cells[acol,arow]);
end;
//------------------------------------------------------------------------------

procedure TmmExtStringGrid.SetDatesTimes(ACol,ARow:Integer;const Value:tDatetime);
begin
 cells[acol,arow]:=DateTimeToStr(value);
end;

function TmmExtStringGrid.GetOptions: TGridOptions;
begin
  Result := inherited Options;
end;
//------------------------------------------------------------------------------
function TmmExtStringGrid.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmExtStringGrid.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmExtStringGrid.SelectCol(aColIndex: Integer);
var
 gr: TGridRect;
begin
 with gr do begin
   if (aColIndex = ColCount) and ( aColIndex > 0) then Dec(aColIndex);
   left:=aColIndex;
   right:=aColIndex;
   top:=fixedrows;
   bottom:=rowcount-1;
   TopLeft.Y:=1;
   Top:=1;
 end;
 inherited selection:=gr;
end;
//------------------------------------------------------------------------------
procedure TmmExtStringGrid.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmExtStringGrid.SetColumnSelect(const Value: Boolean);
begin
  if fColumnSelect <> Value then begin
    fColumnSelect := Value;

    if fColumnSelect then begin
       Options := Options  - [goRowSelect];
       Options := Options  + [goDrawFocusSelected];

       SelectCol(GetRealCol);
    end;
    MouseActions.ColSelect := fColumnSelect;
  end;
end;
//------------------------------------------------------------------------------

procedure TmmExtStringGrid.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TmmExtStringGrid.SetOptions(const Value: TGridOptions);
begin
  inherited Options := Value;
  if (goRowSelect in Options) then
    fColumnSelect := FALSE;
end;
//------------------------------------------------------------------------------
procedure TmmExtStringGrid.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;
//------------------------------------------------------------------------------
procedure TmmExtStringGrid.WMKeyDown(var Msg: TWMKeyDown);
var xCol : integer;
begin
  if fColumnSelect then begin
     xCol:= GetRealCol;
     // Cursor nach rechts
     if (msg.charcode=vk_Down) and (xCol < ColCount-1 ) then
         inc(xCol);
     // Cursor nach links
     if (msg.charcode=vk_Up) and (xCol > 1 ) then
         Dec(xCol);
     // Cursor zur 1. Spalte
     if (msg.charcode=vk_HOME) then begin
         xCol:= FixedCols;
        // ScrollInView(xCol,1);
     end;
     // Cursor zu letzten Spalte
     if (msg.charcode=vk_END) then begin
         xCol:= ColCount-1;
         //ScrollInView(xCol,1);
     end;
     // Cursor Anschlag (links, rechts)
     if xCol > ColCount then xCol:= ColCount;
     if xCol < 1 then xCol:= 1;

     Col:= xCol;
     SelectCol(Col);
  end;
  inherited DoKeyDown(TWMKey( Msg) );
end;
//------------------------------------------------------------------------------

end.

