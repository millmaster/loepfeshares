(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDBDateTimePicker.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmDBDateTimePicker;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, mmDateTimePicker, DBCtrls, DB, CommCtrl;

type
  TmmDBDateTimePicker = class(TmmDateTimePicker)
  private
    FDataLink: TFieldDataLink;
    function GetDataField: string;
    function GetDataSource: TDataSource;
    procedure SetDataField(const Value: string);
    procedure SetDataSource(Value: TDataSource);
    procedure DataChange(Sender: TObject);
    procedure UpdateData(Sender: TObject);
    procedure CNNotify(var Message: TWMNotify); message CN_NOTIFY;
  protected
    procedure KeyPress(var Key: Char); override;
    procedure Change; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{procedure Register;
begin
  RegisterComponents('Data Controls', [TmmDBDateTimePicker]);
end;}

constructor TmmDBDateTimePicker.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDataLink := TFieldDataLink.Create;
  FDataLink.Control := Self;
  FDataLink.OnDataChange := DataChange;
  FDataLink.OnUpdateData := UpdateData;
end;

destructor TmmDBDateTimePicker.Destroy;
begin
  FDataLink.OnUpdateData := nil;
  FDataLink.OnDataChange := nil;
  FDataLink.Free;
  FDataLink := nil;
  inherited Destroy;
end;

function TmmDBDateTimePicker.GetDataField: string;
begin
  Result := FDataLink.FieldName;
end;

function TmmDBDateTimePicker.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

procedure TmmDBDateTimePicker.SetDataSource(Value: TDataSource);
begin
  FDataLink.DataSource := Value;
  if Value <> nil then Value.FreeNotification(Self);
end;

procedure TmmDBDateTimePicker.SetDataField(const Value: string);
begin
  FDataLink.FieldName := Value;
end;

procedure TmmDBDateTimePicker.DataChange(Sender: TObject);
begin
  if FDataLink.Field = nil then
  begin
     if Kind = dtkDate then
       Date := 0
     else
       Time := 0;
  end
  else
  begin
    if (FDataLink.Field.IsNull) or (Trunc(FDataLink.Field.AsDateTime) = 0.0) then
    begin
      Date := SysUtils.Date;
      Time := 0;
      Checked := False;
    end
    else
    begin
      if (Kind = dtkDate) then
        Date := FDataLink.Field.AsDateTime
      else
        Time := FDataLink.Field.AsDateTime;
      Checked := True;
    end;
  end;
end;

procedure TmmDBDateTimePicker.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  if (Key in [#32..#255]) and (FDataLink.Field <> nil) and
    not FDataLink.Field.IsValidChar(Key) then
  begin
    MessageBeep(0);
    Key := #0;
  end;
  case Key of
    ^H, ^V, ^X, #32..#255:
      Change;
    #27:
      begin
        FDataLink.Reset;
        Key := #0;
      end;
  end;
end;

procedure TmmDBDateTimePicker.UpdateData(Sender: TObject);
begin
  if (not Checked) then
    FDataLink.Field.Clear
  else
  begin
    if (Kind = dtkDate) then
      FDataLink.Field.AsDateTime := Trunc(Date) + Frac(FDataLink.Field.AsDateTime)
    else
      FDataLink.Field.AsDateTime := Trunc(FDataLink.Field.AsDateTime) + Frac(Time);
  end;
end;

procedure TmmDBDateTimePicker.Change;
begin
  inherited;
  if (not FDataLink.Editing) then FDataLink.Edit;
  FDataLink.Modified;
  try
    FDataLink.UpdateRecord;
  except
    on Exception do SetFocus;
  end;
end;

procedure TmmDBDateTimePicker.CNNotify(var Message: TWMNotify);
begin
  with Message, NMHdr^ do
  begin
    case code of
      DTN_USERSTRING,DTN_DROPDOWN,DTN_DATETIMECHANGE:
        begin
          if (not FDataLink.Editing) then
          begin
            FDataLink.Edit;
//            Checked := True;
          end;
        end;
    end;
  end;
  inherited;
  with Message, NMHdr^ do
  begin
    case code of
      DTN_CLOSEUP:
        begin
          Change;
        end;
    end;
  end;
end;

end.



