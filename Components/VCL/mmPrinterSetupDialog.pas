(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmPrinterSetupDialog.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 18.02.1999  1.00  Wss | Deriving changed to Innoview's Dialogs
|=========================================================================================*)
unit mmPrinterSetupDialog;

interface

uses
  Classes, IvMlDlgs;

type
  TmmPrinterSetupDialog = class(TIvPrinterSetupDialog)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.
