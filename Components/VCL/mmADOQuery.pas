(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmADOQuery.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.04.2002  1.00  Wss | Initial Release
|=========================================================================================*)
unit mmADOQuery;

interface

uses
  ADODb;

type
  TmmADOQuery = class(TADOQuery)
  private
  protected
  public
  published
  end;


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
end.

