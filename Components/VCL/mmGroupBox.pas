(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGroupBox.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 17.08.2000  0.00  Wss | Property CaptionSpace included
|=========================================================================================*)
unit mmGroupBox;

interface

uses
  classes, StdCtrls;

type
  TmmGroupBox = class(TGroupBox)
  private
    fCaptionSpace: Boolean;
    procedure SetCaptionSpace(const Value: Boolean);
    function GetCaption: String;
    procedure SetCaption(const Value: String);
  protected
  public
    constructor Create(aOwner: TComponent); override;
  published
    property CaptionSpace: Boolean read fCaptionSpace write SetCaptionSpace default False;
    property Caption: String read GetCaption write SetCaption;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  SysUtils;

//------------------------------------------------------------------------------
// TmmGroupBox
//------------------------------------------------------------------------------
constructor TmmGroupBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fCaptionSpace := False;
end;
//------------------------------------------------------------------------------
function TmmGroupBox.GetCaption: String;
begin
  Result := Trim(inherited Caption);
end;
//------------------------------------------------------------------------------
procedure TmmGroupBox.SetCaptionSpace(const Value: Boolean);
begin
  if Value <> fCaptionSpace then begin
    fCaptionSpace := Value;
    Caption       := Caption;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmGroupBox.SetCaption(const Value: String);
begin
  if fCaptionSpace then
    inherited Caption := Format(' %s ', [Trim(Value)])
  else
    inherited Caption := Value;
end;

end.
