(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmEdit.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
  Funktion wurde so Erweitert, dass der Zustand Edit/Read Only visualisiert
  wird.

  ShowMode = smNormal
    Komponente verhaelt sich gleich wie TDBEdit

  ShowMode = smExtended
  Komponente ist im erweiterten Modus. In diesem Mode ist das inherited Enabled
  immer auf True, da die Darstellung bei False "geisterhaft" erscheint.

  Enabled = False
    Die Farbe wird auf clMMReadOnlyColor gesetzt und darunter ReadOnly = True
  Enabled = True
      Die Farbe wird auf die Originalfarbe gesetzt und darunter ReadOnly = False
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 17.03.1999  1.10  Kr  | Alignment implemented
| 12.04.2000  1.10  Wss | ES_MULTILINE at CreateParams removed -> password mode was disabled
| 15.05.2000  1.10  Wss | fAutolabel with FreeAndNil removed -> problems with Kr at Notification()
| 11.12.2001  1.11  Wss | Show component state Edit/ReadOnly implemented
| 19.12.2001  1.12  Wss | Extended ShowMode changed to use ReadOnly state
| 25.11.2003        Lok | Erweitert um einen NumMode. Der nummerische Wert kann mit AsInteger oder AsFloat abgefragt werden
| 19.12.2003        Lok | Property Value sichtbarkeit in Public ge�ndert.
| 06.08.2004        Lok | Neue Funktionen:
|                       |   Anzahl Stellen hinter dem Komma (-1 = keine Beschr�nkun, 0 = Integer) (Decimals)
|                       |   Min und Max Begrenzung
|                       |   Validierung bei der Eingabe oder beim Verlassen des Feldes (ValdationMode)
|                       |   Validierungs Texte (InvalidInput und InvalidRange)
|                       |   Validierungs Events (InvalidInput und InvalidRange)
| 18.05.2005        Lok | Validierung auch bei Read/Write Zugriff erm�glichen (vmReadWrite).
| 13.09.2005        Lok | Validierung bei GetAsFloat() vor dem auslesen durchf�hren
|                       |   Auf vielf�ltigen Wunsch in Validate() bei zu vielen Dezimalstellen den Wert nicht
|                       |   abschneiden, sondern runden.
| 26.09.2005        Wss | Validierung mal korrigiert. Erhebe aber keinen Anspruch auf Fehlerfreiheit ;-)
| 30.01.2006        Wss | In AsFloat den Format() auf '%.*f' mit FDecimals ge�ndert
|=========================================================================================*)
unit mmEdit;

interface

uses
  AutoLabelClass, Classes, StdCtrls, Controls, Windows, Graphics, LoepfeGlobal, messages;

type
  // Zeitpunkt der Validierung
  TValidateMode = (vmInput,  // es wird bei jeder Eingabe validiert
                   vmExit,         // es wird errst Validiert, wenn das Element verlassen wird
                   vmReadWrite // Validierung bei Read/Write Zugriffen
                   );// TValidateMode
  TValidateModeSet= Set of TValidateMode;

const
  cDefaultValidateMode = [];

type
{
  // Art der Validierung
  TValidateFrom = (vfApp,
                   vfInput,
                   vfExit
                   );// TValidateFrom}

  TInvalidInput = procedure(Sender: TObject; aOrgText: string) of object;
  TInvalidRange = procedure(Sender: TObject; aMinValue, aMaxValue, aValue: extended) of object;

  TmmEdit = class (TEdit)
  private
    fAlignment: TAlignment;
    fAutoLabel: TAutoLabel;
    fColor: TColor;
    FDecimals: Integer;
    fInfoText: String;
    FInvalidInputMessage: string;
    FInvalidRangeMessage: string;
    FMaxValue: Extended;
    FMinValue: Extended;
    fNumMode: Boolean;
    FOnInvalidInput: TInvalidInput;
    FOnInvalidRange: TInvalidRange;
    fReadOnlyColor: TColor;
    fShowMode: TShowMode;
    FValidateMode: TValidateModeSet;
    mDecimalSeparatorPressed: Boolean;
    mFontColor: TColor;
    mIsValidating: Boolean;
    mOldValue: String;
    function GetAsFloat: Extended;
    function GetAsInteger: Integer;
    function GetColor: TColor;
    function GetReadOnly: Boolean;
    function GetText: String;
    function GetValue: Variant;
    function GetVisible: Boolean;
    procedure SetAsFloat(const Value: Extended);
    procedure SetAsInteger(const Value: Integer);
    procedure SetColor(Value: TColor);
    procedure SetDecimals(const Value: Integer);
    procedure SetReadOnly(const Value: Boolean);
    procedure SetReadOnlyColor(Value: TColor);
    procedure SetShowMode(Value: TShowMode);
    procedure SetText(aValue: String);
    procedure SetValue(const Value: Variant);
    procedure SetVisible(Value: Boolean); virtual;
    procedure UpdateProperties;
    procedure SetNumMode(const Value: Boolean);
    procedure SetInfoText(const Value: String);
  protected
    procedure Change; override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure DoInvalidInput(aOrgText: string);
    procedure DoInvalidRange(aMinValue, aMaxValue, aValue: extended);
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetAlignment(Value: TAlignment);
    procedure SetEnabled(Value: Boolean); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Validate;
    property AsFloat: Extended read GetAsFloat write SetAsFloat;
    property AsInteger: Integer read GetAsInteger write SetAsInteger;
    property Value: Variant read GetValue write SetValue;
  published
    property Alignment: TAlignment read fAlignment write SetAlignment default taLeftJustify;
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Color: TColor read GetColor write SetColor;
    property Decimals: Integer read FDecimals write SetDecimals;
    property InfoText: String read fInfoText write SetInfoText;
    property InvalidInputMessage: string read FInvalidInputMessage write FInvalidInputMessage;
    property InvalidRangeMessage: string read FInvalidRangeMessage write FInvalidRangeMessage;
    property MaxValue: Extended read FMaxValue write FMaxValue;
    property MinValue: Extended read FMinValue write FMinValue;
    property NumMode: Boolean read fNumMode write SetNumMode;
    property OnInvalidInput: TInvalidInput read FOnInvalidInput write FOnInvalidInput;
    property OnInvalidRange: TInvalidRange read FOnInvalidRange write FOnInvalidRange;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property ReadOnlyColor: TColor read fReadOnlyColor write SetReadOnlyColor;
    property ShowMode: TShowMode read fShowMode write SetShowMode;
    property Text: String read GetText write SetText;
    property ValidateMode: TValidateModeSet read FValidateMode write FValidateMode default cDefaultValidateMode;
    property Visible: Boolean read GetVisible write SetVisible;
  end;
  

implementation
uses
  mmMBCS, mmCS,
  SysUtils, mmDialogs, dialogs, math;

//:---------------------------------------------------------------------------
//:--- Class: TmmEdit
//:---------------------------------------------------------------------------
constructor TmmEdit.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAlignment     := taLeftJustify;
  fAutoLabel     := TAutoLabel.Create(Self);
  fColor         := inherited Color;
  fInfoText      := '';
  fNumMode       := False;
  fReadOnlyColor := clMMReadOnlyColor;
  fShowMode      := smNormal;

  mFontColor := Font.Color;

  fValidateMode  := cDefaultValidateMode;
end;

//:---------------------------------------------------------------------------
destructor TmmEdit.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.Change;
begin
  inherited Change;
  mDecimalSeparatorPressed := (AnsiPos(Decimalseparator, Text) > 0);
  if NumMode then begin
//    if vmInput in FValidateMode then
    if (not mIsValidating) and (vmInput in FValidateMode) then
      Validate;
  end else begin
    if fInfoText <> '' then begin
      if Focused or (not AnsiSameText(inherited Text, fInfoText)) then begin
        Font.Color := mFontColor;
      end else
        Font.Color := clGrayText;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.CreateParams(var Params: TCreateParams);
  
  const
    Alignments: array[TAlignment] of DWORD = (ES_LEFT, ES_RIGHT, ES_CENTER);
  
begin
  inherited CreateParams(Params);
  Params.Style := Params.Style {or ES_MULTILINE {}or Alignments[Alignment]{};
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.DoEnter;
begin
  if NumMode then
    mOldValue := FloatToStr(AsFloat)
  else
    mOldValue := '';

  inherited DoEnter;
  //:...................................................................
  Font.Color := mFontColor;
  if AnsiSameText(inherited Text, fInfoText) then begin
    inherited Text := '';
  end;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.DoExit;
var
  xOldValidateMode: TValidateModeSet;
begin
  inherited DoExit;

  //:...................................................................
  if vmExit in FValidateMode then begin
    xOldValidateMode := FValidateMode;
    try
      (* Tempor�r ausschliessen, da sonst dauernd Validate aufgerufen werden k�nnte (Change).
         Das kann passieren, wenn Min und/oder Max Werte angegeben sind *)
      Exclude(FValidateMode, vmInput);
      Validate;
    finally
      FValidateMode := xOldValidateMode;
    end;// try finally
  end;// if FValidateMode in [vmExit, vmInput] then begin

  //:...................................................................
  if (inherited Text = '') and (fInfoText <> '') then
    Text := fInfoText;
end;

procedure TmmEdit.DoInvalidInput(aOrgText: string);
begin
  if Assigned(FOnInvalidInput) then FOnInvalidInput(Self, aOrgText);
end;

procedure TmmEdit.DoInvalidRange(aMinValue, aMaxValue, aValue: extended);
begin
  if Assigned(FOnInvalidRange) then FOnInvalidRange(Self, aMinValue, aMaxValue, aValue);
end;

//:---------------------------------------------------------------------------
function TmmEdit.GetAsFloat: Extended;
begin
  if not(mIsValidating) then begin
    if NumMode and (vmReadWrite in  FValidateMode) then
      Validate;
  end;// if not(mIsValidating) then begin

  try
    if Text > '' then
      result := StrToFloat(Text)
    else
      result := 0;
  except
    result := 0;
  end;

end;

//:---------------------------------------------------------------------------
function TmmEdit.GetAsInteger: Integer;
begin
  result := Trunc(AsFloat);
end;

//:---------------------------------------------------------------------------
function TmmEdit.GetColor: TColor;
begin
  Result := fColor;
end;

//:---------------------------------------------------------------------------
function TmmEdit.GetReadOnly: Boolean;
begin
  Result := inherited ReadOnly;
end;

//:---------------------------------------------------------------------------
function TmmEdit.GetText: String;
begin
  Result := inherited Text;
  if AnsiSameText(Result, fInfoText) then
    Result := '';
end;// function TmmEdit.GetText: String;

//:---------------------------------------------------------------------------
function TmmEdit.GetValue: Variant;
begin
  if not(mIsValidating) then begin
    if NumMode and (vmReadWrite in  FValidateMode) then
      Validate;
  end;// if not(mIsValidating) then begin

  result := Text;
end;

//:---------------------------------------------------------------------------
function TmmEdit.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.Loaded;
begin
  inherited Loaded;
  UpdateProperties;
  if (inherited Text = '') and (fInfoText <> '') then begin
    inherited Text := fInfoText;
  end;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetAlignment(Value: TAlignment);
begin
  if FAlignment <> Value then begin
    FAlignment := Value;
    RecreateWnd;
  end;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetAsFloat(const Value: Extended);
begin
  try
    Text := Format('%.*f', [FDecimals, Value]);
  except
    Text := '0';
  end;
  
  if not(mIsValidating) then begin
    if NumMode and (vmReadWrite in  FValidateMode) then
      Validate;
  end;// if not(mIsValidating) then begin
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetAsInteger(const Value: Integer);
begin
  AsFloat := Value;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetColor(Value: TColor);
begin
  if fColor <> Value then begin
    fColor := Value;
    UpdateProperties;
  end;
end;

procedure TmmEdit.SetDecimals(const Value: Integer);
begin
  if FDecimals <> Value then
  begin
    FDecimals := Value;
    Validate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  UpdateProperties;
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetReadOnly(const Value: Boolean);
begin
  inherited ReadOnly := Value;
  UpdateProperties;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetReadOnlyColor(Value: TColor);
begin
  if fReadOnlyColor <> Value then begin
    fReadOnlyColor := Value;
    UpdateProperties;
  end;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetShowMode(Value: TShowMode);
begin
  if fShowMode <> Value then begin
    fShowMode := Value;
    UpdateProperties;
  end;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetText(aValue: String);
begin
  if (not Focused) and (aValue = '') then
    aValue := fInfoText;

  inherited Text := aValue;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetValue(const Value: Variant);
begin
  Text := Value;

  if not(mIsValidating) then begin
    if NumMode and (vmReadWrite in  FValidateMode) then
      Validate;
  end;// if not(mIsValidating) then begin
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

//:---------------------------------------------------------------------------
procedure TmmEdit.UpdateProperties;
begin
  if (ComponentState - [csFreeNotification]) = [] then begin
    if (fShowMode = smExtended) and ReadOnly then
      inherited Color := fReadOnlyColor
    else
      inherited Color := fColor;
  end;
end;


procedure TmmEdit.SetNumMode(const Value: Boolean);
begin
  fNumMode := Value;
  if FNumMode then
    Alignment := taRightJustify
  else
    Alignment := taLeftJustify;
end;

procedure TmmEdit.SetInfoText(const Value: String);
begin
  if (Value <> fInfoText) then begin
    if (AnsiSameText(inherited Text, fInfoText)) then
      inherited Text := Value;
    fInfoText := Value;
    Change;
  end;// if (Value <> fInfoText) then begin
end;

(*---------------------------------------------------------
  Validiert die Eingabe im Nummerischen Modus
----------------------------------------------------------*)
procedure TmmEdit.Validate;
var
  xOrgText: string;
  xExtended: Extended;
  //..........................................................
  procedure LocalSetText(aText: string);
  begin
    inherited Text := aText;
  end;
  //..........................................................
  function LocalGetText: string;
  begin
    Result := inherited Text;
  end;
  //..........................................................
  procedure ChangeText(aText: string);
  var
    xOldSelStart: Integer;
  begin
      xOldSelStart := SelStart;// - 1;
      LocalSetText(aText);
      SelStart := xOldSelStart;
  end;// procedure ChangeText(aText: string);
  //..........................................................
begin
  mIsValidating := true;
  try
    if NumMode then begin
      // Erst mal die Eingabe sichern
      xOrgText := LocalGetText;
      try
        // Beschr�nkung auf Zahlen
        if LocalGetText > '' then begin
          if (LocalGetText <> '-') and (LocalGetText <> '+')  then begin
            // Erzeugt eine Exception, wenn die Eingabe ung�ltige Zeichen enth�lt
            xExtended := StrToFloat(xOrgText);
            // Wenn die Umwandlung funktinoierte, dann gleich wieder nach Text konvertieren
            // und zwar mit der Precision wie im Property Decimals angegeben
            if FDecimals < 0 then
              ChangeText(Format('%g', [xExtended]))
            else begin
              if AnsiPos('E', UpperCase(xOrgText)) > 0 then
                ChangeText(Format('%.*g', [FDecimals, xExtended]))
              else
                ChangeText(Format('%.*f', [FDecimals, xExtended]));
            end;
          end;
        end;// if LocalGetText > '' then begin
        // Jetzt ist sichergestellt, dass die Eingabe schon mal nummerisch ist
        mOldValue := LocalGetText;
      except
        // Wenn definiert, dann Fehlermeldung ausgeben
        if FInvalidInputMessage > '' then
          MMMessageDlg(FInvalidInputMessage, mtError, [mbOK], 0);
        // Event ausl�sen
        DoInvalidInput(xOrgText);
        // und alten Wert wieder herstellen
        ChangeText(mOldValue);
      end;// try except

{
      if FDecimals >= 0 then begin
        (* Kontrolle und Anpassung der Dezimalstellen. Es muss ber�cksichtigt werden, dass die Eingabe
           auch mit Exponenten best�ckt sein k�nnte (z.B.: '1.23e+5' oder '1.23e5' oder '1.23e-5' oder '-1.23e+5' usw.)

           1) Trennung von Vorkomma- und Nachkomma Anteil (xPre, xPost)
           2) Wenn ein Exponent vorhanden ist diesen separat sichern (xExponent)
           3) Nachkommastellen begrenzen.
              Es gilt: Eingaben werden NICHT abgeschnitten sondern kaufm�nnisch gerundet.
                       Decimals = 2; Eingabe = '12.345'; Inhalt Editfeld = '12.35'
                       Decimals = 2; Eingabe = '12.344'; Inhalt Editfeld = '12.34'
                       Decimals = 2; Eingabe = '12.399'; Inhalt Editfeld = '12.4'
                       Decimals = 2; Eingabe = '12.401'; Inhalt Editfeld = '12.4'
           4) Text aus Vorkamma- nud Nachkomma Anteil wieder zusammensetzen
           5) Pr�fen und gegebenenfalls Korrigieren ob untere Grenzen eingehalten werden
           6) Pr�fen und gegebenenfalls Korrigieren ob obere Grenzen eingehalten werden
        *)
        xExponent := '';

        // 1)
        xPost := LocalGetText;
        xPre := copyLeft(xPost, DecimalSeparator, true);

        // 2)
        xExponentPos := AnsiPos('E', UpperCase(xPost));
        if xExponentPos > 0 then
          xExponent := copy(xPost, xExponentPos, MAXINT);

        // 3)
        if (Length(xPost) - Length(xExponent)) > FDecimals then begin
          (* Um Datentyp unabh�ngig zu sein muss folgender Weg beschritten werden:
             1. Nachkommaanteil auf Anzahl Dezimalstellen + 1 begrenzen und als Integer weiterverarbeiten
             2. Durch 10 teilen --> eine Dezimalstelle
             3. 0.5 addieren um kaufm�nnisch zu Runden -->
                    34.5 + 0.5 = 35
                    34.4 + 0.5 = 34.9
                    34.6 + 0.5 = 35.1
             4. Nachkommanateil abschneiden und das Ergebnis als neuen Nachkommanteil behalten
             5. Abschliessende Nullen entfernen
          *)
          xPostNum := Trunc((StrToIntDef(copy(xPost, 1, FDecimals + 1), 0) / 10) + 0.5);
          // Gesamten Nachkommanteil wieder als String zusammensetzen
          xPost := IntToStr(xPostNum);
          // Nullen am Schluss abschneiden
          while (xPost > '') and (xPost[Length(xPost)] = '0') do
            xPost := Copy(xPost, 1, Length(xPost) - 1);
          xPost := xPost + xExponent;
        end;// if (Length(xPost) - Length(xExponent)) > FDecimals then begin

        // 4)
        xDecimalSeparator := DecimalSeparator;
        // Je nach dem den zu verwendenden DecimalSeparator bestimmen (Wir lassen keine Strings in der Form '12.' zu)
        if (xPost = '') and (not(mDecimalSeparatorPressed)) then
          xDecimalSeparator := '';
        // Ganze Zahl aus Vor- und Nachkommanteil wieder zusammensetzen
        if (FDecimals > 0) then
          ChangeText(xPre + xDecimalSeparator + xPost)
        else
          ChangeText(xPre);

      end;// if FDecimals >= 0 then begin
{}

      // 5) Minimum
      if MinValue <> 0 then begin
        if AsFloat < MinValue then begin
          // Event aufrufen bei Minimum Verletzung
          DoInvalidRange(FMinValue, FMaxValue, AsFloat);
          // Minimum zuweisen
          AsFloat := MinValue;
          // Wenn definiert, dann die Fehlermeldung ausgeben
          if FInvalidRangeMessage > '' then
            MMMessageDlg(FInvalidRangeMessage, mtError, [mbOK], 0);
        end;// if AsFloat < MinValue then begin
      end;// if MinValue <> 0 then begin

      // 6) Maximum
      if MaxValue <> 0 then begin
        // Siehe Minimum
        if AsFloat > MaxValue then begin
          DoInvalidRange(FMinValue, FMaxValue, AsFloat);
          AsFloat := MaxValue;
          if FInvalidRangeMessage > '' then
            MMMessageDlg(FInvalidRangeMessage, mtError, [mbOK], 0);
        end;// if AsFloat > MaxValue then begin
      end;// if MaxValue <> 0 then begin

      // F�r interne Zwecke merken, ob der Dezimal Punkt vorkommt
      mDecimalSeparatorPressed := (AnsiPos(Decimalseparator, xOrgText) > 0);
//      mDecimalSeparatorPressed := (AnsiPos(Decimalseparator, LocalGetText) > 0);
    end;// if NumMode then begin
  finally
    mIsValidating := false;
  end;
end;

end.

