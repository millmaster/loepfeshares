(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmQRChart.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 14.03.2001  0.00  Wss | properties Alignment and AlignToBand enabled
|=========================================================================================*)
unit mmQRChart;

interface

uses
  Classes, QRTee;

type
  TmmQRChart = class(TQRChart)
  private
  protected
  public
  published
    property Alignment;
    property AlignToBand;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;


end.
