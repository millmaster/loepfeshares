(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmToolBar.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmToolBar;

interface

uses
  Classes, ComCtrls,

  Windows, Messages, SysUtils,  Graphics, Controls, Forms, Dialogs,
  ToolWin, ExtCtrls;

type
  TmmToolBar = class(TToolBar)
  private
//    FPicture: TPicture;
//    FDrawing: Boolean;
    //procedure SetPicture(const Value: TPicture);
    //procedure PictureChanged(Sender: TObject);
  protected
    { Protected declarations }
    //function DoPaletteChange: Boolean;
  public
    { Public declarations }
    //constructor Create(AOwner: TComponent); override;
    //destructor Destroy; override;

  published
    { Published declarations }
 //   property Picture: TPicture read FPicture write SetPicture;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//procedure Register;
//begin
//  RegisterComponents('Win32', [TmmToolBar]);
//end;

{ TmmToolBar }
{
constructor TmmToolBar.Create(AOwner: TComponent);
begin
  inherited;
  FPicture := TPicture.Create;
  FPicture.OnChange := PictureChanged;
end;

destructor TmmToolBar.Destroy;
begin
  FPicture.Free;
  inherited Destroy;
end;

function TmmToolBar.DoPaletteChange: Boolean;
var
  ParentForm: TCustomForm;
  Tmp: TGraphic;
begin
  Result := False;
  Tmp := Picture.Graphic;
  if Visible and (not (csLoading in ComponentState)) and (Tmp <> nil) and
    (Tmp.PaletteModified) then
  begin
    if (Tmp.Palette = 0) then
      Tmp.PaletteModified := False
    else
    begin
      ParentForm := GetParentForm(Self);
      if Assigned(ParentForm) and ParentForm.Active and Parentform.HandleAllocated then
      begin
        if FDrawing then
          ParentForm.Perform(WM_QUERYNEWPALETTE, 0, 0)
        else
          PostMessage(ParentForm.Handle, WM_QUERYNEWPALETTE, 0, 0);
        Result := True;
        Tmp.PaletteModified := False;
      end;
    end;
  end;
end;

procedure TmmToolBar.PictureChanged(Sender: TObject);
begin

  if Picture.Graphic <> nil then
    if DoPaletteChange and FDrawing then Update;
  if not FDrawing then Invalidate;

end;

procedure TmmToolBar.SetPicture(const Value: TPicture);
begin
  FPicture.Assign(Value);
  Self.Canvas.Brush.Bitmap.Assign(FPicture.Bitmap);
end;
}
end.
