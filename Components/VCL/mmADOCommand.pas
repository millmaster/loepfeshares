(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmADOCommand.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.04.2002  1.00  Wss | Initial Release
| 28.10.2002  1.01  LOK | CheckForDoubleParameter pr�ft vor dem �ffnen der Datenmenge auf doppelte Parameter
|=========================================================================================*)
unit mmADOCommand;

interface

uses
  mmADODataSet,  // F�r die Routine "CheckForDoubleParameter ()" (siehe Execute)
  ADODb;

type
  TmmADOCommand = class(TADOCommand)
  private
  protected
  public
    function Execute: _Recordset; reintroduce; overload;
  published
  end;


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{ TmmADOCommand }

function TmmADOCommand.Execute: _Recordset;
begin
  CheckForDoubleParameter(Parameters);
  result := inherited Execute;
end;

end.

