(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmADODataSet.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.04.2002  1.00  Wss | Initial Release
| 28.10.2002  1.01  LOK | Diverse Erweiterungen:
|                       |   property ReadOnly erweitert (Kompatibilit�t zu TTable)
|                       |   Assign Routinen um von oder zu einem TNativeAdoQuery zu kopieren
|                       |   CheckForDoubleParameter pr�ft vor dem �ffnen der Datenmenge auf doppelte Parameter
|=========================================================================================*)
unit mmADODataSet;

interface

uses
  ADODb, AdoDBAccess, classes, ADODB_TLB, ADOInt, SysUtils, mmCS;

type
  TmmADODataSet = class(TADODataSet)
  private
    function GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
  protected
    procedure SetActive(Value: Boolean); override;
  public
    procedure AssignTo(Dest: TPersistent); override;
    procedure Assign(Source: TPersistent); override;
  published
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly stored False;
  end;

  procedure CheckForDoubleParameter(xParameters: TParameters);

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------
//  Diese Prozedur setzt Parameter mit gleichem Namen auf den gleichen Wert (Verhalten
//  wie BDE).
//  Bei ADO Express wird lediglich der erste Parameter gefunden. Alle weiteren
//  Parameter mit dem selben Namen werden mit dem Wert NULL an die DB geschickt.
//
//  Funktionsweise:
//    Sobald die Datenmenge aktiv wird (Open oder active := true)  werden
//    alle definierten Parameter gescannt. Wird dabei ein Parameter mit dem Wert "NULL"
//    gefunden, dann wird die Parameterliste nach einem Parameter mit dem selben Namen
//    durchsucht, und im Erfolgsfall der Wert �bernommen.
//
//  Diese Prozedur wird auch von TmmADOCommand verwendet.
//------------------------------------------------------------------------------
procedure CheckForDoubleParameter(xParameters: TParameters);
var
  i,
  j: integer;
  xParamFound: boolean;
begin
  for i:=0 to xParameters.count -1 do begin
    if xParameters[i].value = NULL then begin
      j := i - 1;
      xParamFound := false;
      while ((j >= 0) and (not(xParamFound))) do begin
        if CompareText(xParameters[i].Name, xParameters[j].Name) = 0 then begin
          xParameters[i].Value := xParameters[j].value;
          xParamFound := true;
        end;// if CompareText(xParameters[i].Name, xParameters[j].Name) = 0 then begin
        dec(j);
      end;// while ((j >= 0) and (not(xParamFound))) do begin
    end;// if xParameters[i].value = NULL then begin
  end;// for i:=0 to xParameters.count -1 do begin
end;// procedure CheckForDoubleParameter(xParameters: TParameters);

{ TmmADODataSet }

//------------------------------------------------------------------------------
procedure TmmADODataSet.Assign(Source: TPersistent);
var
  xQry: TNativeAdoQuery;
begin
  try
    if (Source is TNativeAdoQuery) then begin
      xQry := TNativeAdoQuery(Source);
      Connection.ConnectionObject := ADOInt.Connection(xQry.Connection);
      CommandText := xQry.SQL.Text;
      Recordset := ADOInt.Recordset(xQry.Recordset);
    end else begin
      if (Source is TmmAdoDataSet) then begin
        Connection := (Source as TmmAdoDataset).Connection;
      end else begin
        inherited;
      end;// if (Source is TmmAdoDataSet) then begin
    end;// if (Source is TNativeAdoQuery) then begin
  except
    on e:Exception do begin
      codeSite.SendError('TmmAdoDataSet.Assign: ' + e.Message);
      raise;
    end;// on e:Exception do begin
  end;// try except
end;// procedure TmmADODataSet.Assign(Source: TPersistent);

//------------------------------------------------------------------------------
procedure TmmADODataSet.AssignTo(Dest: TPersistent);
var
  xQry: TNativeAdoQuery;
begin
  if (Dest is TNativeAdoQuery) then begin
    try
      xQry := TNativeAdoQuery(Dest);
      xQry.Connection := ADODB_TLB.Connection(Connection.ConnectionObject);
      xQry.SQL.Text := CommandText;
      xQry.Recordset := ADODB_TLB.Recordset(Recordset);
    except
      on e:Exception do begin
        CodeSite.SendError('TmmADODataSet.AssignTo: ' + e.Message);
      end;// on e:Exception do begin
    end;// try finally
  end else begin
    inherited;
  end;// if (Dest is TNativeAdoQuery) then begin
end;// procedure TmmADODataSet.AssignTo(Dest: TPersistent);

//------------------------------------------------------------------------------
function TmmADODataSet.GetReadOnly: Boolean;
begin
  Result := LockType = ltReadOnly;
end;// function TmmADODataSet.GetReadOnly: Boolean;

//------------------------------------------------------------------------------
procedure TmmADODataSet.SetActive(Value: Boolean);
begin
  if ((Active <> Value) and (Active = false)) then
    CheckForDoubleParameter(Parameters);
  inherited SetActive(Value);
end;// procedure TmmADODataSet.SetActive(Value: Boolean);

//------------------------------------------------------------------------------
procedure TmmADODataSet.SetReadOnly(const Value: Boolean);
begin
  if Value then
    LockType := ltReadOnly else
    LockType := ltOptimistic;
end;// procedure TmmADODataSet.SetReadOnly(const Value: Boolean);

end.

