(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmAboutDlg.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmAboutDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TmmAboutDlg = class(TComponent)
  private
    FTitle: String;
    FProgramIcon: TPicture;
    FProgramName: String;
    FProgramVersion: String;
    FCopyright: String;
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Execute: Boolean;
  published
    property Title: String read FTitle write FTitle;
    property ProgramIcon: TPicture read FProgramIcon write FProgramIcon;
    property ProgramName: String read FProgramName write FProgramName;
    property ProgramVersion: String read FProgramVersion write FProgramVersion;
    property Copyright: String read FCopyright write FCopyright;
    { Published declarations }
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  DlgAbout;

//procedure Register;
//begin
//  RegisterComponents('Samples', [TmmAboutDlg]);
//end;

constructor TmmAboutDlg.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FProgramIcon := TPicture.Create;
end;

destructor TmmAboutDlg.Destroy;
begin
  FProgramIcon.Free;
  inherited Destroy;
end;

function TmmAboutDlg.Execute: Boolean;
begin
  DlgAbout_Data := TDlgAbout_Data.Create(Application);
  try
    DlgAbout_Data.Caption                   := Title + ' ' + ProgramName;
    DlgAbout_Data.ImgProgramIcon.Picture    := ProgramIcon;
    DlgAbout_Data.LblProgramName.Caption    := ProgramName;
    DlgAbout_Data.LblProgramVersion.Caption := ProgramVersion;
    DlgAbout_Data.LblCopyRight.Caption      := Copyright;
    Result := (DlgAbout_Data.ShowModal = IDOK);
  finally
    DlgAbout_Data.Free;
  end;
end;

end.
