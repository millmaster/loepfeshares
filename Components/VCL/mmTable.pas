(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmTable.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 25.06.1999  1.00  Wss | Barco code remarked
|=========================================================================================*)
unit mmTable;

interface

uses
  SysUtils, Classes, DB, DBTables, Dialogs, mmDatabase;

type
  PLocalList = ^ALocalList;
  ALocalList = record
    Name: String;
    Value: Variant;
  end;

type
  TmmTable = class(TTable)
  private
{
    FFormatFields: Boolean;
    FUserAccess: TmmUserAccess;
    FExportFile: String;
    FPKFields: TStringList;
    FCopyFilter: String;
    FCreateDependants: String;
//    FAutoRefresh: Boolean;
    CopyPasteBuffer: TList;
    FilterByBuffer: TList;
    FDisabledEvents: Boolean;
    FFilterFields: String;
    FMinFilterFields: String;
    FSaveFilter: String;
    FFilterSaved: Boolean;
    SavAfterCancel: TDataSetNotifyEvent;
    SavAfterClose: TDataSetNotifyEvent;
    SavAfterDelete: TDataSetNotifyEvent;
    SavAfterEdit: TDataSetNotifyEvent;
    SavAfterInsert: TDataSetNotifyEvent;
    SavAfterOpen: TDataSetNotifyEvent;
    SavAfterPost: TDataSetNotifyEvent;
    SavAfterScroll: TDataSetNotifyEvent;
    SavBeforeCancel: TDataSetNotifyEvent;
    SavBeforeClose: TDataSetNotifyEvent;
    SavBeforeDelete: TDataSetNotifyEvent;
    SavBeforeEdit: TDataSetNotifyEvent;
    SavBeforeInsert: TDataSetNotifyEvent;
    SavBeforeOpen: TDataSetNotifyEvent;
    SavBeforePost: TDataSetNotifyEvent;
    SavBeforeScroll: TDataSetNotifyEvent;
    SavOnCalcFields: TDataSetNotifyEvent;
    SavOnDeleteError: TDataSetErrorEvent;
    SavOnEditError: TDataSetErrorEvent;
    SavOnFilterRecord: TFilterRecordEvent;
    SavOnNewRecord: TDataSetNotifyEvent;
    SavOnPostError: TDataSetErrorEvent;
//    SavOnServerYield: TOnServerYieldEvent;
    SavOnUpdateError: TUpdateErrorEvent;
    SavOnUpdateRecord: TUpdateRecordEvent;
    procedure SetFormatFields(Value: Boolean);
    procedure SetUserAccess(Value: TmmUserAccess);
    procedure SetExportFile(Value: String);
//    procedure SetAutoRefresh(Value: Boolean);
    procedure ParseSchemaName;
    function GetPKFields : TStringList;
    function GetCopyBufValid: Boolean;
{}
  protected
{
    procedure InternalPost; override;
    procedure InternalEdit; override;
    procedure InternalDelete; override;
//    procedure InternalAddRecord(Buffer: Pointer; Append: Boolean); override;
    procedure DoAfterInsert; override;
    function KeyEqual(CreateFromKey: Variant; RefTable: TmmTable): Boolean;
{}
  public
{
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DoBeforeInsert; override;
    procedure DoBeforeDelete; override;
    procedure DoBeforePost; override;
    procedure DoBeforeOpen; override;
    procedure DoAfterPost; override;
    procedure DoAfterCancel; override;
    procedure ProtectPKFields;
    procedure UnprotectPKFields;
    property PKFields: TStringList read GetPKFields;
    property ExportFile: String read FExportFile write SetExportFile;
    procedure ExportData(AllFields: Boolean);
    procedure CreateLike(CreateVisiblePKFields: Boolean; PostRec: Boolean; NewKeyvalues:Variant;
              var CreateFromKey: Variant);
    procedure CreateDependantRows(CreateFromKey: Variant);
    procedure CopyToBuffer(Buffer:TList; CheckFilter:Boolean);
    procedure PasteFromBuffer(Buffer:TList);
    procedure ClearBuffer(Buffer:TList);
    procedure DestroyBuffer(var Buffer:TList);
    procedure DisableEvents;
    property  DisabledEvents: Boolean read FDisabledEvents write FDisabledEvents;
    property  CopyBufValid: Boolean read GetCopyBufValid;
    procedure EnableEvents;
    function  GetFilterString(Buffer:TList) : String;
    procedure AddOneListItem(LocField:TField; Buffer:TList; CheckFilter:Boolean);
    procedure GetInternalCopyFilter;
    procedure EnableFilterBy(EnableIt:Boolean);
    procedure CopyAction;
    procedure PasteAction;
    procedure FilterByFormAction;
    procedure ClearAction;
    function  ApplyFilterAction:Boolean;
    function  FilterBySelAction(LocField:TField):Boolean;
{}
  published
{
    property FormatFields: Boolean read FFormatFields write SetFormatFields default False;
    property UserAccess: TmmUserAccess read FUserAccess write SetUserAccess default uaFull;
//    property AutoRefresh: Boolean read FAutoRefresh write SetAutoRefresh default False;
    //CopyFilter = List of columns not to copy
    property CopyFilter: String read FCopyFilter write FCopyFilter ;
    //CreateDependants = List of dependant tables to copy after current(master) record
    property CreateDependants: String read FCreateDependants write FCreateDependants ;
{}
  end;

//procedure Register;

{
const
  eKeyViol              = 9729;
  eRequiredFieldMissing = 9732;
  eForeignKeyError      = 9733;  // hver 9/9/98   13059;
  eDetailsExist         = 9734;
//eMasterRecMissing     = 9733;  // hver 9/9/98
  eReferenceCheck       = 13059; // hver 9/9/98
{}

{ see \Borland\Delphi3\Doc\BDE.INT
  ERRCODE_KEYVIOL               = 1;      { Key violation
  ERRCODE_MINVALERR             = 2;      { Min val check failed
  ERRCODE_MAXVALERR             = 3;      { Max val check failed
  ERRCODE_REQDERR               = 4;      { Field value required
  ERRCODE_FORIEGNKEYERR         = 5;      { Master record missing
  ERRCODE_DETAILRECORDSEXIST    = 6;      { Cannot MODIFY or DELETE this Master record
  ERRCODE_MASTERTBLLEVEL        = 7;      { Master Table Level is incorrect
  ERRCODE_LOOKUPTABLEERR        = 8;      { Field value out of lookup tbl range
  ERRCODE_LOOKUPTBLOPENERR      = 9;      { Lookup Table Open failed
  ERRCODE_DETAILTBLOPENERR      = 10;     { 0x0a Detail Table Open failed
  ERRCODE_MASTERTBLOPENERR      = 11;     { 0x0b Master Table Open failed
  ERRCODE_FIELDISBLANK          = 12;     { 0x0c Field is blank

  ERRBASE_INTEGRITY = 9728

  DBIERR_KEYVIOL                = (ERRBASE_INTEGRITY + ERRCODE_KEYVIOL);
  DBIERR_MINVALERR              = (ERRBASE_INTEGRITY + ERRCODE_MINVALERR);
  DBIERR_MAXVALERR              = (ERRBASE_INTEGRITY + ERRCODE_MAXVALERR);
  DBIERR_REQDERR                = (ERRBASE_INTEGRITY + ERRCODE_REQDERR);
  DBIERR_FORIEGNKEYERR          = (ERRBASE_INTEGRITY + ERRCODE_FORIEGNKEYERR);
  DBIERR_DETAILRECORDSEXIST     = (ERRBASE_INTEGRITY + ERRCODE_DETAILRECORDSEXIST);
  DBIERR_MASTERTBLLEVEL         = (ERRBASE_INTEGRITY + ERRCODE_MASTERTBLLEVEL);
  DBIERR_LOOKUPTABLEERR         = (ERRBASE_INTEGRITY + ERRCODE_LOOKUPTABLEERR);
  DBIERR_LOOKUPTBLOPENERR       = (ERRBASE_INTEGRITY + ERRCODE_LOOKUPTBLOPENERR);
  DBIERR_DETAILTBLOPENERR       = (ERRBASE_INTEGRITY + ERRCODE_DETAILTBLOPENERR);
  DBIERR_MASTERTBLOPENERR       = (ERRBASE_INTEGRITY + ERRCODE_MASTERTBLOPENERR);
  DBIERR_FIELDISBLANK           = (ERRBASE_INTEGRITY + ERRCODE_FIELDISBLANK);
}

implementation // 15.07.2002 added mmMBCS to imported units

{
uses
  mmMBCS,

    mmLib;
{}

//procedure Register;
//begin
//  RegisterComponents('Data Access', [TmmTable]);
//end;

{
constructor TmmTable.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFormatFields   := False;
  FUserAccess     := uaFull;
  FPKFields       := TStringList.Create;
  FDisabledEvents := False;
  FFilterSaved    := False;
//  FAutoRefresh  := False;
end;

destructor TmmTable.Destroy;
begin
  FPKFields.Free;
  if not(CopyPasteBuffer = nil) then DestroyBuffer(CopyPasteBuffer);
  if not(FilterByBuffer  = nil) then DestroyBuffer(FilterByBuffer);
  inherited Destroy;
end;

procedure TmmTable.SetFormatFields(Value: Boolean);
var
  I,NewPos,LastPos: integer;
  FieldName,DLabel: string;
begin
//  Filter   := 'C_DELETE=0';
//  Filtered := True;
  if FFormatFields <> Value then
  begin
    if (Value) then
    begin
      for I := 0 to FieldCount-1 do
      begin
        FieldName := Fields[I].FieldName;
        if (FieldName = 'C_CREATOR') or
           (FieldName = 'C_CREATE_DATE') or
           (FieldName = 'C_MODIFIER') or
           (FieldName = 'C_MODIFY_DATE') or
           (FieldName = 'C_DELETER') or
           (FieldName = 'C_DELETE_DATE') then
         begin
           Fields[I].ReadOnly := True;
           Fields[I].Visible  := False;
         end
        else if (FieldName = 'C_DELETE') then
         begin
           Fields[I].ReadOnly := False;
           Fields[I].Visible  := False;
         end;
        DLabel := Copy(FieldName,3,99);
        if (Pos('_ID',DLabel) <> 0) then
          DLabel := Copy(DLabel,1,Pos('_ID',DLabel)-1);
        LastPos := 1;
        While (Pos('_',DLabel) <> 0) do
        begin
          NewPos := Pos('_',DLabel);
          DLabel := Copy(DLabel,1,LastPos)
                    + LowerCase(Copy(DLabel,LastPos+1,NewPos-LastPos-1))
                    + ' '
                    + Copy(DLabel,NewPos+1,99);
          LastPos := NewPos;
        end;
        if (LastPos = 1) then LastPos := 0;
        DLabel := Copy(DLabel,1,LastPos+1)
                  + LowerCase(Copy(DLabel,LastPos+2,99));
        Fields[I].DisplayLabel := DLabel;
      end;
    end;
    FFormatFields := Value;
  end;
end;

procedure TmmTable.SetUserAccess(Value: TmmUserAccess);
begin
  if FUserAccess <> Value then
  begin
    FUserAccess := Value;
    if Value = uaReadOnly then
      ReadOnly := True
    else
      ReadOnly := False;
  end;
end;

procedure TmmTable.SetExportFile(Value: String);
begin
  if FExportFile <> Value then
  begin
    FExportFile := Value;
  end;
end;

procedure TmmTable.ProtectPKFields;
var
  I:integer;
begin
  for I := 0 to FieldCount-1 do
  begin
    if (Fields[I].Tag and 1) = 1 then
      Fields[I].ReadOnly := TRUE;
  end;
end;

procedure TmmTable.UnprotectPKFields;
var
  I:integer;
begin
  for I := 0 to FieldCount-1 do
  begin
    if (Fields[I].Tag and 1) = 1 then
      Fields[I].ReadOnly := FALSE;
  end;
end;

procedure TmmTable.SetAutoRefresh(Value: Boolean);
begin
  if FAutoRefresh <> Value then
  begin
    FAutoRefresh := Value;
  end;
end;

procedure TmmTable.DoBeforeOpen;
begin
  inherited;
  ParseSchemaName;
  ProtectPKFields;
end;

procedure TmmTable.ParseSchemaName;
var
  LocDatabase: TDatabase;
begin
  if (csDesigning in ComponentState) then Exit;
  if (Pos('.',TableName) = 0) then
  begin
    LocDatabase := Session.FindDatabase(DatabaseName);
    if (LocDataBase <> nil) then
      with LocDatabase as TmmDatabase do
      begin
        if (not LocDatabase.Connected) then LocDatabase.Open;
        if (SchemaName <> '') then
          TableName := SchemaName + '.' + TableName;
      end;
  end;
end;

procedure TmmTable.DoAfterPost;
begin
  inherited;
  ProtectPKFields;
  Refresh;        // to get rowid and 'c_modify_date',...
end;

procedure TmmTable.DoAfterCancel;
begin
  inherited;
  ProtectPKFields;
end;

procedure TmmTable.DoBeforeInsert;
begin
  if (UserAccess <> uaFull) then Abort;
  UnprotectPKFields;
  inherited;
end;

procedure TmmTable.DoBeforeDelete;
begin
  if (UserAccess <> uaFull) then Abort;
//  try  //Catch the delete action and try to convert it into setting the deletion mark
//    if not (State in [dsEdit,dsInsert]) then Edit;
//    FieldValues['C_DELETE'] := 1;
//    Post;
//    SysUtils.Abort;
//  except
    inherited;
//  end;
end;

procedure TmmTable.DoBeforePost;
var
  I:integer;
begin
 for I := 0 to FieldCount-1 do
  begin
    case Fields[I].DataType of
     ftstring:
       if ((Fields[I].Value = '') and ((Fields[I].Tag and 2) = 2)) then
       begin
         Fields[I].Value := Null;
       end;
     ftSmallInt,ftInteger,ftWord,ftFloat,ftDate,ftTime,ftDateTime:
       if ((Fields[I].Value = 0) and ((Fields[I].Tag and 2) = 2)) then
       begin
         Fields[I].Value := Null;
       end;
    end;
  end;
  inherited;
end;

procedure TmmTable.ExportData(AllFields: Boolean);
var
  ExpFile: TextFile;
  Bookmark: TBookmark;
  i: Integer;
begin
  if FExportFile <> '' then
  begin
    Bookmark := GetBookmark;
    AssignFile(ExpFile,ExportFile);
    DisableControls;
    try
      Rewrite(ExpFile);
      First;
      for i:=0 to FieldCount-1 do
          if (Fields[i].Visible or AllFields) then
             Write(ExpFile, Fields[i].DisplayLabel, ' ' :Fields[i].DisplayWidth-Length(Fields[i].DisplayLabel));
      Writeln(ExpFile);
      while not EOF do
      begin
        for i:=0 to FieldCount-1 do
          if (Fields[i].Visible or AllFields) then
             Write(ExpFile, Fields[i].AsString, ' ' :Fields[i].DisplayWidth-Length(Fields[i].AsString));
        Writeln(ExpFile);
        Next;
      end;
    finally
      GotoBookmark(Bookmark);
      EnableControls;
      CloseFile(ExpFile);
      FreeBookmark(Bookmark);
    end;
  end;
end;

function TmmTable.GetPKFields : TStringList;
var
  I: Integer;
begin
  // Fill in PKFields using loop activesource.dataset.fields[n].tag = 1
  if FPKFields.Count = 0 then
  begin
    for I := 0 to FieldCount - 1 do
      if ((Fields[I].Tag and 1) = 1) then
          FPKFields.Add(Fields[I].FieldName);
  end;
  Result := FPKFields;
end;

procedure TmmTable.InternalPost;
var
  iDBIError,I,Pos1,Pos2: Integer;
  LString, LString2: string;
begin
  LString := '';
  try
    Inherited;
  except
    On E: EDatabaseError do
    begin
//
      mmException(Self,E);
//
      iDBIError := (E as EDBEngineError).Errors[0].Errorcode;
      case iDBIError of
        eKeyViol:
        begin
          Pos1 := Pos('_PK_',(E as EDBEngineError).Errors[1].message);
          if (Pos1 > 0) then
          begin
            for I := 0 to PKFields.Count-1  do
            begin
              if I = 0 then
                LString := LString + FieldByName(PKFields.Strings[I]).DisplayLabel
                           + ' = ' + FieldByName(PKFields.Strings[I]).Asstring
              else
                LString := LString + ' and ' + FieldByName(PKFields.Strings[I]).DisplayLabel
                           + ' = ' + FieldByName(PKFields.Strings[I]).Asstring;
            end;
            if LString <> '' then
              E.Message := 'Post not succeeded.'#13#10'A record with '+LString+' already exists';
          end
          else
          begin
            Pos1 := Pos('.CON_',(E as EDBEngineError).Errors[1].message);
            Pos2 := Pos('_UNI_',(E as EDBEngineError).Errors[1].message);
            if (Pos1 > 0) and (Pos2 > 0) then
            begin
              LString := Copy((E as EDBEngineError).Errors[1].message,Pos1+5,Pos2-Pos1-5);
              E.Message := 'Post not succeeded.'#13#10+LString+' should be unique';
            end
          end;
          raise;
        end;
        eForeignKeyError:
        begin
          Pos1 := Pos('.CON_',(E as EDBEngineError).Errors[1].message);
          Pos2 := Pos('_FK_',(E as EDBEngineError).Errors[1].message);
          if (Pos1 > 0) and (Pos2 > 0) then
          begin
            LString := Copy((E as EDBEngineError).Errors[1].message,Pos1+5,Pos2-Pos1-5);
            E.Message := 'Post not succeeded.'#13#10'Unknown '+LString+' reference';
          end
          else
          begin
            Pos2 := Pos('_CHK_',(E as EDBEngineError).Errors[1].message);
            if (Pos1 > 0) and (Pos2 > 0) then
            begin
              LString := Copy((E as EDBEngineError).Errors[1].message,Pos1+5,Pos2-Pos1-5);
              E.Message := 'Post not succeeded.'#13#10'Validity check on '+LString+' failed';
            end;
          end;
          raise;
        end;
        eDetailsExist, eReferenceCheck:
        begin
          Pos1 := Pos('.CON_',(E as EDBEngineError).Errors[1].message);
          Pos2 := Pos('_FK_',(E as EDBEngineError).Errors[1].message);
          if (Pos1 > 0) and (Pos2 > 0) then
          begin
//            for I := 0 to PKFields.Count-1  do
//            begin
//              if I = 0 then
//                LString := LString + FieldByName(PKFields.Strings[I]).DisplayLabel
//                           + ' = ' + FieldByName(PKFields.Strings[I]).OldValue
//              else
//                LString := LString + ' and ' + FieldByName(PKFields.Strings[I]).DisplayLabel
//                           + ' = ' + FieldByName(PKFields.Strings[I]).OldValue;
//            end;
            LString2 := Copy((E as EDBEngineError).Errors[1].message,Pos2+4,64);
            Pos1 := Pos(')',LString2);
            LString2 := Copy(LString2,1,Pos1-1);
//            E.Message := 'Post not succeeded.'#13#10+'Record with '+LString+' still used in '+LString2+' table';
            E.Message := 'Post not succeeded.'#13#10+'Record still used in '+LString2+' table';
          end
          else
          begin
            Pos2 := Pos('_CHK_',(E as EDBEngineError).Errors[1].message);
            if (Pos1 > 0) and (Pos2 > 0) then
            begin
              LString := Copy((E as EDBEngineError).Errors[1].message,Pos1+5,Pos2-Pos1-5);
              E.Message := 'Post not succeeded.'#13#10'Validity check on '+LString+' failed';
            end;
          end;
          raise;
        end;
        eRequiredFieldMissing:
        begin
          E.Message := 'Post not succeeded.'#13#10'A required field missing';
          raise;
        end;
//      eMasterRecMissing:
//      begin
//        E.Message := 'Post not succeeded.'#13#10'Master record missing';
//        raise;
//      end;
        else
          raise;
      end;
    end;
  end;
end;

procedure TmmTable.InternalEdit;
begin
  Inherited;
end;

procedure TmmTable.InternalDelete;
var
  iDBIError,Pos1,Pos2: Integer;
  LString: string;
begin
  LString := '';
  try
    Inherited;
  except
    On E: EDatabaseError do
    begin
//
      mmException(Self,E);
//
      iDBIError := (E as EDBEngineError).Errors[0].Errorcode;
      case iDBIError of
        eForeignKeyError:
        begin
          Pos1 := Pos('_FK_',(E as EDBEngineError).Errors[1].message);
          Pos2 := Pos(')',(E as EDBEngineError).Errors[1].message);
          if (Pos1 > 0) and (Pos2 > 0) then
          begin
            LString := Copy((E as EDBEngineError).Errors[1].message,Pos1+4,Pos2-Pos1-4);
            E.Message := 'Delete not succeeded.'#13#10'The current record is still being referenced by table '+LString;
          end;
          raise;
        end;
        eDetailsExist,eReferenceCheck:
        begin
          Pos1 := Pos('.CON_',(E as EDBEngineError).Errors[1].message);
          Pos2 := Pos('_FK_',(E as EDBEngineError).Errors[1].message);
          if (Pos1 > 0) and (Pos2 > 0) then
          begin
            LString := Copy((E as EDBEngineError).Errors[1].message,Pos2+4,64);
            Pos1 := Pos(')',LString);
            LString := Copy(LString,1,Pos1-1);
            E.Message := 'Delete not succeeded.'#13#10+'Record still used in '+LString+' table';
          end
          else
            E.Message := 'Delete not succeeded.'#13#10'First delete related rows';
          raise;
        end;
      end;
    end;
  end;
end;

//procedure TmmTable.InternalAddRecord(Buffer: Pointer; Append: Boolean);
procedure TmmTable.DoAfterInsert;
var
  i: integer;
begin
  inherited;
  for i := 0 to PKFields.Count-1 do
    if (FieldByName(PKFields.Strings[i]).Visible) then
    begin
      FieldByName(PKFields.Strings[i]).FocusControl;
      Break;
    end;
end;

procedure TmmTable.CreateLike(CreateVisiblePKFields: Boolean; PostRec: Boolean;
                         NewKeyvalues: Variant; var CreateFromKey:Variant);
var
  i: integer;
  LocalList: TList;
  AListItem: PLocalList;
  LFilterFields: String;
begin
//filter : copyfilter + LFilterFields(C_CREATOR,...) + Visible PK Fields
  LFilterFields := 'C_CREATOR;C_CREATE_DATE;C_MODIFIER;C_MODIFY_DATE;C_DELETER;C_DELETE_DATE;C_DELETE;C_ROWNR';
  if (CopyFilter <> '') then
    LFilterFields := LFilterFields + ';' + Uppercase(CopyFilter);
  if (not CreateVisiblePKFields) then  // remark : always copy invisible PK fields
    for i := 0 to PKFields.Count-1 do
      if (FieldByName(PKFields.Strings[i]).Visible) then
        LFilterFields := LFilterFields + ';' + Uppercase(PKFields.Strings[i]);
// make copy of current record
  LocalList := TList.Create;
  try
    for i := 0 to FieldCount-1 do
    begin
      if Pos(Uppercase(Fields[i].FieldName),LFilterFields) = 0 then
      begin
        New(AListItem);
        AListItem^.Name  := Fields[i].FieldName;
        AListItem^.Value := Fields[i].Value;
        LocalList.Add(AListItem);
      end;
    end;
//Save PK fields for possible 'createdependants'
// when more than one key segment -> VarArray
// when only one key segment -> Variant
    if PKFields.Count > 1 then
    begin
      CreateFromKey := VarArrayCreate([0,PKFields.Count-1], varVariant);
      for I := 0 to PKFields.Count-1 do
        CreateFromKey[i] := FieldByName(PKFields.Strings[i]).Value;
    end
    else
      if PKFields.Count = 1 then
        CreateFromKey := FieldByName(PKFields.Strings[0]).Value;
//create new record
    insert;
//assign values from copied record
    for i := 0 to LocalList.Count-1 do
    begin
      AListItem := LocalList.Items[i];
      FieldValues[AListItem^.Name] := AListItem^.Value;
    end;
//assign keyvalues
    if VarArrayDimCount(NewKeyValues) > 0 then
//createfromkey[0..2] : VarArrayLowBound = 0
//                    VarArrayHighBound = 2
      For i := VarArrayLowBound(NewKeyValues,1) to
             VarArrayHighBound(NewKeyValues,1) do
        Fields[i].Value := NewKeyValues[i];
  finally
    //Cleanup
    // Cleanup: must free the list items as well as the list
    for i := 0 to (LocalList.Count - 1) do
    begin
      AListItem := LocalList.Items[i];
      Dispose(AListItem);
    end;
    LocalList.Free;
  end;
// post new record, can only be done when NewKeyValues is known
  if PostRec then Post;
end;

procedure TmmTable.CreateDependantRows(CreateFromKey: Variant);
var
  Container: TComponent;
  XTable: TmmTable;
  LCreateDependants: String;
  SavePlace: TBookmark;
  NewKeyValues: Variant;
  KeyNames: String;
  pp,I: Integer;
  SavMasterSource: TDataSource;
  SavMasterFields: String;
  ClosXtbl: Boolean;
  LocCreateFromKey: Variant;
begin
// Save PKFields values
  NewKeyValues := VarArrayCreate([0,PKFields.Count-1], varVariant);
  for I := 0 to PKFields.Count-1 do
    NewKeyValues[i] := FieldByName(PKFields.Strings[i]).Value;
// CreateDependants : can contain name of detailtable when there is one
  LCreateDependants := CreateDependants;
  Container := Self.Owner;
//create records for each dependant table
  while LCreateDependants <> '' do
  begin
    PP := Pos(';',LCreateDependants);
    if PP > 0 then
    begin
      XTable := TmmTable(Container.FindComponent(Copy(LCreateDependants,1,PP-1)));
      LCreateDependants := Copy(LCreateDependants,Pos(';',LCreateDependants)+1,Length(LCreateDependants));
    end
    else
    begin
      XTable := TmmTable(Container.FindComponent(Copy(LCreateDependants,1,Length(LCreateDependants))));
      LCreateDependants := '';
    end;
//save mastersource,masterfields defined for detailtable
    SavMasterSource := XTable.MasterSource;
    SavMasterFields := XTable.MasterFields;
    //break link between detail and master table
    Xtable.MasterSource := nil;
    Xtable.MasterFields := '';
//Disable datadisplays to prevent flickering
  XTable.DisableControls;
//Open detailtable when necessary
    ClosXtbl := False;
    if not XTable.Active then
    begin
      ClosXtbl := True;
      XTable.Open;
    end;
  //Get PKField names from deptable in a string separated by ';'
//    XTable.First;
    try
      KeyNames := '';
      for I := 0 to PKFields.Count-1 do
        KeyNames := KeyNames + XTable.PKFields.Strings[i] + ';';
      if KeyNames <> '' then KeyNames := Copy(KeyNames,1,Length(KeyNames)-1); //submit last ';'
  // move to record
      XTable.Locate(KeyNames,CreateFromKey,[loPartialKey]);
  //Loop detail rows
      while (KeyEqual(CreateFromKey,XTable)) and (not XTable.EOF) do
      begin
        SavePlace := XTable.GetBookmark;
        try
          XTable.Createlike(True,True,NewKeyValues,LocCreateFromKey);
        except
          XTable.Cancel;
          Raise;
        end;
        if XTable.CreateDependants <> '' then
          XTable.CreateDependantRows(LocCreateFromKey);
        // Move back to the bookmark
        // this may not be the next record anymore
        // if something else is changing the dataset asynchronously
        XTable.GotoBookmark(SavePlace);
        XTable.FreeBookmark(SavePlace);
        XTable.Next;
      end;
    finally
        //restore mastersource,masterfields defined for detailtable
        Xtable.MasterSource := SavMasterSource;
        Xtable.MasterFields := SavMasterFields;
        XTable.EnableControls;
        if ClosXtbl then XTable.Close;
    end;
    XTable.Refresh;
// also create dependants of dependant tables...
  end;
end;

function TmmTable.KeyEqual(CreateFromKey: Variant; RefTable: TmmTable): Boolean;
var
  i: integer;
begin
  Result := True;
//  if VarArrayDimCount(CreateFromKey) > 1 then
  if VarArrayDimCount(CreateFromKey) > 0 then
  begin
//createfromkey[0..2] : VarArrayLowBound = 0
//                    VarArrayHighBound = 2
    For i := VarArrayLowBound(CreateFromKey,1) to
             VarArrayHighBound(CreateFromKey,1) do
      if CreateFromKey[i] <> RefTable.Fields[i].Value then
      begin
        Result := False;
        Exit;
      end;
  end
  else
    if CreateFromKey <> RefTable.Fields[0].Value then
      Result := False;
end;

procedure TmmTable.DisableEvents;
begin
  if Assigned(SavAfterCancel) then
    SavAfterCancel    := AfterCancel;
  if Assigned(SavAfterClose) then
    SavAfterClose     := AfterClose;
  if Assigned(SavAfterDelete) then
    SavAfterDelete    := AfterDelete;
  if Assigned(SavAfterEdit) then
    SavAfterEdit      := AfterEdit;
  if Assigned(SavAfterInsert) then
    SavAfterInsert    := AfterInsert;
  if Assigned(SavAfterOpen) then
    SavAfterOpen      := AfterOpen;
  if Assigned(SavAfterPost) then
    SavAfterPost      := AfterPost;
  if Assigned(SavAfterScroll) then
    SavAfterScroll    := AfterScroll;
  if Assigned(SavBeforeCancel) then
    SavBeforeCancel   := BeforeCancel;
  if Assigned(SavBeforeClose) then
    SavBeforeClose    := BeforeClose;
  if Assigned(SavBeforeDelete) then
    SavBeforeDelete   := BeforeDelete;
  if Assigned(SavBeforeEdit) then
    SavBeforeEdit     := BeforeEdit;
  if Assigned(SavBeforeInsert) then
    SavBeforeInsert   := BeforeInsert;
  if Assigned(SavBeforeOpen) then
    SavBeforeOpen     := BeforeOpen;
  if Assigned(SavBeforePost) then
    SavBeforePost     := BeforePost;
  if Assigned(SavBeforeScroll) then
    SavBeforeScroll   := BeforeScroll;
  if Assigned(SavOnCalcFields) then
    SavOnCalcFields   := OnCalcFields;
  if Assigned(SavOnDeleteError) then
    SavOnDeleteError  := OnDeleteError;
  if Assigned(SavOnEditError) then
    SavOnEditError    := OnEditError;
  if Assigned(SavOnFilterRecord) then
    SavOnFilterRecord := OnFilterRecord;
  if Assigned(SavOnNewRecord) then
    SavOnNewRecord    := OnNewRecord;
  if Assigned(SavOnPostError) then
    SavOnPostError    := OnPostError;
//  if Assigned(SavOnServerYield) then
//    SavOnServerYield  := OnServerYield;
  if Assigned(SavOnUpdateError) then
    SavOnUpdateError  := OnUpdateError;
  if Assigned(SavOnUpdateRecord) then
    SavOnUpdateRecord := OnUpdateRecord;
  AfterCancel    := nil;
  AfterClose     := nil;
  AfterDelete    := nil;
  AfterEdit      := nil;
  AfterInsert    := nil;
  AfterOpen      := nil;
  AfterPost      := nil;
  AfterScroll    := nil;
  BeforeCancel   := nil;
  BeforeClose    := nil;
  BeforeDelete   := nil;
  BeforeEdit     := nil;
  BeforeInsert   := nil;
  BeforeOpen     := nil;
  BeforePost     := nil;
  BeforeScroll   := nil;
  OnCalcFields   := nil;
  OnDeleteError  := nil;
  OnEditError    := nil;
  OnFilterRecord := nil;
  OnNewRecord    := nil;
  OnPostError    := nil;
//  OnServerYield  := nil;
  OnUpdateError  := nil;
  OnUpdateRecord := nil;
  FDisabledEvents := True;
end;

procedure TmmTable.EnableEvents;
begin
  AfterCancel    := SavAfterCancel;
  AfterClose     := SavAfterClose;
  AfterDelete    := SavAfterDelete;
  AfterEdit      := SavAfterEdit;
  AfterInsert    := SavAfterInsert;
  AfterOpen      := SavAfterOpen;
  AfterPost      := SavAfterPost;
  AfterScroll    := SavAfterScroll;
  BeforeCancel   := SavBeforeCancel;
  BeforeClose    := SavBeforeClose;
  BeforeDelete   := SavBeforeDelete;
  BeforeEdit     := SavBeforeEdit;
  BeforeInsert   := SavBeforeInsert;
  BeforeOpen     := SavBeforeOpen;
  BeforePost     := SavBeforePost;
  BeforeScroll   := SavBeforeScroll;
  OnCalcFields   := SavOnCalcFields;
  OnDeleteError  := SavOnDeleteError;
  OnEditError    := SavOnEditError;
  OnFilterRecord := SavOnFilterRecord;
  OnNewRecord    := SavOnNewRecord;
  OnPostError    := SavOnPostError;
//  OnServerYield  := SavOnServerYield;
  OnUpdateError  := SavOnUpdateError;
  OnUpdateRecord := SavOnUpdateRecord;
  DisabledEvents := False;
end;

procedure TmmTable.ClearBuffer(Buffer:TList);
var
  i: integer;
  AListItem: PLocalList;
begin
  if (Buffer = nil) then exit;
  for i := 0 to (Buffer.Count - 1) do
  begin
    AListItem := Buffer.Items[i];
//    VarClear(AListItem^.Value);
    AListItem^.Value := Null;
    Buffer.Items[i] := AListItem;
  end;
end;

procedure TmmTable.DestroyBuffer(var Buffer:TList);
var
  i: integer;
  AListItem: PLocalList;
begin
  if (Buffer = nil) then exit;
  for i := 0 to (Buffer.Count - 1) do
  begin
    AListItem := Buffer.Items[i];
    Dispose(AListItem);
  end;
  Buffer.Free;
  Buffer := nil;
end;

Procedure TmmTable.AddOneListItem(LocField:TField; Buffer:TList; CheckFilter:Boolean);
var
  AListItem: PLocalList;
  AddRecord: Boolean;
begin
  if FFilterFields = '' then  GetInternalCopyFilter;
  AddRecord := False;
  if CheckFilter then
  begin
    if Pos(Uppercase(LocField.FieldName),FFilterFields) = 0 then
    begin
      AddRecord := True;
    end;
  end
  else
    if Pos(Uppercase(LocField.FieldName),FMinFilterFields) = 0 then
    begin
      AddRecord := True;
    end;
  if AddRecord then
  begin
    New(AListItem);
    AListItem^.Name  := LocField.FieldName;
    AListItem^.Value := LocField.Value;
//    AListItem^.DataT := GetDataType(LocField.DataType);
    Buffer.Add(AListItem);
  end;
end;

function TmmTable.GetFilterString(Buffer:TList) : String;
var
  AListItem: PLocalList;
  i: integer;
begin
  inherited;
  Result := '';
  for i:=0 to Buffer.Count-1 do
  begin
    AListItem := Buffer.Items[i];
    if not(VarIsEmpty(AListItem^.Value) or VarisNull(AListItem^.Value)) then
    begin
      if pos(uppercase(AListItem^.Name),Result) = 0 then
      begin
        case VarType(AListItem^.Value) of
          VarDouble:
          begin
            Result := Result + ' AND ' + AListItem^.Name + '=' + FloatToStr(AListItem^.Value);
          end;
          VarDate:
          begin
            Result := Result + ' AND ' + AListItem^.Name + '=' + Char(39) + DateTimeToStr(AListItem^.Value) + Char(39);
          end;
          VarString:
          begin
            if AListItem^.Value <> '' then
              Result := Result + ' AND ' + AListItem^.Name + '=' + Char(39) + AListItem^.Value + Char(39);
          end;
        end;
      end;
    end;
  end;
  Result := copy(Result,6,length(Result)) ;  // for first AND
end;

procedure TmmTable.GetInternalCopyFilter;
var
  i: integer;
  LocField: TField;
begin
  if not FFilterSaved then
  begin
    FSaveFilter  := Filter;
    FFilterSaved := True;
  end;
//filter : copyfilter + FFilterFields(C_CREATOR,...) + Visible PK Fields + Readonly fields
  FMinFilterFields := 'C_CREATOR;C_CREATE_DATE;C_MODIFIER;C_MODIFY_DATE;C_DELETER;C_DELETE_DATE;C_DELETE;C_ROWNR';
//filter + Readonly fields
//  FMinFilterFields := '';
  for i := 0 to FieldCount-1 do
  begin
    if (Pos(Uppercase(Fields[i].FieldName),FMinFilterFields) = 0) then
    begin
//      if (Fields[i].ReadOnly) and {lmd 14/10 : bij paste werd de pkfield mee gewijzigd     not((Fields[i].Tag and 1) = 1)) or
         (Fields[i].FieldKind in [fkLookup]) then               //no lookup fields
        FMinFilterFields := FMinFilterFields + ';' + Uppercase(Fields[i].FieldName);
    end;
  end;
//  FMinFilterFields := Copy(FMinFilterFields,2,Length(FMinFilterFields));
//filter + copyFilter
  if (CopyFilter <> '') then
    FFilterFields := FFilterFields + ';' + Uppercase(CopyFilter);
//filter + VisiblePKFields  // remark : always copy invisible PK fields
  for i := 0 to PKFields.Count-1 do
  begin
    LocField := FieldByName(PKFields.Strings[i]);
    if not LocField.Visible then
      FFilterFields := FFilterFields + ';' + Uppercase(PKFields.Strings[i]);
  end;
  FFilterFields := FFilterFields + ';' + FMinFilterFields;
end;

procedure TmmTable.CopyToBuffer(Buffer:TList; CheckFilter:Boolean);
var
  i: integer;
begin
// make copy of current record
  for i := 0 to FieldCount-1 do
  begin
    AddOneListItem(Fields[i],Buffer,CheckFilter);
  end;
end;

procedure TmmTable.PasteFromBuffer(Buffer:TList);
var
  i: integer;
  AListItem: PLocalList;
begin
//assign values from copied record
  for i := 0 to Buffer.Count-1 do
  begin
    AListItem := Buffer.Items[i];
    FieldValues[AListItem^.Name] := AListItem^.Value;
  end;
end;

procedure TmmTable.EnableFilterBy(EnableIt:Boolean);
var
  LocFilter: String;
begin
  if (EnableIt) then
  begin
    LocFilter := GetFilterString(FilterByBuffer);
    if (FSaveFilter <> '') then
      Filter   := Char(39) + FSaveFilter + Char(39) + ' AND ' + LocFilter
    else
      Filter   := LocFilter;
    Filtered := True;
  end
  else
  begin
    if FSaveFilter <> '' then
    begin
      Filter   := FSaveFilter;
      Filtered := True;
    end
    else
      Filtered := False;
  end;
end;

procedure TmmTable.CopyAction;
begin
//  inherited;
  //copy
  //Cleanup
  // Cleanup: must free the list items as well as the list
  if not(CopyPasteBuffer = nil) then
    ClearBuffer(CopyPasteBuffer)
  else
    CopyPasteBuffer := TList.Create;
  CopyToBuffer(CopyPasteBuffer,True);
end;

procedure TmmTable.PasteAction;
begin
  inherited;
  //paste
  if not(CopyPasteBuffer = nil) then
  begin
    if not (State in [dsEdit,dsInsert]) then Edit ;
    PasteFromBuffer(CopyPasteBuffer);
  end;
end;

procedure TmmTable.FilterByFormAction;
begin
  DisableEvents;
  try
    Insert;
    if not (FilterByBuffer = nil) then PasteFromBuffer(FilterByBuffer);
  except
    EnableEvents;
  end;
end;

procedure TmmTable.ClearAction;
begin
  Cancel;
  Insert;
  if not(FilterByBuffer = nil) then ClearBuffer(FilterByBuffer);
end;

function TmmTable.ApplyFilterAction:Boolean;
var
  LocFilter: String;
begin
  if not(FilterByBuffer = nil) then
    ClearBuffer(FilterByBuffer)
  else
    FilterByBuffer := TList.Create;
  CopyToBuffer(FilterByBuffer,False);
  LocFilter := GetFilterString(FilterByBuffer);
  Cancel;
  Result := False;
  if LocFilter <> '' then
  begin
    EnableFilterBy(True);
    Result := True;
  end
  else
    EnableFilterBy(False);
  EnableEvents;
end;

function TmmTable.FilterBySelAction(LocField:TField): Boolean;
var
  AListItem: PLocalList;
  i: integer;
  LocFilter: String;
  LocFound: Boolean;
begin
  if (FilterByBuffer = nil) then
  begin
    FilterByBuffer := TList.Create;
    AddOneListItem(LocField,FilterByBuffer,False);
  end
  else
  begin
    LocFound := False;
    for i:=0 to FilterByBuffer.Count-1 do
    begin
      AListItem := FilterByBuffer.Items[i];
      if (AListItem^.Name = LocField.FieldName) then
      begin
        LocFound := True;
        //delete and insert is not necessary => modify
        AListItem^.Name  := LocField.FieldName;
        AListItem^.Value := LocField.Value;
//        AListItem^.DataT := GetDataType(LocField.DataType);
      end;
    end;
    if not LocFound then
      AddOneListItem(LocField,FilterByBuffer,False);
  end;
  LocFilter := GetFilterString(FilterByBuffer);
  Result := False;
  if LocFilter <> '' then
  begin
    EnableFilterBy(True);
    Result := True;
  end
  else
    EnableFilterBy(False);
end;

function TmmTable.GetCopyBufValid: Boolean;
begin
  Result := (CopyPasteBuffer <> nil);
end;
{}

end.
