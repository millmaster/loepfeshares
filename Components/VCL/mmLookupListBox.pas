(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmLookupListBox.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
|=========================================================================================*)
unit mmLookupListBox;

interface

uses
  AutoLabelClass, Classes, StdCtrls, DB, DBTables, mmMemTable;

type
  TmmLookupListBox = class(TListBox)
  private
    fAutoLabel: TAutoLabel;
    FLookupSource : TDataSource;  { Datasource from which to select }
    FLookupField : string;        { internal field used for further processing }
    FLookupDisplay : string;      { field displayed during selection }
    FDisplayValue : string;       { selected value of LookupDisplay }
    FValue : Variant;             { corresponding value for LookupField }
    FFilter: String;
    FFiltered: Boolean;
    function GetLookupSource : TDataSource;
    procedure SetLookupSource( Value : TDataSource);
    procedure SetLookupFieldName (const Value : string);
    procedure SetLookupDisplayName (const Value : string);
    procedure SetVisible(Value: Boolean); virtual;
    function GetVisible: Boolean;
  protected
    Procedure Loaded; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
//    Procedure Change; override;
  public
    property DisplayValue : string read FDisplayValue;
    property Value : Variant read FValue;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property LookupSource : TDataSource read GetLookupSource write SetLookupSource;
    property LookupField : string read FLookupField write SetLookupFieldName;
    property LookupDisplay : string read FLookupDisplay write SetLookupDisplayName;
    property Filter: String read FFilter write FFilter;
    property Filtered: Boolean read FFiltered write FFiltered;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, sysutils;

//procedure Register;
//begin
//  RegisterComponents('Standard', [TmmLookupListBox]);
//end;

function TmmLookupListBox.GetLookupSource : TDataSource;
begin
  Result := FLookupSource;
end;

procedure TmmLookupListBox.SetLookupSource (Value : TDataSource);
var
  SourceActive : Boolean;
  LFilter: String;
  LFiltered: Boolean;
begin
  FLookupSource := Value;
  if Value <> nil then Value.FreeNotification(Self);

// if already filtered -> copy filter in local var.
// set new filter
  if (not (csDesigning in ComponentState)) then
  begin
    LFilter   := '';
    LFiltered := false;
    if (FLookupSource <> nil) then
      if (FLookupSource.Dataset is TTable) then
        with FLookupSource.Dataset as TTable do
        begin
          if (Filter <> '') then
          begin
            LFilter   := Filter;
            LFiltered := Filtered;
          end;
          Filter   := FFilter;
          Filtered := FFiltered;
        end;
//   Dropdown list
    Items.Clear;
    if (FLookupSource <> nil) then
      if (FLookupSource.Dataset is TTable) or
         (FLookupSource.Dataset is TmmMemTable) then
        with FLookupSource.Dataset do
          begin
            SourceActive := Active;
            if Active then First else Open;
            Items.BeginUpdate;
            while not EOF do
              begin
                Items.Add( FieldByName(FLookupDisplay).AsString);
                Next;
              end;
            Items.EndUpdate;
            if not SourceActive then Close
          end
      else if (FLookupSource.Dataset is TQuery) then
        with FLookupSource.Dataset as TQuery do
          begin
            SourceActive := Active;
            if Active then First else Open;
            while not EOF do
              begin
                Items.Add( FieldByName(FLookupDisplay).AsString);
                Next;
              end;
            if not SourceActive then Close;
          end;
//   restore filter from local var.
    if (FLookupSource <> nil) then
      if (FLookupSource.Dataset Is TTable) then
        with FLookupSource.Dataset as TTable do
        begin
          Filter   := LFilter;
          Filtered := LFiltered;
        end;
  end;

end;

procedure TmmLookupListBox.SetLookupFieldName (const Value : string);
begin
  FLookupField := Value;
end;

procedure TmmLookupListBox.SetLookupDisplayName (const Value : string);
begin
  FLookupDisplay := Value;
end;

procedure TmmLookupListBox.Loaded;
{var
  SourceActive : Boolean;
  LFilter: String;
  LFiltered: Boolean;
}begin
// if already filtered -> copy filter in local var.
// set new filter
{  if (not (csDesigning in ComponentState)) then
  begin
    LFilter   := '';
    LFiltered := false;
    if (FLookupSource <> nil) then
      if (FLookupSource.Dataset Is TTable) then
        with FLookupSource.Dataset as TTable do
        begin
          if (Filter <> '') then
          begin
            LFilter   := Filter;
            LFiltered := Filtered;
          end;
          Filter   := FFilter;
          Filtered := FFiltered;
        end;
//   Dropdown list
    Items.Clear;
    if (FLookupSource <> nil) then
      if (FLookupSource.Dataset IS TTable) then
        with FLookupSource.Dataset as TTable do
          begin
            SourceActive := Active;
            if Active then First else Open;
            Items.BeginUpdate;
            while not EOF do
              begin
                Items.Add( FieldByName(FLookupDisplay).AsString);
                Next;
              end;
            Items.EndUpdate;
            if not SourceActive then Close
          end
      else if (FLookupSource.Dataset IS TQuery) then
        with FLookupSource.Dataset as TQuery do
          begin
            SourceActive := Active;
            if Active then First else Open;
            while not EOF do
              begin
                Items.Add( FieldByName(FLookupDisplay).AsString);
                Next;
              end;
            if not SourceActive then Close;
          end;
 }   Inherited;
{//   restore filter from local var.
    if (FLookupSource <> nil) then
      if (FLookupSource.Dataset Is TTable) then
        with FLookupSource.Dataset as TTable do
        begin
          Filter   := LFilter;
          Filtered := LFiltered;
        end;
  end;
}
end;

{procedure TmmLookupListBox.Change ;
var
  SourceActive : Boolean;
begin
  if (FLookupSource <> nil) then
    if (FLookupSource.Dataset IS TTable) then
      with FLookupSource.Dataset as TTable do
        begin
          SourceActive := Active;
          if Active then First else Open;
          while (not EOF) and
                (FieldByName(FLookupDisplay).AsString<>Items[Itemindex]) do
              Next;
          if not EOF then
            case FieldByName(FLookupField).DataType of
              ftString : FValue := FieldByName(FLookupField).AsString;
              ftSmallint, ftInteger, ftWord:
                         FValue := FieldByName(FLookupField).AsInteger;
              ftBoolean: FValue := FieldByName(FLookupField).AsBoolean;
              ftFloat  : FValue := FieldByName(FLookupField).AsFloat;
              ftDate, ftTime, ftDateTime :
                         FValue := FieldByName(FLookupField).AsDateTime;
            end;
          if not SourceActive then Close;
        end
    else if (FLookupSource.Dataset IS TQuery) then
      with FLookupSource.Dataset as TQuery do
        begin
          SourceActive := Active;
          if Active then First else Open;
          while (not EOF) and
                (FieldByName(FLookupDisplay).AsString<>Items[Itemindex]) do
              Next;
          if not EOF then
            case FieldByName(FLookupField).DataType of
              ftString : FValue := FieldByName(FLookupField).AsString;
              ftSmallint, ftInteger, ftWord:
                         FValue := FieldByName(FLookupField).AsInteger;
              ftBoolean: FValue := FieldByName(FLookupField).AsBoolean;
              ftFloat  : FValue := FieldByName(FLookupField).AsFloat;
              ftDate, ftTime, ftDateTime :
                         FValue := FieldByName(FLookupField).AsDateTime;
            end;
          if not SourceActive then Close;
        end;
  Inherited;
end;
 }
constructor TmmLookupListBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAutoLabel := TAutoLabel.Create(Self);
end;
//------------------------------------------------------------------------------
destructor TmmLookupListBox.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TmmLookupListBox.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmLookupListBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmLookupListBox.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmLookupListBox.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TmmLookupListBox.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

end.
