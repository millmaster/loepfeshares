(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmPageControl.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmPageControl;

interface

uses
  ComCtrls, Classes;

type
  TmmPageControl = class(TPageControl)
  private
    { Private declarations }
  protected
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//procedure Register;
//begin
//  RegisterComponents('Win32', [TmmPageControl]);
//end;

constructor TmmPageControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TmmPageControl.Loaded;
var
  I: integer;
begin
  // make sure to always show the first page at startup
  for I := 0 to PageCount - 1 do
    if (Pages[I].PageIndex = 0) then ActivePage := Pages[I];
  inherited Loaded;
end;

end.
