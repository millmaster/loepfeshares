(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDatabase.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  1.00  Wss | Initial Release
| 15.08.2000  1.00  Wss | DoDisconnect overriden to catch exception in DbiCloseDatabase
|=========================================================================================*)
unit mmDatabase;

interface

uses
  classes, dbtables, DB;

type
  TmmDatabase = class(TDatabase)
  private
    FSchemaName: String;
    procedure SetSchemaName(Value: String);
  protected
    procedure DoDisconnect; override;
  public
  published
    property SchemaName: String read FSchemaName write SetSchemaName;
  end;


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
//------------------------------------------------------------------------------
procedure TmmDatabase.DoDisconnect;
begin
  try
    inherited DoDisconnect;
  except
    // if database is used in packages in DbiCloseDatabase an exception occurs!!
    Handle := Nil;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmDatabase.SetSchemaName(Value: String);
begin
  if (FSchemaName <> Value) then
  begin
    if (not Connected) then FSchemaName := Value;
  end;
end;
//------------------------------------------------------------------------------
end.


