(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDBLookupComboBox.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
  Funktion wurde so Erweitert, dass der Zustand Edit/Read Only visualisiert 
  wird.
  
  ShowMode = smNormal
    Komponente verhaelt sich gleich wie TDBLookupComboBox
  
  ShowMode = smExtended
  Komponente ist im erweiterten Modus. In diesem Mode ist das inherited Enabled 
  immer auf True, da die Darstellung bei False "geisterhaft" erscheint.
  
  Enabled = False
    Die Farbe wird auf clMMReadOnlyColor gesetzt und darunter ReadOnly = True
  Enabled = True
      Die Farbe wird auf die Originalfarbe gesetzt und darunter ReadOnly = False
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 10.04.2001  1.00  Wss | Show component state Edit/ReadOnly implemented
| 19.12.2001  1.03  Wss | Extended ShowMode changed to use ReadOnly state
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
|=========================================================================================*)
unit mmDBLookupComboBox;

interface

uses
  AutoLabelClass, Classes, Controls, DBCtrls, Graphics, LoepfeGlobal;

type
  {:----------------------------------------------------------------------------
   }
  TmmDBLookupComboBox = class (TDBLookupComboBox)
  private
    fAutoLabel: TAutoLabel;
    fColor: TColor;
    fReadOnlyColor: TColor;
    fShowMode: TShowMode;
    function GetColor: TColor;
    function GetVisible: Boolean;
    procedure SetColor(Value: TColor);
    procedure SetReadOnlyColor(Value: TColor);
    procedure SetShowMode(Value: TShowMode);
    procedure SetVisible(Value: Boolean); virtual;
    procedure UpdateProperties;
    function GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y:
            Integer); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
            override;
    procedure SetEnabled(Value: Boolean); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Color: TColor read GetColor write SetColor;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property ReadOnlyColor: TColor read fReadOnlyColor write SetReadOnlyColor;
    property ShowMode: TShowMode read fShowMode write SetShowMode;
    property Visible: Boolean read GetVisible write SetVisible;
  end;
  
//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  SysUtils;
  
procedure Register;
begin
end;

{:------------------------------------------------------------------------------
 TmmDBLookupComboBox}
{:-----------------------------------------------------------------------------}
constructor TmmDBLookupComboBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAutoLabel     := TAutoLabel.Create(Self);
  fColor         := inherited Color;
  fReadOnlyColor := clMMReadOnlyColor;
  fShowMode      := smNormal;
end;

{:-----------------------------------------------------------------------------}
destructor TmmDBLookupComboBox.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;

{:-----------------------------------------------------------------------------}
function TmmDBLookupComboBox.GetColor: TColor;
begin
  Result := fColor;
end;

{:-----------------------------------------------------------------------------}
function TmmDBLookupComboBox.GetReadOnly: Boolean;
begin
  Result := inherited ReadOnly;
end;

{:-----------------------------------------------------------------------------}
function TmmDBLookupComboBox.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if not Enabled and (fShowMode = smExtended) then
    Abort
  else
    Inherited;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.KeyPress(var Key: Char);
begin
  if not Enabled and (fShowMode = smExtended) then
    Abort
  else
    inherited;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.Loaded;
begin
  inherited Loaded;
  UpdateProperties;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.MouseDown(Button: TMouseButton; Shift: 
        TShiftState; X, Y: Integer);
begin
  if not Enabled and (fShowMode = smExtended) then
    Abort
  else
    Inherited;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.Notification(AComponent: TComponent; Operation:
        TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.SetColor(Value: TColor);
begin
  if fColor <> Value then
  begin
    fColor := Value;
    UpdateProperties;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  UpdateProperties;
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.SetReadOnly(const Value: Boolean);
begin
  inherited ReadOnly := Value;
  UpdateProperties;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.SetReadOnlyColor(Value: TColor);
begin
  if fReadOnlyColor <> Value then
  begin
    fReadOnlyColor := Value;
    UpdateProperties;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.SetShowMode(Value: TShowMode);
begin
  if fShowMode <> Value then
  begin
    fShowMode := Value;
    UpdateProperties;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBLookupComboBox.UpdateProperties;
begin
  if (ComponentState - [csFreeNotification]) = [] then begin
    if (fShowMode = smExtended) and ReadOnly then
      inherited Color := fReadOnlyColor
    else
      inherited Color := fColor;
  end;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
end.
