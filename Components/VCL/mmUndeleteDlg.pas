(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmUndeleteDlg.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmUndeleteDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DlgUndelete, mmDataSource, DB, DBTAbles;

type
  TmmUndeleteDlg = class(TComponent)
  private
    FDataSource: TDataSource;
    { Private declarations }
  protected
    { Protected declarations }
  public
    function Execute: Boolean;
    { Public declarations }
  published
    property DataSource: TDataSource read FDataSource write FDataSource;
    { Published declarations }
  end;

procedure Register;

implementation
uses
  mmMBCS;

procedure Register;
begin
  RegisterComponents('Samples', [TmmUndeleteDlg]);
end;

function TmmUndeleteDlg.Execute: Boolean;
begin
  DlgUndelete_Data := TDlgUndelete_Data.Create(Application);
  try
    TDataSource(DlgUndelete_Data.ActiveSource) := DataSource;
    DlgUndelete_Data.PrepareData;
    Result := (DlgUndelete_Data.ShowModal = IDOK);
  finally
    DlgUndelete_Data.Free;
  end;
end;


end.
