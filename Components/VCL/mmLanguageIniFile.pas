(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmLanguageIniFile.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmLanguageIniFile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IniFiles, TypInfo, DBGrids, ComCtrls;

type
  TmmLanguageIniFile = class(TComponent)
  private
    { Private declarations }
  protected
    FFormName: String;
    FIniFilename: String;
    FPath: String;
  public
    constructor Create(AOwner: TComponent); override;
    procedure SaveFormStrings;
    procedure TranslateForm;
  published
    property Language: String Read FFormName Write FFormName;
    property IniFileName: String Read FIniFileName Write FIniFilename;
    property Path: String Read FPath Write FPath;
  end;

  TSpeedyIniFile = class(TObject)
  private
    Sections: TStringList;
  public
    INIFileName: String;
    constructor Create;
    destructor Destroy; override;
    procedure ReadIniFile(Name: String);
    function ReadString(Section, Ident, Default: String): String;
  end;

  procedure SetStringProperty (Component: TComponent; const PropName: String; Value: String);
  function  GetStringProperty (Component: TComponent; const PropName: String): string;
  function  GetPointerProperty(Component: TComponent; const PropName: String): Pointer;

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure Register;
begin
  RegisterComponents('Samples', [TmmLanguageIniFile]);
end;

constructor TmmLanguageIniFile.Create(AOwner: TComponent);
begin
  Inherited Create(AOwner);
  FFormName    := '';
  FIniFileName := '';
  FPath        := '';
end;

procedure TmmLanguageIniFile.SaveFormStrings;
var
  TI: TIniFile;
  S: String;
  I,J: Integer;
  C: TComponent;
  P: Pointer;
//  Q: Pointer;
//  D: TComponent;
begin
  If FIniFileName='' Then
    begin
      Raise Exception.Create('Property "IniFileName" not set!');
    end;
  If FFormName = '' Then
    begin
      Raise Exception.Create('Property "Language" not set!');
    end;
  TI:=TIniFile.Create(FPath+FIniFileName);
  {--- form caption ---}
  S := GetStringProperty(TForm(Owner),'Caption');
  If S<>'' Then TI.WriteString(FFormName,TForm(Owner).Name+'.Caption',S);
  For I:=0 to Owner.ComponentCount-1 Do
    begin
      C:=Owner.Components[I];
//      If (C.Name<>'') and (C.Tag<>9999) and (C is TControl) then
      If (C.Name<>'') and (C.Tag<>9999) then
      begin
        {--- Caption ---}
        S:=GetStringProperty(C,'Caption');
        If S<>'' Then TI.WriteString(FFormName,C.Name+'.Caption',S);
        {--- Hint ---}
        S:=GetStringProperty(C,'Hint');
        If S<>'' Then TI.WriteString(FFormName,C.Name+'.Hint',S);
        {--- Text ---}
        S:=GetStringProperty(C,'Text');
        If S<>'' Then TI.WriteString(FFormName,C.Name+'.Text',S);
        {--- Items ---}
        P:=GetPointerProperty(C,'Items');
        If (P<>NIL) and (TPersistent(P) is TStrings) and (TStrings(P).Count>0) Then
          begin
            For J:=0 to TStrings(P).Count-1 Do
              TI.WriteString(FFormName,C.Name+'.Item'+IntToStr(J)+'.Text',TStrings(P)[J]);
          end;
        {--- Hintss ---}
        P:=GetPointerProperty(C,'Hints');
        If (P<>NIL) and (TPersistent(P) is TStrings) and (TStrings(P).Count>0) Then
          begin
            For J:=0 to TStrings(P).Count-1 Do
              TI.WriteString(FFormName,C.Name+'.Hint'+IntToStr(J)+'.Text',TStrings(P)[J]);
          end;
        {--- Columns ---}
        P:=GetPointerProperty(C,'Columns');
        If (P<>NIL) and (TPersistent(P) is TDBGridColumns) and (TDBGridColumns(P).Count>0) Then
          begin
            For J:=0 to TDBGridColumns(P).Count-1 Do
              TI.WriteString(FFormName,C.Name+'.'+TDBGridColumns(P)[J].FieldName+'.Caption',TDBGridColumns(P)[J].Title.Caption);
          end
        else If (P<>NIL) and (TPersistent(P) is TListColumns) and (TListColumns(P).Count>0) Then
          begin
            For J:=0 to TListColumns(P).Count-1 Do
              TI.WriteString(FFormName,C.Name+'.Columns['+IntToStr(J)+'].Caption',TListColumns(P)[J].Caption);
          end;
        {--- Lines ---}
        P:=GetPointerProperty(C,'Lines');
        If (P<>NIL) and (TPersistent(P) is TStrings) and (TStrings(P).Count>0) Then
          begin
            For J:=0 to TStrings(P).Count-1 Do
              TI.WriteString(FFormName,C.Name+'.Line'+IntToStr(J)+'.Text',TStrings(P)[J]);
          end;
        {--- Sections[n].Text ---}
        P:=GetPointerProperty(C,'Sections');
        If (P<>NIL) and (TPersistent(P) is THeaderSections) and (THeaderSections(P).Count>0) Then
          begin
            For J:=0 to THeaderSections(P).Count-1 Do
              TI.WriteString(FFormName,C.Name+'.Sections'+IntToStr(J)+'.Text',THeaderSections(P)[J].Text);
          end;
//        if (C is TQRDesignTeeChart) then
//        begin
//          {--- ChartTitle ---}
//          S := TQRDesignTeeChart(C).Chart.Title.Text[0];
//          If S<>'' Then TI.WriteString(FFormName,C.Name+'.ChartTitle.Text',S);
//          {--- AxisTitles ---}
//          S := TQRDesignTeeChart(C).Chart.TopAxis.Title.Caption;
//          If S<>'' Then TI.WriteString(FFormName,C.Name+'.TopAxisTitle.Caption',S);
//          S := TQRDesignTeeChart(C).Chart.BottomAxis.Title.Caption;
//          If S<>'' Then TI.WriteString(FFormName,C.Name+'.BottomAxisTitle.Caption',S);
//          S := TQRDesignTeeChart(C).Chart.LeftAxis.Title.Caption;
//          If S<>'' Then TI.WriteString(FFormName,C.Name+'.LeftAxisTitle.Caption',S);
//          S := TQRDesignTeeChart(C).Chart.RightAxis.Title.Caption;
//          If S<>'' Then TI.WriteString(FFormName,C.Name+'.RightAxisTitle.Caption',S);
//          {--- SeriesTitles ---}
//          for J := 0 to TQRDesignTeeChart(C).Chart.SeriesList.Count-1 do
//          begin
//            S := TQRDesignTeeChart(C).Chart.Series[J].Title;
//            If S<>'' Then TI.WriteString(FFormName,C.Name+'.Series'+IntToStr(J)+'.Title',S);
//          end;
//        end;
//lmd-->
        end;
//      end
//      else If (C.Name<>'') and (C.Tag<>9999) then
//      begin
//        {--- Caption ---}
//        S:=GetStringProperty(C,'Caption');
//        If S<>'' Then TI.WriteString(FFormName,C.Name+'.Caption',S);
//        {--- Items ---}
//        P:=GetPointerProperty(C,'Items');
//        If (P<>NIL) and (TPersistent(P) is TStrings) and (TStrings(P).Count>0) Then
//          begin
//            For J:=0 to TStrings(P).Count-1 Do
//              TI.WriteString(FFormName,C.Name+'.Item'+IntToStr(J)+'.Text',TStrings(P)[J]);
//          end;
//      end;
//-->lmd
    end;
  TI.Free;
end;

procedure TmmLanguageIniFile.TranslateForm;
var
//  TI: TIniFile;
  TI: TSpeedyIniFile;
  S: String;
  I,J: Integer;
  C: TComponent;
  P: Pointer;
begin
  If FIniFileName='' Then
    begin
      Raise Exception.Create('Property "IniFileName" not set!');
    end;
  If FFormName = '' Then
    begin
      Raise Exception.Create('Property "Language" not set!');
    end;
//  TI:=TIniFile.Create(FIniFileName);
  TI:=TSpeedyIniFile.Create;
  TI.ReadIniFile(FPath+FIniFileName);
  S:=TI.ReadString(FFormName,TForm(Owner).Name+'.Caption','');
  If S<>'' Then SetStringProperty(TForm(Owner),'Caption',S);
  For I:=0 to Owner.ComponentCount-1 Do
    begin
      C:=Owner.Components[I];
//      If (C.Name<>'') and (C.Tag<>9999) and (C is TControl) then
      If (C.Name<>'') and (C.Tag<>9999) then
      begin
        {--- Caption ---}
        S:=TI.ReadString(FFormName,C.Name+'.Caption','');
        If S<>'' Then
          SetStringProperty(C,'Caption',S);
        {--- Hint ---}
        S:=TI.ReadString(FFormName,C.Name+'.Hint','');
        If S<>'' Then
          SetStringProperty(C,'Hint',S);
        {--- Text ---}
        S:=TI.ReadString(FFormName,C.Name+'.Text','');
        If S<>'' Then
          SetStringProperty(C,'Text',S);
        {--- Pages ---}
{        If (C is TTabbedNotebook) and (TTabbedNotebook(C).Pages.Count>0) Then
        begin
          For J:=0 to TTabbedNotebook(C).Pages.Count-1 Do
            begin
              S:=TI.ReadString(FFormName,C.Name+'.Item'+IntToStr(J+1),'');
              If S<>'' Then TTabbedNotebook(C).Pages[J]:=S;
            end;
        end; }
        {--- Items ---}
        P:=GetPointerProperty(C,'Items');
        If (P<>NIL) and (TPersistent(P) is TStrings) and (TStrings(P).Count>0) Then
          begin
            For J:=0 to TStrings(P).Count-1 Do
              begin
                S:=TI.ReadString(FFormName,C.Name+'.Item'+IntToStr(J)+'.Text','');
                If S<>'' Then TStrings(P)[J]:=S;
              end;
          end;
        {--- Columns ---}
        P:=GetPointerProperty(C,'Columns');
        If (P<>NIL) and (TPersistent(P) is TDBGridColumns) and (TDBGridColumns(P).Count>0) Then
          begin
            For J:=0 to TDBGridColumns(P).Count-1 Do
              begin
                S:=TI.ReadString(FFormName,C.Name+'.'+TDBGridColumns(P)[J].FieldName+'.Caption','');
                If S<>'' Then TDBGridColumns(P)[J].Title.Caption:=S;
              end;
          end
        else If (P<>NIL) and (TPersistent(P) is TListColumns) and (TListColumns(P).Count>0) Then
          begin
            For J:=0 to TListColumns(P).Count-1 Do
              begin
                S:=TI.ReadString(FFormName,C.Name+'.Columns['+IntToStr(J)+'].Caption','');
                If S<>'' Then TListColumns(P)[J].Caption:=S;
              end;
          end;

        {--- Hints ---}
        P:=GetPointerProperty(C,'Hints');
        If (P<>NIL) and (TPersistent(P) is TStrings) and (TStrings(P).Count>0) Then
          begin
            For J:=0 to TStrings(P).Count-1 Do
              begin
                S:=TI.ReadString(FFormName,C.Name+'.Hint'+IntToStr(J)+'.Text','');
                If S<>'' Then TStrings(P)[J]:=S;
              end;
          end;
        {--- Lines ---}
        P:=GetPointerProperty(C,'Lines');
        If (P<>NIL) and (TPersistent(P) is TStrings) and (TStrings(P).Count>0) Then
          begin
            For J:=0 to TStrings(P).Count-1 Do
              begin
                S:=TI.ReadString(FFormName,C.Name+'.Line'+IntToStr(J)+'.Text','');
                If S<>'' Then TStrings(P)[J]:=S;
              end;
          end;
        {--- Sections[n].Text ---}
        P:=GetPointerProperty(C,'Sections');
        If (P<>NIL) and (TPersistent(P) is THeaderSections) and (THeaderSections(P).Count>0) Then
          begin
            For J:=0 to THeaderSections(P).Count-1 Do
              begin
                S:=TI.ReadString(FFormName,C.Name+'.Sections'+IntToStr(J)+'.Text','');
                If S<>'' Then THeaderSections(P)[J].Text:=S;
              end;
          end;
        {--- RuntimeMessages ---}
{        P:=GetPointerProperty(C,'RuntimeMessages');
          If (P<>NIL) and (TPersistent(P) is TStrings) and (TStrings(P).Count>0) Then
            begin
              For J:=0 to TStrings(P).Count-1 Do
                begin
                  S:=TI.ReadString(FFormName,C.Name+'.Item'+IntToStr(J)+'.Text','');
                  If S<>'' Then TStrings(P)[J]:=S;
                end;
            end;  }
//          if (C is TQRDesignTeeChart) then
//          begin
//            {--- ChartTitle ---}
//            S := TI.ReadString(FFormName,C.Name+'.ChartTitle.Text','');
//            If S<>'' Then TQRDesignTeeChart(C).Chart.Title.Text[0] := S;
//            {--- AxisTitles ---}
//            S := TI.ReadString(FFormName,C.Name+'.TopAxisTitle.Caption','');
//            If S<>'' Then TQRDesignTeeChart(C).Chart.TopAxis.Title.Caption := S;
//            S := TI.ReadString(FFormName,C.Name+'.BottomAxisTitle.Caption','');
//            If S<>'' Then TQRDesignTeeChart(C).Chart.BottomAxis.Title.Caption := S;
//            S := TI.ReadString(FFormName,C.Name+'.LeftAxisTitle.Caption','');
//            If S<>'' Then TQRDesignTeeChart(C).Chart.LeftAxis.Title.Caption := S;
//            S := TI.ReadString(FFormName,C.Name+'.RightAxisTitle.Caption','');
//            If S<>'' Then TQRDesignTeeChart(C).Chart.RightAxis.Title.Caption := S;
//            {--- SeriesTitles ---}
//            for J := 0 to TQRDesignTeeChart(C).Chart.SeriesList.Count-1 do
//            begin
//            S := TI.ReadString(FFormName,C.Name+'.Series'+IntToStr(J)+'.Title','');
//              If S<>'' Then TQRDesignTeeChart(C).Chart.Series[J].Title := S;
//            end;
//          end;
      end;
    end;
  TI.Free;
end;

procedure SetStringProperty(Component: TComponent; const PropName: String; Value: string);
var
  ptrPropInfo: PPropInfo;
begin
  ptrPropInfo := GetPropInfo(Component.ClassInfo,PropName);
  if (ptrPropInfo <> nil) then
    SetStrProp(Component,ptrPropInfo,Value);
end;

function GetStringProperty(Component: TComponent; const PropName: String): string;
var
 ptrPropInfo: PPropInfo;
begin
 ptrPropInfo := GetPropInfo(Component.ClassInfo,PropName);
 if (ptrPropInfo = nil) then
   Result := ''
 else
   Result := GetStrProp(Component,ptrPropInfo);
end;

function GetPointerProperty(Component: TComponent; const PropName: String): Pointer;
var
 ptrPropInfo: PPropInfo;
begin
 ptrPropInfo := GetPropInfo(Component.ClassInfo,PropName);
 if (ptrPropInfo = nil) then
   Result := nil
 else
   Result := Pointer(GetOrdProp(Component,ptrPropInfo));
end;


constructor TSpeedyIniFile.Create;
begin
  Inherited Create;
  Sections:=TStringList.Create;
  Sections.Sorted:=True;
  INIFileName:='';
end;

destructor TSpeedyIniFile.Destroy;
var
  X,Y: Integer;
  L: TStringList;
begin
  For X:=0 to Sections.Count-1 Do
    begin
      L:=TStringList(Sections.Objects[X]);
      For Y:=0 to L.Count-1 Do DisposeStr(Pointer(L.Objects[Y]));
      L.Free;
    end;
  Sections.Free;
  Inherited Destroy;
end;

procedure TSpeedyIniFile.ReadIniFile(Name: String);
var
  T: TextFile;
  S,S2: String;
  ASection: TStringList;
  FM: Integer;
begin
  AssignFile(T,Name);
  FM:=FileMode;
  FileMode:=0;
  Try
    Reset(T);
  Except
    Raise Exception.Create('File '+Name+' not found!');
  End;
  INIFileName:=Name;
  FileMode:=FM;
  ASection:=NIL;
  While Not Eof(T) Do
    begin
      Readln(T,S);
      If S<>'' Then
        begin
          If S[1]='[' Then
            begin
              S:=UpperCase(Copy(S,2,Length(S)-2));
              ASection:=TStringList.Create;
              ASection.Sorted:=True;
              Sections.AddObject(S,ASection);
            end
          else
            begin
              S2:=Copy(S,Pos('=',S)+1,Length(S));
              ASection.AddObject( UpperCase(Copy(S,1,Pos('=',S)-1)),
                                  Pointer(NewStr(S2)));
            end;
        end;
    end;
  CloseFile(T);
end;

function TSpeedyIniFile.ReadString(Section, Ident, Default: String): String;
var
  I,J: Integer;
begin
  Result:=Default;
  I:=Sections.IndexOf(UpperCase(Section));
  If I=-1 Then Exit;
  J:=TStringList(Sections.Objects[I]).IndexOf(UpperCase(Ident));
  If J=-1 Then Exit;
  Result :=PString(TStringList(Sections.Objects[I]).Objects[J])^;
end;

end.
