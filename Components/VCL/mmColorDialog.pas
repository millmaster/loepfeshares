(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmColorDialog.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 18.02.1999  1.00  Wss | Deriving changed to Innoview's Dialogs
| 19.10.2000  1.00  Wss | Deriving changed to mmIvMlDlgs to work with Floor
|=========================================================================================*)
unit mmColorDialog;

interface

uses
  Classes, mmIvMlDlgs;

type
  TmmColorDialog = class(TIvColorDialog)
  private
  protected
  public
  published
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.

