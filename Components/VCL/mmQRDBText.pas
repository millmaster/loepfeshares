(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmQRDBText.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmQRDBText;

interface

uses
  Classes, Qrctrls, Quickrpt, StdCtrls, DB, DBctrls, mmCtrls,
  Graphics, Controls;

type
  TmmQRDBText = class(TQRDBText)
  private
    FAutoWidth: Boolean;
    FReportLabel: TQRLabel;
    FLabelControl: TmmReportLabelControl;
    procedure SetAutoWidth(Value: Boolean);
    procedure SetReportLabel(Value: TQRLabel);
    procedure SetLabelControl(Value: TmmReportLabelControl);
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure UpdateReportLabel;
  published
    property AutoWidth: Boolean read FAutoWidth write SetAutoWidth default False;
    property ReportLabel: TQRLabel read FReportLabel write SetReportLabel;
    property LabelControl: TmmReportLabelControl read FLabelControl write SetLabelControl;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  mmLib;

//procedure Register;
//begin
//  RegisterComponents('QReport', [TmmQRDBText]);
//end;

constructor TmmQRDBText.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FAutoWidth := False;
  FLabelControl := TmmReportLabelControl.Create;
end;

destructor TmmQRDBText.Destroy;
begin
  FLabelControl.Free;
  inherited Destroy;
end;

procedure TmmQRDBText.SetAutoWidth(Value: Boolean);
var
  LField: TField;
  LFont: TFont;
begin
  if FAutoWidth <> Value then
  begin
    if (Value) and (DataSet <> nil) and (csDesigning in ComponentState) then
    begin
      LField := Dataset.FieldByName(DataField);
      LFont  := Font;
      Width  := CalcFieldWidth(LField, LFont);
      if (FReportLabel <> nil) then
        FLabelControl.Update(TGraphicControl(Self),FReportLabel,DataSet,DataField);
      FAutoWidth := Value;
    end
    else if (not Value) then FAutoWidth := Value;
  end;
end;

procedure TmmQRDBText.UpdateReportLabel;
begin
  if (FReportLabel <> nil) then
    FLabelControl.Update(TGraphicControl(Self),FReportLabel,DataSet,DataField);
end;

procedure TmmQRDBText.SetReportLabel(Value: TQRLabel);
begin
  if FReportLabel <> Value then
  begin
    FReportLabel := Value;
  end;
end;

procedure TmmQRDBText.SetLabelControl(Value: TmmReportLabelControl);
begin
  FLabelControl.Assign(Value);
end;

end.
