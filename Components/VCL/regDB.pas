(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: regDB.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components for 'Data Access, Data Controls'
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit regDB;

interface

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  Classes, mmBatchMove, mmDatabase, mmDataSource, mmDBCheckBox,
  mmDBComboBox, mmDBCtrlGrid, mmDBGrid, mmDBEdit, mmDBImage,
  mmDBListBox, mmDBLookupComboBox, mmDBLookupListBox, mmDBMemo,
  mmDBNavigator, mmDBRadioGroup, mmDBText, mmQuery, mmSession,
  mmStoredProc, mmTable, mmUpdateSQL, mmDBRichEdit, mmProvider,
  mmClientDataSet, 
  mmMemTable, mmNestedTable, mmDBDateTimePicker, mmDBMonthCalendar;

procedure Register;
begin
  RegisterComponents('Data Access',[
                          TmmBatchMove,
                          TmmDatabase,
                          TmmDataSource,
                          TmmQuery,
                          TmmSession,
                          TmmStoredProc,
                          TmmNestedTable,
                          TmmTable,
                          TmmUpdateSQL,
                          TmmProvider,
                          TmmClientDataSet,
                          TmmMemTable
                                ]);
  RegisterComponents('Data Controls',[
                          TmmDBCheckBox,
                          TmmDBComboBox,
                          TmmDBCtrlGrid,
                          TmmDBGrid,
                          TmmDBEdit,
                          TmmDBImage,
                          TmmDBListBox,
                          TmmDBLookupComboBox,
                          TmmDBLookupListBox,
                          TmmDBMemo,
                          TmmDBNavigator,
                          TmmDBRadioGroup,
                          TmmDBText,
                          TmmDBRichEdit,
                          TmmDBDateTimePicker,
                          TmmDBMonthCalendar
                                ]);
end;

end.
