(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: mmTranslator.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 18.02.1999  1.00  Wss | Initial Release
| 16.03.2000  1.00  Wss | TranslateObject checks for TNTUser, TNTPrivilege
|                       | component for preventing translation
|=============================================================================*)
unit mmTranslator;

interface

uses
  Classes, IvEMulti, IvMulti;

type
  TmmTranslator = class(TIvExtendedTranslator)
  private
  protected
//    procedure TranslateObject(obj: TObject; restrictions: TIvRestrictions); override;
  public
  published
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{ TmmTranslator }

{
procedure TmmTranslator.TranslateObject(obj: TObject; restrictions: TIvRestrictions);
begin
  if (obj is TNTUSerMan) or ( obj is TNTPrivilege )then Exit;
  inherited;
end;
{}

end.
