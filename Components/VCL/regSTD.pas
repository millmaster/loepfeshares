(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: regSTD.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components in different pages
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit regSTD;

interface

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  Classes, dsgnintf, mmBevel, mmBitBtn, mmButton, mmCheckBox, mmColorDialog,
  mmComboBox, mmDdeClientConv, mmDdeClientItem, mmDdeServerConv,
  mmDdeServerItem, mmDrawGrid, mmEdit, mmFindDialog, mmFontDialog,
  mmGroupBox, mmHeaderControl, mmHotKey, mmImage, mmImageList,
  mmLabel, mmListBox, mmListView, mmMainMenu, mmMaskEdit,
  mmMediaPlayer, mmMemo, mmOleContainer, mmOpenDialog, mmPageControl,
  mmPaintBox, mmPanel, mmPopupMenu, mmPrintDialog, mmPrinterSetupDialog,
  mmProgressBar, mmRadioGroup, mmReplaceDialog, mmRichEdit,
  mmSaveDialog, mmScrollBar, mmScrollBox, mmShape, mmSpeedButton,
  mmStatusBar, mmStringGrid, mmTabControl, mmTimer, mmTrackBar,
  mmTreeView, mmUpDown, mmAnimate, mmCheckListBox, mmCoolBar,
  mmDateTimePicker, mmOpenPictureDialog, mmSavePictureDialog,
  mmSplitter, mmToolBar, mmRadioButton, mmStaticText, mmMonthCalendar,
  mmPageScroller, mmControlBar, mmActionList, mmApplicationEvents,
  advgrid, asgde;

procedure Register;
begin
  RegisterComponents('Additional',[
                          TmmApplicationEvents,
                          TmmBevel,
                          TmmBitBtn,
                          TmmDrawGrid,
                          TmmImage,
                          TmmMaskEdit,
                          TmmScrollBox,
                          TmmShape,
                          TmmSpeedButton,
                          TmmStringGrid,
                          TmmCheckListBox,
                          TmmSplitter,
                          TmmStaticText,
                          TmmControlBar
                                ]);
  RegisterComponentEditor(TmmStringGrid, TAdvStringGridEditor);

  RegisterComponents('Standard',[
                          TmmButton,
                          TmmCheckBox,
                          TmmComboBox,
                          TmmEdit,
                          TmmGroupBox,
                          TmmLabel,
                          TmmListBox,
                          TmmMainMenu,
                          TmmMemo,
                          TmmPanel,
                          TmmPopupMenu,
                          TmmRadioGroup,
                          TmmScrollBar,
                          TmmRadioButton,
                          TmmActionList
                                ]);
  RegisterComponents('Dialogs',[
                          TmmColorDialog,
                          TmmFindDialog,
                          TmmFontDialog,
                          TmmOpenDialog,
                          TmmPrintDialog,
                          TmmPrinterSetupDialog,
                          TmmReplaceDialog,
                          TmmSaveDialog,
                          TmmOpenPictureDialog,
                          TmmSavePictureDialog
                                ]);
  RegisterComponents('System',[
                          TmmDdeClientConv,
                          TmmDdeClientItem,
                          TmmDdeServerConv,
                          TmmDdeServerItem,
                          TmmMediaPlayer,
                          TmmOleContainer,
                          TmmPaintBox,
                          TmmTimer
                                ]);
  RegisterComponents('Win32',[
                          TmmHeaderControl,
                          TmmHotKey,
                          TmmImageList,
                          TmmListView,
                          TmmPageControl,
                          TmmProgressBar,
                          TmmRichEdit,
                          TmmStatusBar,
                          TmmTabControl,
                          TmmTrackBar,
                          TmmTreeView,
                          TmmUpDown,
                          TmmAnimate,
                          TmmCoolBar,
                          TmmDateTimePicker,
                          TmmMonthCalendar,
                          TmmToolBar,
                          TmmPageScroller
                                ]);
end;

end.
