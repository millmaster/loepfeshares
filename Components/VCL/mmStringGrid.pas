(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmStringGrid.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.08.1998  1.00  Wss | Before compare 2 string in StringCompare() test if they aren't empty
| 24.09.1998  1.01  Wss | call of OnDrawCell property in DrawCell()
| 02.12.1998  1.02  Wss | DeleteRow, DeleteColumn changed to override
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 31.08.1999  1.03  SDo | Change TmmStringGrid.Parentclass form TStingGrid to TAdvStringGrid
| 05.10.1999  1.03  Wss | Cleanup unneeded code from previous TSortGrid
| 21.09.2000  1.04  SDo | Neue Proc. SelectCol (selektiert eine ganze Spalte)
| 22.09.2000  1.04  SDo | Propery ColSelect
| 12.01.2001  1.04  Wss | Inheritance changed back to TStringGrid
| 05.11.2001  1.04  Wss | Some methodes made as public: DeleteColumn/Row, MoveColumn/Row
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
| 13.06.2007  1.05WssNue| In TmmStringGrid.DeleteRow xDoManualClear added.
|=========================================================================================*)
unit mmStringGrid;

interface

uses
  Classes, AutoLabelClass, Grids;

type
  TmmStringGrid = class(TStringGrid)
  private
    fAutoLabel: TAutoLabel;
  protected
    function GetVisible: Boolean;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
    procedure SetVisible(Value: Boolean); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DeleteColumn(ACol: Longint); override;
    procedure DeleteRow(ARow: Longint); override;
    procedure MoveColumn(FromIndex, ToIndex: Longint);
    procedure MoveRow(FromIndex, ToIndex: Longint);
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, sysutils;

{******************************************************************************
 ** Public Members for TmmStringGrid                                         **
 ******************************************************************************}
constructor TmmStringGrid.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fAutoLabel := TAutoLabel.Create(Self);
end;
//------------------------------------------------------------------------------
procedure TmmStringGrid.DeleteColumn(ACol: Integer);
begin
  inherited DeleteColumn(ACol);
end;
//------------------------------------------------------------------------------
procedure TmmStringGrid.DeleteRow(ARow: Integer);
var
  xDoManualClear: Boolean;
begin
  xDoManualClear := ((RowCount - FixedRows) = 1);
  inherited DeleteRow(ARow);
  if xDoManualClear then
    Rows[0].Text := '';
end;
//------------------------------------------------------------------------------
destructor TmmStringGrid.Destroy;
begin
  FreeAndNil(fAutoLabel);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TmmStringGrid.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmStringGrid.MoveColumn(FromIndex, ToIndex: Integer);
begin
  inherited MoveColumn(FromIndex, ToIndex);
end;
//------------------------------------------------------------------------------
procedure TmmStringGrid.MoveRow(FromIndex, ToIndex: Integer);
begin
  inherited MoveRow(FromIndex, ToIndex);
end;
//------------------------------------------------------------------------------
procedure TmmStringGrid.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmStringGrid.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmStringGrid.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TmmStringGrid.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;
//------------------------------------------------------------------------------

end.

