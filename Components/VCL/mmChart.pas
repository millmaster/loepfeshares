(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmChart.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 22.01.2001  0.00  Wss | added some functionality: clear series, rotate, move, tilt
| 14.03.2001  0.00  Wss | OnLegendClick property event added
| 03.05.2001  0.00  Wss | MoseMove: call standard inherited also if not View3D or Orthogonal for zoom
| 25.07.2001  0.00  Wss | Bug in MoveSeries fixed (out of bounds error)
| 19.07.2002  0.00  Wss | TOnLegendClickEvent parameter extended with Button, Shift state
| 24.07.2002  0.00  Wss | Property OnLegendClick not necessary anymore -> already exists OnClickLegend :-)
| 22.12.2003  0.00  Wss | MoveSeries liefert Index von neuer Position zur�ck
|=========================================================================================*)
unit mmChart;

interface

uses
  Classes, Chart, Controls, TeeProcs, TeEngine, mmSeries;

type
  TMouseMode = (mmNormal, mmRotate, mmMove);
  TMoveSeries = (msFirst, msBefore, msBack, msLast);

  TOnGetLegendCursorEvent = procedure (Sender: TObject; var aCursor: TCursor) of object;
  TmmChart = class (TChart)
  private
    fMouseMode: TMouseMode;
    fOnGetLegendCursor: TOnGetLegendCursorEvent;
    mLastX: Integer;
    mLastY: Integer;
    function GetSeriesEx(Index: Integer): IExtendSeries;
  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure Resize; override;
    property OnGetLegendCursor: TOnGetLegendCursorEvent read fOnGetLegendCursor write fOnGetLegendCursor;
  public
    constructor Create(aOwner: TComponent); override;
    function MoveSeries(aIndex: Integer; aMode: TMoveSeries): Integer;
    property SeriesEx[Index: Integer]: IExtendSeries read GetSeriesEx;
  published
    property MouseMode: TMouseMode read fMouseMode write fMouseMode;
  end;
  

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  SysUtils;
  
//:---------------------------------------------------------------------------
//:--- Class: TmmChart
//:---------------------------------------------------------------------------
constructor TmmChart.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fMouseMode         := mmNormal;
  fOnGetLegendCursor := Nil;
end;

//:---------------------------------------------------------------------------
function TmmChart.GetSeriesEx(Index: Integer): IExtendSeries;
begin
  Series[Index].GetInterface(IExtendSeries, Result);
end;

//:---------------------------------------------------------------------------
procedure TmmChart.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseDown(Button, Shift, X, Y);
  
  if Button = mbLeft then begin
    mLastX := X;
    mLastY := Y;
  end;
end;

//:---------------------------------------------------------------------------
procedure TmmChart.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  // call standard inherited also if not View3D or Orthogonal for zoom
  if (fMouseMode = mmNormal) or View3DOptions.Orthogonal or not View3D then begin
    inherited MouseMove(Shift, X, Y);
    // if Event property is set change cursor if over legend
    if Assigned(OnClickLegend) then begin
  //     xCursor := crDefault;
  //     if (Legend.Clicked(X, Y) <> -1) and Assigned(fOnGetLegendCursor) then
  //       fOnGetLegendCursor(Self, xCursor);
  //     Cursor := xCursor;
  
      if Legend.Clicked(X, Y) <> -1 then
        Cursor := crHandPoint
      else
        Cursor := crDefault;
    end;
  end else if (ssLeft in Shift) and not View3DOptions.Orthogonal then begin
    with View3DOptions do begin
      case fMouseMode of
        mmRotate: begin
            if ssCtrl in Shift then begin
              Tilt := Tilt  - (Y - mLastY);
            end else begin
              Elevation := Elevation - (Y - mLastY);
              Rotation  := Rotation  + (X - mLastX);
            end;
          end;
        mmMove: begin
            HorizOffset := HorizOffset + (X - mLastX);
            VertOffset  := VertOffset  + (Y - mLastY);
          end;
      else
      end; // case
    end; // with View3dOptions
    mLastX := X;
    mLastY := Y;
  end;
end;

//:---------------------------------------------------------------------------
function TmmChart.MoveSeries(aIndex: Integer; aMode: TMoveSeries): Integer;
var
  i: Integer;
  xStr: String;
  xSerie: TChartSeries;
begin
  Result := aIndex;
  
  xSerie := SeriesList.Series[aIndex];
  xStr := xSerie.Title;
  for i:=0 to SeriesCount-1 do begin
    if AnsiSameText(SeriesList.Series[i].Title, xStr) then begin
      if (aMode = msFirst) and (i < (SeriesCount-1)) then
        Result := SeriesCount-1
      else if (aMode = msBefore) and (i < (SeriesCount-1)) then
        Result := aIndex + 1
      else if (aMode = msBack) and (i > 0) then
        Result := aIndex - 1
      else if (aMode = msLast) and (i > 0) then
        Result := 0;
  
      SeriesList.Move(aIndex, Result);
  
      Break;
    end;
  end;
  Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TmmChart.Resize;
begin
  inherited;
end;

end.
