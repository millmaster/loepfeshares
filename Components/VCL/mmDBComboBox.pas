(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDBComboBox.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 29.05.1999  1.00  Wss | AutoLabel implemented
| 30.09.2003        Lok | AutoLabel.Free durch FreeAndNil(AutoLabel) erstzt (Probleme mit Notification)
|=========================================================================================*)
unit mmDBComboBox;

interface

uses
  AutoLabelClass, Classes, DB, DBCtrls, Graphics, StdCtrls, mmCtrls;

type
  TmmDBComboBox = class(TDBComboBox)
  private
  protected
  public
  published

  private
    fAutoLabel: TAutoLabel;
//    FAutoWidth: Boolean;
//    FFieldLabel: TLabel;
//    FLabelControl: TmmFieldLabelControl;
    function GetVisible: Boolean;
    procedure SetVisible(Value: Boolean); virtual;
//    procedure SetAutoWidth(Value: Boolean);
//    procedure SetFieldLabel(Value: TLabel);
//    procedure SetLabelControl(Value: TmmFieldLabelControl);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  { Public declarations }
  published
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
//    property AutoWidth: Boolean read FAutoWidth write SetAutoWidth default False;
//    property FieldLabel: TLabel read FFieldLabel write SetFieldLabel;
//    property LabelControl: TmmFieldLabelControl read FLabelControl write SetLabelControl;
    property Visible: Boolean read GetVisible write SetVisible;
    { Published declarations }
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  mmLib, sysutils;

//procedure Register;
//begin
//  RegisterComponents('Data Controls', [TmmDBComboBox]);
//end;

constructor TmmDBComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
//  FAutoWidth := False;
//  FLabelControl := TmmFieldLabelControl.Create;
  fAutoLabel := TAutoLabel.Create(Self);
end;

destructor TmmDBComboBox.Destroy;
begin
  FreeAndNil(fAutoLabel);
//  FLabelControl.Free;
  inherited Destroy;
end;

(*
procedure TmmDBComboBox.SetAutoWidth(Value: Boolean);
var
  LField: TField;
  LFont: TFont;
begin
  if FAutoWidth <> Value then
  begin
    if (Value) and (DataSource <> nil) and (csDesigning in ComponentState) then
    begin
      LField := DataSource.Dataset.FieldByName(DataField);
      LFont  := Font;
      Width  := CalcFieldWidth(LField, LFont) + 20;
      if ((DataSource.DataSet.FieldByName(DataField).Tag and 1) = 1) then
        Color := ClInfoBk;
      if (FFieldLabel <> nil) then
        FLabelControl.Update(Self,FFieldLabel,DataSource,DataField);
      FAutoWidth := Value;
    end
    else if (not Value) then FAutoWidth := Value;
  end;
end;

procedure TmmDBComboBox.SetFieldLabel(Value: TLabel);
begin
  if FFieldLabel <> Value then
  begin
    FFieldLabel := Value;
//    if (FFieldLabel <> nil) and (csDesigning in ComponentState) then
//      FLabelControl.Update(Self,FFieldLabel,DataSource,DataField);
//SendMessage(Handle, WM_PAINT, Message.DC, 0)
  end;
end;

procedure TmmDBComboBox.SetLabelControl(Value: TmmFieldLabelControl);
begin
  FLabelControl.Assign(Value);
//  if (FFieldLabel <> nil) and (csDesigning in ComponentState) then
//    FLabelControl.Update(Self,FFieldLabel,DataSource,DataField);
end;

{procedure TmmDBComboBox.SetAutoLabel(Value: Boolean);
var
  I,IDiff: integer;
  FLabelName: string;
  Height1,Height2,RDiff: real;
begin
  if FAutoLabel <> Value then
  begin
    if (Value) and (DataSource <> nil) then
    begin
      FLabelName := 'Lbl' + Copy(Name,4,99);
      for I:= 0 to Self.Owner.ComponentCount-1 do
      begin
        if (Self.Owner.Components[I].Name = FLabelName) then
        begin
          TLabel(Self.Owner.Components[I]).Caption :=
            DataSource.Dataset.FieldByName(DataField).DisplayLabel + ':';
          Height1 := Height;
          Height2 := TLabel(Self.Owner.Components[I]).Height;
          RDiff := (Height1 - Height2) / 2.0;
          IDiff := 4;
          TLabel(Self.Owner.Components[I]).Top := Top + IDiff;
          TLabel(Self.Owner.Components[I]).Left := Left - 6
            - TLabel(Self.Owner.Components[I]).Width;
        end;
      end;
      FAutoLabel := Value;
    end
    else if (not Value) then FAutoLabel := Value;
  end;
end;}
*)

//------------------------------------------------------------------------------
function TmmDBComboBox.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TmmDBComboBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if Assigned(fAutoLabel) then
    if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
      fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmDBComboBox.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TmmDBComboBox.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TmmDBComboBox.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;

end.
