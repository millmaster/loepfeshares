(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDBNavigator.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 25.06.1999  1.00  Wss | Barco code remarked
| 11.04.2001  1.00  Wss | Navigator icons replaced with new one -> mmDBNavigator.res
| 14.12.2001  1.00  Wss | Navigator hints replacable through property UseMMHints with resourcestrings
|=========================================================================================*)
unit mmDBNavigator;

interface

uses
  DBCtrls, Classes, DB, Messages, Controls, mmClasses,
  mmMemTable, SysUtils, Windows;

type
  TmmDBNavigator = class(TDBNavigator)
  private
    fUseMMHints: Boolean;
    procedure InitButtons;
    procedure SetUseMMHints(const Value: Boolean);
  protected
  public
    constructor Create(AOwner: TComponent); override;
  published
    property UseMMHints: Boolean read fUseMMHints write SetUseMMHints default False;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R mmDBNavigator}

resourcestring
  rsHint0 = '(*)Erster Datensatz'; // ivlm
  rsHint1 = '(*)Vorgaengiger Datensatz'; // ivlm
  rsHint2 = '(*)Naechster Datensatz'; // ivlm
  rsHint3 = '(*)Letzter Datensatz'; // ivlm
  rsHint4 = '(*)Einfuegen'; // ivlm
  rsHint5 = '(*)Loeschen'; // ivlm
  rsHint6 = '(*)Bearbeiten'; // ivlm
  rsHint7 = '(*)Speichern'; // ivlm
  rsHint8 = '(*)Abbrechen'; // ivlm
  rsHint9 = '(*)Daten aktualisieren'; // ivlm

const
  cBtnTypeName: Array[TNavigateBtn] of PChar = ('FIRST', 'PRIOR', 'NEXT',
    'LAST', 'INSERT', 'DELETE', 'EDIT', 'POST', 'CANCEL', 'REFRESH');

{
 cBtnHintStr: Array[TNavigateBtn] of String = (
    '(*)Erster Datensatz',
    '(*)Vorgaengiger Datensatz',
    '(*)Naechster Datensatz',
    '(*)Letzter Datensatz',
    '(*)Einfuegen',
    '(*)Loeschen',
    '(*)Bearbeiten',
    '(*)Speichern',
    '(*)Abbrechen',
    '(*)Daten aktualisieren'
  );
{}
//------------------------------------------------------------------------------
constructor TmmDBNavigator.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fUseMMHints := False;
  InitButtons;
end;
//------------------------------------------------------------------------------
procedure TmmDBNavigator.InitButtons;
var
  i: TNavigateBtn;
  xResName: string;
begin
  for i := Low(Buttons) to High(Buttons) do begin
    xResName := Format('MMDBN_%s', [cBtnTypeName[i]]);
    Buttons[i].Glyph.LoadFromResourceName(HInstance, xResName);
    Buttons[i].NumGlyphs := 2;
  end;
end;
//------------------------------------------------------------------------------

procedure TmmDBNavigator.SetUseMMHints(const Value: Boolean);
begin
  if (Value <> fUseMMHints) then begin
    fUseMMHints := Value;
    Hints.Text := rsHint0;
    Hints.Add(rsHint1);
    Hints.Add(rsHint2);
    Hints.Add(rsHint3);
    Hints.Add(rsHint4);
    Hints.Add(rsHint5);
    Hints.Add(rsHint6);
    Hints.Add(rsHint7);
    Hints.Add(rsHint8);
    Hints.Add(rsHint9);
  end;
end;

end.

