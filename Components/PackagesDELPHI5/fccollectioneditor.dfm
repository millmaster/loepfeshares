object fcCollectionEditorForm: TfcCollectionEditorForm
  Left = 413
  Top = 76
  Width = 179
  Height = 257
  BorderIcons = [biSystemMenu]
  Caption = 'fcCollectionEditorForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ItemsList: TListView
    Left = 0
    Top = 31
    Width = 171
    Height = 199
    Align = alClient
    Columns = <
      item
      end>
    DragMode = dmAutomatic
    HideSelection = False
    MultiSelect = True
    ReadOnly = True
    RowSelect = True
    PopupMenu = PopupMenu1
    ShowColumnHeaders = False
    TabOrder = 0
    ViewStyle = vsReport
    OnDragDrop = ItemsListDragDrop
    OnDragOver = ItemsListDragOver
    OnKeyUp = ItemsListKeyUp
    OnMouseUp = ItemsListMouseUp
  end
  object ToolBar: TToolBar
    Left = 0
    Top = 0
    Width = 171
    Height = 31
    ButtonHeight = 24
    ButtonWidth = 24
    EdgeBorders = [ebTop, ebBottom]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Wrapable = False
    object AddButton: TSpeedButton
      Left = 0
      Top = 2
      Width = 25
      Height = 24
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888888888888888888887777777777777788FFFFFFFFFFFFFF800000000000
        0007877777777777777F80FBFBFBFB00FB0787F8888888778F7F80BFBFBFBF08
        0F0787F88888887F787F80FBFBFBFB0B800787F88888887FF77F80BFBFBFBF00
        000787FFF8888877777FF0F7BFBFBBFBFB078F77F88F8888887F7BB7FB7BFFBF
        BF0778F7F8788888887F87F7B7BFBBFBFB0787F7F7888888887F777F7FBFBFBF
        BF07777F7FFFFFFFFF7FFB7BF77770000008777FF7777777777887B7B7B88888
        88888777F788888888887B87F87B888888887787F87888888888B887B8878888
        88887887F887888888888887F888888888888887888888888888}
      NumGlyphs = 2
      OnClick = AddButtonClick
    end
    object DeleteButton: TSpeedButton
      Left = 25
      Top = 2
      Width = 25
      Height = 24
      Flat = True
      Glyph.Data = {
        56010000424D56010000000000007600000028000000200000000E0000000100
        040000000000E000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00887777777777
        777788777777777777778000000000000007877777777777777780FBFBFBFB00
        FB0787F787878777F77770BFBFBFBF080F077778787878777F7710FBFBFBFB0B
        800777F78787F777777711BFBF71BF000007777F787778777777717BF717FBFB
        FB077777F777878787778117B11FBFBFBF078777777878787877871111FBFBFB
        FB07877777878787877787111FBFBFBFBF0787777F7F7F7F7F77711117000000
        0008777777777777777811781178888888887778777FF8888888888881178888
        888888888777FF88888888888811788888888888887778888888}
      NumGlyphs = 2
      OnClick = DeleteButtonClick
    end
    object ToolButton1: TToolButton
      Left = 50
      Top = 2
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 0
      Style = tbsDivider
    end
    object MoveUpButton: TSpeedButton
      Tag = -1
      Left = 58
      Top = 2
      Width = 25
      Height = 24
      Flat = True
      Glyph.Data = {
        FA000000424DFA000000000000007600000028000000180000000B0000000100
        0400000000008400000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888FFFFF88888800000888888877777F88888806660888888877777F8888880
        6660888888877777F8888880666088888FF77777FFFF00006660000877777777
        7778806666666088877777777788880666660888887777777888888066608888
        888777778888888806088888888877788888888880888888888887888888}
      NumGlyphs = 2
      OnClick = MoveButtonClick
    end
    object MoveDownButton: TSpeedButton
      Tag = 1
      Left = 83
      Top = 2
      Width = 25
      Height = 24
      Flat = True
      Glyph.Data = {
        FA000000424DFA000000000000007600000028000000180000000B0000000100
        0400000000008400000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        888888F88888888880888888888887FF88888888060888888888777FF8888880
        6660888888877777FF88880666660888887777777FF880666666608887777777
        77FF00006660000877777777777888806660888888877777F888888066608888
        88877777F88888806660888888877777F888888000008888888777778888}
      NumGlyphs = 2
      OnClick = MoveButtonClick
    end
    object ToolButton2: TToolButton
      Left = 108
      Top = 2
      Width = 25
      Caption = 'ToolButton2'
      ImageIndex = 1
      Style = tbsDivider
      Visible = False
    end
    object UserButton1: TSpeedButton
      Left = 133
      Top = 2
      Width = 25
      Height = 24
      Flat = True
      Visible = False
    end
    object UserButton2: TSpeedButton
      Left = 158
      Top = 2
      Width = 25
      Height = 24
      Flat = True
      Visible = False
    end
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 20
    Top = 48
    object Add1: TMenuItem
      Caption = '&Add'
      OnClick = AddButtonClick
    end
    object Delete1: TMenuItem
      Caption = '&Delete'
      OnClick = DeleteButtonClick
    end
    object MoveUp1: TMenuItem
      Tag = -1
      Caption = 'Move &Up'
      OnClick = MoveButtonClick
    end
    object MoveDown1: TMenuItem
      Tag = 1
      Caption = 'Move Dow&n'
      OnClick = MoveButtonClick
    end
    object SelectAll1: TMenuItem
      Caption = '&Select All'
      OnClick = SelectAll1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ShowToolbarMenu: TMenuItem
      Caption = '&Show Toolbar'
      OnClick = ShowToolbarMenuClick
    end
  end
end
