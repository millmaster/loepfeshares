(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DelphiDevReg.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Package pascal file for DelphiDev users
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 14.08.2000  1.00  Wss | Initial Release
|=========================================================================================*)
unit LoepfeReg;

interface
//------------------------------------------------------------------------------
procedure Register;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  regCommon,
  regWorbis,
  regMMSecurity;

//------------------------------------------------------------------------------
procedure Register;
begin
  regCommon.MMRegister;
  regWorbis.MMRegister;
  regMMSecurity.MMRegister;
end;
//------------------------------------------------------------------------------
end.
