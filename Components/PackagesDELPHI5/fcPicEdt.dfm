object fcPictureEditor: TfcPictureEditor
  Left = 228
  Top = 116
  BorderStyle = bsDialog
  Caption = 'Picture Editor'
  ClientHeight = 304
  ClientWidth = 354
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object OKButton: TButton
    Left = 274
    Top = 12
    Width = 75
    Height = 24
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelButton: TButton
    Left = 274
    Top = 41
    Width = 75
    Height = 24
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object HelpButton: TButton
    Left = 274
    Top = 70
    Width = 75
    Height = 24
    Caption = '&Help'
    TabOrder = 2
    OnClick = HelpButtonClick
  end
  object Panel: TPanel
    Left = 10
    Top = 12
    Width = 257
    Height = 281
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 3
    object ImagePanel: TPanel
      Left = 10
      Top = 10
      Width = 237
      Height = 233
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Color = clWindow
      TabOrder = 0
      object Image: TImage
        Left = 4
        Top = 4
        Width = 225
        Height = 221
        Center = True
      end
    end
    object LoadButton: TButton
      Left = 10
      Top = 250
      Width = 75
      Height = 23
      Caption = '&Load...'
      TabOrder = 1
      OnClick = LoadButtonClick
    end
    object SaveButton: TButton
      Left = 91
      Top = 250
      Width = 75
      Height = 23
      Caption = '&Save...'
      TabOrder = 2
      OnClick = SaveButtonClick
    end
    object ClearButton: TButton
      Left = 172
      Top = 250
      Width = 75
      Height = 23
      Caption = '&Clear'
      TabOrder = 3
      OnClick = ClearButtonClick
    end
  end
  object OpenDialog: TOpenPictureDialog
    DefaultExt = '*.bmp'
    Filter = 
      'All (*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf)|*.jpg;*.jpeg;*.bmp|JP' +
      'EG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bitm' +
      'aps (*.bmp)|*.bmp'
    Left = 54
    Top = 42
  end
  object SaveDialog: TSavePictureDialog
    Left = 90
    Top = 42
  end
end
