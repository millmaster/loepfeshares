(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: Main.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: MDI oder SDI GUI Tamplate
|                 Style mit Formproperty 'FormStyle' setzen [fsMDIForm,fsNormal]
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.11.1998  0.00  Sdo | Projekt begonnen
| 11.01.1999  1.00  Wss | Property Problem von TMMSecurity behoben (Stream read error)
| 04.03.1999  1.01  Wss | Template revised
| 25.03.1999  1.02  Wss | public property ApplicationName implemented for OLE Automation
|                       | public methode WriteLog() implemented for OLE Automation
| 29.09.1999  1.03  SDo | Property ShowStatusbarHint added (Show autom. hint in statusbar)
|                       | procedure DisplayHint is now overridebel
| 21.01.2000  1.04  Wss | Deriving changed to TForm: problem wiht ChildForms at maximized state
| 15.03.2000  1.04  Wss | Message of MMClient start/stop removed
| 12.04.2000  1.04  Wss | Login parameter set at BeforeConnect property instead OnLogin
| 20.04.2000  1.04  Wss | Hotkey for close (Ctrl+F4) removed -> system menu = Alt+F4
| 05.01.2001  1.05  SDo | Fill up the gApplicationName
| 22.11.2001  1.06  Wss | MemoryLeak: mLog was not cleaned up -> create now only if needed in WriteLog
| 17.01.2002  1.07  Wss | Sticker functionality removed finally
| 10.07.2002  1.07  Wss | Erweiterungen/Anpassungen fuer Chinesisch
| 20.09.2002        LOK | Umbau ADO
| 24.02.2003        Wss | GetUserToolButtons und SaveUserToolButtons Virtual gemacht, damit
                          diese Funktionen �berschrieben werden k�nnen, um das Konfigurieren
                          der Toolbar zu verhindern. -> MaConfig
| 03.10.2003        Wss | UpdateSecurity nach Login hinzugef�gt
| 13.01.2004        Wss | Wenn T�rkish dann auch Sprachauswahl limitieren
| 24.02.2005        Wss | In GetActivePrinterName() wird gepr�ft ob �berhaupt ein Drucker installiert ist
|=============================================================================*)
unit BASEAPPLMAIN;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, ExtCtrls, mmPanel, ComCtrls, mmStatusBar, ToolWin, mmToolBar,
  mmAnimate, IvDictio, IvAMulti, IvBinDic, mmDictionary, IvMulti, IvEMulti,
  mmTranslator, mmLogin, Menus, mmPopupMenu, mmOpenDialog, MMMessages,
  IvMlDlgs, mmSaveDialog, mmPrintDialog, mmPrinterSetupDialog, Db,
  mmMainMenu, StdActns, ActnList,
  mmActionList, ImgList, mmImageList,
  MMEventLog, StdCtrls, mmLabel, mmRadioGroup, mmButton,
  BaseApplAboutBox;

const
  // message strings
//  cMMClientStart = '(*)MMClient wurde gestartet';                        // ivlm
//  cMMClientStop =  '(*)MMClient wurde beendet';                          // ivlm
  cMsgMMClient = '(*)MMClient wurde nicht gestartet.'; // ivlm
  cMsgStdSparache1 = '(*)Sie muessen eine Standardsprache auswaehlen!'; // ivlm
  cMsgStdSparache2 = '(*)Es ist nur eine Standardsprache erlaubt!'; // ivlm

type
  // DO NOT CHANGE DERIVING OF THIS CLASS TO TmmForm (Wss)
  TBaseApplMainForm = class(TForm)
    ImageList16x16: TmmImageList;
    mmActionListMain: TmmActionList;
    acExit: TAction;
    acNew: TAction;
    acOpen: TAction;
    acSave: TAction;
    acSaveas: TAction;
    acPreView: TAction;
    acPrinterSetup: TAction;
    acPrint: TAction;
    acLogin1: TAction;
    acGerman: TAction;
    acEnglish: TAction;
    acWindowCascade: TWindowCascade;
    acWindowTileVertical: TWindowTileVertical;
    acTileHorizontally: TWindowTileHorizontal;
    acInfo: TAction;
    acHelp: TAction;
    acStandard: TAction;
    acCustomize: TAction;
    mmPrinterSetupDialog: TmmPrinterSetupDialog;
    mmPrintDialog: TmmPrintDialog;
    mmSaveDialog: TmmSaveDialog;
    mmOpenDialog: TmmOpenDialog;
    pmToolbar: TmmPopupMenu;
    pmiStandard: TMenuItem;
    pmiCustomize: TMenuItem;
    MMLogin: TMMLogin;
    mTranslator: TmmTranslator;
    mDictionary: TmmDictionary;
    plTopPanel: TmmPanel;
    Panel2: TmmPanel;
    Animate: TmmAnimate;
    mToolBar: TmmToolBar;
    mmStatusBar: TmmStatusBar;
    mMainMenu: TmmMainMenu;
    miDatei: TMenuItem;
    miNew: TMenuItem;
    N11: TMenuItem;
    miOpen: TMenuItem;
    N21: TMenuItem;
    miSave: TMenuItem;
    miSaveAs: TMenuItem;
    N31: TMenuItem;
    miPrinterSetup: TMenuItem;
    miPreview: TMenuItem;
    miPrint: TMenuItem;
    N41: TMenuItem;
    miExit: TMenuItem;
    miBearbeiten: TMenuItem;
    miExtras: TMenuItem;
    miLogin: TMenuItem;
    N6: TMenuItem;
    miLanguage: TMenuItem;
    miGerman: TMenuItem;
    miEnglish: TMenuItem;
    N7: TMenuItem;
    miToolbar: TMenuItem;
    miStandard: TMenuItem;
    N1: TMenuItem;
    miCustomize: TMenuItem;
    miFenster: TMenuItem;
    miCascade: TMenuItem;
    miTileHorizontally: TMenuItem;
    miTileVertically: TMenuItem;
    miHilfe: TMenuItem;
    miMMHilfe: TMenuItem;
    N5: TMenuItem;
    miInfo: TMenuItem;
    miSpanish: TMenuItem;
    acSpanish: TAction;
    acItalian: TAction;
    acPortug: TAction;
    miPortug: TMenuItem;
    miItalian: TMenuItem;
    miFrench: TMenuItem;
    acFrench: TAction;
    acChineseTrad: TAction;
    acChineseSimpl: TAction;
    miChineseTrad: TMenuItem;
    miChineseSimpl: TMenuItem;
    acTurkish: TAction;
    miTurkish: TMenuItem;
    procedure acCustomizeExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acInfoExecute(Sender: TObject);
    procedure acLanguageExecute(Sender: TObject);
    procedure acLogin1Execute(Sender: TObject);
    procedure acNewExecute(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure acPreViewExecute(Sender: TObject);
    procedure acPrintHint(var HintStr: string; var CanShow: Boolean);
    procedure acPrinterSetupExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acSaveasExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure acStandardExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
  private
    FMsgLogin: Boolean;
    FShowStatusbarHint: Boolean;
    mIniFile: string;
    mLog: TEventLogWriter;              // Schreibt Events ins Logfile
    mMessageApplID: WORD;
    fProductName: string;
    procedure CheckStandardButtons;
    function GetActivePrinterName: string;
    function GetDictionary: TmmDictionary;
    procedure GetStandardFunction(aStandardFunc: string);
    procedure ReadIniFile;
    procedure SetStandardToolButtons;
  protected
    mStandaloneMode: Boolean;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure DisplayHint(Sender: TObject); virtual;
    procedure GetUserToolButtons; virtual;
    procedure InitStandalone; virtual;
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SaveUserToolButtons; virtual;
    procedure UpdateSecurity; virtual;
  public
//    property ApplicationName: String read gApplicationName;
    property Dictionary: TmmDictionary read GetDictionary;
    property OnMsgLogin: Boolean read FMsgLogin write FMsgLogin;
    constructor Create(aOwner: TComponent); override;
    procedure CreateAboutBox; virtual;
    destructor Destroy; override;
    procedure SetNewLanguage(Sender: TObject; aLanguage: Integer);
    procedure SetWaitState(aSetWait: Boolean);
    procedure WriteLog(aEvent: TEventType; aText: string; aBuf: PByte = nil; aCount: DWord = 0);
    property ProductName: string read fProductName write fProductName;
  published
    property ShowStatusbarHint: Boolean read FShowStatusbarHint write FShowStatusbarHint;
  end;

var
  BaseApplMainForm: TBaseApplMainForm;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  LoepfeGlobal, ComServ, Printers, mmRegistry, ToolbarCustom, IniFiles;


{$R *.DFM}
{$R MMLogo.RES}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
procedure TBaseApplMainForm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);

  if not mStandaloneMode then begin
    with Params do begin
      ExStyle := ExStyle or WS_EX_TOPMOST; // or WS_EX_PALETTEWINDOW;
      WndParent := 0;
    end;
  end;
end;
//------------------------------------------------------------------------------
constructor TBaseApplMainForm.Create(aOwner: TComponent);
  //..............................................................
  procedure FreeComponent(aName: String);
  var
    xComponent: TComponent;
  begin
    xComponent := FindComponent('mi' + aName);
    if Assigned(xComponent) then xComponent.Free;
    xComponent := FindComponent('ac' + aName);
    if Assigned(xComponent) then xComponent.Free;
  end;
  //..............................................................
begin
  mStandaloneMode := ComServer.StartMode = smStandalone;
  mLog := Nil;

  inherited Create(aOwner);

  if mDictionary.LoepfeSystemLanguage in [glChineseTrad, glChineseSimpl, glTurkish] then begin
    FreeComponent('German');
    FreeComponent('Spanish');
    FreeComponent('Portug');
    FreeComponent('Italian');
    FreeComponent('French');
    if mDictionary.LoepfeSystemLanguage = glTurkish then begin
      FreeComponent('ChineseTrad');
      FreeComponent('ChineseSimpl');
    end else begin
      FreeComponent('Turkish');
    end;
  end else begin
    FreeComponent('ChineseTrad');
    FreeComponent('ChineseSimpl');
    FreeComponent('Turkish');
  end;

//  if mDictionary.IsChineseTrad or mDictionary.IsChineseSimpl then begin
//    FreeComponent('German');
//    FreeComponent('Spanish');
//    FreeComponent('Portug');
//    FreeComponent('Italian');
//    FreeComponent('French');
//  end else begin
//    FreeComponent('ChineseTrad');
//    FreeComponent('ChineseSimpl');
//  end;
//
  mDictionary.FloorCheck := True;
  FShowStatusbarHint := FALSE;

  // create a handle for a message event to receive messages from MMClient
  mMessageApplID := RegisterWindowMessage(cMsgAppl);

  // set gApplicationName if it isn't define in the project file
  if gApplicationName = '' then begin
    gApplicationName := ExtractFileName(Application.ExeName);
    gApplicationName := ChangeFileExt(gApplicationName, '');
  end;
  mIniFile := Format('%s\%s.ini', [GetCurrentDir, gApplicationName]);

  // UserButtons einlesen
  ReadIniFile;

  if mStandaloneMode then
    InitStandalone;

  // turns of the animation and set cursor to crDefault
  SetWaitState(FALSE);
end;
//------------------------------------------------------------------------------
destructor TBaseApplMainForm.Destroy;
begin
  if Assigned(mLog) then
    FreeAndNil(mLog);

  SaveUserToolButtons;
  inherited Destroy;
end;

//******************************************************************************
// Actions
//******************************************************************************
procedure TBaseApplMainForm.acCustomizeExecute(Sender: TObject);
begin
  // Form mit der Funktionensauswahl fuer die ToolButtons zeigen
  with TBaseConfigForm.Create(mmActionListMain, Self, mToolBar, FALSE, mIniFile) do try
    if ShowModal = mrOK then
      CheckStandardButtons;
  finally
    Free;
  end;
end;                                    // END  acCustomizeExecute
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acExitExecute(Sender: TObject);
begin
  Application.Terminate;
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acLanguageExecute(Sender: TObject);
begin
  // Tag funktioniert nicht, da Feld fuer "Standard" benutzt wird
  if Sender = acGerman then        mDictionary.GlossaryLanguage := glGerman
  else if Sender = acSpanish then  mDictionary.GlossaryLanguage := glSpanish
  else if Sender = acItalian then  mDictionary.GlossaryLanguage := glItalian
  else if Sender = acPortug then   mDictionary.GlossaryLanguage := glPortuguese
  else if Sender = acFrench then   mDictionary.GlossaryLanguage := glFrench
  else if Sender = acTurkish then  mDictionary.GlossaryLanguage := glTurkish
  else if Sender = acChineseTrad then  mDictionary.GlossaryLanguage := glChineseTrad
  else if Sender = acChineseSimpl then  mDictionary.GlossaryLanguage := glChineseSimpl
  else mDictionary.GlossaryLanguage := glEnglish;
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acInfoExecute(Sender: TObject);
begin
  CreateAboutBox;
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acLogin1Execute(Sender: TObject);
begin
  if MMLogin.Execute then
    UpdateSecurity;
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acNewExecute(Sender: TObject);
begin
//
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.UpdateSecurity;
begin
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acOpenExecute(Sender: TObject);
begin
//
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acPreViewExecute(Sender: TObject);
begin
  //
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acPrinterSetupExecute(Sender: TObject);
begin
  mmPrinterSetupDialog.Execute;
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acPrintExecute(Sender: TObject);
begin
  //
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acPrintHint(var HintStr: string; var CanShow: Boolean);
begin
  HintStr := GetActivePrinterName + '  ';
  acPrint.Hint := HintStr;
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acSaveExecute(Sender: TObject);
begin
  //
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acSaveasExecute(Sender: TObject);
begin
  mmSaveDialog.Execute;
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acStandardExecute(Sender: TObject);
begin
  acStandard.Checked := True;
  acCustomize.Checked := False;
  // Standartbuttons erstellen
  SetStandardToolButtons;
  CheckStandardButtons;
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.CheckStandardButtons;
var
  i,
    xStandardActionCounter, xAnzStandardAction, xAnzButtons: Integer;
  xAction: TAction;
begin
  xStandardActionCounter := 0;
  xAnzStandardAction := 0;
  xAnzButtons := 0;

  // Standard Action zaehlen
  for i := 0 to mmActionListMain.ActionCount - 1 do
    if mmActionListMain.Actions[i].Tag > 0 then
      inc(xStandardActionCounter);

  //Active Sprache setzen und 'Standard' oder 'Customize' im Menu markieren
  for i := 0 to mToolBar.ButtonCount - 1 do begin
    // Check ob nur StandardButtons in Toolbar vorhanden sind.
    // -> Markierung des MenueItem 'Standard' oder 'Customize'
    xAction := mToolBar.Buttons[i].Action as TAction;
    if Assigned(xAction) then begin
      inc(xAnzButtons);
      if xAction.Tag > 0 then
        inc(xAnzStandardAction);
    end;
  end;

  // Menuepunkt 'Standard' oder 'Customize' markieren
  if (xAnzStandardAction = xStandardActionCounter) and
    (xAnzButtons = xStandardActionCounter) then begin
    acStandard.Checked := TRUE;
    miStandard.Checked := TRUE;
  end else begin
    acCustomize.Checked := TRUE;
    miCustomize.Checked := TRUE;
  end;
end;                                    //END CheckStandardButtons
//******************************************************************************
// Erstellt ein  'About' Fenster (virtual)
//******************************************************************************
procedure TBaseApplMainForm.CreateAboutBox;
var xAbout: TBaseApplAboutBoxForm;
begin
  xAbout := TBaseApplAboutBoxForm.Create(Self);
  with xAbout do
  try
    ProductName := fProductName;
    ShowModal;
  finally
    Free;
  end;
  //ShowMessage('About box has to be implemented in your application. (wss)');
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
procedure TBaseApplMainForm.DisplayHint(Sender: TObject);
begin
  if FShowStatusbarHint then
    mmStatusBar.SimpleText := GetLongHint(Application.Hint);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittelt den aktuellen Druckername
//******************************************************************************
function TBaseApplMainForm.GetActivePrinterName: string;
begin
  if Printer.PrinterIndex < Printer.Printers.Count then
    Result := Printer.Printers[Printer.PrinterIndex]
  else
    Result := '';
end;
//------------------------------------------------------------------------------
function TBaseApplMainForm.GetDictionary: TmmDictionary;
begin
  Result := mDictionary;
end;
//******************************************************************************
// Setzt Standardfunktionen mittels Actions;  -> Action.Tag = 1
//******************************************************************************
procedure TBaseApplMainForm.GetStandardFunction(aStandardFunc: string);
var
  xText: string;
  xList: TStringlist;
  xi, xActCount: Integer;
begin
  xList := TStringlist.Create;
  xText := aStandardFunc;

  // Actions in Stringliste packen
  while (pos(';', xText) > 0) do begin
    xi := pos(';', xText);
    xList.Add(copy(xText, 0, pos(';', xText) - 1));
    xText := Copy(xText, xi + 1, Length(xText));
  end;

  //StandardActions -> Action.Tag auf 1 setzen
  for xi := 0 to xList.Count - 1 do begin
    for xActCount := 0 to mmActionListMain.ActionCount - 1 do
      if xList.Strings[xi] = mmActionListMain.Actions[xActCount].name then begin
        mmActionListMain.Actions[xActCount].Tag := 1;
      end;
  end;
  xList.free;
end;                                    // END GetStandardFunction
//------------------------------------------------------------------------------

//******************************************************************************
// User ToolButtons einlesen und anzeigen
//******************************************************************************
procedure TBaseApplMainForm.GetUserToolButtons;
begin
  // registriert eine Klasse fuer das persistente Objekt (TToolbutton)
  RegisterClass(TToolbutton);

  // Alle ToolButtons auf Toolbar loeschen
  while mToolBar.ButtonCount > 0 do
    mToolBar.Buttons[0].Free;

  if FileExists(gApplicationName + cTBExtension) then
    ReadComponentResFile(gApplicationName + cTBExtension, mToolBar)
  else
    SetStandardToolButtons;

  //Toolbar MenueItem-Markierung
  CheckStandardButtons;
end;                                    // END GetUserToolButtons
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.InitStandalone;
begin
  plTopPanel.Height := 32;

  // AVI-File aus Res. laden
  Animate.ResName := 'LOGO';

  SetWaitState(TRUE);

  Application.OnHint := DisplayHint;    // Hint auf Statusbar

  // read the resource file for Toolbar
  GetUserToolButtons;                 // aus *.TB File
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.Loaded;
begin
  inherited Loaded;

  // THIS FORM CAN NOT BE A CHILD FORM
  if FormStyle = fsMDIChild then
    FormStyle := fsNormal;

//  gApplicationName := ExtractFileName(Application.ExeName);
//  delete(gApplicationName, pos('.', gApplicationName), 4);
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);

{
  // if we are in running mode and forms are creating or destroying check for action state
  if (ComponentState = []) and (FormStyle = fsMDIForm) and
     AComponent.InheritsFrom(TForm) and (Operation = opRemove) then
    CheckActionState;
{}
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.ReadIniFile;
begin
  // Daten
  with TIniFile.Create(mIniFile) do try
    GetStandardFunction(ReadString(cIniAbschittStandard, cIniDefaultActions, ''));
  finally
    Free;
  end;
end;                                    // END ReadIniFile
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//******************************************************************************
// User ToolButtons in File speichern
//******************************************************************************
procedure TBaseApplMainForm.SaveUserToolButtons;
begin
  WriteComponentResFile(gApplicationName + cTBExtension, mToolBar);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Sprache setzen
//******************************************************************************
//procedure TBaseApplMainForm.SetNewLanguage(Sender: TObject; aMenuItem: TMenuItem);
procedure TBaseApplMainForm.SetNewLanguage(Sender: TObject; aLanguage: Integer);
begin
  mDictionary.PrimaryLanguage := aLanguage;
end;
//******************************************************************************
// User Standard-ToolButtons setzen
//******************************************************************************
procedure TBaseApplMainForm.SetStandardToolButtons;
begin
  // Form mit der Funktionensauswahl fuer die ToolButtons zeigen
  with TBaseConfigForm.Create(mmActionListMain, Self, mToolBar, TRUE, mIniFile) do try
//    Hide;
    SetStandardButtons;
  finally
    Free;
  end;
end;
//******************************************************************************
//  Setzt den WaitState (Animation, Cursor)
//******************************************************************************
procedure TBaseApplMainForm.SetWaitState(aSetWait: Boolean);
begin
  Animate.Active := aSetWait;
  if aSetWait then Cursor := crHourGlass
  else Cursor := crDefault;
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.WriteLog(aEvent: TEventType; aText: string; aBuf: PByte; aCount: DWord);
begin
  if not Assigned(mLog) then
    mLog := TEventLogWriter.Create(cEventLogMillMasterClass,
                                   gMMHost,
                                   ssApplication,
                                   gApplicationName + ': ',
                                   True);

  if Assigned(aBuf) and (aCount > 0) then
    mLog.WriteBin(aEvent, aText, aBuf, aCount)
  else
    mLog.Write(aEvent, aText);
end;
//------------------------------------------------------------------------------
procedure TBaseApplMainForm.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

end.

