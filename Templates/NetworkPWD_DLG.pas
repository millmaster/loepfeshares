{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: NetworkPWD_DLG.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...: -
|                 -
|                 -
| Info..........: Wird in Zusammenhang mit der API Func. WNetAddConnection2
|                 gebraucht. Wenn eine Verbindung fehlschlägt, sollte dieser
|                 TNetworkPWD-DLG aufgerufen werden. Danach mit Username +
|                 PWD die Func. NetWare.MachineConnection() nochmals aufrufen.
|
| Develop.system: Win2k
| Target.system.: Win2k /XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 26.03.2003  1.00  SDo | File created
===============================================================================}
unit NetworkPWD_DLG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmLabel, mmEdit;

type
  TNetworkPWD = class(TForm)
    edConnectAs: TmmEdit;
    edPWD: TmmEdit;
    mmLabel1: TmmLabel;
    lbPWD: TmmLabel;
    lbConnectAs: TmmLabel;
    mmButton1: TmmButton;
    mmButton2: TmmButton;
    lbPCName: TmmLabel;
  private
    FPcName: String;
    procedure SetPcName(const Value: String);
    function GetConnectAS: String;
    function GetPWD: String;
  private
    { Private declarations }
  public
    { Public declarations }
    property PcName : String write SetPCName ;
    property ConnectAS : String read GetConnectAS ;
    property PWD : String read GetPWD ;
  end;

var
  NetworkPWD: TNetworkPWD;

implementation

{$R *.DFM}

{ TNetworkPWD }

//------------------------------------------------------------------------------
procedure TNetworkPWD.SetPcName(const Value: String);
begin
  FPcName := Value;
  FPcName := StringReplace(FPcName, '\\','',[rfReplaceAll]) ;
  lbPCName.Caption :=  '\\' + FPcName;
end;
//------------------------------------------------------------------------------
function TNetworkPWD.GetConnectAS: String;
begin
  result := edConnectAs.Text;
end;
//------------------------------------------------------------------------------
function TNetworkPWD.GetPWD: String;
begin
  result := edPWD.Text;
end;
//------------------------------------------------------------------------------
end.
