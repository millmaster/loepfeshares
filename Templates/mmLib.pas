(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmLib.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmLib;

interface

uses
  Windows, SysUtils, Forms, Dialogs, Controls, Classes, DB, Graphics, ComCtrls;

type
  TDataModuleClass = class of TDataModule;

  TSearchTree = class(TObject)
  Private
    FSubDirs : TStringList;
    FStatus : Integer;
    FCurPath : String;
    FRootPath : String;
    FRootAttr : Integer;
    FSearchRec : TSearchRec;
    procedure LoadResult;
    procedure LoadSubdirs;
  Public
    Time : Integer;
    Size : Integer;
    Attr : Integer;
    Status : Integer;
    FileName : TFileName;
    PathName : String;
    constructor Create;
    destructor Destroy;override;
    procedure First(const Path:String; Attr: Integer);
    procedure Next;
  end;

{var}

procedure ActivateData(Owner: TComponent; var Reference; DataModuleClass: TDataModuleClass);
procedure ActivateForm(Owner: TComponent; var Reference; FormClass: TFormClass);
function  CalcFieldWidth(Field: TField; Font: TFont): TWidth;
procedure CloseData(Owner: TComponent; var Reference);
procedure CloseForm(Owner: TComponent; var Reference);
procedure DisplayForm(Owner: TComponent; var Reference; FormClass: TFormClass);
function  ExtractElement(const Elements,Separator: string; var Pos: Integer): string;
function  FormActive(Owner: TComponent; var Reference; FormClass: TFormClass): Boolean;
function  IntTimeToStrTime(ITime: Integer): string;
function  mmErrorDlg(Interactive: Boolean; const Msg: string; DlgType: TMsgDlgType;
                                Buttons: TMsgDlgButtons; HelpCtx: Longint): Integer;
procedure mmException(Sender: TObject; E: Exception);
function  mmUserName: String;
function  StrTimeToIntTime(Text: string; const MaxHrs: integer): Integer;
function Rat(InStr: String; SearchStr: String): Byte;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{uses}
//------------------------------------------------------------------------------
procedure ActivateData(Owner: TComponent; var Reference; DataModuleClass: TDataModuleClass);
var
  Instance: TDataModule;
  Create: Boolean;
begin
  if (TDataModule(Reference) = nil) then
    Create := True
  else if (TDataModule(Reference).Owner <> Owner) then
    Create := True
  else
    Create := False;
  if Create then
  begin
    Screen.Cursor := crHourGlass;
    try
      Instance := TDataModule(DataModuleClass.NewInstance);
      TDataModule(Reference) := Instance;
      try
        Instance.Create(Owner);
///        Instance.Owner := Owner;
      except
        TDataModule(Reference) := nil;
        Instance.Free;
        raise;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure ActivateForm(Owner: TComponent; var Reference; FormClass: TFormClass);
var
  Instance: TForm;
  Create: Boolean;
begin
  if (TForm(Reference) = nil) then
    Create := True
  else if (TForm(Reference).Owner <> Owner) then
    Create := True
  else
    Create := False;
  if Create then
  begin
    Screen.Cursor := crHourGlass;
    try
      Instance := TForm(FormClass.NewInstance);
      TForm(Reference) := Instance;
      try
        Instance.Create(Owner);
///        Instance.Owner := Owner;
        Instance.Show;
      except
        TForm(Reference) := nil;
        Instance.Free;
        raise;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end
  else
  begin
    if (TForm(Reference).WindowState in [wsMinimized]) then
      TForm(Reference).WindowState := wsNormal;
    TForm(Reference).Show;
  end;
end;
//------------------------------------------------------------------------------
function CalcFieldWidth(Field: TField; Font: TFont) : TWidth;
var
  len: integer;
  datatype: TFieldType;
 begin
  datatype := Field.DataType;
  Result   := 0;
 { (previous version)
  if datatype = ftstring then
    Result := ((len + 1)* size) + len
  else if datatype in [ftSmallInt,ftInteger,ftWord,ftFloat] then
    Result := ((len + 1)* size) + len
 }
  if datatype = ftstring then
    begin
      len       := Field.Size;
      if len = 1 then
        Result := 22
      else if len = 2 then
        Result := 36
      else if len < 11 then
        Result := ((len + 1) * Font.size) + Font.Size + (Len div 2)
      else
        begin
          Result := ((len + 1) * Font.size) + len;
          Result := round((-0.0001429 * Result * Result) + (0.9319154 * Result));
        end
    end
  else if datatype in [ftSmallInt,ftInteger,ftWord,ftFloat] then
    begin
      len    := Field.DisplayWidth;
  //    if len < 6 then
        Result := ((len + 1) * Font.size) + len
  //    else
  //      begin
  //        Result := ((len + 1) * Font.size) + len;
  //        Result := round((2 * Result) / 3);
  //      end
    end
  else if datatype = ftDate then
    Result := 60
  else if datatype = ftTime then
    Result := 60
  else if datatype = ftDateTime then
    Result := 120;
end;
//------------------------------------------------------------------------------
procedure CloseData(Owner: TComponent; var Reference);
var
  Close: Boolean;
begin
  if (TDataModule(Reference) = nil) then
    Close := False
  else if (TDataModule(Reference).Owner <> Owner) then
    Close := False
  else
    Close := True;
  if Close then
  begin
    TDataModule(Reference).FreeInstance;
    TDataModule(Reference) := nil;
  end;
end;
//------------------------------------------------------------------------------
procedure CloseForm(Owner: TComponent; var Reference);
var
  Close: Boolean;
begin
  if (TForm(Reference) = nil) then
    Close := False
  else if (TForm(Reference).Owner <> Owner) then
    Close := False
  else
    Close := True;
  if Close then
  begin
    TForm(Reference).FreeInstance;
    TForm(Reference) := nil;
  end;
end;
//------------------------------------------------------------------------------
procedure DisplayForm(Owner: TComponent; var Reference; FormClass: TFormClass);
var
  Instance: TForm;
  Create: Boolean;
  SavOnHint, SavOnActiveControlChange: TNotifyEvent;
begin
  SavOnHint := Application.OnHint;
  SavOnActiveControlChange := Screen.OnActiveControlChange;
  Application.OnHint := nil;
  SavOnActiveControlChange := nil;
  Instance := nil;
  try
    if (TForm(Reference) = nil) then
      Create := True
    else if (TForm(Reference).Owner <> Owner) then
      Create := True
    else
      Create := False;
    if Create then
    begin
      Screen.Cursor := crHourGlass;
      try
        Instance := TForm(FormClass.NewInstance);
        TForm(Reference) := Instance;
        try
          Instance.Create(Owner);
          Instance.ShowModal;
        except
          TForm(Reference) := nil;
          Instance.Free;
          raise;
        end;
      finally
      //> HVER 13/11/97 (called by Floor)
        if (TForm(Reference) <> nil) then
        begin
          TForm(Reference) := nil;
          Instance.Free;
        end;
      //< HVER 13/11/97
        Screen.Cursor := crDefault;
      end;
    end
    else
      TForm(Reference).ShowModal;
  finally
    Application.OnHint := SavOnHint;
    Screen.OnActiveControlChange := SavOnActiveControlChange;
  end;
end;
//------------------------------------------------------------------------------
function  ExtractElement(const Elements,Separator: string; var Pos: Integer): string;
var
  I: Integer;
begin
  I := Pos;
  while (I <= Length(Elements)) and (Elements[I] <> Separator) do Inc(I);
  Result := Trim(Copy(Elements,Pos,I-Pos));
  if (I <= Length(Elements)) and (Elements[I] = Separator) then Inc(I);
  Pos := I;
end;
//------------------------------------------------------------------------------
function FormActive(Owner: TComponent; var Reference; FormClass: TFormClass): Boolean;
begin
  if (TForm(Reference) = nil) then
    Result := False
  else if (TForm(Reference).Owner <> Owner) then
    Result := False
  else
    Result := True;
  if Result then  // (re-)display the form
  begin
    Screen.Cursor := crHourGlass;
    try
      try
        if (TForm(Reference).WindowState in [wsMinimized]) then
          TForm(Reference).WindowState := wsNormal;
        TForm(Reference).Show;
      except
        TForm(Reference) := nil;
        Result := False;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;
//------------------------------------------------------------------------------
function  IntTimeToStrTime(ITime: Integer): string;
var
  LText:  string;
begin
  {Convert integer to string time}
  LText  := IntToStr(ITime);
  {Add ':' in right position}
  case Length(LText) of
     1: Result := '00:0' + LText;
     2: Result := '00:'  + LText;
     3: Result := '0' + Copy(LText,1,1) + ':' + Copy(LText,2,2);
     4:
       begin
         Insert(':',LText,3);
         Result := LText;
       end;
  end;
end;
//------------------------------------------------------------------------------
function mmErrorDlg(Interactive: Boolean; const Msg: string; DlgType: TMsgDlgType;
                               Buttons: TMsgDlgButtons; HelpCtx: Longint): Integer;
var
  FileName: String;
  ErrorLog: TextFile;
begin
  if (Interactive) then
    Result := MessageDlgPos(Msg, DlgType, Buttons, HelpCtx, -1, -1)
  else
  begin
//  FileName := Copy(Application.ExeName, 1, Pos('.', Application.ExeName)-1) + '.ERR';
    FileName := Copy(Application.ExeName, 1, Rat(Application.ExeName, '.')-1) + '.ERR';
    AssignFile(ErrorLog,FileName);
    {Open the file for appending. If it does not exist then create it.}
    try
      Append(ErrorLog);
    except
      on EInOutError do Rewrite(ErrorLog);
    end;
    {Write the date, time user name and error message to the error log file.}
    Writeln(Errorlog, format('%s',['-----------------']));
    Writeln(Errorlog, format('%s [User: %s] %s',[DateTimeToStr(Now),mmUserName,Msg]));
    {Close the error log file.}
    CloseFile(ErrorLog);
    Result := 0;
  end;
end;
//------------------------------------------------------------------------------
procedure mmException(Sender: TObject; E: Exception);
begin
//  ExceptProc := SaveExcept;    {Restore exception procedure}
  {Display (LOG) the error message dialog box.}
  mmErrorDlg(False, E.Message, mtError, [mbOk], 0);
//  ExceptProc := @LibError;   {Re-Install LibError exception handler}
end;
//------------------------------------------------------------------------------
function mmUserName: String;
var
  Name: String;
  NameLength, ErrorCode: Dword;
  Status: Boolean;
begin
  SetLength(Name,31);
  NameLength := Length(Name);
  Status := GetUserName(PChar(Name), NameLength);
  if Status then
    begin
     SetLength(Name, NameLength);
     Result := Name
    end
  else
    begin
      Result := '?';
      ErrorCode := GetLastError();
      MessageDlg(IntToStr(ErrorCode),mtWarning,[mbok],0);
    end;
end;
//------------------------------------------------------------------------------
function  StrTimeToIntTime(Text: string; const MaxHrs: integer): Integer;
var
  LText,LTextLoc:  string;
  I,ILen,Code: integer;
begin
  {Remove leading zeroes in Text when ':' is used}
  if (Pos(':',Text) > 3) then
  begin
    LText := Copy(Text,1,Pos(':',Text)-1);
    while Pos('0',LText) = 1 do LText := Copy(LText,2,Length(LText)-1);
    Text := LText + Copy(Text,Pos(':',Text),Length(Text)-Pos(':',Text)+1);
  end;
  {Check # positions after ':'}
  if (Pos(':',Text) < Length(Text)) then
  begin
    LText := Copy(Text,Pos(':',Text)+1,Length(Text)-Pos(':',Text));
    while Pos('0',LText) = 1 do LText := Copy(LText,2,Length(LText)-1);
    Text := Copy(Text,1,Pos(':',Text)) + LText;
  end;

  case Pos(':',Text) of
    {When ':' is in the first position of Text(Only minutes are entered}
    1:
      begin
        LText := Copy(Text,Pos(':',Text)+1,Length(Text)-1);
        Val(LText,I,Code);
        if ((Code = 0) and (I > 59)) then
        begin
          MessageDlg ('The value for minutes is not correct !' + #13#10 +
            '    Invalid value : ' + LText, mtWarning, [mbOK],0);
          Abort;
        end;
      end;
    {When ':' is in the second position of Text}
    2:
      begin
        LTextLoc := Copy(Text,Pos(':',Text)+1,Length(Text)-Pos(':',Text));
        Val(LTextLoc,I,Code);
        if ((Code = 0) and (I > 59)) then
        begin
          MessageDlg ('The value for minutes is not correct !' + #13#10 +
            '    Invalid value : ' + LTextLoc, mtWarning, [mbOK],0);
          Abort;
        end
        else
        begin
          {When 2 positions used for minutes in the given Text}
          if (Length(LTextLoc) = 2) then
            LText := '0' + Copy(Text,1,1) + Copy(Text,3,Length(Text)-Pos(':',Text))
          else
          begin
            {When 1 position used for minutes in the given Text}
            if (Length(LTextLoc) = 1) then
              LText := '0' + Copy(Text,1,1) + '0' + Copy(Text,3,Length(Text)-Pos(':',Text))
            else
            begin
              {When no positions used for minutes in the given Text}
              LText := '0' + Copy(Text,1,1) + '00';
            end;
          end;
        end;
      end;
    {When ':' is in the third position of Text}
    3:
      begin
        {Check hours}
        LTextLoc := Copy(Text,1,Pos(':',Text)-1);
        Val(LTextLoc,I,Code);
        if ((Code = 0) and (I > MaxHrs)) then
        begin
          MessageDlg ('The value for Hours is not correct !' + #13#10 +
            '    Invalid value : ' + LTextLoc, mtWarning, [mbOK],0);
          Abort;
        end;
        {Check minutes}
        LTextLoc := Copy(Text,Pos(':',Text)+1,Length(Text)-Pos(':',Text));
        Val(LTextLoc,I,Code);
        if ((Code = 0) and (I > 59)) then
        begin
          MessageDlg ('The value for minutes is not correct !' + #13#10 +
            '    Invalid value : ' + LTextLoc, mtWarning, [mbOK],0);
          Abort;
        end
        else
        begin
          {When 2 positions used for minutes in the given Text}
          if (Length(LTextLoc) = 2) then
            LText := Copy(Text,1,2) + Copy(Text,4,Length(Text)-Pos(':',Text))
          else
          begin
            {When 1 position used for minutes in the given Text}
            if (Length(LTextLoc) = 1) then
              LText := Copy(Text,1,2) + '0' + Copy(Text,4,Length(Text)-Pos(':',Text))
            else
            begin
              {When no positions used for minutes in the given Text}
              LText := Copy(Text,1,2) + '00';
            end;
          end;
        end;
      end;
  else
    begin
      {When ':' is used but not in a usual position => Hours is too big}
      if (Pos(':',Text) > 0) then
      begin
        MessageDlg ('The value for hours is not correct !' + #13#10 +
          '    Invalid value : ' + Copy(Text,1,Pos(':',Text)-1), mtWarning, [mbOK],0);
        Abort;
      end;
      {When no ':' is used}
      ILen := Length(Text);
      if (ILen >= 2) then
      begin
        {Check minutes}
        LTextLoc := Copy(Text,ILen-1,2);
        Val(LTextLoc,I,Code);
        if ((Code = 0) and (I > 59)) then
        begin
          MessageDlg ('The value for minutes is not correct !' + #13#10 +
            '    Invalid value : ' + LTextLoc, mtWarning, [mbOK],0);
          Abort;
        end;
        if ILen > 2 then
        begin
          {Check hours}
          LTextLoc := Copy(Text,1,ILen-2);
          Val(LTextLoc,I,Code);
          if ((Code = 0) and (I > MaxHrs)) then
          begin
            MessageDlg ('The value for hours is not correct !' + #13#10 +
              '    Invalid value : ' + LTextLoc, mtWarning, [mbOK],0);
            Abort;
          end;
        end;
      end;
      LText := Text;
    end;
  end;
  try
    {Convert LText (=string whitout ':') to integer}
    Result := StrToInt(LText);
  except
    if LText = '' then
      Result := 0
    else
    begin
      Result := 0;
      MessageDlg ('The value for time is not correct !', mtWarning, [mbOK],0);
      Abort;
    end;
  end;
end;
//------------------------------------------------------------------------------
function Rat(InStr: String; SearchStr: String): Byte;
Var
 Quit    : Boolean;
 Pos,Len : Integer;
Begin
 RAT := 0;
 Quit:= False;
 Len := Length(SearchStr);
 Pos := Length(InStr) - Len;
 while Quit = False do begin
   if Copy(InStr,Pos,Len) = SearchStr then begin
     RAT := Pos;
     Quit:= True;
   end;
   if Pos = 1 then begin
     Quit:= True;
   end;
   Dec(Pos,1);
 end;
end;

//*****************************************************************************
//* TSearchThree
//*****************************************************************************
constructor TSearchTree.Create;
begin
  inherited Create;
  FSubDirs := TStringList.Create;
  FStatus := -1;
  Status := 0;
end;
//------------------------------------------------------------------------------
Destructor TSearchTree.Destroy;
begin
  if (FStatus = 0) then FindClose(FSearchRec);
  FSubDirs.Free;
  inherited;
end;
//------------------------------------------------------------------------------
procedure TSearchTree.First(const Path:String; Attr: Integer);
begin
  FSubDirs.Clear;
  If (FStatus = 0) then FindClose(FSearchRec);
  FRootPath := Path;
  FRootAttr := Attr;
  FStatus := 1;
  FSubDirs.Add(FRootPath);
  while (FSubDirs.Count>0) and (FStatus <> 0)do
    begin
      FCurPath := FSubDirs.Strings[0];
      FSubDirs.Delete(0);
      LoadSubdirs;
      FStatus := FindFirst(FCurPath, Attr, FSearchRec);
      if (FStatus <> 0) then
        FindClose(FSearchRec);
    end;
  if (FStatus=0) then LoadResult;
  Status := FStatus;
end;
//------------------------------------------------------------------------------
procedure TSearchTree.LoadResult;
begin
  Time := FSearchRec.Time;
  Size := FSearchRec.Size;
  Attr := FSearchRec.Attr;
  FileName := FSearchRec.Name;
  PathName := FCurPath;
end;
//------------------------------------------------------------------------------
procedure TSearchTree.LoadSubdirs;
var
  LocAttr: Integer ;
  SearchStr : String;
  i : Integer;
begin
  SearchStr := FCurPath;
  i := Length(SearchStr);
  while (i>3)and(Copy(SearchStr,i,1)<>'\') do i:=i-1;
  if (Copy(SearchStr,i,1)='\') then
    SearchStr:=Copy(SearchStr,1,i)+'*.*'
  else
    Exit;
  LocAttr := faDirectory;
  Status := FindFirst(SearchStr,LocAttr,FSearchRec);
  while (Status = 0) do
    begin
      if (FSearchRec.Attr and faDirectory>0) and
         (FSearchRec.Name <> '.')and
         (FSearchRec.Name <> '..') then
        FSubDirs.Add(Concat(Copy(Searchstr,1,i),FSearchRec.Name,'\',
                     Copy(FCurPath,i+1,Length(FCurPath)-i)));
      Status := FindNext(FSearchRec);
    end;
  FindClose(FSearchRec);
end;
//------------------------------------------------------------------------------
Procedure TSearchTree.Next;
begin
  if (FStatus = 0) then
    begin
      FStatus := FindNext(FSearchRec);
      if (FStatus <> 0) then FindClose(FSearchRec);
    end;
  while (FSubDirs.Count>0) and (FStatus <> 0) do
    begin
      FCurPath := FSubDirs.Strings[0];
      FSubDirs.Delete(0);
      LoadSubdirs;
      FStatus := FindFirst(FCurPath, Attr, FSearchRec);
      if (FStatus <> 0) then FindClose(FSearchRec);
    end;
  if (FStatus=0) then LoadResult;
  Status := FStatus;
end;
//------------------------------------------------------------------------------

end.

