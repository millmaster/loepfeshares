(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DlgUndelete.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit DlgUndelete;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmCtrls, StdCtrls, DBCtrls, mmDBCheckBox, mmLabel, DBTables, DB,
  mmDataSource, ComCtrls, mmButton, mmPageControl, ExtCtrls, mmPanel,
  mmLookupBox, mmTable, mmGroupBox, mmDBListBox, BaseDialog;

//  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
//  StdCtrls, DBCtrls, mmDBListBox, mmLabel, mmButton, DB,
// DBTables, mmTable, mmDataSource;

type
  TDlgUndelete_Data = class(TmmDialog)
    LblKeyInfoUnd: TmmLabel;
    mmDBListBox1: TmmDBListBox;
    ActiveSource: TmmDataSource;
    SrcKeyInfoUnd: TmmDataSource;
    TblKeyInfoUnd: TmmTable;
    BtnOK: TmmButton;
    BtnCancel: TmmButton;
    BtnHelp: TmmButton;
    procedure BtnOKClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure PrepareData;
    { Public declarations }
  end;

var
  DlgUndelete_Data: TDlgUndelete_Data;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}
procedure TDlgUndelete_Data.PrepareData;
begin
  TblKeyInfoUnd.DatabaseName := TTable(ActiveSource.DataSet).DatabaseName;
  TblKeyInfoUnd.TableName :=
    UpperCase(TTable(ActiveSource.DataSet).TableName);
  TblKeyInfoUnd.IndexName := TTable(ActiveSource.DataSet).IndexName;
  if not (TblKeyInfoUnd.Active) then TblKeyInfoUnd.Open;
end;

procedure TDlgUndelete_Data.BtnOKClick(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
//  try
//    if (not (ActiveSource.DataSet.State = dsEdit)) then
//      ActiveSource.DataSet.Edit;
//    with SrcKeyInfoUnd.DataSet as TTable do
//      for I := 0 to FieldCount - 1 do
//      begin
 //       LFieldName := Fields[I].FieldName;
 //       if (Pos(LFieldName,LFilterFields) = 0) then
 //         ActiveSource.DataSet.FieldValues[LFieldName] := SrcKeyInfoUnd.DataSet.FieldValues[LFieldName];
 //     end;
 // finally
    Screen.Cursor := crDefault;
//  end;
  Close;
  ModalResult := mrOk;

end;

procedure TDlgUndelete_Data.BtnHelpClick(Sender: TObject);
begin
  inherited;
  Application.HelpContext(HelpContext);
end;

end.
