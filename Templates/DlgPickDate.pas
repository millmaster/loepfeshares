(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DlgPickDate.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit DlgPickDate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmCtrls, Grids, DBGrids, mmDBGrid, StdCtrls, DBCtrls, mmDBCheckBox,
  mmDBText, mmLabel, DBTables, DB, mmQuery, mmDataSource, ComCtrls,
  mmButton, mmPageControl, ExtCtrls, mmPanel, Calendar, Buttons,
  mmSpeedButton, Mask, mmMaskEdit, mmUpDown,
  mmDateTimePicker, mmEdit, BaseDialog;

type
  TDlgPick_Date = class(TmmDialog)
    PnlMain: TmmPanel;
    PnlButtons: TmmPanel;
    BtnCancel: TmmButton;
    BtnOk: TmmButton;
    BtnPrevMonth: TmmSpeedButton;
    BtnNextMonth: TmmSpeedButton;
    LblMonthName: TmmLabel;
    Calendar: TCalendar;
    procedure BtnPrevMonthClick(Sender: TObject);
    procedure BtnNextMonthClick(Sender: TObject);
    procedure CalendarChange(Sender: TObject);
    procedure CalendarDblClick(Sender: TObject);
  private
    procedure SetDate(DateValue: TDateTime);
    function GetDate: TDateTime;
  public
    property DateValue: TDateTime read GetDate write SetDate;
  end;

var
  DlgPick_Date: TDlgPick_Date;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TDlgPick_Date.BtnPrevMonthClick(Sender: TObject);
begin
  Calendar.PrevMonth;
end;

procedure TDlgPick_Date.BtnNextMonthClick(Sender: TObject);
begin
  Calendar.NextMonth;
end;

procedure TDlgPick_Date.CalendarChange(Sender: TObject);
begin
  LblMonthName.Caption := FormatDateTime('MMMM, YYYY', Calendar.CalendarDate);
end;

procedure TDlgPick_Date.SetDate(DateValue: TDateTime);
begin
 Calendar.CalendarDate := DateValue;
end;

function TDlgPick_Date.GetDate: TDateTime;
begin
  Result := Calendar.CalendarDate;

end;

procedure TDlgPick_Date.CalendarDblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
