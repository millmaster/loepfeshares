(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DlgAbout.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit DlgAbout;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmCtrls, StdCtrls, mmLabel, ComCtrls, mmButton, ExtCtrls, mmPanel, mmImage,
  BaseDialog;

type
  TDlgAbout_Data = class(TmmDialog)
    PnlAbout: TmmPanel;
    PnlAboutInfo: TmmPanel;
    BtnOK: TmmButton;
    LblProgramVersion: TmmLabel;
    LblCopyRight: TmmLabel;
    ImgProgramIcon: TmmImage;
    LblProgramName: TmmLabel;
  end;

var
  DlgAbout_Data: TDlgAbout_Data;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

end.
