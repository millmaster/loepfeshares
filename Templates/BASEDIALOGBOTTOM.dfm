inherited DialogBottom: TDialogBottom
  Left = 338
  Top = 104
  Caption = 'DialogBottom'
  ClientHeight = 255
  ClientWidth = 418
  PixelsPerInch = 96
  TextHeight = 13
  object bOK: TmmButton
    Left = 226
    Top = 225
    Width = 87
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '(9)OK'
    ModalResult = 1
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bCancel: TmmButton
    Left = 326
    Top = 225
    Width = 87
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '(9)Abbrechen'
    ModalResult = 2
    TabOrder = 1
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
end
