(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmStringList.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit mmStringList;

interface

uses
  Classes;

type
  TIniStringList = class(TStringList)
  private
    function GetValue(const Name: string): Variant;
    procedure SetValue(const Name: String; Value: Variant);
  public
    property Values[const Name: string]: Variant read GetValue write SetValue;
    function ValueDef(aName: String; aDefault: Variant): Variant;
  end;
//------------------------------------------------------------------------------
  TmmStringList = class(TIniStringList)
  private
  protected
  public
    function AddVar(const aValue: Variant): Integer;
    function Value(aIndex: Integer): Variant;
  published
  end;
//------------------------------------------------------------------------------
  TObjectStringList = class(TStringList)
  private
    procedure ClearObjects;
    procedure ClearObject(aIndex: Integer);
  public
    procedure Clear; override;
    procedure Delete(Index: Integer); override;
    destructor Destroy; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, sysutils;

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TIniStringList
//------------------------------------------------------------------------------
function TIniStringList.GetValue(const Name: string): Variant;
begin
  Result := inherited Values[Name];
end;
//------------------------------------------------------------------------------
procedure TIniStringList.SetValue(const Name: String; Value: Variant);
begin
  inherited Values[Name] := Value;
end;
//------------------------------------------------------------------------------
function TIniStringList.ValueDef(aName: String; aDefault: Variant): Variant;
var
  i: integer;
  xFound: boolean;
begin
  Result := Values[aName];
  // varString darf nicht noch zusätzlich im vorhergehenden Set drin sein, da ein Set max
  // 255 Elemente haben darf, varString aber auf $100 (=256) definiert ist.
  if (VarType(aDefault) in [varOleStr, varVariant, varStrArg]) or (VarType(aDefault) = varString) then begin
    i := 0;
    xFound := false;
    while (i < count) and (not xFound) do begin
      xFound := AnsiSameText(aName, Names[i]);
      inc (i);
    end;// while (i < count) and (not(xFound)) do begin
    if not xFound then
      result := aDefault;
  end else begin
    if (Result = '') and (VarType(aDefault) in [varSmallint,varInteger,varSingle,varDouble,varCurrency,varDate,varBoolean,varByte]) then
      Result := aDefault;
  end;
(*  Result := Values[aName];
  if (Result = '') and (VarType(aDefault) in [varSmallint,varInteger,varSingle,varDouble,varCurrency,varDate,varBoolean,varByte]) then
    Result := aDefault;*)
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TmmStringList
//------------------------------------------------------------------------------
function TmmStringList.AddVar(const aValue: Variant): Integer;
begin
  Result := inherited Add(aValue);
end;
//------------------------------------------------------------------------------
function TmmStringList.Value(aIndex: Integer): Variant;
begin
  Result := inherited Strings[aIndex];
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TObjectStringList
//------------------------------------------------------------------------------
procedure TObjectStringList.Clear;
begin
  inherited Clear;
end;
//------------------------------------------------------------------------------
procedure TObjectStringList.ClearObject(aIndex: Integer);
begin
  if Assigned(Objects[aIndex]) then
  try
    Objects[aIndex].Free;
    Objects[aIndex] := Nil;
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TObjectStringList.ClearObjects;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    ClearObject(i);
end;
//------------------------------------------------------------------------------
procedure TObjectStringList.Delete(Index: Integer);
begin
  ClearObject(Index);
  inherited Delete(Index);
end;
//------------------------------------------------------------------------------
destructor TObjectStringList.Destroy;
begin
  ClearObjects;
  inherited Destroy;
end;
//------------------------------------------------------------------------------

end.
