object DlgRename_Data: TDlgRename_Data
  Left = 436
  Top = 330
  ActiveControl = EdtRepKey
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Rename'
  ClientHeight = 168
  ClientWidth = 488
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BtnCancel: TmmButton
    Left = 192
    Top = 132
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    Default = True
    ModalResult = 2
    TabOrder = 0
    Visible = True
    OnClick = BtnCancelClick
    AutoLabel.LabelPosition = lpLeft
  end
  object BtnOK: TmmButton
    Left = 112
    Top = 132
    Width = 75
    Height = 25
    Caption = 'OK'
    Enabled = False
    ModalResult = 1
    TabOrder = 1
    Visible = True
    OnClick = BtnOKClick
    AutoLabel.LabelPosition = lpLeft
  end
  object BtnHelp: TmmButton
    Left = 304
    Top = 132
    Width = 75
    Height = 25
    Caption = 'Help'
    TabOrder = 2
    Visible = True
    OnClick = BtnHelpClick
    AutoLabel.LabelPosition = lpLeft
  end
  object GrbKeyInfo: TmmGroupBox
    Left = 32
    Top = 16
    Width = 429
    Height = 101
    Caption = 'Key Information'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object LblCurKey: TmmLabel
      Left = 8
      Top = 32
      Width = 133
      Height = 17
      AutoSize = False
      Caption = 'Current key value:'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object LblRepKey: TmmLabel
      Left = 8
      Top = 64
      Width = 133
      Height = 17
      AutoSize = False
      Caption = 'Replace key with:'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object EdtRepKey: TmmMaskEdit
      Left = 148
      Top = 60
      Width = 209
      Height = 24
      TabOrder = 0
      Visible = True
      OnChange = EdtRepKeyChange
      AutoLabel.LabelPosition = lpLeft
    end
    object EdtCurKey: TmmMaskEdit
      Left = 148
      Top = 28
      Width = 209
      Height = 24
      Enabled = False
      ReadOnly = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
end
