object frmSelectionDialog: TfrmSelectionDialog
  Left = 425
  Top = 104
  BorderStyle = bsDialog
  Caption = '(*)Auswahldialogbox'
  ClientHeight = 355
  ClientWidth = 437
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  ShowHint = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object mmPanel4: TmmPanel
    Left = 0
    Top = 315
    Width = 437
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object bOK: TmmButton
      Left = 150
      Top = 10
      Width = 90
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 245
      Top = 10
      Width = 90
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 340
      Top = 10
      Width = 90
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object gbSelectionList: TmmPanel
    Left = 0
    Top = 0
    Width = 437
    Height = 315
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 2
    TabOrder = 1
    object mmPanel1: TmmPanel
      Left = 2
      Top = 2
      Width = 200
      Height = 311
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object mmLabel2: TmmLabel
        Left = 0
        Top = 2
        Width = 67
        Height = 16
        Caption = '(*)Startliste:'
        FocusControl = lbSource
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object lbSource: TmmListBox
        Left = 0
        Top = 20
        Width = 200
        Height = 291
        Align = alBottom
        Anchors = [akLeft, akTop, akRight, akBottom]
        Enabled = True
        ItemHeight = 16
        MultiSelect = True
        Sorted = True
        TabOrder = 0
        Visible = True
        OnDblClick = acMoveRightExecute
        OnMouseMove = OnMouseMove
        AutoLabel.Control = mmLabel2
        AutoLabel.LabelPosition = lpTop
      end
    end
    object mmPanel2: TmmPanel
      Left = 202
      Top = 2
      Width = 33
      Height = 311
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object bMoveRight: TmmSpeedButton
        Left = 5
        Top = 120
        Width = 25
        Height = 25
        Action = acMoveRight
        Anchors = [akTop]
        Glyph.Data = {
          92000000424D9200000000000000760000002800000007000000070000000100
          0400000000001C00000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00880888808800
          88808800088088000080880008808800888088088880}
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bMoveLeft: TmmSpeedButton
        Left = 5
        Top = 147
        Width = 25
        Height = 25
        Action = acMoveLeft
        Anchors = [akTop]
        Glyph.Data = {
          92000000424D9200000000000000760000002800000007000000070000000100
          0400000000001C00000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
          08808800088080000880880008808880088088880880}
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object mmPanel3: TmmPanel
      Left = 235
      Top = 2
      Width = 200
      Height = 311
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object mmLabel3: TmmLabel
        Left = 0
        Top = 2
        Width = 62
        Height = 16
        Caption = '(*)Zielliste:'
        FocusControl = lbDest
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object lbDest: TmmListBox
        Left = 0
        Top = 20
        Width = 200
        Height = 291
        Align = alBottom
        Anchors = [akLeft, akTop, akRight, akBottom]
        Enabled = True
        ItemHeight = 16
        MultiSelect = True
        Sorted = True
        TabOrder = 0
        Visible = True
        OnDblClick = acMoveLeftExecute
        OnMouseMove = OnMouseMove
        AutoLabel.Control = mmLabel3
        AutoLabel.LabelPosition = lpTop
      end
    end
  end
  object mmActionList: TmmActionList
    OnUpdate = mmActionListUpdate
    Left = 25
    Top = 61
    object acMoveRight: TAction
      Hint = '(*)Auswahl in die Zielliste uebernehmen'
      ImageIndex = 4
      OnExecute = acMoveRightExecute
    end
    object acMoveLeft: TAction
      Hint = '(*)Auswahl in die Startliste uebernehmen'
      ImageIndex = 3
      OnExecute = acMoveLeftExecute
    end
    object acOK: TAction
      Caption = '(9)OK'
      OnExecute = acOKExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe...'
      Hint = '(*)Ruft die Hilfe auf'
      ShortCut = 112
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Schliesst den Dialog'
      OnExecute = acCancelExecute
    end
  end
end
