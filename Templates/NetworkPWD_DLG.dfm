object NetworkPWD: TNetworkPWD
  Left = 390
  Top = 261
  BorderStyle = bsDialog
  Caption = 'Enter Network Password'
  ClientHeight = 116
  ClientWidth = 462
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 10
    Top = 10
    Width = 216
    Height = 13
    Caption = 'Incorrect password or unknown username for:'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lbPWD: TmmLabel
    Left = 21
    Top = 84
    Width = 49
    Height = 13
    Caption = 'Password:'
    FocusControl = edPWD
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lbConnectAs: TmmLabel
    Left = 13
    Top = 54
    Width = 57
    Height = 13
    Caption = 'Connect as:'
    FocusControl = edConnectAs
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lbPCName: TmmLabel
    Left = 20
    Top = 30
    Width = 50
    Height = 13
    Caption = 'lbPCName'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object edConnectAs: TmmEdit
    Left = 100
    Top = 50
    Width = 250
    Height = 21
    Color = clWindow
    TabOrder = 0
    Visible = True
    AutoLabel.Control = lbConnectAs
    AutoLabel.Distance = 30
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object edPWD: TmmEdit
    Left = 100
    Top = 80
    Width = 250
    Height = 21
    Color = clWindow
    PasswordChar = '*'
    TabOrder = 1
    Visible = True
    AutoLabel.Control = lbPWD
    AutoLabel.Distance = 30
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object mmButton1: TmmButton
    Left = 370
    Top = 10
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton2: TmmButton
    Left = 370
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
end
