(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: BaseForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 18.11.1998  1.00  Wss | File completed, OpenKeyRel used of TmmRegistry
| 17.02.1999  1.01  Wss | Property OnOLEDestroy implemented to decrement RefCount in OLE Server
| 22.12.1999  1.02  Wss | SetOnOLEDestroy: value assingnment changed
|                       | Destroy: exception handlingon on calling fOnOLEDestroy
| 08.03.2000  1.02  Wss | GetCreateChildform changed to check from registry if sticker are supported
| 24.04.2001  1.03  Wss | Check if caption is really different before notification via SetWindowLong in SetCaption
| 02.05.2001  1.04  Wss | Restore/SaveFormLayout: Size and State will only affected if BorderStyle = bsSizeable
| 16.05.2001  1.05  Wss | if Form is in OLEMode automatically save and restore form layout
| 10.09.2001  1.05  Wss | SetOLEMode: check if parameter is changed
| 22.02.2002  1.05  Wss | SaveFormLayout, RestoreFormLayout wird in OleMode nur noch aufgerufen, wenn
                          Fenster nicht bsDialog Eigenschaft hat.
| 28.02.2002  1.05  Wss | Aufruf von RestoreFormLayout nun beim setzen von Property LoepfePlugin
| 28.04.2004  1.05  Wss | WrapperHandling f�r Sub-OLE-Aufrufe aus Floor korrigiert (Q-Offlimit -> LabReport) 
|=========================================================================================*)
unit BaseForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, LOEPFELIBRARY_TLB;

type
  TmmForm = class(TForm)
  private
    fLoepfePlugin: ILoepfePlugin;
    fOLEMode: Boolean;
    fWrapperForm: TmmForm;
    procedure SetLoepfePlugin(const Value: ILoepfePlugin);
    function GetCaption: String;
    procedure SetCaption(const Value: String);
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure DoClose(var Action: TCloseAction); override;
    procedure SetOLEMode(const Value: Boolean); virtual;
  public
    property LoepfePlugin: ILoepfePlugin read fLoepfePlugin write SetLoepfePlugin;
    property OLEMode: Boolean read fOLEMode write SetOLEMode;
    property WrapperForm: TmmForm read fWrapperForm write fWrapperForm;

    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure RestoreFormLayout(aRegistryKey: String = ''); virtual;
    procedure SaveFormLayout(aRegistryKey: String = ''); virtual;
//    function ShowModal: Integer; override;
  published
    property Caption: String read GetCaption write SetCaption;
  end;

var
  frmData: TmmForm;

//------------------------------------------------------------------------------
function GetCreateChildForm(aFormClass: TFormClass; var aForm: TForm; aCaller: TObject = Nil; aDoCreate: Boolean = True): Boolean;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units

{$R *.DFM}

uses
  mmMBCS,

  mmCS,
  ComObj, mmRegistry, LoepfeGlobal;

//------------------------------------------------------------------------------
function GetCreateChildForm(aFormClass: TFormClass; var aForm: TForm; aCaller: TObject; aDoCreate: Boolean): Boolean;
var
  xCallerInOleMode: Boolean;
  xCallerIsForm: Boolean;

  //----------------------------------------------------------------------------
  procedure PrepareChildForm;
  begin
    if xCallerInOleMode then begin
      // if basically the form is a child form then remove menu from main form
      with aForm as TmmForm do begin
        // remove the auto merged menu from main form
        if FormStyle = fsMDIChild then
          Application.MainForm.Menu.Unmerge(Menu);

        if BorderStyle = bsDialog then
          BorderStyle := bsSingle;

        FormStyle := Forms.fsNormal;
        // copy the interface pointer to new form
        if aCaller is TmmForm then begin
          LoepfePlugin := (aCaller as TmmForm).LoepfePlugin;
        end;
        // if this form were created from the plugin directly, LoepfePlugin property
        // is set in TLoepfePlugin implementation to prevent of importing the
        // LoepfePluginIMPL unit.
      end;

    end;
  end;
  //----------------------------------------------------------------------------
  procedure CreateForm(aInstanceClass: TComponentClass; var aReference);
  var
    xInstance: TForm;
  begin
    xInstance := TForm(aInstanceClass.NewInstance);
    TForm(aReference) := xInstance;
    try
      xInstance.Create(aCaller as TForm);
    except
      TForm(aReference) := nil;
      raise;
    end;
  end;
  //----------------------------------------------------------------------------
begin
  xCallerInOleMode := False;
  xCallerIsForm    := False;
  if Assigned(aCaller) then begin
    if aCaller.InheritsFrom(TmmForm) then begin
      xCallerIsForm    := True;
      xCallerInOleMode := (aCaller as TmmForm).OLEMode;
    end else begin
      xCallerInOleMode := aCaller.InheritsFrom(TAutoObject);
    end;
  end;

  // create always a new form if it isn't based on the StickerForm
  Result := False;
  // no form of type aFormClass is available -> create new one
  if not Result and aDoCreate then
  try
    if xCallerIsForm then
      // use this form as owner to recognise in create if createparams has to be overridden
      CreateForm(aFormClass, aForm)
    else
      Application.CreateForm(aFormClass, aForm);

    PrepareChildForm;
    Result := True;
  except
  end;
end;
//------------------------------------------------------------------------------
constructor TmmForm.Create(aOwner: TComponent);
begin
  fOLEMode      := False;
  inherited Create(aOwner);

  fWrapperForm  := Nil;
  fLoepfePlugin := Nil;
end;
//------------------------------------------------------------------------------
procedure TmmForm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  if Owner is TmmForm then
  begin
    if (Owner as TmmForm).OLEMode then
    begin
      OLEMode := True;
      FormStyle := Forms.fsNormal;
      with Params do
      begin
        ExStyle := ExStyle; // or WS_EX_PALETTEWINDOW;// or WS_EX_TOPMOST;
        WndParent := TmmForm(Owner).Handle;
      end
    end

    // PPW 21/06/2017 : MM-13 Fix for modal dialogs so they stay in front on their parent
    else
    begin
      Params.WndParent := TForm(Owner).Handle;
    end;
  end;
end;
//------------------------------------------------------------------------------
destructor TmmForm.Destroy;
begin
  // OLE Automation Server with ILoepfePlugin set this property to determine the closing
  // of this form to decrement the RefCount through ObjRelease methode in TLoepfePlugin class
  if fOLEMode and (BorderStyle <> bsDialog) then begin
    SaveFormLayout;
    if Assigned(fLoepfePlugin) then
    try
      fLoepfePlugin._Release;
    except
    end;
  end;

  if Assigned(fWrapperForm) then begin
    // prevents calling close methode recursive
    fWrapperForm.WrapperForm := Nil;
    fWrapperForm.Close;
  end;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TmmForm.GetCaption: String;
begin
  Result := inherited Caption;
end;
//------------------------------------------------------------------------------
procedure TmmForm.RestoreFormLayout(aRegistryKey: String);
var
  xStr: String;
  xPath: String;
begin
  if not(csDesigning in ComponentState) then begin
    with TmmRegistry.Create do
    try
      RootKey := HKEY_CURRENT_USER;
      if aRegistryKey = '' then
        xPath := Format('%s\%s', [cRegMMWindowPos, Self.ClassName])
      else
        xPath := aRegistryKey;
      if OpenKeyReadOnly(xPath) then begin
        Position := poDesigned;
        if ValueExists('Position') then begin
          xStr := ReadString('Position');
          Top := StrToIntDef(Copy(xStr,1,Pos(',',xStr)-1),Top);
          Left:= StrToIntDef(Copy(xStr,Pos(',',xStr)+1,Length(xStr)),Left);
        end;
        if BorderStyle = bsSizeable then begin
          if ValueExists('Size') then begin
            xStr := ReadString('Size');
            Height := StrToIntDef(Copy(xStr, 1, Pos(',', xStr)-1), Height);
            Width  := StrToIntDef(Copy(xStr, Pos(',', xStr)+1, Length(xStr)), Width);
          end;
          if ValueExists('State') then begin
            xStr := ReadString('State');
            WindowState := TWindowState(StrToIntDef(xStr, Ord(WindowState)));
            if (WindowState = wsMinimized) then
              WindowState := wsNormal;
          end;
        end;
      end;
    finally
      Free;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmForm.SaveFormLayout(aRegistryKey: String);
var
  xPath: String;
begin
  if not(csDesigning in ComponentState) and (BorderStyle <> bsDialog) then
    with TmmRegistry.Create do
    try
      RootKey := HKEY_CURRENT_USER;
      if aRegistryKey = '' then
        xPath := Format('%s\%s', [cRegMMWindowPos, Self.ClassName])
      else
        xPath := aRegistryKey;

      if OpenKey(xPath, True) then begin
        WindowState := wsNormal;
        if BorderStyle = bsSizeable then begin
          WriteString('State', '0');  // writes always WindowState = wsNormal
          WriteString('Size', Format('%d,%d', [Height, Width]));
        end;
        WriteString('Position', Format('%d,%d', [Top, Left]));
      end;
    finally
      Free;
    end;
end;
//------------------------------------------------------------------------------
procedure TmmForm.SetCaption(const Value: String);
var
  xStyle: Integer;
  xBool: Boolean;
begin
  xBool := (inherited Caption <> Value);
  inherited Caption := Value;
  if OLEMode and xBool then begin
    xStyle := GetWindowLong(Handle, GWL_STYLE);
    SetWindowLong(Handle, GWL_STYLE, xStyle);
  end;
end;
//------------------------------------------------------------------------------
procedure TmmForm.SetLoepfePlugin(const Value: ILoepfePlugin);
begin
  if Assigned(Value) and (Value <> fLoepfePlugin) then begin
    fLoepfePlugin := Value;
    fLoepfePlugin._AddRef;
    OLEMode := True;
    WindowState := wsNormal;
    if (BorderStyle <> bsDialog) then
      RestoreFormLayout;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmForm.SetOLEMode(const Value: Boolean);
begin
  if fOLEMode <> Value then begin
    fOLEMode := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TmmForm.DoClose(var Action: TCloseAction);
begin
  inherited DoClose(Action);
  // Wenn z.B. der LabReport vom Q-Offlimit aufgerufen wird, dann existiert kein Link zur Floor.
  // In diesem Fall wird dem LabReport der WindowHandle vom WrapperFenster vom Q-Offlimit mitgegeben
  // und dieser wird im Datenfenster vom LabReport an seinem eigenen WrapperFenster als
  // ParentWindow gesetzt. Dies verhindert das verschwinden des LabReport Fenster hinter die Floor.
  // Hier wird nun gepr�ft, ob eine solche Konstellation vorhanden ist und es wird der ParentWindow
  // wieder gel�scht. Das ist n�tig, falls das Q-Offlimit Fenster vor dem LabReport geschlossen wird
  // und somit der Link zur Floor fehlt. Wenn dies nicht gemacht wird, gibt es einen Fehler wenn
  // anschliessend das LabReport Fenster auch geschlossen wird.
  if Assigned(WrapperForm) then begin
    if WrapperForm.ParentWindow <> 0 then
      WrapperForm.ParentWindow := 0;
  end;
end;

end.

