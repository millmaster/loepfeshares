{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SelectionDialog.pas
| Projectpart...: Millmaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.10.1999  1.00  Wss | Dialogvorlage erstellt
| 16.02.2000  1.01  Wss | Hintfunktion implementiert (OnMouseMove)
| 20.09.2002        LOK | Umbau ADO
|=========================================================================================*}
unit SelectionDialog;

interface

uses
  Classes, Consts, Forms, Menus, SysUtils, StdCtrls, Controls, Windows, BaseForm,
  mmCheckListBox, mmSpeedButton, mmListBox, mmCheckBox, mmLabel,
  mmBitBtn, mmPanel, mmGroupBox, mmPopupMenu, LoepfeGlobal,
  Buttons, ExtCtrls, CheckLst, ImgList, mmImageList, ActnList, mmActionList,
  mmBevel, ComCtrls, mmStatusBar,
  ToolWin, mmToolBar, mmButton;

type
  //............................................................................
  TfrmSelectionDialog = class(TmmForm)
    mmActionList: TmmActionList;
    acMoveRight: TAction;
    acMoveLeft: TAction;
    mmPanel4: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;
    gbSelectionList: TmmPanel;
    mmPanel1: TmmPanel;
    mmLabel2: TmmLabel;
    lbSource: TmmListBox;
    mmPanel2: TmmPanel;
    bMoveRight: TmmSpeedButton;
    bMoveLeft: TmmSpeedButton;
    mmPanel3: TmmPanel;
    mmLabel3: TmmLabel;
    lbDest: TmmListBox;
    acOK: TAction;
    acHelp: TAction;
    acCancel: TAction;
    procedure mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acMoveRightExecute(Sender: TObject);
    procedure acMoveLeftExecute(Sender: TObject);
    procedure OnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
  private
    mOldHintIndex: Integer;
  protected
    function GetSelected(aListBox: TCustomListBox; var aIndex: Integer): Boolean;
  public
  end;

//------------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  mmStringList;

{$R *.DFM}


//******************************************************************************
// TfrmSelectionDialog
//******************************************************************************
procedure TfrmSelectionDialog.acCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectionDialog.acMoveLeftExecute(Sender: TObject);
var
  xIndex: Integer;
begin
  xIndex := 0;
  while GetSelected(lbDest, xIndex) do begin
    // move selected items to left list box
    lbSource.Items.AddObject(lbDest.Items.Strings[xIndex], lbDest.Items.Objects[xIndex]);
    lbDest.Items.Delete(xIndex);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectionDialog.acMoveRightExecute(Sender: TObject);
var
  xIndex: Integer;
begin
  xIndex := 0;
  while GetSelected(lbSource, xIndex) do begin
    // move selected items to right list box
    lbDest.Items.AddObject(lbSource.Items.Strings[xIndex], lbSource.Items.Objects[xIndex]);
    lbSource.Items.Delete(xIndex);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectionDialog.acOKExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectionDialog.FormCreate(Sender: TObject);
begin
  mOldHintIndex := -1;
end;

//------------------------------------------------------------------------------
function TfrmSelectionDialog.GetSelected(aListBox: TCustomListBox; var aIndex: Integer): Boolean;
begin
  if (aIndex < 0) then
    aIndex := 0;

  Result := False;
  with aListBox do
    while (aIndex < Items.Count) do begin
      Result := Selected[aIndex];
      if Result then
        Break;
      inc(aIndex);
    end;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectionDialog.mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  bMoveRight.Enabled := lbSource.SelCount > 0;
  bMoveLeft.Enabled  := lbDest.SelCount > 0;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectionDialog.OnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  xTextWidth: Integer;
  xIndex: Integer;
begin
  with Sender as TListBox do begin
    ShowHint := False;
    xIndex := ItemAtPos(Point(X, Y), True);
    if (xIndex >= 0) and (xIndex = mOldHintIndex) then begin
      xTextWidth := Self.Canvas.TextWidth(Items.Strings[xIndex]);
      if xTextWidth > ClientWidth then begin
        Hint     := Items.Strings[xIndex];
        ShowHint := True;
      end;
    end;
  end;
  mOldHintIndex := xIndex;
end;
//------------------------------------------------------------------------------

end.

