unit BASEDialogBottom;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, StdCtrls, Buttons, mmBitBtn, mmButton;

type
  TDialogBottom = class(TmmDialog)
    bOK: TmmButton;
    bCancel: TmmButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogBottom: TDialogBottom;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

end.
