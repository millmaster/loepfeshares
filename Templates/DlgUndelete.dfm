inherited DlgUndelete_Data: TDlgUndelete_Data
  Left = 518
  Top = 233
  Caption = 'Undelete'
  PixelsPerInch = 96
  TextHeight = 13
  object LblKeyInfoUnd: TmmLabel
    Left = 32
    Top = 40
    Width = 65
    Height = 17
    AutoSize = False
    Caption = 'Key Information'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmDBListBox1: TmmDBListBox
    Left = 132
    Top = 44
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object BtnOK: TmmButton
    Left = 32
    Top = 196
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 1
    Visible = True
    OnClick = BtnOKClick
    AutoLabel.LabelPosition = lpLeft
  end
  object BtnCancel: TmmButton
    Left = 120
    Top = 196
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object BtnHelp: TmmButton
    Left = 248
    Top = 196
    Width = 75
    Height = 25
    Caption = 'Help'
    TabOrder = 3
    Visible = True
    OnClick = BtnHelpClick
    AutoLabel.LabelPosition = lpLeft
  end
  object ActiveSource: TmmDataSource
    Left = 360
    Top = 44
  end
  object SrcKeyInfoUnd: TmmDataSource
    DataSet = TblKeyInfoUnd
    Left = 356
    Top = 76
  end
  object TblKeyInfoUnd: TmmTable
    Left = 356
    Top = 108
  end
end
