(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmRegistry.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 18.11.1998  1.00  Wss | OpenKey2 to OpenKeyRel changed
|=========================================================================================*)
unit mmRegistry;

interface

uses
  Windows, Classes, SysUtils, Registry;

type
  TmmRegistry = class(TRegistry)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    function OpenKeyRel(const Key: string; CanCreate: Boolean): Boolean;
  published
    { Published declarations }
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------
function IsRelative(const Value: string): Boolean;
begin
  Result := not ((Value <> '') and (Value[1] = '\'));
end;
//------------------------------------------------------------------------------
function TmmRegistry.OpenKeyRel(const Key: string; CanCreate: Boolean): Boolean;
var
  TempKey: HKey;
  S: string;
  Disposition: Cardinal;
  Relative: Boolean;
  ErrorCode: Integer;
begin
  S := Key;
  Relative := IsRelative(S);
  if not Relative then
    Delete(S, 1, 1);
  TempKey := 0;

  if not CanCreate or (S = '') then begin
    ErrorCode := RegOpenKeyEx(GetBaseKey(Relative), PChar(S), 0, KEY_READ or KEY_WRITE, TempKey);
    if ErrorCode <> ERROR_SUCCESS then
      ErrorCode := RegOpenKeyEx(GetBaseKey(Relative), PChar(S), 0, KEY_READ, TempKey);
    Result := ErrorCode = ERROR_SUCCESS;
  end else
    Result := RegCreateKeyEx(GetBaseKey(Relative), PChar(S), 0, nil, REG_OPTION_NON_VOLATILE,
                             KEY_READ or KEY_WRITE, nil, TempKey, @Disposition) = ERROR_SUCCESS;

  if Result then begin
    if (CurrentKey <> 0) and Relative then
      S := CurrentPath + '\' + S;
    ChangeKey(TempKey, S);
  end;
end;
//------------------------------------------------------------------------------

end.
