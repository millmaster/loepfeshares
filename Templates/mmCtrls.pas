(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmCtrls.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 18.11.1998  0.00  Wss | TmmForm, TmmDialog, TmmDataModule should never be uses (mail from Hans Vervaeke)
|                       | Use BASEFORM, BASEDIALOG, BASEMODULE instead
|=========================================================================================*)
unit mmCtrls;

interface

uses Classes, StdCtrls, Forms, DB, DBCtrls, Dialogs,
     Qrctrls, ExtCtrls, Controls;

type

{ TmmFieldLabelControl }

  TmmFieldLabelPosition = (lpNormal, lpTop, lpLeft);
  TmmFieldLabelControl = class (TPersistent)
  private
    FAutoCaption: Boolean;
    FAutoProperties: Boolean;
    FPosition: TmmFieldLabelPosition;
    FDistance: Integer;
    procedure SetAutoCaption(Value: Boolean);
    procedure SetAutoProperties(Value: Boolean);
    procedure SetPosition(Value: TmmFieldLabelPosition);
    procedure SetDistance(Value: Integer);
  protected
  public
    procedure Update(AComponent: TWinControl; ALabel: TLabel; ADatasource: TDataSource;
               ADataField: string);
    constructor Create;
    procedure Assign(Source: TPersistent); override;
  published
    property AutoCaption: Boolean read FAutoCaption write SetAutoCaption default True;
    property AutoProperties: Boolean read FAutoproperties write SetAutoProperties default True;
    property Position: TmmFieldLabelPosition read Fposition write SetPosition default lpLeft;
    property Distance: Integer read FDistance write SetDistance default 6;
  end;

{ TmmReportLabelControl }

  TmmReportLabelControl = class (TPersistent)
  private
    FAutoCaption: Boolean;
    FAutoProperties: Boolean;
    FPosition: TmmFieldLabelPosition;
    FDistance: Integer;
    procedure SetAutoCaption(Value: Boolean);
    procedure SetAutoProperties(Value: Boolean);
    procedure SetPosition(Value: TmmFieldLabelPosition);
    procedure SetDistance(Value: Integer);
  protected
  public
    procedure Update(AComponent: TGraphicControl; ALabel: TQRLabel; ADataSet: TDataSet;
               ADataField: string);
    constructor Create;
    procedure Assign(Source: TPersistent); override;
  published
    property AutoCaption: Boolean read FAutoCaption write SetAutoCaption default True;
    property AutoProperties: Boolean read FAutoproperties write SetAutoProperties default True;
    property Position: TmmFieldLabelPosition read Fposition write SetPosition default lpLeft;
    property Distance: Integer read FDistance write SetDistance default 6;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{ TmmFieldLabelControl }

constructor TmmFieldLabelControl.Create;
begin
  inherited Create;
  AutoCaption := True;
  AutoProperties := True;
  Position := lpLeft;
  Distance := 6;
end;

procedure TmmFieldLabelControl.Assign(Source: TPersistent);
begin
  if Source is TmmFieldLabelControl then
  begin
    AutoCaption := TmmFieldLabelControl(Source).AutoCaption;
    AutoProperties := TmmFieldLabelControl(Source).AutoProperties;
    Position := TmmFieldLabelControl(Source).Position;
    Distance := TmmFieldLabelControl(Source).Distance;
    Exit;
  end;
  inherited Assign(Source);
end;

procedure TmmFieldLabelControl.SetAutoCaption(Value: Boolean);
begin
  if FAutoCaption <> Value then
  begin
    FAutoCaption := Value;
  end;
end;

procedure TmmFieldLabelControl.SetAutoProperties(Value: Boolean);
begin
  if FAutoProperties <> Value then
  begin
    FAutoProperties := Value;
  end;
end;

procedure TmmFieldLabelControl.SetPosition(Value: TmmFieldLabelPosition);
begin
  if FPosition <> Value then
  begin
    FPosition := Value;
    case FPosition of
      lpLeft: FDistance := 6;
      lpTop:  FDistance := 3;
    end;
  end;
end;

procedure TmmFieldLabelControl.SetDistance(Value: Integer);
begin
  if FDistance <> Value then
  begin
    FDistance := Value;
  end;
end;

procedure TmmFieldLabelControl.Update(AComponent: TWinControl; ALabel: TLabel;
           ADatasource: TDataSource; ADataField: string);
begin
  if (FAutoCaption) and (ADataSource.Dataset <> nil) then ALabel.Caption :=
    ADataSource.Dataset.FieldByName(ADataField).DisplayLabel + ':';
  if FAutoProperties then
  begin
    ALabel.Name := 'Lbl' + Copy(AComponent.Name,4,99);
    if FPosition = lpLeft then
      ALabel.Alignment := taRightJustify
    else if FPosition = lpTop then
      ALabel.Alignment := taLeftJustify;
/////    ALabel.AutoSize := True;
    ALabel.FocusControl := AComponent;
  end;
  case FPosition of
    lpLeft:
      begin
        ALabel.Alignment := taRightJustify;
        ALabel.Top := AComponent.Top + ((AComponent.Height - ALabel.Height) div 2);
        ALabel.Left := AComponent.Left - FDistance - ALabel.Width;
      end;
    lpTop:
      begin
        ALabel.Alignment := taLeftJustify;
        ALabel.Top := AComponent.Top - ALabel.Height - FDistance;
        ALabel.Left := AComponent.Left;
      end;
  end;
end;

{ TmmReportLabelControl }

constructor TmmReportLabelControl.Create;
begin
  inherited Create;
  AutoCaption := True;
  AutoProperties := True;
  Position := lpTop;
  Distance := 6;
end;

procedure TmmReportLabelControl.Assign(Source: TPersistent);
begin
  if Source is TmmReportLabelControl then
  begin
    AutoCaption := TmmReportLabelControl(Source).AutoCaption;
    AutoProperties := TmmReportLabelControl(Source).AutoProperties;
    Position := TmmReportLabelControl(Source).Position;
    Distance := TmmReportLabelControl(Source).Distance;
    Exit;
  end;
  inherited Assign(Source);
end;

procedure TmmReportLabelControl.SetAutoCaption(Value: Boolean);
begin
  if FAutoCaption <> Value then
  begin
    FAutoCaption := Value;
  end;
end;

procedure TmmReportLabelControl.SetAutoProperties(Value: Boolean);
begin
  if FAutoProperties <> Value then
  begin
    FAutoProperties := Value;
  end;
end;

procedure TmmReportLabelControl.SetPosition(Value: TmmFieldLabelPosition);
begin
  if FPosition <> Value then
  begin
    FPosition := Value;
    case FPosition of
      lpLeft: FDistance := 6;
      lpTop:  FDistance := 3;
    end;
  end;
end;

procedure TmmReportLabelControl.SetDistance(Value: Integer);
begin
  if FDistance <> Value then
  begin
    FDistance := Value;
  end;
end;

procedure TmmReportLabelControl.Update(AComponent: TGraphicControl; ALabel: TQRLabel;
           ADataSet: TDataSet; ADataField: string);
begin
  if (FAutoCaption) and (ADataset <> nil) then
  begin
    if (FPosition = lpTop) then
      ALabel.Caption := ADataset.FieldByName(ADataField).DisplayLabel
    else
      ALabel.Caption := ADataset.FieldByName(ADataField).DisplayLabel + ':';
  end;
  if FAutoProperties then
  begin
    ALabel.Name := 'Lbl' + Copy(AComponent.Name,4,99);
    if FPosition = lpLeft then
      ALabel.Alignment := taRightJustify
    else if FPosition = lpTop then
      ALabel.Alignment := taLeftJustify;
    ALabel.AutoSize := True;
//    ALabel.FocusControl := AComponent;
  end;
  case FPosition of
    lpLeft:
      begin
        ALabel.Alignment := taRightJustify;
        ALabel.Top := AComponent.Top + ((AComponent.Height - ALabel.Height) div 2);
        ALabel.Left := AComponent.Left - FDistance - ALabel.Width;
      end;
    lpTop:
      begin
        ALabel.Alignment := taLeftJustify;
//        ALabel.Top := AComponent.Top - ALabel.Height - FDistance;
        ALabel.Left := AComponent.Left;
      end;
  end;
end;

end.
