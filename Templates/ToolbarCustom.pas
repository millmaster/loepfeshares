(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ToolbarCustom.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Toolbuttons auswaehlen und setzen
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 19.11.1998  0.00  Sdo | Projekt begonnen
| 12.01.1999  0.00  Sdo | Erweitert
|=============================================================================*)
unit ToolbarCustom;

interface


uses
  Windows, Messages, SysUtils, Classes, Forms, Graphics, Controls, Dialogs,
  StdCtrls, CheckLst, Buttons, ActnList, comctrls, mmCheckListBox, mmBitBtn,
  mmActionList, mmMainMenu, mmToolBar, Menus, mmButton, IniFiles,
  MMMessages, BaseForm, mmCheckBox,
  mmRadioButton, ExtCtrls, mmRadioGroup, mmGroupBox, IvDictio, IvMulti,
  IvEMulti, mmTranslator;

//------------------------------------------------------------------------------
// some const for Templates
//------------------------------------------------------------------------------
const
  // message strings
  cMsgStdSparache1  = '(*)Sie muessen eine Standardsprache auswaehlen!';  // ivlm
  cMsgStdSparache2  = '(*)Es ist nur eine Standardsprache erlaubt!';    // ivlm

  // constants to identifies some actions
  cSeparatorChar = '-';
  cCategoryLanguage = 'SPRACHE';
  cCategoryToolbar  = 'TOOLBAR';

  // File extensions
  cTBExtension       = '.Tb';
  cIniFileExtension  = '.Ini';

  //INI-File sections
  cIniAbschittStandard = 'Standard';
  cIniDefaultActions = 'DefaultActions';


type
  TBaseConfigForm = class(TmmForm)
    Label1: TLabel;
    CheckListBox1: TmmCheckListBox;
    CheckListBoxLanguage: TmmCheckListBox;
    bCancel: TmmBitBtn;
    Label2: TLabel;
    bOK: TmmBitBtn;
    mmGroupBox1: TmmGroupBox;
    cbShowStandard: TmmCheckBox;
    cbSetStandard: TmmCheckBox;
    mmTranslator1: TmmTranslator;
    procedure bOKClick(Sender: TObject);
    procedure cbShowStandardClick(Sender: TObject);
    procedure CheckListBox1Click(Sender: TObject);
    procedure CheckListBoxLanguageClick(Sender: TObject);
    procedure cbSetStandardClick(Sender: TObject);
  private
    { Private declarations }
    mDefaultButtons, mSelectedFunctions,
    mAllFunctions, mOldToolButtons : TStringList;
    mMainForm: TForm;
    mToolBar:TmmToolbar;
    mActionList :TmmActionList;
    mMitStandardButtons : Boolean;
    mInIFile:String;
    mSaveFlag: Boolean;

    procedure GetALLFunctions(aMitStandardButtons : Boolean);
    procedure GetCheckedFunctions(aCheckBox : TmmCheckListBox);
    procedure SetUserFunctions;
    procedure AddButtons(aToolBar: TmmToolBar);
    procedure GetToolBarButtons;
    function  SaveStandard:Boolean;
    procedure GetStandard;
  public
    { Public declarations }
    constructor Create(aActionList :TmmActionlist;
                       aForm:TForm;
                       aToolBar : TmmToolbar;
                       aStandardButtons: Boolean;
                       aInIFile : String); reintroduce;

    destructor Destroy; override;
    procedure SetStandardButtons;
  end;

var
  BaseConfigForm: TBaseConfigForm;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  LoepfeGlobal;
{$R *.DFM}

//------------------------------------------------------------------------------
constructor TBaseConfigForm.Create(aActionList :TmmActionlist;
                               aForm:TForm; aToolBar : TmmToolbar;
                               aStandardButtons: Boolean;
                               aInIFile : String);
var i: Integer;
    xAction : TAction;
begin

  inherited Create(aForm);

  mMainForm   := aForm;
  mActionList := aActionList;
  mToolbar    := aToolBar;
  mMitStandardButtons:=aStandardButtons;
  mInIFile:=aInIFile;
  mSaveFlag:=False;

  mSelectedFunctions := TStringList.create;
  mAllFunctions      := TStringList.create;
  mOldToolButtons    := TStringList.create;

  // Default ToolbarButtons in 'mDefaultButtons' Liste schreiben
  // DefaultButtons haben als TAction.Tag der Wert '1'
  mDefaultButtons := TStringList.create;
  mDefaultButtons.Sorted := True;

  for i:=0 to mActionList.ActionCount-1 do
  begin
     xAction := mActionList.Actions[i] as TAction;
     if xAction.Tag = 1 then
        mDefaultButtons.Add(xAction.Name); // aus ActionList
  end;

  GetToolBarButtons;
  GetALLFunctions(aStandardButtons); // StandardButton gedrueckt ?

  // Fals nur Standardbuttons vorhanden, dann Markierung in Checkbox
  cbSetStandard.Checked := FALSE;
  if mDefaultButtons.Count = mOldToolButtons.count then
     begin
      if mDefaultButtons.Count > 0 then  cbShowStandard.Checked:=TRUE
      else  cbShowStandard.Checked:=FALSE;
    end
  else
     cbShowStandard.Checked:=FALSE;

end; // END Create
//------------------------------------------------------------------------------
destructor TBaseConfigForm.Destroy;
begin
  mDefaultButtons.free;
  mSelectedFunctions.free;
  mAllFunctions.free;
  mOldToolButtons.free;

  inherited Destroy;
end;  // END Destroy
//------------------------------------------------------------------------------

// Buttons
//********

//******************************************************************************
// Setzt ausgewaehlte Funktionen als Toolbuttons
//******************************************************************************
procedure TBaseConfigForm.bOKClick(Sender: TObject);
begin
  mDefaultButtons.clear;
  mOldToolButtons.clear;

  if cbSetStandard.Checked then
     // Speichert die neuen Standardbuttons ins INI-File
     if not SaveStandard then
        exit;

  mSelectedFunctions.clear;
  GetCheckedFunctions(CheckListBox1);
  GetCheckedFunctions(CheckListBoxLanguage);
  SetUserFunctions;
//  Close;  not necessary because ModalResult is mrOK and will be closed automatically
end; // END bOKClick
//------------------------------------------------------------------------------
procedure TBaseConfigForm.cbShowStandardClick(Sender: TObject);
begin
  // StandardToolbuttons in ToolBar einsetzen
  if (cbShowStandard.Checked) then
    GetStandard;
end;
//------------------------------------------------------------------------------
procedure TBaseConfigForm.CheckListBox1Click(Sender: TObject);
begin
  cbShowStandard.Checked := False;
end;
//------------------------------------------------------------------------------
procedure TBaseConfigForm.CheckListBoxLanguageClick(Sender: TObject);
begin
  cbShowStandard.Checked := False;
end;
//------------------------------------------------------------------------------
procedure TBaseConfigForm.cbSetStandardClick(Sender: TObject);
begin
   cbShowStandard.Checked := False;
end;
//------------------------------------------------------------------------------


//******************************************************************************
// Ermittelt ausgewaehlte Funktionen und schreibt diese in die
// Stringliste 'mSelectedFunctions'
//******************************************************************************
procedure TBaseConfigForm.GetCheckedFunctions(aCheckBox : TmmCheckListBox);
var xn, i                       : Integer;
    xCheckboxText, xTextstrList,
    xCategory, xTempText        : String;
begin

  xCategory := '';

  if (mSelectedFunctions.count-1 > 0) then
     xCategory := cSeparatorChar; // Separator fuer Sprache (andere Checkbox)

  for xn:=0 to aCheckBox.Items.Count-1 do
    // nur markierte CheckListbox-Eintraege bearbeiten
    if aCheckBox.Checked[xn] then begin
{
    if aCheckBox.State[xn] = cbChecked then
    begin
{}
      // Text aus Checkbox
      xCheckboxText := aCheckBox.Items.Strings[xn];
      // markierten Eintrag in Stringliste (mSelectedFunctions) suchen und
      // die dazugehoerigen Infos nach xTempList kopieren
      for i:=0 to mAllFunctions.count - 1 do
      begin
        // Text aus Checkbox
//         xCheckboxText := aCheckBox.Items.Strings[xn];
        // Text aus Stringliste 'mAllFunctions'
        xTextstrList := mAllFunctions.Strings[i];
        xTempText := GetIndexStr(2, xTextStrList, ';');

        // Bei Uebereinstimmung String aus 'mAllFunctions'-Stringliste
        // nach mSelectedFunctions kopieren
        if Pos(xCheckboxText , xTextstrList) > 0 then
          // Sicherheitscheck
          if length(xCheckboxText) =  length(xTempText)  then
          begin
            // Kategorie extrahieren  (nach 2. Zeichen)
            xTempText := GetIndexStr(3, xTextStrList, ';');

            // Bei Kategoriewechsel einen Separator einfuegen
            if (UpperCase(xTempText) <> UpperCase(xCategory)) and (i > 1) and
               (xCategory <> '') then
              mSelectedFunctions.Add(cSeparatorChar);

            mSelectedFunctions.AddObject(xTextstrList,mAllFunctions.Objects[i]);
            xCategory := GetIndexStr(3, xTextStrList, ';');
          end;
      end;
    end;
end; // END GetCheckedFunctions
//------------------------------------------------------------------------------

//******************************************************************************
// Loescht die Toolbar und setzt die ausgewaehlten Funktionen als Buttons
// in die Toolbar (-> AddButtons)
//******************************************************************************
procedure TBaseConfigForm.SetUserFunctions;
begin
  // Toolbuttons loeschen
  while mToolBar.ButtonCount > 0 do
    mToolBar.Buttons[0].Free;

 // Neue Toolbuttons in aToolBar einsetzen
 AddButtons(mToolBar);
end; // END SetUserFunctions
//------------------------------------------------------------------------------

//******************************************************************************
// Setzt die ausgewaehlten Funktionen als Buttons in die Toolbar
// Ein Sparache-Toolbutton ist eingedrueckt(Down) -> meistens die Defaultsparche
//******************************************************************************
procedure TBaseConfigForm.AddButtons(aToolBar: TmmToolBar);
var i : Integer;
begin
  for i := mSelectedFunctions.Count-1 downto 0  do
  begin
    with TToolButton.Create(aToolBar) do
    begin
      Parent := aToolBar;
      if mSelectedFunctions.Strings[i] = cSeparatorChar then
      begin
        Style := tbsSeparator;
        Width := 10;
      end else begin
        Style     := tbsButton;
        Action    := mSelectedFunctions.Objects[i] as TAction;
        Name      := 'tb' + inttostr(i);
        //Tag       := (Action as TAction).Tag;
        // ToolButtons als Gruppe nur bei Category 'Sprache'.
        // Es kann nur eine Sprache aktive sein.
        // Bei der Abspeicherung der Toolbuttons (Event Form-Destroy)
        // wird der Tag des Buttons der zuletzt gewaehlten Sprache
        // (Button-Down) auf Tag = 1 gesetzt.
{
        if UpperCase((Action as TAction).Category) = cCategoryLanguage then
        begin
          AllowAllUp := True;
          Grouped    := True;
          Style      := tbsCheck;
          Down       := FALSE;
        end;
{}
      end;
    end;
  end;
end; // END AddButtons
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittlet alle Funktionen und markiert in der Checkbox die StandardButtons
// oder die vorhandenen Buttons in der Toolbar
//******************************************************************************
procedure TBaseConfigForm.GetALLFunctions(aMitStandardButtons : Boolean);
var i, xInsPos                         :Integer;
    xAct                               :TAction;
    xActName, xActCaption, xActCategory :String;
begin
  CheckListBox1.clear;
  CheckListBoxLanguage.clear;
  mAllFunctions.clear;
  //mAllFunctions.Sorted := TRUE;
  // mAllFunctions.Sort;
  //CheckListBox1.Sorted:=TRUE;
  xInsPos := 0;
  for i:=0 to mActionList.ActionCount-1 do
  begin
    xAct     := mActionList.Actions[i] as TAction;
    xActName := xAct.Name;
    xActCaption := DeleteSubStr(Trim(xAct.Caption), '&');
    xActCategory := xAct.Category;

    with mActionList do
    begin
      // Markierung der Sprache in 'CheckListBoxLanguage'
      if UpperCase(xActCategory) = cCategoryLanguage then
      begin
        if mAllFunctions.Count > 0 then
          mAllFunctions.Add(cSeparatorChar);  // indicates to insert a separator in ToolBar

        if xActCaption <> '' then
          xInsPos := CheckListBoxLanguage.Items.Add(xActCaption);

        // Defaultbuttons markieren
        if aMitStandardButtons then
          if (mDefaultButtons.IndexOf(xActName) >= 0) or (mOldToolButtons.IndexOf(xActName) >= 0) then
             CheckListBoxLanguage.Checked[xInsPos] := True;
        // Existiernen auf Toolbar Toolbuttons, so werden diese in der Checkbox
        // markiert
        if (mOldToolButtons.IndexOf(xActName) >= 0) then
            CheckListBoxLanguage.Checked[xInsPos] := True;

      end else if (UpperCase(xActCategory) <> cCategoryToolbar) and
                  (xAct.Visible)  then begin
        // Markierung der Funktionen in 'CheckListBox1'
        if xActCaption <> '' then
        xInsPos := CheckListBox1.Items.Add(xActCaption);

        // Defaultbuttons markieren
        if aMitStandardButtons then
           if (mDefaultButtons.IndexOf(xActName) >= 0) or (mOldToolButtons.IndexOf(xActName) >= 0) then
           CheckListBox1.Checked[xInsPos] := True;

        // Existiernen auf Toolbar Toolbuttons, so werden diese in der Checkbox
        // markiert
        if (mOldToolButtons.IndexOf(xActName) >= 0) then
            CheckListBox1.Checked[xInsPos] := True;
      end;

      //Action und Actionname in Stringliste 'mAllFunctions' kopieren
      if xActCaption <> '' then
      mAllFunctions.AddObject(xActName + ';' + xActCaption + ';' + xActCategory, xAct);

    end;
  end;
end; // END GetALLFunctions
//------------------------------------------------------------------------------

//******************************************************************************
// Setzt die StandardButtons von Extern (Aufruf von 'Main.pas')
//******************************************************************************
procedure TBaseConfigForm.SetStandardButtons;
begin
 // StandardToolbuttons in aToolBar einsetzen
  mOldToolButtons.clear;
  GetALLFunctions(mMitStandardButtons);
  GetCheckedFunctions(CheckListBox1);
  GetCheckedFunctions(CheckListBoxLanguage);
  SetUserFunctions;
end; // END SetStandardButtons
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittelt vorhandene Toolbuttons in der Toolbar
//******************************************************************************
procedure TBaseConfigForm.GetToolBarButtons;
var i      : Integer;
    xAction: TAction;
begin
  mOldToolButtons.Clear;
  for i:=0 to mToolBar.ButtonCount-1 do
  begin
    xAction := mToolBar.Buttons[i].Action as TAction;
    //Nur Buttons, keine Seperatoren
    if xAction <> NIL then
      mOldToolButtons.Add(xAction.Name);
  end;
end; // END GetToolBarButtons
//------------------------------------------------------------------------------

//******************************************************************************
// Schreibt Standard Funktionen ins INI-File
//******************************************************************************
function TBaseConfigForm.SaveStandard:Boolean;
var Ini            : TIniFile;
    xi, xActCount  : Integer;
    xText, xStdAct : String;
    xAct           : TAction;
begin
  Result :=TRUE;
  GetCheckedFunctions(CheckListBox1);
  GetCheckedFunctions(CheckListBoxLanguage);

  //Als Standardsprache ist nur eine Sprache erlaubt
 {
  xActCount:=0;
  for xi:=0 to CheckListBoxLanguage.Items.Count-1 do
    if CheckListBoxLanguage.Checked[xi] then
      inc(xActCount);

  // keine Standard-Sprache ausgewaehlt
  if xActCount = 0 then
  begin
    beep;
    MessageDlg(cMsgStdSparache1, mtInformation,[mbYes], 0);
    Result :=FALSE;
    mSelectedFunctions.Clear;
    exit;
  end else if xActCount > 1 then
  begin
    // mehr als eine Standard-Sprache ausgewaehlt
    beep;
    MessageDlg(cMsgStdSparache2, mtInformation,[mbYes], 0);
    Result :=FALSE;
    mSelectedFunctions.Clear;
    exit;
  end;
  {}
  for xi := mSelectedFunctions.Count-1 downto 0  do
   if mSelectedFunctions.Strings[xi] <> '' then
   begin
     xStdAct:= copy(mSelectedFunctions.Strings[xi], 0,
                              pos(';',mSelectedFunctions.Strings[xi])-1);
     xText := copy(mSelectedFunctions.Strings[xi], 0,
                              pos(';',mSelectedFunctions.Strings[xi])) + xText;

     for xActCount:=0 to mActionList.ActionCount-1 do
       begin
         mActionList.Actions[xActCount].Tag := 0;
         xAct := mActionList.Actions[xActCount] as TAction;
         if xStdAct = xAct.Name then
           mActionList.Actions[xActCount].Tag := 1;
       end;
   end;

  // Pfad mit Filenamen
  Ini:=TIniFile.Create(mIniFile);
  //Daten
  Ini.WriteString(cIniAbschittStandard, cIniDefaultActions, xText );
  Ini.free;
end; // END SaveStandard
//------------------------------------------------------------------------------

//******************************************************************************
// Liest die Stanardbuttons aus dem INI-File
//******************************************************************************
procedure TBaseConfigForm.GetStandard;
var Ini           : TIniFile;
    xText         : String;
    xList         : TStringlist;
    xi, xActCount :Integer;
begin
  mDefaultButtons.clear;
  mOldToolButtons.clear;
  CheckListBox1.clear;

  xList:=TStringlist.Create;

  Ini:=TIniFile.Create(mIniFile);
  xText:=ini.ReadString(cIniAbschittStandard,cIniDefaultActions, '' );

  // Actions in Stringliste packen
  while ( pos(';', xText) >0) do
    begin
      xi:=  pos(';', xText);
      xList.Add(copy(xText,0,pos(';', xText)-1));
      xText:=Copy(xText,xi+1, Length(xText) );
    end;

  //StandardActions -> Action.Tag auf 1 setzen
  for xi:=0 to xList.Count-1 do begin
    for xActCount:=0 to mActionList.ActionCount-1 do
      if xList.Strings[xi] = mActionList.Actions[xActCount].name then
      begin
        mActionList.Actions[xActCount].tag:=1;
        // Standardbuttons in TListen schreiben
        mDefaultButtons.Add(mActionList.Actions[xActCount].Name);
        mOldToolButtons.Add(mActionList.Actions[xActCount].Name);
      end;
  end;
  xList.free;

  GetALLFunctions(TRUE);
  cbShowStandard.Checked:=TRUE;
end; // GetStandard
//------------------------------------------------------------------------------

end.


