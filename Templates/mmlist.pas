(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmList.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
| 21.06.2002  0.00  Wss | Klasse um virtuelle Methoden Clear und Delete erweitert,
                          damit bei abgeleiteten Klassen nur noch Delete ueberschrieben werden
                          muss umd das Objekt sauber aufzuraeumen.
|=========================================================================================*)
unit mmList;

interface

uses
  Classes, Consts, SysUtils;

type
  TmmList = class(TList)
  private
  protected
  public
    procedure Assign(Source: TObject); virtual;
    procedure Clear; override;
    procedure Delete(Index: Integer); virtual;
  published
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------
// TmmList
//------------------------------------------------------------------------------
procedure TmmList.Assign(Source: TObject);
var
  xSourceName: string;
begin
  if Source <> nil then
    xSourceName := Source.ClassName
  else
    xSourceName := 'nil';
  raise EConvertError.CreateResFmt(@SAssignError, [xSourceName, ClassName]);
end;
//------------------------------------------------------------------------------
procedure TmmList.Clear;
begin
  while Count > 0 do
    Delete(0);
  inherited Clear;
end;
//------------------------------------------------------------------------------
procedure TmmList.Delete(Index: Integer);
begin
  inherited Delete(Index);
end;
//------------------------------------------------------------------------------
end.

