object DlgPick_Date: TDlgPick_Date
  Left = 401
  Top = 220
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Pick Date'
  ClientHeight = 271
  ClientWidth = 446
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object PnlMain: TmmPanel
    Left = 0
    Top = 0
    Width = 446
    Height = 239
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object BtnPrevMonth: TmmSpeedButton
      Left = 60
      Top = 12
      Width = 24
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333FF3333333333333003333333333333F77F33333333333009033
        333333333F7737F333333333009990333333333F773337FFFFFF330099999000
        00003F773333377777770099999999999990773FF33333FFFFF7330099999000
        000033773FF33777777733330099903333333333773FF7F33333333333009033
        33333333337737F3333333333333003333333333333377333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      Visible = True
      OnClick = BtnPrevMonthClick
      AutoLabel.LabelPosition = lpLeft
    end
    object BtnNextMonth: TmmSpeedButton
      Left = 360
      Top = 12
      Width = 24
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333FF3333333333333003333
        3333333333773FF3333333333309003333333333337F773FF333333333099900
        33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
        99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
        33333333337F3F77333333333309003333333333337F77333333333333003333
        3333333333773333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      Visible = True
      OnClick = BtnNextMonthClick
      AutoLabel.LabelPosition = lpLeft
    end
    object LblMonthName: TmmLabel
      Left = 92
      Top = 16
      Width = 249
      Height = 16
      Alignment = taCenter
      AutoSize = False
      Caption = 'August, 1996'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object Calendar: TCalendar
      Left = 74
      Top = 38
      Width = 295
      Height = 139
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      StartOfWeek = 0
      TabOrder = 0
      OnChange = CalendarChange
      OnDblClick = CalendarDblClick
    end
  end
  object PnlButtons: TmmPanel
    Left = 0
    Top = 239
    Width = 446
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 1
    object BtnCancel: TmmButton
      Left = 360
      Top = 2
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object BtnOk: TmmButton
      Left = 276
      Top = 2
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
end
