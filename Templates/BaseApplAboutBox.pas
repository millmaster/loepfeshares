(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: About.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: MillMaster Info Fenster  (MM Template)
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.11.1998  0.00  Sdo |
| 12.02.2003  0.00  Wss | Anpassung an Cooperate Identity
|=============================================================================*)
unit BaseApplAboutBox;

interface

uses
  BaseForm, Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, mmPanel, mmImage, mmLabel, mmButton, IvDictio,
  IvMulti, IvEMulti, mmTranslator;

type
  TBaseApplAboutBoxForm = class(TmmForm)
    mmTranslator: TmmTranslator;
    mmPanel1: TmmPanel;
    BitBtn1: TmmButton;
    mmImage1: TmmImage;
    lbProd: TmmLabel;
    lbVers: TmmLabel;
    mmLabel1: TmmLabel;
  private
    { Private declarations }
    fProductName: String;
    procedure SetProductName(const Value: String);
    procedure SetProdName;
  protected
    procedure  Loaded; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property ProductName : String write SetProductName;

  end;

var
  BaseApplAboutBoxForm: TBaseApplAboutBoxForm;


implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  LoepfeGlobal;

resourcestring
  cProdName = '(20)Produktename'; //ivlm

//------------------------------------------------------------------------------
// TBaseApplAboutBoxForm }
//------------------------------------------------------------------------------
constructor TBaseApplAboutBoxForm.Create(AOwner: TComponent);
begin
   inherited;
end;
//------------------------------------------------------------------------------
procedure TBaseApplAboutBoxForm.Loaded;
var xVers: String;
begin
  inherited;
  SetProdName;

   // Version
  xVers :=  GetFileVersion(Application.ExeName);
  if xVers = '' then
    xVers := DateToSTr(FileDateToDateTime(FileAge(Application.ExeName)));

  lbVers.Caption := Translate(lbVers.Caption) + ' :  ' + xVers;
end;
//------------------------------------------------------------------------------
procedure TBaseApplAboutBoxForm.SetProdName;
var xAppl : String;
begin
  // Produktname oder Dateiname
  xAppl := ExtractFileName(Application.ExeName);
  xAppl := Copy(xAppl, 0,  Pos('.', xAppl)-1 );
  if fProductName = '' then fProductName := xAppl;
  lbProd.Caption :=  cProdName + ' :  ' + Translate(fProductName);
end;
//------------------------------------------------------------------------------
procedure TBaseApplAboutBoxForm.SetProductName(const Value: String);
begin
  fProductName := Value;
  SetProdName;
end;
//------------------------------------------------------------------------------
end.

