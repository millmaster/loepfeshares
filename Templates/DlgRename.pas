(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DlgRename.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.1998  0.00  Wss | Initial Release
|=========================================================================================*)
unit DlgRename;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmCtrls, StdCtrls, mmLabel, ComCtrls, mmButton, ExtCtrls, mmPanel,
  mmLookupBox, mmGroupBox, Mask, mmEdit, mmMaskEdit, BaseDialog;

type
  TDlgRename_Data = class(TmmDialog)
    BtnCancel: TmmButton;
    BtnOK: TmmButton;
    BtnHelp: TmmButton;
    GrbKeyInfo: TmmGroupBox;
    LblCurKey: TmmLabel;
    LblRepKey: TmmLabel;
    EdtRepKey: TmmMaskEdit;
    EdtCurKey: TmmMaskEdit;
    procedure BtnHelpClick(Sender: TObject);
    procedure EdtRepKeyChange(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
//    FDataField: String;  //hver
    FCurrentKey: String;
    FReplaceKey: String;
    FCharCase: TEditCharCase;
    FEditMask: String;
  public
    procedure PrepareData;
    property CurrentKey: String read FCurrentKey write FCurrentKey;
    property ReplaceKey: String read FReplaceKey write FReplaceKey;
    property CharCase: TEditCharCase read FCharCase write FCharCase;
    property EditMask: String read FEditMask write FEditMask;
  end;

var
  DlgRename_Data: TDlgRename_Data;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TDlgRename_Data.PrepareData;
begin
  EdtCurKey.CharCase := CharCase;
  EdtCurKey.EditMask := EditMask;
  EdtRepKey.CharCase := CharCase;
  EdtRepKey.EditMask := EditMask;
end;

procedure TDlgRename_Data.FormShow(Sender: TObject);
begin
  ReplaceKey := '';
  EdtRepKey.Text := ReplaceKey;
  EdtCurKey.Text := CurrentKey;
end;

procedure TDlgRename_Data.EdtRepKeyChange(Sender: TObject);
begin
  inherited;
  if Trim(EdtRepKey.Text) <> '' then
  begin
    BtnOK.Enabled     := True;
    BtnOK.Default     := True;
    BtnCancel.Default := False;
  end
  else
  begin
    BtnOK.Enabled     := False;
    BtnOK.Default     := False;
    BtnCancel.Default := True;
  end;
end;

procedure TDlgRename_Data.BtnOKClick(Sender: TObject);
begin
  if Trim(EdtRepKey.Text) <> '' then
    ReplaceKey := Trim(EdtRepKey.Text)
  else
    ReplaceKey := Trim(CurrentKey);
end;

procedure TDlgRename_Data.BtnCancelClick(Sender: TObject);
begin
  ReplaceKey := '';
end;

procedure TDlgRename_Data.BtnHelpClick(Sender: TObject);
begin
  inherited;
  Application.HelpContext(HelpContext);
end;

end.
