object DlgAbout_Data: TDlgAbout_Data
  Left = 397
  Top = 343
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 168
  ClientWidth = 297
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PnlAbout: TmmPanel
    Left = 0
    Top = 0
    Width = 297
    Height = 168
    Align = alClient
    TabOrder = 0
    object PnlAboutInfo: TmmPanel
      Left = 3
      Top = 3
      Width = 290
      Height = 128
      BevelOuter = bvLowered
      TabOrder = 0
      object LblProgramVersion: TmmLabel
        Left = 7
        Top = 73
        Width = 276
        Height = 18
        Alignment = taCenter
        AutoSize = False
        Caption = 'LblProgramVersion'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object LblCopyRight: TmmLabel
        Left = 7
        Top = 98
        Width = 276
        Height = 17
        Alignment = taCenter
        AutoSize = False
        Caption = 'LblCopyRight'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object ImgProgramIcon: TmmImage
        Left = 8
        Top = 8
        Width = 38
        Height = 38
        Stretch = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object LblProgramName: TmmLabel
        Left = 7
        Top = 49
        Width = 276
        Height = 18
        Alignment = taCenter
        AutoSize = False
        Caption = 'LblProgramName'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object BtnOK: TmmButton
      Left = 119
      Top = 140
      Width = 61
      Height = 20
      Cancel = True
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
end
