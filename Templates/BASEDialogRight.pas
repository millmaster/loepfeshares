unit BASEDialogRight;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, StdCtrls, Buttons, mmBitBtn;

type
  TDialogRight = class(TmmDialog)
    bOK: TmmBitBtn;
    bCancel: TmmBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogRight: TDialogRight;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

end.
