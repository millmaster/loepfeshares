inherited DialogRight: TDialogRight
  Top = 198
  BorderStyle = bsDialog
  Caption = 'DialogRight'
  ClientHeight = 225
  ClientWidth = 418
  PixelsPerInch = 96
  TextHeight = 13
  object bOK: TmmBitBtn
    Left = 326
    Top = 10
    Width = 87
    Height = 25
    Anchors = [akTop, akRight]
    TabOrder = 0
    Visible = True
    Kind = bkOK
    AutoLabel.LabelPosition = lpLeft
  end
  object bCancel: TmmBitBtn
    Left = 326
    Top = 45
    Width = 87
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '(9)Abbrechen'
    TabOrder = 1
    Visible = True
    Kind = bkCancel
    AutoLabel.LabelPosition = lpLeft
  end
end
