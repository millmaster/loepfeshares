program YMSettingsMultilizer;
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  Forms,
  YMSettingsGUI in 'W:\LoepfeShares\YMSettings\YMSettingsGUI.pas' {GUIYMSettings: TFrame},
  CurveLayer in 'W:\LoepfeShares\YMSettings\CurveLayer.pas',
  DialogConfigCode in 'W:\LoepfeShares\YMSettings\DialogConfigCode.pas',
  DisplayLayer in 'W:\LoepfeShares\YMSettings\DisplayLayer.pas',
  EditBox in 'W:\LoepfeShares\YMSettings\EditBox.pas',
  KeySpinEdit in 'W:\LoepfeShares\YMSettings\KeySpinEdit.pas',
  PresetValue in 'W:\LoepfeShares\YMSettings\PresetValue.pas',
  QualityMatrix in 'W:\LoepfeShares\YMSettings\QualityMatrix.pas',
  QualityMatrixBase in 'W:\LoepfeShares\YMSettings\QualityMatrixBase.pas',
  QualityMatrixDef in 'W:\LoepfeShares\YMSettings\QualityMatrixDef.pas',
  QualityMatrixDesc in 'W:\LoepfeShares\YMSettings\QualityMatrixDesc.pas',
  ScaleLayer in 'W:\LoepfeShares\YMSettings\ScaleLayer.pas',
  SelectionLayer in 'W:\LoepfeShares\YMSettings\SelectionLayer.pas',
  YMBasicProdParaBox in 'W:\LoepfeShares\YMSettings\YMBasicProdParaBox.pas' {GBBasicProdPara: TFrame},
  YMDataDef in 'W:\LoepfeShares\YMSettings\YMDataDef.pas',
  YMHostDef in 'W:\LoepfeShares\YMSettings\YMHostDef.pas',
  YMMachConfigBox in 'W:\LoepfeShares\YMSettings\YMMachConfigBox.pas' {GBMachConfigBox: TFrame},
  YMParaDBAccess in 'W:\LoepfeShares\YMSettings\YMParaDBAccess.pas',
  YMParaDBAccessForGUI in 'W:\LoepfeShares\YMSettings\YMParaDBAccessForGUI.pas',
  YMParaDef in 'W:\LoepfeShares\YMSettings\YMParaDef.pas',
  YMParaEditBox in 'W:\LoepfeShares\YMSettings\YMParaEditBox.pas',
  YMParaUtils in 'W:\LoepfeShares\YMSettings\YMParaUtils.pas',
  YMParaUtilsNew in 'W:\LoepfeShares\YMSettings\YMParaUtilsNew.pas',
  YMParaUtilsSav in 'W:\LoepfeShares\YMSettings\YMParaUtilsSav.pas',
  ChannelCurve in 'W:\LoepfeShares\YMSettings\ChannelCurve.pas',
  BASEDialog in 'W:\LOEPFESHARES\TEMPLATES\BASEDIALOG.pas' {mmDialog};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TmmDialog, mmDialog);
  Application.Run;
end.
